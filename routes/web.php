<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AddBrandController;
use App\Http\Controllers\DeleteImageController;
use App\Http\Controllers\FessController;
use App\Http\Controllers\PatentController;
use App\LitigationNotice;

Route::resource('agency','AgencyController');

Route::get('/tests','AddBrandController@testss');
Route::get('/analytics','AddBrandController@Analytics');

Route::get('/design/show', 'DesignController@index');
Route::post('/design/save', 'DesignController@store');
Route::get('/design/create', 'DesignController@Create');
Route::get('/design/{id}/edit','DesignController@edit');
Route::post('/design/update_e/{id}','DesignController@update');
Route::get('/design/{id}/view', 'DesignController@view');
Route::get('design/{id}/update_status', 'Update_Status_Design_Controller@status_create');
Route::post('/design/status/submit', 'Update_Status_Design_Controller@submit');
Route::get('/design/license_in/{id}','Design_lic_in_Controller@license_create');
Route::post('/license_in/save/design', 'Design_lic_in_Controller@store');

Route::get('/design/license_in/edit/{id}','Design_lic_in_Controller@license_edit');
Route::post('/license_in/save/design/{id}', 'Design_lic_in_Controller@update');

Route::get('/design/license_out/{id}','Design_lic_out_Controller@license_create');
Route::post('/license_out/save/design', 'Design_lic_out_Controller@store');

Route::get('/design/license_out/edit/{id}','Design_lic_out_Controller@license_edit');
Route::post('/license_out/save/design/{id}', 'Design_lic_out_Controller@update');

Route::get('/design/assign_in/{id}','Design_ass_in_Controller@assign_create');
Route::post('/assign_in/save/design', 'Design_ass_in_Controller@store');
Route::get('/design/assign_in/edit/{id}','Design_ass_in_Controller@assign_edit');
Route::post('/assign_in/save/design/{id}', 'Design_ass_in_Controller@update');

Route::get('/design/assign_out/{id}','Design_ass_out_Controller@assign_create');
Route::post('/assign_out/save/design', 'Design_ass_out_Controller@store');

Route::get('/design/assign_out/edit/{id}','Design_ass_out_Controller@assign_edit');
Route::post('/assign_out/save/design/edit/{id}', 'Design_ass_out_Controller@update');

Route::post('/design/show', 'DesignController@search');
Route::get('design/update_status/edit/{id}','DesignController@edit_status');
Route::post('/design/edit_status/submit','DesignController@edit_status_save');

// Copyright

Route::resource('copyrights', 'CopyrightController');
Route::post('copyright/update/{id}',['uses' =>'CopyrightController@update','as' => 'copyrights.update']);
Route::post('copyrights/store',['uses' =>'CopyrightController@store','as' => 'copyrights.store']);
Route::get('/copyright/{id}/update_status', 'update_status_copyrightController@status_create');
Route::post('/copyright/status/submit', 'update_status_copyrightController@submit');
Route::get('/copyright/license_in/{id}','Copyright_lic_in_Controller@license_create');
Route::post('/license_in/save', 'Copyright_lic_in_Controller@store');

Route::get('/copyright/license_in/edit/{id}','Copyright_lic_in_Controller@license_edit');
Route::post('/license_in/save/{id}', 'Copyright_lic_in_Controller@update');

Route::get('/copyright/license_out/{id}','Copyright_lic_out_Controller@license_create');
Route::post('/license_out/save', 'Copyright_lic_out_Controller@store');

Route::get('/copyright/license_out/edit/{id}','Copyright_lic_out_Controller@license_edit');
Route::post('/license_out/save/{id}', 'Copyright_lic_out_Controller@update');

Route::get('/copyright/assign_in/{id}','Copyright_ass_in_Controller@assign_create');
Route::post('/assign_in/save', 'Copyright_ass_in_Controller@store');
Route::get('/copyright/assign_in/edit/{id}','Copyright_ass_in_Controller@assign_edit');
Route::post('/assign_in/save/{id}', 'Copyright_ass_in_Controller@update');
Route::get('/copyright/assign_out/{id}','Copyright_ass_out_Controller@assign_create');
Route::post('/assign_out/save', 'Copyright_ass_out_Controller@store');
Route::get('/copyright/assign_out/edit/{id}','Copyright_ass_out_Controller@assign_edit');
Route::post('/assign_out/save/{id}', 'Copyright_ass_out_Controller@edit_update');
Route::get('/copyright/{id}/registration_certificate','Copyright_roc_Controller@roc_create');
Route::post('copyrights', 'CopyrightController@search');
Route::get('copyright/edit/update_status/{id}','CopyrightController@update_status_create');
Route::post('copyright/edit_status/submit','CopyrightController@update_status_save');


Route::get('/patent/show', 'PatentController@index');
Route::get('/patent/create', 'PatentController@Create');
Route::get('/patent/edit/{id}', 'PatentController@edit');
Route::post('/patent/submit','PatentController@store');
Route::post('/patent/edit/save','PatentController@update');
Route::get('/patent/view/{id}','PatentController@show');

Route::post('/patent/show', 'PatentController@search');

Route::get('/patent/update_status/{id}','PatentController@update_s');
Route::post('/patent/save/status','PatentController@save_status');

Route::get('/patent/license_in/{id}','PatentLicInController@create');
Route::post('/patent/license_in/save','PatentLicInController@store');

Route::get('/patent/license_in/edit/{id}','PatentLicInController@edit');
Route::post('/patent/license_in/save/{id}','PatentLicInController@update');

Route::get('/patent/license_out/{id}','PatentLicOutController@create');
Route::post('/patent/license_out/save','PatentLicOutController@store');

Route::get('/patent/license_out/edit/{id}','PatentLicOutController@edit');
Route::post('/patent/license_out/save/{id}','PatentLicOutController@update');

Route::get('/patent/assign_in/{id}','PatentAssInController@create');
Route::post('/patent/assign_in/save','PatentAssInController@store');

Route::get('/patent/assign_in/edit/{id}','PatentAssInController@edit');
Route::post('/patent/assign_in/save/edit/{id}','PatentAssInController@update');

Route::get('/patent/assign_out/{id}','PatentAssOutController@create');
Route::post('/patent/assign_out/save','PatentAssOutController@store');

Route::get('/patent/assign_out/edit/{id}','PatentAssOutController@edit');
Route::post('/patent/assign_out/save/{id}','PatentAssOutController@update');

Route::resource('applicant_category','CategoryApplicantController');

Route::get('/invest/show', 'AddBrandController@InvestShow');
Route::post('/invest/show', 'AddBrandController@search');
Route::get('/invest/create', 'AddBrandController@InvestCreate');
Route::post('/invest/save', 'AddBrandController@InvestSave');
Route::get('/edit_inv/{id}', 'AddBrandController@EditInv');
Route::post('/invest/update', 'AddBrandController@InvestUpdate');
Route::get('/expensein/{id}/expenses', 'AddBrandController@expensesin');
Route::post('expensein/submit', 'AddBrandController@expensave');
Route::get('/expensein/{id}/edit','AddBrandController@expeins_edit');
Route::post('expensein/edit/submit','AddBrandController@expeins_save');

Route::get('show_inv/{id}','AddBrandController@show_all');
Route::get('/show/all','AddBrandController@viewall');

Route::get('/view_enforce/{id}','AddBrandController@EnforceView');
Route::get('/enforce/show', 'AddBrandController@EnforceShow');
Route::post('/enforce/show', 'AddBrandController@searche');
Route::get('/enforce/create', 'AddBrandController@EnforceCreate');
Route::post('/enforce/save','AddBrandController@EnforceSave');
Route::get('/edit_enforce/{id}', 'AddBrandController@EditEnf');
Route::post('/enforce/update', 'AddBrandController@EnforceUpdate');
Route::get('/expensen/{id}/expenses', 'AddBrandController@expensesen');
Route::post('expensen/submit', 'AddBrandController@expenesave');
Route::get('/expensen/{id}/edit','AddBrandController@expens_edit');
Route::post('expensen/edit/submit','AddBrandController@expens_save');
Route::get('/fir_details/{id}', 'AddBrandController@Fir');
Route::post('/fir/submit','AddBrandController@FirSave');

Route::get('/targets/show', 'AddBrandController@TargetsShow');
Route::get('/targets/create', 'AddBrandController@TargetsCreate');
Route::post('/targets/save', 'AddBrandController@TargetsSave');
Route::get('edit/{id}/target', 'AddBrandController@TargetsEdit');
Route::post('/targets/update', 'AddBrandController@TargetsUpdate');

Route::get('/state/show', 'AddBrandController@state');
Route::get('/state/create', 'AddBrandController@stateCreate');
Route::post('/state/save', 'AddBrandController@stateSave');
Route::get('edit/{id}/state', 'AddBrandController@stateEdit');
Route::post('/state/update', 'AddBrandController@stateUpdate');


Route::get('/city/show', 'AddBrandController@city');
Route::get('/city/create', 'AddBrandController@cityCreate');
Route::post('/city/save', 'AddBrandController@citySave');
Route::get('edit/{id}/city', 'AddBrandController@cityEdit');
Route::post('/city/update', 'AddBrandController@cityUpdate');

Route::get('/', 'FrontEndController@index');
Route::get('/dashbord', 'FrontEndController@index');
Route::get('/tm_portfolio', 'FrontEndController@tradmark_portfolio');
Route::get('/tm_dashbord', 'FrontEndController@tm_index');
Route::get('/new_tradmark', 'FrontEndController@add_tradmark');
Route::get('/tradmark_summary', 'FrontEndController@tradmark_summary');
Route::post('/new_tradmark/save', 'AddTradmarkController@Tradmarksave');
Route::get('/edit_summary/{id}', 'AddTradmarkController@edit');
Route::post('/tradmark/update', 'AddTradmarkController@update');
Route::get('/tradmark/{id}/show', 'AddTradmarkController@show_tradmark');
Route::post('update/status', 'AddTradmarkController@update_status');
Route::get('unit/show', 'AddTradmarkController@ShowUnit');
Route::get('unit/create', 'AddTradmarkController@UnitCreate');
Route::post('unit/save', 'AddTradmarkController@UnitSave');
Route::get('edit/{id}/unit', 'AddTradmarkController@UnitEdit');
Route::post('unit/update', 'AddTradmarkController@UnitUpdate');

Route::get('function/show', 'AddTradmarkController@ShowFunction');
Route::get('function/create', 'AddTradmarkController@FunctionCreate');
Route::post('function/save', 'AddTradmarkController@FunctionSave');
Route::get('edit/{id}/function', 'AddTradmarkController@FunctionEdit');
Route::post('function/update', 'AddTradmarkController@FunctionUpdate');

Route::get('designation/show', 'AddTradmarkController@ShowDesignation');
Route::get('designation/create', 'AddTradmarkController@DesignationCreate');
Route::post('designation/save', 'AddTradmarkController@DesignationSave');
Route::get('edit/{id}/designation', 'AddTradmarkController@DesignationEdit');
Route::post('designation/update', 'AddTradmarkController@DesignationUpdate');

Route::get('entity/show', 'AddTradmarkController@entity');
Route::get('entity/create', 'AddTradmarkController@entityCreate');
Route::post('entity/save', 'AddTradmarkController@entitySave');
Route::get('edit/{id}/entity', 'AddTradmarkController@entityEdit');
Route::post('entity/update/{id}', 'AddTradmarkController@entityUpdate');

Route::get('entity_mapping/show', 'AddTradmarkController@entityMapping');
Route::get('entity_mapping/create', 'AddTradmarkController@entityMappingCreate');
Route::post('entity_mapping/save', 'AddTradmarkController@entityMappingSave');

Route::get('user/show', 'UserController@ShowUser');
Route::get('user/create', 'UserController@CreateUser');
Route::post('user/save', 'UserController@UserSave');
Route::get('user/{id}/edit', 'UserController@EditUser');
Route::post('user/update', 'UserController@UpdateUser');
Route::post('update/user/status', 'UserController@update_status');

Route::get('/master', 'AddTradmarkController@master');

Route::get('/nature_of_the_application/show', 'AddTradmarkController@NatureApplicationShow');
Route::get('/nature_of_the_application/create', 'AddTradmarkController@NatureApplicationCreate');
Route::post('/nature_of_the_application/save', 'AddTradmarkController@NatureApplicationSave');
Route::get('edit/{id}/nature_of_application', 'AddTradmarkController@NatureApplicationEdit');
Route::post('/nature_of_the_application/update', 'AddTradmarkController@NatureApplicationUpdate');

Route::get('/application_field/show', 'AddTradmarkController@FieldApplicationShow');
Route::get('/application_field/create', 'AddTradmarkController@FieldApplicationCreate');
Route::post('/application_field/save', 'AddTradmarkController@FieldApplicationSave');
Route::get('edit/{id}/application_field', 'AddTradmarkController@FieldApplicationEdit');
Route::post('/application_field/update', 'AddTradmarkController@FieldApplicationUpdate');

Route::get('/nature_applicant/show', 'AddTradmarkController@NatureApplicantShow');
Route::get('/nature_applicant/create', 'AddTradmarkController@NatureApplicantCreate');
Route::post('/nature_applicant/save', 'AddTradmarkController@NatureApplicantSave');
Route::get('edit/{id}/nature_applicant', 'AddTradmarkController@NatureApplicantEdit');
Route::post('/nature_applicant/update', 'AddTradmarkController@NatureApplicantUpdate');

Route::get('/nature_agent/show', 'AddTradmarkController@NatureAgentShow');
Route::get('/nature_agent/create', 'AddTradmarkController@NatureAgentCreate');
Route::post('/nature_agent/save', 'AddTradmarkController@NatureAgentSave');
Route::get('edit/{id}/nature_agent', 'AddTradmarkController@NatureAgentEdit');
Route::post('/nature_agent/update/{id}', 'AddTradmarkController@NatureAgentUpdate');

Route::get('/category_mark/show', 'AddTradmarkController@CategoryMarkShow');
Route::get('/category_mark/create', 'AddTradmarkController@CategoryMarkCreate');
Route::post('/category_mark/save', 'AddTradmarkController@CategoryMarkSave');
Route::get('edit/{id}/category_mark', 'AddTradmarkController@CategoryMarkEdit');
Route::post('/category_mark/update', 'AddTradmarkController@CategoryMarkUpdate');

Route::get('/class_good_services/show', 'AddTradmarkController@ClassGoodServicesShow');
Route::get('/class_good_services/create', 'AddTradmarkController@ClassGoodServicesCreate');
Route::post('/class_good_services/save', 'AddTradmarkController@ClassGoodServicesSave');
Route::get('edit/{id}/class_good_services', 'AddTradmarkController@ClassGoodServicesEdit');
Route::post('/class_good_services/update', 'AddTradmarkController@ClassGoodServicesUpdate');

Route::get('/language/show', 'AddTradmarkController@LanguageShow');
Route::get('/language/create', 'AddTradmarkController@LanguageCreate');
Route::post('/language/save', 'AddTradmarkController@LanguageSave');
Route::get('edit/{id}/language', 'AddTradmarkController@LanguageEdit');
Route::post('/language/update', 'AddTradmarkController@LanguageUpdate');

Route::get('/applicant/show', 'AddTradmarkController@ApplicantShow');
Route::get('/applicant/create', 'AddTradmarkController@ApplicantCreate');
Route::post('/applicant/save', 'AddTradmarkController@ApplicantSave');
Route::get('edit/{id}/applicant', 'AddTradmarkController@ApplicantEdit');
Route::post('/applicant/update', 'AddTradmarkController@ApplicantUpdate');

Route::get('/agent/show', 'AddTradmarkController@AgentShow');
Route::get('/agent/create', 'AddTradmarkController@AgentCreate');
Route::post('/agent/save', 'AddTradmarkController@AgentSave');
Route::get('edit/{id}/agent', 'AddTradmarkController@AgentEdit');
Route::post('/agent/update/{id}', 'AddTradmarkController@AgentUpdate');

Route::get('/sub_class_good_services/show', 'AddTradmarkController@SubClassShow');
Route::get('/sub_class_good_services/create', 'AddTradmarkController@SubClassCreate');
Route::post('/sub_class_good_services/save', 'AddTradmarkController@SubClassSave');
Route::get('/edit/{id}/sub_class_good_services', 'AddTradmarkController@SubClassEdit');
Route::post('/sub_class_good_services/update', 'AddTradmarkController@SubClassUpdate');
Route::get('findCityWithStateID/{id}','AddTradmarkController@findCityWithStateID');
Route::get('edit_summary/findCityWithStateID/{id}','AddTradmarkController@editfindCityWithStateID');

Route::get('/login', 'SessionsController@create');
Route::post('/login/submit', 'SessionsController@store')->name('/login/submit');
Route::get('/logout', 'SessionsController@destroy');
Route::get('/add_tradmark/{id}/update_status', 'FrontEndController@status_create');
Route::post('/status/submit', 'AddTradmarkController@insert_status');
Route::get('/status/{id}/edit', 'FrontEndController@status_edit');
// Route::post('status/updated', 'AddTradmarkController@update_insert_status');
Route::get('/tm_reports', 'AddTradmarkController@tm_reports');
Route::get('test', 'FrontEndController@test');
Route::get('/support', 'AddTradmarkController@support');
Route::post('/tm_portfolio', 'AddTradmarkController@search');
Route::post('/tradmark/search', 'AddTradmarkController@search');
Route::post('/tm_reports', 'AddTradmarkController@tm_reports_search');
Route::get('status_table/{modal}', 'AddTradmarkController@dataShow');
Route::get('/action/{id}/license', 'AddTradmarkController@actionitem');
Route::post('action/submit', 'AddTradmarkController@actionitemsave');
Route::get('license/{id}/edit', 'AddTradmarkController@actionitemedit');
Route::post('/license/update', 'AddTradmarkController@actionitemupdate');
// Route::get('tradmark/pdf', 'AddTradmarkController@printPDF');
// Route::get('tradmark/{id}/pdf', [ 'as' => 'tradmark.printpdf', 'uses' => 'AddTradmarkController@printPDF']);
Auth::routes();
// Auth::routes(['login'=> false]);

// Route::get('/home', 'HomeController@index')->name('home');
Route::redirect('register', 'login', 301);
Route::redirect('home', 'dashbord', 301);
Route::get('events/calendar', 'AddTradmarkController@eventsCalendar')->name("events.update");
Route::get('modal-data/piechart/{id}', 'FrontEndController@modaldatapiechart');
Route::get('modal-datas/piechart/{id}', 'FrontEndController@modaldataspiechart');
Route::get('modal-data2/piechart/{id}', 'FrontEndController@modaldata2piechart');
Route::get('modal-data3/piechart/{id}', 'FrontEndController@modaldata3piechart');
Route::get('modal-data4/piechart/{id}', 'FrontEndController@modaldata4piechart');
Route::get('modal-data5/piechart/{id}', 'FrontEndController@modaldata5piechart');
Route::get('modal-data6/piechart/{id}', 'FrontEndController@modaldata6piechart');
Route::get('modal-data7/piechart/{id}', 'FrontEndController@modaldata7piechart');
Route::get('modal-data11/piechart/{id}', 'FrontEndController@modaldata11piechart');
Route::get('modal-data/barchart/{id}', 'FrontEndController@modalbarchart');
Route::get('modalge/{id}','AddBrandController@modalgeodata');
Route::get('modal-datap/piechart/{id}', 'PatentController@modlp');
Route::get('modal-dataps/piechart/{id}', 'PatentController@modlps');
// Route::get('view/all', 'AddTradmarkController@viewall');
// Route::get('tradmark/search', 'AddTradmarkController@tradmark_search');
// Route::get('/show_license', 'AddTradmarkController@show_license');
// Route::get('/edit/{id}/edit_license', 'AddTradmarkController@edit_license');
// Route::post('license/update', 'AddTradmarkController@update_license');
Route::resource('notices','LitigationNoticesController');
Route::resource('/summary','LitigationSummaryController');
Route::get('litigation_master','LitigationMasterController@index');
Route::resource('liti_type','LitiTypeController');
Route::resource('area_exp','AraeExController');
Route::resource('law_firm','LawFirmController');
Route::resource('ex_counsel','ExtCounselController');
Route::resource('stages','StagesController');
Route::resource('legal_not','LegalNoticeController');
Route::resource('Court_list','CourtListController');
Route::resource('advoc','AdvocateController');
Route::resource('currenc','CurrencController');
Route::post('/noticess', 'LitigationNoticesController@search');
Route::post('/summarys', 'LitigationSummaryController@search');
Route::resource('hearing','LitiHearingController');
Route::resource('completion','CompletionController');
Route::get('hearing/create/{sum}','HearingController@create');
Route::post('hearing/submit','HearingController@store');
Route::get('hearing/edit/{id}','HearingController@edit');
Route::post('headering/store/{id}','HearingController@update');
Route::get('liti_doc/create/{id}','LitiDocController@create');
Route::post('liti_doc/submit','LitiDocController@store');
Route::resource('product','ProductController');
Route::get('notsta/create/{id}','NoticeStatController@create');
Route::post('notsta/submit','NoticeStatController@store');
Route::resource('des_mas','DesignMasterController');
Route::resource('trade_mas','TradeMasterController');
Route::resource('sections','DesignClassController');
Route::get('agent_fee/trademark/{id}','AgentFeeController@trade_fee');
Route::post('agent_fee/save','AgentFeeController@trade_save');
Route::resource('/trademark_fees','TradeMarkGovFeesController');
Route::get('/findtype','ChildController@findtype');
Route::get('/findfee','ChildController@findfee');
Route::get('/findagent','ChildController@findagent');
Route::get('/findnat','ChildController@findnat');
Route::get('/finduser','ChildController@finduser');
Route::get('classdes','ChildController@classdes');
Route::resource('/trade_class','TradeClassController');
Route::resource('licenseIn','LicenceInController');
Route::get('trlicense/{id}','TrLicInController@tr_index');
Route::post('trlicense/save','TrLicInController@tr_save');
Route::get('trin/{id}','TrLicInController@tr_edit');
Route::post('trin/save/{id}','TrLicInController@tr_edit_save');
Route::get('cr_lice','LicenceInController@cr_lice');
Route::get('des_lice','LicenceInController@des_lice');
Route::get('pat_lice','LicenceInController@pat_lice');
Route::resource('licenseOut','LicenceOutController');
Route::get('cr_liceo','LicenceOutController@cr_liceo');
Route::get('des_liceo','LicenceOutController@des_liceo');
Route::get('pat_liceo','LicenceOutController@pat_liceo');
Route::get('trlicense/out/{id}','TrLicOutController@tr_index');
Route::post('trlicense/out/save','TrLicOutController@tr_save');
Route::get('trout/{id}','TrLicOutController@tr_edit');
Route::post('trout/save/{id}','TrLicOutController@tr_edit_save');
Route::resource('assignOut','AssignOutController');
Route::get('cr_assout','AssignOutController@cr_assout');
Route::get('des_assout','AssignOutController@des_assout');
Route::get('pat_assout','AssignOutController@pat_assout');
Route::resource('assignIn','AssignInController');
Route::get('cr_assin','AssignInController@cr_assin');
Route::get('des_assin','AssignInController@des_assin');
Route::get('pat_assin','AssignInController@pat_assin');
Route::get('trass/in/{id}','TrAssInController@tr_index');
Route::post('trass/in/save','TrAssInController@tr_save');
Route::get('asin/{id}','TrAssInController@tr_edit');
Route::post('asin/save/{id}','TrAssInController@tr_edit_save');
Route::get('trass/out/{id}','TradAssOutController@tr_index');
Route::post('trass/out/save','TradAssOutController@tr_save');
Route::get('asout/{id}','TradAssOutController@tr_edit');
Route::post('asout/save/{id}','TradAssOutController@tr_edit_save');
Route::resource('leads','LeadController');
Route::get('lead_gen','leadGenController@create');
Route::post('lead_save','leadGenController@save');
Route::resource('country','CountryController');
Route::resource('state','StateController');
Route::get('/findstate','ChildController@findStates');
Route::get('/findcity','ChildController@findcity');
Route::get('/findprod','ChildController@findprod');
Route::get('/trademark/update_status/edit/{id}','FrontEndController@edit_status');
Route::post('edit/status/submit','AddTradmarkController@update_insert_status');
Route::get('images/{image}/delete/{id}','DeleteImageController@tradeimg');
Route::get('imagess/{image}/delete/{id}','DeleteImageController@tradestate');
Route::get('imagesss/{image}/delete/{id}','DeleteImageController@tradeslogo');
Route::get('images/delete/{image}/{id}','DeleteImageController@updateImg');
Route::get('imagesh/delete/{image}/{id}','DeleteImageController@updateImgh');
Route::get('update_status/delete/{id}','DeleteListController@trade_status');
Route::get('imagesc/delete/{image}/{id}','DeleteImageController@copyimg');
Route::get('imagesc/deletec/{image}/{id}','DeleteImageController@copyimgd');
Route::get('imagesc/deleteca/{image}/{id}','DeleteImageController@copyimgda');
Route::get('imaged/delete/{image}/{id}','DeleteImageController@designimgda');
Route::get('imagesd/delete/{image}/{id}','DeleteImageController@designimgdas');
Route::get('imagesds/delete/{image}/{id}','DeleteImageController@designimgdass');
Route::get('imagesdsd/delete/{image}/{id}','DeleteImageController@designimgdassd');
Route::get('imagi/delete/{image}/{id}','DeleteImageController@investimg');
Route::get('design/images/delete/{image}/{id}','DeleteImageController@designimgd');
Route::get('designcor/images/delete/{image}/{id}','DeleteImageController@designimgcor');
Route::get('copy/delete/{image}/{id}','DeleteImageController@cuimg');
Route::get('enf/delete/{image}/{id}','DeleteImageController@enfimg');
Route::get('branddashboard','DashboardController@brand');
Route::get('designdashboard','DashboardController@design');
Route::get('copydashboard','DashboardController@copy');
Route::get('patentdashboard','DashboardController@patent');
Route::get('litidashboard','DashboardController@liti');
Route::get('trad_master','MasterController@trade');
Route::get('common_master','MasterController@common');
Route::get('patent_master','MasterController@patent');
Route::get('brand_master','MasterController@brand');
Route::get('copy_master','MasterController@copy');
Route::get('notice/delete/{image}/{id}','DeleteImageController@noticeimg');
Route::get('/summary/hearing/{id}','HearingController@hearing');
Route::get('/summary/documents/{id}','LitiDocController@index');
Route::get('/summary/fees/{id}','FessController@index');
Route::get('summary/completion/{id}','CompletionController@comple');
Route::get('counsel-fees/{id}','FessController@counsel');
Route::post('counsel_fees/submit','FessController@save');
Route::get('summary/counsel-fee/edit/{id}','FessController@edit');
Route::post('counsel_fees_edit/submit','FessController@edit_save');
Route::get('delete/counsel/{image}/{id}','DeleteImageController@counsel');
Route::get('counsel-paid-fees/{id}','FessController@counsel_paid');
Route::get('counsel/paid_fees/{id}','FessController@counsel_paid_fee');
Route::post('counsel_paid_fees/submit','FessController@counsel_save_fee');
Route::get('counsel-paid-fees/edit/{id}','FessController@counsel_paid_edit');
Route::get('/delete/paid_fess/{image}/{id}','DeleteImageController@paid_fees');
Route::post('counsel_paid_fees_edit/submit/{id}','FessController@counsel_paid_save');
Route::get('advocate-fees/{id}','FessController@advocate');
Route::post('adv_fees/submit','FessController@adv_save');
Route::get('summary/advocate-fee/edit/{id}','FessController@adv_edit');
Route::get('delete/advocate/{image}/{id}','DeleteImageController@advocate');
Route::post('advocate_fees_edit/submit','FessController@adv_edit_save');
Route::get('advocate-paid-fees/{id}','FessController@advocate_paid');
Route::get('advocate/paid_fees/{id}','FessController@advocate_paid_fee');
Route::post('advocate_paid_fees/submit','FessController@advocate_save_fee');
Route::get('advocate-paid-fees/edit/{id}','FessController@advocate_paid_edit');
Route::post('advocate_paid_fees_edit/submit/{id}','FessController@advocate_paid_save');
Route::get('/delete/paid_fess/adv/{image}/{id}','DeleteImageController@adv_paid_fees');
Route::get('delete/liti-doc/{id}','LitiDocController@delete');
Route::get('delete/hearing/{id}','HearingController@delete');
Route::get('delete/expin/{image}/{id}','DeleteImageController@expins');
Route::get('delete/expens/{image}/{id}','DeleteImageController@expens');
Route::get('lead_home','leadGenController@show');
Route::get('leads-assign/{id}','LeadController@assign');
Route::post('assign-to/save','LeadController@save_ass');
Route::get('assign-to/{id}','LeadController@view_ass');
Route::get('forward-investigation/{id}','LeadController@invest_create');
Route::post('investc/save','LeadController@investsave');
Route::get('convert_to/{id}','LitigationNoticesController@convert');
Route::post('save/litigationc/{id}','LitigationNoticesController@save_convert');
Route::get('notices/delete/{id}','LitigationNoticesController@deleten');
Route::get('summary/delete/{id}','LitigationSummaryController@deletes');
Route::resource('/appff','ApplicatIionFieldssController');
Route::get('/fir/{id}/edit','AddBrandController@fir_edit');
Route::post('fir/update/{id}','AddBrandController@update_fir');
Route::get('design/{id}/representation','DesignController@represantion');
Route::post('design/represnt/{id}','DesignController@represantion_save');
Route::get('/design/{id}/registration_certificate','DesignController@registration');
Route::post('design/regist/{id}','DesignController@regis_save');
Route::get('/design/{image}/delete/{id}','DeleteImageController@regis_des');
Route::post('roc_save/{id}','Copyright_roc_Controller@save');
Route::get('reg_cert/edit/{id}','Copyright_roc_Controller@edit');
Route::get('delete/cert/{image}/image/{id}','DeleteImageController@reg_cert');
Route::post('roc_save/edit/{id}','Copyright_roc_Controller@edit_save');
Route::get('notice-status/edit/{id}','NoticeStatController@edit');
Route::get('notice-status/edit/{image}/{id}','DeleteImageController@noticeimgst');
Route::post('notsta/edit/submit/{id}','NoticeStatController@update');
Route::get('delete/litiimg/{image}/{id}','DeleteImageController@litihear');
Route::get('edit-completion/{id}','CompletionController@edit');
Route::resource('/cities','CitiesController');
Route::get('/draft','DraftController@index');
Route::resource('/brand-product','BrandProductController');
Route::get('/copyright-draft','CopyrightDraftController@Draft');
Route::get('delete/poa/{image}/{id}','DeleteImageController@poa');
Route::get('copyright-fee/{id}','CopyAgentFeeController@create');
Route::post('save/agent-fee/{id}','CopyAgentFeeController@store');
Route::get('copyright-fee/edit/{id}','CopyAgentFeeController@edit');
Route::post('update/agent-fee/{id}','CopyAgentFeeController@update');
Route::get('delete/copy-fee/{image}/{id}','DeleteImageController@copy_invoice');
Route::get('delete/copy-fee/stamp/{image}/{id}','DeleteImageController@copy_stamp');
Route::resource('design-draft','DesignDraftController');
Route::get('design-fee/{id}','DesignAgentController@create');
Route::post('design/save/agent-fee/{id}','DesignAgentController@store');
Route::get('design-fee/edit/{id}','DesignAgentController@edit');
Route::post('design/update/agent-fee/{id}','DesignAgentController@update');
Route::get('delete/des-fee/{image}/{id}','DeleteImageController@des_invoice');
Route::get('delete/des-fee/stamp/{image}/{id}','DeleteImageController@des_stamp');
Route::get('delete/msme/{image}/{id}','DeleteImageController@msme');
Route::get('delete/trad/poa/{image}/{id}','DeleteImageController@trad_poa');
Route::get('trade-fee/{id}','TradAgentFeeController@create');
Route::post('trade/save/agent-fee/{id}','TradAgentFeeController@store');
Route::get('trade-fee/edit/{id}','TradAgentFeeController@edit');
Route::post('trade/update/agent-fee/{id}','TradAgentFeeController@update');
Route::get('delete/trade-fee/{image}/{id}','DeleteImageController@trade_invoice');
Route::get('delete/trade-fee/stamp/{image}/{id}','DeleteImageController@trade_stamp');
Route::get('assign-to','LeadController@assign_to');
Route::get('/testing','TestingController@index');
Route::resource('/factsheet','FactSheetController');
Route::get('/edit_factsheet/{id}', 'FactSheetController@edit');
Route::post('/factsheet/update', 'FactSheetController@update');
Route::get('/factsheet/{id}/show', 'FactSheetController@show_factsheet');
Route::post('/factsheet/delete', 'FactSheetController@factSheetDelete');
Route::post('/factsheet/approval_process', 'FactSheetController@factSheetApprovalProcess');
Route::get('/fsa/{id}', 'FactSheetController@factSheetInitiatorAp');
Route::post('/factsheetstatusshared', 'FactSheetController@factSheetStatusShared');
Route::get('/shared_success', 'FactSheetController@factSheetStatusSharedSuccess');

Route::get ( 'email', function () {
	Mail::send ( [ ], [ ], function ($message) {
		$message->to ( 'dineshcmca2009@gmail.com', 'IPR Tool Email Test' )->subject ( 'Subject of the email - Test Mail' )->setBody ( 'Test mail from Local' );
	} );
} );