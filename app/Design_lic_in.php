<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Design_lic_in extends Model
{
    protected $fillable = ['name_of_licensor','name_of_licensee','term_of_license','license_date','expiry_date','docs','comments','licin_id'];
}
