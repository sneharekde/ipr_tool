<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvocateFee extends Model
{
    public function advoc(){
        return $this->belongsTo(Advocate::class);
    }

    public function law()
    {
        return   $this->belongsTo(LawFirm::class);
    }
}
