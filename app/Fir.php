<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fir extends Model
{
    protected $fillable = [
    	'fir_date','fir_no','police','nof','dof','fir_id'
    ];

    public function fir()
    {
        return $this->belongsTo(Enf::class);
    }
}
