<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExternalCounsel extends Model
{
    public function country()
    {
       return $this->belongsTo(country::class);
    }

    public function state(){
       return $this->belongsTo(Stete::class);
    }

    public function law(){
       return $this->belongsTo(LawFirm::class);
    }

    public function expert(){
       return $this->belongsTo(AreaExp::class);
    }

}
