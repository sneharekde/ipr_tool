<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionList extends Model
{
    //
    protected $fillable = ['function'];
}
