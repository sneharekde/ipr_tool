<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expensen extends Model
{
	 protected $fillable = ['invoice_no','amount','upload','pay_stat','enforce_id'];
}
