<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryMark extends Model
{
    //
    protected $fillable = ['category_of_mark'];
}
