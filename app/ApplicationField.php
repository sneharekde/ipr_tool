<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationField extends Model
{
    //
    protected $fillable = ['application_field'];
}
