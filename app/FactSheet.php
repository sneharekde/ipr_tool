<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FactSheet extends Model {
	protected $table = "factsheet_master";
	
	protected $fillable = [ 
			'name_of_initiator',
			'designation',
			'name_of_channel',
			'registration_mark',
			'mark_used_from',
			'class_of_registration',
			'jurisdiction_for_registration',
			'significance_of_work'
	];
}
