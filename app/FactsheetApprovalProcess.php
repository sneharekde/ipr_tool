<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FactsheetApprovalProcess extends Model
{
	protected $table = "factsheet_approval_process";
	
	protected $fillable = [
			'factsheet_id',
			'initiator_id',
			'initiator_agent_name',
			'initiator_agent_email',
			'initiator_optional_id',
			'initiator_optional_agent_name', 
			'initiator_optional_agent_email', 
			'legal_id',
			'legal_agent_name', 
			'legal_agent_email', 
			'coo_id',
			'coo_agent_name', 
			'coo_agent_email', 
			'cfo_id',
			'cfo_agent_name',
			'cfo_agent_email',
			'ceo_id',
			'ceo_agent_name',
			'ceo_agent_email',
			'factsheet_status_steps',
			'factsheet_approval_status',
			'initiator_agent_comment',
			'initiator_optional_agent_comment',
			'legal_agent_comment',
			'coo_agent_comment',
			'cfo_agent_comment',
			'ceo_agent_comment',
			'initiator_optional_approval_status',
			'legal_approval_status',
			'coo_approval_status',
			'cfo_approval_status',
			'ceo_approval_status',
			'initiator_approval_back',
			'initiator_initiated_approval'
	];
}
