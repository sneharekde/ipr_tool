<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentEntity extends Model
{
    //
    protected $fillable = ['parent_entity'];

    public function EntityList()
    {
        return $this->hasMany('App\EntityList');
    }
}
