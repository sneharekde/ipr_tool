<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    public function nat()
    {
        return $this->belongsTo(NatureApplicant::class);
    }
}
