<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designm extends Model
{
    public function classg(){
        return $this->belongsTo(DesignClass::class);
    }

    public function agent(){
        return $this->belongsTo(Agent::class);
    }

    public function natappli(){
        return $this->belongsTo(ApplicationField::class);
    }
    public function natent(){
        return $this->belongsTo(NatureApplicant::class);
    }
    public function ent(){
        return $this->belongsTo(Entity::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function lang(){
        return $this->belongsTo(Language::class);
    }
}
