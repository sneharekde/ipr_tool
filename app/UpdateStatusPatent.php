<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpdateStatusPatent extends Model
{
    protected $fillable = ['patent_id','status','sub_status'];
}
