<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Expr\FuncCall;

class Copyrightn extends Model
{
    public function agent(){
        return $this->belongsTo(Agent::class);
    }

    public function lang(){
        return $this->belongsTo(Language::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
