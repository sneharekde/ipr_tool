<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NatureApplication extends Model
{
    //
    protected $fillable = ['nature_of_application'];
}
