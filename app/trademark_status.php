<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trademark_status extends Model
{
    //
    protected $fillable = ['trademark_status'];
}
