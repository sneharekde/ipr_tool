<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignationList extends Model
{
    //
    protected $fillable = ['designation'];
}
