<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Copyright;
use App\CopyrightLicIn;
use App\User;
use App\Language;
use App\DesignationList;
use Session;
use Storage;
use Auth;
use DB;
use PDF;
use Illuminate\Support\Facades\Input;
use App\Copyrightn;

class Copyright_lic_in_Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function license_create($id)
    {
    	$comp = Copyrightn::where('id', '=',$id)->first();
        $licin = CopyrightLicIn::where('licin_id', '=',$id)->get();
    	return view('copyright.licin', compact('comp','licin'))->with('no',1);
    }

    public function store(Request $request)
    {
        $this->validate($request,array(
            'licensor' => 'required',
            'licensee' => 'required',
            'term' => 'required|numeric',
            'lic_date' => 'required',
            'consideration' => 'required',
        ));
        $tems = $request->term * 12;
        $trad = new CopyrightLicIn;
        $trad->licin_id = $request->licin_id;
        $trad->licensor = $request->licensor;
        $trad->licensee = $request->licensee;
        $trad->term = $request->term;
        $trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
        $trad->exp_date = date('Y-m-d',strtotime($tems.' months',strtotime($request->lic_date)));
        $trad->consideration = $request->consideration;
        if($request->consideration == 'Periodic royalty fee'){
            $trad->rem_ip = $request->rem_ip;
            $trad->ent_date = $request->ent_date;
        }
        if($request->consideration == 'One time royalty fee'){
            $trad->ent_date2 = $request->ent_date2;
        }
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
        $trad->reminder = $request->reminder;
        $trad->comments = $request->comments;
        $trad->save();
        Session::flash('success','License In Successfully Added');
        return redirect('copyright/license_in/'.$request->licin_id);
    }

    public function license_edit($id)
    {

        $licin = CopyrightLicIn::find($id);
    	return view('copyright.licin_edit', compact('licin'));
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,array(
            'licensor' => 'required',
            'licensee' => 'required',
            'term' => 'required|numeric',
            'lic_date' => 'required',
            'consideration' => 'required',
        ));
        $tems = $request->term * 12;
        $trad = CopyrightLicIn::find($id);
        $trad->licin_id = $request->licin_id;
        $trad->licensor = $request->licensor;
        $trad->licensee = $request->licensee;
        $trad->term = $request->term;
        $trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
        $trad->exp_date = date('Y-m-d',strtotime($tems.' months',strtotime($request->lic_date)));
        $trad->consideration = $request->consideration;
        if($request->consideration == 'Periodic royalty fee'){
            $trad->rem_ip = $request->rem_ip;
            $trad->ent_date = $request->ent_date;
        }
        if($request->consideration == 'One time royalty fee'){
            $trad->ent_date2 = $request->ent_date2;
        }
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
        $trad->reminder = $request->reminder;
        $trad->comments = $request->comments;
        $trad->save();
        Session::flash('success','License In Successfully Added');
        return redirect('copyright/license_in/'.$request->licin_id);
    }

}
