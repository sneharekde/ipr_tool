<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Copyright;
use App\CopyAssIn;
use App\Copyrightn;
use App\User;
use App\Language;
use App\DesignationList;
use Session;
use Storage;
use Auth;
use DB;
use PDF;
use Illuminate\Support\Facades\Input;

class Copyright_ass_in_Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function assign_create($id)
    {
    	$comp = Copyrightn::find($id);
    	$licin = CopyAssIn::where('copy_id',$id)->get();
    	return view('copyright.assin',compact('comp','licin'))->with('no',1);
    }

    public function store(Request $request)
    {
        $this->validate($request,array(
            'licensor' => 'required',
            'licensee' => 'required',
            'lic_date' => 'required',
            'consideration' => 'required',
        ));
        $trad = new CopyAssIn;
        $trad->copy_id = $request->copy_id;
        $trad->licensor = $request->licensor;
        $trad->licensee = $request->licensee;
        $trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
        $trad->consideration = $request->consideration;
        $trad->rem_ip = $request->rem_ip;
        $trad->ent_date = $request->ent_date;
        $trad->ent_date2 = $request->ent_date2;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
        $trad->reminder = $request->reminder;
        $trad->comments = $request->comments;
        $trad->save();
        Session::flash('success','Assign In Successfully Added');
        return redirect('copyright/assign_in/'.$request->copy_id);
    }

    public function assign_edit($id)
    {
    	$licin = CopyAssIn::find($id);
    	return view('copyright.edit_assin',compact('licin'));
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,array(
            'licensor' => 'required',
            'licensee' => 'required',
            'lic_date' => 'required',
            'consideration' => 'required',
        ));
        $trad = CopyAssIn::find($id);
        $trad->copy_id = $request->copy_id;
        $trad->licensor = $request->licensor;
        $trad->licensee = $request->licensee;
        $trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
        $trad->consideration = $request->consideration;
        $trad->rem_ip = $request->rem_ip;
        $trad->ent_date = $request->ent_date;
        $trad->ent_date2 = $request->ent_date2;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
        $trad->reminder = $request->reminder;
        $trad->comments = $request->comments;
        $trad->save();
        Session::flash('success','Assign In Successfully Added');
        return redirect('copyright/assign_in/'.$request->copy_id);
    }
}
