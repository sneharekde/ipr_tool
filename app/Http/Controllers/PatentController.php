<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\CategoryApplicant;
use App\UpdateStatusPatent;
use App\Patent;
use Session;
use DB;
use Storage;
use Auth;
use Illuminate\Support\Facades\Input;
class PatentController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

    public function modlp($id)
    {
        $name = str_replace("%20", " ", $id);
        $results = Patent::all();
        return view('patent.modlp',compact('name','results'))->with('no',1);
    }

    public function modlps($id)
    {
        $name = str_replace("%20", " ", $id);
        $results = Patent::all();
        return view('patent.modalps',compact('name','results'))->with('no',1);
    }

    public function index()
    {
        $patents = Patent::orderBy('id', 'DESC')->get();
        $cats = CategoryApplicant::all();
    	return view('patent.index',compact('patents','cats'))->with('no', 1);
    }

    public function create()
    {
        $users = User::all();
        $cats = CategoryApplicant::all();
    	return view('patent.create',compact('users','cats'));
    }

    public function store(Request $request)
    {
        $this->validate($request,array(
            'priority_date' => 'required',
            'filing_date' => 'required',
            'application_no' => 'required',
            'applicant_name' => 'required',
            'applicant_id' => 'required',
            'cat_app' => 'required',
            'invent_name' => 'required',
            'title_inven' => 'required',
            'reg_pat' => 'required',
            'renewal' => 'required',
            'gov' => 'required'
        ));

        $patent = new Patent;
        $patent->priority_date = date('Y-m-d',strtotime($request->priority_date));
        $patent->filing_date = date('Y-m-d',strtotime($request->filing_date));
        $patent->application_no = $request->application_no;
        $patent->applicant_name = $request->applicant_name;
        $patent->applicant_id = $request->applicant_id;
        $patent->cat_app = $request->cat_app;
        if($request->cat_app == 'other'){
            $patent->cat_app_s = $request->cat_app_s;
        }
        $patent->invent_name = $request->invent_name;
        $patent->title_inven = $request->title_inven;
        $patent->reg_pat = $request->reg_pat;
        $patent->pct = $request->pct;
        if($request->pct == 'yes'){
            $patent->int_app_no = $request->int_app_no;
            $patent->int_fili_date = date('Y-m-d',strtotime($request->int_fili_date));
        }
        $patent->renewal = date('Y-m-d',strtotime($request->renewal));
        $patent->gov = $request->gov;
        $patent->user_id = $request->user_id;

        if($request->hasFile('file') && $request->file->isvalid())
             {
                $file = $request->file->getClientOriginalName(); //Get Image Name
                $file_logo = $file;
                $request->file->move(public_path('patent/docs/'),$file_logo);
                $patent->file = $file_logo;
             }

        $patent->save();
        // Update Patent Aaplication status
        $patent_update = new UpdateStatusPatent();
        $patent_update->patent_id = $patent->id;
        $patent_update->date = $patent->filing_date;
        $patent_update->status = 'Active Application';
        $patent_update->save();

        Session::flash('success','Patent Successfully Added');
        return redirect('/patent/show');
    }

    public function show($id)
    {
        $pat = Patent::find($id);
        return view('patent.view',compact('pat'));
    }

    public function edit($id)
    {
        $pat = Patent::find($id);
        $users = User::all();
        $cats = CategoryApplicant::all();
    	return view('patent.edit',compact('users','cats','pat'));
    }

    public function update(Request $request)
    {
        $this->validate($request,array(
           'priority_date' => 'required',
            'filing_date' => 'required',
            'application_no' => 'required',
            'applicant_name' => 'required',
            'applicant_id' => 'required',
            'cat_app' => 'required',
            'invent_name' => 'required',
            'title_inven' => 'required',
            'reg_pat' => 'required',
            'renewal' => 'required',
            'gov' => 'required'
        ));

        $patent = Patent::find($request->id);

        $patent->priority_date = date('Y-m-d',strtotime($request->priority_date));
        $patent->filing_date = date('Y-m-d',strtotime($request->filing_date));
        $patent->application_no = $request->application_no;
        $patent->applicant_name = $request->applicant_name;
        $patent->applicant_id = $request->applicant_id;
        $patent->cat_app = $request->cat_app;
        if ($request->cat_app == 'other') {
            $patent->cat_app_s = $request->cat_app_s;
        } else {
            $patent->cat_app_s = null;
        }
        $patent->invent_name = $request->invent_name;
        $patent->title_inven = $request->title_inven;
        $patent->reg_pat = $request->reg_pat;
        $patent->pct = $request->pct;
        if ($request->pct == 'yes') {
            $patent->int_app_no = $request->int_app_no;
            $patent->int_fili_date = date('Y-m-d',strtotime($request->int_fili_date));
        } else {
            $patent->int_app_no = null;
            $patent->int_fili_date = null;
        }


        $patent->renewal = date('Y-m-d',strtotime($request->renewal));
        $patent->gov = $request->gov;
        $patent->user_id = $request->user_id;
        if($request->hasFile('file') && $request->file->isvalid())
             {
                $file = $request->file->getClientOriginalName(); //Get Image Name
                $file_logo = $file;
                $request->file->move(public_path('patent/docs/'),$file_logo);
                $old = $patent->file;
                $patent->file = $file_logo;
                Storage::delete('public/patent/docs/'.$old);
             }
        $patent->save();
        Session::flash('success','Patenet Edit Successfully');
        return redirect('/patent/show');
    }

    public function search(Request $request)
    {
        $priority_date = $request->priority_date;
        $filing_date = $request->filing_date;
        $application_no = $request->application_no;
        $applicant_name = $request->applicant_name;
        $applicant_id = $request->applicant_id;
        $cat_app = $request->cat_app;
        $cat_app_s = $request->cat_app_s;
        $invent_name = $request->invent_name;
        $title_inven = $request->title_inven;
        $reg_pat = $request->reg_pat;
        $pct = $request->pct;
        $int_app_no = $request->int_app_no;
        $int_fili_date = $request->int_fili_date;
        $renewal = $request->renewal;

        $shows = Patent::orderBy('id','DESC')->get();
        $cats = CategoryApplicant::all();

        $results = DB::table('patents')->where(function($query) use ($priority_date,$filing_date,$application_no,$applicant_name,$applicant_id,$cat_app,$cat_app_s,$invent_name,$title_inven,$reg_pat,$pct,$int_app_no,$int_fili_date,$renewal){
            if ($priority_date) {
                $query->where('priority_date','=',$priority_date);
            }
            if ($filing_date) {
                $query->where('filing_date','=',$filing_date);
            }
            if ($application_no) {
                $query->where('application_no','=',$application_no);
            }
            if ($applicant_name) {
                $query->where('applicant_name','=',$applicant_name);
            }
            if ($applicant_id) {
                $query->where('applicant_id','=',$applicant_id);
            }
            if ($cat_app) {
                $query->where('cat_app','=',$cat_app);
            }
            if ($cat_app_s) {
                $query->where('cat_app_s','=',$cat_app_s);
            }
            if ($invent_name) {
                $query->where('invent_name','=',$invent_name);
            }
            if ($title_inven) {
                $query->where('title_inven','=',$title_inven);
            }
            if ($reg_pat) {
                $query->where('reg_pat','=',$reg_pat);
            }
            if ($pct) {
                $query->where('pct','=',$pct);
            }
            if ($int_app_no) {
                $query->where('int_app_no','=',$int_app_no);
            }
            if ($int_fili_date) {
                $query->where('int_fili_date','=',$int_fili_date);
            }
            if ($renewal) {
                $query->where('renewal','=',$renewal);
            }
        })->get();

        return view('patent.search',compact('shows','results','cats'));
    }

    public function update_s($id)
    {
        $patent = Patent::find($id);
        $ups = UpdateStatusPatent::where('patent_id', $id)->orderBy('id', 'desc')->get();
        return view('patent.update',compact('patent','ups'));
    }

    public function save_status(Request $request)
    {
        $this->validate($request,[
            'status' => 'required',
        ]);

        if($request->hasFile('docs') && $request->docs->isvalid()){
                $file = $request->docs->getClientOriginalName(); //Get Image Name
                $file_logos = $file;
                $request->docs->move(public_path('patent/docs'),$file_logos);
             }
             else{
                $file_logos = "no-image.png";
             }

        $create = UpdateStatusPatent::create([
            'patent_id' => $request->pat_id,
            'docs' =>$file_logos,
            'comment' => $request->comment,
            'date' => date('Y-m-d',strtotime($request->date)),
            'status' => $request->status,
            'sub_status'=> $request->sub_status,
            'stage' => $request->stage
        ]);

        $updated = $update_status = Patent::find($request->pat_id);
        $update_status->status = $request->status;
        $update_status->sub_status = $request->sub_status;
        $update_status->save();

        if ($create) {
            return redirect('patent/update_status/'.$request->pat_id)->with('success','Status Created Successfully');
        }

    }
}
