<?php

namespace App\Http\Controllers;

use App\Advocate;
use App\CounselFee;
use App\LitigationNotice;
use Illuminate\Http\Request;
use App\LitigationSummary;
use Session;
use Storage;
use DB;
use App\LawFirm;
use App\ExternalCounsel;
use App\Currency;
use App\CounselFees;
use App\CounselPaidFee;
use App\AdvocateFee;
use App\AdvocatePaidFee;

class FessController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($id){
        $summ = LitigationSummary::find($id);
        $feescs = CounselFee::where('liti_id',$id)->get();
        $feesca = AdvocateFee::where('liti_id',$id)->get();
        return view('litifees.index',compact('summ','feescs','feesca'))->with('no',1);
    }

    public function counsel($id){
        $summ = LitigationSummary::find($id);
        $laws = LawFirm::all();
        $exts = ExternalCounsel::all();
        $currs = Currency::all();
        $ar1 = array();
        $ar2 = array();
        $cur = array();
        foreach ($laws as $law) {
            $ar1[$law->id] = $law->name;
        }
        foreach ($exts as $ext) {
            $ar2[$ext->id] = $ext->counsel;
        }
        foreach ($currs as $curr) {
            $cur[$curr->code] = $curr->code;
        }
        return view('litifees.counsel',compact('summ','ar1','ar2','cur'));
    }

    public function save(Request $request)
    {
        $this->validate($request,array(
            'law_id' => 'required',
            'counsel_id' => 'required',
            'fees_type' => 'required',
            'date' => 'required',
            'fees' => 'required',
            'currency' => 'required',
            'conv_curr' => 'required',
            'conv_amt' => 'required'
        ));

        $post = new CounselFee;
        $post->liti_id = $request->liti_id;
        $post->law_id = $request->law_id;
        $post->counsel_id = $request->counsel_id;
        $post->fees_type = $request->fees_type;
        if($request->fees_type == 'appearance'){
            $post->effect = $request->effect;
        }
        $post->comment = $request->comment;
        if ($request->hasFile('doc'))
        {
            $file= $request->file('doc');
            $destinationPath= 'litigation/fess';
            $fullname = $file->getClientOriginalName();
            $upload_success = $file->move(public_path($destinationPath), $fullname);
            $post->docs = $fullname;
        }
        $post->date =   date('Y-m-d',strtotime($request->date));
        $post->fees = $request->fees;
        $post->currency = $request->currency;
        $post->conv_curr = $request->conv_curr;
        $post->conv_amt = $request->conv_amt;
        $post->save();
        Session::flash('success','Counsel Fees Added Successfully!');
        return redirect('/summary/fees/'.$request->liti_id);
    }

    public function edit($id){
        $post = CounselFee::find($id);
        $laws = LawFirm::all();
        $exts = ExternalCounsel::all();
        $currs = Currency::all();
        $ar1 = array();
        $ar2 = array();
        $cur = array();
        foreach ($laws as $law) {
            $ar1[$law->id] = $law->name;
        }
        foreach ($exts as $ext) {
            $ar2[$ext->id] = $ext->counsel;
        }
        foreach ($currs as $curr) {
            $cur[$curr->code] = $curr->code;
        }
        return view('litifees.editc',compact('post','ar1','ar2','cur'));
    }

    public function edit_save(Request $request)
    {
        $this->validate($request,array(
            'law_id' => 'required',
            'counsel_id' => 'required',
            'fees_type' => 'required',
            'date' => 'required',
            'fees' => 'required',
            'currency' => 'required',
            'conv_curr' => 'required',
            'conv_amt' => 'required'
        ));

        $post = CounselFee::find($request->post_id);
        $post->law_id = $request->law_id;
        $post->counsel_id = $request->counsel_id;
        $post->fees_type = $request->fees_type;
        if($request->fees_type == 'appearance'){
            $post->effect = $request->effect;
        }
        $post->comment = $request->comment;
        if ($request->hasFile('doc'))
        {
            $file= $request->file('doc');
            $destinationPath= 'litigation/fess';
            $fullname = $file->getClientOriginalName();
            $upload_success = $file->move(public_path($destinationPath), $fullname);
            $post->docs = $fullname;
        }
        $post->date =   date('Y-m-d',strtotime($request->date));
        $post->fees = $request->fees;
        $post->currency = $request->currency;
        $post->conv_curr = $request->conv_curr;
        $post->conv_amt = $request->conv_amt;
        $post->save();
        Session::flash('success','Counsel Fees Updated Successfully!');
        return redirect('/summary/fees/'.$post->liti_id);
    }

    public function counsel_paid($id)
    {
        $post = CounselFee::find($id);
        $fees = CounselPaidFee::where('counself_id',$id)->get();
        return view('litifees.paid_counsel',compact('post','fees'))->with('no',1);
    }

    public function counsel_paid_fee($id){
        $post = CounselFee::find($id);
        $currs = Currency::all();
        $cur = array();
        foreach ($currs as $curr) {
            $cur[$curr->code] = $curr->code;
        }
        return view('litifees.counsel_paid_fee',compact('post','cur'));
    }

    public function counsel_save_fee(Request $request)
    {
        $this->validate($request,array(
            'inv_no' => 'required',
            'inv_amt' => 'required',
            'inv_curr' => 'required',
            'inv_date' => 'required',
            'date' => 'required',
            'paid_fees' => 'required',
            'currency' => 'required',
            'conv_curr' => 'required',
            'conv_amt' => 'required'
        ));
        $post = new CounselPaidFee;
        $post->counself_id = $request->liti_id;
        $post->inv_no = $request->inv_no;
        $post->inv_amt = $request->inv_amt;
        $post->inv_curr = $request->inv_curr;
        $post->inv_date = date('Y-m-d',strtotime($request->inv_date));
        $post->comment = $request->comment;
        if ($request->hasFile('doc'))
        {
            $file= $request->file('doc');
            $destinationPath= 'litigation/fess';
            $fullname = $file->getClientOriginalName();
            $upload_success = $file->move(public_path($destinationPath), $fullname);
            $post->docs = $fullname;
        }
        $post->date = date('Y-m-d',strtotime($request->date));
        $post->paid_fees = $request->paid_fees;
        $post->currency = $request->currency;
        $post->conv_curr = $request->conv_curr;
        $post->conv_amt = $request->conv_amt;
        $post->save();
        Session::flash('success','Fees Paid Successfully');
        return redirect('counsel-paid-fees/'.$request->liti_id);
    }

    public function counsel_paid_edit($id)
    {
        $post = CounselPaidFee::find($id);
        $currs = Currency::all();
        $cur = array();
        foreach ($currs as $curr) {
            $cur[$curr->code] = $curr->code;
        }
        return view('litifees.edit_counsel_fees',compact('post','cur'));
    }

    public function counsel_paid_save(Request $request,$id)
    {
        $this->validate($request,array(
            'inv_no' => 'required',
            'inv_amt' => 'required',
            'inv_curr' => 'required',
            'inv_date' => 'required',
            'date' => 'required',
            'paid_fees' => 'required',
            'currency' => 'required',
            'conv_curr' => 'required',
            'conv_amt' => 'required'
        ));
        $post = CounselPaidFee::find($id);
        $post->inv_no = $request->inv_no;
        $post->inv_amt = $request->inv_amt;
        $post->inv_curr = $request->inv_curr;
        $post->inv_date = date('Y-m-d',strtotime($request->inv_date));
        $post->comment = $request->comment;
        if ($request->hasFile('doc'))
        {
            $file= $request->file('doc');
            $destinationPath= 'litigation/fess';
            $fullname = $file->getClientOriginalName();
            $upload_success = $file->move(public_path($destinationPath), $fullname);
            $post->docs = $fullname;
        }
        $post->date = date('Y-m-d',strtotime($request->date));
        $post->paid_fees = $request->paid_fees;
        $post->currency = $request->currency;
        $post->conv_curr = $request->conv_curr;
        $post->conv_amt = $request->conv_amt;
        $post->save();
        Session::flash('success','Fees Paid Successfully');
        return redirect('counsel-paid-fees/'.$post->counself_id);
    }

    public function advocate($id){
        $summ = LitigationSummary::find($id);
        $laws = LawFirm::all();
        $exts = Advocate::all();
        $currs = Currency::all();
        $ar1 = array();
        $ar2 = array();
        $cur = array();
        foreach ($laws as $law) {
            $ar1[$law->id] = $law->name;
        }
        foreach ($exts as $ext) {
            $ar2[$ext->id] = $ext->advocate;
        }
        foreach ($currs as $curr) {
            $cur[$curr->code] = $curr->code;
        }
        return view('litifees.advocates',compact('summ','ar1','ar2','cur'));
    }

    public function adv_save(Request $request)
    {
        $this->validate($request,array(
            'law_id' => 'required',
            'advoc_id' => 'required',
            'fees_type' => 'required',
            'date' => 'required',
            'fees' => 'required',
            'currency' => 'required',
            'conv_curr' => 'required',
            'conv_amt' => 'required'
        ));

        $post = new AdvocateFee;
        $post->liti_id = $request->liti_id;
        $post->law_id = $request->law_id;
        $post->advoc_id = $request->advoc_id;
        $post->fees_type = $request->fees_type;
        if($request->fees_type == 'appearance'){
            $post->effect = $request->effect;
        }
        $post->comment = $request->comment;
        if ($request->hasFile('doc'))
        {
            $file= $request->file('doc');
            $destinationPath= 'litigation/fess';
            $fullname = $file->getClientOriginalName();
            $upload_success = $file->move(public_path($destinationPath), $fullname);
            $post->docs = $fullname;
        }
        $post->date =   date('Y-m-d',strtotime($request->date));
        $post->fees = $request->fees;
        $post->currency = $request->currency;
        $post->conv_curr = $request->conv_curr;
        $post->conv_amt = $request->conv_amt;
        $post->save();
        Session::flash('success','Advocate Fees Added Successfully!');
        return redirect('/summary/fees/'.$request->liti_id);
    }

    public function adv_edit($id){
        $post = AdvocateFee::find($id);
        $laws = LawFirm::all();
        $exts = Advocate::all();
        $currs = Currency::all();
        $ar1 = array();
        $ar2 = array();
        $cur = array();
        foreach ($laws as $law) {
            $ar1[$law->id] = $law->name;
        }
        foreach ($exts as $ext) {
            $ar2[$ext->id] = $ext->advocate;
        }
        foreach ($currs as $curr) {
            $cur[$curr->code] = $curr->code;
        }
        return view('litifees.edita',compact('post','ar1','ar2','cur'));
    }
    public function adv_edit_save(Request $request)
    {
        $this->validate($request,array(
            'law_id' => 'required',
            'advoc_id' => 'required',
            'fees_type' => 'required',
            'date' => 'required',
            'fees' => 'required',
            'currency' => 'required',
            'conv_curr' => 'required',
            'conv_amt' => 'required'
        ));

        $post = AdvocateFee::find($request->post_id);
        $post->law_id = $request->law_id;
        $post->advoc_id = $request->advoc_id;
        $post->fees_type = $request->fees_type;
        if($request->fees_type == 'appearance'){
            $post->effect = $request->effect;
        }
        $post->comment = $request->comment;
        if ($request->hasFile('doc'))
        {
            $file= $request->file('doc');
            $destinationPath= 'litigation/fess';
            $fullname = $file->getClientOriginalName();
            $upload_success = $file->move(public_path($destinationPath), $fullname);
            $post->docs = $fullname;
        }
        $post->date =   date('Y-m-d',strtotime($request->date));
        $post->fees = $request->fees;
        $post->currency = $request->currency;
        $post->conv_curr = $request->conv_curr;
        $post->conv_amt = $request->conv_amt;
        $post->save();
        Session::flash('success','Advocate Fees Updated Successfully!');
        return redirect('/summary/fees/'.$post->liti_id);
    }

    public function advocate_paid($id)
    {
        $post = AdvocateFee::find($id);
        $fees = AdvocatePaidFee::where('advf_id',$id)->get();
        return view('litifees.paid_advocate',compact('post','fees'))->with('no',1);
    }

    public function advocate_paid_fee($id){
        $post = AdvocateFee::find($id);
        $currs = Currency::all();
        $cur = array();
        foreach ($currs as $curr) {
            $cur[$curr->code] = $curr->code;
        }
        return view('litifees.advocate_paid_fee',compact('post','cur'));
    }

    public function advocate_save_fee(Request $request)
    {
        $this->validate($request,array(
            'inv_no' => 'required',
            'inv_amt' => 'required',
            'inv_curr' => 'required',
            'inv_date' => 'required',
            'date' => 'required',
            'paid_fees' => 'required',
            'currency' => 'required',
            'conv_curr' => 'required',
            'conv_amt' => 'required'
        ));
        $post = new AdvocatePaidFee;
        $post->advf_id = $request->liti_id;
        $post->inv_no = $request->inv_no;
        $post->inv_amt = $request->inv_amt;
        $post->inv_curr = $request->inv_curr;
        $post->inv_date = date('Y-m-d',strtotime($request->inv_date));
        $post->comment = $request->comment;
        if ($request->hasFile('doc'))
        {
            $file= $request->file('doc');
            $destinationPath= 'litigation/fess';
            $fullname = $file->getClientOriginalName();
            $upload_success = $file->move(public_path($destinationPath), $fullname);
            $post->docs = $fullname;
        }
        $post->date = date('Y-m-d',strtotime($request->date));
        $post->paid_fees = $request->paid_fees;
        $post->currency = $request->currency;
        $post->conv_curr = $request->conv_curr;
        $post->conv_amt = $request->conv_amt;
        $post->save();
        Session::flash('success','Fees Paid Successfully');
        return redirect('advocate-paid-fees/'.$request->liti_id);
    }
    public function advocate_paid_edit($id)
    {
        $post = AdvocatePaidFee::find($id);
        $currs = Currency::all();
        $cur = array();
        foreach ($currs as $curr) {
            $cur[$curr->code] = $curr->code;
        }
        return view('litifees.edit_advocate_fees',compact('post','cur'));
    }

    public function advocate_paid_save(Request $request,$id)
    {
        $this->validate($request,array(
            'inv_no' => 'required',
            'inv_amt' => 'required',
            'inv_curr' => 'required',
            'inv_date' => 'required',
            'date' => 'required',
            'paid_fees' => 'required',
            'currency' => 'required',
            'conv_curr' => 'required',
            'conv_amt' => 'required'
        ));
        $post = AdvocatePaidFee::find($id);
        $post->inv_no = $request->inv_no;
        $post->inv_amt = $request->inv_amt;
        $post->inv_curr = $request->inv_curr;
        $post->inv_date = date('Y-m-d',strtotime($request->inv_date));
        $post->comment = $request->comment;
        if ($request->hasFile('doc'))
        {
            $file= $request->file('doc');
            $destinationPath= 'litigation/fess';
            $fullname = $file->getClientOriginalName();
            $upload_success = $file->move(public_path($destinationPath), $fullname);
            $post->docs = $fullname;
        }
        $post->date = date('Y-m-d',strtotime($request->date));
        $post->paid_fees = $request->paid_fees;
        $post->currency = $request->currency;
        $post->conv_curr = $request->conv_curr;
        $post->conv_amt = $request->conv_amt;
        $post->save();
        Session::flash('success','Fees Paid Successfully');
        return redirect('advocate-paid-fees/'.$post->advf_id);
    }
}
