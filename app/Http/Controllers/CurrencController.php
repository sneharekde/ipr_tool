<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;
use Session;

class CurrencController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $litis = Currency::all();
        return view('curr.index',compact('litis'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('curr.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'name' => 'required',
            'code' => 'required'
        ));

        $liti = new Currency;
        $liti->code = $request->code;
        $liti->name = $request->name;

        $liti->save();
        Session::flash('success','Currency Added');
        return redirect('currenc');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $liti = Currency::find($id);
        return view('curr.edit',compact('liti'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'name' => 'required',
            'code' => 'required'
        ));

        $liti = Currency::find($id);
        $liti->code = $request->code;
        $liti->name = $request->name;

        $liti->save();
        Session::flash('success','Currency Updated');
        return redirect('currenc');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
