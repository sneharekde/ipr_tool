<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddTradmark;
use App\TradAssIn;
use App\Trademarks;
use Session;
use DB;
use Auth;
class TrAssInController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function tr_index($id)
    {
    	$comp = Trademarks::where('id', '=',$id)->first();
    	$licin = TradAssIn::where('trad_id', '=',$id)->get();
    	return view('assin.tr_index',compact('comp','licin'))->with('no',1);
    }

    public function tr_save(Request $request)
    {
    	$this->validate($request,array(
    		'licensor' => 'required',
    		'licensee' => 'required',
    		'lic_date' => 'required',
    		'consideration' => 'required',
    	));
    	$trad = new TradAssIn;
    	$trad->trad_id = $request->trad_id;
    	$trad->licensor = $request->licensor;
        $trad->licensee = $request->licensee;
    	$trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
    	$trad->consideration = $request->consideration;
    	$trad->rem_ip = $request->rem_ip;
    	$trad->ent_date = $request->ent_date;
    	$trad->ent_date2 = $request->ent_date2;
    	if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
    	$trad->reminder = $request->reminder;
    	$trad->comments = $request->comments;
    	$trad->save();
    	Session::flash('success','Assign In Successfully Added');
    	return redirect('trass/in/'.$request->trad_id);
    }
    public function tr_edit($id)
    {
    	$licin = TradAssIn::find($id);
    	return view('assin.tr_edit',compact('licin'))->with('no',1);
    }
    public function tr_edit_save(Request $request,$id)
    {
    	$this->validate($request,array(
    		'licensor' => 'required',
    		'licensee' => 'required',
    		'lic_date' => 'required',
    		'consideration' => 'required',
    	));
    	$trad = TradAssIn::find($id);
    	$trad->trad_id = $request->trad_id;
    	$trad->licensor = $request->licensor;
    	$trad->licensee = $request->licensee;
    	$trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
    	$trad->consideration = $request->consideration;
    	$trad->rem_ip = $request->rem_ip;
    	$trad->ent_date = $request->ent_date;
    	$trad->ent_date2 = $request->ent_date2;
    	if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
    	$trad->reminder = $request->reminder;
    	$trad->comments = $request->comments;
    	$trad->save();
    	Session::flash('success','Assign In Successfully Added');
    	return redirect('trass/in/'.$request->trad_id);
    }
}
