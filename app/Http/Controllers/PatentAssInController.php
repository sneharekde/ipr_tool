<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patent;
use App\PatAssIn;
use Session;
use Storage;
use Auth;
use DB;
class PatentAssInController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($id)
    {
    	$comp = Patent::find($id);
    	$licin = PatAssIn::all();
    	return view('patent.assin',compact('comp','licin'))->with('no',1);
    }

    public function store(Request $request)
    {
    	$this->validate($request,array(
            'licensor' => 'required',
            'licensee' => 'required',
            'lic_date' => 'required',
            'consideration' => 'required',
        ));
        $trad = new PatAssIn;
        $trad->pat_id = $request->pat_id;
        $trad->licensor = $request->licensor;
        $trad->licensee = $request->licensee;
        $trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
        $trad->consideration = $request->consideration;
        $trad->rem_ip = $request->rem_ip;
        $trad->ent_date = $request->ent_date;
        $trad->ent_date2 = $request->ent_date2;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
        $trad->reminder = $request->reminder;
        $trad->comments = $request->comments;
        $trad->save();
        Session::flash('success','Assign In Successfully Added');
        return redirect('patent/assign_in/'.$request->pat_id);
    }

    public function edit($id)
    {
    	$licin = PatAssIn::find($id);
    	return view('patent.assin_edit',compact('licin'));
    }

    public function update(Request $request,$id)
    {
    	$this->validate($request,array(
            'licensor' => 'required',
            'licensee' => 'required',
            'lic_date' => 'required',
            'consideration' => 'required',
        ));
        $trad = PatAssIn::find($id);
        $trad->pat_id = $request->pat_id;
        $trad->licensor = $request->licensor;
        $trad->licensee = $request->licensee;
        $trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
        $trad->consideration = $request->consideration;
        $trad->rem_ip = $request->rem_ip;
        $trad->ent_date = $request->ent_date;
        $trad->ent_date2 = $request->ent_date2;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
        $trad->reminder = $request->reminder;
        $trad->comments = $request->comments;
        $trad->save();
        Session::flash('success','Assign In Successfully Added');
        return redirect('patent/assign_in/'.$request->pat_id);
    }
}
