<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agency;
use Session;
use Auth;
use DB;
class AgencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agency = Agency::all();
        return view('agency.index',compact('agency'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('agency.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'name' => 'required',
            'contp' => 'required',
            'contn' => 'required|numeric',
            'email' =>'required'
        ));

        $agency = new Agency;
        $agency->name = $request->name;
        $agency->email = $request->email;
        $agency->contact = $request->contn;
        $agency->person = $request->contp;

        $agency->save();
        Session::flash('success','Agency Added Successfully');
        return  redirect()->route('agency.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agency = Agency::find($id);
        return view('agency.edit',compact('agency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agency = Agency::find($id);
        $this->validate($request,array(
            'name' => 'required',
            'person' => 'required',
            'contact' => 'required|numeric',
            'email' =>'required'
        ));

        $agency = Agency::find($id);
        $agency->name = $request->input('name');
        $agency->email = $request->input('email');
        $agency->contact = $request->input('contact');
        $agency->person = $request->input('person');


        $agency->save();

        Session::flash('success','Agency Updated Successfully');
        return redirect()->route('agency.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
