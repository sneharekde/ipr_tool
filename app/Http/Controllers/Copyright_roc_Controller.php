<?php

namespace App\Http\Controllers;

use App\Copyrightn;
use Illuminate\Http\Request;
use Session;
use Storage;
use Auth;
class Copyright_roc_Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function roc_create($id)
    {
        $post = Copyrightn::find($id);
        return view('copyright.roc',compact('post'))->with('no',1);
    }

    public function save(Request $request,$id)
    {
        $this->validate($request,array(
            'roc' => 'required|unique:copyrightns,roc'
        ));

        $post = Copyrightn::find($id);
        $post->roc = $request->roc;

        $image = $request->reg_cert->getClientOriginalName(); //Get Image Name
        $image_scan_copy = $image;
        $request->reg_cert->move(public_path('copyright/img'),$image_scan_copy);

        $post->reg_cert = $image_scan_copy;
        $post->save();
        Session::flash('success','Certificate of Registration added Succesfully!');
        return redirect()->route('copyrights.show',$id);
    }

    public function edit($id)
    {
        $post = Copyrightn::find($id);
        return view('copyright.edit_roc',compact('post'));
    }

    public function edit_save(Request $request,$id)
    {
        $this->validate($request,array(
            'roc' => 'required|unique:copyrightns,roc,'.$id,
        ));

        $post = Copyrightn::find($id);
        $post->roc = $request->roc;

        $image = $request->reg_cert->getClientOriginalName(); //Get Image Name
        $image_scan_copy = $image;
        $request->reg_cert->move(public_path('copyright/img'),$image_scan_copy);

        $post->reg_cert = $image_scan_copy;
        $post->save();
        Session::flash('success','Certificate of Registration updates Succesfully!');
        return redirect()->route('copyrights.show',$id);
    }
}
