<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Copyright;
use App\Copyrightn;
use App\Update_status_Copyright;
use App\UpdateCopyrightStatus;
use App\User;
use App\Language;
use App\DesignationList;
use Session;
use Storage;
use Auth;
use DB;
use PDF;

class update_status_copyrightController extends Controller
{
 	public function status_create($id)
 	{
 		$copyright = Copyrightn::find($id);
        $statuss = UpdateCopyrightStatus::where('copy_id', $id)->orderBy('id', 'desc')->get();
 		return view('copyright.update',compact('copyright','statuss'))->with('no',1);
 	}

 	public function submit(Request $request)
 	{
        $this->validate($request,array(
            'status' => 'required'
        ));

        $uppdate_cop = new UpdateCopyrightStatus;
        $uppdate_cop->status = $request->status;
        $uppdate_cop->copy_id = $request->copy_id;

        // Query for Status
        if ($request->status == 'Abandoned') {
            $uppdate_cop->date = date('Y-m-d',strtotime($request->date1));
            if ($request->hasFile('docs1'))
            {
                $file= $request->file('docs1');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            $uppdate_cop->comment = $request->comment1;
            // Query for previous status change
            $sent_status_1 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Mandatory waiting period',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Opposition',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Hearing',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query for previous status change
            $sent_status_4 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Appeal',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
            // Query for previous status change
            $sent_status_5 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Discrepancy stage',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_5) {
                $sent_status_5->sent_status = '0';
                $sent_status_5->save();
            }
        }

        if ($request->status == 'Rejected') {
            $uppdate_cop->date = date('Y-m-d',strtotime($request->date2));
            if ($request->hasFile('docs2'))
            {
                $file= $request->file('docs2');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            $uppdate_cop->comment = $request->comment2;
            // Query for previous status change
            $sent_status_1 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Mandatory waiting period',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Opposition',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Hearing',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query for previous status change
            $sent_status_4 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Appeal',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
            // Query for previous status change
            $sent_status_5 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Discrepancy stage',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_5) {
                $sent_status_5->sent_status = '0';
                $sent_status_5->save();
            }
        }

        if ($request->status == 'Registered') {
            $uppdate_cop->dor = date('Y-m-d',strtotime($request->dor));
            if ($request->hasFile('cor'))
            {
                $file= $request->file('cor');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            $uppdate_cop->comment = $request->comment3;
            if($request->reg_remider !=''){
                $uppdate_cop->reg_rem = date('Y-m-d',strtotime($request->reg_remider));
            }else{
                $uppdate_cop->reg_rem = null;
            }
            // find Application Date
            $uppdate_cop->dren = date('Y-m-d',strtotime($request->dor. ' +21900 days'));
            // Query for previous status change
            $sent_status_1 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Mandatory waiting period',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Opposition',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Hearing',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query for previous status change
            $sent_status_4 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Appeal',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
            // Query for previous status change
            $sent_status_5 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Discrepancy stage',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_5) {
                $sent_status_5->sent_status = '0';
                $sent_status_5->save();
            }
        }

        if ($request->status == 'Expired') {

            $uppdate_cop->exp_date = date('Y-m-d',strtotime($request->exp_date_exp));
            if ($request->hasFile('docs_exp'))
            {
                $file= $request->file('docs_exp');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            $uppdate_cop->comment = $request->comment_exp;

            // Query for previous status change
            $sent_status_1 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Mandatory waiting period',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Opposition',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Hearing',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query for previous status change
            $sent_status_4 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Appeal',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
            // Query for previous status change
            $sent_status_5 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Discrepancy stage',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_5) {
                $sent_status_5->sent_status = '0';
                $sent_status_5->save();
            }
            // Query for previous status change
            $sent_status_6 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'status' => 'Registered',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_6) {
                $sent_status_6->sent_status = '0';
                $sent_status_6->save();
            }
        }

        if ($request->status == 'Renewed') {
            $uppdate_cop->ren_date = date('Y-m-d',strtotime($request->date3));
            $uppdate_cop->exp_date = date('Y-m-d',strtotime($request->doexp));
            // Query for previous status change
            $sent_status_1 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Mandatory waiting period',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Opposition',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Hearing',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query for previous status change
            $sent_status_4 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Appeal',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
            // Query for previous status change
            $sent_status_5 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Discrepancy stage',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_5) {
                $sent_status_5->sent_status = '0';
                $sent_status_5->save();
            }
            // Query for previous status change
            $sent_status_6 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'status' => 'Registered',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_6) {
                $sent_status_6->sent_status = '0';
                $sent_status_6->save();
            }
        }

        if ($request->status == 'Extension expired') {
            // Query for previous status change
            $sent_status_1 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Mandatory waiting period',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Opposition',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Hearing',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query for previous status change
            $sent_status_4 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Appeal',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
            // Query for previous status change
            $sent_status_5 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Discrepancy stage',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_5) {
                $sent_status_5->sent_status = '0';
                $sent_status_5->save();
            }
            // Query for previous status change
            $sent_status_6 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'status' => 'Registered',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_6) {
                $sent_status_6->sent_status = '0';
                $sent_status_6->save();
            }
        }

        // Query for Sub Status
        if ($request->status == 'Active Application' && $request->sub_status == 'Mandatory waiting period')
        {
            $uppdate_cop->sub_status = $request->sub_status;
            // date of issues of diary no
            $dated = Copyrightn::find($request->copy_id);
            $uppdate_cop->mwp_date = date('Y-m-d',strtotime($dated->dof. ' +30days'));
            $uppdate_cop->rem_mwp_date = date('Y-m-d',strtotime($request->rem));
            $uppdate_cop->sent_status = '1';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Opposition')
        {
            $uppdate_cop->sub_status = $request->sub_status;
            $uppdate_cop->date = date('Y-m-d',strtotime($request->date4));
            $uppdate_cop->comment = $request->comment4;
            if ($request->hasFile('docs3'))
            {
                $file= $request->file('docs3');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            $uppdate_cop->rep_dead = date('Y-m-d',strtotime($request->date. ' +30 days'));
            $uppdate_cop->rem1 = date('Y-m-d',strtotime($request->rem11));
            $uppdate_cop->rem2 = date('Y-m-d',strtotime($request->rem21));
            $uppdate_cop->rem3 = date('Y-m-d',strtotime($request->rem31));
            $uppdate_cop->sent_status = '1';
            // Query for previous status change
            $sent_status_1 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Mandatory waiting period',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Reply to opposition submitted')
        {
            $uppdate_cop->sub_status = $request->sub_status;
            $uppdate_cop->date = date('Y-m-d',strtotime($request->date5));
            $uppdate_cop->comment = $request->comment5;
            if ($request->hasFile('docs4'))
            {
                $file= $request->file('docs4');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            // Query for previous status change
            $sent_status_1 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Mandatory waiting period',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Opposition',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Hearing')
        {
            $uppdate_cop->sub_status = $request->sub_status;
            $uppdate_cop->dfh = date('Y-m-d',strtotime($request->date6));
            $uppdate_cop->comment = $request->comment6;
            if ($request->hasFile('docs5'))
            {
                $file= $request->file('docs5');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            $uppdate_cop->rem1 = date('Y-m-d',strtotime($request->rem12));
            $uppdate_cop->rem2 = date('Y-m-d',strtotime($request->rem22));
            $uppdate_cop->rem3 = date('Y-m-d',strtotime($request->rem32));
            $uppdate_cop->sent_status = '1';
            // Query for previous status change
            $sent_status_1 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Mandatory waiting period',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Opposition',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Appeal')
        {
            $uppdate_cop->sub_status = $request->sub_status;
            $uppdate_cop->ddj = date('Y-m-d',strtotime($request->date7));
            $uppdate_cop->deadline = date('Y-m-d',strtotime($request->date7. ' +90 days'));

            $uppdate_cop->rem1 = date('Y-m-d',strtotime($request->rem13));
            $uppdate_cop->rem2 = date('Y-m-d',strtotime($request->rem23));
            $uppdate_cop->rem3 = date('Y-m-d',strtotime($request->rem33));
            $uppdate_cop->sent_status = '1';
            // Query for previous status change
            $sent_status_1 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Mandatory waiting period',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Opposition',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Hearing',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Scrutinization stage')
        {
            $uppdate_cop->sub_status = $request->sub_status;
            $uppdate_cop->d_rer = date('Y-m-d',strtotime($request->date8));
            $uppdate_cop->comment = $request->comment7;
            if ($request->hasFile('docs6'))
            {
                $file= $request->file('docs6');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }

            // Query for previous status change
            $sent_status_1 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Mandatory waiting period',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Opposition',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Hearing',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query for previous status change
            $sent_status_4 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Appeal',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Discrepancy stage')
        {
            $uppdate_cop->sub_status = $request->sub_status;
            $uppdate_cop->d_rer = date('Y-m-d',strtotime($request->date9));
            $uppdate_cop->comment = $request->comment8;
            if ($request->hasFile('docs7'))
            {
                $file= $request->file('docs7');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            $uppdate_cop->deadline = date('Y-m-d',strtotime($request->date9. '+30 days'));
            $uppdate_cop->rem1 = date('Y-m-d',strtotime($request->rem14));
            $uppdate_cop->rem2 = date('Y-m-d',strtotime($request->rem24));
            $uppdate_cop->rem3 = date('Y-m-d',strtotime($request->rem34));
            // Query for previous status change
            $sent_status_1 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Mandatory waiting period',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Opposition',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Hearing',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query for previous status change
            $sent_status_4 = UpdateCopyrightStatus::where([
                'copy_id' => $request->copy_id,
                'sub_status' => 'Appeal',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
        }

        // update on Design Table
        $update_stat = Copyrightn::find($request->copy_id);
        // Update Status and Priority status if only Status is upload
        if ($request->status == 'Abandoned') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '2';
            $update_stat->ssp = '';
        }
        if ($request->status == 'Rejected') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '3';
            $update_stat->ssp = '';
        }
        if ($request->status == 'Registered') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '4';
            $update_stat->ssp = '';
            $update_stat->roc = $request->roc;
            $update_stat->dor = date('Y-m-d',strtotime($request->dor));

            if ($request->hasFile('cor'))
            {
                $file= $request->file('cor');
                $fullname = $file->getClientOriginalName();
                $update_stat->reg_cert = $fullname;
            }
        }
        if ($request->status == 'Expired') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '5';
            $update_stat->ssp = '';
        }
        if ($request->status == 'Renewed') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '6';
            $update_stat->ssp = '';
        }
        if ($request->status == 'Extension expired') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '7';
            $update_stat->ssp = '';
        }


        // Update status,sub status,and Priototy
        if ($request->status == 'Active Application' && $request->sub_status == 'Mandatory waiting period')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '2';
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Opposition')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '3';
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Reply to opposition submitted')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '4';
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Hearing')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '5';
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Appeal')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '6';
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Scrutinization stage')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '7';
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Discrepancy stage')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '8';
        }
        $uppdate_cop->save();
        $update_stat->save();
        Session::flash('success','Copyright status updated successfully!');
        return redirect('copyright/'.$request->copy_id.'/update_status');
    }

}
