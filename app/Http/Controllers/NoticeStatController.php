<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LitigationNotice;
use App\NoticeStatus;
use App\User;
use Session;
use Storage;
use DB;
use Auth;
use PHPUnit\Framework\Error\Notice;

class NoticeStatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($id)
    {
        $post = LitigationNotice::find($id);
        $users = User::all();
    	$ar1 = array();
    	foreach ($users as $user) {
    		$ar1[$user->id] = $user->first_name;
    	}
    	return view('notice.status',compact('post','ar1'))->with('no',1);
    }

    public function store(Request $request)
    {
    	$this->validate($request,array(
    		'r_date' => 'required',
    		'act_take' => 'required',
    		'next_act' => 'required',
    		'due_date' => 'required',
    		'reminder' => 'required',
    		'user_id' => 'required',

    	));

    	$status = new NoticeStatus;
        $status->stat_id = $request->stat_id;
        $status->status = $request->status;
    	$status->r_date = date('Y-m-d',strtotime($request->r_date));
    	$status->act_take = $request->act_take;
    	$status->next_act = $request->next_act;
    	$status->due_date = date('Y-m-d',strtotime($request->due_date));
    	$status->reminder = date('Y-m-d',strtotime($request->reminder));
    	$status->user_id = $request->user_id;
        $status->sent_status = '1';
    	if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'litigation/notices';
            $images=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $images[] = $fullname;
                $has = implode(",",$images);
            }


            $status->docs = $has;
        }

        // Change Status Previous if status is Pending
        if($request->status == 'Pending'){
            // Query for Previous status
            $sent1 = NoticeStatus::where('stat_id',$request->stat_id)
                    ->where('status','Replied')
                    ->where('sent_status','1')
                    ->first();
            $sent2 = NoticeStatus::where('stat_id',$request->stat_id)
                    ->where('status','Closed')
                    ->where('sent_status','1')
                    ->first();
            $sent3 = NoticeStatus::where('stat_id',$request->stat_id)
                    ->where('status','Convert To Litigation')
                    ->where('sent_status','1')
                    ->first();

            // update preious Sent Status
            if($sent1 !=''){
                $sent1->sent_status = '0';
                $sent1->save();
            }
            if($sent2 !=''){
                $sent2->sent_status = '0';
                $sent2->save();
            }
            if($sent3 !=''){
                $sent3->sent_status = '0';
                $sent3->save();
            }
        }

        // Change Status Previous if status is Replied
        if($request->status == 'Replied'){
            // Query for Previous status
            $sent1 = NoticeStatus::where('stat_id',$request->stat_id)
                    ->where('status','Pending')
                    ->where('sent_status','1')
                    ->first();
            $sent2 = NoticeStatus::where('stat_id',$request->stat_id)
                    ->where('status','Closed')
                    ->where('sent_status','1')
                    ->first();
            $sent3 = NoticeStatus::where('stat_id',$request->stat_id)
                    ->where('status','Convert To Litigation')
                    ->where('sent_status','1')
                    ->first();

            // update preious Sent Status
            if($sent1 !=''){
                $sent1->sent_status = '0';
                $sent1->save();
            }
            if($sent2 !=''){
                $sent2->sent_status = '0';
                $sent2->save();
            }
            if($sent3 !=''){
                $sent3->sent_status = '0';
                $sent3->save();
            }
        }

        // Change Status Previous if status is Replied
        if($request->status == 'Closed'){
            // Query for Previous status
            $sent1 = NoticeStatus::where('stat_id',$request->stat_id)
                    ->where('status','Pending')
                    ->where('sent_status','1')
                    ->first();
            $sent2 = NoticeStatus::where('stat_id',$request->stat_id)
                    ->where('status','Replied')
                    ->where('sent_status','1')
                    ->first();
            $sent3 = NoticeStatus::where('stat_id',$request->stat_id)
                    ->where('status','Convert To Litigation')
                    ->where('sent_status','1')
                    ->first();

            // update preious Sent Status
            if($sent1 !=''){
                $sent1->sent_status = '0';
                $sent1->save();
            }
            if($sent2 !=''){
                $sent2->sent_status = '0';
                $sent2->save();
            }
            if($sent3 !=''){
                $sent3->sent_status = '0';
                $sent3->save();
            }
        }

        // Change Status Previous if status is Replied
        if($request->status == 'Convert To Litigation'){
            // Query for Previous status
            $sent1 = NoticeStatus::where('stat_id',$request->stat_id)
                    ->where('status','Pending')
                    ->where('sent_status','1')
                    ->first();
            $sent2 = NoticeStatus::where('stat_id',$request->stat_id)
                    ->where('status','Replied')
                    ->where('sent_status','1')
                    ->first();
            $sent3 = NoticeStatus::where('stat_id',$request->stat_id)
                    ->where('status','Closed')
                    ->where('sent_status','1')
                    ->first();

            // update preious Sent Status
            if($sent1 !=''){
                $sent1->sent_status = '0';
                $sent1->save();
            }
            if($sent2 !=''){
                $sent2->sent_status = '0';
                $sent2->save();
            }
            if($sent3 !=''){
                $sent3->sent_status = '0';
                $sent3->save();
            }
        }

        $update = LitigationNotice::find($request->stat_id);
        $update->status = $request->status;

        $status->save();
        $update->save();
        Session::flash('success','Status Addedd');
        return redirect()->route('notices.show',$request->stat_id);
    }

    public function edit($id)
    {
        $post = NoticeStatus::find($id);
        $users = User::all();
    	$ar1 = array();
    	foreach ($users as $user) {
    		$ar1[$user->id] = $user->first_name;
    	}
        return view('notice.edit_status',compact('post','ar1'));
    }

    public function update(Request $request,$id)
    {
        $post = NoticeStatus::find($id);
        $this->validate($request,array(
    		'r_date' => 'required',
    		'act_take' => 'required',
    		'next_act' => 'required',
    		'due_date' => 'required',
    		'reminder' => 'required',
    		'user_id' => 'required',

        ));

        $post->status = $request->status;
    	$post->r_date = date('Y-m-d',strtotime($request->r_date));
    	$post->act_take = $request->act_take;
    	$post->next_act = $request->next_act;
    	$post->due_date = date('Y-m-d',strtotime($request->due_date));
    	$post->reminder = date('Y-m-d',strtotime($request->reminder));
    	$post->user_id = $request->user_id;
    	if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'litigation/notices';
            $images=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $images[] = $fullname;
                $has = implode(",",$images);
            }
            $post->docs = $has;
        }
        $post->save();
        Session::flash('success','Status Addedd');
        return redirect()->route('notices.show',$post->stat_id);

    }

}
