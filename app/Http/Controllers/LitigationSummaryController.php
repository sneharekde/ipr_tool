<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LitigationSummary;
use App\LitiDoc;
use App\Hearing;
use App\Completion;
Use App\EntityList;
use App\Unit;
use App\FunctionList;
use App\LitiType;
use App\User;
use App\ExternalCounsel;
use App\Currency;
use App\LawFirm;
use App\Advocate;
use App\CourtList;
use Storage;
use DB;
use Auth;
use Session;

class LitigationSummaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $summs = LitigationSummary::orderBy('id', 'DESC')->get();
        $ents = DB::table('litigation_summaries')->select('entity')->groupBy('entity')->get();
        $locs = DB::table('litigation_summaries')->select('Location')->groupBy('Location')->get();
        $deps = DB::table('litigation_summaries')->select('department')->groupBy('department')->get();
        $bys = DB::table('litigation_summaries')->select('by_ag')->groupBy('by_ag')->get();
        $cats = DB::table('litigation_summaries')->select('category')->groupBy('category')->get();
        $cnps = DB::table('litigation_summaries')->select('int_per')->groupBy('int_per')->get();
        $status = DB::table('litigation_summaries')->select('status')->groupBy('status')->get();
        $critics = DB::table('litigation_summaries')->select('critic')->groupBy('critic')->get();
        return view('summary.index',compact('summs','ents','locs','deps','bys','cats','cnps','status','critics'))->with('no', 1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $entities = EntityList::all();
        $units = Unit::all();
        $functions = FunctionList::all();
        $categories = LitiType::all();
        $users = User::where('role','!=','Brand Protection Manager')->get();
        $exts = ExternalCounsel::all();
        $currs = Currency::all();
        $laws = LawFirm::all();
        $advocs = Advocate::all();
        $courts = CourtList::all();
        $entity = array();
        $uni = array();
        $func = array();
        $cat = array();
        $use = array();
        $ext = array();
        $cur = array();
        $lao = array();
        $adv = array();
        $cour = array();
        foreach ($entities as $ent) {
            $entity[$ent->entity_child] = $ent->entity_child;
        }
        foreach ($units as $unit) {
            $uni[$unit->unit] = $unit->unit;
        }
        foreach ($functions as $function) {
            $func[$function->function] = $function->function;
        }
        foreach ($categories as $cate) {
            $cat[$cate->name] = $cate->name;
        }
        foreach ($users as $user) {
            $use[$user->id] = $user->first_name;
        }
        foreach ($exts as $ex) {
            $ext[$ex->counsel] = $ex->counsel;
        }
        foreach ($currs as $curr) {
            $cur[$curr->code] = $curr->code;
        }
        foreach ($laws as $law) {
            $lao[$law->name] = $law->name;
        }
        foreach ($advocs as $advoc) {
            $adv[$advoc->advocate] = $advoc->advocate;
        }
        foreach ($courts as $court) {
            $cour[$court->name] = $court->name;
        }

        return view('summary.create',compact('entity','func','uni','cat','use','ext','cur','lao','adv','cour'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'entity' => 'required',
            'Location' => 'required',
            'department' => 'required',
            'category' => 'required',
            'by_ag' => 'required',
            'by_part' => 'required',
            'against_part' => 'required',
            'acting_as' => 'required',
            'law_firm' => 'required',
            'adv' => 'required',
            'case_date' => 'required',
            'critic' => 'required',
            'opp_act_as' => 'required',
        ));

        $summary = new LitigationSummary;
        $summary->entity = $request->entity;
        $summary->Location = $request->Location;
        $summary->department = $request->department;
        $summary->category = $request->category;
        $summary->by_ag = $request->by_ag;
        $summary->by_part = $request->by_part;
        $summary->against_part = $request->against_part;
        $summary->acting_as = $request->acting_as;
        $summary->ext_counsel = $request->ext_counsel;
        $summary->law_firm = $request->law_firm;
        $summary->adv = $request->adv;
        $summary->user_id = $request->user_id;
        $summary->case_date = date('Y-m-d',strtotime($request->case_date));
        $summary->int_per = $request->int_per;
        $summary->critic = $request->critic;
        $summary->opp_act_as = $request->opp_act_as;
        $summary->opp_part_add = $request->opp_part_add;
        $summary->opp_part_firm = $request->opp_part_firm;
        $summary->opp_part_adv = $request->opp_part_adv;
        $summary->opp_part_adv_c = $request->opp_part_adv_c;
        $summary->case_ref = $request->case_ref;
        $summary->juris = $request->juris;
        $summary->court = $request->court;
        $summary->rel_law = $request->rel_law;
        $summary->comments = $request->comments;
        $summary->date = date('Y-m-d',strtotime($request->date));
        $summary->amount_involved = $request->amount_involved;
        $summary->currency = $request->currency;
        $summary->conv_curr = $request->conv_curr;
        $summary->conv_amt = $request->conv_amt;

        $summary->save();
        Session::flash('success','Litigation Summary Added Successfully');
        return redirect('summary');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $summ = LitigationSummary::find($id);
        $comps = Completion::where('liti_id',$id)->orderBy('id','desc')->get();
        $hears = Hearing::where('hear_id',$id)->orderBy('id','desc')->get();
        $docss = LitiDoc::where('doc_id',$id)->orderBy('id','desc')->get();

        $courts = CourtList::all();
        $cour = array();
        foreach ($courts as $court) {
            $cour[$court->name] = $court->name;
        }
        return view('summary.show',compact('summ','cour','comps','hears','docss'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $summ = LitigationSummary::find($id);
        $entities = EntityList::all();
        $units = Unit::all();
        $functions = FunctionList::all();
        $categories = LitiType::all();
        $users = User::where('role','!=','Brand Protection Manager')->get();
        $exts = ExternalCounsel::all();
        $currs = Currency::all();
        $laws = LawFirm::all();
        $advocs = Advocate::all();
        $courts = CourtList::all();
        $entity = array();
        $uni = array();
        $func = array();
        $cat = array();
        $use = array();
        $ext = array();
        $cur = array();
        $lao = array();
        $adv = array();
        $cour = array();
        foreach ($entities as $ent) {
            $entity[$ent->entity_child] = $ent->entity_child;
        }
        foreach ($units as $unit) {
            $uni[$unit->unit] = $unit->unit;
        }
        foreach ($functions as $function) {
            $func[$function->function] = $function->function;
        }
        foreach ($categories as $cate) {
            $cat[$cate->name] = $cate->name;
        }
        foreach ($users as $user) {
            $use[$user->id] = $user->first_name;
        }
        foreach ($exts as $ex) {
            $ext[$ex->counsel] = $ex->counsel;
        }
        foreach ($currs as $curr) {
            $cur[$curr->code] = $curr->code;
        }
        foreach ($laws as $law) {
            $lao[$law->name] = $law->name;
        }
        foreach ($advocs as $advoc) {
            $adv[$advoc->advocate] = $advoc->advocate;
        }
        foreach ($courts as $court) {
            $cour[$court->name] = $court->name;
        }

        return view('summary.edit',compact('summ','entity','func','uni','cat','use','ext','cur','lao','adv','cour'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'entity' => 'required',
            'Location' => 'required',
            'department' => 'required',
            'category' => 'required',
            'by_ag' => 'required',
            'by_part' => 'required',
            'against_part' => 'required',
            'acting_as' => 'required',
            'law_firm' => 'required',
            'adv' => 'required',
            'case_date' => 'required',
            'critic' => 'required',
            'opp_act_as' => 'required',
        ));

        $summary = LitigationSummary::find($id);
        $summary->entity = $request->entity;
        $summary->Location = $request->Location;
        $summary->department = $request->department;
        $summary->category = $request->category;
        $summary->by_ag = $request->by_ag;
        $summary->by_part = $request->by_part;
        $summary->against_part = $request->against_part;
        $summary->acting_as = $request->acting_as;
        $summary->ext_counsel = $request->ext_counsel;
        $summary->law_firm = $request->law_firm;
        $summary->adv = $request->adv;
        $summary->user_id = $request->user_id;
        $summary->case_date = date('Y-m-d',strtotime($request->case_date));
        $summary->int_per = $request->int_per;
        $summary->critic = $request->critic;
        $summary->opp_act_as = $request->opp_act_as;
        $summary->opp_part_add = $request->opp_part_add;
        $summary->opp_part_firm = $request->opp_part_firm;
        $summary->opp_part_adv = $request->opp_part_adv;
        $summary->opp_part_adv_c = $request->opp_part_adv_c;
        $summary->case_ref = $request->case_ref;
        $summary->juris = $request->juris;
        $summary->court = $request->court;
        $summary->rel_law = $request->rel_law;
        $summary->comments = $request->comments;
        $summary->date = date('Y-m-d',strtotime($request->date));
        $summary->amount_involved = $request->amount_involved;
        $summary->currency = $request->currency;
        $summary->conv_curr = $request->conv_curr;
        $summary->conv_amt = $request->conv_amt;

        $summary->save();
        Session::flash('success','Litigation Summary Update Successfully');
        return redirect('summary');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = LitigationSummary::find($id);
        dd($post);
    }

    public function search(Request $request)
    {
        $ent = $request->ent;
        $loc = $request->loc;
        $dep = $request->dep;
        $by_ag = $request->by_ag;
        $cat = $request->cat;
        $cnp = $request->cnp;
        $from_date = $request->form;
        $to_date = $request->to;
        $stat = $request->stat;
        $crit = $request->crit;

        $ents = DB::table('litigation_summaries')->select('entity')->groupBy('entity')->get();
        $locs = DB::table('litigation_summaries')->select('Location')->groupBy('Location')->get();
        $deps = DB::table('litigation_summaries')->select('department')->groupBy('department')->get();
        $bys = DB::table('litigation_summaries')->select('by_ag')->groupBy('by_ag')->get();
        $cats = DB::table('litigation_summaries')->select('category')->groupBy('category')->get();
        $cnps = DB::table('litigation_summaries')->select('int_per')->groupBy('int_per')->get();
        $status = DB::table('litigation_summaries')->select('status')->groupBy('status')->get();
        $critics = DB::table('litigation_summaries')->select('critic')->groupBy('critic')->get();

        $results = DB::table('litigation_summaries')
            ->where(function ($query) use ($ent,$loc, $dep, $by_ag,$cat, $cnp, $from_date, $to_date,$stat,$crit){
                if ($ent) {
                    $query->where('entity','=', $ent);
                }if ($loc) {
                    $query->where('Location','=', $loc);
                }
                if ($dep) {
                    $query->where('department','=', $dep);
                }
                if ($by_ag) {
                    $query->where('by_ag','=', $by_ag);
                }
                if ($cat) {
                    $query->where('category','=', $cat);
                }
                if ($cnp) {
                    $query->where('int_per','=', $cnp);
                }
                if ($from_date) {
                    $query->where('created_at','=',[$from_date, $to_date]);
                }
                if ($stat) {
                    $query->where('status','=', $stat);
                }
                if ($crit) {
                    $query->where('critic','=', $crit);
                }

            })->get();
            return view('summary.search', compact('results','ents','locs','deps','bys','cats','cnps','status','critics','ent','loc','dep','by_ag','cat','cnp','from_date','to_date','stat','crit'))->with('no',1);
    }

    public function deletes($id)
    {
        $post = LitigationSummary::find($id);
        Storage::delete($post->docs);
        $post->delete();

        Session::flash('success','Litigation Summary deleted!');
        return redirect()->route('summary.index');
    }
}
