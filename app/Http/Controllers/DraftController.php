<?php

namespace App\Http\Controllers;

use App\Trademarks;
use Illuminate\Http\Request;

class DraftController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $posts = Trademarks::where('submit_status','draft')->orderBY('id','DESC')->get();
        return view('draft.index',compact('posts'))->with('no',1);
    }
}
