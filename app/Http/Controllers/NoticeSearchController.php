<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LitigationNotice;
Use App\EntityList;
use App\Unit;
use App\FunctionList;
use App\LitiType;
use App\User;
use App\ExternalCounsel;
use App\Currency;
use Storage;
use DB;
use Auth;
use Session;

class NoticeSearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('notice.search');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ent = $request->ent;
        $loc = $request->loc;
        $dep = $request->dep;
        $by_ag = $request->by_ag;
        $cat = $request->cat;
        $cnp = $request->cnp;
        $from_date = $request->form;
        $to_date = $request->to;

        $ents = DB::table('litigation_notices')->select('entity')->groupBy('entity')->get();
        $locs = DB::table('litigation_notices')->select('Location')->groupBy('Location')->get();
        $deps = DB::table('litigation_notices')->select('department')->groupBy('department')->get();
        $bys = DB::table('litigation_notices')->select('by_ag')->groupBy('by_ag')->get();
        $cats = DB::table('litigation_notices')->select('category')->groupBy('category')->get();
        $cnps = DB::table('litigation_notices')->select('int_per')->groupBy('int_per')->get();

        $results = DB::table('litigation_notices')
            ->where(function ($query) use ($ent,$loc, $dep, $by_ag,$cat, $cnp, $from_date, $to_date){
                if ($ent) {
                    $query->where('entity','=', $ent);
                }if ($loc) {
                    $query->where('Location','=', $loc);
                }
                if ($dep) {
                    $query->where('department','=', $dep);
                }
                if ($by_ag) {
                    $query->where('by_ag','=', $by_ag);
                }
                if ($cat) {
                    $query->where('category','=', $cat);
                }
                if ($cnp) {
                    $query->where('int_per','=', $cnp);
                }
                if ($from_date) {
                    $query->where('created_at','=',[$from_date, $to_date]);
                }
            })->get();
            return view('notice.search', compact('results','ents','locs','deps','bys','cats','cnps'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
