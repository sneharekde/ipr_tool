<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use DB;
use Session;
use App\Completion;
use App\LitigationSummary;
use App\CourtList;

class CompletionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function comple($id){
        $comps = Completion::where('liti_id',$id)->orderBy('id','desc')->get();
        $summ = LitigationSummary::find($id);
        $courts = CourtList::all();
        $cour = array();
        foreach ($courts as $court) {
            $cour[$court->name] = $court->name;
        }
        return view('complete.index',compact('comps','summ','cour'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'status' => 'required',
        ));
        $comp = new Completion;
        $comp->liti_id = $request->liti_id;
        $comp->status = $request->status;
        $comp->dis_date = date('Y-m-d',strtotime($request->dis_date));
        $comp->comments = $request->comments;
        if ($request->status == 'Against') {
            if($request->filing == 'yes'){
                $comp->last_date = date('Y-m-d',strtotime($request->last_date));
            }
        }
        if($request->hasFile('doc') && $request->doc->isvalid())
        {
            $doc = $request->doc->getClientOriginalName(); //Get Image Name
            $file = $doc;
            $request->doc->move(public_path('litigation/summary/update'),$file);

            $comp->doc = $file;
        }
        $comp->synop = $request->synop;
        $comp->court = $request->court;
        $comp->save();

        $update = LitigationSummary::find($request->liti_id);
        $update->status = $request->status;
        if ($request->status == 'In Favour') {
            $update->prio = '2';
        }elseif($request->status == 'Against')
        {
            $update->prio = '3';
        }elseif($request->status == 'Settled')
        {
            $update->prio = '4';
        }elseif($request->status == 'WithDrawn')
        {
            $update->prio = '5';
        }
        $update->save();

        Session::flash('success','Update Successfully');
        return redirect('summary/completion/'.$request->liti_id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comp = Completion::find($id);
        $courts = CourtList::all();
        $cour = array();
        foreach ($courts as $court) {
            $cour[$court->name] = $court->name;
        }
        return view('complete.edit',compact('comp','cour'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
