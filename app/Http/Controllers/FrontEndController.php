<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Trademarks;
use App\Copyrightn;
use App\Designm;
use App\Unit;
use App\NatureApplication;
use App\ApplicationField;
use App\User;
use App\Applicant;
use App\NatureApplicant;
use App\Agent;
use App\CategoryMark;
use App\Language;
use App\ClassGoodServices;
use App\SubClassGoodsService;
use DB;
use App\trademark_status;
use App\UpdateStatus;
use App\EntityList;
use Charts;
use App\Investigation;
use App\Enforcement;
use App\Copyright;
use App\Design;
use App\Patent;
use App\UpdateStatusTrad;
use App\LitigationSummary;
use App\TradeClass;
use App\Enf;
use App\Entity;
use App\Invest;


class FrontEndController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        // Fethc Count Value on Dashboard
        $total_tradmark = DB::table('trademarks')->count();
        $total_invest = DB::table('invests')->count();
        $total_enforce = DB::table('enfs')->count();
        $total_brand = $total_invest + $total_enforce;
        $total_copyright = DB::table('copyrightns')->count();
        $total_design = DB::table('designms')->count();
        $total_patent = DB::table('patents')->count();

        $total_liti = DB::table('litigation_summaries')->count();

        // Chars
        $liti = LitigationSummary::selectRaw('count(status) as count,status')->orderBy('prio')->groupBy('status')->get();
        $liti_ar = array();
        foreach ($liti as $lit) {
                $liti_ar[$lit->status] = (int)$lit->count;
        }

        // Fetch Count Data and value on Charts
        $data = Trademarks::selectRaw('count(status) as count,status')->orderBy('sp','ASC')->groupBy('status')->get();
        $array = array();
        foreach ($data as $result)
        {
            $array[$result->status]=(int)$result->count;
        }
        // Fetch Sub Status
        $datas = Trademarks::selectRaw('count(sub_status) as count,sub_status')->orderBy('ssp','ASC')->groupBy('sub_status')->where('ssp','!=','0')->get();
        $arrayss = array();
        foreach ($datas as $resultss)
        {
            $arrayss[$resultss->sub_status]=(int)$resultss->count;
        }
        // Show all trademark
        $showes = Trademarks::all();
        // Fetch Count Data and value of Investigation Module
        $data2 = Invest::selectRaw('count(target) as count, target')->groupBy('target')->get();
        $array3 = array();
        foreach ($data2 as $result2)
        {
            $array3[$result2->target] =(int)$result2->count;
        }
        // Fetch Count Data and Value on Enforcement Value in Charts
        $data3 = Enf::selectRaw('count(target) as count, target')->groupBy('target')->get();
        $array4 = array();
        foreach ($data3 as $result3)
        {
            $array4[$result3->target] =(int)$result3->count;
        }
        // Show all Enforcemant Data
        $showes3 = Enf::all();

        $data4 = Copyrightn::selectRaw('count(status) as count , status')->orderBy('sp')->groupBy('status')->get();
        $array5 = array();
        foreach ($data4 as $result4) {
            $array5[$result4->status] = (int)$result4->count;
        }
        $data5 = Copyrightn::selectRaw('count(sub_status) as count , sub_status')->orderBy('ssp')->groupBy('sub_status')->where('ssp','!=','')->get();
        $array6 = array();
        foreach ($data5 as $result5) {
            $array6[$result5->sub_status] = (int)$result5->count;
        }

        $data6 = Designm::selectRaw('count(status) as count , status')->orderBy('sp')->groupBy('status')->get();
        $array7 = array();
        foreach ($data6 as $result6) {
            $array7[$result6->status] = (int)$result6->count;
        }
        $data7 = Designm::selectRaw('count(sub_status) as count , sub_status')->orderBy('ssp')->groupBy('sub_status')->where('ssp','!=','')->get();
        $array8 = array();
        foreach ($data7 as $result7) {
            $array8[$result7->sub_status] = (int)$result7->count;
        }

        $data8 = Patent::selectRaw('count(status) as count , status')->groupBy('status')->get();
        $array9 = array();
        foreach ($data8 as $result8) {
            $array9[$result8->status] = (int)$result8->count;
        }
        $data9 = Patent::selectRaw('count(sub_status) as count , sub_status')->where('sub_status','!=','')->groupBy('sub_status')->get();
        $array10 = array();
        foreach ($data9 as $result9) {
            $array10[$result9->sub_status] = (int)$result9->count;
        }
        return view('dashbord', compact('total_tradmark', 'total_brand','total_design','total_copyright','array','array3','array4','array5','array6','array7','array8','array9','array10','total_patent','data','data2','data3','data4','data5','data6','data7','data8','data9','datas','arrayss','total_liti','liti_ar','liti'));
    }

    public function tradmark_portfolio()
    {
        $showes = Trademarks::where('submit_status','publish')->orderBy('id', 'DESC')->get();
        $trademark_statuss = Trademarks::selectRaw('status')->groupBy('status')->get();

        //$class = Trademarks::selectRaw('classg_id')->groupBy('classg_id')->get();
        $prods = Trademarks::selectRaw('prod_id')->groupBy('prod_id')->get();
        $ages = Trademarks::selectRaw('agent_code')->groupBy('agent_code')->get();
        $natapls = Trademarks::selectRaw('natapl_id')->groupBy('natapl_id')->get();
        $nates = Trademarks::selectRaw('nate_id')->groupBy('nate_id')->get();
        $nateps = Trademarks::selectRaw('appf_id')->groupBy('appf_id')->get();
        $catmaks = Trademarks::selectRaw('catmak_id')->groupBy('catmak_id')->get();
        $applcs = Trademarks::selectRaw('name_in')->groupBy('name_in')->get();
        $agents = Trademarks::selectRaw('agent_id')->groupBy('agent_id')->get();
        $ents = Trademarks::selectRaw('ent_id')->groupBy('ent_id')->get();
        $stats = Trademarks::selectRaw('status')->groupBy('status')->get();
        return view('tm_portfolio' , compact('showes','trademark_statuss','ages','prods','natapls','nates','nateps','catmaks','applcs','agents','ents','stats'))->with('no',1);
    }

    public function tm_index(){
        $total_tradmark = DB::table('trademarks')->count();



    // $data = DB::table('trademarks')
    //   ->select(
    //     DB::raw('status as status'),
    //     DB::raw('count(*) as number'))
    //   ->groupBy('status')
    //   ->get();

    //  $array[] = ['status','number'];
    //  foreach($data as $key => $value)
    //  {
    //   $array[++$key] = [$value->status, $value->number];

    //  }

        $data = Trademarks::selectRaw('count(status) as count,status')->groupBy('status')->get();

        $array = array();
         foreach ($data as $result) {
             $array[$result->status]=(int)$result->count;
         }




     $showes = Trademarks::all();


    //     $data_one = DB::table('trademarks')
    //   ->select(
    //     DB::raw('entitty as entitty'),
    //     DB::raw('status as status'),
    //     DB::raw('count(*) as number'))
    //   ->groupBy('entitty', 'status')
    //   ->get();
    //  $entitty[] = ['entitty','status','number'];
    //  foreach($data_one as $key => $value)
    //  {
    //   $entitty[++$key] = [$value->entitty, $value->status, $value->number];
    //  }


    $data_two = Trademarks::selectRaw('entitty as entitty')->selectRaw('count(status) as count,status')->groupBy('entitty','status')->get();

          $array2 = array();
         foreach ($data_two as $key=>  $result) {
             $array2[++$key] = [$result->entitty,$result->status,(int)$result->count];
         }


           return view('tm_dashbord', compact('total_tradmark', 'showes','array', 'array2'));
    }


    public function add_tradmark()
    {
        $natureapplications = NatureApplication::all();
        $prods = Product::all();
        $applicationfields = ApplicationField::all();
        $users = User::all();
        $applicants = Applicant::all();
        $natureapplicants = NatureApplicant::all();
        $agents = Agent::all();
        $categorymarks = CategoryMark::all();
        $languages = Language::all();
        $classgoodservicess = ClassGoodservices::all();
        $subclassgood = SubClassGoodsService::all();
        $entitys = Entity::all();
        $classtrads = TradeClass::all();

        $ar1 = array();
        $ar2 = array();
        $ar3 = array();
        $ar4 = array();
        $ar5 = array();
        $ar6 = array();
        $ar7 = array();
        $ar8 = array();
        $ar9 = array();
        $ar10 = array();

        foreach ($prods as $prod) {
            $ar1[$prod->id] = $prod->name;
        }
        foreach ($natureapplications as $nat) {
            $ar2[$nat->id] = $nat->nature_of_application;
        }
        foreach ($users as $user) {
            $ar3[$user->id] = $user->first_name." ".$user->last_name;
        }
        foreach ($applicationfields as $appf) {
            $ar4[$appf->id] = $appf->application_field;
        }
        foreach ($entitys as $entity) {
            $ar5[$entity->id] = $entity->name;
        }
        foreach ($natureapplicants as $natappnt) {
            $ar6[$natappnt->id] = $natappnt->nature_of_applicant;
        }
        foreach ($agents as $agent) {
            $ar7[$agent->id] = $agent->name;
        }
        foreach ($categorymarks as $catmak) {
            $ar8[$catmak->id] = $catmak->category_of_mark;
        }
        foreach ($languages as $lang) {
            $ar9[$lang->id] = $lang->language;
        }
        foreach ($classtrads as $class) {
            $ar10[$class->id] = $class->name;
        }
        return view('new_trademark', compact('ar1','ar2','ar3','ar4','ar5','ar6','ar7','ar8','ar9','ar10', 'applicants'));
    }

    public function tradmark_summary(){
        $showes = Trademarks::all();
        return view('trademarks_summary', compact('showes'));
    }

    public function status_create($id){
        $stats = UpdateStatusTrad::where('trade_id',$id)->orderBy('id','desc')->get();
        $add_tradmark = Trademarks::find($id);
        $trademark_statuss = trademark_status::all();
        $statues = UpdateStatus::where('tradmark_id', $id)->orderBy('id','desc')->get();
        $appeal_opposeds = UpdateStatus::where('status', '=', 'Appealed/Opposed')->get();
        return view('update_status', compact('add_tradmark','trademark_statuss', 'statues','appeal_opposeds',"stats"))->with('no',1);
    }

    public function edit_status($id){
        $stats = UpdateStatusTrad::find($id);
        return view('edit_trade_status',compact('stats'));
    }

    public function welcome(){
        $total_tradmark = DB::table('trademarks')->count();



    // $data = DB::table('trademarks')
    //   ->select(
    //     DB::raw('status as status'),
    //     DB::raw('count(*) as number'))
    //   ->groupBy('status')
    //   ->get();

    //  $array[] = ['status','number'];
    //  foreach($data as $key => $value)
    //  {
    //   $array[++$key] = [$value->status, $value->number];

    //  }

        $data = Trademarks::selectRaw('count(status) as count,status')->groupBy('status')->get();

        $array = array();
         foreach ($data as $result) {
             $array[$result->status]=(int)$result->count;
         }




     $showes = Trademarks::all();


    //     $data_one = DB::table('trademarks')
    //   ->select(
    //     DB::raw('entitty as entitty'),
    //     DB::raw('status as status'),
    //     DB::raw('count(*) as number'))
    //   ->groupBy('entitty', 'status')
    //   ->get();
    //  $entitty[] = ['entitty','status','number'];
    //  foreach($data_one as $key => $value)
    //  {
    //   $entitty[++$key] = [$value->entitty, $value->status, $value->number];
    //  }


    $data_two = Trademarks::selectRaw('count(entitty) as count,entitty')->selectRaw('count(status) as count,status')->groupBy('entitty','status')->get();

          $array2 = [];
         foreach ($data_two as $key=> $result) {
             $array2[++$key] = [$result->entitty,$result->status,(int)$result->count];
         }
        return view('welcome', compact('total_tradmark','showes','array', 'array2'));
    }

    public function status_edit($id){
        $status = UpdateStatus::find($id);
        $appeal_opposeds = UpdateStatus::where('status', '=', 'Appealed/Opposed')->get();
         $trademark_statuss = trademark_status::all();
         $statues = UpdateStatus::where('tradmark_id', $id)->get();
        return view('edit_status', compact('status', 'appeal_opposeds', 'trademark_statuss', 'statues'));
    }

    public function modaldatapiechart($id) {

        $name = str_replace("%20"," ",$id);
         $results = Trademarks::all();



      return view('modal-data', compact('name','results'))->with('no',1);
    }

    public function modaldataspiechart($id) {

     $name = str_replace("%20"," ",$id);
      $results = Trademarks::all();



      return view('modal-datas', compact('name','results'))->with('no',1);
    }

    public function modaldata2piechart($id)
    {
        $name = str_replace("%20", " ", $id);
        $results = Invest::all();

        return view('modal-data2', compact('name','results'))->with('no',1);
    }

    public function modaldata3piechart($id)
    {
        $name = str_replace("%20", " ", $id);
        $results = Enf::all();

        return view('modal-data3', compact('name','results'))->with('no',1);
    }

    public function modaldata4piechart($id)
    {
        $name = str_replace("%20", " ", $id);
        $results = Designm::all();

        return view('modal-data4', compact('name','results'))->with('no',1);
    }

    public function modaldata5piechart($id)
    {
        $name = str_replace("%20", " ", $id);
        $results = Designm::all();

        return view('modal-data5', compact('name','results'))->with('no',1);
    }

    public function modaldata6piechart($id)
    {
        $name = str_replace("%20", " ", $id);
        $results = Copyrightn::all();

        return view('modal-data6', compact('name','results'))->with('no',1);
    }

    public function modaldata7piechart($id)
    {
        $name = str_replace("%20", " ", $id);
        $results = Copyrightn::all();

        return view('modal-data7', compact('name','results'))->with('no',1);
    }

    public function modaldata11piechart($id)
    {
        $name = str_replace("%20", " ", $id);
        $results = LitigationSummary::all();

        return view('modal-data11', compact('name','results'))->with('no',1);
    }

    public function modalbarchart($id){
     $string = str_replace("%40"," ",$id);
      $results = Trademarks::all();
        $str1 = str_replace(" ","%20",$string);
        $str2 = str_replace("@"," ", $str1);
        $array = explode(" ",$str2);
        $com_name = $array[0];
        $status_name= $array[1];
        if(strpos($com_name,'%20') !== false){
        $com_name = str_replace("%20"," ",$com_name);
        }
        if(strpos($status_name,'%20') !== false) {
        $status_name = str_replace("%20"," ",$status_name);
        }

         return view('modal-data-barchart', compact('results','string','com_name', 'status_name'));

    }



}
