<?php

namespace App\Http\Controllers;

use App\CopyAgentFee;
use App\Copyrightn;
use Illuminate\Http\Request;
use Session;
use Storage;
use Auth;

class CopyAgentFeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $post = Copyrightn::find($id);
        return view('copyright.fees',compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $post = new CopyAgentFee();
        $post->copy_id = $id;
        $post->date = date('Y-m-d',strtotime($request->date));
        $post->fee = $request->fee;
        if($request->hasFile('invoice') && $request->invoice->isvalid())
        {
            $invoice = $request->invoice->getClientOriginalName(); //Get invoice Name
            $invoice_scan_copy = $invoice;
            $request->invoice->move(public_path('copyright/img'),$invoice_scan_copy);
            $post->invoice = $invoice_scan_copy;
        }
        $post->not_fee = $request->not_fee;
        if($request->hasFile('stamp') && $request->stamp->isvalid())
        {
            $stamp = $request->stamp->getClientOriginalName(); //Get stamp Name
            $stamp_scan_copy = $stamp;
            $request->stamp->move(public_path('copyright/img'),$stamp_scan_copy);
            $post->stamp = $stamp_scan_copy;
        }
        $post->save();
        Session::flash('success','Agent or Attorney Added!');
        return redirect()->route('copyrights.show',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = CopyAgentFee::find($id);
        return view('copyright.fees_edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = CopyAgentFee::find($id);
        $post->date = date('Y-m-d',strtotime($request->date));
        $post->fee = $request->fee;
        if($request->hasFile('invoice') && $request->invoice->isvalid())
        {
            $invoice = $request->invoice->getClientOriginalName(); //Get invoice Name
            $invoice_scan_copy = $invoice;
            $request->invoice->move(public_path('copyright/img'),$invoice_scan_copy);
            $post->invoice = $invoice_scan_copy;
        }
        $post->not_fee = $request->not_fee;
        if($request->hasFile('stamp') && $request->stamp->isvalid())
        {
            $stamp = $request->stamp->getClientOriginalName(); //Get stamp Name
            $stamp_scan_copy = $stamp;
            $request->stamp->move(public_path('copyright/img'),$stamp_scan_copy);
            $post->stamp = $stamp_scan_copy;
        }
        $post->save();
        Session::flash('success','Agent or Attorney Updated!');
        return redirect()->route('copyrights.show',$post->copy_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
