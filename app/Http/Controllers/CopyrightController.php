<?php

namespace App\Http\Controllers;

use App\Agent;
use App\CopyAgentFee;
use Illuminate\Http\Request;
use App\Copyright;
use App\UpdateCopyrightStatus;
use App\User;
use App\Language;
use App\DesignationList;
use App\Copyrightn;
use App\Trademarks;
use Session;
use Storage;
use DB;
use File;
use Auth;
use PDF;

class CopyrightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $copyrights = Copyrightn::where('draft','submit')->orderBy('id','desc')->get();
        // Group by all search data

        $nofa = DB::table('copyrightns')->select('nofa')->groupBy('nofa')->get();
        $roc = DB::table('copyrightns')->select('roc')->groupBy('roc')->get();
        $dairyn = DB::table('copyrightns')->select('diary')->groupBy('diary')->get();
        $titlew = DB::table('copyrightns')->select('titlew')->groupBy('titlew')->get();
        $status = DB::table('copyrightns')->select('status')->groupBy('status')->get();
        return view('copyright.index', compact('copyrights','nofa','roc','dairyn','titlew','status'))->with('no', 1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $languages = Language::all();
        $agents = Agent::all();
        return view('copyright.create', compact('users','languages','agents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->has('draft')){
            $this->validate($request,array(
                'gov' =>'required|integer',
                'dairy' => 'required',
                'dof' => 'required',
                'user' => 'required',
                'noap' =>'required',
                'title' => 'required',
                'description' => 'required',
            ));

            $draft = new Copyrightn();
            $draft->gov = $request->gov;
            $draft->agent_id = $request->agent_id;
            $draft->diary = $request->dairy;
            $draft->dof = date('Y-m-d', strtotime($request->dof));
            $draft->user_id = $request->user;
            $draft->nofa = $request->noap;
            $draft->titlew = $request->title;
            $draft->description = $request->description;
            $draft->lang_id = $request->lang;
            $draft->remarks = $request->remark;
            // upload Work
            if($request->hasFile('image') && $request->image->isvalid())
            {
                $image = $request->image->getClientOriginalName(); //Get Image Name
                $image_scan_copy = $image;
                $request->image->move(public_path('copyright/img'),$image_scan_copy);
                $draft->image = $image_scan_copy;
            }

            // User Affidavit
            if($request->hasFile('affid') && $request->affid->isvalid())
            {
                $affid = $request->affid->getClientOriginalName(); //Get affid Name
                $affid_scan_copy = $affid;
                $request->affid->move(public_path('copyright/docs'),$affid_scan_copy);
                $draft->user_aff = $affid_scan_copy;
            }
            // Payment Details
            if($request->hasFile('coppdf') && $request->coppdf->isvalid())
            {
                $coppdf = $request->coppdf->getClientOriginalName(); //Get coppdf Name
                $coppdf_scan_copy = $coppdf;
                $request->coppdf->move(public_path('copyright/docs'),$coppdf_scan_copy);
                $draft->copy_app = $coppdf_scan_copy;
            }
            // POA
            if($request->hasFile('poa') && $request->poa->isvalid())
            {
                $poa = $request->poa->getClientOriginalName(); //Get poa Name
                $poa_scan_copy = $poa;
                $request->poa->move(public_path('copyright/docs'),$poa_scan_copy);
                $draft->poa = $poa_scan_copy;
            }
            $draft->addrp = $request->addrapp;
            $draft->app_filed = $request->app_filed;
            $draft->agent_code = $request->agent_code;
            $draft->agent_address = $request->agent_address;
            $draft->work_title = $request->work_title;
            $draft->draft = 'draft';
            $draft->save();
            Session::flash('success','Save as Draft successfully!');
            return redirect('/copyright-draft');
        }elseif($request->has('submit')){
            $this->validate($request,array(
                'gov' =>'required|integer',
                'dairy' => 'required',
                'dof' => 'required',
                'user' => 'required',
                'noap' =>'required',
                'title' => 'required',
                'description' => 'required',
            ));

            $comp = new Copyrightn();
            $comp->gov = $request->gov;
            $comp->agent_id = $request->agent_id;
            $comp->diary = $request->dairy;
            $comp->dof = date('Y-m-d', strtotime($request->dof));
            $comp->user_id = $request->user;
            $comp->nofa = $request->noap;
            $comp->titlew = $request->title;
            $comp->description = $request->description;
            $comp->lang_id = $request->lang;
            $comp->remarks = $request->remark;
            // upload Work
            if($request->hasFile('image') && $request->image->isvalid())
            {
                $image = $request->image->getClientOriginalName(); //Get Image Name
                $image_scan_copy = $image;
                $request->image->move(public_path('copyright/img'),$image_scan_copy);
                $comp->image = $image_scan_copy;
            }

            // User Affidavit
            if($request->hasFile('affid') && $request->affid->isvalid())
            {
                $affid = $request->affid->getClientOriginalName(); //Get affid Name
                $affid_scan_copy = $affid;
                $request->affid->move(public_path('copyright/docs'),$affid_scan_copy);
                $comp->user_aff = $affid_scan_copy;
            }
            // Payment Details
            if($request->hasFile('coppdf') && $request->coppdf->isvalid())
            {
                $coppdf = $request->coppdf->getClientOriginalName(); //Get coppdf Name
                $coppdf_scan_copy = $coppdf;
                $request->coppdf->move(public_path('copyright/docs'),$coppdf_scan_copy);
                $comp->copy_app = $coppdf_scan_copy;
            }
            // POA
            if($request->hasFile('poa') && $request->poa->isvalid())
            {
                $poa = $request->poa->getClientOriginalName(); //Get poa Name
                $poa_scan_copy = $poa;
                $request->poa->move(public_path('copyright/docs'),$poa_scan_copy);
                $comp->poa = $poa_scan_copy;
            }
            $comp->addrp = $request->addrapp;
            $comp->app_filed = $request->app_filed;
            $comp->agent_code = $request->agent_code;
            $comp->agent_address = $request->agent_address;
            $comp->work_title = $request->work_title;
            $comp->status = 'Active Application';
            $comp->sub_status = 'Applied CR';
            $comp->draft = 'submit';
            $comp->save();

            // Update Status
            $update_s = new UpdateCopyrightStatus();
            $update_s->copy_id = $comp->id;
            $update_s->date = $comp->dof;
            $update_s->status = $comp->status;
            $update_s->sub_status = $comp->sub_status;
            $update_s->save();

            Session::flash('success', 'The Copyright Application Successfully saved');
            return redirect()->route('copyrights.index')->with('success', 'The Copyright Application Successfully saved');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $copyright = Copyrightn::find($id);
        $posts = CopyAgentFee::where('copy_id',$id)->orderBy('id','DESC')->get();
        return view('copyright.show',compact('copyright','posts'))->with('no',1);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $copyright = Copyrightn::find($id);
        $users = User::all();
        $languages = Language::all();
        $agents = Agent::all();
        $ar1 = array();
        $ar2 = array();
        $ar3 = array();
        foreach($agents as $agent){
            $ar1[$agent->id] = $agent->name;
        }
        foreach($users as $user){
            $ar2[$user->id] = $user->first_name.' '.$user->last_name;
        }
        foreach($languages as $lang){
            $ar3[$lang->id] = $lang->language;
        }
        return view('copyright.edit',compact('copyright','ar1','ar2','ar3','users','languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if($request->has('draft')){


            $draft = Copyrightn::find($id);
            $draft->gov = $request->gov;
            $draft->diary = $request->diary;
            $draft->dof = date('Y-m-d', strtotime($request->dof));
            $draft->user_id = $request->user_id;
            $draft->nofa = $request->nofa;
            $draft->titlew = $request->titlew;
            $draft->description = $request->description;
            $draft->lang_id = $request->lang_id;
            $draft->remarks = $request->remarks;
            // upload Work
            if($request->hasFile('image') && $request->image->isvalid())
            {
                $image = $request->image->getClientOriginalName(); //Get Image Name
                $image_scan_copy = $image;
                $request->image->move(public_path('copyright/img'),$image_scan_copy);
                $draft->image = $image_scan_copy;
            }

            // User Affidavit
            if($request->hasFile('user_aff') && $request->user_aff->isvalid())
            {
                $user_aff = $request->user_aff->getClientOriginalName(); //Get user_aff Name
                $user_aff_scan_copy = $user_aff;
                $request->user_aff->move(public_path('copyright/docs'),$user_aff_scan_copy);
                $draft->user_aff = $user_aff_scan_copy;
            }
            // Payment Details
            if($request->hasFile('copy_app') && $request->copy_app->isvalid())
            {
                $copy_app = $request->copy_app->getClientOriginalName(); //Get copy_app Name
                $copy_app_scan_copy = $copy_app;
                $request->copy_app->move(public_path('copyright/docs'),$copy_app_scan_copy);
                $draft->copy_app = $copy_app_scan_copy;
            }

            $draft->addrp = $request->addrp;
            $draft->app_filed = $request->app_filed;
            if ($request->app_filed == 'Agent/Attorney') {
                $draft->agent_id = $request->agent_id;
                $draft->agent_code = $request->agent_code;
                $draft->agent_address = $request->agent_address;

                // POA
                if($request->hasFile('poa') && $request->poa->isvalid())
                {
                    $poa = $request->poa->getClientOriginalName(); //Get poa Name
                    $poa_scan_copy = $poa;
                    $request->poa->move(public_path('copyright/docs'),$poa_scan_copy);
                    $draft->poa = $poa_scan_copy;
                }
            } else {
                $draft->agent_id = null;
                $draft->agent_code = null;
                $draft->agent_address = null;
            }
            $draft->work_title = $request->work_title;
            $draft->draft = 'draft';
            $draft->save();
            Session::flash('success','Save as Draft successfully!');
            return redirect('/copyright-draft');
        }elseif($request->has('submit')){


            $comp = Copyrightn::find($id);
            $comp->gov = $request->gov;
            $comp->diary = $request->diary;
            $comp->dof = date('Y-m-d', strtotime($request->dof));
            $comp->user_id = $request->user_id;
            $comp->nofa = $request->nofa;
            $comp->titlew = $request->titlew;
            $comp->description = $request->description;
            $comp->lang_id = $request->lang_id;
            $comp->remarks = $request->remarks;
            // upload Work
            if($request->hasFile('image') && $request->image->isvalid())
            {
                $image = $request->image->getClientOriginalName(); //Get Image Name
                $image_scan_copy = $image;
                $request->image->move(public_path('copyright/img'),$image_scan_copy);
                $comp->image = $image_scan_copy;
            }

            // User Affidavit
            if($request->hasFile('user_aff') && $request->user_aff->isvalid())
            {
                $user_aff = $request->user_aff->getClientOriginalName(); //Get user_aff Name
                $user_aff_scan_copy = $user_aff;
                $request->user_aff->move(public_path('copyright/docs'),$user_aff_scan_copy);
                $comp->user_aff = $user_aff_scan_copy;
            }
            // Payment Details
            if($request->hasFile('copy_app') && $request->copy_app->isvalid())
            {
                $copy_app = $request->copy_app->getClientOriginalName(); //Get copy_app Name
                $copy_app_scan_copy = $copy_app;
                $request->copy_app->move(public_path('copyright/docs'),$copy_app_scan_copy);
                $comp->copy_app = $copy_app_scan_copy;
            }
            $comp->addrp = $request->addrp;
            $comp->app_filed = $request->app_filed;
            if ($request->app_filed == 'Agent/Attorney') {
                $comp->agent_id = $request->agent_id;
                $comp->agent_code = $request->agent_code;
                $comp->agent_address = $request->agent_address;

                // POA
                if($request->hasFile('poa') && $request->poa->isvalid())
                {
                    $poa = $request->poa->getClientOriginalName(); //Get poa Name
                    $poa_scan_copy = $poa;
                    $request->poa->move(public_path('copyright/docs'),$poa_scan_copy);
                    $comp->poa = $poa_scan_copy;
                }
            } else {
                $comp->agent_id = null;
                $comp->agent_code = null;
                $comp->agent_address = null;
            }
            $comp->work_title = $request->work_title;
            $comp->status = 'Active Application';
            $comp->sub_status = 'Applied CR';
            $comp->draft = 'submit';
            $comp->save();

            // check
            $check = UpdateCopyrightStatus::where('copy_id',$comp->id)->first();
            if($check == ''){
                // Update Status
            $update_s = new UpdateCopyrightStatus();
            $update_s->copy_id = $comp->id;
            $update_s->date = $comp->dof;
            $update_s->status = $comp->status;
            $update_s->sub_status = $comp->sub_status;
            $update_s->save();
            }

            Session::flash('success', 'The Copyright Application Successfully saved');
            return redirect()->route('copyrights.index')->with('success', 'The Copyright Application Successfully saved');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {
        $nofas = $request->nofas;
        $rocs = $request->rocs;
        $dairys = $request->dairys;
        $doas = $request->doas;
        $titles = $request->titles;
        $statuss = $request->statuss;
        $from = $request->from;
        $to = $request->to;

        $shows = Copyrightn::orderBy('id','DESC')->get();
        // Group by all search data
        $nofa = DB::table('copyrightns')->select('nofa')->groupBy('nofa')->get();
        $roc = DB::table('copyrightns')->select('roc')->groupBy('roc')->get();
        $dairyn = DB::table('copyrightns')->select('diary')->groupBy('diary')->get();
        $titlew = DB::table('copyrightns')->select('titlew')->groupBy('titlew')->get();
        $status = DB::table('copyrightns')->select('status')->groupBy('status')->get();

        $results = DB::table('copyrightns')->where(function($query) use ($nofas,$rocs,$dairys,$doas,$titles,$statuss,$from,$to){
            if ($nofas) {
                $query->where('nofa', '=',$nofas);
            }
            if ($rocs) {
                $query->where('roc','=',$rocs);
            }
            if ($dairys) {
                $query->where('diary','=',$dairys);
            }
            if ($doas) {
                $query->where('dof','=',$doas);
            }
            if ($titles) {
                $query->where('titlew','=',$titles);
            }
            if ($statuss) {
                $query->where('status','=',$statuss);
            }
            if ($from) {
                $query->whereBetween('dof',[$from,$to]);
            }
        })->get();

        return view('copyright.search', compact('results','shows','nofa','roc','dairyn','titlew','status','nofas','rocs','dairys','doas','titles','statuss','from','to'))->with('no',1);
    }
    public function update_status_create($id)
    {
        $copyright = UpdateCopyrightStatus::find($id);
        $trade = Copyrightn::where('id',$copyright->copy_id)->first();
        return view('copyright.edit_update',compact('copyright','trade'));
    }

    public function update_status_save(Request $request)
    {
        $this->validate($request,array(
            'status' => 'required'
        ));

        $uppdate_cop = UpdateCopyrightStatus::find($request->copy_id);
        $uppdate_cop->status = $request->status;
        // Query for Status
        if ($request->status == 'Abandoned') {
            $uppdate_cop->date = date('Y-m-d',strtotime($request->date1));
            if ($request->hasFile('docs1'))
            {
                $file= $request->file('docs1');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            $uppdate_cop->comment = $request->comment1;
        }

        if ($request->status == 'Rejected') {
            $uppdate_cop->date = date('Y-m-d',strtotime($request->date2));
            if ($request->hasFile('docs2'))
            {
                $file= $request->file('docs2');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            $uppdate_cop->comment = $request->comment2;
        }

        if ($request->status == 'Registered') {
            $uppdate_cop->dor = date('Y-m-d',strtotime($request->dor));
            if ($request->hasFile('cor'))
            {
                $file= $request->file('cor');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            $uppdate_cop->comment = $request->comment3;
            if($request->reg_remider !=''){
                $uppdate_cop->reg_rem = date('Y-m-d',strtotime($request->reg_remider));
            }else{
                $uppdate_cop->reg_rem = null;
            }

            // find Application Date
            $uppdate_cop->dren = date('Y-m-d',strtotime($request->dor. ' +21900 days'));
        }

        if ($request->status == 'Expired') {
            $uppdate_cop->exp_date = date('Y-m-d',strtotime($request->exp_date_exp));
            if ($request->hasFile('docs_exp'))
            {
                $file= $request->file('docs_exp');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            $uppdate_cop->comment = $request->comment_exp;
        }

        // update ROC and Certifcate of Registration
        $post = Copyrightn::find($uppdate_cop->copy_id);

        if ($request->status == 'Registered') {
            $post->roc = $request->roc;
            $post->dor = date('Y-m-d',strtotime($request->dor));
            if ($request->hasFile('cor'))
            {
                $file= $request->file('cor');
                $fullname = $file->getClientOriginalName();
                $post->reg_cert = $fullname;
            }
        }

        if ($request->status == 'Renewed') {
            $uppdate_cop->ren_date = date('Y-m-d',strtotime($request->date3));
            $uppdate_cop->exp_date = date('Y-m-d',strtotime($request->doexp));
        }

        // Query for Sub Status
        if ($request->status == 'Active Application' && $request->sub_status == 'Mandatory waiting period')
        {
            $uppdate_cop->sub_status = $request->sub_status;
            // date of issues of diary no
            $dated = Copyrightn::find($uppdate_cop->copy_id);
            $uppdate_cop->mwp_date = date('Y-m-d',strtotime($dated->dof. ' +30 days'));
            $uppdate_cop->rem_mwp_date = date('Y-m-d',strtotime($request->rem));
            $uppdate_cop->sent_status = '1';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Opposition')
        {
            $uppdate_cop->sub_status = $request->sub_status;
            $uppdate_cop->date = date('Y-m-d',strtotime($request->date4));
            $uppdate_cop->comment = $request->comment4;
            if ($request->hasFile('docs3'))
            {
                $file= $request->file('docs3');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            $uppdate_cop->rep_dead = date('Y-m-d',strtotime($request->date. ' +30 days'));
            $uppdate_cop->rem1 = date('Y-m-d',strtotime($request->rem11));
            $uppdate_cop->rem2 = date('Y-m-d',strtotime($request->rem21));
            $uppdate_cop->rem3 = date('Y-m-d',strtotime($request->rem31));
            $uppdate_cop->sent_status = '1';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Reply to opposition submitted')
        {
            $uppdate_cop->sub_status = $request->sub_status;
            $uppdate_cop->date = date('Y-m-d',strtotime($request->date5));
            $uppdate_cop->comment = $request->comment5;
            if ($request->hasFile('docs4'))
            {
                $file= $request->file('docs4');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Hearing')
        {
            $uppdate_cop->sub_status = $request->sub_status;
            $uppdate_cop->dfh = date('Y-m-d',strtotime($request->date6));
            $uppdate_cop->comment = $request->comment6;
            if ($request->hasFile('docs5'))
            {
                $file= $request->file('docs5');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            $uppdate_cop->rem1 = date('Y-m-d',strtotime($request->rem12));
            $uppdate_cop->rem2 = date('Y-m-d',strtotime($request->rem22));
            $uppdate_cop->rem3 = date('Y-m-d',strtotime($request->rem32));
            $uppdate_cop->sent_status = '1';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Appeal')
        {
            $uppdate_cop->sub_status = $request->sub_status;
            $uppdate_cop->ddj = date('Y-m-d',strtotime($request->date7));
            $uppdate_cop->deadline = date('Y-m-d',strtotime($request->date7. ' +90 days'));

            $uppdate_cop->rem1 = date('Y-m-d',strtotime($request->rem13));
            $uppdate_cop->rem2 = date('Y-m-d',strtotime($request->rem23));
            $uppdate_cop->rem3 = date('Y-m-d',strtotime($request->rem33));
            $uppdate_cop->sent_status = '1';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Scrutinization stage')
        {
            $uppdate_cop->sub_status = $request->sub_status;
            $uppdate_cop->d_rer = date('Y-m-d',strtotime($request->date8));
            $uppdate_cop->comment = $request->comment7;
            if ($request->hasFile('docs6'))
            {
                $file= $request->file('docs6');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Discrepancy stage')
        {
            $uppdate_cop->sub_status = $request->sub_status;
            $uppdate_cop->d_rer = date('Y-m-d',strtotime($request->date9));
            $uppdate_cop->comment = $request->comment8;
            if ($request->hasFile('docs7'))
            {
                $file= $request->file('docs7');
                $destinationPath= 'copyright';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $uppdate_cop->docs = $fullname;
            }
            $uppdate_cop->deadline = date('Y-m-d',strtotime($request->date9. '+30 days'));
            $uppdate_cop->rem1 = date('Y-m-d',strtotime($request->rem14));
            $uppdate_cop->rem2 = date('Y-m-d',strtotime($request->rem24));
            $uppdate_cop->rem3 = date('Y-m-d',strtotime($request->rem34));
        }

        $uppdate_cop->save();
        Session::flash('success','Copyright status updated successfully!');
        return redirect('copyright/'.$uppdate_cop->copy_id.'/update_status');
    }

}
