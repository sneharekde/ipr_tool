<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApplicationField;
use App\TrademarkGovFees;
use Session;


class TradeMarkGovFeesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comps = TrademarkGovFees::all();
        return view('tfees.index',compact('comps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $apps = ApplicationField::all();
        $ar1 = array();
        foreach ($apps as $app) {
            $ar1[$app->id] = $app->application_field;
        }
        return view('tfees.create',compact('ar1'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,array(
            'appf_id' => 'required',
            'app_type' => 'required',
            'fees' => 'required|numeric'
        ));

        $com = new TrademarkGovFees;
        $com->appf_id = $request->appf_id;
        $com->app_type = $request->app_type;
        $com->fees = $request->fees;
        $com->save();
        Session::flash('success','Addedd');
        return redirect('trademark_fees');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
