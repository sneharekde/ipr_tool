<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Target;
use App\Investigation;
use App\Enforcement;
use App\Enforcements;
use App\Expensein;
use App\Expensen;
use App\Fir;
use App\State;
use App\City;
use Storage;
use File;
use App\User;
use App\EntityList;
use Illuminate\Support\Facades\Input;
use Auth;
use DB;
use PDF;
use App\Agency;
use App\BrandProduct;
use Session;
use App\Country;
use App\Enf;
use App\Entity;
use App\Stete;
use App\Invest;

class AddBrandController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function viewall()
    {
        return view('viewall');
    }

    public function InvestShow()
    {
        $enitys = Invest::select('ent_id')->groupBy('ent_id')->get();
        $users = Invest::select('user_id')->groupBy('user_id')->get();
        $agnecis = Invest::select('inag_id')->groupBy('inag_id')->get();
    	$shows = Invest::orderBy('id','DESC')->get();
        $country = Invest::select('country_id')->groupBy('country_id')->get();
        $state = Invest::select('state_id')->groupBy('state_id')->get();
        $cities = Invest::select('city_id')->groupBy('city_id')->get();
        $products = Invest::select('nop_id')->groupBy('nop_id')->get();
        $targets = Invest::select('target')->groupBY('target')->get();
    	return view('invest.show', compact('shows','targets','enitys','users','agnecis','country','state','cities','products'))->with('no',1);
    }

    public function InvestCreate()
    {

        $enitys = Entity::all();
    	$users = User::where('role','=','Brand Protection Manager')->get();
    	$targets = Target::all();
        $agnecis = Agency::all();
        $countries = Country::all();
        $ar1 = [];
        $ar2 = [];
        $ar3 = [];
        $ar4 = [];
        foreach ($enitys as $ent) {
            $ar1[$ent->id] = $ent->name;
        }
        foreach ($agnecis as $agen) {
            $ar2[$agen->id] = $agen->name;
        }
        foreach ($users as $user) {
            $ar3[$user->id] = $user->first_name.' '.$user->last_name;
        }
        foreach ($targets as $tar) {
            $ar4[$tar->targets] = $tar->targets;
        }
    	return view('invest.create',compact('ar3','ar4','ar1','ar2','countries'));
    }

    public function InvestSave(Request $request)
    {

        $dsi = date("Y-m-d", strtotime($request->dsi));
        $dei = date("Y-m-d", strtotime($request->dei));

        $invest = new Invest;

        $invest->inag_id = $request->inag_id;
        $invest->state_id = $request->state;
        $invest->city_id = $request->city_id;
        $invest->target = $request->target;
        $invest->nop_id = $request->nop_id;
        $invest->dsi = $dsi;
        $invest->dei = $dei;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'Scan_Copy_Of_Application';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
            }
            $invest->docs = $has;
        }
        $invest->tm_app = $request->tm_app;
        $invest->user_id = $request->user_id;
        $invest->ent_id = $request->ent_id;
        $invest->country_id = $request->country;
        if($request->nota !=''){
            $invest->nota = implode(":",$request->nota);
        }
        if($request->tar_add !=''){
            $invest->n_addr = implode(":",$request->tar_add);
        }
        $invest->save();
        return redirect('/invest/show')->with('success','Investigation Report Added Successfully');
    }

    public function show_all($id)
    {
        $invest = Invest::find($id);
        $expens = Expensein::where('invest_id',$id)->get();
        return view('invest.show_all',compact('invest','expens'))->with('no',1);
    }

    public function EditInv($id)
    {
    	$invest = Invest::find($id);
    	$enitys = Entity::all();
        $users = User::where('role','=','Brand Protection Manager')->get();
        $targets = Target::all();
        $agnecis = Agency::all();
        $countries = Country::all();
        $states = Stete::where('country_id','=',$invest->country_id)->get();
        $cities = City::where('state_id','=',$invest->state_id)->get();
        $products = BrandProduct::where('ent_id',$invest->ent_id)->get();
    	return view('invest.edit', compact('invest','users','targets','enitys','agnecis','countries','states','cities','products'));
    }

    public function InvestUpdate(Request $request)
    {
    	$invest = Invest::find($request->id);

        $this->validate($request,[
    		'entity' => 'required',
            'inag' => 'required',
            'brandp' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city_id' => 'required',
            'target' => 'required',
            'prod' => 'required',
            'dsi' => 'required',
            'dei' => 'required',
            'nota' => 'required',
            'tar_add' => 'required'

    	]);


        $dsi = date("Y-m-d", strtotime($request->dsi));
        $dei = date("Y-m-d", strtotime($request->dei));

        $invest->inag_id = $request->inag;
        $invest->state_id = $request->state;
        $invest->city_id = $request->city_id;
        $invest->target = $request->target;
        $invest->nop_id = $request->prod;
        $invest->dsi = $dsi;
        $invest->dei = $dei;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'Scan_Copy_Of_Application';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
            }
            $invest->docs = $has;
        }
        $invest->tm_app = $request->tm_app;
        $invest->user_id = $request->brandp;
        $invest->ent_id = $request->entity;
        $invest->country_id = $request->country;
        if($request->nota !=''){
            $invest->nota = implode(":",$request->nota);
        }
        if($request->tar_add !=''){
            $invest->n_addr = implode(":",$request->tar_add);
        }

        $invest->save();
        return redirect('/invest/show')->with('success','Investigation Report Updated Successfully');

    }

    public function search(Request $request)
    {
        $inag = $request->na;
        $entity = $request->entity;
        $countrys = $request->country;
        $states = $request->state;
        $citys = $request->city;
        $product = $request->product;
        $target = $request->target;
        $from_date = $request->form;
        $to_date = $request->to;

        $enitys = Invest::select('ent_id')->groupBy('ent_id')->get();
        $users = Invest::select('user_id')->groupBy('user_id')->get();
        $agnecis = Invest::select('inag_id')->groupBy('inag_id')->get();
    	$shows = Invest::orderBy('id','DESC')->get();
        $country = Invest::select('country_id')->groupBy('country_id')->get();
        $state = Invest::select('state_id')->groupBy('state_id')->get();
        $city = Invest::select('city_id')->groupBy('city_id')->get();
        $products = Invest::select('nop_id')->groupBy('nop_id')->get();
        $targets = Invest::select('target')->groupBY('target')->get();

        $results = Invest::where(function ($query) use ($inag,$entity,$countrys,$states,$citys,$product,$target,$from_date,$to_date)
        {
                if ($inag) {
                    $query->where('inag_id','=', $inag);
                }
                if($entity){
                    $query->where('ent_id','=',$entity);
                }
                if ($countrys) {
                    $query->where('country_id','=', $countrys);
                }
                if ($states) {
                    $query->where('state_id','=', $states);
                }
                if ($citys) {
                    $query->where('city_id','=', $citys);
                }
                if ($product) {
                    $query->where('nop_id','=', $product);
                }
                if ($target) {
                    $query->where('target','=', $target);
                }
                if ($from_date) {
                    $query->where('created_at','=',[$from_date, $to_date]);
                }
        })->get();
        return view('invest.search', compact('results','targets','enitys','users','agnecis','country','state','city','products','inag','entity','countrys','states','citys','product','target','from_date','to_date'))->with('no',1);
    }

    public function EnforceShow()
    {
        $enitys = Enf::select('ent_id')->groupBy('ent_id')->get();
        $countries = Enf::select('country_id')->groupBy('country_id')->get();
    	$states = Enf::select('state_id')->groupBy('state_id')->get();
        $cities = Enf::select('city_id')->groupBy('city_id')->get();
        $tm_apps = Enf::select('tm_app')->groupBy('tm_app')->get();
        $shows = Enf::orderBy('id','DESC')->get();
        $targets = Enf::select('target')->groupBy('target')->get();
    	return view('enforce.show',compact('shows','targets','enitys','countries','cities','states','tm_apps'))->with('no',1);
    }

    public function EnforceCreate()
    {
    	$users = User::all();
    	$targets = Target::all();
        $enitys = Entity::all();
        $agnecis = Agency::all();
        $countries = Country::all();



    	return view('enforce.create',compact('users','targets','enitys','agnecis','countries'));
    }



    public function EnforceSave(Request $request)
    {
        $this->validate($request,[
            'tname' => 'required',
            'oname' => 'required',
            'date' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'target' => 'required',
            'qty' => 'required',
            'amt' => 'required'
        ]);
        $enforce = new Enf;
        $enforce->ent_id = $request->entity;
        $enforce->target_name = $request->tname;
        $enforce->owner = $request->oname;
        $enforce->date = date('Y-m-d',strtotime($request->date));
        $enforce->state_id = $request->state_id;
        $enforce->city_id = $request->city_id;
        $enforce->target = $request->target;
        $enforce->nop_id = $request->nop_id;
        // $enforce->user = $request->state;
        $enforce->qty = $request->qty;
        $enforce->amt = $request->amt;
        $enforce->tm_app = $request->tm_app;
        $enforce->country_id = $request->country;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'document';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
            }
            $enforce->docs = $has;
        }


        $enforce->save();
        Session::flash('success','Enforcement report added successfully');
        return redirect('/enforce/show');
    }

    public function EditEnf($id)
    {
        $user = User::all();
        $enforce = Enf::find($id);
        $targets = Target::all();
        $enitys = Entity::all();
        $agnecis = Agency::all();
        $countries = Country::all();
        $states = Stete::where('country_id','=',$enforce->country_id)->get();
        $cities = City::where('state_id','=',$enforce->state_id)->get();
        $products = BrandProduct::where('ent_id',$enforce->ent_id)->get();
        return view('enforce.edit', compact('enforce','user','targets','enitys','agnecis','countries','states','cities','products'));
    }

    public function EnforceUpdate(Request $request)
    {

        $this->validate($request,[
            'tname' => 'required',
            'oname' => 'required',
            'date' => 'required',
            'state' => 'required',
            'city_id' => 'required',
            'target' => 'required',
            'qty' => 'required',
            'amt' => 'required'
        ]);
        $enforce = Enf::find($request->id);
        $date = date("Y-m-d", strtotime($request->date));
        $enforce->target_name = $request->tname;
        $enforce->owner = $request->oname;
        $enforce->state_id = $request->state;
        $enforce->city_id = $request->city_id;
        $enforce->target = $request->target;
        $enforce->nop_id = $request->nop;
        // $enforce->user = $request->state;
        $enforce->qty = $request->qty;
        $enforce->amt = $request->amt;
        $enforce->date = $date;
        $enforce->tm_app = $request->tm_app;
        $enforce->ent_id = $request->entity;
        $enforce->country_id = $request->country;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'document';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
            }
            $enforce->docs = $has;
        }
        $enforce->save();

        return redirect('/enforce/show')->with('success','Enforcement Report Updated Successfully');

    }

    public function searche(Request $request)
    {
        $entity = $request->entity;
        $country = $request->country;
        $state = $request->state;
        $city = $request->city;
        $target = $request->target;
        $tm_app = $request->tm_app;
        $from_date = $request->from;
        $to_date = $request->to;

        $enitys = Enf::select('ent_id')->groupBy('ent_id')->get();
        $countries = Enf::select('country_id')->groupBy('country_id')->get();
    	$states = Enf::select('state_id')->groupBy('state_id')->get();
        $cities = Enf::select('city_id')->groupBy('city_id')->get();
        $tm_apps = Enf::select('tm_app')->groupBy('tm_app')->get();
        $shows = Enf::orderBy('id','DESC')->get();
        $targets = Enf::select('target')->groupBy('target')->get();

        $results = Enf::where(function ($query) use ($entity,$country,$state, $city, $target, $tm_app, $from_date, $to_date){
                if ($entity) {
                    $query->where('ent_id','=', $entity);
                }
                if ($country) {
                    $query->where('country_id','=', $country);
                }
                if ($state) {
                    $query->where('state_id','=', $state);
                }
                if ($city) {
                    $query->where('city_id','=', $city);
                }
                if ($target) {
                    $query->where('target','=', $target);
                }
                if ($tm_app) {
                    $query->where('tm_app', '=', $tm_app);
                }
                if ($from_date) {
                    $query->where('date','=',[$from_date, $to_date]);
                }
            })->get();
            return view('enforce.search', compact('results','shows','targets','enitys','countries','states','cities','tm_apps','entity','country','state','city','target','tm_app','from_date','to_date'))->with('no',1);
    }

    public function expensesin($id)
    {
    	$show = Invest::find($id);
    	$expens = Expensein::where('invest_id',$id)->get();
    	return view('expensein.add',compact('show','expens'))->with('no',1);
    }

    public function expensave(Request $request)
    {
        $this->validate($request,[
            'invoic' => 'required',
            'amount' => 'required',
            'pay' => 'required',
        ]);

        $expens = new Expensein;

        $expens->invoice_no = $request->invoic;
        $expens->amount = $request->amount;
        if ($request->hasFile('inv_doc'))
        {
            $files= $request->file('inv_doc');
            $destinationPath= 'document';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
            }
            $expens->upload = $has;
        }
        $expens->pay_stat = $request->pay;
        if ($request->pay == 'Paid') {
            $expens->date = date('Y-m-d',strtotime($request->pay_date));
            $expens->remarks = $request->remark;
        }
        $expens->invest_id = $request->invest_id;
        $expens->save();
        return redirect('expensein/'.$request->invest_id.'/expenses')->with('success', 'Invoice Successfully Created!');
    }


    public function EnforceView($id)
    {
        $enforce = Enf::find($id);
        $expens = Expensen::where('enforce_id',$id)->get();
        $firs = Fir::where('fir_id',$id)->get();
        return view('enforce.full',compact('enforce','expens','firs'))->with('no',1);
    }

    public function expensesen($id)
    {
        $show = Enf::find($id);
        $expens = Expensen::where('enforce_id',$id)->get();
        return view('expensen.add',compact('show','expens'))->with('no',1);
    }

    public function expenesave(Request $request)
    {
        $this->validate($request,[
            'invoic' => 'required',
            'amount' => 'required',
            'pay' => 'required',
        ]);

        $expens = new Expensen;

        $expens->invoice_no = $request->invoic;
        $expens->amount = $request->amount;
        if ($request->hasFile('inv_doc'))
        {
            $files= $request->file('inv_doc');
            $destinationPath= 'document';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
            }
            $expens->upload = $has;
        }
        $expens->pay_stat = $request->pay;
        if ($request->pay == 'Paid') {
            $expens->date = date('Y-m-d',strtotime($request->pay_date));
            $expens->remarks = $request->remark;
        }
        $expens->enforce_id = $request->enforce_id;
        $expens->save();
        return redirect('expensen/'.$request->enforce_id.'/expenses')->with('success', 'Invoice Successfully Created!');
    }

    public function expens_edit($id)
    {
        $expn = Expensen::find($id);
        return view('expensen.edit',compact('expn'));
    }

    public function expens_save(Request $request)
    {
        $this->validate($request,[
            'invoic' => 'required',
            'amount' => 'required',
            'pay' => 'required',
        ]);

        $expens = Expensen::find($request->eid);

        $expens->invoice_no = $request->invoic;
        $expens->amount = $request->amount;
        if ($request->hasFile('inv_doc'))
        {
            $files= $request->file('inv_doc');
            $destinationPath= 'document';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
            }
            $expens->upload = $has;
        }
        $expens->pay_stat = $request->pay;
        if ($request->pay == 'Paid') {
            $expens->date = date('Y-m-d',strtotime($request->pay_date));
            $expens->remarks = $request->remark;
        }else{
            $expens->date = null;
            $expens->remarks = null;
        }


        $expens->enforce_id = $request->enforce_id;
        $expens->save();
        return redirect('expensen/'.$request->enforce_id.'/expenses')->with('success', 'Invoice Successfully Edit!');
    }

    public function expeins_edit($id)
    {
        $expin = Expensein::find($id);
        return view('expensein.edit',compact('expin'));
    }

    public function expeins_save(Request $request)
    {
        $this->validate($request,[
            'invoic' => 'required',
            'amount' => 'required',
            'pay' => 'required',
        ]);

        $expens = Expensein::find($request->iid);

        $expens->invest_id = $request->invest_id;
        $expens->invoice_no = $request->invoic;
        $expens->amount = $request->amount;
        if ($request->hasFile('inv_doc'))
        {
            $files= $request->file('inv_doc');
            $destinationPath= 'document';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
            }
            $expens->upload = $has;
        }
        $expens->pay_stat = $request->pay;
        if ($request->pay == 'Paid') {
            $expens->date = date('Y-m-d',strtotime($request->pay_date));
            $expens->remarks = $request->remark;
        }else{
           $expens->date = null;
           $expens->remarks = null;
        }
        $expens->invest_id = $request->invest_id;
        $expens->save();
        return redirect('expensein/'.$request->invest_id.'/expenses')->with('success', 'Invoice Successfully Updated!');
    }



    public function Fir($id)
    {
        $show = Enf::find($id);
        $expens = Fir::where('fir_id',$id)->get();
        return view('fir.add',compact('show','expens'))->with('no',1);
    }

    public function FirSave(Request $request)
    {
        $this->validate($request,[
            'fir_date' => 'required',
            'fir_no' => 'required',
            'police' => 'required',
            'nof' => 'required',
        ]);

        $fir_date = date('Y-m-d',strtotime($request->fir_date));

        $post = new Fir();
        $post->fir_date = $fir_date;
        $post->fir_no = $request->fir_no;
        $post->police = $request->police;
        $post->nof = $request->nof;
        $post->fir_id = $request->fir_id;
        $post->dof = date('Y-m-d',strtotime($fir_date.' +90 days'));
        $post->rem1 =  date('Y-m-d',strtotime($fir_date.' +30 days'));
        $post->rem2 =  date('Y-m-d',strtotime($fir_date.' +60 days'));
        $post->rem3 =  date('Y-m-d',strtotime($fir_date.' +75 days'));
        $post->save();
        return redirect('fir_details/'.$request->fir_id)->with('success', 'FIR Successfully Created!');
    }

    public function fir_edit($id){
        $show = Fir::find($id);
        return view('fir.edit',compact('show'));
    }

    public function update_fir(Request $request,$id){
        $this->validate($request,[
            'fir_date' => 'required',
            'fir_no' => 'required',
            'police' => 'required',
            'nof' => 'required',
        ]);
        $fir_date = date('Y-m-d',strtotime($request->fir_date));

        $post = Fir::find($id);
        $post->fir_date = $fir_date;
        $post->fir_no = $request->fir_no;
        $post->police = $request->police;
        $post->nof = $request->nof;
        $post->dof = date('Y-m-d',strtotime($fir_date.' +90 days'));
        $post->rem1 =  date('Y-m-d',strtotime($fir_date.' +30 days'));
        $post->rem2 =  date('Y-m-d',strtotime($fir_date.' +60 days'));
        $post->rem3 =  date('Y-m-d',strtotime($fir_date.' +75 days'));
        $post->save();
        return redirect('fir_details/'.$post->fir_id)->with('success', 'FIR Successfully Created!');

    }

    public function TargetsShow(){
        $targets = Target::all();
        return view('targets.show', compact('targets'))->with('no',1);
    }

    public function TargetsCreate(){
        return view('targets.create');
    }

    public function TargetsSave(Request $request){
        $this->validate($request,[
                'targets'=> 'required'
            ]);

        $created = Target::create([
            'targets'=>$request->targets
        ]);

        if($created){
            return redirect('/targets/show')->with('success','Category Of Target created successfully');
        }
    }

    public function TargetsEdit($id){
        $targets = Target::find($id);
        return view('targets.edit', compact('targets'));
    }

    public function TargetsUpdate(Request $request){
        $updated = $targets = Target::find($request->id);
                   $targets->targets = $request->targets;
                   $targets->save();

      if($updated){
            return redirect('/targets/show')->with('success', 'Category Of Target Successfully Updated!');
        }
    }

    public function state(){
        $states = State::all();
        return view('states.show', compact('states'));
    }
    public function stateCreate(){
        return view('states.create');
    }

    public function stateSave(Request $request){
        $this->validate($request,[
                'state'=> 'required'
            ]);

        $created = State::create([
            'state'=>$request->state
        ]);

        if($created){
            return redirect('/state/show')->with('success','State created successfully');
        }
    }

    public function city(){
        $states = City::all();
        return view('city.show', compact('states'));
    }
    public function cityCreate(){
        return view('city.create');
    }

    public function citySave(Request $request){
        $this->validate($request,[
                'state'=> 'required'
            ]);

        $created = City::create([
            'state'=>$request->state
        ]);

        if($created){
            return redirect('/state/show')->with('success','City created successfully');
        }
    }

    // Anaylytics

    public function Analytics()
    {

        $total_inv = DB::table('invests')->count();
        $total_enf = DB::table('enfs')->count();

        $all_brand = $total_inv + $total_enf;

        $sum_inv = DB::table('expenseins')->sum('amount');
        $sum_enf = DB::table('expensens')->sum('amount');

        $all_amount = $sum_inv + $sum_enf;

        $sez_val = DB::table('enfs')->sum('amt');

        // Maps
        $enfs = Enf::selectRaw('count(city_id) as count, city_id')->groupBy('city_id')->get();
        $ar1 = array();
        foreach ($enfs as $result) {
            $ar1[] = $result;
        }

        return view('analytic.show', compact('total_inv','total_enf','all_brand','sum_inv','sum_enf','all_amount','sez_val','ar1','enfs'));
    }

    public function modalgeodata($id)
    {
        $name = str_replace("%20", " ", $id);
        $results = Enf::all();
        return view('geo-details',compact('name','results'))->with('no',1);
    }

    // public function GeoChart()
    // {
    //     $enfs = Enforcement::selectRaw('count(city) as count, city')->groupBy('city')->get();
    //     $ar1 = array();
    //     foreach ($enfs as $enf) {
    //         $ar1[$enf->city] = (int)$enf->count;
    //     }

    //     return view('analytic.show',compact('ar1'));
    // }


}
