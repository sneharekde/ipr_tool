<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trademarks;
use Storage;
use File;
use App\Unit;
use App\FunctionList;
use App\DesignationList;
use App\EntityList;
use App\ParentEntity;
use App\EntityMapping;
use App\NatureApplication;
use App\ApplicationField;
use App\NatureApplicant;
use App\NatureAgent;
use App\CategoryMark;
use App\ClassGoodServices;
use App\Language;
use App\Applicant;
use App\Agent;
use App\SubClassGoodsService;
use App\User;
use App\UpdateStatus;
use Illuminate\Support\Facades\Input;
use App\trademark_status;
use App\ActionItem;
use App\Entity;
use App\UpdateStatusTrad;
use App\Product;
use App\TradAgentFee;
use App\TradeClass;
use App\TrademarkGovFees;
use Auth;
use DB;
use PDF;
use Session;


class AddTradmarkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Tradmarksave(Request $request)
    {
        if ($request->has('draft')) {
            $draft = new Trademarks;
            $draft->app_no = $request->app_no;
            $draft->app_date = date('Y-m-d',strtotime($request->app_date));
            $draft->natapl_id = $request->natapl_id;
            $draft->user_id = $request->user_id;
            $draft->application_no = $request->application_no;
            $draft->agent_fee = $request->agent_fee;
            $draft->appf_id = $request->appf_id;
            $draft->app_type_id = $request->app_type_id;
            $draft->gov_fee = $request->gov_fee;
            if($request->appf_id == '3'){
                $draft->name_in = $request->name_in;
            }
            if($request->appf_id == '4' || $request->appf_id == '5' || $request->appf_id == '6'){
                $draft->ent_id = $request->ent_id;
                $draft->nate_id = $request->nate_id;
            }
            $draft->agent_code = $request->agent_code;
            $draft->agent_id = $request->agent_id;
            if ($request->hasFile('poa'))
            {
                $file= $request->file('poa');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->poa = $fullname;
            }
            $draft->trademark = $request->trademark;
            $draft->catmak_id = $request->catmak_id;
            $draft->lang_id = $request->lang_id;
            $draft->desc_mark = $request->desc_mark;
            $draft->remarks = $request->remarks;
            $draft->name_person = $request->name_person;
            $draft->authority = $request->authority;
            $draft->details_use = $request->details_use;
            if($request->details_use == 'Used since'){
                $draft->datea = date('Y-m-d',strtotime($request->datea));
            }else{
                $draft->datea = null;
            }
            if ($request->hasFile('logo'))
            {
                $file= $request->file('logo');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->logo = $fullname;
            }
            if ($request->hasFile('msme_cert'))
            {
                $file= $request->file('msme_cert');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->msme_cert = $fullname;
            }
            if ($request->hasFile('statement_mark'))
            {
                $files= $request->file('statement_mark');
                $destinationPath= 'trademark';
                $images=array();
                foreach($files as $file)
                {
                    $fullname = $file->getClientOriginalName();
                    $hashname = $fullname;
                    $upload_success = $file->move(public_path($destinationPath), $hashname);
                    $images[] = $fullname;
                }
                $draft->statement_mark = implode(",",$images);
            }

            if ($request->hasFile('scan_copy_app'))
            {
                $files= $request->file('scan_copy_app');
                $destinationPath= 'trademark';
                $images=array();
                foreach($files as $file)
                {
                    $fullname = $file->getClientOriginalName();
                    $hashname = $fullname;
                    $upload_success = $file->move(public_path($destinationPath), $hashname);
                    $images[] = $fullname;
                }

                $draft->scan_copy_app = implode(",",$images);
            }
            $draft->submit_status = 'draft';
            $draft->save();
            $draft->trade_class()->sync($request->classg_id,false);
            Session::flash('success','Tradmark created successfully');
            return redirect('/draft');

        } elseif($request->has('submit')) {
            $trad = new Trademarks;
            $trad->app_no = $request->app_no;
            $trad->app_date = date('Y-m-d',strtotime($request->app_date));
            $trad->natapl_id = $request->natapl_id;
            $trad->user_id = $request->user_id;
            $trad->application_no = $request->application_no;
            $trad->agent_fee = $request->agent_fee;
            $trad->appf_id = $request->appf_id;
            $trad->app_type_id = $request->app_type_id;
            $trad->gov_fee = $request->gov_fee;
            if($request->appf_id == '3'){
                $trad->name_in = $request->name_in;
            }
            if($request->appf_id == '4' || $request->appf_id == '5' || $request->appf_id == '6'){
                $trad->ent_id = $request->ent_id;
                $trad->nate_id = $request->nate_id;
            }
            $trad->agent_code = $request->agent_code;
            $trad->agent_id = $request->agent_id;
            if ($request->hasFile('poa'))
            {
                $file= $request->file('poa');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $trad->poa = $fullname;
            }
            $trad->trademark = $request->trademark;
            $trad->catmak_id = $request->catmak_id;
            $trad->lang_id = $request->lang_id;
            $trad->desc_mark = $request->desc_mark;
            $trad->remarks = $request->remarks;
            $trad->name_person = $request->name_person;
            $trad->authority = $request->authority;
            $trad->details_use = $request->details_use;
            if($request->details_use == 'Used since'){
                $trad->datea = date('Y-m-d',strtotime($request->datea));
            }else{
                $trad->datea = null;
            }

            if ($request->hasFile('logo'))
            {
                $file= $request->file('logo');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $trad->logo = $fullname;
            }
            if ($request->hasFile('msme_cert'))
            {
                $file= $request->file('msme_cert');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $trad->msme_cert = $fullname;
            }
            if ($request->hasFile('statement_mark'))
            {
                $files= $request->file('statement_mark');
                $destinationPath= 'trademark';
                $images=array();
                foreach($files as $file)
                {
                    $fullname = $file->getClientOriginalName();
                    $hashname = $fullname;
                    $upload_success = $file->move(public_path($destinationPath), $hashname);
                    $images[] = $fullname;
                }
                $trad->statement_mark = implode(",",$images);
            }

            if ($request->hasFile('scan_copy_app'))
            {
                $files= $request->file('scan_copy_app');
                $destinationPath= 'trademark';
                $images=array();
                foreach($files as $file)
                {
                    $fullname = $file->getClientOriginalName();
                    $hashname = $fullname;
                    $upload_success = $file->move(public_path($destinationPath), $hashname);
                    $images[] = $fullname;
                }

                $trad->scan_copy_app = implode(",",$images);
            }
            $trad->submit_status = 'publish';
            $trad->save();
            $trad->trade_class()->sync($request->classg_id,false);
            $update_status = new UpdateStatusTrad();
            $update_status->trade_id = $trad->id;
            $update_status->date = $trad->app_date;
            $update_status->status = 'Active Application';
            $update_status->sub_status = 'Application Submitted';
            $update_status->save();
            Session::flash('success','Tradmark created successfully');
            return redirect('/tm_portfolio');
        }
    }

    public function edit($id)
    {
        $tradmark = Trademarks::find($id);
        $natureapplications = NatureApplication::all();
        $prods = Product::all();
        $applicationfields = ApplicationField::all();
        $users = User::all();
        $applicants = Applicant::all();
        $natureapplicants = NatureApplicant::all();
        $agents = Agent::all();
        $categorymarks = CategoryMark::all();
        $languages = Language::all();
        $classgoodservicess = ClassGoodservices::all();
        $subclassgood = SubClassGoodsService::all();
        $entitys = Entity::all();
        $classtrads = TradeClass::all();
        $app_typs = TrademarkGovFees::where('appf_id',$tradmark->appf_id)->get();
        $classgs = DB::table('trade_class_trademarks')->where('trademarks_id',$id)->get();
        $class_ar = array();
        foreach($classgs as $classg){
            $class_ar = $classg;
        }
        $ar1 = array();
        $ar2 = array();
        $ar3 = array();
        $ar4 = array();
        $ar5 = array();
        $ar6 = array();
        $ar7 = array();
        $ar8 = array();
        $ar9 = array();
        $ar10 = array();

        foreach ($prods as $prod)
        {
            $ar1[$prod->id] = $prod->name;
        }
        foreach ($natureapplications as $nat)
        {
            $ar2[$nat->id] = $nat->nature_of_application;
        }
        foreach ($users as $user)
        {
            $ar3[$user->id] = $user->first_name." ".$user->last_name;
        }
        foreach ($applicationfields as $appf)
        {
            $ar4[$appf->id] = $appf->application_field;
        }
        foreach ($entitys as $entity)
        {
            $ar5[$entity->id] = $entity->name;
        }
        foreach ($natureapplicants as $natappnt)
        {
            $ar6[$natappnt->id] = $natappnt->nature_of_applicant;
        }
        foreach ($agents as $agent)
        {
            $ar7[$agent->id] = $agent->name;
        }
        foreach ($categorymarks as $catmak)
        {
            $ar8[$catmak->id] = $catmak->category_of_mark;
        }
        foreach ($languages as $lang)
        {
            $ar9[$lang->id] = $lang->language;
        }
        foreach ($classtrads as $class)
        {
            $ar10[$class->id] = $class->name;
        }
        return view('edit_trademark', compact('tradmark','classtrads','class_ar' ,'ar1','ar2','ar3','ar4','ar5','ar6','ar7','ar8','ar9','ar10', 'applicants','app_typs'));
    }

    public function update(Request $request)
    {

        if ($request->has('draft')) {
            $draft = Trademarks::find($request->id);
            $draft->app_no = $request->app_no;
            $draft->app_date = date('Y-m-d',strtotime($request->app_date));
            $draft->natapl_id = $request->natapl_id;
            $draft->user_id = $request->user_id;
            $draft->application_no = $request->application_no;
            $draft->agent_fee = $request->agent_fee;
            $draft->appf_id = $request->appf_id;
            $draft->app_type_id = $request->app_type_id;
            $draft->gov_fee = $request->gov_fee;
            if($request->appf_id == '3'){
                $draft->name_in = $request->name_in;
                $draft->ent_id = null;
                $draft->nate_id = null;
            }
            if($request->appf_id == '4' || $request->appf_id == '5' || $request->appf_id == '6'){
                $draft->ent_id = $request->ent_id;
                $draft->nate_id = $request->nate_id;
                $draft->name_in = null;
            }
            $draft->agent_code = $request->agent_code;
            $draft->agent_id = $request->agent_id;
            if ($request->hasFile('poa'))
            {
                $file= $request->file('poa');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->poa = $fullname;
            }
            $draft->trademark = $request->trademark;
            $draft->catmak_id = $request->catmak_id;
            $draft->lang_id = $request->lang_id;
            $draft->desc_mark = $request->desc_mark;
            $draft->remarks = $request->remarks;
            $draft->name_person = $request->name_person;
            $draft->authority = $request->authority;
            $draft->details_use = $request->details_use;
            if($request->details_use == 'Used since'){
                $draft->datea = date('Y-m-d',strtotime($request->datea));
            }else{
                $draft->datea = null;
            }
            if ($request->hasFile('logo'))
            {
                $file= $request->file('logo');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->logo = $fullname;
            }
            if ($request->hasFile('msme_cert'))
            {
                $file= $request->file('msme_cert');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->msme_cert = $fullname;
            }
            if ($request->hasFile('statement_mark'))
            {
                $files= $request->file('statement_mark');
                $destinationPath= 'trademark';
                $images=array();
                foreach($files as $file)
                {
                    $fullname = $file->getClientOriginalName();
                    $hashname = $fullname;
                    $upload_success = $file->move(public_path($destinationPath), $hashname);
                    $images[] = $fullname;
                }
                $draft->statement_mark = implode(",",$images);
            }

            if ($request->hasFile('scan_copy_app'))
            {
                $files= $request->file('scan_copy_app');
                $destinationPath= 'trademark';
                $images=array();
                foreach($files as $file)
                {
                    $fullname = $file->getClientOriginalName();
                    $hashname = $fullname;
                    $upload_success = $file->move(public_path($destinationPath), $hashname);
                    $images[] = $fullname;
                }

                $draft->scan_copy_app = implode(",",$images);
            }
            $draft->submit_status = 'draft';
            $draft->save();
            $draft->trade_class()->sync($request->classg_id,false);
            Session::flash('success','Tradmark created successfully');
            return redirect('/draft');

        } elseif($request->has('submit')) {
            $trad = Trademarks::find($request->id);
            $trad->app_no = $request->app_no;
            $trad->app_date = date('Y-m-d',strtotime($request->app_date));
            $trad->natapl_id = $request->natapl_id;
            $trad->user_id = $request->user_id;
            $trad->application_no = $request->application_no;
            $trad->agent_fee = $request->agent_fee;
            $trad->appf_id = $request->appf_id;
            $trad->app_type_id = $request->app_type_id;
            $trad->gov_fee = $request->gov_fee;
            if($request->appf_id == '3'){
                $trad->name_in = $request->name_in;
                $trad->ent_id = null;
                $trad->nate_id = null;
            }
            if($request->appf_id == '4' || $request->appf_id == '5' || $request->appf_id == '6'){
                $trad->ent_id = $request->ent_id;
                $trad->nate_id = $request->nate_id;
                $trad->name_in = null;
            }
            $trad->agent_code = $request->agent_code;
            $trad->agent_id = $request->agent_id;
            if ($request->hasFile('poa'))
            {
                $file= $request->file('poa');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $trad->poa = $fullname;
            }
            $trad->trademark = $request->trademark;
            $trad->catmak_id = $request->catmak_id;
            $trad->lang_id = $request->lang_id;
            $trad->desc_mark = $request->desc_mark;
            $trad->remarks = $request->remarks;
            $trad->name_person = $request->name_person;
            $trad->authority = $request->authority;
            $trad->details_use = $request->details_use;
            if($request->details_use == 'Used since'){
                $trad->datea = date('Y-m-d',strtotime($request->datea));
            }else{
                $trad->datea = null;
            }

            if ($request->hasFile('logo'))
            {
                $file= $request->file('logo');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $trad->logo = $fullname;
            }
            if ($request->hasFile('msme_cert'))
            {
                $file= $request->file('msme_cert');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $trad->msme_cert = $fullname;
            }
            if ($request->hasFile('statement_mark'))
            {
                $files= $request->file('statement_mark');
                $destinationPath= 'trademark';
                $images=array();
                foreach($files as $file)
                {
                    $fullname = $file->getClientOriginalName();
                    $hashname = $fullname;
                    $upload_success = $file->move(public_path($destinationPath), $hashname);
                    $images[] = $fullname;
                }
                $trad->statement_mark = implode(",",$images);
            }

            if ($request->hasFile('scan_copy_app'))
            {
                $files= $request->file('scan_copy_app');
                $destinationPath= 'trademark';
                $images=array();
                foreach($files as $file)
                {
                    $fullname = $file->getClientOriginalName();
                    $hashname = $fullname;
                    $upload_success = $file->move(public_path($destinationPath), $hashname);
                    $images[] = $fullname;
                }

                $trad->scan_copy_app = implode(",",$images);
            }
            $trad->submit_status = 'publish';
            $trad->save();
            $trad->trade_class()->sync($request->classg_id,false);
            // check if update status is exist
            $check = UpdateStatusTrad::where('trade_id',$trad->id)->first();
            if($check == ''){
                $update_status = new UpdateStatusTrad();
                $update_status->trade_id = $trad->id;
                $update_status->date = $trad->app_date;
                $update_status->status = 'Active Application';
                $update_status->sub_status = 'Application Submitted';
                $update_status->save();
            }
            Session::flash('success','Tradmark created successfully');
            return redirect('/tm_portfolio');
        }
    }

    public function update_status(Request $request)
    {
        $update_status = Trademarks::find($request->id);
        $update_status->app_status = $request->status;
        $update_status->save();
            return redirect('/tm_portfolio')->with('success', 'Tradmark Successfully Disable!');
    }

    public function ShowUnit()
    {
        $units = Unit::orderBy('id', 'DESC')->get();
        return view('unit.show_unit', compact('units'))->with('no',1);
    }

    public function UnitCreate()
    {
        return view('unit.add_unit');
    }

    public function UnitSave(Request $request)
    {
        $this->validate($request,['unit' => 'required']);
        $created = Unit::create(['unit'=>$request->unit]);
        if($created)
        {
            return redirect('/unit/show')->with('success','Unit created successfully');
        }
    }

    public function UnitEdit($id)
    {
        $unit = Unit::find($id);
        return view('unit.edit', compact('unit'));
    }

    public function UnitUpdate(Request $request)
    {
        $updated = $unit = Unit::find($request->id);
        $unit->unit = $request->unit;
        $unit->save();
        if($updated)
        {
            return redirect('/unit/show')->with('success', 'Unit Successfully Updated!');
        }
    }

    public function ShowFunction()
    {
        $functions = FunctionList::all();
        return view('function.show_function', compact('functions'))->with('no',1);
    }

    public function FunctionCreate()
    {
        return view('function.add_function');
    }

    public function FunctionSave(Request $request)
    {
        $this->validate($request,['function' => 'required']);
        $created = FunctionList::create(['function'=>$request->function]);
        if($created)
        {
            return redirect('/function/show')->with('success','Function created successfully');
        }
    }

    public function FunctionEdit($id)
    {
        $function = FunctionList::find($id);
        return view('function.edit', compact('function'));
    }

    public function FunctionUpdate(Request $request)
    {
        $updated = $function = FunctionList::find($request->id);
        $function->function = $request->function;
        $function->save();
        if($updated)
        {
            return redirect('/function/show')->with('success', 'Function Successfully Updated!');
        }
    }

    public function ShowDesignation()
    {
        $designation = DesignationList::all();
        return view('designation.show_designation', compact('designation'))->with('no',1);
    }

    public function DesignationCreate()
    {
        return view('designation.add_designation');
    }

    public function DesignationSave(Request $request)
    {
        $this->validate($request,['designation' => 'required']);
        $created = DesignationList::create(['designation'=>$request->designation]);
        if($created){
            return redirect('/designation/show')->with('success','Designation created successfully');
        }
    }

    public function DesignationEdit($id)
    {
        $designation = DesignationList::find($id);
        return view('designation.edit', compact('designation'));
    }

    public function DesignationUpdate(Request $request)
    {
        $updated = $designation = DesignationList::find($request->id);
        $designation->designation = $request->designation;
        $designation->save();
        if($updated){
            return redirect('/designation/show')->with('success', 'Designation Successfully Updated!');
        }
    }

    public function entity()
    {
        $entitys = Entity::orderby('id', 'DESC')->get();
        return view('entity.show_entity', compact('entitys'))->with('no',1);
    }

    public function entityCreate()
    {
        $natent = NatureApplicant::all();
        $ar1 = [];
        foreach ($natent as $nat) {
            $ar1[$nat->id] = $nat->nature_of_applicant;
        }
        return view('entity.add_entity', compact('ar1'));
    }

    public function entitySave(Request $request)
    {
        $post = new Entity();
        $post->name = $request->name;
        $post->nat_id = $request->nat_id;
        $post->save();
        Session::flash('success','Entity Added Succesfully!');
        return redirect('entity/show');
    }

    public function entityEdit($id)
    {
        $entity = Entity::find($id);
        $natent = NatureApplicant::all();
        $ar1 = [];
        foreach ($natent as $nat) {
            $ar1[$nat->id] = $nat->nature_of_applicant;
        }
        return view('entity.edit', compact('entity', 'ar1'));
    }

    public function entityUpdate(Request $request,$id)
    {

        $post = Entity::find($id);
        $post->name = $request->name;
        $post->nat_id = $request->nat_id;
        $post->save();
        Session::flash('success','Entity Updated Succesfully!');
        return redirect('entity/show');

    }

    public function entityMapping()
    {
        $entity_mappings = EntityMapping::all();
        return view('entity_mapping.show_mapping', compact('entity_mappings'))->with('no',1);
    }

    public function entityMappingCreate()
    {
        $parent_entityes = EntityList::all();
        $units = Unit::all();
        $functions = FunctionList::all();
        return view('entity_mapping.create', compact('parent_entityes', 'units', 'functions'));
    }

    public function entityMappingSave(Request $request)
    {
        $this->validate($request,[
            'entity'=> 'required|not_in:0',
            'unit' => 'required|not_in:0',
            'function' => 'required|not_in:0'
        ]);

        $entity_mapping = $request->input('function');
        $entity_mapping = implode(',', $entity_mapping);
        $created = EntityMapping::create([
            'entity'=>$request->entity,
            'unit'=>$request->unit,
            'function'=>$entity_mapping
        ]);

        if($created){
            return redirect('/entity_mapping/show')->with('success','Entity Mapping created successfully');
        }
    }

    public function master()
    {
        return view('master');
    }

    public function NatureApplicationShow()
    {
        $natureapps = NatureApplication::all();
        return view('natureapplication.show', compact('natureapps'))->with('no',1);
    }

    public function NatureApplicationCreate()
    {
        return view('natureapplication.create');
    }

    public function NatureApplicationSave(Request $request)
    {
        $this->validate($request,['nature_of_application'=> 'required']);
        $created = NatureApplication::create([
            'nature_of_application'=>$request->nature_of_application
        ]);
        if($created){
            return redirect('/nature_of_the_application/show')->with('success','Nature Of Application created successfully');
        }
    }

    public function NatureApplicationEdit($id)
    {
        $natureapps = NatureApplication::find($id);
        return view('natureapplication.edit', compact('natureapps'));
    }

    public function NatureApplicationUpdate(Request $request)
    {
        $updated = $naturapp = NatureApplication::find($request->id);
        $naturapp->nature_of_application = $request->nature_of_application;
        $naturapp->save();
        if($updated){
            return redirect('/nature_of_the_application/show')->with('success', 'Nature Of Application Successfully Updated!');
        }
    }

    public function FieldApplicationShow()
    {
        $applicationfields = ApplicationField::all();
        return view('application_field.show', compact('applicationfields'))->with('no',1);
    }

    public function FieldApplicationCreate()
    {
        return view('application_field.create');
    }

    public function FieldApplicationSave(Request $request)
    {
        $this->validate($request,['application_field'=> 'required']);
        $created = ApplicationField::create([
            'application_field'=>$request->application_field
        ]);

        if($created){
            return redirect('/application_field/show')->with('success','Application Field created successfully');
        }
    }

    public function FieldApplicationEdit($id)
    {
        $applicationfield = ApplicationField::find($id);
        return view('application_field.edit', compact('applicationfield'));
    }

    public function FieldApplicationUpdate(Request $request)
    {
        $upadted = $fieldapplication = ApplicationField::find($request->id);
        $fieldapplication->application_field = $request->application_field;
        $fieldapplication->save();
        if($upadted){
            return redirect('/application_field/show')->with('success', 'Application Field Successfully Updated!');
        }
    }

    public function NatureApplicantShow()
    {
        $natueapplicants = NatureApplicant::orderBy('id', 'DESC')->get();
        return view('nature_applicant.show', compact('natueapplicants'))->with('no',1);
    }

    public function NatureApplicantCreate()
    {
        return view('nature_applicant.create');
    }

    public function NatureApplicantSave(Request $request)
    {
        $this->validate($request,['nature_of_applicant'=> 'required']);

        $created = NatureApplicant::create([
            'nature_of_applicant'=>$request->nature_of_applicant
        ]);

        if($created){
            return redirect('/nature_applicant/show')->with('success','Nature of Applicant created successfully');
        }
    }

    public function NatureApplicantEdit($id)
    {
        $natueapplicant = NatureApplicant::find($id);
        return view('nature_applicant.edit', compact('natueapplicant'));
    }

    public function NatureApplicantUpdate(Request $request)
    {
        $updated = $natueapplicant = NatureApplicant::find($request->id);
        $natueapplicant->nature_of_applicant = $request->nature_of_applicant;
        $natueapplicant->save();
        if($updated){
            return redirect('/nature_applicant/show')->with('success', 'Nature of Applicant Successfully Updated!');
        }
    }

    public function NatureAgentShow()
    {
        $natureagents = NatureAgent::all();
        return view('nature_agent.show', compact('natureagents'))->with('no',1);
    }

    public function NatureAgentCreate()
    {
        return view('nature_agent.create');
    }

    public function NatureAgentSave(Request $request)
    {
        $this->validate($request,['nature_of_agent'=> 'required']);
        $created = NatureAgent::create(['nature_of_agent'=>$request->nature_of_agent]);
        if($created){
            return redirect('/nature_agent/show')->with('success','Nature of Applicant created successfully');
        }
    }

    public function NatureAgentEdit($id)
    {
        $natureagent = NatureAgent::find($id);
        return view('nature_agent.edit', compact('natureagent'));
    }

    public function NatureAgentUpdate(Request $request,$id)
    {
        $updated = $natureagent = NatureAgent::find($id);
        $natureagent->nature_of_agent = $request->nature_of_agent;
        $natureagent->save();
        if($updated){
            return redirect('/nature_agent/show')->with('success', 'Nature of Applicant Successfully Updated!');
        }
    }

    public function CategoryMarkShow()
    {
        $categorymarks = CategoryMark::all();
        return view('category_mark.show', compact('categorymarks'))->with('no',1);
    }

    public function CategoryMarkCreate()
    {
        return view('category_mark.create');
    }

    public function CategoryMarkSave(Request $request)
    {
        $this->validate($request,['category_of_mark'=> 'required']);
        $created = CategoryMark::create([
            'category_of_mark'=>$request->category_of_mark
        ]);
        if($created){
            return redirect('/category_mark/show')->with('success','Category Of Mark created successfully');
        }
    }

    public function CategoryMarkEdit($id)
    {
        $categorymark = CategoryMark::find($id);
        return view('category_mark.edit', compact('categorymark'));
    }

    public function CategoryMarkUpdate(Request $request)
    {
        $updated = $categorymark = CategoryMark::find($request->id);
        $categorymark->category_of_mark = $request->category_of_mark;
        $categorymark->save();
        if($updated){
            return redirect('/category_mark/show')->with('success', 'Category Of Mark Successfully Updated!');
        }
    }

    public function ClassGoodServicesShow()
    {
        $classgoods = ClassGoodServices::orderBy('id', 'DESC')->get();
        return view('class_good_services.show', compact('classgoods'));
    }

    public function ClassGoodServicesCreate()
    {
        return view('class_good_services.create');
    }

    public function ClassGoodServicesSave(Request $request)
    {
        $this->validate($request,['class_of_good_services'=> 'required']);
        $created = ClassGoodServices::create([
            'class_of_good_services'=>$request->class_of_good_services
        ]);

        if($created){
            return redirect('/class_good_services/show')->with('success','Category Of Mark created successfully');
        }
    }

    public function ClassGoodServicesEdit($id)
    {
        $classgoodservices = ClassGoodServices::find($id);
        return view('class_good_services.edit', compact('classgoodservices'));
    }

    public function ClassGoodServicesUpdate(Request $request)
    {
        $updated = $classgoodservices = ClassGoodServices::find($request->id);
        $classgoodservices->class_of_good_services = $request->class_of_good_services;
        $classgoodservices->save();
        if($updated){
            return redirect('/class_good_services/show')->with('success', 'Category Of Mark Successfully Updated!');
        }
    }

    public function LanguageShow()
    {
        $languages = Language::all();
        return view('language.show', compact('languages'));
    }

    public function LanguageCreate()
    {
        return view('language.create');
    }

    public function LanguageSave(Request $request)
    {
        $this->validate($request,['language'=> 'required']);
        $created = Language::create(['language'=>$request->language]);
        if($created){
            return redirect('/language/show')->with('success','Language Added successfully');
        }
    }

    public function LanguageEdit($id)
    {
        $language = Language::find($id);
        return view('language.edit', compact('language'));
    }

    public function LanguageUpdate(Request $request)
    {
        $updated = $language = Language::find($request->id);
        $language->language = $request->language;
        $language->save();
        if($updated){
            return redirect('/language/show')->with('success', 'Language Successfully Updated!');
        }
    }

    public function ApplicantShow()
    {
        $applicants = Applicant::all();
        return view('applicant.show', compact('applicants'))->with('no',1);
    }

    public function ApplicantCreate()
    {
        $parents_ides = ParentEntity::all();
        $entity = EntityList::all();
        return view('applicant.create', compact('parents_ides','entity'));
    }

    public function ApplicantSave(Request $request)
    {
        $this->validate($request,[
            'entity'=> 'required|not_in:0',
            'address_for_service'=> 'required',
            'trading_as'=> 'required',
            'country' => 'required',
            'state' => 'required',
            'mobile_number' => 'required',
            'applicant_name' => 'required',
            'address' => 'required',
            's_state' => 'required',
            's_country' => 'required',
            'e_mail' => 'required'
        ]);

        $created = Applicant::create($request->all());
        if($created){
            return redirect('/applicant/show')->with('success','Applicant created successfully');
        }
    }

    public function ApplicantEdit($id)
    {
        $parents_ides = ParentEntity::all();
        $applicant = Applicant::find($id);
        return view('applicant.edit', compact('applicant', 'parents_ides'));
    }

    public function ApplicantUpdate(Request $request)
    {
        $updated = $applicant = Applicant::find($request->id);
        $applicant->entity = $request->entity;
        $applicant->address_for_service = $request->address_for_service;
        $applicant->trading_as = $request->trading_as;
        $applicant->country = $request->country;
        $applicant->state = $request->state;
        $applicant->mobile_number = $request->mobile_number;
        $applicant->applicant_name = $request->applicant_name;
        $applicant->address = $request->address;
        $applicant->s_state = $request->s_state;
        $applicant->s_country = $request->s_country;
        $applicant->e_mail = $request->e_mail;
        $applicant->save();

        if($updated){
            return redirect('/applicant/show')->with('success', 'Applicant Successfully Updated!');
        }
    }

    public function AgentShow()
    {
        $agents = Agent::all();
        return view('agent.show', compact('agents'))->with('no',1);
    }

    public function AgentCreate()
    {
        return view('agent.create');
    }

    public function AgentSave(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'address' => 'required',
            'e_mail' => 'required',
            'mobile_number' => 'required',
            'registration_no' => 'required'
        ]);

        $created = Agent::create($request->all());
        if($created){
            return redirect('/agent/show')->with('success','Agent created successfully');
        }
    }

    public function AgentEdit($id)
    {
        $agent = Agent::find($id);
        return view('agent.edit', compact('agent'));
    }

    public function AgentUpdate(Request $request,$id)
    {
        $upadted = $agent = Agent::find($id);
        $agent->name = $request->name;
        $agent->address = $request->address;
        $agent->e_mail = $request->e_mail;
        $agent->mobile_number = $request->mobile_number;
        $agent->registration_no = $request->registration_no;
        $agent->save();
        if($upadted){
            return redirect('/agent/show')->with('success', 'Agent Successfully Updated!');
        }
    }

    public function SubClassShow()
    {
        $subclasss = SubClassGoodsService::orderBy('id', 'DESC')->get();
        return view('sub_class_good_services.show', compact('subclasss'));
    }

    public function SubClassCreate()
    {
        $classgoodservicess = ClassGoodServices::all();
        return view('sub_class_good_services.create', compact('classgoodservicess'));
    }

    public function SubClassSave(Request $request)
    {
        $this->validate($request,[
            'class_good_services_id' => 'required|not_in:0',
            'class' => 'required',
            'description' => 'required'
        ]);
        $created = SubClassGoodsService::create($request->all());
        if($created){
            return redirect('/sub_class_good_services/show')->with('success','Sub Class Goods Service created successfully');
        }
    }

    public function SubClassEdit($id)
    {
        $classgoodservicess = ClassGoodServices::all();
        $subclassgood = SubClassGoodsService::find($id);
        return view('sub_class_good_services.edit', compact('subclassgood','classgoodservicess'));
    }

    public function SubClassUpdate(Request $request)
    {
        $updated = $subclassgood = SubClassGoodsService::find($request->id);
        $subclassgood->class_good_services_id = $request->class_good_services_id;
        $subclassgood->class = $request->class;
        $subclassgood->description = $request->description;
        $subclassgood->save();
        if($updated){
            return redirect('/sub_class_good_services/show')->with('success', 'Sub Class Goods Service Successfully Updated!');
        }
    }

    public function findCityWithStateID($id)
    {
        $city = SubClassGoodsService::where('class_good_services_id',$id)->get();
        return response()->json($city);
    }

    public function editfindCityWithStateID($id)
    {
        $city = SubClassGoodsService::where('class_good_services_id',$id)->get();
        return response()->json($city);
    }

    public function insert_status(Request $request)
    {
        $this->validate($request,array('status' => 'required',));
        $update = new UpdateStatusTrad;
        $update->status = $request->status;
        $update->trade_id = $request->trade_id;
        // Query for Status
        if ($request->status == 'Registered')
        {
            $update->dor_date = date('Y-m-d',strtotime($request->dor));
            $update->comments = $request->comm9;
            if ($request->hasFile('docs7'))
            {
                $file= $request->file('docs7');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }
            $update->ren_date = date('Y-m-d',strtotime($request->dor. ' +3650 days'));
            $update->rem1 = date('Y-m-d',strtotime($request->dor. ' +3470 days'));
            $update->rem2 = date('Y-m-d',strtotime($request->dor. ' +3560 days'));
            $update->rem3 = date('Y-m-d',strtotime($request->dor. ' +3620 days'));
            $update->rem4 = date('Y-m-d',strtotime($request->dor. ' +3643 days'));
            $update->sent_status = '1';

            // Query for previous status change
            $sent_status_1 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Examination report received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Hearing for objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Opposition noticed received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query for previous status change
            $sent_status_4 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Evidence and hearing',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
        }

        if ($request->status == 'Abandoned or rejected')
        {
            $update->sub_status = $request->sub_status;
            $update->date = date('Y-m-d',strtotime($request->date3));
            $update->comments = $request->comm10;
            if ($request->hasFile('docs8'))
            {
                $file= $request->file('docs8');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }


            // Query for previous status change
            $sent_status_1 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Examination report received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Hearing for objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Opposition noticed received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query for previous status change
            $sent_status_4 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Evidence and hearing',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
            // Query for previous status change
            $sent_status_5 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'status' => 'Registered',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_5) {
                $sent_status_5->sent_status = '0';
                $sent_status_5->save();
            }
        }

        if ($request->status == 'Withdrawn')
        {
            $update->sub_status = $request->sub_status;
            $update->date = date('Y-m-d',strtotime($request->date4));
            $update->comments = $request->comm11;
            if ($request->hasFile('docs9'))
            {
                $file= $request->file('docs9');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }

            // Query for previous status change
            $sent_status_1 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Examination report received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Hearing for objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Opposition noticed received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query for previous status change
            $sent_status_4 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Evidence and hearing',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
            // Query for previous status change
            $sent_status_5 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'status' => 'Registered',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_5) {
                $sent_status_5->sent_status = '0';
                $sent_status_5->save();
            }
        }

        // Query for Sub Status
        if ($request->status == 'Active Application' && $request->sub_status == 'Sent to Vienna Codification')
        {
            $update->sub_status = $request->sub_status;
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Marked for exam')
        {
            $update->sub_status = $request->sub_status;
            $update->date = date('Y-m-d',strtotime($request->date1));
            $update->comments = $request->comm1;
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Examination report received')
        {
            $update->sub_status = $request->sub_status;
            $update->rec_date = date('Y-m-d',strtotime($request->rdate));
            $update->comments = $request->comm2;
            if ($request->hasFile('docs1'))
            {
                $file= $request->file('docs1');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }

            //$update->rep_dead = date('Y-m-d',strtotime($request->rdate. ' +30 days'));
            $update->rep_dead = date('Y-m-d',strtotime($request->edeadline));
            $update->rem1 = date('Y-m-d',strtotime($request->rem11));
            $update->rem2 = date('Y-m-d',strtotime($request->rem21));
            $update->rem3 = date('Y-m-d',strtotime($request->rem31));
            $update->sent_status = '1';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Replied to examination report')
        {
            $update->sub_status = $request->sub_status;
            $update->rep_date = date('Y-m-d',strtotime($request->rpdate));
            $update->comments = $request->comm3;
            if ($request->hasFile('docs2'))
            {
                $file= $request->file('docs2');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }
            // Query for previous status change
            $sent_status_1 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Examination report received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Hearing for objection')
        {
            $update->sub_status = $request->sub_status;
            $update->her_date = date('Y-m-d',strtotime($request->dfh));
            $update->comments = $request->comm4;
            if ($request->hasFile('docss'))
            {
                $files= $request->file('docss');
                $destinationPath= 'trademark';
                $images=array();
                foreach($files as $file)
                {
                    $fullname = $file->getClientOriginalName();
                    $hashname = $fullname;
                    $upload_success = $file->move(public_path($destinationPath), $hashname);
                    $images[] = $fullname;
                    $has2 = implode(",",$images);
                }
                $update->docs = $has2;
            }
            $update->rem1 = date('Y-m-d',strtotime($request->rem12));
            $update->rem2 = date('Y-m-d',strtotime($request->rem22));
            $update->rem3 = date('Y-m-d',strtotime($request->rem32));
            $update->sent_status = '1';

            // Query for previous status change
            $sent_status_1 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Examination report received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Accepted')
        {
            $update->sub_status = $request->sub_status;
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Accepted and advertised')
        {
            $update->sub_status = $request->sub_status;
            $update->adv_date = date('Y-m-d',strtotime($request->doa));
            $update->jno = $request->jno;
            $update->comments = $request->comm5;
            if ($request->hasFile('docs3'))
            {
                $file= $request->file('docs3');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }
            $update->opp_dead = date('Y-m-d',strtotime($request->doa. ' +120 days'));

            // Query for previous status change
            $sent_status_1 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Examination report received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Hearing for objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Opposition noticed received')
        {
            $update->sub_status = $request->sub_status;
            $update->not_date = date('Y-m-d',strtotime($request->nod));
            $update->reo_date = date('Y-m-d',strtotime($request->recd));
            $update->comments = $request->comm6;
            if ($request->hasFile('docs4'))
            {
                $file= $request->file('docs4');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }
            $update->cou_dead = date('Y-m-d',strtotime($request->dfcstate));
            $update->int_dead = date('Y-m-d',strtotime($request->recd. ' +53 days'));
            $update->rem1 = date('Y-m-d',strtotime($request->rem13s));
            $update->rem2 = date('Y-m-d',strtotime($request->rem23s));
            $update->rem3 = date('Y-m-d',strtotime($request->rem33s));
            $update->sent_status = '1';

            // Query for previous status change
            $sent_status_1 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Examination report received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Hearing for objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Counter statment submitted')
        {
            $update->sub_status = $request->sub_status;
            $update->date = date('Y-m-d',strtotime($request->date2));
            $update->comments = $request->comm7;
            if ($request->hasFile('docs5'))
            {
                $file= $request->file('docs5');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }

            // Query for previous status change
            $sent_status_1 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Examination report received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Hearing for objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Opposition noticed received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Evidence and hearing')
        {
            $update->sub_status = $request->sub_status;
            $update->evi_date = date('Y-m-d',strtotime($request->devd));
            $update->comments = $request->comm8;
            if ($request->hasFile('docs6'))
            {
                $file= $request->file('docs6');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }
            $update->rem1 = date('Y-m-d',strtotime($request->rem13));
            $update->rem2 = date('Y-m-d',strtotime($request->rem23));
            $update->rem3 = date('Y-m-d',strtotime($request->rem33));
            $update->sent_status = '1';

            // Query for previous status change
            $sent_status_1 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Examination report received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Hearing for objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Opposition noticed received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Decision pending')
        {
            $update->sub_status = $request->sub_status;
            $update->comments = $request->comm12;

            // Query for previous status change
            $sent_status_1 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Examination report received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            $sent_status_2 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Hearing for objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            $sent_status_3 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Opposition noticed received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query for previous status change
            $sent_status_4 = UpdateStatusTrad::where([
                'trade_id' => $request->trade_id,
                'sub_status' => 'Evidence and hearing',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
        }

        // Updte on Trademark Table
        $update_stat = Trademarks::find($request->trade_id);
        // Update Status and Priority status if only Status is upload
        if ($request->status == 'Registered') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '2';
            $update_stat->ssp = '';
        }

        if ($request->status == 'Abandoned or rejected') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '3';
            $update_stat->ssp = '';
        }

        if ($request->status == 'Withdrawn') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '4';
            $update_stat->ssp = '';
        }

        // Update status,sub status,and Priototy
        if ($request->status == 'Active Application' && $request->sub_status == 'Sent to Vienna Codification')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '2';
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Marked for exam')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '3';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Examination report received')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '4';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Replied to examination report')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '5';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Hearing for objection')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '6';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Accepted')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '7';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Accepted and advertised')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '8';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Opposition noticed received')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '9';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Counter statment submitted')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '10';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Evidence and hearing')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '11';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Decision pending')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '12';
        }

        $update->save();
        $update_stat->save();
        return redirect('add_tradmark/'.$request->trade_id.'/update_status')->with('success','Status created successfully');
    }

  public function update_insert_status(Request $request)
    {
        $this->validate($request,array('status' => 'required',));
        $update = UpdateStatusTrad::find($request->status_id);
        $trad_id = $update->trade_id;
        $update->status = $request->status;
        // Query for Status
        if ($request->status == 'Registered')
        {
            $update->dor_date = date('Y-m-d',strtotime($request->dor));
            $update->comments = $request->comm9;
            if ($request->hasFile('docs7'))
            {
                $file= $request->file('docs7');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }
            $update->ren_date = date('Y-m-d',strtotime($request->dor. ' +3650 days'));
            $update->rem1 = date('Y-m-d',strtotime($request->dor. ' +3470 days'));
            $update->rem2 = date('Y-m-d',strtotime($request->dor. ' +3560 days'));
            $update->rem3 = date('Y-m-d',strtotime($request->dor. ' +3620 days'));
            $update->rem4 = date('Y-m-d',strtotime($request->dor. ' +3643 days'));
            $update->sent_status = '1';

        }
        if ($request->status == 'Abandoned or rejected')
        {
            $update->sub_status = $request->sub_status;
            $update->date = date('Y-m-d',strtotime($request->date3));
            $update->comments = $request->comm10;
            if ($request->hasFile('docs8'))
            {
                $file= $request->file('docs8');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }
        }

        if ($request->status == 'Withdrawn')
        {
            $update->sub_status = $request->sub_status;
            $update->date = date('Y-m-d',strtotime($request->date4));
            $update->comments = $request->comm11;
            if ($request->hasFile('docs9'))
            {
                $file= $request->file('docs9');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }
        }
        // Query for Sub Status
        if ($request->status == 'Active Application' && $request->sub_status == 'Sent to Vienna Codification')
        {
            $update->sub_status = $request->sub_status;
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Marked for exam')
        {
            $update->sub_status = $request->sub_status;
            $update->date = date('Y-m-d',strtotime($request->date1));
            $update->comments = $request->comm1;
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Examination report received')
        {
            $update->sub_status = $request->sub_status;
            $update->rec_date = date('Y-m-d',strtotime($request->rdate));
            $update->comments = $request->comm2;
            if ($request->hasFile('docs1'))
            {
                $file= $request->file('docs1');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }
            ///$update->rep_dead = date('Y-m-d',strtotime($request->rdate. ' +30 days'));
            $update->rep_dead = date('Y-m-d',strtotime($request->edeadline));
            $update->rem1 = date('Y-m-d',strtotime($request->rem11));
            $update->rem2 = date('Y-m-d',strtotime($request->rem21));
            $update->rem3 = date('Y-m-d',strtotime($request->rem31));
            $update->sent_status = '1';
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Replied to examination report')
        {
            $update->sub_status = $request->sub_status;
            $update->rep_date = date('Y-m-d',strtotime($request->rpdate));
            $update->comments = $request->comm3;
            if ($request->hasFile('docs2'))
            {
                $file= $request->file('docs2');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Hearing for objection')
        {
            $update->sub_status = $request->sub_status;
            $update->her_date = date('Y-m-d',strtotime($request->dfh));
            $update->comments = $request->comm4;
            if ($request->hasFile('docss'))
            {
                $files= $request->file('docss');
                $destinationPath= 'trademark';
                $images=array();
                foreach($files as $file)
                {
                    $fullname = $file->getClientOriginalName();
                    $hashname = $fullname;
                    $upload_success = $file->move(public_path($destinationPath), $hashname);
                    $images[] = $fullname;
                    $has2 = implode(",",$images);
                }
                $update->docs = $has2;
            }
            $update->rem1 = date('Y-m-d',strtotime($request->rem12));
            $update->rem2 = date('Y-m-d',strtotime($request->rem22));
            $update->rem3 = date('Y-m-d',strtotime($request->rem32));
            $update->sent_status = '1';
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Accepted')
        {
            $update->sub_status = $request->sub_status;
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Accepted and advertised')
        {
            $update->sub_status = $request->sub_status;
            $update->adv_date = date('Y-m-d',strtotime($request->doa));
            $update->comments = $request->comm5;
            $update->jno = $request->jno;
            if ($request->hasFile('docs3'))
            {
                $file= $request->file('docs3');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }
            $update->opp_dead = date('Y-m-d',strtotime($request->doa. ' +120 days'));
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Opposition noticed received')
        {
            $update->sub_status = $request->sub_status;
            $update->not_date = date('Y-m-d',strtotime($request->nod));
            $update->reo_date = date('Y-m-d',strtotime($request->recd));
            $update->comments = $request->comm6;
            if ($request->hasFile('docs4'))
            {
                $file= $request->file('docs4');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }
            $update->cou_dead = date('Y-m-d',strtotime($request->dfcstate));
            $update->int_dead = date('Y-m-d',strtotime($request->recd. ' +53 days'));
            $update->rem1 = date('Y-m-d',strtotime($request->rem13s));
            $update->rem2 = date('Y-m-d',strtotime($request->rem23s));
            $update->rem3 = date('Y-m-d',strtotime($request->rem33s));

            $update->sent_status = '1';
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Counter statment submitted')
        {
            $update->sub_status = $request->sub_status;
            $update->date = date('Y-m-d',strtotime($request->date2));
            $update->comments = $request->comm7;
            if ($request->hasFile('docs5'))
            {
                $file= $request->file('docs5');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Evidence and hearing')
        {
            $update->sub_status = $request->sub_status;
            $update->evi_date = date('Y-m-d',strtotime($request->devd));
            $update->comments = $request->comm8;
            if ($request->hasFile('docs6'))
            {
                $file= $request->file('docs6');
                $destinationPath= 'trademark';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update->docs = $fullname;
            }
            $update->rem1 = date('Y-m-d',strtotime($request->rem13));
            $update->rem2 = date('Y-m-d',strtotime($request->rem23));
            $update->rem3 = date('Y-m-d',strtotime($request->rem33));
            $update->sent_status = '1';
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Decision pending')
        {
            $update->sub_status = $request->sub_status;
            $update->comments = $request->comm12;
        }
        $update->save();
        return redirect('add_tradmark/'.$trad_id.'/update_status')->with('success','Status Updated successfully');
    }

  public function search(Request $request)
  {
    $natapl = $request->natapl;
    $nate = $request->nate;
    $appf = $request->appf;
    $catmak = $request->catmak;
    $applname = $request->applname;
    $agant = $request->agant;
    $ent = $request->ent;
    $status = $request->status;
    $classs = $request->class;
    $prod = $request->prod;
    $agec = $request->agec;
    $from = $request->from;
    $to = $request->to;




    $showes = Trademarks::orderBy('id', 'DESC')->get();
    $trademark_statuss = Trademarks::selectRaw('status')->groupBy('status')->get();
    $prods = Trademarks::selectRaw('prod_id')->groupBy('prod_id')->get();
    $ages = Trademarks::selectRaw('agent_code')->groupBy('agent_code')->get();
    $natapls = Trademarks::selectRaw('natapl_id')->groupBy('natapl_id')->get();
    $nates = Trademarks::selectRaw('nate_id')->groupBy('nate_id')->get();
    $nateps = Trademarks::selectRaw('appf_id')->groupBy('appf_id')->get();
    $catmaks = Trademarks::selectRaw('catmak_id')->groupBy('catmak_id')->get();
    $applcs = Trademarks::selectRaw('name_in')->groupBy('name_in')->get();
    $agents = Trademarks::selectRaw('agent_id')->groupBy('agent_id')->get();
    $ents = Trademarks::selectRaw('ent_id')->groupBy('ent_id')->get();
    $stats = Trademarks::selectRaw('status')->groupBy('status')->get();

    $results = Trademarks::where(function ($query) use ($natapl,$nate,$appf,$catmak,$applname,$agant,$ent,$status,$agec,$from, $to)
    {
        if($natapl)
        {
            $query->where('natapl_id', '=', $natapl);
        }
        if($nate)
        {
            $query->where('nate_id', '=', $nate);
        }
        if($appf)
        {
            $query->where('appf_id', '=', $appf);
        }
        if($catmak)
        {
            $query->where('catmak_id', '=', $catmak);
        }
        if($applname)
        {
            $query->where('name_in', '=', $applname);
        }
        if($agant)
        {
            $query->where('agent_id', '=', $agant);
        }
        if($ent){
            $query->where('ent_id','=',$ent);
        }
        if($status){
            $query->where('status','=',$status);
        }
        if($agec){
            $query->where('agent_code','=',$agec);
        }
        if($from)
        {
            $query->whereBetween('app_date', [$from, $to]);
        }
    })->get();


    return view('tradmark_search', compact('results', 'showes','trademark_statuss','ages','prods','natapls','nates','nateps','catmaks','applcs','agents','ents','stats','natapl','nate','appf','catmak','applname','agant','ent','status','classs','prod','agec','from','to'))->with('no',1);
}

    public function tm_reports(){
        $showes = Trademarks::orderBy('id', 'DESC')->get();
        $appfs = Trademarks::selectRaw('appf_id')->groupBy('appf_id')->get();
        $agents = Trademarks::selectRaw('agent_id')->groupBy('agent_id')->get();
        $statuss = Trademarks::selectRaw('status')->groupBy('status')->get();
        $namein = Trademarks::selectRaw('name_in')->groupBy('name_in')->get();
        return view('tm_reports', compact('showes','appfs','agents','statuss','namein'))->with('no',1);
    }

    public function tm_reports_search(Request $request){
         $showes = Trademarks::orderBy('id', 'DESC')->get();
         $appfs = Trademarks::selectRaw('appf_id')->groupBy('appf_id')->get();
         $agents = Trademarks::selectRaw('agent_id')->groupBy('agent_id')->get();
         $statuss = Trademarks::selectRaw('status')->groupBy('status')->get();
         $namein = Trademarks::selectRaw('name_in')->groupBy('name_in')->get();



        $from_date = $request->form;
        $to_date = $request->to;
        $appfss = $request->appfss;
        $tstate = $request->status;
        $agent_name = $request->agent_name;
        $applcn = $request->applcn;
        $application_date = $request->application_date;
        $results = Trademarks::where(function ($query) use ($from_date, $to_date, $appfss, $applcn, $agent_name,$tstate,$application_date) {
                if($from_date) {
                    $query->whereBetween('app_date', [$from_date, $to_date]);
                }
                if($appfss){
                    $query->where('appf_id','=',$appfss);
                }

                if($applcn) {

                    $query->where('name_in','=', $applcn);
                }
                if($agent_name) {

                    $query->where('agent_id', '=', $agent_name);
                }
                  if($tstate) {

                    $query->where('status','=',$tstate);
                }
                if($application_date){
                    $query->where('app_date', '=', $application_date);
                }

            })->get();

            //dd($results);


        return view('tm_reports', compact('showes','results','appfs','agents','statuss','namein'))->with('no',1);
    }


    public function show_tradmark($id){
        $class = ClassGoodServices::find($id);
        $show = Trademarks::find($id);
        $statues = UpdateStatusTrad::where('trade_id', $id)->orderBy('id','DESC')->get();
        $posts = TradAgentFee::where('trade_id',$id)->orderBy('id','DESC')->get();
        return view('show_tradmark', compact('show', 'class', 'statues','posts'))->with('no',1);
    }

    public function support(){
        return view('support');
    }

    public function dataShow($id){
        return view('modal-data');
    }



    public function actionitem($id){
        $show = Trademarks::find($id);
        $items = ActionItem::where('tradmark_id', $id)->get();
        return view('action_item.add', compact('show', 'items'));
    }

    public function actionitemsave(Request $request){
        $this->validate($request,[
                'license' => 'required',
                'license_of_sub' => 'required',
                'term_of_license' => 'required',
                'expiry_date' => 'required',
                'license_document'=>'required',
                'reminder_two'=>'required',
                'license_document'=>'required'
            ]);
         if($request->hasFile('license_document') && $request->license_document->isvalid()){
                $file = $request->license_document->getClientOriginalName(); //Get Image Name
                $file_license = $file;
                $request->license_document->move(public_path('license'),$file_license);
             }
             else{
                $file_license = "no-image.png";
             }


         $created = ActionItem::create([
            'license'=>$request->license,
            'license_of_sub'=>$request->license_of_sub,
            'term_of_license'=>$request->term_of_license,
            'expiry_date'=>$request->expiry_date,
            'license_document'=>$file_license,
            'reminder_two'=>$request->reminder_two,
            'tradmark_id'=>$request->tradmark_id
         ]);


       if($created){
             return redirect('/tm_portfolio')->with('success', 'License Successfully Created!');
        }
    }

    public function actionitemedit($id){
        $item = ActionItem::find($id);
        return view('action_item.edit', compact('item'));
    }

    public function actionitemupdate(Request $request){
        $updated = $item = ActionItem::find($request->id);
         if ($request->hasfile('license_document') && $request->license_document->isvalid()) {
                $file = $request->license_document->getClientOriginalName(); //Get Image Name
                $file_license = $file;
                $item->license_document = $file_license;
                $request->license_document->move(public_path('license'), $file_license);
                Storage::delete('/public/license/'.$item->file_license);

        }
        else{
            $file_license = 'no-image.png';
        }

                   $item->license = $request->license;
                   $item->license_of_sub = $request->license_of_sub;
                   $item->term_of_license = $request->term_of_license;
                   $item->expiry_date = $request->expiry_date;
                   $item->reminder_two = $request->reminder_two;
                   $item->save();

       if($updated){
        return redirect('tm_portfolio')->with('success', 'License Successfully Updated!');
       }
    }



    public function printPDF(){
        $show = Trademarks::all();
        $pdf = PDF::loadview('show_tradmark', $show);
        return $pdf->download('trademark_reports');
    }

    public function eventsCalendar(){
        $events = ActionItem::all();
        return view('events.index', compact('events'));
    }

    public function viewall(){
        $showes = Trademarks::orderBy('id', 'DESC')->get();
       $natureapplications = NatureApplication::all();
        $applicationfields = ApplicationField::all();
        $users = User::all();
        $applicants = Applicant::all();
        $natureapplicants = NatureApplicant::all();
        $agents = Agent::all();
        $categorymarks = CategoryMark::all();
        $languages = Language::all();
        $classgoodservicess = ClassGoodServices::all();
        $subclassgood = SubClassGoodsService::all();
        $entitys = EntityList::all();
        $trademark_statuss = trademark_status::all();
        return view('view-all', compact('showes','natureapplications', 'applicationfields', 'users', 'applicants', 'natureapplicants', 'agents', 'categorymarks', 'languages', 'classgoodservicess', 'subclassgood', 'entitys', 'trademark_statuss'));
    }

    public function tradmark_search(Request $request){
    $nature_of_application = $request->nature_of_application;
    $nature_of_application_search = $request->nature_of_application;
    $nature_of_applicant = $request->nature_of_applicant;
    $nature_of_applicant_search = $request->nature_of_applicant;
    $application_field = $request->application_field;
    $categorymark = $request->category_of_mark;
    $applicant_name = $request->applicant_name;
    $language = $request->language;
    $name_person = $request->person_name;
    $trademark_status = $request->tradmark_status;
    $from_date = $request->form;
    $to_date = $request->to;
    $agent_name = $request->agent_name;



    $showes = Trademarks::orderBy('id', 'DESC')->get();
       $natureapplications = NatureApplication::all();
        $applicationfields = ApplicationField::all();
        $users = User::all();
        $applicants = Applicant::all();
        $natureapplicants = NatureApplicant::all();
        $agents = Agent::all();
        $categorymarks = CategoryMark::all();
        $languages = Language::all();
        $classgoodservicess = ClassGoodServices::all();
        $subclassgood = SubClassGoodsService::all();
        $entitys = EntityList::all();
        $trademark_statuss = trademark_status::all();



            $results = DB::table('trademarks')
            ->where(function ($query) use ($nature_of_application, $nature_of_applicant, $application_field, $categorymark, $applicant_name, $agent_name, $trademark_status, $from_date, $to_date) {
                if($nature_of_application) {

                $query->where('series_marks', '=', $nature_of_application);
                }
            if($nature_of_applicant) {
                      $query->where('nature_of_app', '=', $nature_of_applicant);
            }
                  if($application_field) {

                      $query->where('app_fiel', '=', $application_field);
                  }
                  if($categorymark) {

                      $query->where('category_of_mark', '=', $categorymark);
                  }
                  if($applicant_name) {

                      $query->where('appicatn_name', '=', $applicant_name);
                  }
                   if($from_date) {

                  $query->whereBetween('app_date', [$from_date, $to_date]);
                }
                  if($agent_name) {

                      $query->where('agent_name', '=', $agent_name);
                  }
                  if($trademark_status) {

                      $query->where('update_status', '=', $trademark_status);
                  }

            })
            ->get();


        return view('tradmark_search', compact('results', 'showes','natureapplications', 'applicationfields', 'users', 'applicants', 'natureapplicants', 'agents', 'categorymarks', 'languages', 'classgoodservicess', 'subclassgood', 'entitys', 'trademark_statuss'));
    }


    // public function show_license(){
    //     $showes = Trademarks::all();
    //     return view('license_all', compact('showes'));
    // }

    // public function edit_license($id){
    //     $show = Trademarks::find($id);
    //     return view('edit_license', compact('show'));
    // }

    public function update_license(Request $request){
        $updated = $tradmark = Trademarks::find($request->id);

         if ($request->hasfile('license') && $request->license->isvalid()) {
                $file = $request->license->getClientOriginalName(); //Get Image Name
                $file_license = $file;
                $tradmark->license = $file_license;
                $request->license->move(public_path('license'), $file_license);
                Storage::delete('/public/license/'.$tradmark->file_license);

        }
        else{
            $file_license = 'no-image.png';
        }

        $tradmark->save();
        if($updated){
            return redirect('/show_license')->with('success', 'License Successfully Updated!');
        }
    }

}
