<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Design;
use App\Update_Status_Design;
use App\UpdataStatusDesign;
use App\User;
use App\Language;
use App\DesignationList;
use App\Designm;
use Session;
use Storage;
use Auth;
use DB;
use PDF;

class Update_Status_Design_Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function status_create($id)
    {
    	$design = Designm::find($id);
        $statuss = UpdataStatusDesign::where('design_id', $id)->orderBy('id', 'desc')->get();
    	return view('design.update', compact('design','statuss'))->with('no',1);
    }

    public function submit(Request $request)
 	{
        $this->validate($request,array(
            'status' => 'required'
        ));

        $update_design = new UpdataStatusDesign;
        $update_design->status = $request->status;
        $update_design->design_id = $request->des_id;

        // Query for Status
        if ($request->status == 'Abandoned / Rejected') {
            // Query for previous status change
            $sent_status_1 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Examination Report Received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Hearing for Objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Removal of Objection Period Extended',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
        }
        if ($request->status == 'Registered and Awaiting Publication') {
            $update_design->dor = date('Y-m-d',strtotime($request->dor));
            if ($request->hasFile('cor'))
            {
                $file= $request->file('cor');
                $destinationPath= 'copyright/design';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update_design->docs = $fullname;
            }
            $update_design->comment = $request->comment1;
            $update_design->d_renr = date('Y-m-d',strtotime($request->dor. ' +3650 days'));
            $update_design->rem1 = date('Y-m-d',strtotime($request->dor. ' +3470 days'));
            $update_design->rem2 = date('Y-m-d',strtotime($request->dor. ' +3560 days'));
            $update_design->rem3 = date('Y-m-d',strtotime($request->dor. ' +3620 days'));
            $update_design->rem4 = date('Y-m-d',strtotime($request->dor. ' +3635 days'));
            $update_design->sent_status = '1';
            // Query for previous status change
            $sent_status_1 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Examination Report Received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Hearing for Objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Removal of Objection Period Extended',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
        }

        if ($request->status == 'Registered and Published') {
            $update_design->pub_date = date('Y-m-d',strtotime($request->date1));
            $update_design->jrno = $request->jno;

            // Query for previous status change
            $sent_status_1 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Examination Report Received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Hearing for Objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Removal of Objection Period Extended',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query change for previous state change
            $sent_status_4 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'status' => 'Registered and Awaiting Publication',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
        }

        if ($request->status == 'Expired - For Renewal') {
            // Query for previous status change
            $sent_status_1 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Examination Report Received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Hearing for Objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Removal of Objection Period Extended',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query change for previous state change
            $sent_status_4 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'status' => 'Registered and Awaiting Publication',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
        }

        if ($request->status == 'Renewed') {
            $update_design->ren_date = date('Y-m-d',strtotime($request->dorenew));
            $update_design->exp_date = date('Y-m-d',strtotime($request->doe));
            // Query for previous status change
            $sent_status_1 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Examination Report Received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Hearing for Objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Removal of Objection Period Extended',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query change for previous state change
            $sent_status_4 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'status' => 'Registered and Awaiting Publication',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
        }

        if ($request->status == 'Extension Expired') {
            // Query for previous status change
            $sent_status_1 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Examination Report Received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Hearing for Objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
            // Query for previous status change
            $sent_status_3 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Removal of Objection Period Extended',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_3) {
                $sent_status_3->sent_status = '0';
                $sent_status_3->save();
            }
            // Query change for previous state change
            $sent_status_4 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'status' => 'Registered and Awaiting Publication',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_4) {
                $sent_status_4->sent_status = '0';
                $sent_status_4->save();
            }
        }

        // Query for Sub Status
        if ($request->status == 'Active Application' && $request->sub_status == 'Examination Report Received')
        {
            $update_design->sub_status = $request->sub_status;
            $update_design->derrer = date('Y-m-d',strtotime($request->date2));
            $update_design->comment = $request->comment2;
            if ($request->hasFile('docs1'))
            {
                $file= $request->file('docs1');
                $destinationPath= 'copyright/design';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update_design->docs = $fullname;
            }
            $update_design->rep_dead = date('Y-m-d',strtotime($request->derrer. ' +90 days'));
            $update_design->rem1 = date('Y-m-d',strtotime($request->rem11));
            $update_design->rem2 = date('Y-m-d',strtotime($request->rem21));
            $update_design->rem3 = date('Y-m-d',strtotime($request->rem31));
            $update_design->sent_status = '1';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Reply to objection submitted')
        {
            $update_design->sub_status = $request->sub_status;
            $update_design->date = date('Y-m-d',strtotime($request->date3));
            $update_design->comment = $request->comment3;
            if ($request->hasFile('docs2'))
            {
                $file= $request->file('docs2');
                $destinationPath= 'copyright/design';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update_design->docs = $fullname;
            }
            // Query for previous status change
            $sent_status_1 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Examination Report Received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Hearing for Objection')
        {
            $update_design->sub_status = $request->sub_status;
            $update_design->dfh = date('Y-m-d',strtotime($request->her_date));
            $update_design->comment = $request->comment4;
            if ($request->hasFile('docs3'))
            {
                $file= $request->file('docs3');
                $destinationPath= 'copyright/design';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update_design->docs = $fullname;
            }
            $update_design->rem1 = date('Y-m-d',strtotime($request->rem12));
            $update_design->rem2 = date('Y-m-d',strtotime($request->rem22));
            $update_design->rem3 = date('Y-m-d',strtotime($request->rem32));
            $update_design->sent_status = '1';

            // find Date of filing
            $dead = Designm::find($request->des_id);
            $filing_date = $dead->filing_date;
            $update_design->dfrem = date('Y-m-d',strtotime($filing_date. ' +180 days'));
            // Query for previous status change
            $sent_status_1 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Examination Report Received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Removal of Objection Period Extended')
        {
            $update_design->sub_status = $request->sub_status;
            $update_design->comment = $request->comment5;
            if ($request->hasFile('docs4'))
            {
                $file= $request->file('docs4');
                $destinationPath= 'copyright/design';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update_design->docs = $fullname;
            }
            $update_design->rem1 = date('Y-m-d',strtotime($request->rem13));
            $update_design->rem2 = date('Y-m-d',strtotime($request->rem23));
            $update_design->rem3 = date('Y-m-d',strtotime($request->rem33));
            $update_design->sent_status = '1';

            // find Date of filing
            $dead = Designm::find($request->des_id);
            $filing_date = $dead->filing_date;
            $update_design->dfrem = date('Y-m-d',strtotime($filing_date. ' +270 days'));
            // Query for previous status change
            $sent_status_1 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Examination Report Received',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_1) {
                $sent_status_1->sent_status = '0';
                $sent_status_1->save();
            }
            // Query for previous status change
            $sent_status_2 = UpdataStatusDesign::where([
                'design_id' => $request->des_id,
                'sub_status' => 'Hearing For Objection',
                'sent_status' => '1'
            ])->first();
            if ($sent_status_2) {
                $sent_status_2->sent_status = '0';
                $sent_status_2->save();
            }
        }

        // update on Design Table
        $update_stat = Designm::find($request->des_id);
        // Update Status and Priority status if only Status is upload
        if ($request->status == 'Abandoned / Rejected') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '2';
            $update_stat->ssp = '';
        }
        if ($request->status == 'Registered and Awaiting Publication') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '3';
            $update_stat->ssp = '';
            $update_stat->dor = date('Y-m-d',strtotime($request->dor));
            if ($request->hasFile('cor'))
            {
                $file= $request->file('cor');
                $fullname = $file->getClientOriginalName();
                $update_stat->cert_reg = $fullname;
            }

        }
        if ($request->status == 'Registered and Published') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '4';
            $update_stat->ssp = '';
        }
        if ($request->status == 'Expired - For Renewal') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '5';
            $update_stat->ssp = '';
        }
        if ($request->status == 'Renewed') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '6';
            $update_stat->ssp = '';
        }
        if ($request->status == 'Extension Expired') {
            $update_stat->status = $request->status;
            $update_stat->sub_status = '';
            $update_stat->sp = '7';
            $update_stat->ssp = '';
        }

        // Update status,sub status,and Priototy
        if ($request->status == 'Active Application' && $request->sub_status == 'Examination Report Received')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '2';
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Reply to objection submitted')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '3';
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Hearing for Objection')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '4';
        }
        if ($request->status == 'Active Application' && $request->sub_status == 'Removal of Objection Period Extended')
        {
            $update_stat->status = $request->status;
            $update_stat->sub_status = $request->sub_status;
            $update_stat->sp = '1';
            $update_stat->ssp = '5';
        }
        $update_design->save();
        $update_stat->save();
        Session::flash('success','Status Updated Successfully!');
        return redirect('design/'.$request->des_id.'/update_status');

    }


}
