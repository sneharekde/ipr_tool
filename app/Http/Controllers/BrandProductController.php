<?php

namespace App\Http\Controllers;

use App\BrandProduct;
use App\Entity;
use Illuminate\Http\Request;
use Auth;
use Session;
class BrandProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $posts = BrandProduct::orderBy('id','DESC')->get();
        return view('brandproduct.index',compact('posts'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ents = Entity::all();
        $ar1 = [];
        foreach ($ents as $ent) {
            $ar1[$ent->id] = $ent->name;
        }
        return view('brandproduct.create',compact('ar1'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request ,array(
            'name' => 'required |unique:brand_products,name'
        ));

        $post = new BrandProduct();
        $post->name = $request->name;
        $post->ent_id = $request->ent_id;
        $post->save();
        Session::flash('success','Product Added!');
        return redirect()->route('brand-product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = BrandProduct::find($id);
        $ents = Entity::all();
        $ar1 = [];
        foreach ($ents as $ent) {
            $ar1[$ent->id] = $ent->name;
        }
        return view('brandproduct.edit',compact('post','ar1'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request ,array(
            'name' => 'required |unique:brand_products,name,'.$id,
        ));

        $post = BrandProduct::find($id);
        $post->name = $request->name;
        $post->ent_id = $request->ent_id;
        $post->save();
        Session::flash('success','Product Updated!');
        return redirect()->route('brand-product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
