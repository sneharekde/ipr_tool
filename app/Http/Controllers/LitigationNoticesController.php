<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NoticeStatus;
use App\LitigationNotice;
use App\LitigationSummary;
Use App\EntityList;
use App\Unit;
use App\FunctionList;
use App\LitiType;
use App\User;
use App\ExternalCounsel;
use App\Currency;
use App\LawFirm;
use App\Advocate;
use App\CourtList;
use Storage;
use DB;
use Auth;
use Illuminate\Support\Facades\Storage as FacadesStorage;
use Session;

class LitigationNoticesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $notices = LitigationNotice::orderBy('id', 'DESC')->get();
        $ents = DB::table('litigation_notices')->select('entity')->groupBy('entity')->get();
        $locs = DB::table('litigation_notices')->select('Location')->groupBy('Location')->get();
        $deps = DB::table('litigation_notices')->select('department')->groupBy('department')->get();
        $bys = DB::table('litigation_notices')->select('by_ag')->groupBy('by_ag')->get();
        $cats = DB::table('litigation_notices')->select('category')->groupBy('category')->get();
        $cnps = DB::table('litigation_notices')->select('int_per')->groupBy('int_per')->get();
        return view('notice.index',compact('notices','ents','locs','deps','bys','cats','cnps'))->with('no', 1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $entities = EntityList::all();
        $units = Unit::all();
        $functions = FunctionList::all();
        $categories = LitiType::all();
        $users = User::where('role','!=','Brand Protection Manager')->get();
        $exts = ExternalCounsel::all();
        $currs = Currency::all();
        $entity = array();
        $uni = array();
        $func = array();
        $cat = array();
        $use = array();
        $ext = array();
        $cur = array();
        foreach ($entities as $ent) {
            $entity[$ent->entity_child] = $ent->entity_child;
        }
        foreach ($units as $unit) {
            $uni[$unit->unit] = $unit->unit;
        }
        foreach ($functions as $function) {
            $func[$function->function] = $function->function;
        }
        foreach ($categories as $cate) {
            $cat[$cate->name] = $cate->name;
        }
        foreach ($users as $user) {
            $use[$user->id] = $user->first_name;
        }
        foreach ($exts as $ex) {
            $ext[$ex->counsel] = $ex->counsel;
        }
        foreach ($currs as $curr) {
            $cur[$curr->code] = $curr->code;
        }
        return view('notice.create',compact('entity','func','uni','cat','use','ext','cur'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,array(
            'entity' => 'required',
            'Location' => 'required',
            'department' => 'required',
            'by_ag' => 'required',
            'category' => 'required',
            'user_id' => 'required',
            'int_per' => 'required',
        ));

        $notice = new LitigationNotice;
        $notice->entity = $request->entity;
        $notice->Location = $request->Location;
        $notice->department = $request->department;
        $notice->by_ag = $request->by_ag;
        $notice->opp_part = $request->opp_part;
        $notice->category = $request->category;
        $notice->not_ref = $request->not_ref;
        $notice->addr_to = $request->addr_to;
        $notice->user_id = $request->user_id;
        $notice->int_per = $request->int_per;
        $notice->notice_date = date('Y-m-d',strtotime($request->notice_date));
        $notice->sent_rec = date('Y-m-d',strtotime($request->sent_rec));
        $notice->not_reply = date('Y-m-d',strtotime($request->not_reply));
        $notice->not_rem = date('Y-m-d',strtotime($request->not_rem));
        $notice->ext_counsel = $request->ext_counsel;
        $notice->opp_part_adv = $request->opp_part_adv;
        $notice->rel_law = $request->rel_law;
        $notice->comments = $request->comments;

        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'litigation/notices';
            $images=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $images[] = $fullname;
                $has = implode(",",$images);
            }

            $notice->docs = $has;
        }
        $notice->date = date('Y-m-d',strtotime($request->date));
        $notice->amount_involved = $request->amount_involved;
        $notice->currency = $request->currency;
        $notice->conv_curr = $request->conv_curr;
        $notice->conv_amt = $request->conv_amt;

        $notice->save();
        Session::flash('success','Notice Added');
        return redirect('notices');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notice = LitigationNotice::find($id);
        $status = NoticeStatus::where('stat_id',$id)->orderBy('id','Desc')->get();
        return view('notice.show',compact('notice','status'))->with('no',1);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notice = LitigationNotice::find($id);
        $entities = EntityList::all();
        $units = Unit::all();
        $functions = FunctionList::all();
        $categories = LitiType::all();
        $users = User::where('role','!=','Brand Protection Manager')->get();
        $exts = ExternalCounsel::all();
        $currs = Currency::all();
        $entity = array();
        $uni = array();
        $func = array();
        $cat = array();
        $use = array();
        $ext = array();
        $cur = array();
        foreach ($entities as $ent) {
            $entity[$ent->entity_child] = $ent->entity_child;
        }
        foreach ($units as $unit) {
            $uni[$unit->unit] = $unit->unit;
        }
        foreach ($functions as $function) {
            $func[$function->function] = $function->function;
        }
        foreach ($categories as $cate) {
            $cat[$cate->name] = $cate->name;
        }
        foreach ($users as $user) {
            $use[$user->id] = $user->first_name;
        }
        foreach ($exts as $ex) {
            $ext[$ex->counsel] = $ex->counsel;
        }
        foreach ($currs as $curr) {
            $cur[$curr->code] = $curr->code;
        }
        return view('notice.edit',compact('notice','entity','func','uni','cat','use','ext','cur'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'entity' => 'required',
            'Location' => 'required',
            'department' => 'required',
            'by_ag' => 'required',
            'category' => 'required',
            'user_id' => 'required',
            'int_per' => 'required',
        ));

        $notice = LitigationNotice::find($id);

        $notice->entity = $request->entity;
        $notice->Location = $request->Location;
        $notice->department = $request->department;
        $notice->by_ag = $request->by_ag;
        $notice->opp_part = $request->opp_part;
        $notice->category = $request->category;
        $notice->not_ref = $request->not_ref;
        $notice->addr_to = $request->addr_to;
        $notice->user_id = $request->user_id;
        $notice->int_per = $request->int_per;
        $notice->notice_date = date('Y-m-d',strtotime($request->notice_date));
        $notice->sent_rec = date('Y-m-d',strtotime($request->sent_rec));
        $notice->not_reply = date('Y-m-d',strtotime($request->not_reply));
        $notice->not_rem = date('Y-m-d',strtotime($request->not_rem));
        $notice->ext_counsel = $request->ext_counsel;
        $notice->opp_part_adv = $request->opp_part_adv;
        $notice->rel_law = $request->rel_law;
        $notice->comments = $request->comments;

        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'litigation/notices';
            $images=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $images[] = $fullname;
                $has = implode(",",$images);
            }

            $notice->docs = $has;
        }
        $notice->date = date('Y-m-d',strtotime($request->date));
        $notice->amount_involved = $request->amount_involved;
        $notice->currency = $request->currency;
        $notice->conv_curr = $request->conv_curr;
        $notice->conv_amt = $request->conv_amt;

        $notice->save();
        Session::flash('success','Notice Updated');
        return redirect('notices');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function deleten($id)
    {
        $post = LitigationNotice::find($id);
        $notices = NoticeStatus::where('stat_id',$id)->get();
        // foreach($notices as $notice){
        //     $images = $notice->docs;
        //     $npath = public_path('litigation/notices/'.$images);
        //     if(file_exists($npath) && $notice->docs !=''){
        //         unlink($npath);
        //     }

        // }
        $path = public_path('litigation/notices/'.$post->docs);
        if(file_exists($path) && $post->docs != ''){
            unlink($path);
        }
        $post->delete();

        Session::flash('success','Notice deleted!');
        return redirect()->route('notices.index');
    }

    public function search(Request $request)
    {
        $ent = $request->ent;
        $loc = $request->loc;
        $dep = $request->dep;
        $by_ag = $request->by_ag;
        $cat = $request->cat;
        $cnp = $request->cnp;
        $from_date = $request->form;
        $to_date = $request->to;

        $ents = DB::table('litigation_notices')->select('entity')->groupBy('entity')->get();
        $locs = DB::table('litigation_notices')->select('Location')->groupBy('Location')->get();
        $deps = DB::table('litigation_notices')->select('department')->groupBy('department')->get();
        $bys = DB::table('litigation_notices')->select('by_ag')->groupBy('by_ag')->get();
        $cats = DB::table('litigation_notices')->select('category')->groupBy('category')->get();
        $cnps = DB::table('litigation_notices')->select('int_per')->groupBy('int_per')->get();

        $results = DB::table('litigation_notices')
            ->where(function ($query) use ($ent,$loc, $dep, $by_ag,$cat, $cnp, $from_date, $to_date){
                if ($ent) {
                    $query->where('entity','=', $ent);
                }if ($loc) {
                    $query->where('Location','=', $loc);
                }
                if ($dep) {
                    $query->where('department','=', $dep);
                }
                if ($by_ag) {
                    $query->where('by_ag','=', $by_ag);
                }
                if ($cat) {
                    $query->where('category','=', $cat);
                }
                if ($cnp) {
                    $query->where('int_per','=', $cnp);
                }
                if ($from_date) {
                    $query->where('created_at','=',[$from_date, $to_date]);
                }
            })->get();
            return view('notice.search', compact('results','ents','locs','deps','bys','cats','cnps','ent','loc','dep','by_ag','cat','cnp','from_date','to_date'))->with('no',1);
    }

    public function convert($id)
    {
        $post = LitigationNotice::find($id);
        $entities = EntityList::all();
        $units = Unit::all();
        $functions = FunctionList::all();
        $categories = LitiType::all();
        $users = User::all();
        $exts = ExternalCounsel::all();
        $currs = Currency::all();
        $laws = LawFirm::all();
        $advocs = Advocate::all();
        $courts = CourtList::all();
        $entity = array();
        $uni = array();
        $func = array();
        $cat = array();
        $use = array();
        $ext = array();
        $cur = array();
        $lao = array();
        $adv = array();
        $cour = array();
        foreach ($entities as $ent) {
            $entity[$ent->entity_child] = $ent->entity_child;
        }
        foreach ($units as $unit) {
            $uni[$unit->unit] = $unit->unit;
        }
        foreach ($functions as $function) {
            $func[$function->function] = $function->function;
        }
        foreach ($categories as $cate) {
            $cat[$cate->name] = $cate->name;
        }
        foreach ($users as $user) {
            $use[$user->id] = $user->first_name;
        }
        foreach ($exts as $ex) {
            $ext[$ex->counsel] = $ex->counsel;
        }
        foreach ($currs as $curr) {
            $cur[$curr->code] = $curr->code;
        }
        foreach ($laws as $law) {
            $lao[$law->name] = $law->name;
        }
        foreach ($advocs as $advoc) {
            $adv[$advoc->advocate] = $advoc->advocate;
        }
        foreach ($courts as $court) {
            $cour[$court->name] = $court->name;
        }
        return view('notice.convert',compact('post','entity','func','uni','cat','use','ext','cur','lao','adv','cour'));
    }

    public function save_convert(Request $request,$id)
    {
        $this->validate($request,array(
            'entity' => 'required',
            'Location' => 'required',
            'department' => 'required',
            'category' => 'required',
            'by_ag' => 'required',
            'by_part' => 'required',
            'against_part' => 'required',
            'acting_as' => 'required',
            'law_firm' => 'required',
            'adv' => 'required',
            'case_date' => 'required',
            'critic' => 'required',
            'opp_act_as' => 'required',
        ));
        $summary = new LitigationSummary;
        $summary->notice_id = $id;
        $summary->entity = $request->entity;
        $summary->Location = $request->Location;
        $summary->department = $request->department;
        $summary->category = $request->category;
        $summary->by_ag = $request->by_ag;
        $summary->by_part = $request->by_part;
        $summary->against_part = $request->against_part;
        $summary->acting_as = $request->acting_as;
        $summary->ext_counsel = $request->ext_counsel;
        $summary->law_firm = $request->law_firm;
        $summary->adv = $request->adv;
        $summary->user_id = $request->user_id;
        $summary->case_date = date('Y-m-d',strtotime($request->case_date));
        $summary->int_per = $request->int_per;
        $summary->critic = $request->critic;
        $summary->opp_act_as = $request->opp_act_as;
        $summary->opp_part_add = $request->opp_part_add;
        $summary->opp_part_firm = $request->opp_part_firm;
        $summary->opp_part_adv = $request->opp_part_adv;
        $summary->opp_part_adv_c = $request->opp_part_adv_c;
        $summary->case_ref = $request->case_ref;
        $summary->juris = $request->juris;
        $summary->court = $request->court;
        $summary->rel_law = $request->rel_law;
        $summary->comments = $request->comments;
        $summary->date = date('Y-m-d',strtotime($request->date));
        $summary->amount_involved = $request->amount_involved;
        $summary->currency = $request->currency;
        $summary->conv_curr = $request->conv_curr;
        $summary->conv_amt = $request->conv_amt;

        $update = LitigationNotice::find($id);
        $update->status = 'Convert To Litigation';

        $summary->save();
        $update->save();
        Session::flash('success','Litigation Summary Added Successfully');
        return redirect('summary');
    }
}
