<?php

namespace App\Http\Controllers;

use App\Copyrightn;
use Illuminate\Http\Request;

class CopyrightDraftController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Draft()
    {
        $posts = Copyrightn::where('draft','=','draft')->orderBy('id','DESC')->get();
        return view('copyright.draft',compact('posts'))->with('no',1);
    }
}
