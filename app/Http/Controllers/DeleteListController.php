<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trademarks;
use App\UpdateStatusTrad;
use Storage;
use Session;
use DB;
class DeleteListController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function trade_status($id)
    {
        $post = UpdateStatusTrad::find($id);
        $status = $post->status;
        $sub_status = $post->sub_status;
        $trade_id = $post->trade_id;
        $sent_status = $post->sent_status;
        // query for Trademark Status and SubStatus change
        $update = Trademarks::find($trade_id);
        if($status == 'Active Application' && $sub_status == 'Marked for exam'){
            // when status and sub status are equal to the trademark status and sub status table then change the property
            if($update->status == 'Active Application' && $update->sub_status == 'Marked for exam'){
                $update->status = 'Active Application';
                $update->sub_status = 'Application Submitted';
                $update->sp = '1';
                $update->ssp = '1';
            }
        }
        if($status == 'Active Application' && $sub_status == 'Examination report received'){
            // when status and sub status are equal to the trademark status and sub status table then change the property
            if($update->status == 'Active Application' && $update->sub_status == 'Examination report received'){
                $update->status = 'Active Application';
                $update->sub_status = 'Marked for exam';
                $update->sp = '1';
                $update->ssp = '2';
            }
        }
        if($status == 'Active Application' && $sub_status == 'Replied to examination report'){
            // when status and sub status are equal to the trademark status and sub status table then change the property
            if($update->status == 'Active Application' && $update->sub_status == 'Replied to examination report'){
                $update->status = 'Active Application';
                $update->sub_status = 'Examination report received';
                $update->sp = '1';
                $update->ssp = '3';
            }
        }
        if($status == 'Registered'){
            if($update->status == 'Registered'){
                $update->status = 'Active Application';
                $update->sub_status = 'Decision pending';
                $update->sp = '1';
                $update->ssp = '10';
            }
        }
        if($status == 'Abandoned or rejected'){
            if($update->status == 'Abandoned or rejected'){
                $update->status = 'Registered';
                $update->sub_status = '';
                $update->sp = '2';
                $update->ssp = '0';
            }
        }
        if($status == 'Withdrawn'){
            if($update->status == 'Withdrawn'){
                $update->status = 'Abandoned or rejected';
                $update->sub_status = '';
                $update->sp = '3';
                $update->ssp = '0';
            }
        }
        if($status == 'Active Application' && $sub_status == 'Hearing for objection'){
            // when status and sub status are equal to the trademark status and sub status table then change the property
            if($update->status == 'Active Application' && $update->sub_status == 'Hearing for objection'){
                $update->status = 'Active Application';
                $update->sub_status = 'Replied to examination report';
                $update->sp = '1';
                $update->ssp = '4';
            }
        }
        if($status == 'Active Application' && $sub_status == 'Accepted and advertised'){
            // when status and sub status are equal to the trademark status and sub status table then change the property
            if($update->status == 'Active Application' && $update->sub_status == 'Accepted and advertised'){
                $update->status = 'Active Application';
                $update->sub_status = 'Hearing for objection';
                $update->sp = '1';
                $update->ssp = '5';
            }
        }
        if($status == 'Active Application' && $sub_status == 'Opposition noticed received'){
            // when status and sub status are equal to the trademark status and sub status table then change the property
            if($update->status == 'Active Application' && $update->sub_status == 'Opposition noticed received'){
                $update->status = 'Active Application';
                $update->sub_status = 'Accepted and advertised';
                $update->sp = '1';
                $update->ssp = '6';
            }
        }
        if($status == 'Active Application' && $sub_status == 'Counter statment submitted'){
            // when status and sub status are equal to the trademark status and sub status table then change the property
            if($update->status == 'Active Application' && $update->sub_status == 'Counter statment submitted'){
                $update->status = 'Active Application';
                $update->sub_status = 'Opposition noticed received';
                $update->sp = '1';
                $update->ssp = '7';
            }
        }
        if($status == 'Active Application' && $sub_status == 'Evidence and hearing'){
            // when status and sub status are equal to the trademark status and sub status table then change the property
            if($update->status == 'Active Application' && $update->sub_status == 'Evidence and hearing'){
                $update->status = 'Active Application';
                $update->sub_status = 'Counter statment submitted';
                $update->sp = '1';
                $update->ssp = '8';
            }
        }
        if($status == 'Active Application' && $sub_status == 'Decision pending'){
            // when status and sub status are equal to the trademark status and sub status table then change the property
            if($update->status == 'Active Application' && $update->sub_status == 'Decision pending'){
                $update->status = 'Active Application';
                $update->sub_status = 'Evidence and hearing';
                $update->sp = '1';
                $update->ssp = '9';
            }
        }
        $update->save();
        $post->delete();
        return redirect()->back()->with('success','Status Deleted!');
    }
}
