<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patent;
use App\PatAssOut;
use Session;
use Storage;
use Auth;
use DB;

class PatentAssOutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($id)
    {
    	$comp = Patent::find($id);
    	$licin = PatAssOut::where('pat_id', '=',$id)->get();;
    	return view('patent.assout',compact('comp','licin'))->with('no',1);
    }

    public function store(Request $request)
    {
        $this->validate($request,array(
            'licensor' => 'required',
            'licensee' => 'required',
            'lic_date' => 'required',
            'consideration' => 'required',
        ));
        $trad = new PatAssOut;
        $trad->pat_id = $request->pat_id;
        $trad->licensor = $request->licensor;
        $trad->licensee = $request->licensee;
        $trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
        $trad->consideration = $request->consideration;
        $trad->rem_ip = $request->rem_ip;
        $trad->ent_date = $request->ent_date;
        $trad->ent_date2 = $request->ent_date2;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
        $trad->reminder = $request->reminder;
        $trad->comments = $request->comments;
        $trad->save();
        Session::flash('success','Assign Out Successfully Added');
        return redirect('patent/assign_out/'.$request->pat_id);
    }

    public function edit($id)
    {
    	$licin = PatAssOut::find($id);
    	return view('patent.edit_assout',compact('licin'));
    }

    public function Update(Request $request,$id)
    {
        $this->validate($request,array(
            'licensor' => 'required',
            'licensee' => 'required',
            'lic_date' => 'required',
            'consideration' => 'required',
        ));
        $trad = PatAssOut::find($id);
        $trad->pat_id = $request->pat_id;
        $trad->licensor = $request->licensor;
        $trad->licensee = $request->licensee;
        $trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
        $trad->consideration = $request->consideration;
        $trad->rem_ip = $request->rem_ip;
        $trad->ent_date = $request->ent_date;
        $trad->ent_date2 = $request->ent_date2;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
        $trad->reminder = $request->reminder;
        $trad->comments = $request->comments;
        $trad->save();
        Session::flash('success','Assign Out Successfully Added');
        return redirect('patent/assign_out/'.$request->pat_id);
    }
}
