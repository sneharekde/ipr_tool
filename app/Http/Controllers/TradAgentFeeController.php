<?php

namespace App\Http\Controllers;

use App\TradAgentFee;
use App\Trademarks;
use Illuminate\Http\Request;
use Session;
use Storage;
class TradAgentFeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($id)
    {
        $post = Trademarks::find($id);
        return view('trade.agent_fee',compact('post'));
    }

    public function store(Request $request,$id)
    {
        $post = new TradAgentFee();
        $post->trade_id = $id;
        $post->date = date('Y-m-d',strtotime($request->date));
        $post->fee = $request->fee;
        if($request->hasFile('invoice') && $request->invoice->isvalid())
        {
            $invoice = $request->invoice->getClientOriginalName(); //Get invoice Name
            $invoice_scan_copy = $invoice;
            $request->invoice->move(public_path('copyright/img'),$invoice_scan_copy);
            $post->invoice = $invoice_scan_copy;
        }
        $post->not_fee = $request->not_fee;
        if($request->hasFile('stamp') && $request->stamp->isvalid())
        {
            $stamp = $request->stamp->getClientOriginalName(); //Get stamp Name
            $stamp_scan_copy = $stamp;
            $request->stamp->move(public_path('copyright/img'),$stamp_scan_copy);
            $post->stamp = $stamp_scan_copy;
        }
        $post->save();
        Session::flash('success','Agent or Attorney Added!');
        return redirect('tradmark/'.$id.'/show');
    }

    public function edit($id)
    {
        $post = TradAgentFee::find($id);
        return view('trade.edit_fee',compact('post'));
    }

    public function update(Request $request, $id)
    {
        $post = TradAgentFee::find($id);
        $post->date = date('Y-m-d',strtotime($request->date));
        $post->fee = $request->fee;
        if($request->hasFile('invoice') && $request->invoice->isvalid())
        {
            $invoice = $request->invoice->getClientOriginalName(); //Get invoice Name
            $invoice_scan_copy = $invoice;
            $request->invoice->move(public_path('copyright/img'),$invoice_scan_copy);
            $post->invoice = $invoice_scan_copy;
        }
        $post->not_fee = $request->not_fee;
        if($request->hasFile('stamp') && $request->stamp->isvalid())
        {
            $stamp = $request->stamp->getClientOriginalName(); //Get stamp Name
            $stamp_scan_copy = $stamp;
            $request->stamp->move(public_path('copyright/img'),$stamp_scan_copy);
            $post->stamp = $stamp_scan_copy;
        }
        $post->save();
        Session::flash('success','Agent or Attorney Added!');
        return redirect('tradmark/'.$post->trade_id.'/show');
    }

}
