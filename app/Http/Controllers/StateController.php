<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Country;
use App\State;
use App\Stete;

class StateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = Stete::all();
        return view('states.index',compact('states'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        $ar1 = array();
        foreach($countries as $country){
            $ar1[$country->id] = $country->name;
        }
        return view('states.create',compact('ar1'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'name' => 'required',
            'country_id' => 'required'
        ));

        $state = new Stete;
        $state->name = $request->name;
        $state->country_id = $request->country_id;

        $state->save();
        Session::flash('success','Created!');
        return redirect('state');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $state = Stete::find($id);
        $countries = Country::all();
        $ar1 = array();
        foreach($countries as $country){
            $ar1[$country->id] = $country->name;
        }
        return view('states.edit',compact('state','ar1'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'name' => 'required',
            'country_id' => 'required'
        ));

        $state = Stete::find($id);
        $state->name = $request->name;
        $state->country_id = $request->country_id;

        $state->save();
        Session::flash('success','Created!');
        return redirect('state');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
