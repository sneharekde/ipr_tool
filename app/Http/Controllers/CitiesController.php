<?php

namespace App\Http\Controllers;

use App\City;
use App\Stete;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\This;
use Session;

class CitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = City::all();
        return view('cities.index',compact('posts'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = Stete::all();
        $ar1 = array();
        foreach ($states as $state) {
            $ar1[$state->id] = $state->name;
        }
        return view('cities.create',compact('ar1'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'city' => 'required'
        ));

        $post = new City();
        $post->state_id = $request->state_id;
        $post->city = $request->city;
        $post->latitude = $request->latitude;
        $post->longitude = $request->longitude;
        $post->save();
        Session::flash('success','City Added Successfully!');
        return redirect('cities');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = City::find($id);
        $states = Stete::all();
        $ar1 = array();
        foreach ($states as $state) {
            $ar1[$state->id] = $state->name;
        }
        return view('cities.edit',compact('ar1','post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'city' => 'required'
        ));

        $post = City::find($id);
        $post->state_id = $request->state_id;
        $post->city = $request->city;
        $post->latitude = $request->latitude;
        $post->longitude = $request->longitude;
        $post->save();
        Session::flash('success','City Updated Successfully!');
        return redirect('cities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
