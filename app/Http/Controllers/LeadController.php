<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lead;
use App\Country;
use App\User;
use App\Agency;
use App\BrandProduct;
use App\City;
use App\Entity;
use App\EntityList;
use App\Invest;
use App\LeadAssign;
use App\Role;
use App\Stete;
use App\Target;
use Session;
use DB;
use Storage;

class LeadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function assign_to()
    {
        $posts = LeadAssign::where('status','assign')->orderBy('id','DESC')->get();
        return view('leads.assign_to',compact('posts'))->with('no',1);
    }

    public function index()
    {
        $leads = Lead::orderBy('id','DESC')->get();
        return view('leads.index',compact('leads'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Lead::find($id);
        return view('leads.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function assign($id)
    {
        $post = Lead::find($id);
        $inags = Agency::all();
        $users = User::where('role','Brand Protection Manager')->get();
        $ents = Entity::all();
        $countries = Country::all();
        $targets = Target::all();
        $ar1 = array();
        $ar2 = array();
        $ar3 = array();
        $ar4 = array();
        $ar5 = array();
        foreach ($inags as $inag) {
            $ar1[$inag->id] = $inag->name;
        }
        foreach ($ents as $ent) {
            $ar2[$ent->id] = $ent->name;
        }
        foreach ($users as $user) {
            $ar3[$user->id] = $user->first_name.' '.$user->last_name;
        }
        foreach ($countries as $country) {
            $ar4[$country->id] = $country->name;
        }
        foreach ($targets as $target) {
            $ar5[$target->id] = $target->targets;
        }
        return view('leads.lead_assign',compact('post','ar1','ar2','ar3','ar4','ar5'));
    }

    public function save_ass(Request $request)
    {
        $this->validate($request,array(
            'nop' => 'required',
        ));

        $post = new LeadAssign;
        $post->lead_id = $request->lead_id;
        $post->ent_id = $request->ent_id;
        $post->inag_id = $request->inag_id;
        $post->user_id = $request->user_id;
        $post->country_id = $request->country_id;
        $post->state_id = $request->state_id;
        $post->city_id = $request->city_id;
        $post->target_id = $request->target_id;
        $post->nop = $request->nop;
        // Change Status of Lead
        $update = Lead::find($request->lead_id);
        $update->status = 'Assigned';

        $post->save();
        $update->save();

        Session::flash('success','Lead Assign Successfully!');
        return redirect('leads');
    }

    public function view_ass($id)
    {
        $post = LeadAssign::where('lead_id',$id)->first();
        return view('leads.view_assign',compact('post'))->with('no',1);
    }

    public function invest_create($id)
    {
        $post = LeadAssign::find($id);
        $inags = Agency::all();
        $users = User::all();
        $ents = Entity::all();
        $countries = Country::all();
        $targets = Target::all();
        $states = Stete::where('country_id', $post->country_id)->get();
        $cities = City::where('state_id', $post->state_id)->get();
        $products = BrandProduct::where('ent_id',$post->ent_id)->get();
        $ar1 = array();
        $ar2 = array();
        $ar3 = array();
        $ar4 = array();
        $ar5 = array();

        foreach ($inags as $inag) {
            $ar1[$inag->id] = $inag->name;
        }
        foreach ($ents as $ent) {
            $ar2[$ent->id] = $ent->name;
        }
        foreach ($users as $user) {
            $ar3[$user->id] = $user->first_name.' '.$user->last_name;
        }
        foreach ($countries as $country) {
            $ar4[$country->id] = $country->name;
        }
        foreach ($targets as $target) {
            $ar5[$target->targets] = $target->targets;
        }
        return view('leads.investc',compact('post','ar1','ar2','ar3','ar4','ar5','states','cities','products'));
    }

    public function investsave(Request $request)
    {
        $this->validate($request,array(
            'ent_id' => 'required',
            'inag_id' => 'required',
            'user_id' => 'required',
        ));

        $dsi = date("Y-m-d", strtotime($request->dsi));
        $dei = date("Y-m-d", strtotime($request->dei));

        $post = new Invest;
        $post->assign_id = $request->assign_id;
        $post->ent_id = $request->ent_id;
        $post->inag_id = $request->inag_id;
        $post->country_id = $request->country_id;
        $post->state_id = $request->state_id;
        $post->city_id = $request->city_id;
        $post->target = $request->target;
        $post->nop_id = $request->nop_id;
        $post->dsi = $dsi;
        $post->dei = $dei;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'Scan_Copy_Of_Application';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
            }
            $post->docs = $has;
        }
        $post->user_id = $request->user_id;
        $post->tm_app = $request->tm_app;
        if($request->nota !=''){
            $post->nota = implode(":",$request->nota);
        }
        if($request->tar_add !=''){
            $post->n_addr = implode(":",$request->tar_add);
        }

        $update = LeadAssign::find($request->assign_id);
        $update->status = 'Convert To Investigation';

        $post->save();
        $update->save();

        Session::flash('success','Investigation Report Addedd!');
        return redirect('invest/show');

    }
}
