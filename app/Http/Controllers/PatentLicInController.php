<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patent;
use App\PatentLicIn;
use Session;
use Storage;
use Auth;
use DB;
class PatentLicInController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function create($id)
    {
    	$comp = Patent::where('id', '=',$id)->first();
        $licin = PatentLicIn::where('patent_id', '=',$id)->get();
    	return view('patent.licin',compact('comp','licin'))->with('no',1);
    }

    public function store(Request $request)
    {
        $this->validate($request,array(
            'licensor' => 'required',
            'licensee' => 'required',
            'term' => 'required|numeric',
            'lic_date' => 'required',
            'consideration' => 'required',
        ));
        $tems = $request->term * 12;
        $trad = new PatentLicIn;
        $trad->patent_id = $request->patent_id;
        $trad->licensor = $request->licensor;
        $trad->licensee = $request->licensee;
        $trad->term = $request->term;
        $trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
        $trad->exp_date = date('Y-m-d',strtotime($tems.' months',strtotime($request->lic_date)));
        $trad->consideration = $request->consideration;
        $trad->rem_ip = $request->rem_ip;
        $trad->ent_date = $request->ent_date;
        $trad->ent_date2 = $request->ent_date2;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
        $trad->reminder = $request->reminder;
        $trad->comments = $request->comments;
        $trad->save();
        Session::flash('success','License In Successfully Added');
        return redirect('/patent/license_in/'.$request->patent_id);
    }

    public function edit($id)
    {

        $licin = PatentLicIn::find($id);
    	return view('patent.edit_licins',compact('licin'));
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,array(
            'licensor' => 'required',
            'licensee' => 'required',
            'term' => 'required|numeric',
            'lic_date' => 'required',
            'consideration' => 'required',
        ));
        $tems = $request->term * 12;
        $trad = PatentLicIn::find($id);
        $trad->patent_id = $request->patent_id;
        $trad->licensor = $request->licensor;
        $trad->licensee = $request->licensee;
        $trad->term = $request->term;
        $trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
        $trad->exp_date = date('Y-m-d',strtotime($tems.' months',strtotime($request->lic_date)));
        $trad->consideration = $request->consideration;
        $trad->rem_ip = $request->rem_ip;
        $trad->ent_date = $request->ent_date;
        $trad->ent_date2 = $request->ent_date2;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
        $trad->reminder = $request->reminder;
        $trad->comments = $request->comments;
        $trad->save();
        Session::flash('success','License In Successfully Added');
        return redirect('/patent/license_in/'.$request->patent_id);
    }
}
