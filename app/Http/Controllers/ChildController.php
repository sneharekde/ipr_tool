<?php

namespace App\Http\Controllers;

use App\Agent;
use App\City;
use App\Entity;
use App\NatureApplicant;
use Illuminate\Http\Request;
use App\TrademarkGovFees;
use App\TradeClass;
use App\State;
use App\Stete;
use App\User;
use App\BrandProduct;
use DB;
class ChildController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function findtype(Request $request)
    {
    	$data = TrademarkGovFees::select('app_type','id')->where('appf_id',$request->id)->get();
    	return response()->json($data);
    }
    public function findfee(Request $request)
    {
    	$p = TrademarkGovFees::select('fees')->where('id',$request->id)->first();
    	return response()->json($p);
    }
    public function classdes(Request $request)
    {
        $c = TradeClass::select('desc')->whereIn('id',$request->id)->get();
        return response()->json($c);
    }

    public function findStates(Request $request)
    {
    	$data = Stete::where('country_id',$request->id)->get();
    	return response()->json($data);
    }

    public function findcity(Request $request)
    {
    	$data = City::where('state_id',$request->id)->get();
    	return response()->json($data);
    }

    public function findnat(Request $request)
    {
        $ent = Entity::find($request->id);
        $data = NatureApplicant::find($ent->nat_id);
        return response()->json($data);
    }

    public function finduser(Request $request)
    {
        $data = User::find($request->id);
        return response()->json($data);
    }

    public function findprod(Request $request)
    {
        $data = BrandProduct::where('ent_id',$request->id)->get();
        return response()->json($data);
    }

    public function findagent(Request $request)
    {
        $data = Agent::find($request->id);
        return response()->json($data);
    }
}
