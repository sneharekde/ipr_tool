<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enf;
use App\Invest;
use App\Designm;
use App\Copyrightn;
use App\Patent;
use App\LitigationSummary;
use DB;
class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function brand()
    {
        // Fethc Count Value on Dashboard
        $total_tradmark = DB::table('trademarks')->count();
        $total_invest = DB::table('invests')->count();
        $total_enforce = DB::table('enfs')->count();
        $total_brand = $total_invest + $total_enforce;
        $total_copyright = DB::table('copyrightns')->count();
        $total_design = DB::table('designms')->count();
        $total_patent = DB::table('patents')->count();
        $total_liti = DB::table('litigation_summaries')->count();
        // Fetch Count Data and value of Investigation Module
        $data2 = Invest::selectRaw('count(target) as count, target')->groupBy('target')->get();
        $array3 = array();
        foreach ($data2 as $result2)
        {
            $array3[$result2->target] =(int)$result2->count;
        }
        // Fetch Count Data and Value on Enforcement Value in Charts
        $data3 = Enf::selectRaw('count(target) as count, target')->groupBy('target')->get();
        $array4 = array();
        foreach ($data3 as $result3)
        {
            $array4[$result3->target] =(int)$result3->count;
        }
        // Show all Enforcemant Data
        $showes3 = Enf::all();
        return view('dashboard.brand',compact('total_tradmark','total_brand','total_copyright','total_design','total_patent','total_liti','data2','array3','data3','array4'));
    }
    public function design()
    {
        // Fethc Count Value on Dashboard
        $total_tradmark = DB::table('trademarks')->count();
        $total_invest = DB::table('invests')->count();
        $total_enforce = DB::table('enfs')->count();
        $total_brand = $total_invest + $total_enforce;
        $total_copyright = DB::table('copyrightns')->count();
        $total_design = DB::table('designms')->count();
        $total_patent = DB::table('patents')->count();
        $total_liti = DB::table('litigation_summaries')->count();
        // Fetch Count Data and value of Investigation Module
        $data6 = Designm::selectRaw('count(status) as count , status')->orderBy('sp')->groupBy('status')->where('draft','submit')->get();
        $array7 = array();
        foreach ($data6 as $result6) {
            $array7[$result6->status] = (int)$result6->count;
        }
        $data7 = Designm::selectRaw('count(sub_status) as count , sub_status')->orderBy('ssp')->groupBy('sub_status')->where('ssp','!=','')->where('draft','submit')->get();
        $array8 = array();
        foreach ($data7 as $result7) {
            $array8[$result7->sub_status] = (int)$result7->count;
        }
        return view('dashboard.design',compact('total_tradmark','total_brand','total_copyright','total_design','total_patent','total_liti','data6','array7','data7','array8'));
    }
    public function copy()
    {
        // Fethc Count Value on Dashboard
        $total_tradmark = DB::table('trademarks')->count();
        $total_invest = DB::table('invests')->count();
        $total_enforce = DB::table('enfs')->count();
        $total_brand = $total_invest + $total_enforce;
        $total_copyright = DB::table('copyrightns')->count();
        $total_design = DB::table('designms')->count();
        $total_patent = DB::table('patents')->count();
        $total_liti = DB::table('litigation_summaries')->count();
        // Fetch Count Data and value of Investigation Module
        $data4 = Copyrightn::selectRaw('count(status) as count , status')->orderBy('sp')->groupBy('status')->where('draft','submit')->get();
        $array5 = array();
        foreach ($data4 as $result4) {
            $array5[$result4->status] = (int)$result4->count;
        }
        $data5 = Copyrightn::selectRaw('count(sub_status) as count , sub_status')->orderBy('ssp')->groupBy('sub_status')->where('ssp','!=','')->where('draft','submit')->get();
        $array6 = array();
        foreach ($data5 as $result5) {
            $array6[$result5->sub_status] = (int)$result5->count;
        }
        return view('dashboard.copy',compact('total_tradmark','total_brand','total_copyright','total_design','total_patent','total_liti','data4','array5','data5','array6'));
    }
    public function patent()
    {
        // Fethc Count Value on Dashboard
        $total_tradmark = DB::table('trademarks')->count();
        $total_invest = DB::table('invests')->count();
        $total_enforce = DB::table('enfs')->count();
        $total_brand = $total_invest + $total_enforce;
        $total_copyright = DB::table('copyrightns')->count();
        $total_design = DB::table('designms')->count();
        $total_patent = DB::table('patents')->count();
        $total_liti = DB::table('litigation_summaries')->count();
        // Fetch Count Data and value of Investigation Module
        $data8 = Patent::selectRaw('count(status) as count , status')->groupBy('status')->get();
        $array9 = array();
        foreach ($data8 as $result8) {
            $array9[$result8->status] = (int)$result8->count;
        }
        $data9 = Patent::selectRaw('count(sub_status) as count , sub_status')->where('sub_status','!=','')->groupBy('sub_status')->get();
        $array10 = array();
        foreach ($data9 as $result9) {
            $array10[$result9->sub_status] = (int)$result9->count;
        }
        return view('dashboard.patent',compact('total_tradmark','total_brand','total_copyright','total_design','total_patent','total_liti','data8','array9','data9','array10'));
    }
    public function liti()
    {
        // Fethc Count Value on Dashboard
        $total_tradmark = DB::table('trademarks')->count();
        $total_invest = DB::table('invests')->count();
        $total_enforce = DB::table('enfs')->count();
        $total_brand = $total_invest + $total_enforce;
        $total_copyright = DB::table('copyrightns')->count();
        $total_design = DB::table('designms')->count();
        $total_patent = DB::table('patents')->count();
        $total_liti = DB::table('litigation_summaries')->count();
        // Fetch Count Data and value of Investigation Module
        $liti = LitigationSummary::selectRaw('count(status) as count,status')->orderBy('prio')->groupBy('status')->get();
        $liti_ar = array();
        foreach ($liti as $lit) {
                $liti_ar[$lit->status] = (int)$lit->count;
        }
        return view('dashboard.liti',compact('total_tradmark','total_brand','total_copyright','total_design','total_patent','total_liti','liti','liti_ar'));
    }
}
