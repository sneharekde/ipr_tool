<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Design;
use App\DesAssIn;
use App\User;
use App\Language;
use App\DesignationList;
use App\Designm;
use Session;
use Storage;
use Auth;
use DB;
use PDF;
use Illuminate\Support\Facades\Input;

class Design_ass_in_Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function assign_create($id)
    {
    	$comp = Designm::find($id);
    	$licin = DesAssIn::all();
    	return view('design.assin',compact('comp','licin'))->with('no',1);
    }

    public function store(Request $request)
    {
        $this->validate($request,array(
            'licensor' => 'required',
            'licensee' => 'required',
            'lic_date' => 'required',
            'consideration' => 'required',
        ));
        $trad = new DesAssIn;
        $trad->des_id = $request->des_id;
        $trad->licensor = $request->licensor;
        $trad->licensee = $request->licensee;
        $trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
        $trad->consideration = $request->consideration;
        $trad->rem_ip = $request->rem_ip;
        $trad->ent_date = $request->ent_date;
        $trad->ent_date2 = $request->ent_date2;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
        $trad->reminder = $request->reminder;
        $trad->comments = $request->comments;
        $trad->save();
        Session::flash('success','Assign In Successfully Added');
        return redirect('design/assign_in/'.$request->des_id);
    }


    public function assign_edit($id)
    {

    	$licin = DesAssIn::find($id);
    	return view('design.edit_assign',compact('licin'));
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,array(
            'licensor' => 'required',
            'licensee' => 'required',
            'lic_date' => 'required',
            'consideration' => 'required',
        ));
        $trad = DesAssIn::find($id);
        $trad->des_id = $request->des_id;
        $trad->licensor = $request->licensor;
        $trad->licensee = $request->licensee;
        $trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
        $trad->consideration = $request->consideration;
        $trad->rem_ip = $request->rem_ip;
        $trad->ent_date = $request->ent_date;
        $trad->ent_date2 = $request->ent_date2;
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
        $trad->reminder = $request->reminder;
        $trad->comments = $request->comments;
        $trad->save();
        Session::flash('success','Assign In Successfully Added');
        return redirect('design/assign_in/'.$request->des_id);
    }
}
