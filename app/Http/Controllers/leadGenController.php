<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Lead;
use App\Mail\SendLeadMails;
use App\Target;
use Mail;
use App\User;
class leadGenController extends Controller
{
    public function create(){
        $targets = Target::all();
        return view('leads.lead_gen',compact('targets'));
    }

    public function save(Request $request){
        $leadg = new Lead;
        $leadg->location = $request->loca;
        $leadg->leadby = $request->leadby;
        $leadg->product = $request->pname;
        $leadg->qty = $request->qty;
        $leadg->target_id = $request->target_id;
        $leadg->emp_id = $request->emp_id;
        if ($request->hasFile('photos'))
        {
            $files= $request->file('photos');
            $destinationPath= 'trademark';
            $images=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $images[] = $fullname;
                $has2 = implode(",",$images);
            }
            $leadg->docs = $has2;
        }
        $leadg->phone = $request->phone;
        $leadg->comments = $request->comm;
        $leadg->status = 'Not Assign';
        $users = User::where('role','=','Brand Protection Manager')->get();
        $data = array(
            'location' => $request->loca,
            'leadby' => $request->leadby,
            'product' => $request->pname,
        );

        // Send Email
        foreach ($users as $user) {
            $this->sendEmailToUser($user, $data);
        }


        $leadg->save();
        Session::flash('success','New Lead Reporting Successfully!');
        return redirect('lead_home');
    }

    private function sendEmailToUser($user, $data)
    {
        Mail::to($user)->send(new SendLeadMails($data,$user));
    }


    public function show()
    {
        return view('leads.message');
    }
}
