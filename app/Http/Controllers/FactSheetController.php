<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DesignationList;
use App\FactSheet;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\StoreFactsheetRequest;
use File;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\FactsheetApprovalProcess;
use App\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class FactSheetController extends Controller {
	public $op_legal = "";
	public $followed_user = "";
	public $disapproved_followed_user = [];
	public $followed_user_initiator = "";
	public $disapproved_subject = "";
	public $email_subject = "";
	public $mail_from = "";
	public $mail_admin = "";
	
	public function __construct() {
		$this->middleware ( 'auth', [ 
				'except' => [ 
						'factSheetInitiatorAp',
						'factSheetStatusShared',
						'throwFactsheetApprovalProcessEmail',
						'shared_success',
						'factSheetStatusSharedSuccess' 
				] 
		] );
		
		$this->mail_from = Config::get ( 'app.mail_from' );
		$this->mail_admin = Config::get ( 'app.mail_admin' );
	}
	public function index() {
		$designations = DesignationList::all ();
		$showes = FactSheet::select ( 'name_of_initiator', 'designation', 'name_of_channel', 'registration_mark', 'mark_used_from', 'class_of_registration', 'jurisdiction_for_registration', 'significance_of_work', 'factsheet_master.id', 'factsheet_approval_process.factsheet_approval_status', 'factsheet_approval_process.initiator_initiated_approval', 'factsheet_approval_process.initiator_approval_back' )->leftjoin ( 'factsheet_approval_process', 'factsheet_approval_process.factsheet_id', '=', 'factsheet_master.id' )->orderBy ( 'id', 'DESC' )->get ();
		$factsheets = [ ];
		$user = Auth::user ();
		foreach ( $showes as $row ) {
			$designation = DesignationList::where ( 'id', $row ['designation'] )->first ();
			$rm = "";
			if (! blank ( $row ['registration_mark'] )) {
				$reg_mark = json_decode ( $row ['registration_mark'], true );
				if ($reg_mark ['reg_m_type'] == "wm") {
					$rm = "Word Mark : " . $reg_mark ['reg_m_type_value_title'];
				}
				if ($reg_mark ['reg_m_type'] == "im") {
					$rm .= "<br/> Mark Image: <img width='100px' src='Registration_Mark/" . $reg_mark ['reg_m_type_image'] . "' /> <a href='Registration_Mark/" . $reg_mark ['reg_m_type_image'] . "' download><i class='fa fa-download'></i></a>";
				}
				if ($reg_mark ['reg_m_type'] == "wmim") {
					$rm = "Word Mark : " . $reg_mark ['reg_m_type_value_title'];
					$rm .= "<br/> Mark Image: <img width='100px' src='Registration_Mark/" . $reg_mark ['reg_m_type_image'] . "' /> <a href='Registration_Mark/" . $reg_mark ['reg_m_type_image'] . "' download><i class='fa fa-download'></i></a>";
				}
			}
			$mu = "";
			if (! blank ( $row ['mark_used_from'] )) {
				$mark_used_from = json_decode ( $row ['mark_used_from'], true );
				if ($mark_used_from ['m_u_f_type'] == 'date') {
					$mu = "Date :" . $mark_used_from ['m_u_f_value'];
				}
				if ($mark_used_from ['m_u_f_type'] == 'ptbu') {
					$mu = $mark_used_from ['m_u_f_value'];
				}
			}
			$cr = '';
			if (! blank ( $row ['class_of_registration'] )) {
				$class_reg = json_decode ( $row ['class_of_registration'], true );
				if ($class_reg ['cof_type'] == 'single') {
					$cr = "Single :";
					$cr .= "<br>" . $class_reg ['cof_single_value'];
				}
				if ($class_reg ['cof_type'] == 'combo') {
					$cr = "Combo :";
					$cr .= "<br>" . $class_reg ['cof_single_value'];
					$cr .= "<br>" . $class_reg ['cof_combo_value'];
				}
			}
			
			$approval_process = FactsheetApprovalProcess::where ( 'factsheet_id', $row->id )->first ();
			$status_approvals = "<span class='text-danger'>Not Initiated</span>";
			$approval_button = 0;
			if ($approval_process == null) {
				$approval_button = 1;
			}
			if ($approval_process != null) {
				$status_approvals = "";
				$factsheet_step = $approval_process->factsheet_status_steps;
				if ($approval_process->initiator_approval_back == 0) {
					if ($factsheet_step == 2 && $approval_process->factsheet_approval_status == 0) {
						if ($approval_process->initiator_optional_approval_status == 1) {
							$status_approvals .= "<span class='text-success'>External : Approved </span></br>";
						} else {
							
							if ($approval_process->initiator_optional_approval_status == 0) {
								$status_approvals .= "<span class='text-danger'>External : Pending </span></br>";
							} else {
								$status_approvals .= "<span class='text-danger'>External : Disapproved </span></br>";
							}
						}
					}
					if ($factsheet_step == 3 && $approval_process->factsheet_approval_status == 0) {
						if ($approval_process->initiator_optional_approval_status == 1) {
							$status_approvals .= "<span class='text-success'>External : Approved </span></br>";
						} else {
							if (!blank($approval_process->initiator_optional_agent_email)) {
								if ($approval_process->initiator_optional_approval_status == 0) {
									$status_approvals .= "<span class='text-danger'>External : Pending </span></br>";
								} else {
									$status_approvals .= "<span class='text-danger'>External : Disapproved </span></br>";
								}
							}else{
								if ($approval_process->legal_approval_status == 0) {
									$status_approvals .= "<span class='text-danger'>Legal : Pending </span></br>";
								} else {
									$status_approvals .= "<span class='text-danger'>Legal : Disapproved </span></br>";
								}
							}
						}
					}
					if ($factsheet_step == 4 && $approval_process->factsheet_approval_status == 0) {
						if ($approval_process->legal_approval_status == 1) {
							$status_approvals .= "<span class='text-success'>Legal : Approved </span></br>";
						} else {
							if ($approval_process->legal_approval_status == 0) {
								$status_approvals .= "<span class='text-danger'>Legal : Pending </span></br>";
							} else {
								$status_approvals .= "<span class='text-danger'>Legal : Disapproved </span></br>";
							}
						}
					}
					
					if ($factsheet_step == 5 && $approval_process->factsheet_approval_status == 0) {
						if ($approval_process->coo_approval_status == 1) {
							$status_approvals .= "<span class='text-success'>COO : Approved </span></br>";
						} else {
							if ($approval_process->coo_approval_status == 0) {
								$status_approvals .= "<span class='text-danger'>COO : Pending </span></br>";
							} else {
								$status_approvals .= "<span class='text-danger'>COO : Disapproved </span></br>";
							}
						}
					}
					if ($factsheet_step == 6 && $approval_process->factsheet_approval_status == 0) {
						if ($approval_process->cfo_approval_status == 1) {
							$status_approvals .= "<span class='text-success'>CFO : Approved </span>";
						} else {
							if ($approval_process->cfo_approval_status == 0) {
								$status_approvals .= "<span class='text-danger'>CFO : Pending </span></br>";
							} else {
								$status_approvals .= "<span class='text-danger'>CFO : Disapproved </span></br>";
							}
						}
					}
					if ($factsheet_step == 6 && $approval_process->factsheet_approval_status == 1) {
						if ($approval_process->ceo_approval_status == 1) {
							$status_approvals .= "<span class='text-success'>CEO : Approved </span>";
						} else {
							if ($approval_process->ceo_approval_status == 0) {
								$status_approvals .= "<span class='text-danger'>CEO : Pending </span></br>";
							} else {
								$status_approvals .= "<span class='text-danger'>CEO : Disapproved </span></br>";
							}
						}
					}
				}else{
					$disapproved_by = "";
					foreach ( config ( 'app.factsheet_ap_status' ) as $ap_step_dis_key => $ap_dis_steps ) {
						if ($approval_process->initiator_approval_back == $ap_dis_steps) {
							$disapproved_by = $ap_step_dis_key;
							$status_approvals = "<span class='text-danger'>$disapproved_by : Disapproved </span></br>";
						}
					}
				}
			}
			$factsheets [] = array (
					'id' => $row ['id'],
					'name_of_initiator' => $row ['name_of_initiator'],
					'designations' => $designation ['designation'],
					'name_of_channel' => $row ['name_of_channel'],
					'registration_mark' => $rm,
					'mark_used_from' => $mu,
					'class_of_registration' => $cr,
					'jurisdiction_for_registration' => $row ['jurisdiction_for_registration'],
					'significance_of_work' => $row ['significance_of_work'],
					'factsheet_approval_status' => $status_approvals,
					'initiator_initiated_approval' => $row ['initiator_initiated_approval'],
					'initiator_approval_back' => $row ['initiator_approval_back'],
					'approval_btn' => $approval_button,
					'created_on' => Carbon::parse ( $row ['created_at'] )->format ( 'M d Y' ) 
			);
		}
		
		$path = public_path('country_json.json');
		$country_list = json_decode(file_get_contents($path), true);
		return view ( 'factsheet.index', compact ( 'factsheets', 'designations', 'user', 'country_list' ) );
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreFactsheetRequest $request) {
		$input = $request->all ();
		
		$validated = $request->validated ();
		$factsheet = new FactSheet ();
		$factsheet->name_of_initiator = $input ['name_of_initiator'];
		$factsheet->designation = $input ['designation'];
		$factsheet->name_of_channel = $input ['name_of_channel'];
		$factsheet->significance_of_work = $input ['significance_work'];
		$factsheet->jurisdiction_for_registration = $input ['jurisdiction_reg'];
		
		if (isset ( $input ['registration_mark'] ) && $input ['registration_mark'] == "wm") {
			$regi_mark_details = array (
					'reg_m_type' => $input ['registration_mark'],
					'reg_m_type_value_title' => $input ['reg_mark_title'] 
			);
			$factsheet->registration_mark = json_encode ( $regi_mark_details );
		}
		if (isset ( $input ['registration_mark'] ) && $input ['registration_mark'] == "im") {
			$image_path = "";
			if ($request->hasFile ( 'reg_mark_img' )) {
				$files = $request->file ( 'reg_mark_img' );
				$destinationPath = 'Registration_Mark';
				$fullname = $files->getClientOriginalName ();
				$hashname = $fullname;
				$upload_success = $files->move ( public_path ( $destinationPath ), $hashname );
				$image_path = $fullname;
			}
			$regi_mark_details = array (
					'reg_m_type' => $input ['registration_mark'],
					'reg_m_type_image' => $image_path 
			);
			$factsheet->registration_mark = json_encode ( $regi_mark_details );
		}
		
		if (isset ( $input ['registration_mark'] ) && $input ['registration_mark'] == "wmim") {
			$image_path = "";
			if ($request->hasFile ( 'reg_mark_img' )) {
				$files = $request->file ( 'reg_mark_img' );
				$destinationPath = 'Registration_Mark';
				$fullname = $files->getClientOriginalName ();
				$hashname = $fullname;
				$upload_success = $files->move ( public_path ( $destinationPath ), $hashname );
				$image_path = $fullname;
			}
			$regi_mark_details = array (
					'reg_m_type' => $input ['registration_mark'],
					'reg_m_type_value_title' => $input ['reg_mark_title'],
					'reg_m_type_image' => $image_path 
			);
			$factsheet->registration_mark = json_encode ( $regi_mark_details );
		}
		
		if (isset ( $input ['mark_used_from'] ) && $input ['mark_used_from'] == "date") {
			$mark_used_from = array (
					'm_u_f_type' => $input ['mark_used_from'],
					'm_u_f_value' => $input ['mark_used_from_date'] 
			);
			$factsheet->mark_used_from = json_encode ( $mark_used_from );
		}
		
		if (isset ( $input ['mark_used_from'] ) && $input ['mark_used_from'] == "ptbu") {
			$mark_used_from = array (
					'm_u_f_type' => $input ['mark_used_from'],
					'm_u_f_value' => $input ['proposed_to_be_used_txt'] 
			);
			$factsheet->mark_used_from = json_encode ( $mark_used_from );
		}
		
		if (isset ( $input ['class_of_reg'] ) && $input ['class_of_reg'] == "single") {
			$class_of_reg = array (
					'cof_type' => $input ['class_of_reg'],
					'cof_single_value' => $input ['cor_single'] 
			);
			$factsheet->class_of_registration = json_encode ( $class_of_reg );
		}
		
		if (isset ( $input ['class_of_reg'] ) && $input ['class_of_reg'] == "combo") {
			$class_of_reg = array (
					'cof_type' => 'combo',
					'cof_single_value' => $input ['cor_single'],
					'cof_combo_value' => $input ['cor_combo'] 
			);
			$factsheet->class_of_registration = json_encode ( $class_of_reg );
		}
		$factsheet->factsheet_code = Str::random ( 10 );
		$factsheet->save ();
		
		Session::flash ( 'success', 'Factsheet Added' );
		return redirect ( 'factsheet' );
	}
	public function edit($id) {
		$fact_sheet = FactSheet::where ( 'id', $id )->first ();
		if ($fact_sheet == null) {
			Session::flash ( 'success', 'Factsheet not found' );
			return redirect ( 'factsheet' );
		}
		$reg_mark = json_decode ( $fact_sheet->registration_mark );
		$mark_used_from = json_decode ( $fact_sheet->mark_used_from );
		$class_reg = json_decode ( $fact_sheet->class_of_registration );
		$designations = DesignationList::all ();
		
		$path = public_path('country_json.json');
		$country_list = json_decode(file_get_contents($path), true);
		
		return view ( 'factsheet.edit_factsheet', compact ( 'fact_sheet', 'ar2', 'ar3', 'ar4', 'designations', 'reg_mark', 'mark_used_from', 'class_reg', 'country_list') );
	}
	public function update(StoreFactsheetRequest $request) {
		$input = $request->all ();
		$validated = $request->validated ();
		
		$factsheet = FactSheet::where ( 'id', $input ['id'] )->first ();
		if ($factsheet == null) {
			Session::flash ( 'success', 'Factsheet Not Found' );
			return redirect ( 'factsheet' );
		}
		
		$factsheet->name_of_initiator = $input ['name_of_initiator'];
		$factsheet->designation = $input ['designation'];
		$factsheet->name_of_channel = $input ['name_of_channel'];
		$factsheet->significance_of_work = $input ['significance_work'];
		$factsheet->jurisdiction_for_registration = $input ['jurisdiction_reg'];
		
		if (isset ( $input ['registration_mark'] ) && $input ['registration_mark'] == "wm") {
			$regi_mark_details = array (
					'reg_m_type' => $input ['registration_mark'],
					'reg_m_type_value_title' => $input ['reg_mark_title'] 
			);
			$factsheet->registration_mark = json_encode ( $regi_mark_details );
		}
		if (isset ( $input ['registration_mark'] ) && $input ['registration_mark'] == "im") {
			$image_path = $input ['reg_mark_img_prev'];
			if ($request->hasFile ( 'reg_mark_img' )) {
				$files = $request->file ( 'reg_mark_img' );
				$destinationPath = 'Registration_Mark';
				$fullname = $files->getClientOriginalName ();
				$hashname = $fullname;
				$upload_success = $files->move ( public_path ( $destinationPath ), $hashname );
				$image_path = $fullname;
			}
			$regi_mark_details = array (
					'reg_m_type' => $input ['registration_mark'],
					'reg_m_type_image' => $image_path 
			);
			$factsheet->registration_mark = json_encode ( $regi_mark_details );
		}
		
		if (isset ( $input ['registration_mark'] ) && $input ['registration_mark'] == "wmim") {
			$image_path = $input ['reg_mark_img_prev'];
			if ($request->hasFile ( 'reg_mark_img' )) {
				$files = $request->file ( 'reg_mark_img' );
				$destinationPath = 'Registration_Mark';
				$fullname = $files->getClientOriginalName ();
				$hashname = $fullname;
				$upload_success = $files->move ( public_path ( $destinationPath ), $hashname );
				$image_path = $fullname;
			}
			$regi_mark_details = array (
					'reg_m_type' => $input ['registration_mark'],
					'reg_m_type_value_title' => $input ['reg_mark_title'],
					'reg_m_type_image' => $image_path 
			);
			$factsheet->registration_mark = json_encode ( $regi_mark_details );
		}
		
		if (isset ( $input ['mark_used_from'] ) && $input ['mark_used_from'] == "date") {
			$mark_used_from = array (
					'm_u_f_type' => $input ['mark_used_from'],
					'm_u_f_value' => $input ['mark_used_from_date'] 
			);
			$factsheet->mark_used_from = json_encode ( $mark_used_from );
		}
		
		if (isset ( $input ['mark_used_from'] ) && $input ['mark_used_from'] == "ptbu") {
			$mark_used_from = array (
					'm_u_f_type' => $input ['mark_used_from'],
					'm_u_f_value' => $input ['proposed_to_be_used_txt'] 
			);
			$factsheet->mark_used_from = json_encode ( $mark_used_from );
		}
		
		if (isset ( $input ['class_of_reg'] ) && $input ['class_of_reg'] == "single") {
			$class_of_reg = array (
					'cof_type' => $input ['class_of_reg'],
					'cof_single_value' => $input ['cor_single'] 
			);
			$factsheet->class_of_registration = json_encode ( $class_of_reg );
		}
		
		if (isset ( $input ['class_of_reg'] ) && $input ['class_of_reg'] == "combo") {
			$class_of_reg = array (
					'cof_type' => 'combo',
					'cof_single_value' => $input ['cor_single'],
					'cof_combo_value' => $input ['cor_combo'] 
			);
			$factsheet->class_of_registration = json_encode ( $class_of_reg );
		}
		$factsheet->save ();
		
		Session::flash ( 'success', 'Factsheet updated successfully.' );
		return redirect ( '/factsheet' );
	}
	public function show_factsheet($id) {
		$show = FactSheet::find ( $id );
		$rm = "";
		
		if (! blank ( $show ['registration_mark'] )) {
			$reg_mark = json_decode ( $show ['registration_mark'], true );
			if ($reg_mark ['reg_m_type'] == "wm") {
				$rm = "Word Mark : " . $reg_mark ['reg_m_type_value_title'];
			}
			if ($reg_mark ['reg_m_type'] == "im") {
				$config_url = Config::get ( 'app.url' ) . '/Registration_Mark/' . $reg_mark ['reg_m_type_image'];
				$rm .= "<br/> Mark Image: <img width='100px' src='$config_url' />";
			}
			if ($reg_mark ['reg_m_type'] == "wmim") {
				$config_url = Config::get ( 'app.url' ) . '/Registration_Mark/' . $reg_mark ['reg_m_type_image'];
				$rm = "Word Mark : " . $reg_mark ['reg_m_type_value_title'];
				$rm .= "<br/> Mark Image: <img width='100px' src='$config_url' />";
			}
		}
		$mu = "";
		if (! blank ( $show ['mark_used_from'] )) {
			$mark_used_from = json_decode ( $show ['mark_used_from'], true );
			if ($mark_used_from ['m_u_f_type'] == 'date') {
				$mu = "Date :" . $mark_used_from ['m_u_f_value'];
			}
			if ($mark_used_from ['m_u_f_type'] == 'ptbu') {
				$mu = $mark_used_from ['m_u_f_value'];
			}
		}
		$cr = '';
		if (! blank ( $show ['class_of_registration'] )) {
			$class_reg = json_decode ( $show ['class_of_registration'], true );
			if ($class_reg ['cof_type'] == 'single') {
				$cr = "Single :" . $class_reg ['cof_single_value'];
			}
			if ($class_reg ['cof_type'] == 'combo') {
				$cr = "Single :" . $class_reg ['cof_single_value'];
				$cr .= "<br>Combo :" . $class_reg ['cof_combo_value'];
			}
		}
		$designation = DesignationList::where ( 'id', $show->designation )->first ();
		$approval_status = FactsheetApprovalProcess::where ( 'factsheet_id', $show->id )->first ();
		return view ( 'factsheet.show_factsheet', compact ( 'show', 'rm', 'mu', 'cr', 'designation', 'approval_status' ) );
	}
	public function factSheetDelete(Request $request) {
		$input = $request->all ();
		$factsheet = FactSheet::where ( 'id', $input ['id'] )->first ();
		if ($factsheet == null) {
			Session::flash ( 'success', 'Factsheet Not Found' );
			return redirect ( 'factsheet' );
		}
		$factsheet->delete ();
		Session::flash ( 'success', 'Factsheet deleted.' );
		return redirect ( '/factsheet' );
	}
	public function factSheetApprovalProcess(Request $request) {
		$input = $request->all ();
		
		$name_of_initiator = $input ['name_of_initiator'];
		$email_of_initiator = $input ['email_of_initiator'];
		$optional_for_initiator = $input ['optional_for_initiator'];
		
		// $name_of_initiator = 'Dinesh Chahande';
		// $email_of_initiator = 'dineshcmca2009@gmail.com';
		// $optional_for_initiator = '';
		
		$fact_sheet = FactSheet::where ( 'id', $input ['id'] )->first ();
		if ($fact_sheet == null) {
			Session::flash ( 'success', 'Factsheet not found.' );
			return redirect ( '/factsheet' );
		}
		$reg_mark = "";
		if (! blank ( $fact_sheet->registration_mark )) {
			$reg_mark = json_decode ( $fact_sheet->registration_mark, true );
		}
		$mark_used = "";
		if (! blank ( $fact_sheet->mark_used_from )) {
			$mark_used = json_decode ( $fact_sheet->mark_used_from, true );
		}
		$class_reg = "";
		if (! blank ( $fact_sheet->class_of_registration )) {
			$class_reg = json_decode ( $fact_sheet->class_of_registration, true );
		}
		
		$approval_process_array = config ( 'app.factsheet_ap_status' );
		
		$ap_pr_step = $approval_process_array ['Legal'];
		$legal_user = User::where ( 'designation', 'Legal' )->first ();
		$ap_pr_email = "";
		$dear_description = "Legal";
		if ($legal_user != null) {
			$ap_pr_email = $legal_user->email;
		}
		$option_comment = "N/A";
		if (! blank ( $optional_for_initiator )) {
			$ap_pr_step = $approval_process_array ['External'];
			$ap_pr_email = $optional_for_initiator;
			$dear_description = "User";
			$option_comment = "";
		}
		
		$following_line = "The following factsheet needs approval. You are requested to follow the link by clicking on the ID and provide your Approval/Disapproval with comment.";
		
		$data ['email_data'] = array (
				'id' => $fact_sheet->id,
				'factsheet_code' => $fact_sheet->factsheet_code,
				'nm' => $fact_sheet->name_of_initiator,
				'ds' => $fact_sheet->designation,
				'nm_ch' => $fact_sheet->name_of_channel,
				'reg_mark' => $reg_mark,
				'mark_used_from' => $mark_used,
				'class_reg' => $class_reg,
				'jur_reg' => $fact_sheet->jurisdiction_for_registration,
				'sig_work' => $fact_sheet->significance_of_work,
				'dear_details' => $dear_description,
				'following_line' => $following_line,
				'initiator_optional_approval_status' => 0,
				'legal_approval_status' => 0,
				'coo_approval_status' => 0,
				'cfo_approval_status' => 0,
				'ceo_approval_status' => 0 
		);
		
		$this->op_legal = $ap_pr_email;
		
		if (blank ( $this->op_legal )) {
			Session::flash ( 'success', 'Factsheet not shared. External & Legal email not available.' );
			return redirect ( '/factsheet' );
		}
		
		$factsheet_app_process = FactsheetApprovalProcess::where ( 'factsheet_id', $fact_sheet->id )->first ();
		if ($factsheet_app_process == null) {
			$factsheet_app_process = new FactsheetApprovalProcess ();
		}
		
		$factsheet_app_process->factsheet_id = $fact_sheet->id;
		$factsheet_app_process->initiator_id = Auth::user ()->id;
		$factsheet_app_process->initiator_agent_name = Auth::user ()->first_name . ' ' . Auth::user ()->last_name;
		$factsheet_app_process->initiator_agent_email = Auth::user ()->email;
		$factsheet_app_process->initiator_optional_id = 0;
		$factsheet_app_process->initiator_optional_agent_name = "";
		$factsheet_app_process->initiator_optional_agent_email = $optional_for_initiator;
		$factsheet_app_process->legal_id = 0;
		$factsheet_app_process->legal_agent_name = "";
		$factsheet_app_process->legal_agent_email = "";
		$factsheet_app_process->coo_id = 0;
		$factsheet_app_process->coo_agent_name = "";
		$factsheet_app_process->coo_agent_email = "";
		$factsheet_app_process->cfo_id = 0;
		$factsheet_app_process->cfo_agent_name = "";
		$factsheet_app_process->cfo_agent_email = "";
		$factsheet_app_process->ceo_id = 0;
		$factsheet_app_process->ceo_agent_name = "";
		$factsheet_app_process->ceo_agent_email = "";
		$factsheet_app_process->initiator_optional_agent_comment = "";
		$factsheet_app_process->factsheet_approval_status = 0;
		$factsheet_app_process->factsheet_status_steps = $ap_pr_step;
		$factsheet_app_process->initiator_agent_comment = "";
		$factsheet_app_process->legal_agent_comment = "";
		$factsheet_app_process->coo_agent_comment = "";
		$factsheet_app_process->cfo_agent_comment = "";
		$factsheet_app_process->ceo_agent_comment = "";
		$factsheet_app_process->initiator_optional_approval_status = 0;
		$factsheet_app_process->legal_approval_status = 0;
		$factsheet_app_process->coo_approval_status = 0;
		$factsheet_app_process->cfo_approval_status = 0;
		$factsheet_app_process->ceo_approval_status = 0;
		$factsheet_app_process->initiator_approval_back = 0;
		$factsheet_app_process->initiator_initiated_approval = 1;
		$factsheet_app_process->save ();
		
		$this->email_subject = "Factsheet Initiated By The Initiator";
		
		Mail::send ( 'factsheet.factsheet_email_template', $data, function ($message) {
			$message->subject ( $this->email_subject );
			$message->from ( $this->mail_from );
			$message->cc ( $this->mail_admin );
			$message->to ( $this->op_legal );
		} );
		
		Session::flash ( 'success', 'Factsheet shared.' );
		return redirect ( '/factsheet' );
	}
	public function factSheetInitiatorAp($id) {
		$fact_sheet = FactSheet::where ( 'factsheet_code', $id )->first ();
		$step = 1;
		if ($fact_sheet == null) {
			Session::flash ( 'success', 'Factsheet not found.' );
			return redirect ( '/factsheet' );
		}
		$reg_mark = "";
		if (! blank ( $fact_sheet->registration_mark )) {
			$reg_mark = json_decode ( $fact_sheet->registration_mark, true );
		}
		$mark_used = "";
		if (! blank ( $fact_sheet->mark_used_from )) {
			$mark_used = json_decode ( $fact_sheet->mark_used_from, true );
		}
		$class_reg = "";
		if (! blank ( $fact_sheet->class_of_registration )) {
			$class_reg = json_decode ( $fact_sheet->class_of_registration, true );
		}
		
		$is_approvel_process = FactsheetApprovalProcess::where ( 'factsheet_id', $fact_sheet->id )->first ();
		if ($is_approvel_process != null) {
			$step = $is_approvel_process->factsheet_status_steps;
		}
		
		$option_comment = "N/A";
		if (! blank ( $is_approvel_process->initiator_optional_agent_email )) {
			$option_comment = "Pending";
		}
		$legal_comment = "Pending";
		$coo_comment = "Pending";
		$cfo_comment = "Pending";
		$ceo_comment = "Pending";
		
		$dear_description = "Approval";
		$following_line = "The following factsheet needs approval. You are requested to follow the link by clicking on the Name Of Initiator and provide your Approval/Disapproval with comment.";
		$approval_process_array = config ( 'app.factsheet_ap_status' );
		foreach ( config ( 'app.factsheet_ap_status' ) as $ap_step_key => $ap_steps ) {
			if (! blank ( $is_approvel_process->initiator_optional_agent_email )) {
				if ($ap_steps == $approval_process_array ['External']) {
					$option_comment = $is_approvel_process->initiator_optional_agent_comment;
				}
				$dear_description = "Approval";
				$following_line = "The following factsheet has been Approved from Optional Agent (". $is_approvel_process->initiator_optional_agent_email ."). You are requested to follow the link by clicking on Name Of Initiator and provide your comment";
			}
			if ($ap_steps == $approval_process_array ['Legal']) {
				$legal_comment = $is_approvel_process->legal_agent_comment;
				$dear_description = "Legal Head";
				$following_line = "The following factsheet has been Approved from Legal. You are requested to follow the link by clicking on Name Of Initiator and provide your comment";
			}
			if ($ap_steps == $approval_process_array ['COO']) {
				$coo_comment = $is_approvel_process->coo_agent_comment;
				$dear_description = "COO";
				$following_line = "The following factsheet has been Approved from COO. You are requested to follow the link by clicking on Name Of Initiator and provide your comment";
			}
			if ($ap_steps == $approval_process_array ['CFO']) {
				$cfo_comment = $is_approvel_process->cfo_agent_comment;
				$dear_description = "CFO";
				$following_line = "The following factsheet has been Approved from CFO. You are requested to follow the link by clicking on Name Of Initiator and provide your comment";
			}
			if ($ap_steps == $approval_process_array ['CEO']) {
				$ceo_comment = $is_approvel_process->ceo_agent_comment;
				$dear_description = "CEO";
				$following_line = "The following factsheet has been Approved from CEO. You are requested to follow the link by clicking on Name Of Initiator and provide your comment";
			}
		}
		
		$email_data = array (
				'id' => $fact_sheet->id,
				'nm' => $fact_sheet->name_of_initiator,
				'ds' => $fact_sheet->designation,
				'nm_ch' => $fact_sheet->name_of_channel,
				'reg_mark' => $reg_mark,
				'mark_used_from' => $mark_used,
				'class_reg' => $class_reg,
				'jur_reg' => $fact_sheet->jurisdiction_for_registration,
				'sig_work' => $fact_sheet->significance_of_work,
				'factsheet' => $fact_sheet->id,
				'ap_p_status' => $step,
				'option_comment' => blank ( $option_comment ) ? 'Pending' : $option_comment,
				'legal_comment' => blank ( $legal_comment ) ? 'Pending' : $legal_comment,
				'coo_comment' => blank ( $coo_comment ) ? 'Pending' : $coo_comment,
				'cfo_comment' => blank ( $cfo_comment ) ? 'Pending' : $cfo_comment,
				'ceo_comment' => blank ( $ceo_comment ) ? 'Pending' : $ceo_comment,
				'dear_details' => $dear_description,
				'initiator_optional_approval_status' => $is_approvel_process->initiator_optional_approval_status,
				'legal_approval_status' => $is_approvel_process->legal_approval_status,
				'coo_approval_status' => $is_approvel_process->coo_approval_status,
				'cfo_approval_status' => $is_approvel_process->cfo_approval_status,
				'ceo_approval_status' => $is_approvel_process->ceo_approval_status,
				'following_line' => $following_line 
		);
		
		$is_comming_from_email = 1;
		return view ( 'factsheet.factsheet_status_receiver' )->with ( 'email_data', $email_data )->with ( 'is_comming_from_email', $is_comming_from_email );
	}
	public function factSheetStatusShared(Request $request) {
		$input = $request->all ();
		
		$factsheet = FactSheet::where ( 'id', $input ['factsheet_id'] )->first ();
		if ($factsheet == null) {
			Session::flash ( 'success', 'Factsheet not found.' );
			return redirect ()->back ();
		}
		
		$factsheet_ap_pr = FactsheetApprovalProcess::where ( 'factsheet_id', $factsheet->id )->first ();
		if ($factsheet_ap_pr == null) {
			Session::flash ( 'success', 'Something went wrong. Please try again later.' );
			return redirect ()->back ();
		}
		
		if ($factsheet_ap_pr->factsheet_approval_status == 1) {
			Session::flash ( 'success', 'All approval accepted.' );
			return redirect ()->back ();
		}
		
		$approval_process_array = config ( 'app.factsheet_ap_status' );
		
		if ($input ['step'] == $approval_process_array ['External']) {
			if ($input ['submit'] == 'Approved') {
				$factsheet_ap_pr->initiator_optional_agent_comment = $input ['comment'];
				$factsheet_ap_pr->factsheet_status_steps = $approval_process_array ['Legal'];
				$factsheet_ap_pr->initiator_optional_approval_status = 1;
				$factsheet_ap_pr->save ();
				$this->throwFactsheetApprovalProcessEmail ( $factsheet, $factsheet_ap_pr );
				Session::flash ( 'success', 'Comment shared.' );
				$is_comming_from_email = 0;
				return redirect ( 'shared_success' );
			}
			if ($input ['submit'] == 'Disapproved') {
				$factsheet_ap_pr->initiator_optional_agent_comment = $input ['comment'];
				$factsheet_ap_pr->initiator_optional_approval_status = 2;
				$factsheet_ap_pr->factsheet_status_steps = $approval_process_array ['Initiator'];
				$factsheet_ap_pr->initiator_approval_back = $approval_process_array ['External'];
				$factsheet_ap_pr->save ();
				$this->throwFactsheetDisApprovalProcessEmail ( $factsheet, $factsheet_ap_pr );
				Session::flash ( 'success', 'Comment shared.' );
				$is_comming_from_email = 0;
				return redirect ( 'shared_success' );
			}
		}
		
		if ($input ['step'] == $approval_process_array ['Legal']) {
			if ($input ['submit'] == 'Approved') {
				$user = User::where ( 'designation', 'Legal' )->first ();
				if ($user != null) {
					$factsheet_ap_pr->legal_agent_comment = $input ['comment'];
					$factsheet_ap_pr->legal_agent_name = $user->first_name . ' ' . $user->last_name;
					$factsheet_ap_pr->legal_agent_email = $user->email;
					$factsheet_ap_pr->legal_id = $user->id;
					$factsheet_ap_pr->legal_approval_status = 1;
					$factsheet_ap_pr->factsheet_status_steps = $approval_process_array ['COO'];
					$factsheet_ap_pr->save ();
					$this->throwFactsheetApprovalProcessEmail ( $factsheet, $factsheet_ap_pr );
					Session::flash ( 'success', 'Comment shared.' );
					$is_comming_from_email = 0;
					return redirect ( 'shared_success' );
				}
			}
			if ($input ['submit'] == 'Disapproved') {
				$user = User::where ( 'designation', 'Legal' )->first ();
				if ($user != null) {
					$factsheet_ap_pr->legal_agent_comment = $input ['comment'];
					$factsheet_ap_pr->legal_agent_name = $user->first_name . ' ' . $user->last_name;
					$factsheet_ap_pr->legal_agent_email = $user->email;
					$factsheet_ap_pr->legal_id = $user->id;
					$factsheet_ap_pr->legal_approval_status = 2;
					$factsheet_ap_pr->factsheet_status_steps = $approval_process_array ['External'];
					$factsheet_ap_pr->initiator_approval_back = $approval_process_array ['Legal'];
					$factsheet_ap_pr->save ();
					$this->throwFactsheetDisApprovalProcessEmail ( $factsheet, $factsheet_ap_pr );
					Session::flash ( 'success', 'Comment shared.' );
					$is_comming_from_email = 0;
					return redirect ( 'shared_success' );
				}
			}
		}
		
		if ($input ['step'] == $approval_process_array ['COO']) {
			if ($input ['submit'] == 'Approved') {
				$user = User::where ( 'designation', 'COO' )->first ();
				if ($user != null) {
					$factsheet_ap_pr->coo_agent_comment = $input ['comment'];
					$factsheet_ap_pr->coo_agent_name = $user->first_name . ' ' . $user->last_name;
					$factsheet_ap_pr->coo_agent_email = $user->email;
					$factsheet_ap_pr->coo_id = $user->id;
					$factsheet_ap_pr->coo_approval_status = 1;
					$factsheet_ap_pr->factsheet_status_steps = $approval_process_array ['CFO'];
					$factsheet_ap_pr->save ();
					
					$this->throwFactsheetApprovalProcessEmail ( $factsheet, $factsheet_ap_pr );
					Session::flash ( 'success', 'Comment shared.' );
					$is_comming_from_email = 0;
					return redirect ( 'shared_success' );
				}
			}
			
			if ($input ['submit'] == 'Disapproved') {
				$user = User::where ( 'designation', 'COO' )->first ();
				if ($user != null) {
					$factsheet_ap_pr->coo_agent_comment = $input ['comment'];
					$factsheet_ap_pr->coo_agent_name = $user->first_name . ' ' . $user->last_name;
					$factsheet_ap_pr->coo_agent_email = $user->email;
					$factsheet_ap_pr->coo_id = $user->id;
					$factsheet_ap_pr->coo_approval_status = 2;
					$factsheet_ap_pr->factsheet_status_steps = $approval_process_array ['Legal'];
					$factsheet_ap_pr->initiator_approval_back = $approval_process_array ['COO'];
					$factsheet_ap_pr->save ();
					$this->throwFactsheetDisApprovalProcessEmail ( $factsheet, $factsheet_ap_pr );
					Session::flash ( 'success', 'Comment shared.' );
					$is_comming_from_email = 0;
					return redirect ( 'shared_success' );
				}
			}
		}
		
		if ($input ['step'] == $approval_process_array ['CFO']) {
			if ($input ['submit'] == 'Approved') {
				$user = User::where ( 'designation', 'CFO' )->first ();
				if ($user != null) {
					$factsheet_ap_pr->cfo_agent_comment = $input ['comment'];
					$factsheet_ap_pr->cfo_agent_name = $user->first_name . ' ' . $user->last_name;
					$factsheet_ap_pr->cfo_agent_email = $user->email;
					$factsheet_ap_pr->cfo_id = $user->id;
					$factsheet_ap_pr->cfo_approval_status = 1;
					$factsheet_ap_pr->factsheet_status_steps = $approval_process_array ['CEO'];
					$factsheet_ap_pr->save ();
					
					$this->throwFactsheetApprovalProcessEmail ( $factsheet, $factsheet_ap_pr );
					Session::flash ( 'success', 'Comment shared.' );
					$is_comming_from_email = 0;
					return redirect ( 'shared_success' );
				}
			}
			if ($input ['submit'] == 'Disapproved') {
				$user = User::where ( 'designation', 'CFO' )->first ();
				if ($user != null) {
					$factsheet_ap_pr->cfo_agent_comment = $input ['comment'];
					$factsheet_ap_pr->cfo_agent_name = $user->first_name . ' ' . $user->last_name;
					$factsheet_ap_pr->cfo_agent_email = $user->email;
					$factsheet_ap_pr->cfo_id = $user->id;
					$factsheet_ap_pr->cfo_approval_status = 2;
					$factsheet_ap_pr->factsheet_status_steps = $approval_process_array ['COO'];
					$factsheet_ap_pr->initiator_approval_back = $approval_process_array ['CFO'];
					$factsheet_ap_pr->save ();
					$this->throwFactsheetDisApprovalProcessEmail ( $factsheet, $factsheet_ap_pr );
					Session::flash ( 'success', 'Comment shared.' );
					$is_comming_from_email = 0;
					return redirect ( 'shared_success' );
				}
			}
		}
		
		if ($input ['step'] == $approval_process_array ['CEO']) {
			if ($input ['submit'] == 'Approved') {
				$user = User::where ( 'designation', 'CEO' )->first ();
				if ($user != null) {
					$factsheet_ap_pr->ceo_agent_comment = $input ['comment'];
					$factsheet_ap_pr->ceo_agent_name = $user->first_name . ' ' . $user->last_name;
					$factsheet_ap_pr->ceo_agent_email = $user->email;
					$factsheet_ap_pr->ceo_id = $user->id;
					$factsheet_ap_pr->ceo_approval_status = 1;
					$factsheet_ap_pr->factsheet_status_steps = $approval_process_array ['CEO'];
					$factsheet_ap_pr->factsheet_approval_status = 1;
					$factsheet_ap_pr->save ();
					
					$this->throwFactsheetApprovalProcessEmail ( $factsheet, $factsheet_ap_pr );
					Session::flash ( 'success', 'Comment shared.' );
					$is_comming_from_email = 0;
					return redirect ( 'shared_success' );
				}
			}
			
			if ($input ['submit'] == 'Disapproved') {
				$user = User::where ( 'designation', 'CEO' )->first ();
				if ($user != null) {
					$factsheet_ap_pr->ceo_agent_comment = $input ['comment'];
					$factsheet_ap_pr->ceo_agent_name = $user->first_name . ' ' . $user->last_name;
					$factsheet_ap_pr->ceo_agent_email = $user->email;
					$factsheet_ap_pr->ceo_id = $user->id;
					$factsheet_ap_pr->ceo_approval_status = 2;
					$factsheet_ap_pr->factsheet_status_steps = $approval_process_array ['CFO'];
					$factsheet_ap_pr->initiator_approval_back = $approval_process_array ['CEO'];
					$factsheet_ap_pr->factsheet_approval_status = 0;
					$factsheet_ap_pr->save ();
					$this->throwFactsheetDisApprovalProcessEmail ( $factsheet, $factsheet_ap_pr );
					Session::flash ( 'success', 'Comment shared.' );
					$is_comming_from_email = 0;
					return redirect ( 'shared_success' );
				}
			}
		}
		
		Session::flash ( 'success', 'Comment shared.' );
		$is_comming_from_email = 0;
		return redirect ( 'shared_success' );
	}
	public function throwFactsheetApprovalProcessEmail($fact_sheet, $factsheet_approval_process) {
		$data = [ ];
		$reg_mark = "";
		if (! blank ( $fact_sheet->registration_mark )) {
			$reg_mark = json_decode ( $fact_sheet->registration_mark, true );
		}
		$mark_used = "";
		if (! blank ( $fact_sheet->mark_used_from )) {
			$mark_used = json_decode ( $fact_sheet->mark_used_from, true );
		}
		$class_reg = "";
		if (! blank ( $fact_sheet->class_of_registration )) {
			$class_reg = json_decode ( $fact_sheet->class_of_registration, true );
		}
		
		$next_shared = $factsheet_approval_process->factsheet_status_steps;
		
		$next_share_user_role = "";
		foreach ( config ( 'app.factsheet_ap_status' ) as $ap_step_key => $ap_steps ) {
			if ($ap_steps == $next_shared) {
				$next_share_user_role = $ap_step_key;
			}
		}
		
		if (! blank ( $next_share_user_role )) {
			$user = User::where ( 'designation', $next_share_user_role )->first ();
			if ($user != null) {
				$this->followed_user = $user->email;
				$approval_process_array = config ( 'app.factsheet_ap_status' );
				$option_comment = "N/A";
				if (! blank ( $factsheet_approval_process->initiator_optional_agent_email )) {
					$option_comment = "Pending";
				}
				$legal_comment = "Pending";
				$coo_comment = "Pending";
				$cfo_comment = "Pending";
				$ceo_comment = "Pending";
				
				$dear_description = "User";
				$following_line = "The following factsheet needs approval. You are requested to follow the link by clicking on the ID and provide your Approval/Disapproval with comment.";
				if ($factsheet_approval_process->factsheet_status_steps == $approval_process_array ['External']) {
					$option_comment = $factsheet_approval_process->initiator_optional_agent_comment;
					$dear_description = "User";
					$this->email_subject = "Factsheet Initiated By The Initiator";
					$following_line = "The following factsheet has been Approved from External User. You are requested to follow the link by clicking on the ID and provide your comment";
					$option_comment = $factsheet_approval_process->initiator_optional_agent_comment;
					$legal_comment = $factsheet_approval_process->legal_agent_comment;
					$coo_comment = $factsheet_approval_process->coo_agent_comment;
					$cfo_comment = $factsheet_approval_process->cfo_agent_comment;
					$ceo_comment = $factsheet_approval_process->ceo_agent_comment;
					$data ['email_data'] = array (
							'id' => $fact_sheet->id,
							'factsheet_code' => $fact_sheet->factsheet_code,
							'nm' => $fact_sheet->name_of_initiator,
							'ds' => $fact_sheet->designation,
							'nm_ch' => $fact_sheet->name_of_channel,
							'reg_mark' => $reg_mark,
							'mark_used_from' => $mark_used,
							'class_reg' => $class_reg,
							'jur_reg' => $fact_sheet->jurisdiction_for_registration,
							'sig_work' => $fact_sheet->significance_of_work,
							'option_comment' => blank ( $option_comment ) ? 'Pending' : $option_comment,
							'legal_comment' => blank ( $legal_comment ) ? 'Pending' : $legal_comment,
							'coo_comment' => blank ( $coo_comment ) ? 'Pending' : $coo_comment,
							'cfo_comment' => blank ( $cfo_comment ) ? 'Pending' : $cfo_comment,
							'ceo_comment' => blank ( $ceo_comment ) ? 'Pending' : $ceo_comment,
							'dear_details' => $dear_description,
							'following_line' => $following_line,
							'initiator_optional_approval_status' => $factsheet_approval_process->initiator_optional_approval_status,
							'legal_approval_status' => $factsheet_approval_process->legal_approval_status,
							'coo_approval_status' => $factsheet_approval_process->coo_approval_status,
							'cfo_approval_status' => $factsheet_approval_process->cfo_approval_status,
							'ceo_approval_status' => $factsheet_approval_process->ceo_approval_status 
					);
					
					Mail::send ( 'factsheet.factsheet_email_template', $data, function ($message) {
						$message->subject ( $this->email_subject );
						$message->from ( $this->mail_from );
						$message->to ( $this->followed_user );
						$message->cc ( $this->mail_admin );
					} );
					
					return $data;
				}
				if ($factsheet_approval_process->factsheet_status_steps == $approval_process_array ['Legal']) {
					$legal_comment = $factsheet_approval_process->legal_agent_comment;
					$dear_description = "Legal Head";
					$this->email_subject = "Factsheet Approved By The External User";
					$following_line = "The following factsheet has been Approved from External User (". $factsheet_approval_process->initiator_optional_agent_email ."). You are requested to follow the link by clicking on the ID and provide your comment";
					$option_comment = $factsheet_approval_process->initiator_optional_agent_comment;
					$legal_comment = $factsheet_approval_process->legal_agent_comment;
					$coo_comment = $factsheet_approval_process->coo_agent_comment;
					$cfo_comment = $factsheet_approval_process->cfo_agent_comment;
					$ceo_comment = $factsheet_approval_process->ceo_agent_comment;
					$data ['email_data'] = array (
							'id' => $fact_sheet->id,
							'factsheet_code' => $fact_sheet->factsheet_code,
							'nm' => $fact_sheet->name_of_initiator,
							'ds' => $fact_sheet->designation,
							'nm_ch' => $fact_sheet->name_of_channel,
							'reg_mark' => $reg_mark,
							'mark_used_from' => $mark_used,
							'class_reg' => $class_reg,
							'jur_reg' => $fact_sheet->jurisdiction_for_registration,
							'sig_work' => $fact_sheet->significance_of_work,
							'option_comment' => blank ( $option_comment ) ? 'Pending' : $option_comment,
							'legal_comment' => blank ( $legal_comment ) ? 'Pending' : $legal_comment,
							'coo_comment' => blank ( $coo_comment ) ? 'Pending' : $coo_comment,
							'cfo_comment' => blank ( $cfo_comment ) ? 'Pending' : $cfo_comment,
							'ceo_comment' => blank ( $ceo_comment ) ? 'Pending' : $ceo_comment,
							'dear_details' => $dear_description,
							'following_line' => $following_line,
							'initiator_optional_approval_status' => $factsheet_approval_process->initiator_optional_approval_status,
							'legal_approval_status' => $factsheet_approval_process->legal_approval_status,
							'coo_approval_status' => $factsheet_approval_process->coo_approval_status,
							'cfo_approval_status' => $factsheet_approval_process->cfo_approval_status,
							'ceo_approval_status' => $factsheet_approval_process->ceo_approval_status 
					);
					
					Mail::send ( 'factsheet.factsheet_email_template', $data, function ($message) {
						$message->subject ( $this->email_subject );
						$message->from ( $this->mail_from );
						$message->to ( $this->followed_user );
						$message->cc ( $this->mail_admin );
					} );
					
					return $data;
				}
				if ($factsheet_approval_process->factsheet_status_steps == $approval_process_array ['COO']) {
					$coo_comment = $factsheet_approval_process->coo_agent_comment;
					$dear_description = "COO";
					$this->email_subject = "Factsheet Approved By The Legal";
					$following_line = "The following factsheet has been Approved from Legal. You are requested to follow the link by clicking on the ID and provide your comment";
					$option_comment = $factsheet_approval_process->initiator_optional_agent_comment;
					$legal_comment = $factsheet_approval_process->legal_agent_comment;
					$coo_comment = $factsheet_approval_process->coo_agent_comment;
					$cfo_comment = $factsheet_approval_process->cfo_agent_comment;
					$ceo_comment = $factsheet_approval_process->ceo_agent_comment;
					$data ['email_data'] = array (
							'id' => $fact_sheet->id,
							'factsheet_code' => $fact_sheet->factsheet_code,
							'nm' => $fact_sheet->name_of_initiator,
							'ds' => $fact_sheet->designation,
							'nm_ch' => $fact_sheet->name_of_channel,
							'reg_mark' => $reg_mark,
							'mark_used_from' => $mark_used,
							'class_reg' => $class_reg,
							'jur_reg' => $fact_sheet->jurisdiction_for_registration,
							'sig_work' => $fact_sheet->significance_of_work,
							'option_comment' => blank ( $option_comment ) ? 'Pending' : $option_comment,
							'legal_comment' => blank ( $legal_comment ) ? 'Pending' : $legal_comment,
							'coo_comment' => blank ( $coo_comment ) ? 'Pending' : $coo_comment,
							'cfo_comment' => blank ( $cfo_comment ) ? 'Pending' : $cfo_comment,
							'ceo_comment' => blank ( $ceo_comment ) ? 'Pending' : $ceo_comment,
							'dear_details' => $dear_description,
							'following_line' => $following_line,
							'initiator_optional_approval_status' => $factsheet_approval_process->initiator_optional_approval_status,
							'legal_approval_status' => $factsheet_approval_process->legal_approval_status,
							'coo_approval_status' => $factsheet_approval_process->coo_approval_status,
							'cfo_approval_status' => $factsheet_approval_process->cfo_approval_status,
							'ceo_approval_status' => $factsheet_approval_process->ceo_approval_status 
					);
					
					Mail::send ( 'factsheet.factsheet_email_template', $data, function ($message) {
						$message->subject ( $this->email_subject );
						$message->from ( $this->mail_from );
						$message->to ( $this->followed_user );
						$message->cc ( $this->mail_admin );
					} );
					
					return $data;
				}
				if ($factsheet_approval_process->factsheet_status_steps == $approval_process_array ['CFO']) {
					$cfo_comment = $factsheet_approval_process->cfo_agent_comment;
					$dear_description = "CFO";
					$this->email_subject = "Factsheet Approved By The COO";
					$following_line = "The following factsheet has been Approved from COO. You are requested to follow the link by clicking on the ID and provide your comment";
					$option_comment = $factsheet_approval_process->initiator_optional_agent_comment;
					$legal_comment = $factsheet_approval_process->legal_agent_comment;
					$coo_comment = $factsheet_approval_process->coo_agent_comment;
					$cfo_comment = $factsheet_approval_process->cfo_agent_comment;
					$ceo_comment = $factsheet_approval_process->ceo_agent_comment;
					$data ['email_data'] = array (
							'id' => $fact_sheet->id,
							'factsheet_code' => $fact_sheet->factsheet_code,
							'nm' => $fact_sheet->name_of_initiator,
							'ds' => $fact_sheet->designation,
							'nm_ch' => $fact_sheet->name_of_channel,
							'reg_mark' => $reg_mark,
							'mark_used_from' => $mark_used,
							'class_reg' => $class_reg,
							'jur_reg' => $fact_sheet->jurisdiction_for_registration,
							'sig_work' => $fact_sheet->significance_of_work,
							'option_comment' => blank ( $option_comment ) ? 'Pending' : $option_comment,
							'legal_comment' => blank ( $legal_comment ) ? 'Pending' : $legal_comment,
							'coo_comment' => blank ( $coo_comment ) ? 'Pending' : $coo_comment,
							'cfo_comment' => blank ( $cfo_comment ) ? 'Pending' : $cfo_comment,
							'ceo_comment' => blank ( $ceo_comment ) ? 'Pending' : $ceo_comment,
							'dear_details' => $dear_description,
							'following_line' => $following_line,
							'initiator_optional_approval_status' => $factsheet_approval_process->initiator_optional_approval_status,
							'legal_approval_status' => $factsheet_approval_process->legal_approval_status,
							'coo_approval_status' => $factsheet_approval_process->coo_approval_status,
							'cfo_approval_status' => $factsheet_approval_process->cfo_approval_status,
							'ceo_approval_status' => $factsheet_approval_process->ceo_approval_status 
					);
					
					Mail::send ( 'factsheet.factsheet_email_template', $data, function ($message) {
						$message->subject ( $this->email_subject );
						$message->from ( $this->mail_from );
						$message->to ( $this->followed_user );
					} );
					
					return $data;
				}
				if ($factsheet_approval_process->factsheet_status_steps == $approval_process_array ['CEO']) {
					$ceo_comment = $factsheet_approval_process->ceo_agent_comment;
					$dear_description = "CEO";
					$this->email_subject = "Factsheet Approved By The CFO";
					$following_line = "The following factsheet has been Approved from CFO. You are requested to follow the link by clicking on the ID and provide your comment";
					$option_comment = $factsheet_approval_process->initiator_optional_agent_comment;
					$legal_comment = $factsheet_approval_process->legal_agent_comment;
					$coo_comment = $factsheet_approval_process->coo_agent_comment;
					$cfo_comment = $factsheet_approval_process->cfo_agent_comment;
					$ceo_comment = $factsheet_approval_process->ceo_agent_comment;
					$data ['email_data'] = array (
							'id' => $fact_sheet->id,
							'factsheet_code' => $fact_sheet->factsheet_code,
							'nm' => $fact_sheet->name_of_initiator,
							'ds' => $fact_sheet->designation,
							'nm_ch' => $fact_sheet->name_of_channel,
							'reg_mark' => $reg_mark,
							'mark_used_from' => $mark_used,
							'class_reg' => $class_reg,
							'jur_reg' => $fact_sheet->jurisdiction_for_registration,
							'sig_work' => $fact_sheet->significance_of_work,
							'option_comment' => blank ( $option_comment ) ? 'Pending' : $option_comment,
							'legal_comment' => blank ( $legal_comment ) ? 'Pending' : $legal_comment,
							'coo_comment' => blank ( $coo_comment ) ? 'Pending' : $coo_comment,
							'cfo_comment' => blank ( $cfo_comment ) ? 'Pending' : $cfo_comment,
							'ceo_comment' => blank ( $ceo_comment ) ? 'Pending' : $ceo_comment,
							'dear_details' => $dear_description,
							'following_line' => $following_line,
							'initiator_optional_approval_status' => $factsheet_approval_process->initiator_optional_approval_status,
							'legal_approval_status' => $factsheet_approval_process->legal_approval_status,
							'coo_approval_status' => $factsheet_approval_process->coo_approval_status,
							'cfo_approval_status' => $factsheet_approval_process->cfo_approval_status,
							'ceo_approval_status' => $factsheet_approval_process->ceo_approval_status
					);
					
					Mail::send ( 'factsheet.factsheet_email_template', $data, function ($message) {
						$message->subject ( $this->email_subject );
						$message->from ( $this->mail_from );
						$message->to ( $this->followed_user );
						$message->cc ( $this->mail_admin );
					} );
					
					return $data;
				}
			}
		}
		return $data;
	}
	public function factSheetStatusSharedSuccess() {
		return view ( 'factsheet.factsheet_status_receiver_reponse' );
	}
	public function throwFactsheetDisApprovalProcessEmail($fact_sheet, $factsheet_approval_process) {
		$data = [ ];
		$log_data = [ ];
		$reg_mark = "";
		if (! blank ( $fact_sheet->registration_mark )) {
			$reg_mark = json_decode ( $fact_sheet->registration_mark, true );
		}
		$mark_used = "";
		if (! blank ( $fact_sheet->mark_used_from )) {
			$mark_used = json_decode ( $fact_sheet->mark_used_from, true );
		}
		$class_reg = "";
		if (! blank ( $fact_sheet->class_of_registration )) {
			$class_reg = json_decode ( $fact_sheet->class_of_registration, true );
		}
		
		$disapproved_by = "";
		
		foreach ( config ( 'app.factsheet_ap_status' ) as $ap_step_dis_key => $ap_dis_steps ) {
			if ($factsheet_approval_process->initiator_approval_back == $ap_dis_steps) {
				$disapproved_by = $ap_step_dis_key;
			}
		}
		
		foreach ( config ( 'app.factsheet_ap_status' ) as $ap_step_key => $ap_steps ) {
			if ($ap_steps < $factsheet_approval_process->initiator_approval_back) {
				if ($ap_steps == 1) {
					$user = User::where ( 'id', $factsheet_approval_process->initiator_id )->first ();
					if ($user != null) {
						$this->followed_user_initiator = $user->email;
						$this->disapproved_subject = "Factsheet Disapproved By The " . $disapproved_by;
						$dear_description = "Initiator";
						
						$option_comment = $factsheet_approval_process->initiator_optional_agent_comment;
						$legal_comment = $factsheet_approval_process->legal_agent_comment;
						$coo_comment = $factsheet_approval_process->coo_agent_comment;
						$cfo_comment = $factsheet_approval_process->cfo_agent_comment;
						$ceo_comment = $factsheet_approval_process->ceo_agent_comment;
						
						$following_line = "The following factsheet has been Disapproved from ".$disapproved_by." user. You are requested to verify the factsheet and share again.";
						$data ['email_data'] = array (
								'id' => $fact_sheet->id,
								'factsheet_code' => $fact_sheet->factsheet_code,
								'nm' => $fact_sheet->name_of_initiator,
								'ds' => $fact_sheet->designation,
								'nm_ch' => $fact_sheet->name_of_channel,
								'reg_mark' => $reg_mark,
								'mark_used_from' => $mark_used,
								'class_reg' => $class_reg,
								'jur_reg' => $fact_sheet->jurisdiction_for_registration,
								'sig_work' => $fact_sheet->significance_of_work,
								'option_comment' => blank ( $option_comment ) ? 'Pending' : $option_comment,
								'legal_comment' => blank ( $legal_comment ) ? 'Pending' : $legal_comment,
								'coo_comment' => blank ( $coo_comment ) ? 'Pending' : $coo_comment,
								'cfo_comment' => blank ( $cfo_comment ) ? 'Pending' : $cfo_comment,
								'ceo_comment' => blank ( $ceo_comment ) ? 'Pending' : $ceo_comment,
								'dear_details' => $dear_description,
								'following_line' => $following_line,
								'initiator_optional_approval_status' => $factsheet_approval_process->initiator_optional_approval_status,
								'legal_approval_status' => $factsheet_approval_process->legal_approval_status,
								'coo_approval_status' => $factsheet_approval_process->coo_approval_status,
								'cfo_approval_status' => $factsheet_approval_process->cfo_approval_status,
								'ceo_approval_status' => $factsheet_approval_process->ceo_approval_status,
								'initiator_approval_back' => $factsheet_approval_process->initiator_approval_back,
								'factsheet_status_steps' => $factsheet_approval_process->factsheet_status_steps
						);
						
						/*
						Mail::send ( 'factsheet.factsheet_email_template_disapproved', $data, function ($message) {
							$message->subject ( $this->disapproved_subject );
							$message->from ( $this->mail_from );
							$message->to ( $this->followed_user );
							$message->cc ( $this->mail_admin );
						} );
						*/
					}
					$log_data[] = $dear_description;
					Log::info($log_data);
				}
				if ($ap_steps == 2) {
					if (!blank($factsheet_approval_process->initiator_agent_email)) {
						$this->disapproved_followed_user [] = $factsheet_approval_process->initiator_agent_email;
						$this->disapproved_subject = "Factsheet Disapproved By The " . $disapproved_by;
						$dear_description = "Initiator";
						
						$option_comment = $factsheet_approval_process->initiator_optional_agent_comment;
						$legal_comment = $factsheet_approval_process->legal_agent_comment;
						$coo_comment = $factsheet_approval_process->coo_agent_comment;
						$cfo_comment = $factsheet_approval_process->cfo_agent_comment;
						$ceo_comment = $factsheet_approval_process->ceo_agent_comment;
						
						$following_line = "The following factsheet has been Disapproved from ".$disapproved_by." user. You are requested to verify the factsheet and share again.";
						$data ['email_data'] = array (
								'id' => $fact_sheet->id,
								'factsheet_code' => $fact_sheet->factsheet_code,
								'nm' => $fact_sheet->name_of_initiator,
								'ds' => $fact_sheet->designation,
								'nm_ch' => $fact_sheet->name_of_channel,
								'reg_mark' => $reg_mark,
								'mark_used_from' => $mark_used,
								'class_reg' => $class_reg,
								'jur_reg' => $fact_sheet->jurisdiction_for_registration,
								'sig_work' => $fact_sheet->significance_of_work,
								'option_comment' => blank ( $option_comment ) ? 'Pending' : $option_comment,
								'legal_comment' => blank ( $legal_comment ) ? 'Pending' : $legal_comment,
								'coo_comment' => blank ( $coo_comment ) ? 'Pending' : $coo_comment,
								'cfo_comment' => blank ( $cfo_comment ) ? 'Pending' : $cfo_comment,
								'ceo_comment' => blank ( $ceo_comment ) ? 'Pending' : $ceo_comment,
								'dear_details' => $dear_description,
								'following_line' => $following_line,
								'initiator_optional_approval_status' => $factsheet_approval_process->initiator_optional_approval_status,
								'legal_approval_status' => $factsheet_approval_process->legal_approval_status,
								'coo_approval_status' => $factsheet_approval_process->coo_approval_status,
								'cfo_approval_status' => $factsheet_approval_process->cfo_approval_status,
								'ceo_approval_status' => $factsheet_approval_process->ceo_approval_status,
								'initiator_approval_back' => $factsheet_approval_process->initiator_approval_back,
								'factsheet_status_steps' => $factsheet_approval_process->factsheet_status_steps
						);
						
						/*
						Mail::send ( 'factsheet.factsheet_email_template_disapproved', $data, function ($message) {
							$message->subject ( $this->disapproved_subject );
							$message->from ( $this->mail_from );
							$message->to ( $this->followed_user );
							$message->cc ( $this->mail_admin );
						} );
						*/
					}
					$log_data[] = $dear_description;
					Log::info($log_data);
				}
				
				$user = User::where ( 'designation', $ap_step_key)->first ();
				if ($user != null) {
					$this->disapproved_followed_user [] = $user->email;
					$this->disapproved_subject = "Factsheet Disapproved By The " . $disapproved_by;
// 					$dear_description = $ap_step_key;
					$dear_description = "Initiator";
					
					$option_comment = $factsheet_approval_process->initiator_optional_agent_comment;
					$legal_comment = $factsheet_approval_process->legal_agent_comment;
					$coo_comment = $factsheet_approval_process->coo_agent_comment;
					$cfo_comment = $factsheet_approval_process->cfo_agent_comment;
					$ceo_comment = $factsheet_approval_process->ceo_agent_comment;
					
					$following_line = "The following factsheet has been Disapproved by ".$disapproved_by." user. You are requested to verify the factsheet and share again.";
					$data ['email_data'] = array (
							'id' => $fact_sheet->id,
							'factsheet_code' => $fact_sheet->factsheet_code,
							'nm' => $fact_sheet->name_of_initiator,
							'ds' => $fact_sheet->designation,
							'nm_ch' => $fact_sheet->name_of_channel,
							'reg_mark' => $reg_mark,
							'mark_used_from' => $mark_used,
							'class_reg' => $class_reg,
							'jur_reg' => $fact_sheet->jurisdiction_for_registration,
							'sig_work' => $fact_sheet->significance_of_work,
							'option_comment' => blank ( $option_comment ) ? 'Pending' : $option_comment,
							'legal_comment' => blank ( $legal_comment ) ? 'Pending' : $legal_comment,
							'coo_comment' => blank ( $coo_comment ) ? 'Pending' : $coo_comment,
							'cfo_comment' => blank ( $cfo_comment ) ? 'Pending' : $cfo_comment,
							'ceo_comment' => blank ( $ceo_comment ) ? 'Pending' : $ceo_comment,
							'dear_details' => $dear_description,
							'following_line' => $following_line,
							'initiator_optional_approval_status' => $factsheet_approval_process->initiator_optional_approval_status,
							'legal_approval_status' => $factsheet_approval_process->legal_approval_status,
							'coo_approval_status' => $factsheet_approval_process->coo_approval_status,
							'cfo_approval_status' => $factsheet_approval_process->cfo_approval_status,
							'ceo_approval_status' => $factsheet_approval_process->ceo_approval_status,
							'initiator_approval_back' => $factsheet_approval_process->initiator_approval_back,
							'factsheet_status_steps' => $factsheet_approval_process->factsheet_status_steps
					);
					
					/*
					Mail::send ( 'factsheet.factsheet_email_template_disapproved', $data, function ($message) {
						$message->subject ( $this->disapproved_subject );
						$message->from ( $this->mail_from );
						$message->to ( $this->followed_user );
						$message->cc ( $this->mail_admin );
					} );
					*/
					
					$log_data[] = $dear_description;
					Log::info($log_data);
				}
			}
		}
		
		if (count($this->disapproved_followed_user) > 0) {
			Mail::send ( 'factsheet.factsheet_email_template_disapproved', $data, function ($message) {
				$message->subject ( $this->disapproved_subject );
				$message->from ( $this->mail_from );
				$message->to ( $this->followed_user_initiator );
				$message->cc ( $this->mail_admin );
			} );
		}else{
			Mail::send ( 'factsheet.factsheet_email_template_disapproved', $data, function ($message) {
				$message->subject ( $this->disapproved_subject );
				$message->from ( $this->mail_from );
				$message->to ( $this->followed_user_initiator );
			} );
		}
		return $data;
	}
}
