<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TradeClass;
use Session;
class TradeClassController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $litis = TradeClass::all();
        return view('trade.index',compact('litis'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('trade.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'name' => 'required|unique:trade_classes,name',
            'type' => 'required',
            'desc' => 'required'
        ));

        $post = new TradeClass;
        $post->name = $request->name;
        $post->type = $request->type;
        $post->desc = $request->desc;
        $post->save();
        Session::flash('success','Class Added');
        return redirect('trade_class');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $com = TradeClass::find($id);
        return view('trade.edit',compact('com'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'name' => 'required',
            'type' => 'required',
            'desc' => 'required'
        ));

        $post = TradeClass::find($id);
        $post->name = $request->name;
        $post->type = $request->type;
        $post->desc = $request->desc;
        $post->save();
        Session::flash('success','Class Updated');
        return redirect('trade_class');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
