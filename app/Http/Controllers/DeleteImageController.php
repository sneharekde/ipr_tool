<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trademarks;
use App\UpdateStatusTrad;
use App\Copyrightn;
use App\Designm;
use App\Invest;
use App\Enf;
use App\UpdataStatusDesign;
use App\UpdateCopyrightStatus;
use App\LitigationNotice;
use App\CounselFee;
use App\CounselPaidFee;
use App\AdvocateFee;
use App\AdvocatePaidFee;
use App\CopyAgentFee;
use App\DesAgentFee;
use App\Dregistration;
use App\Expensein;
use App\Expensen;
use App\Hearing;
use App\NoticeStatus;
use App\TradAgentFee;
use Symfony\Component\HttpFoundation\Session\Session;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\VarDumper\VarDumper;

class DeleteImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function tradeimg($image,$id)
    {
        $img = Trademarks::find($id);
        $images = $img->scan_copy_app;
        $_images = [];
        $_image = [];
        $_image[] = $image;
        foreach(explode(",",$images) as $imgs){
            $_images[] = $imgs;
        }
        $path = public_path('trademark/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $img->scan_copy_app = implode(",",$new_arr);
        $img->save();
        return redirect()->back();
    }

    public function tradestate($image,$id)
    {
        $img = Trademarks::find($id);
        $images = $img->statement_mark;
        $_images = [];
        $_image = [];
        $_image[] = $image;
        foreach(explode(",",$images) as $imgs){
            $_images[] = $imgs;
        }
        $path = public_path('trademark/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $img->statement_mark = implode(",",$new_arr);
        $img->save();
        return redirect()->back();
    }

    public function tradeslogo($image,$id)
    {
        $img = Trademarks::find($id);
        $images = $img->logo;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('trademark/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $img->logo = implode(",",$new_arr);
        $img->save();
        return redirect()->back();
    }
    public function updateImg($image,$id)
    {
        $img = UpdateStatusTrad::find($id);
        $images = $img->docs;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('trademark/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $img->docs = implode(",",$new_arr);
        $img->save();
        return redirect()->back();
    }
    public function updateImgh($image,$id)
    {
        $post = UpdateStatusTrad::find($id);
        $images = $post->docs;
        $_images = [];
        $_image = [];
        $_image[] = $image;
        foreach (explode(",",$images) as $imgs) {
            $_images[] = $imgs;
        }
        $path = public_path('trademark/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->docs = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function copyimg($image,$id)
    {
        $post = Copyrightn::find($id);
        $images = $post->image;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $new_arr = array_diff($_images,$_image);
        $post->image = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function copyimgd($image,$id)
    {
        $post = Copyrightn::find($id);
        $images = $post->user_aff;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $new_arr = array_diff($_images,$_image);
        $post->user_aff = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function copyimgda($image,$id)
    {
        $post = Copyrightn::find($id);
        $images = $post->copy_app;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $new_arr = array_diff($_images,$_image);
        $post->copy_app = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function designimgda($image,$id)
    {
        $post = Designm::find($id);
        $images = $post->image_t;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('copyright/design/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->image_t = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function poa($image,$id)
    {
        $post = Copyrightn::find($id);
        $images = $post->poa;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('copyright/docs/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->poa = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function copy_stamp($image,$id)
    {
        $post = CopyAgentFee::find($id);
        $images = $post->stamp;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('ccopyright/img/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->stamp = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function copy_invoice($image,$id)
    {
        $post = CopyAgentFee::find($id);
        $images = $post->invoice;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('copyright/img/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->invoice = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }

    public function des_stamp($image,$id)
    {
        $post = DesAgentFee::find($id);
        $images = $post->stamp;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('ccopyright/img/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->stamp = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function des_invoice($image,$id)
    {
        $post = DesAgentFee::find($id);
        $images = $post->invoice;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('copyright/img/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->invoice = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }

    public function trade_stamp($image,$id)
    {
        $post = TradAgentFee::find($id);
        $images = $post->stamp;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('ccopyright/img/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->stamp = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }

    public function trade_invoice($image,$id)
    {
        $post = TradAgentFee::find($id);
        $images = $post->invoice;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('copyright/img/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->invoice = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }

    public function msme($image,$id)
    {
        $post = Trademarks::find($id);
        $images = $post->msme_cert;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('trademark/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->msme_cert = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function trad_poa($image,$id)
    {
        $post = Trademarks::find($id);
        $images = $post->poa;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('trademark/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->poa = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }



    public function designimgdas($image,$id)
    {
        $post = Designm::find($id);
        $images = $post->image;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('copyright/design/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->image= implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function designimgdass($image,$id)
    {
        $post = Designm::find($id);
        $images = $post->copy_application;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('copyright/design/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->copy_application= implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function designimgdassd($image,$id)
    {
        $post = Designm::find($id);
        $images = $post->user_aff;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('copyright/design/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->user_aff= implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function investimg($image,$id)
    {
        $post = Invest::find($id);
        $images = $post->docs;
        $_images = [];
        $_image = [];
        $_image[] = $image;
        foreach (explode(",",$images) as $imgs) {
            $_images[] = $imgs;
        }
        $path = public_path('Scan_Copy_Of_Application/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->docs = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function designimgd($image,$id)
    {
        $post = UpdataStatusDesign::find($id);
        $images = $post->docs;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('copyright/design/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->docs= implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function designimgcor($image,$id)
    {
        $post = UpdataStatusDesign::find($id);
        $images = $post->docs;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('copyright/design/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->docs= implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function cuimg($image,$id)
    {
        $post = UpdateCopyrightStatus::find($id);
        $images = $post->docs;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('copyright/img/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->docs= implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function enfimg($image,$id)
    {
        $img = Enf::find($id);
        $images = $img->docs;
        $_images = [];
        $_image = [];
        $_image[] = $image;
        foreach(explode(",",$images) as $imgs){
            $_images[] = $imgs;
        }

        $path = public_path('document/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $img->docs = implode(",",$new_arr);
        $img->save();
        return redirect()->back();
    }

    public function noticeimg($image,$id)
    {
        $post = LitigationNotice::find($id);
        $images = $post->docs;
        $_images = [];
        $_image = [];
        $_image[] = $image;
        foreach (explode(",",$images) as $imgs) {
            $_images[] = $imgs;
        }
        $path = public_path('litigation/notices/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->docs = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }

    public function noticeimgst($image,$id)
    {
        $post = NoticeStatus::find($id);
        $images = $post->docs;
        $_images = [];
        $_image = [];
        $_image[] = $image;
        foreach (explode(",",$images) as $imgs) {
            $_images[] = $imgs;
        }
        $path = public_path('litigation/notices/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->docs = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }

    public function litihear($image,$id)
    {
        $post = Hearing::find($id);
        $images = $post->docs;
        $_images = [];
        $_image = [];
        $_image[] = $image;
        foreach (explode(",",$images) as $imgs) {
            $_images[] = $imgs;
        }
        $path = public_path('litigation/summary/hearing/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->docs = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }

    public function counsel($image,$id)
    {
        $post = CounselFee::find($id);
        $images = $post->docs;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('litigation/fess/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->docs= implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function paid_fees($image,$id)
    {
        $post = CounselPaidFee::find($id);
        $images = $post->docs;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('litigation/fess/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->docs= implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }

    public function advocate($image,$id)
    {
        $post = AdvocateFee::find($id);
        $images = $post->docs;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('litigation/fess/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->docs= implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
    public function adv_paid_fees($image,$id)
    {
        $post = AdvocatePaidFee::find($id);
        $images = $post->docs;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('litigation/fess/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->docs= implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }

    public function expins($image,$id)
    {
        $post = Expensein::find($id);
        $images = $post->upload;
        $_images = [];
        $_image = [];
        $_image[] = $image;
        foreach (explode(",",$images) as $imgs) {
            $_images[] = $imgs;
        }
        $new_arr = array_diff($_images,$_image);
        $post->upload = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }

    public function expens($image,$id)
    {
        $post = Expensen::find($id);
        $images = $post->upload;
        $_images = [];
        $_image = [];
        $_image[] = $image;
        foreach (explode(",",$images) as $imgs) {
            $_images[] = $imgs;
        }
        $new_arr = array_diff($_images,$_image);
        $post->upload = implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }

    public function regis_des($image,$id)
    {
        $post = Dregistration::where('design_id',$id)->first();
        $images = $post->image;
        $path = public_path("copyright/design/".$images);
        if(file_exists($path)){
            unlink($path);
        }
        $post->delete();
        return redirect()->back();
    }


    public function reg_cert($image,$id)
    {
        $post = Copyrightn::find($id);
        $images = $post->reg_cert;
        $_images = [];
        $_images[] = $images;
        $_image = [];
        $_image[] = $image;
        $path = public_path('copyright/img/'.$image);
        if(file_exists($path)){
            unlink($path);
        }
        $new_arr = array_diff($_images,$_image);
        $post->reg_cert= implode(",",$new_arr);
        $post->save();
        return redirect()->back();
    }
}
