<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DesignClass;
use Session;
use DB;

class DesignClassController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $litis = DesignClass::all();
        return view('desclass.index',compact('litis'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('desclass.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'name' => 'required|unique:design_classes,name',
            'desc' => 'required'
        ));

        $name = new DesignClass;
        $name->name = $request->name;
        $name->desc = $request->desc;
        $name->save();
        Session::flash('success','Class Added');
        return redirect('sections');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $liti = DesignClass::find($id);
        return view('desclass.edit',compact('liti'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'name' => 'required',
            'desc' => 'required'
        ));

        $name = DesignClass::find($id);
        $name->name = $request->name;
        $name->desc = $request->desc;
        $name->save();
        Session::flash('success','Class Updated');
        return redirect('sections');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
