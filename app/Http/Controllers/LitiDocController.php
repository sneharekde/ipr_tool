<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LitiDoc;
use App\LitigationSummary;
use App\User;
use App\LawFirm;
use App\ExternalCounsel;
use Storage;
use DB;
use Auth;
use Session;

class LitiDocController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($id){
        $liti =  LitiDoc::where('doc_id',$id)->orderBy('id','desc')->get();
        $summ = LitigationSummary::find($id);
        return view('doc.index',compact('liti','summ'));
    }
    public function create($id)
    {
        $id = $id;
        return view('doc.create',compact('id'));
    }

    public function store(Request $request)
    {
    	$this->validate($request,array(
    		'docs' => 'required'
    	));

    	$liti = new LitiDoc;
    	$liti->doc_id = $request->doc_id;
     	if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'litigation/summary/hearing';
            $images=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $images[] = $fullname;
                $has = implode(",",$images);
            }

            $liti->docs = $has;
        }
        $liti->name = $request->name;
        $liti->save();
        Session::flash('success','Document Added');
        return redirect('summary/documents/'.$request->doc_id);
    }

    public function delete($id)
    {
        $post = LitiDoc::find($id);
        $path = public_path('litigation/summary/hearing/'.$post->docs);
        if(file_exists($path) && $post->docs){
            unlink($path);
        }
        $post->delete();
        Session::flash('success','Data delete successfully!');
        return redirect('summary/documents/'.$post->doc_id);
    }

}
