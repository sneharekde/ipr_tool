<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Design;
use App\DesignClass;
use App\NatureApplication;
use App\ApplicationField;
use App\User;
use App\Applicant;
use App\NatureApplicant;
use App\Agent;
use App\CategoryMark;
use App\Language;
use App\ClassGoodServices;
use App\DesAgentFee;
use App\SubClassGoodsService;
use App\Designm;
use App\Dregistration;
use App\Entity;
use DB;
use App\EntityList;
use App\UpdataStatusDesign;
use Storage;
use File;
use Auth;
use PDF;
use Session;

class DesignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $showes = Designm::where('draft','submit')->orderBy('id', 'DESC')->get();
        $natureapplications = NatureApplication::all();
        $applicationfields = ApplicationField::all();
        $users = User::all();
        $applicants = Applicant::all();
        $natureapplicants = NatureApplicant::all();
        $agents = Agent::all();
        $categorymarks = CategoryMark::all();
        $languages = Language::all();
        $sections = DesignClass::all();
        $entitys = Entity::all();

        // $trademark_statuss = trademark_status::all();
        // group all the elements
        $app_no = Designm::selectRaw('app_no')->groupBy('app_no')->get();
        $noapp = Designm::selectRaw('natappli_id')->groupBy('natappli_id')->get();
        $appl_na = Designm::selectRaw('name_of_applicant')->groupBy('name_of_applicant')->get();
        $entity = Designm::selectRaw('ent_id')->groupBy('ent_id')->get();
        $natent = Designm::selectRaw('natent_id')->groupBy('natent_id')->get();
        $agent_name = Designm::selectRaw('agent_id')->groupBy('agent_id')->get();
        $status = Designm::selectRaw('status')->groupBy('status')->get();
        return view('design.index' , compact('showes','natureapplications', 'applicationfields', 'users', 'applicants', 'natureapplicants', 'agents', 'categorymarks', 'languages','sections', 'entitys','app_no','noapp','appl_na','entity','natent','agent_name','status'))->with('no', 1);;

    }

    public function Create()
    {
        $natureapplications = NatureApplication::all();
        $applicationfields = ApplicationField::all();
        $users = User::all();
        $applicants = Applicant::all();
        $natureapplicants = NatureApplicant::all();
        $agents = Agent::all();
        $languages = Language::all();
        $entitys = Entity::all();
        $sections = DesignClass::all();

        $ar1 = array();
        $ar2 = array();
        $ar3 = array();
        $ar4 = array();
        $ar5 = array();
        $ar6 = array();
        $ar7 = array();
        $ar8 = array();
        $ar9 = array();

        foreach ($languages as $lang) {
            $ar7[$lang->id] = $lang->language;
        }

        foreach ($sections as $section) {
            $ar6[$section->id] = $section->name;
        }

        foreach ($natureapplicants as $natent) {
            $ar5[$natent->id] = $natent->nature_of_applicant;
        }

        foreach ($entitys as $ent) {
            $ar4[$ent->id] = $ent->name;
        }

        foreach ($applicationfields as $appf) {
            $ar3[$appf->id] = $appf->application_field;
        }

        foreach ($agents as $agent) {
            $ar1[$agent->id] = $agent->name;
        }
        foreach ($users as $user) {
            $ar2[$user->id] = $user->first_name.' '.$user->last_name;
        }

        return view('design.create', compact('natureapplications', 'ar3', 'ar2', 'applicants', 'ar5', 'ar1', 'ar7', 'ar6','ar4'));
    }

    public function store(Request $request)
    {
        if ($request->has('draft')) {
            $this->validate($request,[

                'app_no' => 'required',
                'user_id' => 'required',
                'classg_id' => 'required'
            ]);
            $draft = new Designm;
            // Save Image_t file
            if ($request->hasFile('image_t'))
            {
                $file= $request->file('image_t');
                $destinationPath= 'copyright/design';
                $image_t = $file->getClientOriginalName();
                $hashname = $image_t;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->image_t = $image_t;
            }
            // Save logo
            if ($request->hasFile('image'))
            {
                $file= $request->file('image');
                $destinationPath= 'copyright/design';
                $file_logo = $file->getClientOriginalName();
                $hashname = $file_logo;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->image = $file_logo;
            }
            // Save User Affidavit
            if ($request->hasFile('user_aff'))
            {
                $file= $request->file('user_aff');
                $destinationPath= 'copyright/design';
                $file_scan_copy = $file->getClientOriginalName();
                $hashname = $file_scan_copy;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->user_aff = $file_scan_copy;
            }
            // Scan Copy of Application
            if ($request->hasFile('copy_application'))
            {
                $file= $request->file('copy_application');
                $destinationPath= 'copyright/design';
                $statement_mark = $file->getClientOriginalName();
                $hashname = $statement_mark;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->copy_application = $statement_mark;
            }

            // Scan Copy of Application
            if ($request->hasFile('poa'))
            {
                $file= $request->file('poa');
                $destinationPath= 'copyright/design';
                $statement_mark = $file->getClientOriginalName();
                $hashname = $statement_mark;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->poa = $statement_mark;
            }
            $draft->prop_code = $request->prop_code;
            $draft->govt_fee = $request->govt_fee;
            $draft->app_no = $request->app_no;
            $draft->filing_date = date('Y-m-d',strtotime($request->filing_date));
            $draft->app_type = $request->app_type;
            $draft->user_id = $request->user_id;
            $draft->name_of_applicant = $request->name_of_applicant;
            $draft->natappli_id = $request->natappli_id;
            $draft->ent_id = $request->ent_id;
            $draft->natent_id = $request->natent_id;
            $draft->article_name = $request->article_name;
            $draft->description = $request->description;
            $draft->classg_id = $request->classg_id;
            $draft->lang_id = $request->lang_id;
            $draft->remarks = $request->remarks;
            $draft->agent_id = $request->agent_id;
            $draft->agent_code = $request->agent_code;
            $draft->details_use = $request->details_use;
            if ($request->details_use == 'Used since') {
                $draft->datea = date('Y-m-d',strtotime($request->datea));
            }else{
                $draft->datea = null;
            }
            $draft->name = $request->name;
            $draft->designation = $request->designation;
            $draft->draft = 'draft';
            $draft->save();
            Session::flash('success','Design Application Submitted!');
            return redirect()->route('design-draft.index');
        } elseif($request->has('submit')) {
            $this->validate($request,[

                'app_no' => 'required',
                'user_id' => 'required',
                'classg_id' => 'required'
            ]);
            $design = new Designm;
            // Save Image_t file
            if ($request->hasFile('image_t'))
            {
                $file= $request->file('image_t');
                $destinationPath= 'copyright/design';
                $image_t = $file->getClientOriginalName();
                $hashname = $image_t;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $design->image_t = $image_t;
            }
            // Save logo
            if ($request->hasFile('image'))
            {
                $file= $request->file('image');
                $destinationPath= 'copyright/design';
                $file_logo = $file->getClientOriginalName();
                $hashname = $file_logo;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $design->image = $file_logo;
            }
            // Save User Affidavit
            if ($request->hasFile('user_aff'))
            {
                $file= $request->file('user_aff');
                $destinationPath= 'copyright/design';
                $file_scan_copy = $file->getClientOriginalName();
                $hashname = $file_scan_copy;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $design->user_aff = $file_scan_copy;
            }
            // Scan Copy of Application
            if ($request->hasFile('copy_application'))
            {
                $file= $request->file('copy_application');
                $destinationPath= 'copyright/design';
                $statement_mark = $file->getClientOriginalName();
                $hashname = $statement_mark;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $design->copy_application = $statement_mark;
            }

            // Scan Copy of Application
            if ($request->hasFile('poa'))
            {
                $file= $request->file('poa');
                $destinationPath= 'copyright/design';
                $statement_mark = $file->getClientOriginalName();
                $hashname = $statement_mark;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $design->poa = $statement_mark;
            }
            $design->prop_code = $request->prop_code;
            $design->govt_fee = $request->govt_fee;
            $design->app_no = $request->app_no;
            $design->filing_date = date('Y-m-d',strtotime($request->filing_date));
            $design->app_type = $request->app_type;
            $design->user_id = $request->user_id;
            $design->name_of_applicant = $request->name_of_applicant;
            $design->natappli_id = $request->natappli_id;
            $design->ent_id = $request->ent_id;
            $design->natent_id = $request->natent_id;
            $design->article_name = $request->article_name;
            $design->description = $request->description;
            $design->classg_id = $request->classg_id;
            $design->lang_id = $request->lang_id;
            $design->remarks = $request->remarks;
            $design->agent_id = $request->agent_id;
            $design->agent_code = $request->agent_code;
            $design->details_use = $request->details_use;
            if ($request->details_use == 'Used since') {
                $design->datea = date('Y-m-d',strtotime($request->datea));
            }else{
                $design->datea = null;
            }
            $design->name = $request->name;
            $design->designation = $request->designation;
            $design->status = 'Active Application';
            $design->sub_status = 'New Application';
            $design->draft = 'submit';
            $design->save();
            $update_s = new UpdataStatusDesign();
            $update_s->design_id = $design->id;
            $update_s->date = $design->filing_date;
            $update_s->status = $design->status;
            $update_s->sub_status = $design->sub_status;
            $update_s->save();
            Session::flash('success','Design Application Submitted!');
            return redirect('/design/show');
        }

    }

    public function view($id)
    {
        $show = Designm::find($id);
        $regis = Dregistration::Where('design_id','=',$id)->first();
        $posts = DesAgentFee::where('des_id',$id)->orderBy('id','DESC')->get();
        return view('design.show',compact('show','regis','posts'))->with('no',1);
    }

    public function edit($id)
    {
        $design = Designm::find($id);
        $natureapplications = NatureApplication::all();
        $applicationfields = ApplicationField::all();
        $users = User::all();
        $applicants = Applicant::all();
        $natureapplicants = NatureApplicant::all();
        $agents = Agent::all();
        $languages = Language::all();
        $entitys = Entity::all();
        $sections = DesignClass::all();

        $ar1 = array();
        $ar2 = array();
        $ar3 = array();
        $ar4 = array();
        $ar5 = array();
        $ar6 = array();
        $ar7 = array();
        $ar8 = array();
        $ar9 = array();

        foreach ($languages as $lang) {
            $ar7[$lang->id] = $lang->language;
        }

        foreach ($sections as $section) {
            $ar6[$section->id] = $section->name;
        }

        foreach ($natureapplicants as $natent) {
            $ar5[$natent->id] = $natent->nature_of_applicant;
        }

        foreach ($entitys as $ent) {
            $ar4[$ent->id] = $ent->name;
        }

        foreach ($applicationfields as $appf) {
            $ar3[$appf->id] = $appf->application_field;
        }

        foreach ($agents as $agent) {
            $ar1[$agent->id] = $agent->name;
        }
        foreach ($users as $user) {
            $ar2[$user->id] = $user->first_name.' '.$user->last_name;
        }
    	return view('design.edit', compact('design','natureapplications', 'ar3', 'ar2', 'applicants', 'natureapplicants', 'ar1', 'ar7', 'ar6','ar4'));
    }

    public function update(Request $request,$id)
    {

        if ($request->has('draft')) {
            $this->validate($request,[

                'app_no' => 'required',
                'user_id' => 'required',
                'classg_id' => 'required'
            ]);
            $draft = Designm::find($id);
            // Save Image_t file
            if ($request->hasFile('image_t'))
            {
                $file= $request->file('image_t');
                $destinationPath= 'copyright/design';
                $image_t = $file->getClientOriginalName();
                $hashname = $image_t;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->image_t = $image_t;
            }
            // Save logo
            if ($request->hasFile('image'))
            {
                $file= $request->file('image');
                $destinationPath= 'copyright/design';
                $file_logo = $file->getClientOriginalName();
                $hashname = $file_logo;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->image = $file_logo;
            }
            // Save User Affidavit
            if ($request->hasFile('user_aff'))
            {
                $file= $request->file('user_aff');
                $destinationPath= 'copyright/design';
                $file_scan_copy = $file->getClientOriginalName();
                $hashname = $file_scan_copy;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->user_aff = $file_scan_copy;
            }
            // Scan Copy of Application
            if ($request->hasFile('copy_application'))
            {
                $file= $request->file('copy_application');
                $destinationPath= 'copyright/design';
                $statement_mark = $file->getClientOriginalName();
                $hashname = $statement_mark;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->copy_application = $statement_mark;
            }

            // Scan Copy of Application
            if ($request->hasFile('poa'))
            {
                $file= $request->file('poa');
                $destinationPath= 'copyright/design';
                $statement_mark = $file->getClientOriginalName();
                $hashname = $statement_mark;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $draft->poa = $statement_mark;
            }
            $draft->prop_code = $request->prop_code;
            $draft->govt_fee = $request->govt_fee;
            $draft->app_no = $request->app_no;
            $draft->filing_date = date('Y-m-d',strtotime($request->filing_date));
            $draft->app_type = $request->app_type;
            $draft->user_id = $request->user_id;
            $draft->name_of_applicant = $request->name_of_applicant;
            $draft->natappli_id = $request->natappli_id;
            $draft->ent_id = $request->ent_id;
            $draft->natent_id = $request->natent_id;
            $draft->article_name = $request->article_name;
            $draft->description = $request->description;
            $draft->classg_id = $request->classg_id;
            $draft->lang_id = $request->lang_id;
            $draft->remarks = $request->remarks;
            $draft->agent_id = $request->agent_id;
            $draft->agent_code = $request->agent_code;
            $draft->details_use = $request->details_use;
            if ($request->details_use == 'Used since') {
                $draft->datea = date('Y-m-d',strtotime($request->datea));
            }else{
                $draft->datea = null;
            }
            $draft->name = $request->name;
            $draft->designation = $request->designation;
            $draft->draft = 'draft';
            $draft->save();
            Session::flash('success','Design Application Submitted!');
            return redirect()->route('design-draft.index');
        } elseif($request->has('submit')) {
            $this->validate($request,[

                'app_no' => 'required',
                'user_id' => 'required',
                'classg_id' => 'required'
            ]);
            $design = Designm::find($id);
            // Save Image_t file
            if ($request->hasFile('image_t'))
            {
                $file= $request->file('image_t');
                $destinationPath= 'copyright/design';
                $image_t = $file->getClientOriginalName();
                $hashname = $image_t;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $design->image_t = $image_t;
            }
            // Save logo
            if ($request->hasFile('image'))
            {
                $file= $request->file('image');
                $destinationPath= 'copyright/design';
                $file_logo = $file->getClientOriginalName();
                $hashname = $file_logo;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $design->image = $file_logo;
            }
            // Save User Affidavit
            if ($request->hasFile('user_aff'))
            {
                $file= $request->file('user_aff');
                $destinationPath= 'copyright/design';
                $file_scan_copy = $file->getClientOriginalName();
                $hashname = $file_scan_copy;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $design->user_aff = $file_scan_copy;
            }
            // Scan Copy of Application
            if ($request->hasFile('copy_application'))
            {
                $file= $request->file('copy_application');
                $destinationPath= 'copyright/design';
                $statement_mark = $file->getClientOriginalName();
                $hashname = $statement_mark;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $design->copy_application = $statement_mark;
            }

            // Scan Copy of Application
            if ($request->hasFile('poa'))
            {
                $file= $request->file('poa');
                $destinationPath= 'copyright/design';
                $statement_mark = $file->getClientOriginalName();
                $hashname = $statement_mark;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $design->poa = $statement_mark;
            }
            $design->prop_code = $request->prop_code;
            $design->govt_fee = $request->govt_fee;
            $design->app_no = $request->app_no;
            $design->filing_date = date('Y-m-d',strtotime($request->filing_date));
            $design->app_type = $request->app_type;
            $design->user_id = $request->user_id;
            $design->name_of_applicant = $request->name_of_applicant;
            $design->natappli_id = $request->natappli_id;
            $design->ent_id = $request->ent_id;
            $design->natent_id = $request->natent_id;
            $design->article_name = $request->article_name;
            $design->description = $request->description;
            $design->classg_id = $request->classg_id;
            $design->lang_id = $request->lang_id;
            $design->remarks = $request->remarks;
            $design->agent_id = $request->agent_id;
            $design->agent_code = $request->agent_code;
            $design->details_use = $request->details_use;
            if ($request->details_use == 'Used since') {
                $design->datea = date('Y-m-d',strtotime($request->datea));
            }else{
                $design->datea = null;
            }
            $design->name = $request->name;
            $design->designation = $request->designation;
            $design->status = 'Active Application';
            $design->sub_status = 'New Application';
            $design->draft = 'submit';
            $design->save();
            // check Status is already exist
            $check = UpdataStatusDesign::where('design_id',$design->id)->first();
            if ($check == '') {
                $update_s = new UpdataStatusDesign();
                $update_s->design_id = $design->id;
                $update_s->date = $design->filing_date;
                $update_s->status = $design->status;
                $update_s->sub_status = $design->sub_status;
                $update_s->save();
            }
            Session::flash('success','Design Application Submitted!');
            return redirect('/design/show');
        }
    }

    public function search(Request $request)
    {
        $sapp_no = $request->sapp_no;
        $nat_applnt = $request->natappli_id;
        $nameofap = $request->nameofap;
        $ent_name = $request->ent_id;
        $natentit = $request->natent_id;
        $agentn = $request->agent_id;
        $design_status = $request->design_status;
        $from = $request->from;
        $to = $request->to;

        $shows = Designm::orderBy('id','DESC')->get();
        $natureapplications = NatureApplication::all();
        $applicationfields = ApplicationField::all();
        $users = User::all();
        $applicants = Applicant::all();
        $natureapplicants = NatureApplicant::all();
        $agents = Agent::all();
        $languages = Language::all();
        $subclassgood = SubClassGoodsService::all();
        $entitys = Entity::all();

        // Group by all data for search bar
        $app_no = Designm::selectRaw('app_no')->groupBy('app_no')->get();
        $noapp = Designm::selectRaw('natappli_id')->groupBy('natappli_id')->get();
        $appl_na = Designm::selectRaw('name_of_applicant')->groupBy('name_of_applicant')->get();
        $entity = Designm::selectRaw('ent_id')->groupBy('ent_id')->get();
        $natent = Designm::selectRaw('natent_id')->groupBy('natent_id')->get();
        $agent_name = Designm::selectRaw('agent_id')->groupBy('agent_id')->get();
        $status = Designm::selectRaw('status')->groupBy('status')->get();

        $results = Designm::where(function ($query) use ($sapp_no,$nat_applnt,$nameofap,$ent_name,$natentit,$agentn,$design_status,$from,$to){
                if ($sapp_no) {
                    $query->where('app_no' ,'=', $sapp_no);
                }
                if ($nat_applnt) {
                    $query->where('natappli_id', '=',$nat_applnt);
                }
                if ($nameofap) {
                    $query->where('name_of_applicant', '=',$nameofap);
                }
                if ($ent_name) {
                    $query->where('ent_id', '=', $ent_name);
                }
                if ($natentit) {
                    $query->where('natent_id','=',$natentit);
                }
                if ($agentn) {
                    $query->where('agent_id','=',$agentn);
                }
                if ($design_status) {
                    $query->where('status','=',$design_status);
                }
                if ($from) {
                    $query->whereBetween('filing_date',[$from,$to]);
                }
            })->get();

        return view('design.search', compact('results','shows','natureapplications','applicationfields','users','applicants','natureapplicants','agents','languages','subclassgood','entitys','app_no','noapp','appl_na','entity','natent','agent_name','status','sapp_no','nat_applnt','nameofap','ent_name','natentit','agentn','design_status','from','to'))->with('no',1);
    }

    public function edit_status($id)
    {
        $design = UpdataStatusDesign::find($id);
        return view('design.edit_update',compact('design'));
    }
    public function edit_status_save(Request $request)
    {
        $this->validate($request,array(
            'status' => 'required'
        ));

        $update_design = UpdataStatusDesign::find($request->des_id);
        $update_design->status = $request->status;
        // Query for Status
        if ($request->status == 'Registered and Awaiting Publication') {
            $update_design->dor = date('Y-m-d',strtotime($request->dor));
            if ($request->hasFile('cor'))
            {
                $file= $request->file('cor');
                $destinationPath= 'copyright/design';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update_design->docs = $fullname;
            }
            $update_design->comment = $request->comment1;
            $update_design->d_renr = date('Y-m-d',strtotime($request->dor. ' +3650 days'));
            $update_design->rem1 = date('Y-m-d',strtotime($request->dor. ' +3470 days'));
            $update_design->rem2 = date('Y-m-d',strtotime($request->dor. ' +3560 days'));
            $update_design->rem3 = date('Y-m-d',strtotime($request->dor. ' +3620 days'));
            $update_design->rem4 = date('Y-m-d',strtotime($request->dor. ' +3635 days'));
            $update_design->sent_status = '1';
        }

        if ($request->status == 'Registered and Published') {
            $update_design->pub_date = date('Y-m-d',strtotime($request->date1));
            $update_design->jrno = $request->jno;
        }

        if ($request->status == 'Renewed') {
            $update_design->ren_date = date('Y-m-d',strtotime($request->dorenew));
            $update_design->exp_date = date('Y-m-d',strtotime($request->doe));
        }

        // Query for Sub Status
        if ($request->status == 'Active Application' && $request->sub_status == 'Examination Report Received')
        {
            $update_design->sub_status = $request->sub_status;
            $update_design->derrer = date('Y-m-d',strtotime($request->date2));
            $update_design->comment = $request->comment2;
            if ($request->hasFile('docs1'))
            {
                $file= $request->file('docs1');
                $destinationPath= 'copyright/design';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update_design->docs = $fullname;
            }
            $update_design->rep_dead = date('Y-m-d',strtotime($request->derrer. ' +90 days'));
            $update_design->rem1 = date('Y-m-d',strtotime($request->rem11));
            $update_design->rem2 = date('Y-m-d',strtotime($request->rem21));
            $update_design->rem3 = date('Y-m-d',strtotime($request->rem31));
            $update_design->sent_status = '1';
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Reply to objection submitted')
        {
            $update_design->sub_status = $request->sub_status;
            $update_design->date = date('Y-m-d',strtotime($request->date3));
            $update_design->comment = $request->comment3;
            if ($request->hasFile('docs2'))
            {
                $file= $request->file('docs2');
                $destinationPath= 'copyright/design';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update_design->docs = $fullname;
            }
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Hearing for Objection')
        {
            $update_design->sub_status = $request->sub_status;
            $update_design->dfh = date('Y-m-d',strtotime($request->her_date));
            $update_design->comment = $request->comment4;
            if ($request->hasFile('docs3'))
            {
                $file= $request->file('docs3');
                $destinationPath= 'copyright/design';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update_design->docs = $fullname;
            }
            $update_design->rem1 = date('Y-m-d',strtotime($request->rem12));
            $update_design->rem2 = date('Y-m-d',strtotime($request->rem22));
            $update_design->rem3 = date('Y-m-d',strtotime($request->rem32));
            $update_design->sent_status = '1';

            // find Date of filing
            $dead = Designm::find($request->des_id);
            $filing_date = $dead->filing_date;
            $update_design->dfrem = date('Y-m-d',strtotime($filing_date. ' +180 days'));
        }

        if ($request->status == 'Active Application' && $request->sub_status == 'Removal of Objection Period Extended')
        {
            $update_design->sub_status = $request->sub_status;
            $update_design->comment = $request->comment5;
            if ($request->hasFile('docs4'))
            {
                $file= $request->file('docs4');
                $destinationPath= 'copyright/design';
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $update_design->docs = $fullname;
            }
            $update_design->rem1 = date('Y-m-d',strtotime($request->rem13));
            $update_design->rem2 = date('Y-m-d',strtotime($request->rem23));
            $update_design->rem3 = date('Y-m-d',strtotime($request->rem33));
            $update_design->sent_status = '1';
            // find Date of filing
            $dead = Designm::find($request->des_id);
            $filing_date = $dead->filing_date;
            $update_design->dfrem = date('Y-m-d',strtotime($filing_date. ' +270 days'));
        }
        $update_design->save();
        Session::flash('success','Status Edited Successfully!');
        return redirect('design/'.$update_design->design_id.'/update_status');
    }

    public function represantion($id)
    {
        $post = Designm::find($id);
        return view('design.represnt',compact('post'));
    }

    public function represantion_save(Request $request,$id)
    {
        $this->validate($request,array(
            'image' => 'required'
        ));

        $post = Designm::find($id);
        $file= $request->file('image');
        $destinationPath= 'copyright/design';
        $file_logo = $file->getClientOriginalName();
        $hashname = $file_logo;
        $upload_success = $file->move(public_path($destinationPath), $hashname);
        $post->image = $file_logo;

        $post->save();
        Session::flash('success','Representation is added!');
        return redirect('design/'.$post->id.'/view');
    }

    public function registration($id)
    {
        $post = Designm::find($id);
        return view('design.regis',compact('post'));
    }

    public function regis_save(Request $request,$id)
    {
        $this->validate($request,array(
            'image' => 'required'
        ));

        $post = Designm::find($id);
        $regist = new Dregistration();
        $file= $request->file('image');
        $destinationPath= 'copyright/design';
        $file_logo = $file->getClientOriginalName();
        $hashname = $file_logo;
        $upload_success = $file->move(public_path($destinationPath), $hashname);
        $regist->image = $file_logo;
        $regist->design_id = $post->id;
        $regist->save();
        Session::flash('success','Representation is added!');
        return redirect('design/'.$post->id.'/view');
    }
}
