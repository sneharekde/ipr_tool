<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddTradmark;
use App\Copyright;
use App\Design;
use App\Patent;
use Session;
use DB;
use Auth;
use App\Trademarks;
use App\Designm;
use App\Copyrightn;


class LicenceInController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('licin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tm = Trademarks::where('application_no','=',$request->tm_no)->first();
        return view('licin.create',compact('tm'));
    }

    public function cr_lice(Request $request)
    {
        $cr = Copyrightn::where('diary','=',$request->dairy_no)->first();
        return view('licin.create',compact('cr'));
    }

    public function des_lice(Request $request)
    {
        $des = Designm::where('app_no','=',$request->desi_no)->first();
        return view('licin.create',compact('des'));
    }
    public function pat_lice(Request $request)
    {
        $pat= Patent::where('application_no','=',$request->pat_no)->first();
        return view('licin.create',compact('pat'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
