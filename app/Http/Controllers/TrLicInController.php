<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddTradmark;
use App\TradLicIn;
use Session;
use DB;
use Auth;
use App\Trademarks;
class TrLicInController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function tr_index($id)
    {
    	$comp = Trademarks::where('id', '=',$id)->first();
    	$licin = TradLicIn::where('trad_id', '=',$id)->get();
    	return view('licin.tr_index',compact('comp','licin'))->with('no',1);
    }

    public function tr_save(Request $request)
    {
    	$this->validate($request,array(
    		'licensor' => 'required',
    		'licensee' => 'required',
    		'term' => 'required|numeric',
    		'lic_date' => 'required',
    		'consideration' => 'required',
    	));
    	$tems = $request->term * 12;
    	$trad = new TradLicIn;
    	$trad->trad_id = $request->trad_id;
    	$trad->licensor = $request->licensor;
    	$trad->licensee = $request->licensee;
    	$trad->term = $request->term;
    	$trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
    	$trad->exp_date = date('Y-m-d',strtotime($tems.' months',strtotime($request->lic_date)));
        $trad->consideration = $request->consideration;
        if($request->consideration == 'Periodic royalty fee'){
            $trad->rem_ip = $request->rem_ip;
    	    $trad->ent_date = $request->ent_date;
        }
        if($request->consideration == 'One time royalty fee'){
            $trad->ent_date2 = $request->ent_date2;
        }

    	if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
    	$trad->reminder = $request->reminder;
    	$trad->comments = $request->comments;
    	$trad->save();
    	Session::flash('success','License In Successfully Added');
    	return redirect('trlicense/'.$request->trad_id);
    }
    public function tr_edit($id)
    {
    	$licin = TradLicIn::find($id);
    	return view('licin.tr_edit',compact('licin'))->with('no',1);
    }

    public function tr_edit_save(Request $request,$id)
    {
    	$this->validate($request,array(
    		'licensor' => 'required',
    		'licensee' => 'required',
    		'term' => 'required|numeric',
    		'lic_date' => 'required',
    		'consideration' => 'required',
    	));
    	$tems = $request->term * 12;
    	$trad = TradLicIn::find($id);
    	$trad->trad_id = $request->trad_id;
    	$trad->licensor = $request->licensor;
    	$trad->licensee = $request->licensee;
    	$trad->term = $request->term;
    	$trad->lic_date = date('Y-m-d',strtotime($request->lic_date));
    	$trad->exp_date = date('Y-m-d',strtotime($tems.' months',strtotime($request->lic_date)));
        $trad->consideration = $request->consideration;
        if($request->consideration == 'Periodic royalty fee'){
            $trad->rem_ip = $request->rem_ip;
    	    $trad->ent_date = $request->ent_date;
        }
        if($request->consideration == 'One time royalty fee'){
            $trad->ent_date2 = $request->ent_date2;
        }

    	if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'trademark';
            $docs=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $docs[] = $fullname;
                $has = implode(",",$docs);
                $trad->docs = $has;
            }
        }
    	$trad->reminder = $request->reminder;
    	$trad->comments = $request->comments;
    	$trad->save();
    	Session::flash('success','License In Successfully Added');
    	return redirect('trlicense/'.$request->trad_id);
    }
}
