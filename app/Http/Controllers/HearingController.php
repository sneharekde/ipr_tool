<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hearing;
use App\LitigationSummary;
use App\User;
use App\LawFirm;
use App\ExternalCounsel;
use Storage;
use DB;
use Auth;
use Session;

class HearingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function hearing($id){
        $hears = Hearing::Where('hear_id',$id)->get();
        $summ = LitigationSummary::find($id);
        return view('hearing.index',compact('hears','summ'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($sum)
    {
        $id = $sum;
        $users = User::all();
        $exts = ExternalCounsel::all();
        $laws = LawFirm::all();

        $ar1 = array();
        $ar2 = array();
        $ar3 = array();

        foreach ($users as $user) {
            $ar1[$user->id] = $user->first_name;
        }
        foreach ($exts as $ext) {
            $ar2[$ext->id] = $ext->counsel;
        }
        foreach ($laws as $law) {
            $ar3[$law->id] = $law->name;
        }
        return view('hearing.create',compact('ar1','ar2','ar3','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'date' => 'required',
            'her_date' => 'required',
            'user_id' => 'required',
            'ext_counsel_id' => 'required',
        ));

        $hear = new Hearing;
        $hear->hear_id = $request->hear_id;
        $hear->date = date('Y-m-d',strtotime($request->date));
        $hear->her_date = date('Y-m-d',strtotime($request->her_date));
        $hear->alert1 = date('Y-m-d',strtotime($request->alert1));
        $hear->alert2 = date('Y-m-d',strtotime($request->alert2));
        $hear->alert3 = date('Y-m-d',strtotime($request->alert3));
        $hear->email = $request->email;
        $hear->stage_desc = $request->stage_desc;
        $hear->act_item = $request->act_item;
        $hear->user_id = $request->user_id;
        $hear->ext_counsel_id = $request->ext_counsel_id;
        $hear->law_firm_id = $request->law_firm_id;
        $hear->due_date = date('Y-m-d',strtotime($request->due_date));
        $hear->sent_status = '1';
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'litigation/summary/hearing';
            $images=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $images[] = $fullname;
                $has = implode(",",$images);
            }

            $hear->docs = $has;
        }
        $updated = LitigationSummary::find($request->hear_id);
        $updated->next_date = date('Y-m-d',strtotime($request->her_date));

        $hear->save();
        $updated->save();
        Session::flash('success','Hearing added');
        return redirect('/summary/hearing/'.$request->hear_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hear = Hearing::find($id);
        $users = User::all();
        $exts = ExternalCounsel::all();
        $laws = LawFirm::all();

        $ar1 = array();
        $ar2 = array();
        $ar3 = array();

        foreach ($users as $user) {
            $ar1[$user->id] = $user->first_name;
        }
        foreach ($exts as $ext) {
            $ar2[$ext->id] = $ext->counsel;
        }
        foreach ($laws as $law) {
            $ar3[$law->id] = $law->name;
        }
        return view('hearing.edit',compact('hear','ar1','ar2','ar3'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'date' => 'required',
            'her_date' => 'required',
            'user_id' => 'required',
            'ext_counsel_id' => 'required',
        ));

        $hear = Hearing::find($id);
        $hear->date = date('Y-m-d',strtotime($request->date));
        $hear->her_date = date('Y-m-d',strtotime($request->her_date));
        $hear->alert1 = date('Y-m-d',strtotime($request->alert1));
        $hear->alert2 = date('Y-m-d',strtotime($request->alert2));
        $hear->alert3 = date('Y-m-d',strtotime($request->alert3));
        $hear->email = $request->email;
        $hear->stage_desc = $request->stage_desc;
        $hear->act_item = $request->act_item;
        $hear->user_id = $request->user_id;
        $hear->ext_counsel_id = $request->ext_counsel_id;
        $hear->law_firm_id = $request->law_firm_id;
        $hear->due_date = date('Y-m-d',strtotime($request->due_date));
        if ($request->hasFile('docs'))
        {
            $files= $request->file('docs');
            $destinationPath= 'litigation/summary/hearing';
            $images=array();
            foreach($files as $file)
            {
                $fullname = $file->getClientOriginalName();
                $hashname = $fullname;
                $upload_success = $file->move(public_path($destinationPath), $hashname);
                $images[] = $fullname;
                $has = implode(",",$images);
            }

            $hear->docs = $has;
        }
        $updated = LitigationSummary::find($hear->hear_id);
        $updated->next_date = date('Y-m-d',strtotime($request->her_date));
        $hear->save();
        $updated->save();
        Session::flash('success','Hearing Updated');
        return redirect('/summary/hearing/'.$hear->hear_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $post = Hearing::find($id);
        $path = public_path('litigation/summary/hearing/'.$post->docs);
        if(file_exists($path) && $post->docs !=''){
            unlink($path);
        }
        $post->delete();
        Session::flash('success','Data delete successfully!');
        return redirect('summary/hearing/'.$post->hear_id);
    }

}
