<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MasterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function trade()
    {
        return view('master.trade');
    }
    public function common()
    {
        return view('master.common');
    }
    public function patent()
    {
        return view('master.patent');
    }
    public function brand()
    {
        return view('master.brand');
    }
    public function copy()
    {
        return view('master.copy');
    }
}
