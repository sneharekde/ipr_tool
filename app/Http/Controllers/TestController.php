<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Testing;
use Storage;
use Session;
use DB;
use Auth;
class TestController extends Controller
{
   	public function index()
   	{
   		$tests = Testing::all();
   		return view('test.index',compact('tests'));

   	}

   	public function store(Request $request)
   	{
   		$this->validate($request,array(
   			'image' => 'required',
   			'pdf' => 'required',
            'pdf.*' => 'mimes:doc,pdf,docx,zip',
   			'file' => 'required'
   		));

   		// Single Image Flle
   		if ($request->hasfile('image')) {
   			$image = $request->image->getClientOriginalName();
   			$request->image->move(public_path('test'),$image);
   		}
   		if ($request->hasfile('pdf')) {
   			$pdf = $request->pdf->getClientOriginalName();
   			$request->pdf->move(public_path('test'),$pdf);
   		}

   		
   		if ($request->hasfile('file')) 
   		{
   			foreach ($request->file('file') as $file) 
   			{
   				$name = $file->getClientOriginalName();
   				$file->move(public_path('test'),$name);
   				$date[] = $name;
   			}
   		}

   		$test = new Testing;
   		$test->image = $image;
   		$test->pdf = $pdf;
   		$test->file = json_encode($data);

   		$test->save();
   		Session::flash('success','Files are Successfully Done');
   		return redirect('/testing');
   	}
}
