<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EntityList;
use App\ParentEntity;
use App\Unit;
use App\FunctionList;
use App\DesignationList;
use App\Entity;
use App\User;
use DB;
use Auth;
use App\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ShowUser(){
    	$users = User::orderBy('id', 'desc')->get();
    	return view('users.show', compact('users'))->with('no',1);
    }

    public function CreateUser(){
    	$parents_ides = ParentEntity::all();
        $entity = Entity::all();
        $units = Unit::all();
        $functions = FunctionList::all();
        $designations = DesignationList::all();
        $roles = Role::all();
    	return view('users.create', compact('parents_ides', 'entity', 'units', 'functions', 'designations', 'roles'));
    }

    public function UserSave(Request $request){
    	$this->validate($request, [
    		'first_name' => 'required',
    		'last_name' => 'required',
    		'mobile_no' => 'required|min:10|unique:users,mobile_no',
    		'email' => 'required|email|unique:users,email',
    		'user_name' => 'required|unique:users,user_name',
    		'password' => 'required',
    		'password_confirmed' =>'required|same:password',
    		'enttity' => 'required',
    		'unit' => 'required',
    		'function' => 'required',
    		'employee_id' => 'required|unique:users,employee_id',
    		'designation' => 'required',
    		'role' => 'required',
    		'address' => 'required'

    	], [

    		'user_password.required' => 'Password Confirmation should match the Password',
    	]);

    	$created = User::create($request->all());

        if($created){
            return redirect('user/show')->with('success','User created successfully');
        }
    }

    public function EditUser($id){
    	$user = User::find($id);
        $parents_ides = ParentEntity::all();

        $entity = Entity::all();
        $units = Unit::all();
        $functions = FunctionList::all();
        $designations = DesignationList::all();
        $roles = Role::all();
    	return view('users.edit', compact('user','parents_ides', 'entity', 'units', 'functions', 'designations', 'roles'));
    }

    public function UpdateUser(Request $request){
    	$updated = $user = User::find($request->id);
    			   $user->first_name = $request->first_name;
    			   $user->last_name = $request->last_name;
    			   $user->mobile_no = $request->mobile_no;
    			   $user->email = $request->email;
    			   $user->user_name = $request->user_name;
    			   $user->enttity = $request->enttity;
    			   $user->unit = $request->unit;
    			   $user->function = $request->function;
    			   $user->employee_id = $request->employee_id;
    			   $user->designation = $request->designation;
    			   $user->role = $request->role;
    			   $user->address = $request->address;
    			   $user->save();

    	if($updated){
            return redirect('user/show')->with('success', 'User Successfully Updated!');
        }
    }

    public function update_status(Request $request){
            $update_status = $user = User::find($request->id);
                                         $user->status = $request->status;
                                         $user->save();

            if($update_status){
            return redirect('user/show')->with('success', 'User Successfully Disable!');
        }
    }
}
