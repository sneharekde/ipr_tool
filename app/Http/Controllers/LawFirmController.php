<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LawFirm;
use Session;

class LawFirmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $litis = LawFirm::all();
        return view('law.index',compact('litis'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('law.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'name' => 'required',
            'cp' => 'required',
            'cn' => 'required|numeric',
            'email' => 'required',
            'address' => 'required'
        ));

        $liti = new LawFirm;
        $liti->name = $request->name;
        $liti->cp = $request->cp;
        $liti->cn = $request->cn;
        $liti->email = $request->email;
        $liti->address = $request->address;
        $liti->save();
        Session::flash('success','Law Firm Added');
        return redirect('law_firm');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $liti = LawFirm::find($id);
        return view('law.edit',compact('liti'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'name' => 'required',
            'cp' => 'required',
            'cn' => 'required|numeric',
            'email' => 'required',
            'address' => 'required'
        ));

        $liti = LawFirm::find($id);
        $liti->name = $request->name;
        $liti->cp = $request->cp;
        $liti->cn = $request->cn;
        $liti->email = $request->email;
        $liti->address = $request->address;
        $liti->save();
        Session::flash('success','Law Firm Updated');
        return redirect('law_firm');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
