<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LawFirm;
use App\AreaExp;
use App\ExternalCounsel;
use App\Country;
use App\Stete;
use Session;

class ExtCounselController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $litis = ExternalCounsel::all();
        return view('ext.index',compact('litis'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $laws = LawFirm::all();
        $areas = AreaExp::all();
        $countries = Country::all();
        $ar1 = array();
        $law = array();
        $area = array();
        foreach ($laws as $la) {
            $law[$la->id] = $la->name;
        }
        foreach ($areas as $are) {
            $area[$are->id] = $are->name;
        }
        foreach($countries as $country){
            $ar1[$country->id] = $country->name;
        }
        return view('ext.create',compact('law','area','ar1'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'country_id' => 'required',
            'state_id' => 'required',
            'city' => 'required',
            'law_id' => 'required',
            'counsel' => 'required',
            'expert_id' => 'required',
        ));
        $liti = new ExternalCounsel;
        $liti->country_id = $request->country_id;
        $liti->state_id = $request->state_id;
        $liti->city = $request->city;
        $liti->law_id = $request->law_id;
        $liti->counsel = $request->counsel;
        $liti->number = $request->number;
        $liti->email = $request->email;
        $liti->address = $request->address;
        $liti->expert_id = $request->expert_id;
        $liti->save();
        Session::flash('success','External Counsel Added');
        return redirect('ex_counsel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $liti = ExternalCounsel::find($id);
        $laws = LawFirm::all();
        $areas = AreaExp::all();
        $countries = Country::all();
        $states = Stete::where('country_id','=',$liti->country_id)->get();
        $ar1 = array();
        $ar2 = array();
        $law = array();
        $area = array();
        foreach ($laws as $la) {
            $law[$la->id] = $la->name;
        }
        foreach ($areas as $are) {
            $area[$are->id] = $are->name;
        }
        foreach($countries as $country){
            $ar1[$country->id] = $country->name;
        }
        return view('ext.edit',compact('liti','law','area','ar1','states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'country_id' => 'required',
            'state_id' => 'required',
            'city' => 'required',
            'law_id' => 'required',
            'counsel' => 'required',
            'expert_id' => 'required',
        ));
        $liti = ExternalCounsel::find($id);
        $liti->country_id = $request->country_id;
        $liti->state_id = $request->state_id;
        $liti->city = $request->city;
        $liti->law_id = $request->law_id;
        $liti->counsel = $request->counsel;
        $liti->number = $request->number;
        $liti->email = $request->email;
        $liti->address = $request->address;
        $liti->expert_id = $request->expert_id;
        $liti->save();
        Session::flash('success','External Counsel Updated');
        return redirect('ex_counsel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
