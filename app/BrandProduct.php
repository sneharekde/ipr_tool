<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandProduct extends Model
{
    public function ent()
    {
        return $this->belongsTo(Entity::class);
    }
}
