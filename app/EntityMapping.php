<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntityMapping extends Model
{
    //
    protected $fillable = ['entity', 'unit', 'function'];
}
