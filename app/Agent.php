<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    //
    protected $fillable = ['name', 'address', 'e_mail', 'mobile_number', 'registration_no'];
}
