<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    //
    protected $fillable = ['entity', 'address_for_service', 'trading_as', 'country', 'state', 'mobile_number', 'applicant_name', 'address','s_state', 's_country', 'e_mail'];
}
