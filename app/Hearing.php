<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hearing extends Model
{
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function ext_counsel()
    {
        return $this->belongsTo(ExternalCounsel::class);
    }
    public function hear()
    {
        return $this->belongsTo(LitigationSummary::class);
    }
}
