<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patent extends Model
{
    protected $fillable = ['priority_date','filing_date','application_no','applicant_name','applicant_id','cat_app','invent_name','title_inven','reg_pat','renewal','gov'];


   public function user()
   {
   	return $this->belongsTo('App\User');
   }
}
