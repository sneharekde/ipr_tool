<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NatureApplicant extends Model
{
    //
    protected $fillable = ['nature_of_applicant'];
}
