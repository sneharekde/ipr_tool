<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionItem extends Model
{
    //
    protected $fillable = [ 'license','license_of_sub','term_of_license','expiry_date','reminder_two','license_document','tradmark_id'];
}
