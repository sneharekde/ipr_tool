<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invest extends Model
{
    public function ent(){
        return $this->belongsTo(Entity::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function inag(){
        return $this->belongsTo(Agency::class);
    }
    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function state(){
        return $this->belongsTo(Stete::class);
    }
    public function city(){
        return $this->belongsTo(City::class);
    }
    public function nop()
    {
        return $this->belongsTo(BrandProduct::class);
    }
}
