<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryApplicant extends Model
{
    protected $fillable = ['name'];
}
