<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stete extends Model
{
    public function country(){
        return $this->belongsTo(Country::class);
    }
}
