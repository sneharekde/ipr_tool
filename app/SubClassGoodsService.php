<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubClassGoodsService extends Model
{
    //
    protected $fillable = ['class_good_services_id', 'class', 'description'];

    public function ClassGoodServices(){
    	return $this->belongsTo(ClassGoodServices::class);
    }
}
