<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enf extends Model
{
    public function country(){
        return $this->belongsTo(Country::class);
    }
    public function state(){
        return $this->belongsTo(Stete::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
    public function ent()
    {
        return $this->belongsTo(Entity::class);
    }
    public function nop()
    {
        return $this->belongsTo(BrandProduct::class);
    }
}
