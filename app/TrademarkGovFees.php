<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrademarkGovFees extends Model
{
    public function appf()
    {
    	return $this->belongsTo(ApplicationField::class);
    }
}
