<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MwpReminder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $reminders,$user;
    public function __construct($reminders,$user)
    {
        $this->reminders = $reminders;
        $this->user= $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.MwpReminder')
        ->subject('Trademark Mandatory Waiting Period Reminder')
        ->with('reminders',$this->reminders)
        ->with('user',$this->user);
    }
}
