<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailEmailHear1 extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $reminders,$user;
    public function __construct($reminders,$user)
    {
        $this->reminders = $reminders;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.mail-hear01')
        ->subject('Litigation Hearing First Reminder')
        ->with('reminders',$this->reminders)
        ->with('user',$this->user);
    }
}
