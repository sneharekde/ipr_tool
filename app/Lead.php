<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    public function target(){
        return $this->belongsTo(Target::class);
    }
}
