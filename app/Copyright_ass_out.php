<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Copyright_ass_out extends Model
{
    protected $fillable = ['name_of_assignor','name_of_assignee','date_of_assignment','docs','comments','assout_id'];
}
