<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatentAssOut extends Model
{
    protected $fillable = ['name_of_assignor','name_of_assignee','date_of_assignment','docs','comments','assin_id'];
}
