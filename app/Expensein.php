<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expensein extends Model
{
   protected $fillable = ['invoice_no','amount','upload','pay_stat','invest_id'];
}
