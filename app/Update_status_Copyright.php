<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Update_status_Copyright extends Model
{
    protected $fillable = ['copy_id','status','sub_status','date','rem','comment','docs','reply_deadline','rem1','rem2','rem3','auto_deadline','cor','dor','doe','roc'];
}
