<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradeClass extends Model
{
    public function trademarks(){
        return $this->belongsToMany(Trademarks::class);
    }
}
