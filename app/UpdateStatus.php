<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpdateStatus extends Model
{
    //
    protected $fillable = ['tradmark_id', 'tradmark_name', 'document', 'comment', 'status', 'stage_date', 'opponent_name', 'opponent_tm_journal_date', 'address', 'class', 'mark','date'];

    public function Addtradmark(){
    	return $this->belongsTo('App\AddTradmark');
    }
}
