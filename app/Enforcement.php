<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enforcement extends Model
{
    protected $fillable = ['target_name','owner_name','state','city','target','name_of_product','user','qty','amount','date','docs'];
}
