<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Update_Status_Design extends Model
{
    protected $fillable = ['des_id','status','sub_status','date','comment','doc','rep_d','rem1','rem2','rem3','rem4','cor','dor','jno','doe'];
}
