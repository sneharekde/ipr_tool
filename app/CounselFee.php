<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CounselFee extends Model
{
    public function counsel(){
        return $this->belongsTo(ExternalCounsel::class);
    }

    public function law()
    {
        return   $this->belongsTo(LawFirm::class);
    }
}
