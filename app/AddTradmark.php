<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddTradmark extends Model
{
    public function prod()
    {
    	return $this->belongsTo(Product::class);
    }
    public function natapl()
    {
    	return $this->belongsTo(NatureApplication::class);
    }
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    public function appf()
    {
    	return $this->belongsTo(ApplicationField::class);
    }
    public function app_type()
    {
    	return $this->belongsTo(TrademarkGovFees::class);
    }
    public function ent()
    {
    	return $this->belongsTo(EntityList::class);
    }
    public function nate()
    {
    	return $this->belongsTo(NatureApplicant::class);
    }
    public function agent()
    {
    	return $this->belongsTo(Agent::class);
    }
    public function catmak()
    {
    	return $this->belongsTo(CategoryMark::class);
    }
    public function lang()
    {
    	return $this->belongsTo(Language::class);
    }
    public function classg()
    {
    	return $this->belongsTo(TradeClass::class);
    }
}
