<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntityList extends Model
{
    //
    protected $fillable = ['parent_entity_id', 'entity_child'];

    public function ParentEntity(){
    	return $this->belongsTo('App\ParentEntity');
    }
}
