<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassGoodServices extends Model
{
    //
    protected $fillable = ['class_of_good_services'];

    public function SubClassGoodsService(){
    	return $this->hasMany(SubClassGoodsService::class);
    }
}
