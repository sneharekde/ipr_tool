<?php

namespace App\Console\Commands;

use App\Hearing;
use App\Mail\LitiReminder1;
use App\User;
use Illuminate\Console\Command;
use Mail;

class LitiRem1Emails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:litirem1';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         // Get all reminder for todays
        $reminders = Hearing::query()
        ->with(['hear'])
        ->where('alert1',now()->format('Y-m-d'))
        ->where('sent_status','1')
        ->get();

        //  Group by user
        $data = [];
        foreach ($reminders as $reminder) {
            $data[$reminder->hear->user_id][] = $reminder->toArray();
        }

         // Send Email
         foreach ($data as $userId => $reminders) {
            $this->sendEmailToUser($userId, $reminders);
        }

    }

    private function sendEmailToUser($userId, $reminders)
    {
        $user = User::find($userId);

        Mail::to($user)->send(new LitiReminder1($reminders,$user));
    }
}
