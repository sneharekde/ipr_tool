<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UpdateCopyrightStatus;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmailCopy3;
class SendReminderCopy3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:copy3';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get all reminder for todays
        $reminders = UpdateCopyrightStatus::query()
        ->with(['copy'])
        ->where('rem3',now()->format('Y-m-d'))
        ->where('sent_status','1')
        ->get();

        //  Group by user

        $data = [];
        foreach ($reminders as $reminder) {
            $data[$reminder->copy->user_id][] = $reminder->toArray();
        }

        // Send Email
        foreach ($data as $userId => $reminders) {
            $this->sendEmailToUser($userId, $reminders);
        }
    }

    private function sendEmailToUser($userId, $reminders)
    {
        $user = User::find($userId);

        Mail::to($user)->send(new SendEmailCopy3($reminders,$user));
    }
}
