<?php

namespace App\Console\Commands;

use App\Mail\SendDailyMailReminder;
use App\UpdateStatusTrad;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\User;
use Mail;


class SendDailyMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:dailymail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currdate = Carbon::now();
        $reminders = UpdateStatusTrad::whereRaw('"'.$currdate.'" between `int_dead` and `cou_dead`')
                    ->where('sent_status','1')
                    ->get();

        $users = User::where('role','IP Head')->get();
        // Send Email
        foreach ($users as $user) {
            $this->sendEmailToUser($user, $reminders);
        }
    }

    private function sendEmailToUser($user, $reminders){
        Mail::to($user)->send(new SendDailyMailReminder($reminders,$user));
    }
}
