<?php

namespace App\Console\Commands;

use App\Fir;
use App\Mail\FirReminder;
use App\User;
use Illuminate\Console\Command;
use Mail;

class FirReminder2Email extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:fir2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get all reminder for todays
        $reminders = Fir::query()
        ->with(['fir'])
        ->where('rem2',now()->format('Y-m-d'))
        ->get();

        //  Group by user

        $users = User::where('role','Brand Protection Manager')->get();
        // Send Email
        foreach ($users as $user) {
            $this->sendEmailToUser($user, $reminders);
        }
    }

    private function sendEmailToUser($user, $reminders)
    {
        Mail::to($user)->send(new FirReminder($reminders,$user));
    }
}
