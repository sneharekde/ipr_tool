<?php

namespace App\Console\Commands;

use App\Mail\DesignReminder4;
use App\UpdataStatusDesign;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail as FacadesMail;
use Mail;


class SendDesignReminder4Emails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:design4';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get all reminder for todays
        $reminders = UpdataStatusDesign::query()
        ->with(['design'])
        ->where('rem4',now()->format('Y-m-d'))
        ->where('sent_status','1')
        ->get();

         //  Group by user

        //  $data = [];
        //  foreach ($reminders as $reminder) {
        //      $data[$reminder->design->user_id][] = $reminder->toArray();
        //  }

        $users = User::where('role','=','IP Head')->get();

         // Send Email
        foreach ($users as $user) {
            $this->sendEmailToUser($user, $reminders);
        }
    }

    private function sendEmailToUser($user, $reminders){
        Mail::to($user)->send(new DesignReminder4($reminders,$user));
    }
}
