<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ActionItem;
use Mail;

class ReminderDateMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tradmark:reminderdatemessage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder Date For Trademark';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    //     //
    //      $actions = ActionItem::whereMonth('reminder_one', '=', date('m'))->whereDay('reminder_one', '=', date('d'))->get();
 
    // foreach($actions as $action) {

    //     Mail::queue('emails.reminder_message', ['action' => $action], function ($mail) use ($action) {
    //         $mail->to('asif85.in@gmail.com')
    //             ->from('hello@example.com', 'Company')
    //             ->subject('Trademark Reminder Date!');
    //     });
 
    // }
 
    // $this->info('Reminder messages sent successfully!');

    // }

    $i = 0;
   $actions = ActionItem::whereMonth('reminder_one', '=', date('m'))->whereDay('reminder_one', '=', date('d'))->get();

   foreach($actions as $action)
   {
       $email="4bc0b35070-9718bc@inbox.mailtrap.io";
       Mail::to($email)->send($action);
       $i++; 
   }
   
$this->info($i.' Reminder messages sent successfully!');
}

}
