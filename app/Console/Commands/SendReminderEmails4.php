<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UpdateStatusTrad;
use App\User;
use App\Mail\ReminderEmailFour;
use Mail;

class SendReminderEmails4 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:emails4';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email notification to user about reminders.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get all reminders for today

        $reminders = UpdateStatusTrad::query()
        ->with(['trade'])
        ->where('rem4',now()->format('Y-m-d'))
        ->where('sent_status','1')
        ->get();

        //  Group by user

        // $data = [];
        // foreach ($reminders as $reminder) {
        //     $data[$reminder->trade->user_id][] = $reminder->toArray();
        // }

        $users = User::where('role','=','IP Head')->get();

        // Send Email
       foreach ($users as $user) {
           $this->sendEmailToUser($user, $reminders);
       }
    }

    private function sendEmailToUser($user, $reminders)
    {

        Mail::to($user)->send(new ReminderEmailFour($reminders,$user));
    }
}
