<?php

namespace App\Console;

use Illuminate\Console\Command;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ReminderDateMessage::class,
        Commands\SendReminderEmails::class,
        Commands\SendReminderEmails2::class,
        Commands\SendReminderEmails3::class,
        Commands\SendReminderEmails4::class,
        Commands\SendDesignReminder1::class,
        Commands\SendDesignReminder2::class,
        Commands\SendDesignReminder3::class,
        Commands\SendCopyReminder1::class,
        Commands\ReminderCopy2::class,
        Commands\SendReminderCopy3::class,
        Commands\SendNoticeEmails::class,
        Commands\LitiRem1Emails::class,
        Commands\SendDesignReminder4Emails::class,
        Commands\SendMwpReminderEmails::class,
        Commands\FirReminderEmail1::class,
        Commands\FirReminder2Email::class,
        Commands\FirReminder3Email::class,
        Commands\NoticeReminderEmail::class,
        Commands\LitiHearing02Emails::class,
        Commands\LitiRem3Emails::class,
        Commands\MailEmailHear::class,
        Commands\MailEmailHearLi::class,
        Commands\MailEmailLiti03::class,
        Commands\SendDailyMail::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        //$schedule->command('tradmark:reminderdatemessage');
        //$schedule->command('demo:cron');
        $schedule->command('reminder:emails');
        $schedule->command('reminder:emails2');
        $schedule->command('reminder:emails3');
        $schedule->command('reminder:emails4');
        $schedule->command('reminder:design1');
        $schedule->command('reminder:design2');
        $schedule->command('reminder:design3');
        $schedule->command('reminder:copy1');
        $schedule->command('reminder:copy2');
        $schedule->command('reminder:copy3');
        $schedule->command('reminder:noticer');
        $schedule->command('reminder:litirem1');
        $schedule->command('reminder:design4');
        $schedule->command('reminder:mwp');
        $schedule->command('reminder:fir1');
        $schedule->command('reminder:fir2');
        $schedule->command('reminder:fir3');
        $schedule->command('reminder:notice');
        $schedule->command('reminder:mailem2');
        $schedule->command('reminder:mailem3');
        $schedule->command('reminder:mailem1');
        $schedule->command('reminder:litirem3');
        $schedule->command('reminder:litirem2');
        $schedule->command('reminder:notice');
        $schedule->command('reminder:dailymail');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
