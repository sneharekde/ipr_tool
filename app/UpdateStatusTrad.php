<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpdateStatusTrad extends Model
{
    public function trade()
    {
    	return $this->belongsTo(Trademarks::class);
    }
}
