<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investigation extends Model
{
    protected $fillable = ['inag','contact_n','contact_no','email','state','city','target','product','dsi','dei','docs','user','country'];
}
