<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NatureAgent extends Model
{
    //
    protected $fillable = ['nature_of_agent'];
}
