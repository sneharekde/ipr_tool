<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title>Dashboard</title>
    <!-- This page CSS -->
    <link href="{{asset('css/morris.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('css/style.min.css')}}" rel="stylesheet">
    <!-- Dashboard 31 Page CSS -->
    <link href="{{asset('css/dashboard3.css')}}" rel="stylesheet">
    <link href="{{asset('css/widget-page.css')}}" rel="stylesheet">
    <link href="{{asset('css/css-chart.css')}}" rel="stylesheet">

    <link href="{{asset('css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.dataTables.min.css')}}">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/switchery.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/bootstrap-select.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/bootstrap-tagsinput.css')}}" rel="stylesheet" />
    <link href="{{asset('css/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/multi-select.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/dual-listbox.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/sweetalert2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/parsley.css') }}" type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    {!! Charts::styles() !!}
    <style>
        table thead tr {
            background: #01c0c8;
            color: white;
            font-weight: bold;
        }
        input.form-control {
            border: 1px solid gray;
        }
        textarea.form-control{
            border: 1px solid gray;
        }
        select.form-control{
            border: 1px solid gray;
        }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesnt work if you view the page via file: -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/dual-listbox.js')}}"></script>
    <script src="https://momentjs.com/downloads/moment.min.js"></script>
</head>

<body class="horizontal-nav boxed skin-megna fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
     <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">TV9 Network</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <div class="">
                <!-- Logo icon --><b>
                <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                <!-- Dark Logo icon -->
                <!-- <img src="{{asset('img/logo-icon.png')}}" alt="homepage" class="dark-logo" /> -->
                <!-- Light Logo icon -->
                <!-- <img src="{{asset('img/logo-light-icon.png')}}" alt="homepage" class="light-logo" /> -->
                </b>
                <!--End Logo icon -->
                <!-- Logo text --><span class="hidden-sm-down">
                <!-- dark Logo text -->
                {{-- <img src="{{asset('img/IPonn.jpg')}}" class="img-fluid" alt="homepage" class="dark-logo" / width="10%" height="10%" style="margin-top: 20px;margin-bottom: 20px"> --}}
                <!-- Light Logo text -->
                {{--<img src="{{asset('img/IPonn.jpg')}}" width="50%" class="light-logo img-fluid" alt="homepage" /></span> </a> --}}
            </div>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <img src="{{asset('img/tv9_logo.png')}}" class="img-fluid" alt="homepage" class="dark-logo" style="margin-top: 10px;margin-bottom: 10px; padding-right: 20px;width: 12%;">
                        <li style="padding-top: 50px"> <a href="{{action('FrontEndController@index')}}" ><i class="icon-speedometer"></i><span class="hide-menu">Home <span class="badge badge-pill badge-cyan ml-auto">4</span></span></a>
                        </li>
                        @if((Auth::user()->role == 'Administrator') || (Auth::user()->role == 'IP Head') || (Auth::user()->role == 'Internal Associate'))
                        <li style="padding-top: 50px"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><span class="hide-menu">Trademark</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{action('FrontEndController@tradmark_portfolio')}}"> TM Portfolio</a></li>
                                <li><a href="{{action('AddTradmarkController@tm_reports')}}">Trademark Summary Reports</a></li>
                            </ul>
                        </li>
                        @endif
                        @if((Auth::user()->role == 'Administrator') || (Auth::user()->role == 'IP Head') || (Auth::user()->role == 'Brand Protection Manager') || (Auth::user()->role == 'Internal Associate'))
                        <li style="padding-top: 50px"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><span class="hide-menu">Brand Protection</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{action('AddBrandController@InvestShow')}}"> Investigation</a></li>
                                <li><a href="{{action('AddBrandController@EnforceShow')}}"> Enforcement</a></li>
                                <li><a href="{{action('AddBrandController@Analytics')}}"> Analytics</a></li>
                                <li><a href="{{ url('assign-to') }}">Assign To Investigation</a></li>
                                <li><a href="{{url('leads')}}">Lead Summary</a></li>

                            </ul>
                        </li>
                        @endif
                        @if((Auth::user()->role == 'Administrator') || (Auth::user()->role == 'IP Head') || (Auth::user()->role == 'Internal Associate'))
                        <li style="padding-top: 50px"><a href="{{action('DesignController@index')}}"> Design</a></li>
                        <li style="padding-top: 50px"><a href="{{action('CopyrightController@index')}}"> Copyright</a></li>
                        <li style="padding-top: 50px"><a href="{{action('PatentController@index')}}"> Patent</a></li>
                        <li style="padding-top: 50px"><a href="{{action('FactSheetController@index')}}"> Fact Sheet</a></li>
                        @endif
                        @if((Auth::user()->role == 'Administrator') || (Auth::user()->role == 'IP Head') || (Auth::user()->role == 'Ex-Counsel') || (Auth::user()->role == 'Internal Associate'))
                        <li style="padding-top: 50px"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><span class="hide-menu">Litigation</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('notices') }}"> Notices</a></li>
                                <li><a href="{{ url('summary') }}"> Litigation Summary</a></li>
                            </ul>
                        </li>
                        @endif
                        @if((Auth::user()->role == 'Administrator') || (Auth::user()->role == 'IP Head') || (Auth::user()->role == 'Internal Associate'))
                        <li style="padding-top: 50px"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><span class="hide-menu">License</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('licenseIn') }}"> License In</a></li>
                                <li><a href="{{ url('licenseOut') }}"> License Out</a></li>
                            </ul>
                        </li>
                        <li style="padding-top: 50px"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><span class="hide-menu">Assign</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('assignIn') }}"> Assign In</a></li>
                                <li><a href="{{ url('assignOut') }}"> Assign Out</a></li>
                            </ul>
                        </li>
                        @endif
                        @if(Auth::user()->role == 'Administrator')
                        <li style="padding-top: 50px"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><span class="hide-menu"> Administration</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{action('AddTradmarkController@entity')}}"> <img src="{{url('img/headoffice.png')}}" alt="" height="22px"> Entity</a></li>
                                <li><a href="{{action('AddTradmarkController@ShowUnit')}}"> <img src="{{url('img/location_new.png')}}" alt="" height="22px"> Unit Location</a></li>
                                <li><a href="{{action('UserController@ShowUser')}}"> <img src="{{url('img/AddUser.png')}}" alt="" height="22px"> Users</a></li>
                                <li><a href="{{action('AddTradmarkController@ShowFunction')}}"> <img src="{{url('img/department.jpg')}}" alt="" height="22px"> Function | Department</a></li>
                                <li><a href="{{action('AddTradmarkController@ShowDesignation')}}"><img src="{{url('img/Designation.png')}}" alt="" height="22px"> Designation </a></li>
                                <li><a href="{{action('AddTradmarkController@entityMapping')}}"><img src="{{url('img/UserEntityMapping.png')}}" alt="" height="22px"> Entity Mapping </a></li>
                            </ul>
                        </li>
                        @endif
                        @if((Auth::user()->role == 'Administrator') || (Auth::user()->role == 'IP Head'))
                            <li style="padding-top: 50px"> <a  href="{{action('AddTradmarkController@master')}}" aria-expanded="false"> Masters</a>
                            </li>
                        @endif
                        {{--<li style="padding-top: 50px"> <a  href="{{action('AddTradmarkController@support')}}"><i class="fa fa-phone"></i>Support</a>
                        </li> --}}
                        <li style="padding-top: 50px;"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-user"></i><span class="hide-menu"> </span>@auth {{ Auth::user()->user_name }} @endauth&nbsp;</a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{url('/logout')}}"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- End User Profile -->
                    <!-- ============================================================== -->
                    <!--   <li class="nav-item right-side-toggle"> <a class="nav-link  waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li> -->
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
