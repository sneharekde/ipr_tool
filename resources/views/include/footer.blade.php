    {{--
    <footer class="footer">
        All right reserved By |  <img src="{{url('images/bd.jpg')}}" alt="" width="250px">
    </footer>
    --}}
    </div>
        <script src="{{asset('js/popper.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="{{asset('js/perfect-scrollbar.jquery.min.js')}}"></script>
        <!--Wave Effects -->
        <script src="{{asset('js/waves.js')}}"></script>
        <!--Menu sidebar -->
        <script src="{{asset('js/sidebarmenu.js')}}"></script>
        <!--Custom JavaScript -->
        <script src="{{asset('js/custom.min.js')}}"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue"></script>
        <!-- ============================================================== -->
        <!-- This page plugins -->
        <!-- ============================================================== -->
        <!--sparkline JavaScript -->
        <script src="{{asset('js/raphael-min.js')}}"></script>
        <script src="{{asset('js/morris.js')}}"></script>
        <script src="{{asset('js/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('js/sticky-kit.min.js')}}"></script>
        <!-- Vector map JavaScript -->
        <!-- Chart JS -->
        <script src="{{asset('js/dashboard3.js')}}"></script>
        <script src="{{asset('js/widget-data.js')}}"></script>
        <script src="{{asset('js/moment.js')}}"></script>
        <!-- Color Picker Plugin JavaScript -->
        <script src="{{asset('js/jquery-asColor.js')}}"></script>
        <script src="{{asset('js/jquery-asGradient.js')}}"></script>
        <script src="{{asset('js/jquery-asColorPicker.min.js')}}"></script>
        <!-- This is data table -->
        <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('js/dataTables.responsive.min.js')}}"></script>
        <!-- start - This is for export functionality only -->
        <script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('js/buttons.flash.min.js')}}"></script>
        <script src="{{asset('js/jszip.min.js')}}"></script>
        <script src="{{asset('js/pdfmake.min.js')}}"></script>
        <script src="{{asset('js/vfs_fonts.js')}}"></script>
        <script src="{{asset('js/buttons.html5.min.js')}}"></script>
        <script src="{{asset('js/buttons.print.min.js')}}"></script>
        <script src="{{asset('js/switchery.min.js')}}"></script>
        <script src="{{asset('js/select2.full.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('js/bootstrap-select.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('js/bootstrap-tagsinput.min.js')}}"></script>
        <script src="{{asset('js/jquery.bootstrap-touchspin.js')}}" type="text/javascript"></script>
        <script src="{{asset('js/dff.js')}}" type="text/javascript"></script>
        <script type="text/javascript" src="{{asset('js/jquery.multi-select.js')}}"></script>
        <script src="{{asset('js/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('js/sweet-alert.init.js')}}"></script>
        <script src="{{asset('js/countries.js')}}"></script>
        <script src="{{ asset('js/parsley.min.js') }}"></script>
        <script src="{{ asset('js/function.js') }}"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'desc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([3, 'desc']).draw();
                } else {
                    table.order([3, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
       {
           extend: 'pdf',
           footer: true,
           title: 'tradmark Reports export',
           orientation: 'landscape',
           pageSize: 'LEGAL',
           exportOptions: {
                columns: [0,2,3,4,5,6,7,8,9,10,11,12]
            }
       },
       {
           extend: 'excel',
           title: 'tradmark Reports export',
           footer: false,
           exportOptions: {
                columns: [0,2,3,4,5,6,7,8,9,10,11,12]
            }
       },
       {
        extend: 'print',
        title: 'tradmark Reports export',
        footer: false,
        exportOptions: {
                columns: [0,2,3,4,5,6,7,8,9,10,11,12]
            }
       }
    ]
    } );
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
    <script>
        $(function () {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function () {
                new Switchery($(this)[0], $(this).data());
            });
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();
            //Bootstrap-TouchSpin
            $(".vertical-spin").TouchSpin({
                verticalbuttons: true
            });
            var vspinTrue = $(".vertical-spin").TouchSpin({
                verticalbuttons: true
            });
            if (vspinTrue) {
                $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
            }
            $("input[name='tch1']").TouchSpin({
                min: 0,
                max: 100,
                step: 0.1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                postfix: '%'
            });
            $("input[name='tch2']").TouchSpin({
                min: -1000000000,
                max: 1000000000,
                stepinterval: 50,
                maxboostedstep: 10000000,
                prefix: '$'
            });
            $("input[name='tch3']").TouchSpin();
            $("input[name='tch3_22']").TouchSpin({
                initval: 40
            });
            $("input[name='tch5']").TouchSpin({
                prefix: "pre",
                postfix: "post"
            });
            // For multiselect
            $('#pre-selected-options').multiSelect();
            $('#optgroup').multiSelect({
                selectableOptgroup: true
            });
            $('#public-methods').multiSelect();
            $('#select-all').click(function () {
                $('#public-methods').multiSelect('select_all');
                return false;
            });
            $('#deselect-all').click(function () {
                $('#public-methods').multiSelect('deselect_all');
                return false;
            });
            $('#refresh').on('click', function () {
                $('#public-methods').multiSelect('refresh');
                return false;
            });
            $('#add-option').on('click', function () {
                $('#public-methods').multiSelect('addOption', {
                    value: 42,
                    text: 'test 42',
                    index: 0
                });
                return false;
            });
            $(".ajax").select2({
                ajax: {
                    url: "https://api.github.com/search/repositories",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
                //templateResult: formatRepo, // omitted for brevity, see the source of this page
                //templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
            });
        });
    </script>
</body>

</html>
