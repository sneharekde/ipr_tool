<ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link {{ Request::is('dashbord') ? 'active' : '' }}" href="{{ url('dashbord') }}">Trademark</a>
    </li>
    <li class="nav-item">
      <a class="nav-link {{ Request::is('branddashboard') ? 'active' : '' }}"  href="{{ url('/branddashboard') }}">Brand Protection</a>
    </li>
    <li class="nav-item">
      <a class="nav-link {{ Request::is('designdashboard') ? 'active' : '' }}" href="{{ url('/designdashboard') }}">Design</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Request::is('copydashboard') ? 'active' : '' }}" href="{{ url('/copydashboard') }}">Copyright</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Request::is('patentdashboard') ? 'active' : '' }}" href="{{ url('/patentdashboard') }}">Patent</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Request::is('litidashboard') ? 'active' : '' }}" href="{{ url('/litidashboard') }}">Litigation</a>
    </li>
</ul>
