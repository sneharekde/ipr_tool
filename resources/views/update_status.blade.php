@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);

    }

</style>
  <div class="page-wrapper">
    <div class="container-fluid">
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Trademark Status | <a href="{{action('FrontEndController@tradmark_portfolio')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ action('FrontEndController@index') }}">Home</a></li>
              <li class="breadcrumb-item active">Trademark Status</li>
            </ol>
          </div>
        </div>
      </div>
      @include('include.message')
      <div class="card">
        <div class="card-body" id="app">
          <form action="{{ url('/status/submit') }}" data-parsley-validate="" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="form-group">
                <input type="hidden" name="trade_id" value="{{ $add_tradmark->id }}">
                <label>Status</label>
                <select class="form-control" name="status" id="status" required="">
                  <option value="">Select</option>
                  <option value="Active Application" @if($add_tradmark->status == 'Registered' || $add_tradmark->status == 'Abandoned or rejected' || $add_tradmark->status == 'Withdrawn') disabled @endif>Active Application</option>
                  <option value="Registered" @if($add_tradmark->status == 'Registered' || $add_tradmark->status == 'Abandoned or rejected' || $add_tradmark->status == 'Withdrawn') disabled @endif>Registered</option>
                  <option value="Abandoned or rejected" @if($add_tradmark->status == 'Abandoned or rejected' || $add_tradmark->status == 'Withdrawn') disabled @endif>Abandoned or rejected</option>
                  <option value="Withdrawn" @if($add_tradmark->status == 'Withdrawn') disabled @endif>Withdrawn</option>
                </select>
              </div>
            </div>
            <div class="col-lg-12 col-md-12">
              <div class="form-group" id="sub_status">
                <label>Sub Status</label>
                <select name="sub_status" class="form-control" id="select1">
                  <option value="">Select</option>
                  <option value="Application Submitted" disabled>Application Submitted</option>
                  <option value="Sent to Vienna Codification" @if(($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Sent to Vienna Codification') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Marked for exam') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Examination report received') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Replied to examination report') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Hearing for objection') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Accepted and advertised') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Opposition noticed received') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Counter statment submitted') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Evidence and hearing') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Decision pending')) disabled @endif>Sent to Vienna Codification</option>
                  <option value="Marked for exam" @if(($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Marked for exam') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Examination report received') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Replied to examination report') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Hearing for objection') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Accepted and advertised') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Opposition noticed received') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Counter statment submitted') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Evidence and hearing') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Decision pending')) disabled @endif>Marked for exam</option>
                  <option value="Examination report received" @if(($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Examination report received') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Replied to examination report') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Hearing for objection') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Accepted and advertised') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Opposition noticed received') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Counter statment submitted') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Evidence and hearing') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Decision pending')) disabled @endif>Examination report received</option>
                  <option value="Replied to examination report" @if(($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Replied to examination report') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Hearing for objection') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Accepted and advertised') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Opposition noticed received') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Counter statment submitted') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Evidence and hearing') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Decision pending')) disabled @endif>Replied to examination report</option>
                  <option value="Hearing for objection" @if(($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Hearing for objection') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Accepted and advertised') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Opposition noticed received') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Counter statment submitted') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Evidence and hearing') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Decision pending')) disabled @endif>Hearing for objection</option>
                  <option value="Accepted" @if(($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Accepted') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Accepted and advertised') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Opposition noticed received') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Counter statment submitted') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Evidence and hearing') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Decision pending')) disabled @endif>Accepted</option>
                  <option value="Accepted and advertised" @if(($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Accepted and advertised') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Opposition noticed received') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Counter statment submitted') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Evidence and hearing') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Decision pending')) disabled @endif>Accepted and advertised</option>
                  <option value="Opposition noticed received" @if(($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Opposition noticed received') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Counter statment submitted') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Evidence and hearing') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Decision pending')) disabled @endif>Opposition noticed received</option>
                  <option value="Counter statment submitted" @if(($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Counter statment submitted') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Evidence and hearing') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Decision pending')) disabled @endif>Counter statment submitted</option>
                  <option value="Evidence and hearing" @if(($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Evidence and hearing') || ($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Decision pending')) disabled @endif>Evidence and hearing</option>
                  <option value="Decision pending" @if(($add_tradmark->status == 'Active Application' && $add_tradmark->sub_status == 'Decision pending')) disabled @endif>Decision pending</option>
                </select>
              </div>
            </div>
          </div>
          {{-- Status Wise Field Show --}}
          <div id="reg">
            <div class="form-group col-md-12">
              <label>Date of Registration:</label>
              <input type="text" name="dor" id="regd" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              <input type="file" name="docs7" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm9" class="form-control" rows="5"></textarea>
            </div>
          </div>
          <div id="aban">
            <div class="form-group col-md-12">
              <label>Date:</label>
              <input type="text" name="date3" id="abad" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              <input type="file" name="docs8" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm10" class="form-control" rows="5"></textarea>
            </div>
          </div>
          <div id="witd">
            <div class="form-group col-md-12">
              <label>Date:</label>
              <input type="text" id="withd" name="date4" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              <input type="file" name="docs9" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm11" class="form-control" rows="5"></textarea>
            </div>
          </div>
          {{-- Sub Status Wise Show --}}
          <div id="mfe">
            <div class="form-group col-md-12">
              <label>Date:</label>
              <input type="text" name="date1" id="mfed" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm1" class="form-control" rows="5"></textarea>
            </div>
          </div>
          <div id="errs">
            <div class="form-group col-md-12">
              <label>Date of Receipt of ER:</label>
              <input type="text" name="rdate" id="errd" class="form-control">
            </div>
            <div class="form-group col-md-12">
                <label for="">Deadline for Reply:</label>
                <input type="text" name="edeadline" id="dropdate" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              <input type="file" name="docs1" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm2" class="form-control" rows="5"></textarea>
            </div>
            <div class="form-group col-md-12">
              <label>Reminder1:</label>
              <input type="text" name="rem11" id="remd1" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Reminder2:</label>
              <input type="text" name="rem21" id="remd2" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Reminder3:</label>
              <input type="text" name="rem31" id="remd3" class="form-control">
            </div>
          </div>
          <div id="rter">
            <div class="form-group col-md-12">
              <label>Replied Date:</label>
              <input type="text" id="rtod" name="rpdate" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              <input type="file" name="docs2" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm3" class="form-control" rows="5"></textarea>
            </div>
          </div>
          <div id="hfob">
            <div class="form-group col-md-12">
              <label>Date for Hearing:</label>
              <input type="text" id="herds" name="dfh" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              <input type="file" name="docss[]" class="form-control" multiple>
            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm4" class="form-control" rows="5"></textarea>
            </div>
            <div class="form-group col-md-12">
              <label>Reminder1:</label>
              <input type="text" id="herd1" name="rem12" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Reminder2:</label>
              <input type="text" id="herd2" name="rem22" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Reminder2:</label>
              <input type="text" id="herd3" name="rem32" class="form-control">
            </div>
          </div>
          <div id="acads">
            <div class="form-group col-md-12">
              <label>Date of Advertisement:</label>
              <input type="text" name="doa" id="adad" class="form-control">
            </div>
            <div class="form-group col-md-12">
                <label>Journal number:</label>
                <input type="text" name="jno" class="form-control">
              </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              <input type="file" name="docs3" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm5" class="form-control" rows="5"></textarea>
            </div>
          </div>
          <div id="opnor">
            <div class="form-group col-md-12">
              <label>Date of Notice:</label>
              <input type="text" name="nod" id="opon" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Date of Receipt of Notice:</label>
              <input type="text" name="recd" id="oponr" class="form-control">
            </div>
            <div class="col-md-12 form-group">
                <label for="">Deadline for Counterstatement</label>
                <input type="text" name="dfcstate" id="dfcstate" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              <input type="file" name="docs4" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm6" class="form-control" rows="5"></textarea>
            </div>
            <div class="form-group col-md-12">
                <label>Reminder1:</label>
                <input type="text" id="evdd1s" name="rem13s" class="form-control">
              </div>
              <div class="form-group col-md-12">
                <label>Reminder2:</label>
                <input type="text" id="evdd2s" name="rem23s" class="form-control">
              </div>
              <div class="form-group col-md-12">
                <label>Reminder3:</label>
                <input type="text" id="evdd3s" name="rem33s" class="form-control">
              </div>
          </div>
          <div id="coss">
            <div class="form-group col-md-12">
              <label>Date:</label>
              <input type="text" name="date2" id="countsd" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              <input type="file" name="docs5" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm7" class="form-control" rows="5"></textarea>
            </div>
          </div>
          <div id="evhe">
            <div class="form-group col-md-12">
              <label>Date of evidence / hearing:</label>
              <input type="text" id="evdds" name="devd" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              <input type="file" name="docs6" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm8" class="form-control" rows="5"></textarea>
            </div>

            <div class="form-group col-md-12">
              <label>Reminder1:</label>
              <input type="text" id="evdd1" name="rem13" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Reminder2:</label>
              <input type="text" id="evdd2" name="rem23" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Reminder3:</label>
              <input type="text" id="evdd3" name="rem33" class="form-control">
            </div>
          </div>
          <div id="depe">
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm12" class="form-control" rows="5"></textarea>
            </div>
          </div>
          <button type="submit" class="btn btn-success">Submit</button>
        </form>
        </div>
      </div>




      <div class="card">
        <div class="card-body">
        {{-- Datat --}}
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Date</th>
              <th>Status</th>
              <th>Sub Status</th>
              <th>Docs</th>
              <th>Comments</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach($stats as $stat)
            <tr>
              <td>{{ $no++ }}</td>
              <td>
                @if($stat->date !='')
                  {{ date('d-m-Y',strtotime($stat->date)) }}
                @elseif($stat->rec_date != '')
                  {{ date('d-m-Y',strtotime($stat->rec_date)) }}
                @elseif($stat->rep_date != '')
                  {{ date('d-m-Y',strtotime($stat->rep_date)) }}
                @elseif($stat->her_date != '')
                  {{ date('d-m-Y',strtotime($stat->her_date)) }}
                @elseif($stat->adv_date != '')
                  {{ date('d-m-Y',strtotime($stat->adv_date)) }}
                @elseif($stat->not_date != '')
                  {{ date('d-m-Y',strtotime($stat->not_date)) }}
                @elseif($stat->evi_date != '')
                  {{ date('d-m-Y',strtotime($stat->evi_date)) }}
                @elseif($stat->dor_date != '')
                  {{ date('d-m-Y',strtotime($stat->dor_date)) }}
                @endif
              </td>
              <td>{{ $stat->status }}</td>
              <td>{{ $stat->sub_status }}</td>
              <td>
                @if($stat->docs != '')
                  @foreach(explode(',', $stat->docs) as $file)
                    <li style="list-style: none;">
                      <a href="{{ url('public/trademark/'.$file) }}" download="">{{ $file }}</a>{{--
                      <img src="{{ url('public/trademark/'.$file) }}" class="img-responsive img-thumbnail"> --}}
                    </li>
                  @endforeach
                @endif
              </td>
              <td>{{ $stat->comments }}</td>
              <td>
                  @if ($stat->sub_status !='Application Submitted')
                    <a href="{{ url('/trademark/update_status/edit/'.$stat->id) }}" class="btn btn-success btn-sm">Edit</a>
                  @endif

                {{--  <a href="{{ url('update_status/delete/'.$stat->id) }}" class="btn btn-danger btn-sm">Delete</a>  --}}
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    {{-- Status Wise JS --}}
    $(function()
    {
      $('#sub_status').hide();
      $('#reg').hide();
      $('#aban').hide();
      $('#witd').hide();
      $('#status').change(function()
      {
        if($('#status').val() == '')
        {
          $('#sub_status').hide();
          $('#mfe').hide();
          $('#errs').hide();
          $('#rter').hide();
          $('#hfob').hide();
          $('#acads').hide();
          $('#opnor').hide();
          $('#coss').hide();
          $('#evhe').hide();
          $('#depe').hide();
          $('#reg').hide();
          $('#aban').hide();
          $('#witd').hide();
        }
      });

      $('#status').change(function(){
        if ($('#status').val() == 'Active Application')
          {
            $('#sub_status').show();
          }
          else
          {
            $('#sub_status').hide();
            $('#mfe').hide();
            $('#errs').hide();
            $('#rter').hide();
            $('#hfob').hide();
            $('#acads').hide();
            $('#opnor').hide();
            $('#coss').hide();
            $('#evhe').hide();
            $('#depe').hide();
          }
        });

        $('#status').change(function(){
          if ($('#status').val() == 'Registered')
          {
            $('#reg').show();
            $('#mfe').hide();
            $('#errs').hide();
            $('#rter').hide();
            $('#hfob').hide();
            $('#acads').hide();
            $('#opnor').hide();
            $('#coss').hide();
            $('#evhe').hide();
            $('#depe').hide();
          }
          else
          {
            $('#reg').hide();
          }
        });

        $('#status').change(function(){
          if ($('#status').val() == 'Abandoned or rejected')
          {
            $('#aban').show();
          }
          else
          {
            $('#aban').hide();
          }
        });

        $('#status').change(function(){
          if ($('#status').val() == 'Withdrawn')
          {
            $('#witd').show();
          }
          else
          {
            $('#witd').hide();
          }
        });
      });
      // Sub Status Quer
      $(function()
      {
        $('#mfe').hide();
        $('#errs').hide();
        $('#rter').hide();
        $('#hfob').hide();
        $('#acads').hide();
        $('#opnor').hide();
        $('#coss').hide();
        $('#evhe').hide();
        $('#depe').hide();
        $('#select1').change(function()
        {
          if ($('#select1').val() == 'Marked for exam')
          {
            $('#mfe').show();
          }
          else
          {
            $('#mfe').hide();
          }
        });

        $('#select1').change(function()
        {
          if ($('#select1').val() == 'Examination report received')
          {
            $('#errs').show();
          }
          else
          {
            $('#errs').hide();
          }
        });

        $('#select1').change(function(){
          if ($('#select1').val() == 'Replied to examination report')
          {
            $('#rter').show();
          }
          else
          {
            $('#rter').hide();
          }
        });

        $('#select1').change(function(){
          if ($('#select1').val() == 'Hearing for objection')
          {
            $('#hfob').show();
          }
          else
          {
            $('#hfob').hide();
          }
        });

        $('#select1').change(function(){
          if ($('#select1').val() == 'Accepted and advertised')
          {
            $('#acads').show();
          }
          else
          {
            $('#acads').hide();
          }
        });

        $('#select1').change(function(){
          if ($('#select1').val() == 'Opposition noticed received')
          {
            $('#opnor').show();
          }
          else
          {
            $('#opnor').hide();
          }
        });

        $('#select1').change(function(){
          if ($('#select1').val() == 'Counter statment submitted')
          {
            $('#coss').show();
          }
          else
          {
            $('#coss').hide();
          }
        });

        $('#select1').change(function(){
          if ($('#select1').val() == 'Evidence and hearing')
          {
            $('#evhe').show();
          }
          else
          {
            $('#evhe').hide();
          }
        });

        $('#select1').change(function(){
          if ($('#select1').val() == 'Decision pending')
          {
            $('#depe').show();
          }
          else
          {
            $('#depe').hide();
          }
        });
    });
  </script>
@include('include.footer')



<script>
    $(function() {
        $('#errd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
            onClose: function(){
                var date2 = $('#errd').datepicker('getDate');
                date2.setDate(date2.getDate()+30)
                $('#dropdate').datepicker('setDate',date2);
            }
            });
        $('#dropdate').datepicker();

        $('#regd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0
       });
       $('#abad').datepicker({
        changeMonth: true,
        changeYear: true,
        showAnim: 'slideDown',
        maxDate: 0
        });
        $('#withd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0
        });
        $('#mfed').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0
       });

        $('#rtod').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',

        });
        $('#herds').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',

        });
        $('#adad').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',

        });
        $('#opon').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',


        });
        $('#oponr').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            onClose: function(){
                var date2 = $('#oponr').datepicker('getDate');
                date2.setDate(date2.getDate()+60)
                $('#dfcstate').datepicker('setDate',date2);
            }

        });

        $('#dfcstate').datepicker();
        $('#countsd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',

        });
        $('#evdds').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',

        });

        $('#remd1').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#remd2').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#remd3').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });

        $('#evdd1s').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#evdd2s').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#evdd3s').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });

        $('#herd1').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#herd2').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#herd3').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });

        $('#evdd1').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#evdd2').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#evdd3').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
    });
</script>

