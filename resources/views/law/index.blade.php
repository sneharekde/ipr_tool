@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
   <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Law Firms List | <a href="{{action('LitigationMasterController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active">Law Firms List</li>
                        </ol>
                        <a  href="{{route('law_firm.create')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add</a>
                    </div>
                </div>
            </div>



            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-rounded" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ $message }}
                </div>
            @endif
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-40">
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Law Firm Name</th>
                                        <th>Contact Person</th>
                                        <th>Contact No</th>
                                        <th>Email Id</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($litis as $liti)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $liti->name }}</td>
                                        <td>{{ $liti->cp }}</td>
                                        <td>{{ $liti->cn }}</td>
                                        <td>{{ $liti->email }}</td>
                                        <td>
                                        <a href="{{ route('law_firm.edit',$liti->id) }}" class="btn btn-danger">Edit</a>
                                        {{-- <a href="{{ route('law_firm.destroy',$liti->id) }}" class="btn btn-warning">Delete</a> --}}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
  @include('include.footer')
