<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title>Login</title>
    <link href="{{asset('css/login-register-lock.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.min.css')}}" rel="stylesheet">
</head>
<body class="skin-default card-no-border">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Software Development</p>
        </div>
    </div>
    <section id="wrapper">
        <center></center>
        <div class="login-register">
            <div class="login-box card">
                <img src="{{url('img/tv9_logo.png')}}" alt="" class="img-fluid" width="50%" style="margin-left: 100px;marker-top:10px;margin-top: 51px;">
                <div class="card-body" style="padding: 1.55rem;">
                    <form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('/login/submit') }}">
                        @csrf
                        <input type="hidden" name="status" id="" value="0">
                        <h3 class="text-center m-b-20">Login</h3>
                        <div class="form-group has-feedback {{ $errors->has('message') ? 'has-error' : '' }}">
                            <input type="email" class="form-control" id="email" placeholder="Email" name="email" required>
                            <span class="text-danger">{{ $errors->first('message') }}</span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="form-group text-center">
                            <div class="col-xs-12 p-b-20">
                                <button class="btn btn-block btn-lg btn-info btn-rounded" type="submit">Log In</button>
                            </div>
                        </div>
                    </form>
                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>

                        <a href="{{ url('lead_gen') }}">Lead Reporting</a>
                    @endif <br><br>
					{{--
                    <div class="from-group">
                        <h6 align="center">Design and Created By</h6>
                        <center><img src="{{url('images/bd.jpg')}}" alt="" width="300px"></center>
                    </div>
                    --}}
                </div>
            </div>
        </div>
    </section>
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        // ==============================================================
        // Login and Recover Password
        // ==============================================================
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });
    </script>
</body>
</html>
