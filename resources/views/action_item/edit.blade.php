@include('include.header')
   <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
              <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">License Edit | <a href="{{action('FrontEndController@tradmark_portfolio')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">License Edit</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->

                @if ($message = Session::get('success'))
      <div class="alert alert-success alert-rounded" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{ $message }}
</div>

@endif

              
               <div class="card">
                  <div class="card-body" id="app">
              <form action="{{url('license/update')}}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                     @csrf
                     <input type="hidden" name="id" value="{{$item->id}}">
            <div class="row justify-content-center align-items-center">
              <div class="col-md-10">


                  <div class="from-group {{ $errors->has('license') ? 'has-error' : '' }}">
                    <label for="">Licensee</label>
                    <input type="text" name="license"  class="form-control" value="{{$item->license}}">
                    <span class="text-danger">{{ $errors->first('license') }}</span>
                  </div><br>

                  <div class="from-group {{ $errors->has('license_of_sub') ? 'has-error' : '' }}">
                    <label for="">License Date</label>
                    <input type="date" name="license_of_sub" class="form-control" value="{{$item->license_of_sub}}">
                    <span class="text-danger">{{ $errors->first('license_of_sub') }}</span>
                  </div><br>

                  <div class="from-group {{ $errors->has('term_of_license') ? 'has-error' : '' }}">
                    <label for="">Term of License</label>
                    <input type="text" name="term_of_license"  class="form-control" value="{{$item->term_of_license}}">
                    <span class="text-danger">{{ $errors->first('term_of_license') }}</span>
                  </div><br>

                  <div class="from-group {{ $errors->has('expiry_date') ? 'has-error' : '' }}">
                    <label for="">Expiry date</label>
                    <input type="date" name="expiry_date"  class="form-control" value="{{$item->expiry_date}}">
                    <span class="text-danger">{{ $errors->first('expiry_date') }}</span>
                  </div><br>

                  <div class="from-group {{ $errors->has('license_document') ? 'has-error' : '' }}">
                    <label for="">Document</label><br>
                    <label for="">File Name : {{$item->license_document}}</label>
                    <input type="file" name="license_document" class="form-control" value="{{$item->license_document}}">
                    <span class="text-danger">{{ $errors->first('license_document') }}</span>
                  </div><br>

                  <div class="from-group {{ $errors->has('reminder_two') ? 'has-error' : '' }}">
                    <label for="">Reminder(2 mnt bfr exp date)</label>
                    <input type="date" name="reminder_two"  class="form-control" value="{{$item->reminder_two}}">
                    <span class="text-danger">{{ $errors->first('reminder_two') }}</span>
                  </div><br>
            
                    <div class="col-md-12 text-center"><button type="submit" class="btn btn-success">Submit</button></div>

                   </div>
                 </div>
                  </form>
              

               
          </div>
        </div>

      </div>

    </div>
  </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>

  @include('include.footer')