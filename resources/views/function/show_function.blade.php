@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
   <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor"> Function List  | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                            <a  href="{{action('AddTradmarkController@FunctionCreate')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i>  Add Function  </a>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->

                   @if ($message = Session::get('success'))
      <div class="alert alert-success alert-rounded" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{ $message }}
</div>
@endif

                    <!-- Column -->
                   <div class="col-md-12">
                     <div class="card">
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="" class="table table-bordered table-striped">
                          <thead>
                                <tr>
                                  <th>Sr No</th>
                                  <th>Funcation Name</th>
                                  <th>Edit</th>
                                </tr>
                          </thead>
                              <tbody>
                              @foreach($functions as $function)
                              <tr>
                                <td>{{$no++}}</td>
                                <td>{{$function->function}}</td>
                                <td><a href="{{url('edit/'.$function->id.'/function')}}" class="btn btn-warning">Edit</a></td>
                              </tr>
                              @endforeach
                              </tbody>
                                    </table>
                                </div>





                </div>





            </div>
                   </div>
        </div>
  @include('include.footer')
