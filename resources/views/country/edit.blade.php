@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Country | <a href="{{ url('country') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('country.create') }}">Home</a></li>
                            <li class="breadcrumb-item active">Country</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-12">
            		<div class="card">
            			<div class="card-body">
                            {!! Form::model($country,['route' => ['country.update',$country->id],'method' => 'PUT','data-parsley-validate' => '']) !!}
                                <div class="form-group">
                                    {!! Form::label('name', 'Country') !!}
                                    {!! Form::text('name', null, ['class' => 'form-control','required' => '']) !!}
                                </div>
                                <div class="form-group text-lg-center">
                                    {!! Form::submit('submit', ['class' => 'btn btn-success']) !!}
                                </div>
                            {!! Form::close() !!}
            			</div>
            		</div>
            	</div>
            </div>
		</div>
	</div>
@include('include.footer')
