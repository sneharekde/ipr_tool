@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
   <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
              <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Invoice Add | <a href="{{action('AddBrandController@EnforceShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Invoice</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->

                @if ($message = Session::get('success'))
      <div class="alert alert-success alert-rounded" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{ $message }}
</div>

@endif


               <div class="card">
                  <div class="card-body" id="app">
              <form action="{{url('expensen/submit')}}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                     @csrf
            <div class="row justify-content-center align-items-center">
              <div class="col-md-10">

                  <div class="form-group">
                    <h4 align="center">Enforcement Number: {{$show->id}}</h4>
                    <input type="hidden" name="enforce_id" value="{{$show->id}}">
                  </div>


                  <div class="form-group {{ $errors->has('invoic') ? 'has-error' : '' }}">
                    <label for="">Invoice No</label>
                    <input type="text" name="invoic" class="form-control">
                    <span class="text-danger">{{ $errors->first('invoic') }}</span>
                  </div>

                  <div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
                    <label for="">Amount</label>
                    <input type="text" name="amount"  class="form-control">
                    <span class="text-danger">{{ $errors->first('amount') }}</span>
                  </div>

                  <div class="form-group {{ $errors->has('inv_doc') ? 'has-error' : '' }}">
                    <label for="">Document</label>
                    <input type="file" name="inv_doc[]" class="form-control">
                    <span class="text-danger">{{ $errors->first('inv_doc') }}</span>
                  </div>
                  <div class="form-group">
                    <label>Payment Status</label>
                    <select class="form-control" name="pay" id="mode">
                      <option value="">--Select--</option>
                      <option value="Paid">Paid</option>
                      <option value="Unpaid">Unpaid</option>
                    </select>
                  </div>
                  <div id="dis">
                    <div class="form-group">
                      <label>Payment Date</label>
                      <input type="date" name="pay_date" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Remark</label>
                      <input type="" name="remark" class="form-control">
                    </div>
                  </div>
                    <div class="col-md-12 text-center"><button type="submit" class="btn btn-success">Add</button></div>

                   </div>
                 </div>
                  </form>



          </div>
        </div>

        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <table class="table">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Invoice No</th>
      <th scope="col">Amount</th>
      <th scope="col">Payment Statuse</th>
      <th scope="col">Document</th>
      <th scope="col">Edit</th>
    </tr>
  </thead>
  <tbody>
    @foreach($expens as $item)
    <tr>
      <th scope="row">{{$no++}}</th>
      <td>{{$item->invoice_no}}</td>
      <td>{{$item->amount}}</td>
      <td>{{$item->pay_stat}}</td>
      <td>
        @foreach(explode(',', $item->upload) as $file)
        <a href="{{ url('public/document',$file) }}" download="">{{ $file }}</a>

        @endforeach
      </td>
      <td><a href="{{url('expensen/'.$item->id.'/edit')}}" class="btn btn-primary">Edit</a></td>
    </tr>
    @endforeach
  </tbody>
</table>
              </div>
            </div>
          </div>
        </div>
      </div>

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>

<script type="text/javascript">
  $(function() {
    $('#dis').hide();
    $('#mode').change(function(){
      if ($('#mode').val() == 'Paid') {
        $('#dis').show();
      }else{
        $('#dis').hide();
      }
    });
  });
</script>

  @include('include.footer')
