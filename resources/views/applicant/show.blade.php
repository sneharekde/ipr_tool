@include('include.header')
   <style>
    table{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
   </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Applicant | <a href="{{ url('trad_master') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active">Applicant</li>
                        </ol>
                        <a  href="{{action('AddTradmarkController@ApplicantCreate')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add</a>
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-rounded" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ $message }}
                </div>
            @endif
            <div class="cards">
                <div class="cards-body">
                    <div class="table-responsive">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Entity</th>
                                    <th>Applicant Name</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Mobile No</th>
                                    <th>E-mail</th>
                                    <th>Trading As</th>
                                    <th>Address for Service</th>
                                    <th>Service State</th>
                                    <th>Service Country</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($applicants as $app)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$app->entity}}</td>
                                        <td>{{$app->address_for_service}}</td>
                                        <td>{{$app->trading_as}}</td>
                                        <td>{{$app->country}}</td>
                                        <td>{{$app->state}}</td>
                                        <td>{{$app->mobile_number}}</td>
                                        <td>{{$app->applicant_name}}</td>
                                        <td>{{$app->address}}</td>
                                        <td>{{$app->s_state}}</td>
                                        <td>{{$app->s_country}}</td>
                                        <td class="edit"><a>
                                            <a href="{{url('edit/'.$app->id.'/applicant')}}" type="button" name="submit" value="submit" class="btn btn-primary btn-edit">&nbsp;Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
