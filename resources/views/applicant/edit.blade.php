@include('include.header')
    <style>
        .cards{
            box-shadow: 0 4px 8px 0 rgba(0, 0,0,0.2);
            background: white;
            margin-bottom: 30px;
        }
        .cards-body{
            padding: 20px
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Applicant | <a href="{{action('AddTradmarkController@ApplicantShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active">Applicant</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="cards">
                <div class="cards-body">
                    <form action="{{url('/applicant/update')}}" data-parsley-validate="" method="POST" accept-charset="utf-8">
                        @csrf
                        <input type="hidden" name="id" value="{{$applicant->id}}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box-body">
                                    <div class="form-group {{ $errors->has('entity') ? 'has-error' : '' }}">
                                        <label>Entity * :</label>
                                        <select name="entity" id="type" class="form-control" value="{{ old('entity') }}" required="">
                                            <option value="0">--Select--</option>
                                            @foreach($parents_ides as $ides)
                                                <option value="{{$ides->parent_entity}}" @if($applicant->entity == $ides->parent_entity) selected="selected" @endif>{{$ides->parent_entity}}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('entity') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('address_for_service') ? 'has-error' : '' }}">
                                        <label for="">Address For Service * :</label>
                                        <input type="text" class="form-control" name="address_for_service" value="{{ $applicant->address_for_service }}" required="">
                                        <span class="text-danger">{{ $errors->first('address_for_service') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('trading_as') ? 'has-error' : '' }}">
                                        <label for="">Trading as :</label>
                                        <input type="text" class="form-control" name="trading_as" value="{{ $applicant->trading_as }}">
                                        <span class="text-danger">{{ $errors->first('trading_as') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
                                        <label for="">Country :</label>
                                        <input type="text" class="form-control" name="country" value="{{ $applicant->country }}" required="">
                                        <span class="text-danger">{{ $errors->first('country') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
                                        <label for="">State :</label>
                                        <input type="text" class="form-control" name="state" value="{{ $applicant->state }}" required="">
                                        <span class="text-danger">{{ $errors->first('state') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('mobile_number') ? 'has-error' : '' }}">
                                        <label for="">Mobile Number :</label>
                                        <input type="text" class="form-control" name="mobile_number" value="{{ $applicant->mobile_number }}" required="">
                                        <span class="text-danger">{{ $errors->first('mobile_number') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box-body">
                                    <div class="form-group {{ $errors->has('applicant_name') ? 'has-error' : '' }}">
                                        <label for="">Applicant Name :</label>
                                        <input type="text" class="form-control" name="applicant_name" value="{{ $applicant->applicant_name }}" required="">
                                        <span class="text-danger">{{ $errors->first('applicant_name') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                                        <label for="">Address</label>
                                        <textarea name="address" id="" cols="30" rows="6" class="form-control">{{$applicant->address}}</textarea>
                                        <span class="text-danger">{{ $errors->first('address') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('s_state') ? 'has-error' : '' }}">
                                        <label for="">State :</label>
                                        <input type="text" class="form-control" name="s_state" value="{{ $applicant->s_state}}">
                                        <span class="text-danger">{{ $errors->first('s_state') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('s_country') ? 'has-error' : '' }}">
                                        <label for="">Country :</label>
                                        <input type="text" class="form-control" name="s_country" value="{{ $applicant->s_country }}" required="">
                                        <span class="text-danger">{{ $errors->first('s_country') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('e_mail') ? 'has-error' : '' }}">
                                        <label for="">E-Mail :</label>
                                        <input type="text" class="form-control" name="e_mail" value="{{ $applicant->e_mail }}" required="">
                                        <span class="text-danger">{{ $errors->first('e_mail') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
