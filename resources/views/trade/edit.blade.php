@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0,0,0.2);
            background: white;
        }

    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Class | <a href="{{route('trade_class.index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active">Class</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    @include('include.message')
                    {!! Form::model($com,['route' => ['trade_class.update',$com->id], 'data-parsley-validate' => '' , 'method' => 'PUT']) !!}
                                <div class="form-group">
                                    {{ Form::label('name','Class Name:') }}
                                    {{ Form::text('name',null,['class' => 'form-control','required' => '']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('type','Class Type:') }}
                                    {{ Form::select('type',['Class of Goods' =>'Class of Goods','Class of Services' =>'Class of Services'],null,['class' => 'form-control','required' => '','placeholder' => 'Select']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('desc','Descriptions:') }}
                                    {{ Form::textarea('desc',null,['class' => 'form-control','required' => '']) }}
                                </div>
                                <div class="text-center">
                                    {{ Form::submit('Submit',['class' => 'btn btn-success']) }}
                                </div>
                           </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
  @include('include.footer')
