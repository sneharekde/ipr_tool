@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Trademark | <a href="{{ url('tradmark/'.$post->id.'/show') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    {!! Form::open(['url' => ['trade/save/agent-fee',$post->id],'method' => 'POST','data-parsley-validate' => '','files' => true]) !!}
                        <div class="row">
                            <div class="col-md-4 form-group">
                                {!! Form::label('date', 'Date') !!}
                                {!! Form::text('date', null, ['class' => 'form-control','required' => '','id' => 'dated']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('fee', 'Fee') !!}
                                {!! Form::text('fee', null, ['class' => 'form-control','required' => '','data-parsley-type' => 'number']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('invoice', 'Upload Invoice') !!}
                                {!! Form::file('invoice', ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-md-12 text-center">
                                <h4>Miscellaneous charges</h4><hr>
                            </div>
                            <div class="col-md-6 form-group">
                                {!! Form::label('not_fee', 'Notarisation Charges') !!}
                                {!! Form::text('not_fee', null, ['class' => 'form-control','data-parsley-type' => 'number']) !!}
                            </div>
                            <div class="col-md-6 form-group">
                                {!! Form::label('stamp', 'Stamp Paper') !!}
                                {!! Form::file('stamp', ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-md-12 form-group text-center">
                                {!! Form::submit('submit', ['class' => 'btn btn-success']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@include('include.footer')
    <script>
        $(function(){
            $('#dated').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });
        });
    </script>
