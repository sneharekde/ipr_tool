@include('include.header')
   <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
              <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Events Calendar | <a href="{{action('AddTradmarkController@ApplicantShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                                <li class="breadcrumb-item active">Events Calendar</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->

                <div class="row">
                  <div class="col-md-12">
                    <div id='calendar'></div>
                  </div>
                </div>

                <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
       <div class="modal-dialog" role="document">
                    <form action="" method="get" accept-charset="utf-8">
        <div class="modal-content">

            <div class="modal-body">
                <h4>Edit Events</h4>

                Start time:
                <br />
                <input type="text" class="form-control" name="start_time" id="start_time">

                End time:
                <br />
                <input type="text" class="form-control" name="finish_time" id="finish_time">
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Update</button>
            </div>               
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" id="editModal" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="modal-body">
                <h4>Edit Events</h4>

                Start time:
                <br />
                <input type="text" class="form-control" name="start_time" id="start_time">

                End time:
                <br />
                <input type="text" class="form-control" name="finish_time" id="finish_time">
            </div>
      </div>
      <div class="modal-footer">
         <input type="button" class="btn btn-primary" id="appointment_update" value="Save">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
        
    

                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>



<script>
    $(document).ready(function() {
        // page is now ready, initialize the calendar...
        $('#calendar').fullCalendar({
            // put your options and callbacks here
            events : [
                @foreach($events as $event)
                {

                    title : '{{ $event->tradmark_name . ' ' . $event->status }}',
                    start : '{{ $event->reminder_one }}',
                    color  : 'gray',
                    @if ($event->action_deadline)
                            end: '{{ $event->action_deadline }}',
                    @endif
                },
                @endforeach
            ],
            
             eventClick: function(calEvent, jsEvent, view) {
            $('#start_time').val(moment(calEvent.start).format('YYYY-MM-DD HH:mm:ss'));
            $('#finish_time').val(moment(calEvent.end).format('YYYY-MM-DD HH:mm:ss'));
            $('#editModal').modal();
        }
        
        });


    $('#appointment_update').click(function(e) {
        e.preventDefault();
        var data = {
            _token: '{{ csrf_token() }}',
            start_time: $('#start_time').val(),
            finish_time: $('#finish_time').val(),
        };

        $.post('{{ route('events.update') }}', data, function( result ) {
            $('#calendar').fullCalendar('removeEvents', $('#event_id').val());

            $('#calendar').fullCalendar('renderEvent', {
                title: result.appointment.client.first_name + ' ' + result.appointment.client.last_name,
                start: result.appointment.start_time,
                end: result.appointment.finish_time
            }, true);

            $('#editModal').modal('hide');
        });
    });

    });


    

</script>

  @include('include.footer')