@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">FIR Add | <a href="{{action('AddBrandController@EnforceShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">FIR</li>
                        </ol>
                    </div>
                </div>
            </div>
            @include('include.message')

            <div class="card">
                <div class="card-body" id="app">
                    {!! Form::open(['url' => 'fir/submit','method' => 'POST','data-parsley-validate' => '']) !!}
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="hidden" name="fir_id" value="{{ $show->id }}">
                                    {!! Form::label('fir_date', 'FIR Date* :') !!}
                                    {!! Form::text('fir_date', null, ['class' => 'form-control','id' => 'reservationtime','required' => '']) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('fir_no', 'FIR Number* :') !!}
                                    {!! Form::text('fir_no', null, ['class' => 'form-control','required' => '']) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('police', 'Police Station* :') !!}
                                    {!! Form::text('police', null, ['class' => 'form-control','required' => '']) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('nof', 'Name Of Legislation* :') !!}
                                    {!! Form::text('nof', null, ['class' => 'form-control','required' => '']) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    {!! Form::submit('submit', ['class' => 'btn btn-success']) !!}
                                </div>
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>

        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <table class="table">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">FIR Date</th>
      <th scope="col">FIR </th>
      <th scope="col">Police Station</th>
      <th scope="col">Name OF Legislation</th>
      <th scope="col">Date Of Chargesheet</th>
      <th scope="col">Edit</th>
    </tr>
  </thead>
  <tbody>
    @foreach($expens as $item)
    <tr>
      <td>{{ $no++ }}</td>
      <td>{{ date('d-m-Y',strtotime($item->fir_date)) }}</td>
      <td>{{$item->fir_no}}</td>
      <td>{{$item->police}}</td>
      <td>{{$item->nof}}</td>
      <td>{{ date('d-m-Y',strtotime($item->dof)) }}</td>
      <td><a href="{{url('fir/'.$item->id.'/edit')}}" class="btn btn-primary btn-sm">Edit</a></td>
    </tr>
    @endforeach
  </tbody>
</table>
              </div>
            </div>
          </div>
        </div>
      </div>

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>


<script>
  $(function () {

    //Date picker
    $('#datepicker').datepicker({
      format: 'dd/mm/yy',
      autoclose: true,
      todayHighlight: true
    }).datepicker("setDate", new Date());

    $('#Todatepicker').datepicker({
      format: 'dd/mm/yy',
      autoclose: true,
      todayHighlight: true
    }).datepicker("setDate", new Date());

    $('#reservationtime').datepicker({
      format: 'dd/mm/yy',
      autoclose: true,
      todayHighlight: true
    }).datepicker("setDate", new Date());

    $('#priority_since').datepicker({
      format: 'dd/mm/yy',
      autoclose: true,
      todayHighlight: true
    }).datepicker("setDate", new Date());

  });


</script>
<script>
  $(function () {

    //Date picker
    $('#datepicker').datepicker({
      format: 'dd/mm/yy',
      autoclose: true,
      todayHighlight: true
    }).datepicker("setDate", new Date());

    $('#Todatepicker').datepicker({
      format: 'dd/mm/yy',
      autoclose: true,
      todayHighlight: true
    }).datepicker("setDate", new Date());

    $('.reservationtime').datepicker({
      format: 'dd/mm/yy',
      autoclose: true,
      todayHighlight: true
    }).datepicker("setDate", new Date());

    $('#priority_since').datepicker({
      format: 'dd/mm/yy',
      autoclose: true,
      todayHighlight: true
    }).datepicker("setDate", new Date());

  });


</script>

  @include('include.footer')
