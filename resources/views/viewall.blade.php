@include('include.header')

<div class="page-wrapper">
	<div class="container-fluid">
		<div class="row page-titles">
	        <div class="col-md-5 align-self-center">
	            <h4 class="text-themecolor">View All Brand Protection | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4> 
	        </div>
	        <div class="col-md-7 align-self-center text-right">
	            <div class="d-flex justify-content-end align-items-center">
	                <ol class="breadcrumb">
	                    <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
	                    <li class="breadcrumb-item active">Dashboard</li>
	                </ol>
	            </div>
	        </div>
	    </div>
	    <div class="col-md-12">
	    	<div class="card">
	    		<div class="card-body">
	    			<a href="{{action('AddBrandController@InvestShow')}}" class="btn btn-success">Investigation</a>
	    			<a href="{{action('AddBrandController@EnforceShow')}}" class="btn btn-info">Enforcement</a>
	    			<a href="{{action('AddBrandController@Analytics')}}" class="btn btn-primary">Analytics</a>
	    		</div>
	    	</div>
	    </div>
	</div>
</div>

@include('include.footer')