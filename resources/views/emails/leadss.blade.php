    <style>
        .padd{
        padding: 20px;
        }
        #customers {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }
          #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
          }
          #customers tr:nth-child(even){background-color: #f2f2f2;}
          #customers tr:hover {background-color: #ddd;}
          #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
          }
    </style>

    <div class="padd">

    <h2>Dear {{ $user->first_name }} received Lead Information from {{ $data['leadby'] }}</h2>

    <table id="customers">
        <thead>
            <tr>
                <th>Lead By</th>
                <th>Location</th>
                <th>Product Name</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $data['leadby'] }}</td>
                <td>{{ $data['location'] }}</td>
                <td>{{ $data['product'] }}</td>
            </tr>
        </tbody>
    </table>

</div>
