
    <style>
        .padd{
        padding: 20px;
        }
        #customers {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }
        #customers tr:nth-child(even){background-color: #f2f2f2;}
        #customers tr:hover {background-color: #ddd;}
        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
        #resp{
            overflow-x: auto;
        }
    </style>


    <div class="padd">
        <h3>Dear {{ $user->first_name }}</h3>
        <p>This is first Reminder Mail.kindly follow up according to this mail</p>

        <div id="resp">
            <table id="customers">
                <thead>
                    <tr>
                        <th>FIR Date</th>
                        <th>FIR No</th>
                        <th>Police Station</th>
                        <th>Due Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($reminders as $reminder)
                    <tr>
                        <td>{{ date('d-m-Y',strtotime($reminder['fir_date'])) }}</td>
                        <td>{{  $reminder['fir_no']  }}</td>
                        <td>{{ $reminder['police'] }}</td>
                        <td>{{ date('d-m-Y',strtotime($reminder['dof'])) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
