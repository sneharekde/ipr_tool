<style>
    .padd{
    padding: 20px;
    }
    #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }
    #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }
    #customers tr:nth-child(even){background-color: #f2f2f2;}
    #customers tr:hover {background-color: #ddd;}
    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }
    #resp{
        overflow-x: auto;
    }
</style>


<div class="padd">
    <h3>Dear {{ $user->first_name }}</h3>
    <p>This is Third Reminder Mail.kindly follow up according to this mail</p>

    <div id="resp">
        <table id="customers">
            <thead>
                <tr>
                    <th>Article Name</th>
                    <th>Application No</th>
                    <th>Status</th>
                    <th>Sub Status</th>
                    <th>Due Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($reminders as $reminder)
                <tr>
                    <td>{{ $reminder['design']['article_name'] }}</td>
                    <td>{{ $reminder['design']['app_no'] }}</td>
                    <td>{{ $reminder['design']['status'] }}</td>
                    <td>{{ $reminder['design']['sub_status'] }}</td>
                    <td>@if($reminder['rep_dead'] !='') {{ date('d-m-Y',strtotime($reminder['rep_dead'])) }} @elseif($reminder['dfh'] != '') {{ date('d-m-Y',strtotime($reminder['dfh'])) }} @elseif($reminder['d_renr'] != '') {{ date('d-m-Y',strtotime($reminder['d_renr'])) }} @endif</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
