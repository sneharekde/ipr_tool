

<style>
    .padd{
    padding: 20px;
    }
    #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }
    #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }
    #customers tr:nth-child(even){background-color: #f2f2f2;}
    #customers tr:hover {background-color: #ddd;}
    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }
    #resp{
        overflow-x: auto;
    }
</style>


<div class="padd">
    <h3>Dear {{ $user->first_name }}</h3>
    <p>This is third Reminder Mail.kindly follow up according to this mail</p>

    <div id="resp">
        <table id="customers">
            <thead>
                <tr>
                    <th>Trademark Name</th>
                    <th>Application No</th>
                    <th>Status</th>
                    <th>Sub Status</th>
                    <th>Due Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($reminders as $reminder)
                <tr>
                    <td>{{ $reminder['trade']['trademark'] }}</td>
                    <td>{{ $reminder['trade']['application_no'] }}</td>
                    <td>{{ $reminder['trade']['status'] }}</td>
                    <td>{{ $reminder['trade']['sub_status'] }}</td>
                    <td>@if($reminder['rep_dead'] !='') {{ date('d-m-Y',strtotime($reminder['rep_dead'])) }} @elseif($reminder['her_date'] != '') {{ date('d-m-Y',strtotime($reminder['her_date'])) }} @elseif($reminder['evi_date'] != '') {{ date('d-m-Y',strtotime($reminder['evi_date'])) }} @elseif($reminder['ren_date'] != '') {{ date('d-m-Y',strtotime($reminder['ren_date'])) }} @elseif($reminder['cou_dead'] != '') {{ date('d-m-Y',strtotime($reminder['cou_dead'])) }} @endif</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
