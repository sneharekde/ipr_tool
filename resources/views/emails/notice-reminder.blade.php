
<style>
    .padd{
    padding: 20px;
    }
    #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }
    #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }
    #customers tr:nth-child(even){background-color: #f2f2f2;}
    #customers tr:hover {background-color: #ddd;}
    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }
    #resp{
        overflow-x: auto;
    }
</style>


<div class="padd">
    <h3>Dear {{ $user->first_name }}</h3>
    <p>This is Notice Reply Reminder Mail.kindly follow up according to this mail</p>

    <div id="resp">
        <table id="customers">
            <thead>
                <tr>
                    <th>Notice Date</th>
                    <th>Sent / Received Date</th>
                    <th>Notice Reference No</th>
                    <th>Notice Deadline</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($reminders as $reminder)
                <tr>
                    <td>{{ date('d-m-Y',strtotime($reminder['notice_date'])) }}</td>
                    <td>{{ date('d-m-Y',strtotime($reminder['sent_rec'])) }}</td>
                    <td>{{ $reminder['not_ref'] }}</td>
                    <td>{{ date('d-m-Y',strtotime($reminder['not_reply'])) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
