@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>

  <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
              <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Edit | <a href="{{route('notices.index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Edit</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
                <style type="text/css">
                  .rate {
                    background-color: #ecf0f1;
                    border: 1px solid #000;
                    border-radius: 50px;
                    font-size: 18px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    height: 30px;
                    padding: 0 10px;
                    margin: 30px 15px 30px 25px;
                    }
                </style>
                @include('include.message')
  <div class="card">
    <div class="card-body">
        {!! Form::model($notice,['route' => ['notices.update',$notice->id],'method' => 'PUT','data-parsley-validate' => '','files' => true]) !!}
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('entity','Entity:') }}
            {{ Form::select('entity',$entity,null,['class' => 'form-control','placeholder' => 'Select Entity','required' => '']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('Location','Location:') }}
            {{ Form::select('Location',$uni,null,['class' => 'form-control','placeholder' => 'Select Location','required' => '']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('department','Department:') }}
            {{ Form::select('department',$func,null,['class' => 'form-control','placeholder' => 'Select Department','required' => '']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('by_ag','By / Against Company:') }}
            <select name="by_ag" class="form-control" required="">
              <option value="">Select</option>
              <option value="By Company" @if($notice->by_ag == 'By Company') Selected @endif>By Company</option>
              <option value="Against Company" @if($notice->by_ag == 'Against Company') Selected @endif>Against Company</option>
            </select>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('opp_part','Opposite Party:') }}
            {{ Form::text('opp_part',null,['class' => 'form-control']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('category','Category:') }}
            {{ Form::select('category',$cat,null,['class' => 'form-control','placeholder' => 'select category','required' => '']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('not_ref','Notice / Ref No.:') }}
            {{ Form::text('not_ref',null,['class' => 'form-control']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('addr_to','Address To:') }}
            {{ Form::text('addr_to',null,['class' => 'form-control']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('user_id','Notice Assigned To:') }}
            {{ Form::select('user_id',$use,null,['class' => 'form-control','placeholder' => 'Select','required' => '']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('int_per','Internal Contact Person:') }}
            {{ Form::text('int_per',null,['class' => 'form-control','required' => '']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('notice_date','Notice Date:') }}
            {{ Form::text('notice_date',null,['class' => 'form-control','id' => 'nod']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('sent_rec','Sent / Recieved On:') }}
            {{ Form::text('sent_rec',null,['class' => 'form-control','id' => 'srd']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('not_reply','Notice Reply Deadline:') }}
            {{ Form::text('not_reply',null,['class' => 'form-control','id' => 'nrpd']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('not_rem','Notice Reminder Date:') }}
            {{ Form::text('not_rem',null,['class' => 'form-control','id' => 'remd']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('ext_counsel','External Counsel:') }}
            {{ Form::select('ext_counsel',$ext,null,['class' => 'form-control','placeholder' => 'Select']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('opp_part_adv','Opposite Party Advocate:') }}
            {{ Form::text('opp_part_adv',null,['class' => 'form-control']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('rel_law','Relevent Law:') }}
            {{ Form::text('rel_law',null,['class' => 'form-control']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('comments','Brief Facts/Comments:') }}
            {{ Form::text('comments',null,['class' => 'form-control']) }}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            {{ Form::label('docs','Legal Notice Document:') }}
            @if ($notice->docs != '')
                <div class="col-md-3">
                    @foreach (explode(',',$notice->docs) as $item)
                    <a href="{{ url('public/litigation/notices/'.$item) }}" download="">{{ $item }}</a>
                    <a href="{{ url('notice/delete/'.$item.'/'.$notice->id) }}" class="btn btn-sm btn-danger">Delete</a>
                @endforeach
                </div>
            @else
            {{ Form::file('docs[]',['class' => 'form-control','multiple' => true]) }}
            @endif

          </div>
          <div class="col-md-12">
            <hr><h4 class="text-center">Amount Details</h4><hr>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
            {{ Form::label('date','Date:') }}
            {{ Form::text('date',null,['class' => 'form-control','id' => 'dated']) }}
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
            {{ Form::label('amount_involved','Amount Involved:') }}
            {{ Form::text('amount_involved',null,['class' => 'form-control','id' => 'from_ammount']) }}
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
            {{ Form::label('currency','Currency:') }}
            {{ Form::select('currency',$cur,null,['class' => 'form-control','id' => 'from_currency','placeholder' => 'select']) }}
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
            {{-- <button id="exchange">
              <i class="fas fa-exchange-alt"></i>
            </button> --}}
            <div class="rate" id="rate"></div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
            {{ Form::label('conv_curr','Converted Currency:') }}
            {{ Form::select('conv_curr',$cur,null,['class' => 'form-control','id' => 'to_currency','placeholder' => 'select']) }}
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
            {{ Form::label('conv_amt','Converted Amount:') }}
            {{ Form::text('conv_amt',null,['class' => 'form-control','id' => 'to_ammount','readonly' =>'']) }}
          </div>

          <div class="col-md-12">
          <div class="form-group text-center">
            <button type="submit" name="submit" class="btn btn-success">Create</button>
          </div>
        </div>
      </div>
      {!! Form::close() !!}
    </form>
  </div>
</div>
</div>
</div>

    {{--  <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/bootstrap.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>  --}}
@include('include.footer')
<script>
    $(function() {
        $('#nod').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
            //minDate: +1,
        });
        $('#srd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
            //minDate: +1,
        });
        $('#nrpd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            //maxDate: 0,
            minDate: +1,
        });
        $('#remd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            //maxDate: 0,
            minDate: +1,
        });
        $('#dated').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
            //minDate: +1,
        });
    });
</script>
