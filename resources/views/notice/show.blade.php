@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>

	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Update | <a href="{{ route('notices.index') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>

            {{-- Date --}}
            @include('include.message')
            <div class="card">
            	<div class="card-body">
            		<ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Documents</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Status</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <table class="table table-striped">
                            	<tr>
                            		<th>Entity Name :</th><td>{{ $notice->entity }}</td>
                            	</tr>
                            	<tr>
                            		<th>Location :</th><td>{{ $notice->Location }}</td>
                            	</tr>
                            	<tr>
                            		<th>Department:</th><td>{{ $notice->department }}</td>
                            	</tr>
                            	<tr>
                            		<th>By/Against Company :</th><td>{{ $notice->by_ag }}</td>
                            	</tr>
                            	<tr>
                            		<th>Opposite Party :</th><td>{{ $notice->opp_part }}</td>
                            	</tr>
                            	<tr>
                            		<th>Category : </th><td>{{ $notice->category }}</td>
                            	</tr>
                            	<tr>
                            		<th>Amount involved : </th><td>{{ $notice->amount_involved }} {{ $notice->currency }}</td>
                            	</tr>
                            	<tr>
                            		<th>Notice/Ref. No: </th><td>{{ $notice->not_ref }}</td>
                            	</tr>
                            	<tr>
                            		<th>Address to :</th><td>{{ $notice->addr_to }}</td>
                            	</tr>
                            	<tr>
                            		<th>Notice assigned to :</th><td>{{ $notice->user_id }}</td>
                            	</tr>
                            	<tr>
                            		<th>nternal contact person :</th><td>{{ $notice->int_per }}</td>
                            	</tr>
                            	<tr>
                            		<th>Notice dated :</th><td>{{ date('d-m-Y',strtotime($notice->notice_date)) }}</td>
                            	</tr>
                            	<tr>
                            		<th>Sent/Received on :</th><td>{{ date('d-m-Y',strtotime($notice->sent_rec)) }}</td>
                            	</tr>
                            	<tr>
                            		<th>Notice reply deadline :</th><td>{{ date('d-m-Y',strtotime($notice->not_reply)) }}</td>
                            	</tr>
                            	<tr>
                            		<th>Notice reminder date :</th><td>{{ date('d-m-Y',strtotime($notice->not_rem)) }}</td>
                            	</tr>
                            	<tr>
                            		<th>External Counsel :</th><td>{{ $notice->ext_counsel }}</td>
                            	</tr>
                            	<tr>
                            		<th>Opposite party advocate : </th><td>{{ $notice->opp_part_adv }}</td>
                            	</tr>
                            	<tr>
                            		<th>Relevant law :</th><td>{{ $notice->rel_law }}</td>
                            	</tr>
                            	<tr>
                            		<th>Brief facts/comments:</th><td>{{ $notice->comments }}</td>
                            	</tr>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <table class="table table-bordered table-striped">
                            	<thead>
                            		<tr>
                            			<th>Document Name</th>
                            			<th>Delete</th>
                            		</tr>
                            	</thead>
                            	<tbody>
                            		@if ($notice->docs !='')
                                    @foreach(explode(',', $notice->docs) as $file)
                            			<tr>
                            				<td><a href="{{ url('public/litigation/notices',$file) }}" download="">{{ $file }}</a></td>
                            				<td><a href="{{ url('notice/delete/'.$file.'/'.$notice->id) }}" class="btn btn-xs btn-danger">Delete	</a></td>
                            			</tr>
                            		@endforeach
                                    @endif
                            	</tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="row" style="margin-top: 10px">
                            	<div class="col-md-12">

                                    <a href="{{ url('notsta/create',$notice->id) }}" class="btn btn-warning">Add Status</a>
                            	</div>
                            </div>
                            <div class="row" style="margin-top: 10px">
                            	<div class="col-md-12">
                            		<table class="table table-bordered table-striped">
	                            		<thead>
	                            			<tr>
	                            				<th>#</th>
	                            				<th>Date</th>
	                            				<th>Action Taken</th>
	                            				<th>Next Action Item</th>
	                            				<th>Next Action Due Date</th>
	                            				<th>Responsible Person</th>
	                            				<th>Reminder Date</th>
	                            				<th>Sent Status</th>
	                            				<th>Documents</th>
	                            				<th>Action</th>
	                            			</tr>
	                            		</thead>
	                            		<tbody>
	                            			@foreach($status as $stat)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ date('d-m-Y',strtotime($stat->r_date)) }}</td>
                                                    <td>{{ $stat->act_take }}</td>
                                                    <td>{{ $stat->next_act }}</td>
                                                    <td>{{ date('d-m-Y',strtotime($stat->due_date)) }}</td>
                                                    <td>{{ $stat->user->first_name }}</td>
                                                    <td>{{ date('d-m-Y',strtotime($stat->reminder)) }}</td>
                                                    <td>{{ $stat->status }}</td>
                                                    <td>
                                                        @foreach(explode(',', $stat->docs) as $file)
                                                            <a href="{{ url('public/litigation/notices',$file) }}" download="">{{ $file }}</a>
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        <a href="{{ url('notice-status/edit/'.$stat->id) }}" class="btn btn-xs btn-success">Edit</a>
                                                    </td>
                                                </tr>
                                            @endforeach
	                            		</tbody>
                            	</table>
                            	</div>
                            </div>
                        </div>
                    </div>
            	</div>
            </div>
		</div>
	</div>
@include('include.footer')
