@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Create New | <a href="{{route('notices.show',$post->id)}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Create New</li>
                        </ol>
                    </div>
                </div>
            </div>

            @include('include.message')
            <div class="card">
    			<div class="card-body">
    				{!! Form::open(['url' => 'notsta/submit','method' => 'POST','data-parsley-validate' => '','files' => true]) !!}
    				<div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12 form-group">
                            {!! Form::label('status', "Status *:") !!}
                            <select name="status" id="" class="form-control" required="">
                                <option value="">Select</option>
                                <option value="Pending" @if($post->status == 'Replied' || $post->status == 'Closed' || $post->status == 'Convert To Litigation') disabled @endif>Pending</option>
                                <option value="Replied" @if($post->status == 'Replied' || $post->status == 'Closed' || $post->status == 'Convert To Litigation') disabled @endif>Replied</option>
                                <option value="Closed" @if($post->status == 'Closed' || $post->status == 'Convert To Litigation') disabled @endif>Closed</option>
                                <option value="Convert To Litigation" @if($post->status == 'Convert To Litigation') disabled @endif>Convert To Litigation</option>
                            </select>
                        </div>
    					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                        <input type="hidden" name="stat_id" value="{{ $post->id }}">
            			{{ Form::label('r_date','Date:') }}
            			{{ Form::text('r_date',null,['class' => 'form-control','required' => '','id' => 'dated']) }}
          			</div>

          			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
            			{{ Form::label('act_take','Action Taken:*') }}
            			{{ Form::textarea('act_take',null,['class' => 'form-control','required' => '']) }}
          			</div>

          			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
            			{{ Form::label('next_act','Next Action Item:') }}
            			{{ Form::textarea('next_act',null,['class' => 'form-control','required' => '']) }}
          			</div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                  {{ Form::label('due_date','Action item due Date:*') }}
                  {{ Form::text('due_date',null,['class' => 'form-control','required' => '','id' => 'dued']) }}
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                  {{ Form::label('reminder','Reminder:') }}
                  {{ Form::text('reminder',null,['class' => 'form-control','required' => '','id' => 'remd']) }}
                </div>

          			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                  {{-- <input type="hidden" name="hear_id" value="{{ $id }}"> --}}
            			{{ Form::label('user_id','Responsible Person :') }}
            			{{ Form::select('user_id',$ar1,null,['class' => 'form-control','placeholder' => 'Select','required' => '']) }}
         			 </div>



          			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
            			{{ Form::label('docs','Document:') }}
            			{{ Form::file('docs[]',['class' => 'form-control','multiple' => true]) }}
          			</div>

          			<div class="col-md-12">
              			<div class="form-group text-center">
            				<button type="submit" name="submit" class="btn btn-success">Create</button>
          				</div>
            		</div>
    				</div>


    				{!! Form::close() !!}
    			</div>
    		</div>
		</div>
	</div>

@include('include.footer')
<script>
    $(function() {
        $('#dated').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
            //minDate: +1,
        });
        $('#remd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            //maxDate: 0,
            minDate: +1,
        });
        $('#dued').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            //maxDate: 0,
            minDate: +1,
        });
    });
</script>
