@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Legal Notices | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Legal Notices </li>
                        </ol>
                        <a  href="{{route('notices.create')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</a>
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-rounded" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ $message }}
                </div>
            @endif
            <div class="card">
                <form action="{{url('/noticess')}}" method="POST" accept-charset="utf-8">
                @csrf
                    <div class="form-body">
                        <div class="card-body">
                            <div class="row pt-3">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Entity</label>
                                        <select class="form-control" name="ent">
                                            <option value="">Select</option>
                                            @foreach($ents as $ent)
                                                <option value="{{ $ent->entity }}">{{ $ent->entity }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Location  </label>
                                        <select class="form-control" name="loc">
                                            <option value="">Select</option>
                                            @foreach($locs as $loc)
                                                <option value="{{ $loc->Location }}">{{ $loc->Location }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Department</label>
                                        <select class="form-control" name="dep">
                                            <option value="">Select</option>
                                            @foreach($deps as $dep)
                                                <option value="{{ $dep->department }}">{{ $dep->department }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>By / Against Country</label>
                                        <select class="form-control" name="by_ag">
                                            <option value="">Select</option>
                                            @foreach($bys as $by)
                                                <option value="{{ $by->by_ag }}">{{ $by->by_ag }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select class="form-control" name="cat">
                                            <option value="">Select</option>
                                            @foreach($cats as $cat)
                                                <option value="{{ $cat->category }}">{{ $cat->category }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Contact Person</label>
                                        <select class="form-control" name="cnp">
                                            <option value="">Select</option>
                                            @foreach($cnps as $cnp)
                                                <option value="{{ $cnp->int_per }}">{{ $cnp->int_per }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Legal Notice - From :</label>
                                        <div class="input-group">
                                            <input type="date" name="from" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>To Date :</label>
                                        <div class="input-group">
                                            <input type="date" name="to" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-success">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <p id="demo" align="center"></p>
                        <table id="myTable" class="table table-bordered"  data-page-length='50'>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Entity</th>
                                    <th>Location</th>
                                    <th>Department</th>
                                    <th>Category</th>
                                    <th>Notice Date</th>
                                    <th>Sent / Received Date</th>
                                    <th>Deadline Date</th>
                                    <th>Contact Person</th>
                                    <th>Reminder Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        <tbody>
                            @foreach($notices as $notice)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $notice->entity }}</td>
                                    <td>{{ $notice->Location }}</td>
                                    <td>{{ $notice->department }}</td>
                                    <td>{{ $notice->category }}</td>
                                    <td>{{ date('d-m-Y',strtotime($notice->notice_date)) }}</td>
                                    <td>{{ date('d-m-Y',strtotime($notice->sent_rec)) }}</td>
                                    <td>{{ date('d-m-Y',strtotime($notice->not_reply)) }}</td>
                                    <td>{{ $notice->int_per }}</td>
                                    <td>{{ date('d-m-Y',strtotime($notice->not_rem)) }}</td>
                                    <td>{{ $notice->status }}
                                        @if ($notice->status != 'Convert To Litigation')
                                        <a href="{{ url('convert_to/'.$notice->id) }}" class="btn btn-success btn-sm">Convert To Litigation</a>
                                        @endif

                                    </td>
                                    <td>
                                    <a href="{{ route('notices.edit',$notice->id) }}" type="edit" class="btn btn-warning">Edit</a>
                                    <a href="{{ url('notices/delete',$notice->id) }}" class="btn btn-success">Delete</a>
                                    <a href="{{ route('notices.show',$notice->id) }}" class="btn btn-info">Update</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
<script type="text/javascript">
    var x = document.getElementById("myTable").rows.length - 1;
    document.getElementById("demo").innerHTML = "Total Records are " + x;
</script>
