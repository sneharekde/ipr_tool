@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Agency | <a href="{{action('AgencyController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active">Agency</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('agency.store') }}" method="POST" accept-charset="utf-8">
                    @csrf
                        <div class="row">
                            <div class="col-md-6 form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                <label for="">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            </div>
                            <div class="col-md-6 form-group {{ $errors->has('contp') ? 'has-error' : '' }}">
                                <label for="">Contact Person</label>
                                <input type="text" class="form-control" name="contp" value="{{ old('contp') }}">
                                <span class="text-danger">{{ $errors->first('contp') }}</span>
                            </div>
                            <div class="col-md-6 form-group {{ $errors->has('contn') ? 'has-error' : '' }}">
                                <label for="">Contact Number</label>
                                <input type="text" class="form-control" name="contn" value="{{ old('contn') }}">
                                <span class="text-danger">{{ $errors->first('contn') }}</span>
                            </div>
                            <div class="col-md-6 form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label for="">Email</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            </div>
                        </div>
                        <div class="col-md-12 center">
                            <button type="Submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
