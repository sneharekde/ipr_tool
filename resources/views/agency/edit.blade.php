@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Agency | <a href="{{action('AgencyController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active">Agency</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    {!! Form::model($agency, ['route' => ['agency.update', $agency->id], 'data-parsley-validate' => '', 'method' => 'PUT']) !!}
                        <div class="row">
                            <div class="col-md-6 form-group">
                                {{ Form::label('name', 'Name:')}}
                                {{ Form::text('name', null, ['class' => 'form-control','required' => ''])}}
                            </div>
                            <div class="col-md-6 form-group">
                                {{ Form::label('person', 'Contact Person:')}}
                                {{ Form::text('person', null, ['class' => 'form-control','required' => ''])}}
                            </div>
                            <div class="col-md-6 form-group">
                                {{ Form::label('contact', 'Contact Number:')}}
                                {{ Form::text('contact', null, ['class' => 'form-control','required' => ''])}}
                            </div>
                            <div class="col-md-6 form-group">
                                {{ Form::label('email', 'Title:')}}
                                {{ Form::email('email', null, ['class' => 'form-control','required' => ''])}}
                            </div>
                            <div class="col-md-12">
                                {{ Form::submit('Save Changes', ['class' => 'btn btn-success'])}}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
