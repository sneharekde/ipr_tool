@include('include.header')
    <style type="text/css">
        @media (min-width: 768px)
        {
        .modal-xl
        {
            width: 90%;
            max-width:1200px;
        }
        }
        rect
        {
            cursor:pointer;
        }
        #chart_div text
        {
            cursor:pointer;
        }
        #chart2_div text
        {
            cursor: pointer;
        }
    </style>

    <div class="page-wrapper">
        @if((Auth::user()->role == 'Administrator') || (Auth::user()->role == 'IP Head'))
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Dashboard </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-6" >
                    <a href="{{action('FrontEndController@tradmark_portfolio')}}">
                        <div class="card" style="background: #EDE2E3">
                            <div class="card-body">
                                <div class="row p-t-10 p-b-10">
                                    <div class="col p-r-0">
                                        <h1 class="font-light">{{$total_tradmark}}</h1>
                                        <h6 class="text-muted">Trademark</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 col-md-6">
                    <a href="{{action('AddBrandController@viewall')}}">
                        <div class="card" style="background: #EDE2E3">
                            <div class="card-body">
                                <div class="row p-t-10 p-b-10">
                                    <div class="col p-r-0">
                                        <h1 class="font-light">{{ $total_brand }}</h1>
                                        <h6 class="text-muted">Brand Protection</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 col-md-6">
                    <a href="{{ action('DesignController@index') }}">
                        <div class="card" style="background: #EDE2E3">
                            <div class="card-body">
                                <div class="row p-t-10 p-b-10">
                                    <div class="col p-r-0">
                                        <h1 class="font-light">{{ $total_design }}</h1>
                                        <h6 class="text-muted">Design</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 col-md-6">
                    <a href="{{ action('CopyrightController@index') }}">
                        <div class="card" style="background: #EDE2E3">
                            <div class="card-body" >
                                <div class="row p-t-10 p-b-10">
                                    <div class="col p-r-0">
                                        <h1 class="font-light">{{ $total_copyright }}</h1>
                                        <h6 class="text-muted">Copyright</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 col-md-6">
                    <a href="{{ action('PatentController@index') }}">
                        <div class="card" style="background: #EDE2E3">
                            <div class="card-body">
                                <div class="row p-t-10 p-b-10">
                                    <div class="col p-r-0">
                                        <h1 class="font-light">{{ $total_patent }}</h1>
                                        <h6 class="text-muted">Patent</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                {{-- Space --}}
                <div class="col-lg-2 col-md-2">
                    <a href="{{ route('summary.index') }}">
                        <div class="card" style="background: #EDE2E3">
                            <div class="card-body">
                                <div class="row p-t-10 p-b-10">
                                    <div class="col p-r-0">
                                        <h1 class="font-light">{{ $total_liti }}</h1>
                                        <h6 class="text-muted">Litigation</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            @include('include.chartdashboard')
                            {{--  <div class="row">
                                <div class="col-md-12 align-self-center">
                                    <h4 class="text-themecolor">Time Period </h4>
                                </div>
                                <div class="col-md-4">
                                    <div class="example">
                                        <div class="input-group">
                                            <input type="date" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="example">
                                        <div class="input-group">
                                            <input type="date" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <a href="" class="btn btn-primary" href="">Search Status</a>
                                </div>
                            </div>  --}}
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div id="chart_div"></div>
                                    <div id="modal_div"></div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <table class="table table-bordered" style="margin-top: 20px;">
                                        <thead>
                                            <tr>
                                                <th>Status</th>
                                                <th>Count</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $d)
                                            <tr>
                                                <td>{{ $d->status }}</td>
                                                <td>{{ $d->count }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div id="chart_divs"></div>
                                    <div id="modal_divs"></div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <table class="table table-bordered" style="margin-top: 20px;">
                                        <thead>
                                            <tr>
                                                <th>Status</th>
                                                <th>Count</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($datas as $ds)
                                            <tr>
                                                <td>{{ $ds->sub_status }}</td>
                                                <td>{{ $ds->count }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>

    <script src="{{asset('js/loader.js')}}"></script>
    <script src="{{asset('js/core.js')}}"></script>
    <script src="{{asset('js/charts.js')}}"></script>
    <script src="{{asset('js/animated.js')}}"></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawTradesChart);
        google.charts.setOnLoadCallback(drawTradeChart);



        {{-- Add tradmark Status Charts --}}
        function drawTradesChart()
        {
            var analytics = {!! json_encode($arrayss) !!};
            console.log(analytics);
            var record={!! json_encode($arrayss) !!};
            console.log(record);
            // Create our data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Status');
            data.addColumn('number', 'Total Status');
            for(var k in record)
            {
                var v = record[k];
                data.addRow([k,v]);
                console.log(v);
            }
            var options = {
              title: 'Trademark Status Portfoilio',
              width: 900,
              height: 500
            };
            var chart = new google.visualization.PieChart(document.getElementById('chart_divs'));
            function selectHandler()
            {
                var selectedItem = chart.getSelection()[0];
                if (selectedItem)
                {
                    var topping = data.getValue(selectedItem.row, 0);
                    //console.log(topping)
                    if(topping == topping)
                    {
                        //window.open("/posts/action", "_self", true);
                        var trim = topping.replace(/ /g, "%20");
                        //var id = document.getElementById("stud_id").value;
                        console.log(trim);
                        $('#modal_divs').load('modal-datas/piechart/'+trim,function() {
                            $('#bootstrap-modals').modal({
                                show : true
                            });
                        });
                    }
                }
            }
            google.visualization.events.addListener(chart, 'select', selectHandler);
            chart.draw(data, options);
        }
        {{-- Add tradmark Status Charts --}}
        function drawTradeChart()
        {
            var analytics = {!! json_encode($array) !!};
            console.log(analytics);
            var record={!! json_encode($array) !!};
            console.log(record);
            // Create our data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Status');
            data.addColumn('number', 'Total Status');
            for(var k in record)
            {
                var v = record[k];
                data.addRow([k,v]);
                console.log(v);
            }
            var options = {
              title: 'Trademark Portfoilio',
              width: 900,
              height: 500
            };
            var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
            function selectHandler()
            {
                var selectedItem = chart.getSelection()[0];
                if (selectedItem)
                {
                    var topping = data.getValue(selectedItem.row, 0);
                    //console.log(topping)
                    if(topping == topping)
                    {
                        //window.open("/posts/action", "_self", true);
                        var trim = topping.replace(/ /g, "%20");
                        //var id = document.getElementById("stud_id").value;
                        console.log(trim);
                        $('#modal_div').load('modal-data/piechart/'+trim,function() {
                            $('#bootstrap-modal').modal({
                                show : true
                            });
                        });
                    }
                }
            }
            google.visualization.events.addListener(chart, 'select', selectHandler);
            chart.draw(data, options);
        }




    </script>
@include('include.footer')
