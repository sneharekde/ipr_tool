@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Language | <a href="{{action('AddTradmarkController@LanguageShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active">Language</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{url('/language/save')}}" method="POST" accept-charset="utf-8">
                            @csrf
                                <div class="form-group {{ $errors->has('language') ? 'has-error' : '' }}">
                                    <label for="">Language</label>
                                    <input type="text" class="form-control" name="language" value="{{ old('language') }}">
                                    <span class="text-danger">{{ $errors->first('language') }}</span>
                                </div>
                                <div class="col-md-12 center"><button type="Submit" class="btn btn-success">Submit</button></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
