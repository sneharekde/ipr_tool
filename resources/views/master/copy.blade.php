@include('include.header')
    <style>
        .cards{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            background: aliceblue;
        }
        .cards-body{
            padding: 10px;
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Copyright Master | <a href="{{ url('master') }}" > Go Back <i class="fa fa-arrow-circle-left"></i></a></h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/dashbord') }}">Home</a></li>
                            <li class="breadcrumb-item active">Copyright Master</li>
                        </ol>
                    </div>
                </div>
            </div>

        </div>
    </div>
@include('include.footer')
