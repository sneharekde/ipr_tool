@include('include.header')
    <style>
        .cards{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            background: aliceblue;
        }
        .cards-body{
            padding: 10px;
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Trademark Master | <a href="{{ url('master') }}" > Go Back <i class="fa fa-arrow-circle-left"></i></a></h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/dashbord') }}">Home</a></li>
                            <li class="breadcrumb-item active">Trademark Master</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{action('AddTradmarkController@NatureApplicationShow')}}">Mark of The Application</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{action('AddTradmarkController@FieldApplicationShow')}}">Nature Of The Applicant</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{action('AddTradmarkController@NatureApplicantShow')}}">Nature Of The Entity</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{action('AddTradmarkController@ApplicantShow')}}">Applicant Info</a></h5>
                                </div>
                            </div>
                        </div>
                    </div><br>
                    <div class="row">

                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('trade_class') }}">Class</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{action('AddTradmarkController@CategoryMarkShow')}}">Category Of Mark</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ route('appff.index') }}">Application Field</a></h5>
                                </div>
                            </div>
                        </div>
                        {{--  <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{action('AddTradmarkController@ClassGoodServicesShow')}}">Class of Good or Services</a></h5>
                                </div>
                            </div>
                        </div>  --}}
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{action('TradeMarkGovFeesController@index')}}">Type of Application/Government Fee</a></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
