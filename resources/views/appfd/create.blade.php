@include('include.header')
    <style>
        .cards{
            box-shadow: 0 4px 8px 0 rgba(0, 0,0,0.2);
        }
        .cards-body{
            padding: 20px
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Application Field | <a href="{{action('AddTradmarkController@FieldApplicationShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active">Application Field</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="cards">
                <div class="cards-body">

                    {!! Form::open(['route' => 'appff.store','method' => 'POST','data-parsley-validate' => '']) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Application Field') !!}
                            {!! Form::text('name', null, ['class' => 'form-control','required' => '']) !!}
                        </div>
                        <div class="col-md-12 text-center">
                            {!! Form::submit('submit', ['class' => 'btn btn-success']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
