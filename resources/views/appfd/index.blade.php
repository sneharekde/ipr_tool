

@include('include.header')
<style>
    table{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.3);
    }

</style>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Application Field | <a href="{{ url('trad_master') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                    <a  href="{{ route('appff.create') }}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add</a>
                </div>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-rounded" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ $message }}
            </div>
        @endif
        <div class="cards">
            <div class="cards-body">
                <div class="table-responsive">
                    <table id="" class="table table-bordered table-striped" >
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($posts as $app)
                                <tr>
                                    <td>{{$app->id}}</td>
                                    <td>{{$app->name}}</td>
                                    <td><a href="{{ route('appff.edit',$app->id) }}" class="btn btn-warning">Edit</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('include.footer')
