<style>
.dataTables_filter
{
  display: none;
}
.paginate_button
{
  display: none;
}
.dataTables_info
{
  display: none;
}
</style>
<div class="modal  fade bs-example-modal-lg" id="bootstrap-modal1" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Investigation Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow-x: auto;">
        <div id="demo-modal">
          <div class="table-responsive">
            <table id="example24" class="table table-striped table-bordered " >
              <thead>
                <tr>
                  <th>Sr. No</th>
                  <th>Investigation Agency</th>
                  <th>Contact Person</th>
                  <th>Contact Number</th>
                  <th>Contact Email</th>
                  <th>Location</th>
                  <th>Category Of Target</th>
                  <th>Name Of Product</th>
                  <th>Date of start of Investigation</th>
                  <th>Date Of End of Investigation</th>
                  <th>tradmark Application No</th>
                </tr>
              </thead>
              <tbody>
                @foreach($results as $result)
                  @if($result->target ==  $name)
                    <tr>
                      <td>{{ $no++ }}</td>
                      <td>{{ $result->inag->name }}</td>
                      <td>{{ $result->inag->person }}</td>
                      <td>{{ $result->inag->contact }}</td>
                      <td>{{ $result->inag->email }}</td>
                      <td>{{ $result->city->city }}</td>
                      <td>{{ $result->target }}</td>
                      <td>{{ $result->nop->name }}</td>
                      <td>{{ $result->dsi }}</td>
                      <td>{{ $result->dei }}</td>
                      <td>{{ $result->tm_app }}</td>
                    </tr>
                  @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  $(function () {
    // responsive table
    $('#config-table').DataTable({
      responsive: true
    });
    $('#example24').DataTable({
      dom: 'Bfrtip',
      buttons: [
       {
          extend: 'pdf',
          footer: true,
          title: 'Investigation Reports export',
          orientation: 'landscape',
          pageSize: 'LEGAL',
          exportOptions: {
          columns: [0,1,2,3,4,5,6,7,8,9,10]
        }
      }]
    });
    $('.buttons-pdf').addClass('btn btn-primary mr-1');
  });
</script>
