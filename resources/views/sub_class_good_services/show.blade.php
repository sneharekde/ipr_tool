@include('include.header')
   <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Class of Good or Services | <a href="{{action('AddTradmarkController@master')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4> 
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                                <li class="breadcrumb-item active">Class of Good or Services</li>
                            </ol>
                            <a  href="{{action('AddTradmarkController@SubClassCreate')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add</a>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->


                   @if ($message = Session::get('success'))
      <div class="alert alert-success alert-rounded" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{ $message }}
</div>
@endif

                    <!-- Column -->
                   <div class="col-md-12">
                     <div class="card">
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                              <table id="myTable" class="table table-bordered table-striped" data-order='[[ 0, "desc" ]]'>
                                         <thead>
                            <tr>
                            <th>Id</th>
                            <th>ClassName</th>
                            <th>Subclass</th>
                            <th>Description</th>         
                            <th>Edit</th>  
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($subclasss as $app)  
                            <tr>
                            <td>{{$app->id}}</td>
                            <td> {{$app->ClassGoodServices->class_of_good_services}} </td>
                            <td>{{$app->class}}</td>
                            <td>{{str_limit($app->description, 70)}}</td>
                            <td class="edit"><a>
                                <a href="{{url('edit/'.$app->id.'/sub_class_good_services')}}" type="button" name="submit" value="submit" class="btn btn-primary btn-edit">&nbsp;Edit
                                </a>
                            </a></td> 
                            
                          </tr>
                          @endforeach

                            </tbody>
                      </table>
                                </div>
                    




                </div>

               
    

                
            </div>
                   </div>
        </div>
  @include('include.footer')
