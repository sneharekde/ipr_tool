@include('include.header')
   <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
              <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Class of Good or Services | <a href="{{action('AddTradmarkController@SubClassShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                                <li class="breadcrumb-item active">Class of Good or Services</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
              
              <form action="{{url('/sub_class_good_services/save')}}" method="POST" accept-charset="utf-8">
            @csrf
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                           <div class="col-md-12">
            <div class="box-body">

              <!-- Date -->
              <div class="form-group {{ $errors->has('class_good_services_id') ? 'has-error' : '' }}">
                <label for="">Class of Goods or Services * :</label>
                <select name="class_good_services_id" class="form-control">
                  <option value="0">---Select---</option>
                  @foreach($classgoodservicess as $app)
                  <option value="{{$app->id}}">{{$app->class_of_good_services}}</option>
                  @endforeach
                </select>
                <span class="text-danger">{{ $errors->first('class_good_services_id') }}</span>
              </div>

              
            </div>
            <!-- /.box-body -->
          </div>
          <div class="col-md-6">
            <div class="box-body">
              <!-- Date -->
              <div class="form-group {{ $errors->has('class') ? 'has-error' : '' }}">
                <label for="">Class :</label>
               <input type="text" class="form-control" name="class" value="{{ old('class') }}">
                <span class="text-danger">{{ $errors->first('class') }}</span>
              </div>

            

            </div>
            <!-- /.box-body -->
          </div>

          <div class="col-md-6">
            <div class="box-body">
              <!-- Date -->
              <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="">Description :</label>
               <textarea name="description" id="" cols="30" rows="7" class="form-control"></textarea>
                <span class="text-danger">{{ $errors->first('description') }}</span>
              </div>


            </div>
            <!-- /.box-body -->
          </div>

          <div class="col-md-12">
              <div class="form-group text-center">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
            </div>

                </div>
                  </div>
                </div>

              </form>

               
    

                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
  @include('include.footer')