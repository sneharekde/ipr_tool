@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Create New | <a href="{{url('summary/documents',$id)}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Create New</li>
                        </ol>
                    </div>
                </div>
            </div>

            @include('include.message')
            <div class="card">
    			<div class="card-body">
    				{!! Form::open(['url' => 'liti_doc/submit','method' => 'POST','data-parsley-validate' => '','files' => true]) !!}
    				<div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                            {!! Form::label('name', 'Name*:') !!}
                            {!! Form::text('name', null, ['class' => 'form-control','required' => '']) !!}
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                            <input type="hidden" name="doc_id" value="{{ $id }}">
                            {{ Form::label('docs','Document:') }}
                            {{ Form::file('docs[]',['class' => 'form-control','multiple' => true,'required' => '']) }}
                        </div>

                        <div class="col-md-12">
                            <div class="form-group text-center">
                                <button type="submit" name="submit" class="btn btn-success">Create</button>
                            </div>
                        </div>
    				</div>


    				{!! Form::close() !!}
    			</div>
    		</div>
		</div>
	</div>

@include('include.footer')
