@include('include.header')

    <style>
        .card{
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Update | <a href="{{ route('summary.index') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>
            @include('include.message')
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('/summary/'.$summ->id) }}">Litigation Details</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link"  href="{{ url('/summary/hearing/'.$summ->id) }}">Next Hearing and Stage</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link active" href="{{ url('/summary/documents/'.$summ->id) }}">Documents</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/summary/fees/'.$summ->id) }}">Fees</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/summary/completion/'.$summ->id) }}">Completion</a>
                        </li>
                    </ul>
                    @if ($summ->status == 'Pending')
                    <a href="{{url('liti_doc/create/'.$summ->id)}}" class="btn btn-warning" style="margin-top: 10px;margin-bottom: 10px;" >Add Document</a>
                    @endif

                             <table class="table table-bordered table-striped" style="margin-top: 10px">
                                 <thead>
                                     <tr>
                                         <th>Document Name</th>
                                         <th>Document</th>
                                         <th>Action</th>
                                     </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($liti as $docs)
                                     <tr>
                                            <td>{{ $docs->name }}</td>
                                            <td>
                                            @foreach(explode(',', $docs->docs) as $file)
                                             <a href="{{ url('public/litigation/summary/hearing',$file) }}" download="">{{ $file }}</a><br>
                                             @endforeach
                                            </td>

                                         <td><a href="{{ url('delete/liti-doc',$docs->id) }}" class="btn btn-sm btn-danger">Delete</a></td>
                                     </tr>
                                    @endforeach
                                 </tbody>
                             </table>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
