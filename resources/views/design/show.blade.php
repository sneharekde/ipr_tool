@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Show Design | <a href="{{action('DesignController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Show Design</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-12" id="content">
                <div class="card">
                    <div class="card-body">
                        @include('include.message')

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Details</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Agent / Attorney Fee</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Proprieter Code</th>
                                        <td>{{ $show->prop_code }}</td>
                                    </tr>
                                    <tr>
                                        <th>Govt. Fee</th>
                                        <td>{{ $show->govt_fee }}</td>
                                    </tr>
                                    <tr>
                                        <th>Application / Design Registration Number</th>
                                        <td>{{ $show->app_no }}</td>
                                    </tr>
                                    <tr>
                                        <th>Date of Filing</th>
                                        <td>{{ date('d-m-Y',strtotime($show->filing_date))  }}</td>
                                    </tr>
                                    <tr>
                                        <th>Type Application</th>
                                        <td>{{ $show->app_type }}</td>
                                    </tr>
                                    <tr>
                                        <th>Name of Applicant</th>
                                        <td>{{ $show->name_of_applicant }}</td>
                                    </tr>
                                    <tr>
                                        <th>Nature of the Applicant</th>
                                        <td>{{ $show->natappli->application_field }}</td>
                                    </tr>
                                    <tr>
                                        <th>Entity Name</th>
                                        <td>
                                            @if ($show->ent_id !='')
                                            {{ $show->ent->name }}
                                            @else
                                            NA
                                            @endif
                                            </td>
                                    </tr>
                                    <tr>
                                        <th>Nature of Entity</th>
                                        <td>{{ $show->natent->nature_of_applicant }}</td>
                                    </tr>
                                    <tr>
                                        <th>Name of Article</th>
                                        <td>{{ $show->article_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Image (Image Like Trademark Logo)</th>
                                        <td><a href="{{url('public/copyright/design/'.$show->image_t )}}" download="">{{ $show->image_t }}</a></td>
                                    </tr>
                                    <tr>
                                        <th>Description</th>
                                        <td>{{ $show->description }}</td>
                                    </tr>
                                    <tr>
                                        <th>Class</th>
                                        <td>{{ $show->classg->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Representation</th>
                                        <td>
                                        @if ($show->image !='')
                                        <a href="{{ url('public/copyright/design/'.$show->image) }}" download="">{{ $show->image }}</a>
                                        @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Language</th>
                                        <td>{{ $show->lang->language }}</td>
                                    </tr>
                                    <tr>
                                        <th>Remarks</th>
                                        <td>{{ $show->remarks }}</td>
                                    </tr>
                                    <tr>
                                        <th>Agent / Attorney Name</th>
                                        <td>
                                            @if ($show->agent_id !='')
                                            {{ $show->agent->name }}
                                            @else
                                            NA
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Agent / Attorney Code</th>
                                        <td>{{ $show->agent_code }}</td>
                                    </tr>
                                    <tr>
                                        <th>Power of Attorney</th>
                                        <td>
                                            <a href="{{ url('public/copyright/design',$show->poa) }}" download="">{{ $show->poa }}</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Date of Registration</th>
                                        <td>
                                            @if ($show->dor !='')
                                            {{ date('d-m-Y',strtotime($show->dor))  }}
                                            @else
                                            NA
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Certificate of Registration</th>
                                        <td>
                                            <a href="{{ url('public/copyright/design/'.$show->cert_reg) }}" download="">{{ $show->cert_reg }}</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="" style="margin-top:10px">
                                    <a href="{{ url('design-fee',$show->id) }}" class="btn btn-info">Add Fees</a>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Fees</th>
                                                <th>Invoice</th>
                                                <th>stamp paper</th>
                                                <th>notarisation charges</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($posts as $post)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ date('d-m-Y',strtotime($post->date)) }}</td>
                                                    <td>{{ $post->fee }}</td>
                                                    <td>
                                                        @if ($post->invoice !='')
                                                            <a href="{{ url('public/copyright/img',$post->invoice) }}">{{ $post->invoice }}</a>
                                                        @else
                                                        NA
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($post->stamp !='')
                                                            <a href="{{ url('public/copyright/img',$post->stamp) }}">{{ $post->stamp }}</a>
                                                        @else
                                                        NA
                                                        @endif
                                                    </td>
                                                    <td>{{ $post->not_fee }}</td>
                                                    <td><a href="{{ url('design-fee/edit',$post->id) }}" class="btn btn-success btn-xs">Edit</a></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
