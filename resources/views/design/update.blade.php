@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Update Status of Design | <a href="{{action('DesignController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Update Status of Copyright</li>
                        </ol>
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="card">
                <div class="card-body">
                    <form action="{{ url('/design/status/submit') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    @csrf
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="hidden" name="des_id" value="{{ $design->id }}">
                                <label>Status:</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="">Select Status</option>
                                    <option value="Active Application" @if($design->status == 'Abandoned / Rejected' || $design->status == 'Registered and Awaiting Publication' || $design->status == 'Registered and Published' || $design->status == 'Expired - For Renewal' || $design->status == 'Renewed' || $design->status == 'Extension Expired') disabled @endif>Active application</option>
                                    <option value="Abandoned / Rejected" @if($design->status == 'Abandoned / Rejected' || $design->status == 'Registered and Awaiting Publication' || $design->status == 'Registered and Published' || $design->status == 'Expired - For Renewal' || $design->status == 'Renewed' || $design->status == 'Extension Expired') disabled @endif>Abandoned / Rejected</option>
                                    <option value="Registered and Awaiting Publication" @if($design->status == 'Registered and Awaiting Publication' || $design->status == 'Registered and Published' || $design->status == 'Expired - For Renewal' || $design->status == 'Renewed' || $design->status == 'Extension Expired') disabled @endif>Registered And Awaiting Publications</option>
                                    <option value="Registered and Published" @if($design->status == 'Registered and Published' || $design->status == 'Expired - For Renewal' || $design->status == 'Renewed' || $design->status == 'Extension Expired') disabled @endif>Registered and Published</option>
                                    <option value="Expired - For Renewal" @if($design->status == 'Expired - For Renewal' || $design->status == 'Renewed' || $design->status == 'Extension Expired') disabled @endif>Expired - For Renewal</option>
                                    <option value="Renewed" @if($design->status == 'Renewed' || $design->status == 'Extension Expired') disabled @endif>Renewed</option>
                                    <option value="Extension Expired" @if($design->status == 'Extension Expired') disabled @endif>Extension Expired</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="sub_status">
                            <div class="form-group">
                                <label>Sub Status:</label>
                                <select class="form-control" id="select1" name="sub_status">
                                    <option value="">Select Sub Status</option>
                                    <option value="New Application" disabled>New Application</option>
                                    <option value="Examination Report Received" @if(($design->status == 'Active Application' && $design->sub_status == 'Examination Report Received') || ($design->status == 'Active Application' && $design->sub_status == 'Reply to objection submitted') || ($design->status == 'Active Application' && $design->sub_status == 'Hearing for Objection') || ($design->status == 'Active Application' && $design->sub_status == 'Removal of Objection Period Extended')) disabled @endif>Examination Report Received</option>
                                    <option value="Reply to objection submitted" @if(($design->status == 'Active Application' && $design->sub_status == 'Reply to objection submitted') || ($design->status == 'Active Application' && $design->sub_status == 'Hearing for Objection') || ($design->status == 'Active Application' && $design->sub_status == 'Removal of Objection Period Extended')) disabled @endif>Reply to Objection Submitted</option>
                                    <option value="Hearing for Objection" @if(($design->status == 'Active Application' && $design->sub_status == 'Hearing for Objection') || ($design->status == 'Active Application' && $design->sub_status == 'Removal of Objection Period Extended')) disabled @endif>Hearing for Objection</option>
                                    <option value="Removal of Objection Period Extended" @if(($design->status == 'Active Application' && $design->sub_status == 'Removal of Objection Period Extended')) disabled @endif>Removal of Objection Period Extended</option>
                                </select>
                            </div>
                        </div>

                        {{-- Ststus Wise --}}
                        <div id="aban">
                            <h4 class="text-center">Abandoned / Rejected</h4><hr>
                        </div>
                        <div id="reg">
                            <h4 class="text-center">Registered and Awaiting Publications:</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of Registration:</label>
                                    <input type="text" id="regd" name="dor" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12  col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Certificate of registration (doc):</label>
                                    <input type="file" name="cor" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment1" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div id="repu">
                            <h4 class="text-center">Registered and Published:</h4><hr>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of publication:</label>
                                    <input type="text" id="repud" name="date1" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Journal Number:</label>
                                    <input type="text" name="jno" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div id="exp">
                            <h4 class="text-center">Expired - For Renewal</h4><hr>
                        </div>
                        <div id="ren">
                            <h4 class="text-center">Renewed</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of renewal:</label>
                                    <input type="text" id="rnd" name="dorenew" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of expiration of extension:</label>
                                    <input type="text" id="expd" name="doe" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div  id="ext">
                            <h4 class="text-center">Extension expired</h4><hr>
                        </div>
                        {{-- Sub Status Wise --}}
                        <div id="new">
                            <h4 class="text-center">New application</h4><hr>
                        </div>
                        <!-- Mandatory waiting period -->
                        <div id="err">
                            <h4 class="text-center">Examination Report Received</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of Receipt of examination report:</label>
                                    <input type="text" id="mwpd" name="date2" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea class="form-control" rows="5" name="comment2"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Document</label>
                                    <input type="file" name="docs1" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                {!! Form::label('remindery', 'Reminder') !!}
                                {!! Form::select('remindery', ['Yes' => 'Yes','No' => 'No'], null, ['class' => 'form-control','id' => 'remindery','placeholder' => 'Select']) !!}
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="disprem" style="display: none">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>1st Reminder :</label>
                                            <input type="text" id="remd1" name="rem11" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>2nd reminder:</label>
                                            <input type="text" id="remd2" name="rem21" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>3rd reminder:</label>
                                            <input type="text" id="remd3" name="rem31" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Opposition  -->
                        <div id="rtos">
                            <h4 class="text-center">Reply to Objection Submitted</h4><hr>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Date:</label>
                                    <input type="text" id="rtdd" name="date3" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment3" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    <input type="file" name="docs2" class="form-control">
                                </div>
                            </div>
                        </div>
                        <!-- Reply to opposition submitted -->
                        <div id="hfo">
                            <h4 class="text-center">Hearing for Objection</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date for hearing:</label>
                                    <input type="text" id="herdd" name="her_date" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" rows="5" name="comment4"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    <input type="file" name="docs3" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                {!! Form::label('remindery02', 'Reminder') !!}
                                {!! Form::select('remindery02', ['Yes' => 'Yes','No' => 'No'], null, ['class' => 'form-control','id' => 'remindery02','placeholder' => 'Select']) !!}
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="disprem02" style="display: none">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>1st Reminder :</label>
                                            <input type="text" id="remd11" name="rem12" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>2nd reminder:</label>
                                            <input type="text" id="remd12" name="rem22" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>3rd reminder:</label>
                                            <input type="text" id="remd13" name="rem32" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- Hearing -->
                        <div id="roope">
                            <h4 class="text-center">Removal of Objection Period Extended</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document :</label>
                                    <input type="file" name="docs4" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment5" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                {!! Form::label('remindery03', 'Reminder') !!}
                                {!! Form::select('remindery03', ['Yes' => 'Yes','No' => 'No'], null, ['class' => 'form-control','id' => 'remindery03','placeholder' => 'Select']) !!}
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="disprem03" style="display: none">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>1st reminder:</label>
                                            <input type="text" id="remd21" name="rem13" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>2nd reminder:</label>
                                            <input type="text" id="remd22" name="rem23" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>3rd reminder:</label>
                                            <input type="text" id="remd23" name="rem33" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- End of the status and sub status -->
                        <div class="col-sm-12 col-xs-12 col-lg-12 col-md-12">
                            <div class="form-group text-center">
                                <input type="submit" name="submit" value="Submit" class="btn btn-success">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Sub Status</th>
                                    <th>Document</th>
                                    <th>Comments</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($statuss as $status)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>
                                            @if($status->date !='')
                                                {{ date('d-m-Y',strtotime($status->date)) }}
                                            @elseif($status->derrer != '')
                                                {{ date('d-m-Y',strtotime($status->derrer)) }}
                                            @elseif($status->dfh != '')
                                                {{ date('d-m-Y',strtotime($status->dfh)) }}
                                            @elseif($status->dor != '')
                                                {{ date('d-m-Y',strtotime($status->dor)) }}
                                            @elseif($status->pub_date != '')
                                                {{ date('d-m-Y',strtotime($status->pub_date)) }}
                                            @elseif($status->ren_date != '')
                                                {{ date('d-m-Y',strtotime($status->ren_date)) }}
                                            @endif
                                        </td>
                                        <td>{{ $status->status }}</td>
                                        <td>{{ $status->sub_status }}</td>
                                        <td>
                                            @if($status->docs != '')
                                                @foreach(explode(',', $status->docs) as $file)
                                                    <a href="{{ url('public/copyright/design/'.$file) }}" download="">{{ $file }}</a>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>{{ $status->comment }}</td>
                                        <td>
                                            @if ($status->sub_status !='New Application')
                                            <a href="{{ url('design/update_status/edit/'.$status->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                            @endif

                                            {{--  <a href="" class="btn btn-sm btn-info">View</a>  --}}
                                            {{--  <a href="" class="btn btn-sm btn-danger">Delete</a>  --}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function(){
            $('#remindery').on('change',function(){
                var remindery = $(this).val();
                if(remindery == 'Yes'){
                    $('#disprem').show();
                }
                if(remindery != 'Yes'){
                    $('#disprem').hide();
                }
            });

            $('#remindery02').on('change',function(){
                var remindery02 = $(this).val();
                if(remindery02 == 'Yes'){
                    $('#disprem02').show();
                }
                if(remindery02 != 'Yes'){
                    $('#disprem02').hide();
                }
            });

            $('#remindery03').on('change',function(){
                var remindery03 = $(this).val();
                if(remindery03 == 'Yes'){
                    $('#disprem03').show();
                }
                if(remindery03 != 'Yes'){
                    $('#disprem03').hide();
                }
            });
        });
    </script>

    <script type="text/javascript">
        {{--  Status Wise  --}}
        $(document).ready(function(){
            $('#sub_status').hide();
            $('#aban').hide();
            $('#reg').hide();
            $('#repu').hide();
            $('#exp').hide();
            $('#ren').hide();
            $('#ext').hide();
            $('#new').hide();
            $('#err').hide();
            $('#rtos').hide();
            $('#hfo').hide();
            $('#roope').hide();

            $('#status').change(function(){
                if($('#status').val() == ''){
                    $('#sub_status').hide();
                    $('#aban').hide();
                    $('#reg').hide();
                    $('#repu').hide();
                    $('#exp').hide();
                    $('#ren').hide();
                    $('#ext').hide();

                    $('#new').hide();
                    $('#err').hide();
                    $('#rtos').hide();
                    $('#hfo').hide();
                    $('#roope').hide();
                }
            });

            $('#status').change(function(){
                if ($('#status').val() == 'Active Application')
                {
                    $('#sub_status').show();
                }else{
                    $('#sub_status').hide();
                    $('#new').hide();
                    $('#err').hide();
                    $('#rtos').hide();
                    $('#hfo').hide();
                    $('#roope').hide();
                }
            });

            $('#status').change(function(){
                if ($('#status').val() == 'Abandoned / Rejected')
                {
                    $('#aban').show();
                }else{
                    $('#aban').hide();
                }
            });

            $('#status').change(function(){
                if ($('#status').val() == 'Registered and Awaiting Publication')
                {
                    $('#reg').show();
                }else{
                    $('#reg').hide();
                }
            });

            $('#status').change(function(){
                if ($('#status').val() == 'Registered and Published')
                {
                    $('#repu').show();
                }else{
                    $('#repu').hide();
                }
            });

            $('#status').change(function(){
                if ($('#status').val() == 'Expired - For Renewal')
                {
                    $('#exp').show();
                }else{
                    $('#exp').hide();
                }
            });

            $('#status').change(function(){
                if ($('#status').val() == 'Renewed')
                {
                    $('#ren').show();
                }else{
                    $('#ren').hide();
                }
            });

            $('#status').change(function(){
                if ($('#status').val() == 'Extension Expired')
                {
                    $('#ext').show();
                }else{
                    $('#ext').hide();
                }
            });

            $('#select1').change(function(){
                if($('#select1').val() == ''){
                    $('#new').hide();
                    $('#err').hide();
                    $('#rtos').hide();
                    $('#hfo').hide();
                    $('#roope').hide();
                }

                if($('#select1').val() == 'New Application'){
                    $('#new').show();
                }else{
                    $('#new').hide();
                }

                if($('#select1').val() == 'Examination Report Received'){
                    $('#err').show();
                }else{
                    $('#err').hide();
                }

                if($('#select1').val() == 'Reply to objection submitted'){
                    $('#rtos').show();
                }else{
                    $('#rtos').hide();
                }

                if($('#select1').val() == 'Hearing for Objection'){
                    $('#hfo').show();
                }else{
                    $('#hfo').hide();
                }

                if($('#select1').val() == 'Removal of Objection Period Extended'){
                    $('#roope').show();
                }else{
                    $('#roope').hide();
                }
            });

        });
    </script>
    <script>
        $(function() {
            $('#herdd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });
            $('#remd21').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#remd22').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#remd23').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#remd11').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#remd12').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#remd13').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#rtdd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#mwpd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#remd1').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd2').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#remd3').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#expd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#rnd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#regd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#repud').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });
        });
    </script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
@include('include.footer')
