@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Update Status of Design | <a href="{{action('DesignController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Update Status of Copyright</li>
                        </ol>
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="card">
                <div class="card-body">
                    <form action="{{ url('/design/edit_status/submit') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    @csrf
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="hidden" name="des_id" value="{{ $design->id }}">
                                <label>Status:</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="" @if($design->status == '') selected="selected" @else disabled @endif>Select Status</option>
                                    <option value="Active Application" @if($design->status == 'Active Application') selected="selected" @else disabled @endif>Active application</option>
                                    <option value="Abandoned / Rejected" @if($design->status == 'Abandoned / Rejected') selected="selected" @else disabled @endif>Abandoned / Rejected</option>
                                    <option value="Registered and Awaiting Publication" @if($design->status == 'Registered and Awaiting Publication') selected="selected" @else disabled @endif>Registered And Awaiting Publications</option>
                                    <option value="Registered and Published" @if($design->status == 'Registered and Published') selected="selected" @else disabled @endif>Registered and Published</option>
                                    <option value="Expired - For Renewal" @if($design->status == 'Expired - For Renewal') selected="selected" @else disabled @endif>Expired - For Renewal</option>
                                    <option value="Renewed" @if($design->status == 'Renewed') selected="selected" @else disabled @endif>Renewed</option>
                                    <option value="Extension Expired" @if($design->status == 'Extension Expired') selected="selected" @else disabled @endif>Extension Expired</option>
                                </select>
                            </div>
                        </div>
                        @if ($design->sub_status !='')
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="sub_status">
                            <div class="form-group">
                                <label>Sub Status:</label>
                                <select class="form-control" id="select1" name="sub_status">
                                    <option value="" @if($design->sub_status == '') selected="selected" @else disabled @endif>Select Sub Status</option>
                                    <option value="New Application" disabled>New Application</option>
                                    <option value="Examination Report Received" @if($design->sub_status == 'Examination Report Received') selected="selected" @else disabled @endif>Examination Report Received</option>
                                    <option value="Reply to objection submitted" @if($design->sub_status == 'Reply to objection submitted') selected="selected" @else disabled @endif>Reply to Objection Submitted</option>
                                    <option value="Hearing for Objection" @if($design->sub_status == 'Hearing for Objection') selected="selected" @else disabled @endif>Hearing for Objection</option>
                                    <option value="Removal of Objection Period Extended" @if($design->sub_status == 'Removal of Objection Period Extended') selected="selected" @else disabled @endif>Removal of Objection Period Extended</option>
                                </select>
                            </div>
                        </div>
                        @endif


                        {{-- Ststus Wise --}}
                        @if ($design->status =='Abandoned / Rejected')
                        <div id="aban">
                            <h4 class="text-center">Abandoned / Rejected</h4><hr>
                        </div>
                        @endif
                        @if ($design->status =='Registered and Awaiting Publication')
                        <div id="reg">
                            <h4 class="text-center">Registered and Awaiting Publications:</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of Registration:</label>
                                    <input type="text" id="regd" name="dor" class="form-control" value="{{ $design->dor }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12  col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Certificate of registration (doc):</label>
                                    @if ($design->docs !='')
                                        <div class="col-md-3">
                                            <a href="{{ url('public/copyright/design/'.$design->docs) }}">{{ $design->docs }}</a>
                                            <a href="{{ url('designcor/images/delete/'.$design->docs.'/'.$design->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                        </div>
                                    @else
                                    <input type="file" name="cor" class="form-control">
                                    @endif

                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment1" rows="5">{{ $design->comment }}</textarea>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($design->status =='Registered and Published')
                        <div id="repu">
                            <h4 class="text-center">Registered and Published:</h4><hr>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of publication:</label>
                                    <input type="text" id="repud" name="date1" class="form-control" value="{{ $design->pub_date }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Journal Number:</label>
                                    <input type="text" name="jno" class="form-control" value="{{ $design->jrno }}">
                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($design->status =='Expired - For Renewal')
                        <div id="exp">
                            <h4 class="text-center">Expired - For Renewal</h4><hr>
                        </div>
                        @endif
                        @if ($design->status =='Renewed')
                        <div id="ren">
                            <h4 class="text-center">Renewed</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of renewal:</label>
                                    <input type="text" id="rnd" name="dorenew" class="form-control" value="{{ $design->ren_date }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of expiration of extension:</label>
                                    <input type="text" id="expd" name="doe" class="form-control" value="{{ $design->exp_date }}">
                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($design->status =='Extension Expired')
                        <div  id="ext">
                            <h4 class="text-center">Extension expired</h4><hr>
                        </div>
                        @endif

                        {{-- Sub Status Wise --}}
                        @if ($design->sub_status =='Examination Report Received')
                        <!-- Mandatory waiting period -->
                        <div id="err">
                            <h4 class="text-center">Examination Report Received</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of Receipt of examination report:</label>
                                    <input type="text" id="mwpd" name="date2" value="{{ $design->derrer }}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea class="form-control" rows="5" name="comment2">{{ $design->comment }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Document</label>
                                    @if ($design->docs !='')
                                        <div class="col-md-3">
                                            <a href="{{ url('public/copyright/design/'.$design->docs) }}">{{ $design->docs }}</a>
                                            <a href="{{ url('designcor/images/delete/'.$design->docs.'/'.$design->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                        </div>
                                    @else
                                    <input type="file" name="docs1" class="form-control">
                                    @endif

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>1st Reminder :</label>
                                    <input type="text" id="remd1" name="rem11" class="form-control" value="{{ $design->rem1 }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>2nd reminder:</label>
                                    <input type="text" id="remd2" name="rem21" class="form-control" value="{{ $design->rem2 }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>3rd reminder:</label>
                                    <input type="text" id="remd3" name="rem31" class="form-control" value="{{ $design->rem3 }}">
                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($design->sub_status =='Reply to objection submitted')
                        <!-- Opposition  -->
                        <div id="rtos">
                            <h4 class="text-center">Reply to Objection Submitted</h4><hr>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Date:</label>
                                    <input type="text" id="rtdd" name="date3" class="form-control" value="{{ $design->date }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment3" rows="5">{{ $design->comment }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    @if ($design->docs !='')
                                        <div class="col-md-3">
                                            <a href="{{ url('public/copyright/design/'.$design->docs) }}">{{ $design->docs }}</a>
                                            <a href="{{ url('designcor/images/delete/'.$design->docs.'/'.$design->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                        </div>
                                    @else
                                    <input type="file" name="docs2" class="form-control">
                                    @endif

                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($design->sub_status =='Hearing for Objection')
                        <!-- Reply to opposition submitted -->
                        <div id="hfo">
                            <h4 class="text-center">Hearing for Objection</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date for hearing:</label>
                                    <input type="text" id="herdd" name="her_date" class="form-control" value="{{ $design->dfh }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" rows="5" name="comment4">{{ $design->comment }}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    @if ($design->docs !='')
                                        <div class="col-md-3">
                                            <a href="{{ url('public/copyright/design/'.$design->docs) }}">{{ $design->docs }}</a>
                                            <a href="{{ url('designcor/images/delete/'.$design->docs.'/'.$design->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                        </div>
                                    @else
                                    <input type="file" name="docs3" class="form-control">
                                    @endif

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>1st Reminder :</label>
                                    <input type="text" id="remd11" name="rem12" class="form-control" value="{{ $design->rem1 }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>2nd reminder:</label>
                                    <input type="text" id="remd12" name="rem22" class="form-control" value="{{ $design->rem2 }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>3rd reminder:</label>
                                    <input type="text" id="remd13" name="rem32" class="form-control" value="{{ $design->rem3 }}">
                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($design->sub_status =='Removal of Objection Period Extended')
                        <!-- Hearing -->
                        <div id="roope">
                            <h4 class="text-center">Removal of Objection Period Extended</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document :</label>
                                    @if ($design->docs !='')
                                        <div class="col-md-3">
                                            <a href="{{ url('public/copyright/design/'.$design->docs) }}">{{ $design->docs }}</a>
                                            <a href="{{ url('design/images/delete/'.$design->docs.'/'.$design->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                        </div>
                                    @else
                                        <input type="file" name="docs4" class="form-control">
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment5" rows="5">{{ $design->comment }}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>1st reminder:</label>
                                    <input type="text" id="remd21" name="rem13" value="{{ $design->rem1 }}" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>2nd reminder:</label>
                                    <input type="text" id="remd22" name="rem23" value="{{ $design->rem2 }}" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>3rd reminder:</label>
                                    <input type="text" id="remd23" name="rem33" value="{{ $design->rem3 }}" class="form-control">
                                </div>
                            </div>
                        </div>
                        @endif
                        <!-- End of the status and sub status -->
                        <div class="col-sm-12 col-xs-12 col-lg-12 col-md-12">
                            <div class="form-group text-center">
                                <input type="submit" name="submit" value="Submit" class="btn btn-success">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/bootstrap.min.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script>
        $(function() {
            $('#herdd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });
            $('#remd21').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#remd22').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#remd23').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#remd11').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#remd12').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#remd13').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#rtdd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#mwpd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#remd1').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd2').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#remd3').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#expd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#rnd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#regd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#repud').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });
        });
    </script>
@include('include.footer')
