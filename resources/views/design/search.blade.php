@include('include.header')
   {{-- <style>
  table thead tr th{
    border: 1px solid black;
    padding: 5px;
  }
   table tbody tr td{
    border: 1px solid black;
    padding: 10px;
   }
   table{
    margin-bottom: 20px;
   }
   select{
    -webkit-box-shadow: 5px 5px 5px #666;
    -moz-box-shadow: 5px 5px 5px #666;
    box-shadow: 5px 5px 5px #666;
    display:inline-block;
   }
      input{
    -webkit-box-shadow: 5px 5px 5px #666;
    -moz-box-shadow: 5px 5px 5px #666;
    box-shadow: 5px 5px 5px #666;
   }
</style> --}}
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Design Portfolio | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Design Portfolio</li>
                        </ol>
                        <a  href="{{action('DesignController@Create')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add Design</a>
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-rounded" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ $message }}
                </div>
            @endif
            <div class="card">
                <form action="{{url('/design/show')}}" method="POST" accept-charset="utf-8">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Application / Registration No.</label>
                                <select name="sapp_no" id="" class="form-control">
                                    <option value="">Select Application Number</option>
                                    @foreach ($app_no as $item)
                                        <option value="{{ $item->app_no }}" @if($item->app_no == $sapp_no) selected @endif>{{ $item->app_no }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nature of the Applicant :</label>
                                <select name="natappli_id" class="form-control">
                                    <option value="" >Select Nature of Applicant</option>
                                    @foreach($noapp as $application)
                                    <option value="{{$application->natappli_id}}" @if($application->natappli_id == $nat_applnt) selected @endif>{{$application->natappli->application_field}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Applicant Name :</label>
                                <select name="nameofap" class="form-control">
                                    <option value="" >Select Applicant Name</option>
                                    @foreach($appl_na as $applicant)
                                    @if ($applicant->name_of_applicant !='')
                                    <option value="{{$applicant->name_of_applicant}}" @if($applicant->name_of_applicant == $nameofap) selected @endif>{{$applicant->name_of_applicant}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Entity Name:</label>
                                <select name="ent_id" class="form-control">
                                    <option value="" >Select Entity Name</option>
                                    @foreach($entity as $ent)
                                            @if($ent->ent_id !='')
                                                <option value="{{$ent->ent_id}}" @if($ent->ent_id == $ent_name) selected @endif>{{$ent->ent->name}}</option>
                                            @endif
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nature of Entity :</label>
                                <select name="natent_id" class="form-control">
                                    <option value="" >Select Nature of Entity</option>
                                    @foreach($natent as $natureapplicant)
                                    @if($natureapplicant->natent_id != '')
                                        <option value="{{$natureapplicant->natent_id}}" @if($natureapplicant->natent_id == $natentit) selected @endif>{{$natureapplicant->natent->nature_of_applicant}}</option>
                                    @endif
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Agent Name :</label>
                                <select name="agent_id" class="form-control">
                                    <option value="">Select Agent Name</option>
                                    @foreach($agent_name as $agent)
                                    @if($agent->agent_id != '')
                                    <option value="{{$agent->agent_id}}" @if($agent->agent_id == $agentn) selected @endif>{{$agent->agent->name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Status :</label>
                                <select name="design_status" class="form-control" >
                                    <option value="">Select Status</option>
                                    @foreach ($status as $item)
                                    @if ($item->status !='')

                                    <option value="{{ $item->status }}" @if($item->status == $design_status) selected @endif>{{ $item->status }}</option>

                                    @endif

                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>From Date :</label>
                                <div class="input-group">
                                    <input type="date" name="from" class="form-control" value="{{ $from }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="icon-calender"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>To Date :</label>
                                <div class="input-group">
                                    <input type="date" name="to" class="form-control" value="{{ $to }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="icon-calender"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <p id="demo" align="center"></p>
                        <table id="myTable" class="table table-bordered" data-order='[[ 0, "desc" ]]' data-page-length='50'>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Application / Design Registration No.</th>
                                    <th>Date of filing</th>
                                    <th>Name of Article</th>
                                    <th>Class</th>
                                    <th>Best Image</th>
                                    <th>Agent</th>
                                    <th>status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($results))
                                    @foreach($results as $result)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $result->app_no }}</td>
                                            <td>{{ $result->filing_date }}</td>
                                            <td>{{ $result->article_name }}</td>
                                            <td>{{ $result->classg->name }}</td>
                                            <td><img src="{{asset('public/copyright/design/'.$result->image_t)}}" height="20px"></td>
                                            <td>
                                                @if ($result->agent !='')
                                                {{ $result->agent->name }}
                                                @else
                                                NA
                                                @endif
                                                </td>
                                            <td>
                                                @if ($result->status == 'Active Application')
                                                {{ $result->status.' -> '.$result->sub_status }}
                                                @else
                                                {{ $result->status }}
                                                @endif
                                                </td>
                                            <td>
                                                <a href="{{ url('/design/'.$result->id.'/edit') }}" type="edit" class="btn btn-warning">Edit</a>
                                                <a href="{{ url('/design/'.$result->id.'/update_status') }}" class="btn btn-info">Update</a>
                                                <a href="{{ url('/design/'.$result->id.'/view') }}" class="btn btn-primary" href="">View</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

  <script type="text/javascript">
    var x = document.getElementById("myTable").rows.length - 1;
    document.getElementById("demo").innerHTML = "Total Records are " + x;
  </script>

  <script type="text/javascript">
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping

        });

  </script>
  @include('include.footer')
