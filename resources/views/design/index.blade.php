@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
   {{-- <style>
    table thead tr th{
        border: 1px solid black;
        padding: 5px;
    }
    table tbody tr td{
        border: 1px solid black;
        padding: 10px;
    }
    table{
        margin-bottom: 20px;
    }
    select{
        -webkit-box-shadow: 5px 5px 5px #666;
        -moz-box-shadow: 5px 5px 5px #666;
        box-shadow: 5px 5px 5px #666;
        display:inline-block;
    }
    input{
        -webkit-box-shadow: 5px 5px 5px #666;
        -moz-box-shadow: 5px 5px 5px #666;
        box-shadow: 5px 5px 5px #666;
    }
    </style> --}}
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Design Portfolio | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <a href="{{ route('design-draft.index') }}" class="btn btn-success"><i class="fa fa-upload"></i> Draft</a>
                        <a  href="{{action('DesignController@Create')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add Design</a>
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-rounded" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ $message }}
                </div>
            @endif
            <div class="card">
                <form action="{{url('/design/show')}}" method="POST" accept-charset="utf-8">
                @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Application / Registration No.</label>
                                    <select name="sapp_no" id="" class="form-control">
                                        <option value="">Select Application Number</option>
                                        @foreach ($app_no as $item)
                                            <option value="{{ $item->app_no }}">{{ $item->app_no }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nature of the Applicant :</label>
                                    <select name="natappli_id" class="form-control">
                                        <option value="" >Select Nature of Applicant</option>
                                        @foreach($noapp as $application)
                                        <option value="{{$application->natappli_id}}">{{$application->natappli->application_field}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Applicant Name :</label>
                                    <select name="nameofap" class="form-control">
                                        <option value="" >Select Applicant Name</option>
                                        @foreach($appl_na as $applicant)
                                        @if ($applicant->name_of_applicant !='')
                                        <option value="{{$applicant->name_of_applicant}}">{{$applicant->name_of_applicant}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Entity Name:</label>
                                    <select name="ent_id" class="form-control">
                                        <option value="" >Select Entity Name</option>
                                        @foreach($entity as $ent)
                                            @if($ent->ent_id !='')
                                                <option value="{{$ent->ent_id}}">{{$ent->ent->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nature of Entity :</label>
                                    <select name="natent_id" class="form-control">
                                        <option value="" >Select Nature of Entity</option>
                                        @foreach($natent as $natureapplicant)
                                            @if($natureapplicant->natent_id != '')
                                                <option value="{{$natureapplicant->natent_id}}">{{$natureapplicant->natent->nature_of_applicant}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Agent Name :</label>
                                    <select name="agent_id" class="form-control">
                                        <option value="">Select Agent Name</option>
                                        @foreach($agent_name as $agent)
                                        @if($agent->agent_id != '')
                                        <option value="{{$agent->agent_id}}">{{$agent->agent->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Status :</label>
                                    <select name="design_status" class="form-control" >
                                        <option value="">Select Status</option>
                                        @foreach ($status as $item)
                                            <option value="{{ $item->status }}">{{ $item->status }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>From Date :</label>
                                    <div class="input-group">
                                        <input type="date" name="from" class="form-control">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>To Date :</label>
                                    <div class="input-group">
                                        <input type="date" name="to" class="form-control">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-success">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <p id="demo" align="center"></p>
                        <table  id="myTable" class="table table-bordered"  data-page-length='50'>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Application / Design Registration No.</th>
                                    <th>Date of filing</th>
                                    <th>Name of Article</th>
                                    <th>Name of the Applicant</th>
                                    <th>Class</th>
                                    <th>Best Image</th>
                                    <th>Agent/Attorney</th>
                                    <th>status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            {{-- @if(isset($results))
                                @foreach($results as $result)
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <a href="" type="edit" class="btn btn-warning">Edit</a><br><br>
                                            <a href="" class="btn btn-info">Update</a><br><br>
                                            <a href="" class="btn btn-primary" href="">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else --}}
                                @foreach($showes as $show)
                                {{-- @if($show->status == 0) --}}
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $show->app_no }}</td>
                                        <td>{{ date('d-m-Y',strtotime($show->filing_date))  }}</td>
                                        <td>{{ $show->article_name }}</td>
                                        <td>{{ $show->name_of_applicant }}</td>
                                        <td>{{ $show->classg->name }}</td>
                                        <td><img src="{{asset('public/copyright/design/'.$show->image_t)}}" height="20px"></td>
                                        <td>
                                            @if($show->agent_id !='')
                                            {{ $show->agent->name }}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($show->status == 'Active Application')
                                            {{ $show->status.' -> '.$show->sub_status }}
                                            @else
                                            {{ $show->status }}
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ url('/design/'.$show->id.'/edit') }}" type="edit" class="btn btn-warning">Edit</a>
                                            <a href="{{ url('/design/'.$show->id.'/update_status') }}" class="btn btn-info">Update</a>
                                            <a href="{{ url('/design/'.$show->id.'/view') }}" class="btn btn-primary" href="">View</a>
                                        </td>
                                    </tr>
                                {{--  @endif --}}
                                @endforeach
                            {{--                   @endif --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var x = document.getElementById("myTable").rows.length - 1;
        document.getElementById("demo").innerHTML = "Total Records are " + x;
    </script>
    <script type="text/javascript">
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping

        });
  </script>
@include('include.footer')
