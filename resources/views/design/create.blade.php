@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Add Design | <a href="{{action('DesignController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Add Design</li>
                        </ol>
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="card">
                <div class="card-body">

                    {!! Form::open(['url' => '/design/save','method' => 'POST','data-parsley-validate','files' => true]) !!}
                        <div class="row">
                            <div class="col-md-4 form-group">
                                {!! Form::label('prop_code', 'Proprieter Code*:') !!}
                                {!! Form::text('prop_code', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('govt_fee', 'Govt. Fee*:') !!}
                                {!! Form::text('govt_fee', null, ['class' => 'form-control','required' => '','data-parsley-type' => 'number']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('app_no', 'Application / Design Registration Number :*:') !!}
                                {!! Form::text('app_no', null, ['class' => 'form-control','required' => '']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('filing_date', 'Date of Filing *:') !!}
                                {!! Form::text('filing_date', null, ['class' => 'form-control','required' => '','id' => 'designd']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('app_type', 'Type Application *:') !!}
                                {!! Form::select('app_type', ['Online' => 'Online','Offline' => 'Offline'], null, ['class' => 'form-control','placeholder' => 'Select']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('user_id', 'User *:') !!}
                                {!! Form::select('user_id', $ar2, null, ['class' => 'form-control','placeholder' => 'Select','id' => 'userID','required' => '']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('name_of_applicant', 'Name of Applicant :') !!}
                                {!! Form::text('name_of_applicant', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('natappli_id', 'Nature of the Applicant *:') !!}
                                {!! Form::select('natappli_id', $ar3, null, ['class' => 'form-control','placeholder' => 'Select','required' => '']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {{ Form::label('ent_id','Entity Name:') }}
                                {{ Form::select('ent_id',$ar4,null,['class' => 'form-control', 'id' => 'entid', 'placeholder' => 'Select']) }}
                            </div>
                            <div class="col-md-4 form-group">
                                {{ Form::label('natent_id','Nature of Entity:') }}
                                <select name="natent_id" class="form-control" id="natid">
                                    <option value="">Select</option>
                                </select>
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('article_name', 'Name of Article * :') !!}
                                {!! Form::text('article_name', null, ['class' => 'form-control','required' => '']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('image_t', 'Image (Image Like Trademark Logo) * :') !!}
                                {!! Form::file('image_t', ['class' => 'form-control','required' => '']) !!}
                            </div>
                            <div class="col-md-12 form-group">
                                {!! Form::label('description', 'Description * :') !!}
                                {!! Form::textarea('description',null, ['class' => 'form-control','required' => '']) !!}
                            </div>
                            <div class="col-md-3 form-group">
                                {!! Form::label('classg_id', 'Class *:') !!}
                                {!! Form::select('classg_id', $ar6, null, ['class' => 'form-control','placeholder' => 'Select','required' => '']) !!}
                            </div>
                            <div class="col-md-3 form-group">
                                {!! Form::label('image', 'Representation :') !!}
                                {!! Form::file('image', ['class' => 'form-control','accept' => 'application/pdf']) !!}
                            </div>
                            <div class="col-md-3 form-group">
                                {!! Form::label('lang_id', 'Language *:') !!}
                                {!! Form::select('lang_id', $ar7, null, ['class' => 'form-control','placeholder' => 'Select','required' => '']) !!}
                            </div>
                            <div class="col-md-3 form-group">
                                {!! Form::label('remarks', 'Remarks :') !!}
                                {!! Form::text('remarks', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-md-12">
                                <hr><h4 class="text-center">Agent / Attorney Info (if any)</h4><hr>
                              </div>
                              <div class="col-md-4 form-group">
                                {{ Form::label('agent_id','Name:') }}
                                {{ Form::select('agent_id',$ar1,null,['class' => 'form-control','id' => 'agent_id','placeholder' => 'Select']) }}
                              </div>
                              <div class="col-md-4 form-group">
                                  {{ Form::label('agent_code','Agent / Attorney Code:') }}
                                  {{ Form::text('agent_code',null,['class' => 'form-control','id' => 'agent_code']) }}
                              </div>
                              <div class="col-md-4 form-group">
                                  {!! Form::label('poa', 'Power of Attorney :') !!}
                                  {!! Form::file('poa', ['class' => 'form-control']) !!}
                              </div>
                            <div class="col-md-12">
                                <hr><h4 class="text-center">Details of Use</h4><hr>
                            </div>
                            <div class="col-md-12 form-group">
                                {!! Form::select('details_use', ['Proposed to be Used' => 'Proposed to be Used','Used since' => 'Used since'], null, ['class' => 'form-control', 'id' => 'useofde' ,'placeholder' => 'Select']) !!}
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="dispuse" style="display: none">
                                    <div class="col-md-6 form-group">
                                        {!! Form::label('datea', 'Date') !!}
                                        {!! Form::text('datea', null, ['class' => 'form-control','id' => 'dataap']) !!}
                                    </div>
                                    <div class="col-md-6 form-group">
                                        {!! Form::label('user_aff', 'Upload User Affidavit') !!}
                                        {!! Form::file('user_aff', ['class' => 'form-control','accept' => 'application/pdf']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr><h4 align="center">Detail Of The Person Submitting The Application</h4><hr>
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('name', 'Name :') !!}
                                {!! Form::text('name', null, ['class' => 'form-control','id' => 'namein']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('designation', 'Designation :') !!}
                                {!! Form::text('designation', null, ['class' => 'form-control','id' => 'designation']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('copy_application', 'Copy Of Application :') !!}
                                {!! Form::file('copy_application', ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <input type="submit" name="draft" value="Save as Draft" class="btn btn-info">
                                    <input type="submit" name="submit" value="Submit" class="btn btn-success">
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/bootstrap.min.js"></script>
    <script>
        $(function() {
            $('#designd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#dataap').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });
        });
    </script>
    <script>
        $(document).ready(function(){
            $('#useofde').on('change',function(){
                var useofde = $(this).val();
                if(useofde == 'Used since'){
                    $('#dispuse').show();
                }
                if(useofde !='Used since'){
                    $('#dispuse').hide();
                }
            });

            $(document).on('change','#userID',function(){
                var userid = $(this).val();
                $('#namein').attr('value','');
                $('#designation').attr('value','');
                $.ajax({
                  type:'get',
                  url:'{!! URL::to('finduser') !!}',
                  data:{'id':userid},
                  success:function(data){
                    $('#namein').attr('value',data.first_name);
                    $('#designation').attr('value',data.designation);
                  }
                });
            });

            $('#agent_id').on('change',function(){
                var agent_id = $(this).val();
                $('#agent_code').attr('value','');
                $('#agent_address').attr('value','');
                $.ajax({
                    type:'get',
                    url:'{!! URL::to('findagent') !!}',
                    data:{'id':agent_id},
                    dataType:'json',
                    success:function(data){
                      $('#agent_code').attr('value',data.registration_no);
                      $('#agent_address').attr('value',data.address);
                  },
                });
            });

            $(document).on('change','#entid',function(){
                var entids = $(this).val();
                $('#natid').find('option').not(':first').remove();
                $.ajax({
                  type:'get',
                  url:'{!! URL::to('findnat') !!}',
                  data:{'id':entids},
                  success:function(data){
                      var id = data['id'];
                      var name = data['nature_of_applicant'];
                      $('#natid').append("<option value='"+id+"' selected>"+name+"</option>");
                  }
                });
            });
        });
    </script>
@include('include.footer')
