@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Upload Registration | <a href="{{ url('/design/'.$post->id.'/view') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Add Design</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                {!! Form::open(['url' => ['design/regist',$post->id],'method' => 'POST','data-parsley-validate' => '','files' => true]) !!}
                <div class="row">
                    <div class="col-md-12 form-group">
                        {!! Form::label('image', 'Registration:') !!}
                        {!! Form::file('image', ['class' => 'form-control','required' => '','accept' => 'application/pdf']) !!}
                    </div>
                    <div class="col-md-12 form-group">
                        {!! Form::submit('submit', ['class' => 'btn btn-success']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@include('include.footer')
