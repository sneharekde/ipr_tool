@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Enforcement | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Enforcement</li>
                        </ol>
                        <a  href="{{action('AddBrandController@EnforceCreate')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add Enforcement</a>
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-rounded" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ $message }}
                </div>
            @endif
            <div class="card">
                <form action="{{url('/enforce/show')}}" method="POST" accept-charset="utf-8">
                  @csrf
                    <div class="form-body">
                        <div class="card-body">
                        <div class="row pt-3">
                            <div class="col-md-4">
                            <div class="form-group">
                                <label>Entity</label>
                                <select name="entity" class="form-control">
                                <option value="">--Select--</option>

                                @foreach($enitys as $ent)
                                @if ($ent->ent_id !='')
                                <option value="{{ $ent->ent_id }}" @if($ent->ent_id == $entity) selected @endif>{{ $ent->ent->name }}</option>
                                @endif

                                @endforeach
                                </select>
                            </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                <label>Country</label>
                                <select name="country" class="form-control">
                                <option value="">--Select--</option>
                                @foreach($countries as $countr)
                                <option value="{{ $countr->country_id }}" @if($countr->country_id == $country) selected @endif>{{ $countr->country->name }}</option>
                                @endforeach
                                </select>
                            </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                <label>State</label>
                                <select name="state" class="form-control">
                                <option value="">--Select--</option>
                                @foreach($states as $show)
                                <option value="{{ $show->state_id }}" @if($show->state_id == $state) selected @endif>{{ $show->state->name }}</option>
                                @endforeach
                                </select>
                            </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                <label>City / Location</label>
                                <select name="city" class="form-control">
                                <option value="">--Select--</option>
                                @foreach($cities as $show)
                                <option value="{{ $show->city_id }}" @if($show->city_id == $city) selected @endif>{{ $show->city->city }}</option>
                                @endforeach
                                </select>
                            </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                <label>Category Of Target</label>
                                <select name="target" class="form-control">
                                <option value="">--Select--</option>
                                @foreach($targets as $targe)
                                <option value="{{ $targe->target }}" @if($targe->target == $target) selected @endif>{{ $targe->target }}</option>
                                @endforeach
                                </select>
                            </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                <label>TM Application No.</label>
                                <select name="tm_app" class="form-control">
                                <option value="">--Select--</option>
                                @foreach($tm_apps as $target)
                                @if ($target->tm_app !='')
                                <option value="{{ $target->tm_app }}" @if($target->tm_app == $tm_app) selected @endif>{{ $target->tm_app }}</option>
                                @endif
                                @endforeach
                                </select>
                            </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                            <label>From Date :</label>
                                <div class="input-group">
                                <input type="date" name="from" class="form-control" value="{{ $from_date }}">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="icon-calender"></i></span>
                                </div>
                                </div>
                            </div>
                            </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label>To Date :</label>
                            <div class="input-group">
                                <input type="date" name="to" class="form-control" value="{{ $to_date }}">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="icon-calender"></i></span>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-12">
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success">Search</button>
                        </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div class="table-responsive">
                      <p id="demo" align="center"></p>
                      <table id="myTable" class="table table-bordered"  data-page-length='50'>
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Target Name</th>
                            <th>Owner Name</th>
                            <th>Location</th>
                            <th>Category of target</th>
                            <th>Name of product</th>
                            <th>Quantity</th>
                            <th>Amount</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if(isset($results))
                          @foreach($results as $result)
                            <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{$result->target_name}}</td>
                            <td>{{$result->owner}}</td>
                            <td>{{$result->country->name.'-'.$result->state->name.'-'.$result->city->city}}</td>
                            <td>{{$result->target}}</td>
                            <td>{{$result->nop->name}}</td>
                            <td>{{$result->qty}}</td>
                            <td>{{$result->amt}}</td>
                            <td>
                              <a href="{{url('edit_enforce/'.$result->id)}}" type="edit" class="btn btn-warning">Edit</a>
                              <a href="{{ url('view_enforce/'.$result->id) }}" class="btn btn-primary">View</a>
                              <a href="{{url('/expensen/'.$result->id.'/expenses')}}" class="btn btn-success">Fees / Expenses</a>
                              <a href="{{url('fir_details/'.$result->id)}}" class="btn btn-danger">FIR Details</a>
                            </td>
                          </tr>
                          @endforeach
                          @else
                          @foreach($shows as $show)
                          <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{$show->target_name}}</td>
                            <td>{{$show->owner}}</td>
                            <td>{{$show->country->name.' - '.$show->state->name.'-'.$show->city}}</td>
                            <td>{{$show->target}}</td>
                            <td>{{$show->nop}}</td>
                            <td>{{$show->qty}}</td>
                            <td>{{$show->amt}}</td>
                            <td>
                              <a href="{{url('edit_enforce/'.$show->id)}}" type="edit" class="btn btn-warning">Edit</a>
                              <a href="{{ url('view_enforce/'.$show->id) }}" class="btn btn-primary">View</a>
                              <a href="{{url('/expensen/'.$show->id.'/expenses')}}" class="btn btn-success">Fees / Expenses</a>
                              <a href="{{url('fir_details/'.$show->id)}}" class="btn btn-danger">FIR Details</a>
                            </td>
                          </tr>
                          @endforeach
                          @endif
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
        </div>
    </div>

    <script type="text/javascript">
    var x = document.getElementById("myTable").rows.length - 1;
    document.getElementById("demo").innerHTML = "Total Records are " + x;
  </script>

  <script type="text/javascript">
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping

        });

  </script>
@include('include.footer')
