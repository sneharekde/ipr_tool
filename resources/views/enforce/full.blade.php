@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>

   <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Show Enforcement | <a href="{{action('AddBrandController@EnforceShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Show Enforcement</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12" id="content">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Details</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Expenses</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">FIR</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <table id="example23" class="table table-bordered">
                                    <tr>
                                        <th width="400px" align="left">Entity :</th>
                                        <td>
                                            @if ($enforce->ent_id !='')
                                                {{ $enforce->ent->name}}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="400px" align="left">Country :</th>
                                        <td>{{ $enforce->country->name }}</td>
                                    </tr>
                                    <tr>
                                        <th width="400px" align="left">State :</th>
                                        <td>{{ $enforce->state->name }}</td>
                                    </tr>
                                    <tr>
                                        <th width="400px" align="left">City:</th>
                                        <td>{{ $enforce->city->city }}</td>
                                    </tr>
                                    <tr>
                                        <th width="400px" align="left">Target Name:</th>
                                        <td>{{ $enforce->target_name }}</td>
                                    </tr>
                                    <tr>
                                        <th width="400px" align="left">Owner Name:</th>
                                        <td>{{ $enforce->owner }}</td>
                                    </tr>
                                    <tr>
                                        <th width="400px" align="left">Name of Product:</th>
                                        <td>{{ $enforce->nop->name }}</td>
                                    </tr>
                                    <tr>
                                        <th width="400px" align="left">Quantity:</th>
                                        <td>{{ $enforce->qty }}</td>
                                    </tr>
                                    <tr>
                                        <th width="400px" align="left">Amount:</th>
                                        <td>{{ $enforce->amt }}</td>
                                    </tr>
                                    <tr>
                                        <th width="400px" align="left">TM Application No.:</th>
                                        <td>{{ $enforce->tm_app }}</td>
                                    </tr>
                                    <tr>
                                        <th width="400px" align="left">Category Of Target:</th>
                                        <td>{{ $enforce->target }}</td>
                                    </tr>
                                    <tr>
                                        <th width="400px" align="left">Docs :</th>
                                        <td>
                                            @foreach(explode(',', $enforce->docs) as $file)
                                                @if ($file !='')
                                                    <a href="{{ url('public/document',$file) }}" download="">{{ $file }}</a>
                                                @endif
                                            @endforeach
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Invoice No</th>
                                                <th>Amount</th>
                                                <th>Payment Status</th>
                                                <th>Document</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($expens as $item)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $item->invoice_no }}</td>
                                                    <td>{{ $item->amount }}</td>
                                                    <td>{{ $item->pay_stat }}</td>
                                                    <td>
                                                        @if ($item->upload !='')
                                                            @foreach (explode(',',$item->upload) as $file)
                                                                <a href="{{ url('public/document',$file) }}" download="">{{ $file }}</a>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>FIR Date</th>
                                                <th>FIR No.</th>
                                                <th>Police Station</th>
                                                <th>Legislation</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1;  ?>
                                            @foreach ($firs as $item)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ date("d-m-Y",strtotime($item->fir_date)) }}</td>
                                                    <td>{{ $item->fir_no }}</td>
                                                    <td>{{ $item->police }}</td>
                                                    <td>{{ $item->nof }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
@include('include.footer')
