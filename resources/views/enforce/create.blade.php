@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
  <div class="page-wrapper">
    <div class="container-fluid">
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Add Enforcement | <a href="{{action('AddBrandController@EnforceShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Add Enforcement</li>
                </ol>
            </div>
        </div>
      </div>
      <div class="card">
        <div class="card-body">



          <form action="{{url('/enforce/save')}}" method="POST" data-parsley-validate="" enctype="multipart/form-data" accept-charset="utf-8">
            @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group {{ $errors->has('entity') ? 'has-error' : '' }}">
                  <label>Entity</label>
                  <select name="entity"  class="form-control" id="entid" required="">
                    <option value="">--Select--</option>
                    @foreach($enitys as $ent)
                    <option value="{{$ent->id}}">{{$ent->name}}</option>
                    @endforeach
                  </select>
                  <span class="text-danger">{{ $errors->first('entity') }}</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group {{ $errors->has('tname') ? 'has-error' : '' }}">
                  <label>Target Name</label>
                  <input type="text" name="tname" value="{{old('tname')}}" class="form-control" required="">
                  <span class="text-danger">{{ $errors->first('tname') }}</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group {{ $errors->has('oname') ? 'has-error' : '' }}">
                  <label>Owner Name</label>
                  <input type="text" name="oname" value="{{old('oname')}}" class="form-control" required="">
                  <span class="text-danger">{{ $errors->first('oname') }}</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group {{ $errors->has('oname') ? 'has-error' : '' }}">
                  <label>Date</label>
                  <input type="text" id="enfd" name="date"  value="{{old('date')}}" class="form-control" required="">
                  <span class="text-danger">{{ $errors->first('date') }}</span>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
                  <label>Country</label>
                  <select name="country" id="country_id" class="form-control input-lg" data-dependent="state" required="">
                    <option value="">Select Country</option>
                    @foreach ($countries as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                    </select>
                  <span class="text-danger">{{ $errors->first('country') }}</span>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
                  <label>State</label>
                  <select name="state_id" id="state_id" class="form-control input-lg" required="">
                    <option value="">Select Option</option>
                    </select>
                  <span class="text-danger">{{ $errors->first('state') }}</span>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group {{ $errors->has('city_id') ? 'has-error' : '' }}">
                  <label>City</label>
                  <select name="city_id" id="city_id" class="form-control input-lg" required="">
                    <option value="">Select Option</option>
                    </select>
                  <span class="text-danger">{{ $errors->first('city_id') }}</span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group {{ $errors->has('target') ? 'has-error' : '' }}">
                  <label>Category Of Target</label>
                  <select name="target" class="form-control" required="">
                    <option value="">--Select--</option>
                    @foreach($targets as $target)
                    <option value="{{$target->targets}}">{{$target->targets}}</option>
                    @endforeach
                  </select>
                  <span class="text-danger">{{ $errors->first('target') }}</span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Name Of Product</label>
                  <select name="nop_id" id="nopid" class="form-control">
                    <option value="">Select Product</option>
                </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Quantity</label>
                  <input type="text" name="qty" class="form-control">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Amount</label>
                  <input type="text" name="amt" class="form-control">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Documents</label>
                  <input type="file" name="docs[]" class="form-control" multiple>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>tradmark Application Number(Optional)</label>
                  <input type="text" name="tm_app" value="{{old('tm_app')}}"  class="form-control">
                </div>
              </div>
              <div class="col-md-12">
              <div class="form-group text-center">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

{{-- <script type="text/javascript">
  $(document).ready(function() { $("#country").select2(); });
  $(document).ready(function() { $("#state").select2(); });
</script>
 --}}

 <script>
    $(document).ready(function(){
        $('#entid').change(function(){
            var entids = $(this).val();
            $('#nopid').find('option').not(':first').remove();
            $.ajax({
                url:'{!! URL::to('findprod') !!}',
                type:'GET',
                data:{'id':entids},
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                dataType:'json',
                success:function(data){
                //    console.log(data);
                var leng = data.length;
                    for(var i=0; i<leng; i++){
                        var id = data[i]['id'];
                        var name = data[i]['name'];
                        $('#nopid').append("<option value='"+id+"'>"+name+"</option>");
                    }
                }
            });
        });
    });
</script>

<script>
    $(document).ready(function(){
        $('#country_id').change(function(){
            var country_ids = $(this).val();
            console.log(country_ids);
            $('#state_id').find('option').not(':first').remove();
            $('#city_id').find('option').not(':first').remove();
            $.ajax({
                url:'{!! URL::to('findstate') !!}',
                type:'GET',
                data:{'id':country_ids},
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                dataType:'json',
                success:function(data){
                //    console.log(data);
                var leng = data.length;
                    for(var i=0; i<leng; i++){
                        var id = data[i]['id'];
                        var name = data[i]['name'];
                        $('#state_id').append("<option value='"+id+"'>"+name+"</option>");
                    }
                }
            });
        });

        $('#state_id').change(function(){
            var state_ids = $(this).val();
            console.log(state_ids);
            $('#city_id').find('option').not(':first').remove();
            $.ajax({
                url:'{!! URL::to('findcity') !!}',
                type:'GET',
                data:{'id':state_ids},
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                dataType:'json',
                success:function(data){
                //    console.log(data);
                var leng = data.length;
                    for(var i=0; i<leng; i++){
                        var id = data[i]['id'];
                        var name = data[i]['city'];
                        $('#city_id').append("<option value='"+id+"'>"+name+"</option>");
                    }
                }
            });
        });
    });
</script>
<script>
    $(function() {
        $('#enfd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,

        });
    });
</script>
@include('include.footer')

