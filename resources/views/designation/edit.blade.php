@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
   <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
              <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Edit Designation | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Edit Designation</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->

                <div class="row">
                <div class="col-md-12">
                  <div class="card">
                  <div class="card-body">
                    <form action="{{url('designation/update')}}" method="POST" accept-charset="utf-8">
            @csrf
            <input type="hidden" name="id" value="{{  $designation->id  }}">
              <div class="form-group {{ $errors->has('designation') ? 'has-error' : '' }}">
              <label for="">Designation</label>
              <input type="text" class="form-control" name="designation" value="{{ $designation->designation }}">
              <span class="text-danger">{{ $errors->first('designation') }}</span>
            </div>
                      <div class="col-md-12 center"><button type="Submit" class="btn btn-success">Submit</button></div>
                    </form>
                  </div>
                </div>
                </div>

                </div>





            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
  @include('include.footer')
