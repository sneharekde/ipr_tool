@component('mail::message')
#Dear {{ $user->first_name }}
This is Second Reminder Mail.kindly follow up according to this mail.

@component('mail::table')
|Article Name|Application No|Status|Sub Status|Due Date|
|:-------------|:-------------|:-----|:---------|:-------|
@foreach($reminders as $reminder)
| {{ $reminder['design']['article_name'] }} | {{ $reminder['design']['app_no'] }} | {{ $reminder['design']['status'] }} | {{ $reminder['design']['sub_status'] }} | @if($reminder['rep_dead'] !='') {{ date('d-m-Y',strtotime($reminder['rep_dead'])) }} @elseif($reminder['dfh'] != '') {{ date('d-m-Y',strtotime($reminder['dfh'])) }} @elseif($reminder['d_renr'] != '') {{ date('d-m-Y',strtotime($reminder['d_renr'])) }} @endif|
@endforeach

@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
