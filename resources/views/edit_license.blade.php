@include('include.header')
 <div class="page-wrapper">
    <div class="container-fluid">
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Edit License| <a href="{{action('FrontEndController@tradmark_portfolio')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4> 
        </div>
        <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                <li class="breadcrumb-item active">Edit License</li>
            </ol>
          </div>
        </div>
      </div>
      @include('include.message')
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h4>Application No : {{$show->application_no}}</h4><br>
            <form action="{{url('license/update')}}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
            @csrf
              <input type="hidden" name="id" value="{{$show->id}}">
              <div class="from-group">
                <label for="">License</label> 
                <input type="file" class="form-control" name="license" value="{{$show->license}}" required="required" accept="application/pdf">
              </div><br>
              <button type="submit" class="btn btn-primary">Upload</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@include('include.footer')
