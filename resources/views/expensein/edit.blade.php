@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
   <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                        <div class="col-md-5 align-self-center">
                            <h4 class="text-themecolor">Invoice Edit | <a href="{{action('AddBrandController@InvestShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                        </div>
                        <div class="col-md-7 align-self-center text-right">
                            <div class="d-flex justify-content-end align-items-center">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                    <li class="breadcrumb-item active">Invoice</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->

                @if ($message = Session::get('success'))
      <div class="alert alert-success alert-rounded" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{ $message }}
</div>

@endif


               <div class="card">
                  <div class="card-body" id="app">
              <form action="{{url('expensein/edit/submit')}}" method="POST" data-parsley-validate="" accept-charset="utf-8" enctype="multipart/form-data">
                     @csrf
            <div class="row justify-content-center align-items-center">
              <div class="col-md-12">
                <input type="hidden" name="iid" value="{{ $expin->id }}">
                <input type="hidden" name="invest_id" value="{{ $expin->invest_id }}">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="">Invoice No</label>
                            <input type="text" name="invoic" value="{{ $expin->invoice_no }}" class="form-control" required>
                            <span class="text-danger">{{ $errors->first('invoic') }}</span>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Amount</label>
                            <input type="text" name="amount" value="{{ $expin->amount }}"  class="form-control" required>
                            <span class="text-danger">{{ $errors->first('amount') }}</span>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Document</label>
                            @if ($expin->upload !='')
                                @foreach (explode(',',$expin->upload) as $item)
                                    <div class="col-md-12">
                                        <a href="{{ url('public/document',$item) }}" download="">{{ $item }}</a>
                                        <a href="{{ url('delete/expin/'.$item.'/'.$expin->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                    </div>
                                @endforeach
                            @else
                            <input type="file" name="inv_doc[]" class="form-control">
                            @endif

                            <span class="text-danger">{{ $errors->first('inv_doc') }}</span>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Payment Status</label>
                            <select class="form-control" name="pay" id="mode" required>
                                <option value="">--Select--</option>
                                <option value="Paid" @if($expin->pay_stat == 'Paid') selected="selected" @endif>Paid</option>
                                <option value="Unpaid" @if($expin->pay_stat == 'Unpaid') selected="selected" @endif>Unpaid</option>
                            </select>
                        </div>
                    </div>
                    @if ($expin->pay_stat =='Paid')
                        <div id="dis2">
                            <div class="form-group">
                                <label>Payment Date</label>
                                <input type="date" name="pay_date" value="{{ $expin->date }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Remark</label>
                                <input type="text" value="{{ $expin->remarks }}" name="remark" class="form-control">
                            </div>
                        </div>
                    @else
                        <div id="dis">
                            <div class="form-group">
                                <label>Payment Date</label>
                                <input type="date" name="pay_date" value="{{ $expin->date }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Remark</label>
                                <input type="text" value="{{ $expin->remarks }}" name="remark" class="form-control">
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12 text-center"><button type="submit" class="btn btn-success">Add</button></div>
                </div>
            </div>
        </form>
    </div>
        </div>


      </div>

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>

<script type="text/javascript">
  $(function() {
    $('#dis').hide();
    $('#mode').change(function(){
      if ($('#mode').val() == 'Paid') {
          $('#dis2').show();
        $('#dis').show();
      }else{
        $('#dis').hide();
        $('#dis2').hide();
      }
    });
  });
</script>

  @include('include.footer')
