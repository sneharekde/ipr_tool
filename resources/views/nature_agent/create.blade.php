@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Nature of Agent | <a href="{{action('AddTradmarkController@NatureAgentShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active">Nature of Agent</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    {!! Form::open(['url' => '/nature_agent/save','method' => 'POST','data-parsley-validate' => '']) !!}
                        <div class="row">
                            <div class="form-group col-md-12">
                                {!! Form::label('nature_of_agent', 'Nature of Agent') !!}
                                {!! Form::text('nature_of_agent', null, ['class' => 'form-control','required' => '']) !!}
                            </div>
                            <div class="col-md-12 form-group">
                                {!! Form::submit('submit', ['class' => 'btn btn-success']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
