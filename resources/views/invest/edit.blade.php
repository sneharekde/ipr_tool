@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Edit Investigation | <a href="{{action('AddBrandController@InvestShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Edit Investigation</li>
                        </ol>
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="card">
                <div class="card-body">
                    <form action="{{url('/invest/update')}}" data-parsley-validate="" method="POST" accept-charset="utf-8" data-parsley-validate="" enctype="multipart/form-data">
                    @csrf
                        <input type="hidden" name="id" value="{{$invest->id}}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('entity') ? 'has-error' : '' }}">
                                    <label>Entity</label>
                                    <select name="entity" id="entid"  class="form-control" required="">
                                        <option value="">--Select--</option>
                                        @foreach($enitys as $ent)
                                        <option value="{{$ent->id}}" @if($invest->ent_id == $ent->id) selected="selected" @endif>{{$ent->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger">{{ $errors->first('entity') }}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('inag') ? 'has-error' : '' }}">
                                    <label>Investigation Agency</label>
                                    <select name="inag"  class="form-control" required="">
                                        <option value="">--Select--</option>
                                        @foreach($agnecis as $agency)
                                        <option value="{{ $agency->id }}" @if($invest->inag_id == $agency->id) selected="selected" @endif>{{ $agency->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger">{{ $errors->first('inag') }}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('brandp') ? 'has-error' : '' }}">
                                    <label>Brand Protection Manager</label>
                                    <select name="brandp"  class="form-control" required>
                                        <option value="">--Select--</option>
                                        @foreach($users as $user)
                                        <option value="{{ $user->id }}" @if($invest->user_id == $user->id) selected="selected" @endif>{{ $user->user_name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger">{{ $errors->first('brandp') }}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
                                    <label>Country</label>
                                    <select name="country" id="country_id" class="form-control input-lg dynamic" data-dependent="state" required>
                                        <option value="">Select Country</option>
                                        @foreach($countries as $item)
                                        <option value="{{ $item->id }}" @if($invest->country_id == $item->id) selected="selected" @endif>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger">{{ $errors->first('country') }}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
                                    <label>State</label>
                                    <select name="state" id="state_id" class="form-control input-lg dynamic" required>
                                        <option value="">Select Option</option>
                                        @foreach ($states as $item)
                                        <option value="{{ $item->id }}" @if($invest->state_id == $item->id) selected="selected" @endif>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger">{{ $errors->first('state') }}</span>
                                </div>
                                {{ csrf_field() }}
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('city_id') ? 'has-error' : '' }}">
                                    <label>City</label>
                                    <select name="city_id" id="city_id" class="form-control input-lg" required="">
                                        <option value="">Select Option</option>
                                        @foreach ($cities as $item)
                                        <option value="{{ $item->id }}" @if($invest->city_id == $item->id) selected="selected" @endif>{{ $item->city }}</option>
                                        @endforeach
                                        </select>
                                      <span class="text-danger">{{ $errors->first('city_id') }}</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('target') ? 'has-error' : '' }}">
                                    <label>Category Of Target</label>
                                    <select name="target" id="target" class="form-control" required>
                                        <option value="">--Select--</option>
                                        @foreach($targets as $target)
                                        <option value="{{$target->targets}}" @if($invest->target == $target->targets) selected="selected" @endif>{{$target->targets}}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger">{{ $errors->first('target') }}</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('prod') ? 'has-error' : '' }}">
                                    <label>Name Of Product</label>
                                    <select name="prod" id="nopid" class="form-control">
                                        <option value="">Select Product</option>
                                        @foreach ($products as $item)
                                            <option value="{{ $item->id }}" @if($item->id == $invest->nop_id) selected @endif>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger">{{ $errors->first('prod') }}</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('dsi') ? 'has-error' : '' }}">
                                    <label>Start Date Of Investigation</label>
                                    <input type="text" name="dsi"  value="{{ $invest->dsi }}"  id="startd" class="form-control" required>
                                    <span class="text-danger">{{ $errors->first('dsi') }}</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('dei') ? 'has-error' : '' }}">
                                    <label>End Date Of Investigation</label>
                                    <input type="text" name="dei" id="endd" value="{{ $invest->dei }}" class="form-control" required>
                                    <span class="text-danger">{{ $errors->first('dei') }}</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Documents</label>
                                    @if ($invest->docs !='')
                                        @foreach (explode(",",$invest->docs) as $item)
                                            <div class="col-md-6">
                                                <a href="{{ url('public/Scan_Copy_Of_Application/'.$item) }}" download="">{{ $item }}</a>
                                                <a href="{{ url('imagi/delete/'.$item.'/'.$invest->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                            </div>
                                        @endforeach
                                    @else
                                        <input type="file" name="docs[]" class="form-control" multiple>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>tradmark Application Number(Optional)</label>
                                    <input type="text" name="tm_app" value="{{ $invest->tm_app }}"  class="form-control">
                                </div>
                            </div>

                        </div>
                        @if ($invest->nota !='' && $invest->n_addr !='')
                        <div class="row">
                            <div class="col-md-4">
                                @foreach (explode(':',$invest->nota) as $not)
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name of Target</label>
                                        <input type="text" name="nota[]" value="{{ $not }}"  class="form-control">
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="col-md-8">
                                @foreach (explode(':',$invest->n_addr) as $adr)
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" name="tar_add[]" value="{{ $adr }}"  class="form-control">
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @else
                        <div class="table-responsive">
                            <table class="table table-bordered" id="item_table">
                                <tr class="">
                                    <th>Name of Target</th>
                                    <th>Address</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td><input type="text" name="nota[]" class="form-control nota"></td>
                                    <td><input type="text" name="tar_add[]" class="form-control tar_add"></td>
                                    <td><button type="button" name="add" class="btn btn-sm btn-success add">Add</button></td>
                                </tr>
                            </table>
                        </div>
                        @endif


              <div class="col-md-12">
              <div class="form-group text-center">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>




<script>
        $(document).ready(function(){
            $('#entid').change(function(){
                var entids = $(this).val();
                $('#nopid').find('option').not(':first').remove();
                $.ajax({
                    url:'{!! URL::to('findprod') !!}',
                    type:'GET',
                    data:{'id':entids},
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    dataType:'json',
                    success:function(data){
                    //    console.log(data);
                    var leng = data.length;
                        for(var i=0; i<leng; i++){
                            var id = data[i]['id'];
                            var name = data[i]['name'];
                            $('#nopid').append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            });
        });
    </script>
<script>
    $(document).ready(function(){
        $('#country_id').change(function(){
            var country_ids = $(this).val();
            console.log(country_ids);
            $('#state_id').find('option').not(':first').remove();
            $('#city_id').find('option').not(':first').remove();
            $.ajax({
                url:'{!! URL::to('findstate') !!}',
                type:'GET',
                data:{'id':country_ids},
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                dataType:'json',
                success:function(data){
                //    console.log(data);
                var leng = data.length;
                    for(var i=0; i<leng; i++){
                        var id = data[i]['id'];
                        var name = data[i]['name'];
                        $('#state_id').append("<option value='"+id+"'>"+name+"</option>");
                    }
                }
            });
        });

        $('#state_id').change(function(){
            var state_ids = $(this).val();
            console.log(state_ids);
            $('#city_id').find('option').not(':first').remove();
            $.ajax({
                url:'{!! URL::to('findcity') !!}',
                type:'GET',
                data:{'id':state_ids},
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                dataType:'json',
                success:function(data){
                //    console.log(data);
                var leng = data.length;
                    for(var i=0; i<leng; i++){
                        var id = data[i]['id'];
                        var name = data[i]['city'];
                        $('#city_id').append("<option value='"+id+"'>"+name+"</option>");
                    }
                }
            });
        });
    });
</script>
<script>
    $(document).ready(function(){
        $(document).on('click','.add',function(){
            var html = '';
            html += '<tr>';
            html += '<td><input type="text" name="nota[]" class="form-control nota"></td>';
            html += '<td><input type="text" name="tar_add[]" class="form-control tar_add"></td>';
            html += '<td><button type="button" name="remove" class="btn btn-sm btn-danger remove">Remove</button></td></tr>';
            $('#item_table').append(html);
        });
        $(document).on('click','.remove',function(){
            $(this).closest('tr').remove();
        });
    });
</script>
<script>
    $(function() {
        $('#startd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,

        });
        $('#endd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
        });
    });
</script>

@include('include.footer')

