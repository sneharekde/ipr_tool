@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Add Investigation | <a href="{{action('AddBrandController@InvestShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Add Investigation</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">

                    {!! Form::open(['url' => '/invest/save','method' => 'POST','data-parsley-validate' => '','files' => true]) !!}
                        <div class="row">
                            <div class="col-md-4 form-group">
                                {!! Form::label('ent_id', 'Entity Name:') !!}
                                {!! Form::select('ent_id', $ar1, null, ['class' => 'form-control','id' => 'entid','required' => '','placeholder' => 'Select Entity']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('inag_id', 'Investigation Agency:') !!}
                                {!! Form::select('inag_id', $ar2, null, ['class' => 'form-control','required' => '','placeholder' => 'Select Investigation Agency']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('user_id', 'Brand Protection Manager:') !!}
                                {!! Form::select('user_id', $ar3, null, ['class' => 'form-control','required' => '','placeholder' => 'Select Brand Protection Manager']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Country</label>
                                <select name="country" id="country_id" class="form-control input-lg dynamic" data-dependent="state" required="">
                                    <option value="">Select Country</option>
                                    @foreach ($countries as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('country') }}</span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>State</label>
                                <select name="state" id="state_id" class="form-control input-lg dynamic" required="">
                                    <option value="">Select Option</option>
                                </select>
                                <span class="text-danger">{{ $errors->first('state') }}</span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>City</label>
                                <select name="city_id" id="city_id" class="form-control input-lg" required="">
                                    <option value="">Select Option</option>
                                </select>
                                <span class="text-danger">{{ $errors->first('city_id') }}</span>
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('target', 'Category Of Target:') !!}
                                {!! Form::select('target', $ar4, null, ['class' => 'form-control','required' => '','placeholder' => 'Select Category Of Target']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('nop_id', 'Name of Product:') !!}
                                <select name="nop_id" id="nopid" class="form-control">
                                    <option value="">Select Product</option>
                                </select>
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('dsi', 'Start Date Of Investigation:') !!}
                                {!! Form::text('dsi', null, ['class' => 'form-control','required' => '','id' => 'startd']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('dei', 'End Date Of Investigation:') !!}
                                {!! Form::text('dei', null, ['class' => 'form-control','required' => '','id' => 'endd']) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('docs', 'Documents:') !!}
                                {!! Form::file('docs[]', ['class' => 'form-control','multiple' => true]) !!}
                            </div>
                            <div class="col-md-4 form-group">
                                {!! Form::label('tm_app', 'Trademark Application Number:') !!}
                                {!! Form::text('tm_app', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="item_table">
                                    <tr class="">
                                        <th>Name of Target</th>
                                        <th>Address</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td><input type="text" name="nota[]" class="form-control nota"></td>
                                        <td><input type="text" name="tar_add[]" class="form-control tar_add"></td>
                                        <td><button type="button" name="add" class="btn btn-sm btn-success add">Add</button></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-12 form-group text-center">
                                {!! Form::submit('submit', ['class' => 'btn btn-success']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('#entid').change(function(){
                var entids = $(this).val();
                $('#nopid').find('option').not(':first').remove();
                $.ajax({
                    url:'{!! URL::to('findprod') !!}',
                    type:'GET',
                    data:{'id':entids},
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    dataType:'json',
                    success:function(data){
                    var leng = data.length;
                        for(var i=0; i<leng; i++){
                            var id = data[i]['id'];
                            var name = data[i]['name'];
                            $('#nopid').append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            });
        });
    </script>
    <script>
        $(document).ready(function(){
            $('#country_id').change(function(){
                var country_ids = $(this).val();
                console.log(country_ids);
                $('#state_id').find('option').not(':first').remove();
                $('#city_id').find('option').not(':first').remove();
                $.ajax({
                    url:'{!! URL::to('findstate') !!}',
                    type:'GET',
                    data:{'id':country_ids},
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    dataType:'json',
                    success:function(data){
                    //    console.log(data);
                    var leng = data.length;
                        for(var i=0; i<leng; i++){
                            var id = data[i]['id'];
                            var name = data[i]['name'];
                            $('#state_id').append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            });

            $('#state_id').change(function(){
                var state_ids = $(this).val();
                $('#city_id').find('option').not(':first').remove();
                $.ajax({
                    url:'{!! URL::to('findcity') !!}',
                    type:'GET',
                    data:{'id':state_ids},
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    dataType:'json',
                    success:function(data){
                    var leng = data.length;
                        for(var i=0; i<leng; i++){
                            var id = data[i]['id'];
                            var name = data[i]['city'];
                            $('#city_id').append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            });
        });
    </script>
    <script>
        $(document).ready(function(){
            $(document).on('click','.add',function(){
                var html = '';
                html += '<tr>';
                html += '<td><input type="text" name="nota[]" class="form-control nota"></td>';
                html += '<td><input type="text" name="tar_add[]" class="form-control tar_add"></td>';
                html += '<td><button type="button" name="remove" class="btn btn-sm btn-danger remove">Remove</button></td></tr>';
                $('#item_table').append(html);
            });
            $(document).on('click','.remove',function(){
                $(this).closest('tr').remove();
            });
        });
    </script>
    <script>
        $(function() {
            $('#startd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,

            });
            $('#endd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
            });
        });
    </script>
    @include('include.footer')

