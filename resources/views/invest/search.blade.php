@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Investigation | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Investigation</li>
                        </ol>
                        <a  href="{{action('AddBrandController@InvestCreate')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add Investigation</a>
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="">
                <div class="card">
                    <div class="card-body">
                        <form action="{{url('/invest/show')}}" method="POST" accept-charset="utf-8">
                            @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Entity</label>
                                        <select name="entity" class="form-control" id='select'>
                                            <option value="">--Select--</option>
                                            @foreach($enitys as $ent)
                                                <option value="{{ $ent->ent_id }}" @if($ent->ent_id == $entity) selected @endif>{{ $ent->ent->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Investigation agency</label>
                                        <select name="na" class="form-control" id='select'>
                                            <option value="">--Select--</option>
                                            @foreach($agnecis as $agency)
                                                <option value="{{ $agency->inag_id }}" @if($agency->inag_id == $inag) selected @endif>{{ $agency->inag->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <select name="country" class="form-control">
                                            <option value="">--Select--</option>
                                            @foreach($country as $count)
                                                <option value="{{ $count->country_id }}" @if($count->country_id == $countrys) selected @endif>{{ $count->country->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>State</label>
                                        <select name="state" class="form-control">
                                            <option value="">--Select--</option>
                                            @foreach($state as $stat)
                                                <option value="{{ $stat->state_id }}" @if($stat->state_id == $states) selected @endif>{{ $stat->state->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>City / Location</label>
                                        <select name="city" class="form-control">
                                            <option value="">--Select--</option>
                                            @foreach($city as $cit)
                                                <option value="{{ $cit->city_id }}" @if($cit->city_id == $citys) selected @endif>{{ $cit->city->city }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Category Of Target</label>
                                        <select name="target" class="form-control">
                                            <option value="">--Select--</option>
                                            @foreach($targets as $targe)
                                                <option value="{{ $targe->target }}" @if($targe->target == $target) selected @endif>{{ $targe->target }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Name of Product</label>
                                        <select name="product" class="form-control">
                                            <option value="">--Select--</option>
                                            @foreach($products as $produc)
                                                <option value="{{ $produc->nop_id }}" @if($produc->nop_id == $product) selected @endif>{{ $produc->nop->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>From Date :</label>
                                        <div class="input-group">
                                            <input type="date" name="form" class="form-control" value="{{ $from_date }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>To Date :</label>
                                        <div class="input-group">
                                            <input type="date" name="to" class="form-control" value="{{ $to_date }}">
                                            <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-success">Search</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <p id="demo" align="center"></p>
                        <table id="myTable" class="table table-bordered"  data-page-length='50'>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Entity</th>
                                    <th>Investigating agency</th>
                                    <th>Location</th>
                                    <th>Category of target</th>
                                    <th>Name of product</th>
                                    <th>Start date of investigation</th>
                                    <th>End date of investigation</th>
                                    <th>Document</th>
                                    <th>TM App No.</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($results))
                                @foreach($results as $result)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $result->ent->name }}</td>
                                    <td>{{ $result->inag->name}}</td>
                                    <td>{{ $result->country->name.'-'.$result->state->name.'-'.$result->city->city}}</td>
                                    <td>{{ $result->target}}</td>
                                    <td>{{ $result->nop->name}}</td>
                                    <td>{{ date('d-m-Y',strtotime($result->dsi))}}</td>
                                    <td>{{$result->dei}}</td>
                                    <td>
                                        <a href="{{url('public/Scan_Copy_Of_Applications/'.$result->docs)}}" download>{{$result->docs}}></a>
                                    </td>
                                    <td>{{$result->tm_app}}</td>
                                    <td>
                                        <a href="{{url('edit_inv/'.$result->id)}}" type="edit" class="btn btn-warning">Edit</a>
                                        <a href="{{ url('show_inv/'.$result->id) }}" class="btn btn-primary">View</a>
                                        <a href="{{url('/expensein/'.$result->id.'/expenses')}}" class="btn btn-success">Expenses / Fees</a>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                @foreach($shows as $show)
                                    <tr>
                                        <td>{{$show->inag}}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{$show->city->city}}</td>
                                        <td>{{$show->target}}</td>
                                        <td>{{$show->product}}</td>
                                        <td>{{$show->dsi}}</td>
                                        <td>{{$show->dei}}</td>
                                        <td>
                                            <a href="{{url('public/Scan_Copy_Of_Applications/'.$show->docs)}}" download>{{$show->docs}}
                                        </td>
                                        <td>{{$show->tm_app}}</td>
                                        <td>
                                            <a href="{{url('edit_inv/'.$show->id)}}" type="edit" class="btn btn-warning">Edit</a><br><br>
                                            <a href="{{ url('show_inv/'.$show->id) }}" class="btn btn-primary">View</a>
                                            <a href="{{url('/expensein/'.$show->id.'/expenses')}}" class="btn btn-success">Expenses / Fees</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var x = document.getElementById("myTable").rows.length - 1;
        document.getElementById("demo").innerHTML = "Total Records are " + x;
    </script>
    <script type="text/javascript">
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
        });
    </script>
@include('include.footer')
