@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor"> Agent | <a href="{{ url('common_master') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active"> Agent</li>
                        </ol>
                        <a  href="{{action('AddTradmarkController@AgentCreate')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add</a>
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-rounded" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ $message }}
                </div>
            @endif
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Email</th>
                                        <th>Mobile No</th>
                                        <th>Registration No</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($agents as $app)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$app->name}}</td>
                                            <td>{{$app->address}}</td>
                                            <td>{{$app->e_mail}}</td>
                                            <td>{{$app->mobile_number}}</td>
                                            <td>{{$app->registration_no}}</td>
                                            <td class="edit"><a>
                                                <a href="{{url('edit/'.$app->id.'/agent')}}" type="button" name="submit" value="submit" class="btn btn-primary btn-edit">Edit</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  @include('include.footer')
