@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
   <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
              <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Litigation Type | <a href="{{route('liti_type.index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                                <li class="breadcrumb-item active">Litigation Type</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
                @include('include.message')
                <div class="row">
                <div class="col-md-12">
                  <div class="card">
                  <div class="card-body">
                   {!! Form::model($liti,['route' => ['liti_type.update',$liti->id],'data-parsley-validate' => '', 'method' => 'PUT']) !!}
                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                        {{ Form::label('name','Litigation type:') }}
                        {{ Form::text('name',null,['class' => 'form-control','required' => '']) }}
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        {{ Form::submit('Submit',['class' => 'btn btn-success']) }}
                      </div>
                    </div>
                    {!! Form::close() !!}
                  </div>
                </div>
                </div>

                </div>





            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
  @include('include.footer')
