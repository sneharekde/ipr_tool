@include('include.header')


    <script src="{{asset('public/js/loader.js')}}"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      var analytics = {!! json_encode($array) !!};
      
      console.log(analytics);

      function drawChart() {

       var record={!! json_encode($array) !!};
       console.log(record);
       // Create our data table.
       var data = new google.visualization.DataTable();
        data.addColumn('string', 'Source');
       data.addColumn('number', 'Total_Signup');
       for(var k in record){
            var v = record[k];
           
             data.addRow([k,v]);
          console.log(v);
          }
        var options = {
          title: 'tradmark Portfoilio',
          width: 1000,
          height: 400
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));

        function selectHandler() {
          var selectedItem = chart.getSelection()[0];
          if (selectedItem) {
            var topping = data.getValue(selectedItem.row, 0);
            //console.log(topping)
    if(topping == topping){
      //window.open("/posts/action", "_self", true);
   var trim = topping.replace(/ /g, "%20");;
        //var id = document.getElementById("stud_id").value;
        console.log(trim);
        $('#modal_div').load('modal-data/piechart/'+trim,
                function() {
                    $('#bootstrap-modal').modal({
                        show : true
                    });
                });
    
    }
          }

        }

        google.visualization.events.addListener(chart, 'select', selectHandler);    
        chart.draw(data, options);
      }

    </script>
    




  </head>
  <style type="text/css">
    @media (min-width: 768px) {
      .modal-xl {
        width: 90%;
       max-width:1200px;
      }
    }
    rect {
        cursor:pointer;
    }
    #chart_div text {
        cursor:pointer;
    }

</style>

    
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Dashboard </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->

                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <a href="{{action('FrontEndController@tradmark_portfolio')}}">
                          <div class="card">
                            <div class="card-body">
                                <div class="row p-t-10 p-b-10">
                                    <!-- Column -->
                                    <div class="col p-r-0">
                                        <h1 class="font-light">{{$total_tradmark}}</h1>
                                        <h6 class="text-muted">tradmark</h6></div>
                                    <!-- Column -->
                                    <div class="col text-right align-self-center">
                                        <div data-label="" class="css-bar m-b-0 css-bar-primary css-bar-20"><img src="{{url('public/img/tm_icon.png')}}" alt=""></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row p-t-10 p-b-10">
                                    <!-- Column -->
                                    <div class="col p-r-0">
                                        <h1 class="font-light">248</h1>
                                        <h6 class="text-muted">Copy Right</h6></div>
                                    <!-- Column -->
                                    <div class="col text-right align-self-center">
                                        <div data-label="30%" class="css-bar m-b-0 css-bar-danger css-bar-20"><i class="mdi mdi-briefcase-check"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row p-t-10 p-b-10">
                                    <!-- Column -->
                                    <div class="col p-r-0">
                                        <h1 class="font-light">352</h1>
                                        <h6 class="text-muted">Patent</h6></div>
                                    <!-- Column -->
                                    <div class="col text-right align-self-center">
                                        <div data-label="40%" class="css-bar m-b-0 css-bar-warning css-bar-40"><i class="mdi mdi-star-circle"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->

                    <div class="col-md-12 align-self-center">
                        <h4 class="text-themecolor">Time Period </h4>
                    </div>

                     <div class="col-md-4">
                            <div class="example">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="datepicker-autoclose" placeholder="mm/dd/yyyy">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                        </div>

                </div>

                 <div class="col-md-4">
                          <div class="example">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="datepickers-autoclose" placeholder="mm/dd/yyyy">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                        </div>

                </div>

                <div class="col-md-4">
                       <a href="" class="btn btn-primary" href="">Search Status</a>    

                </div><br><br>

                <div class="col-md-12 align-self-center">
                        <h4 class="text-themecolor">Time Period </h4>
                    </div><br><br>
                    

                <div class="col-md-12">
                    <div class="card">
                                    <div class="card-body align-center">
                                       
                                            
                                                

                                                <div id="chart_div"></div>
                                                <div id="modal_div"></div>


                                    </div>
                                </div>
                </div>

  
                
                <style>
                    #chartbardiv {
width:100%;
height: 500px;
}
                </style>
                
                
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
            <div id="chartbardiv"></div>
      
                        </div>
                    </div>
                </div>
                
        
        
  



                </div>

               
    

                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        
<script src="{{asset('public/js/core.js')}}"></script>
    <script src="{{asset('public/js/charts.js')}}"></script>
    <script src="{{asset('public/js/animated.js')}}"></script>
<script src="{{asset('public/js/jquery.min.js')}}"></script>
  
<script>$(window).on("load", function() {
myxychart();
});  
     function myxychart() {   
        am4core.ready(function () {
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end
                var chart = am4core.create("chartbardiv", am4charts.XYChart);
                
        var record=[{!! json_encode($array2) !!}];
                 
          
          
        var oj = record[0];
        var ojvalue = Object.values(oj);
        var records = [];
        var ojrecord = {};
          var i;
//var ojrecord2 ={};
        ojvalue.forEach(function (item, index, array) {
           // console.log(records);
            var company = "Company";
            var item1 = item[1];
            var item2 = item[2];
            var record2 = [];
           record2.push(records);
          //  console.log(item1);
            if (records.length !=0) {

              //   ojrecord[company] = item[0];
          //       ojrecord[item1] = item2;
         //     record2.push(ojrecord);
         //                 ojrecord = {};
          //    console.log(records);
              //  for (var i = 0; i < records.length; i++) {
                    // 
              if(records.some(recs => recs.Company === item[0])) {
              var o = records.findIndex(x => x.Company === item[0] );
                    // console.log(o);
                    
                       // i = records.findIndex(x => x.Company === item[0]);
                       records[o][item1] = item2;
                        //console.log(item[0]);
                  
              }
               else {
                      
                         //console.log("newentry");
                      ojrecord = {};
                      ojrecord[company] = item[0];
                      ojrecord[item1] = item2;
                      records.push(ojrecord);
                 //  console.log("pehlaelse");
                //   console.log(records);
                    }
                
              
            
            } else {
                // console.log("emptyu");
              ojrecord = {};
                ojrecord[company] = item[0];
                ojrecord[item1] = item2;
                records.push(ojrecord);
                ojrecord = {};
            }
      
            //console.log(records);
 });
       // console.log(records);
        var status = ['Active tradmark', 'Marked for Exam', 'Reply to Examination Report', 'Due for Hearing', 'Accepted and Advertised','Opposition Notice Received','Reply to Opposition','Hearing for Opposition','Decision Pending','Registered','Rejected by TM Office','Abandoned'];
        var i;
        var new_data = [];
        // console.log(ojrecord);
        // Create chart instance


        // Add data
        chart.data = records;



        chart.legend = new am4charts.Legend();
        chart.legend.position = "right";

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "Company";
        categoryAxis.renderer.grid.template.opacity = 0;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.min = 0;
        valueAxis.renderer.grid.template.opacity = 0;
        valueAxis.renderer.ticks.template.strokeOpacity = 0.5;
        valueAxis.renderer.ticks.template.stroke = am4core.color("#495C43");
        valueAxis.renderer.ticks.template.length = 10;
        valueAxis.renderer.line.strokeOpacity = 0.5;
        valueAxis.renderer.baseGrid.disabled = true;
        valueAxis.renderer.minGridDistance = 40;

        // Create series
               function createSeries(field, name) {
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueX = field;
            series.dataFields.categoryY = "Company";
            series.stacked = true;
            series.name = name;
            var labelBullet = series.bullets.push(new am4charts.LabelBullet());
            labelBullet.locationX =
                0.5;
            labelBullet.label.text = "{valueX}";
            labelBullet.label.fill = am4core.color("#fff");
            series.tooltip.label.interactionsEnabled = true;
            series.tooltip.keepTargetHover = true;
            // series.columns.template.tooltipHTML =
            //     '<div id="info">{Company}</div>';
            series.columns.template.tooltipHTML =
                '<div id="info">{Company}</div><div id="name">{name}</div>';
            series.events.on("hit", () => {
                var com = document.getElementById("info").innerHTML;
                var name = series.name;
                var ctrim = com.replace(/ /g, "%20");
                var nametrim = name.replace(/ /g, "%20");
                var finaltrim = ctrim + '%40' + nametrim;
                console.log(finaltrim);

                //var id = document.getElementById("stud_id").value;
                console.log(finaltrim);
                $('#modal_div').load('modal-data/barchart/'+finaltrim,
                    function () {
                        $('#bootstrap-modal').modal({
                            show: true
                        });
                    });

myxychart();
                //console.log(name);
            });
        }
        //clickevent


        //var z = [];
        for (var k in status) {
            createSeries(status[k], status[k]);
            //onsole.log(status[k]);
        }
                var t = 1;

        // Create series
        // var series = chart.series.push(new am4charts.ColumnSeries());
        // series.dataFields.valueY = field;
        // series.dataFields.categoryX = "Company";
 
        // Set up tooltips

        //console.log(z);
    }); // end am4core.ready()
}
</script>





@include('include.footer')
       