@include('include.header')
   <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
              <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Internal Litigation | <a href="{{route('int_lit_code.index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                                <li class="breadcrumb-item active">Internal Litigation</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->

                <div class="row">
                <div class="col-md-12">
                  <div class="card">
                  <div class="card-body">
                   <form action="{{url('/category_mark/save')}}" method="POST" accept-charset="utf-8">
            @csrf
              <div class="form-group {{ $errors->has('category_of_mark') ? 'has-error' : '' }}">
              <label for="">Internal litigation code </label>
              <input type="text" class="form-control" name="category_of_mark" value="{{ old('category_of_mark') }}">
              <span class="text-danger">{{ $errors->first('category_of_mark') }}</span>
            </div>
            <label for="">Code description </label>
              <textarea class="form-control "></textarea>
              <span class="text-danger">{{ $errors->first('category_of_mark') }}</span>
            </div>
                      <div class="col-md-12 center"><button type="Submit" class="btn btn-success">Submit</button></div>
                    </form>
                  </div>
                </div>
                </div>

                </div>

               
    

                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
  @include('include.footer')