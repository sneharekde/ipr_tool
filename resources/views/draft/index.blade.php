@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Draft | <a href="{{ url('tm_portfolio') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Trademark</th>
                                    <th>Logo|Device</th>
                                    <th>Entity</th>
                                    <th>Application Number</th>
                                    <th>Applicant Date</th>
                                    <th>Application Field</th>
                                    <th>Classes</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posts as $post)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $post->trademark }}</td>
                                    <td><a href="{{ url('public/trademark',$post->logo) }}" download="">{{ $post->logo }}</a></td>
                                    <td>
                                        @if ($post->ent_id !='')
                                        {{ $post->ent->name }}
                                        @else
                                        NA
                                        @endif
                                    </td>
                                    <td>{{ $post->application_no }}</td>
                                    <td>{{ date('d-m-Y',strtotime($post->app_date)) }}</td>
                                    <td>{{ $post->appf->application_field }}</td>
                                    <td>
                                        @foreach ($post->trade_class as $item)
                                    <span class="label label-success" >{{ $item->name }}</span>
                                    @endforeach
                                    </td>
                                    <td>
                                        <a href="{{url('edit_summary/'.$post->id)}}" type="edit" class="btn btn-warning btn-sm">Edit</a><br><br>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
