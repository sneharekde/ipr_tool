@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
  <div class="page-wrapper">
    <div class="container-fluid">
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Add Trademark | <a href="{{action('FrontEndController@tradmark_portfolio')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ action('FrontEndController@index') }}">Home</a></li>
              <li class="breadcrumb-item active">Add Trademark</li>
            </ol>
          </div>
        </div>
      </div>
      @include('include.message')
      <div class="card">
        <div class="card-body">
          {!! Form::open(['url' => 'new_tradmark/save','data-parsley-validate' => '','method' => 'POST','files' => true]) !!}
          <div class="row">
            <div class="col-md-6 form-group">
              {{ Form::label('app_no','Proprietor Code *:') }}
              {{ Form::text('app_no',null,['class' => 'form-control','required' => '','placeholder' => 'Enter Proprieter Code...']) }}
            </div>
            <div class="col-md-6 form-group">
              {{ Form::label('app_date','Application Date *:') }}
              {{ Form::text('app_date',null,['class' => 'form-control','id' => 'tradedate','required' => '']) }}
            </div>
            <div class="col-md-6 form-group">
              {{ Form::label('natapl_id','Type of Trademark *:') }}
              {{ Form::select('natapl_id',$ar2,null,['class' => 'form-control','placeholder' => 'Select','required' => '']) }}
            </div>
            <div class="col-md-6 form-group">
              {{ Form::label('user_id','Users *:') }}
              {{ Form::select('user_id',$ar3,null,['class' => 'form-control','placeholder' => 'Select','id' => 'userID','required' => '']) }}
            </div>
            <div class="col-md-6 form-group">
              {{ Form::label('application_no','Application Number *:') }}
              {{ Form::text('application_no',null,['class' => 'form-control','required','placeholder' => 'Enter Application Number...']) }}
            </div>
            <div class="col-md-6 form-group">
              {{ Form::label('agent_fee','Agent Fee / Attorney Fee:') }}
              {{ Form::text('agent_fee',null,['class' => 'form-control','placeholder' => 'Enter Agent Fee...']) }}
            </div>
            <div class="col-md-4 form-group">
              {{ Form::label('appf_id','Nature of the Applicant *:') }}
              {{ Form::select('appf_id',$ar4,null,['class' => 'form-control','placeholder' => 'Select','id' => 'appf_id','required' => '']) }}
            </div>
            <div class="col-md-4 form-group">
              {{ Form::label('app_type_id','Type of Application *:') }}
              <select class="form-control" name="app_type_id" required="" id="app_type_id">
                <option value="" >Select</option>
              </select>
            </div>
            <div class="col-md-4 form-group">
              {{ Form::label('gov_fee','Government Fee:') }}
                <input type="text" name="gov_fee" class="form-control" id="fee" readonly>
            </div>
            <div class="col-md-12">
              <hr><h4 class="text-center">Applicant</h4><hr>
            </div>
            <div class="col-md-12" id="showf" style="display: none">
                <div class="row">
                    <div class="col-md-6 form-group">
                        {{ Form::label('ent_id','Entity Name:') }}
                        {{ Form::select('ent_id',$ar5,null,['class' => 'form-control', 'id' => 'entid', 'placeholder' => 'Select']) }}
                    </div>
                    <div class="col-md-6 form-group">
                        {{ Form::label('nate_id','Nature of Entity:') }}
                        <select name="nate_id" class="form-control" id="natid">
                            <option value="">Select</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12 form-group" id="msme" style="display: none">
                {!! Form::label('msme_cert', 'MSME / Start-up Certificate:') !!}
                {!! Form::file('msme_cert', ['class' => 'form-control']) !!}
            </div>
            <div class="col-md-12 form-group" id="showap" style="display: none">
                {{ Form::label('name_in','Name:') }}
                {{ Form::text('name_in',null,['class' => 'form-control','placeholder' => 'Enter Applicant Name...']) }}
            </div>


            <div class="col-md-12">
              <hr><h4 class="text-center">Agent / Attorney Info (if any)</h4><hr>
            </div>
            <div class="col-md-4 form-group">
                {{ Form::label('agent_id','Name:') }}
                {{ Form::select('agent_id',$ar7,null,['class' => 'form-control','id' => 'agent_id','placeholder' => 'Select']) }}
              </div>
              <div class="col-md-4 form-group">
                  {{ Form::label('agent_code','Agent / Attorney Code:') }}
                  {{ Form::text('agent_code',null,['class' => 'form-control','id' => 'agent_code']) }}
              </div>
              <div class="col-md-4 form-group">
                  {!! Form::label('poa', 'Power of Attorney :') !!}
                  {!! Form::file('poa', ['class' => 'form-control']) !!}
              </div>
            <div class="col-md-12">
              <hr><h4 class="text-center">Trademark</h4><hr>
            </div>
            <div class="col-md-4 form-group">
              {{ Form::label('trademark','Trademark:') }}
              {{ Form::text('trademark',null,['class' => 'form-control','required' => '','placeholder' => 'Enter Trademark...']) }}
            </div>
            <div class="col-md-4 form-group">
              {{ Form::label('catmak_id','Category of Mark *:') }}
              {{ Form::select('catmak_id',$ar8,null,['class' => 'form-control','placeholder' => 'Select','required' => '',]) }}
            </div>
            <div class="col-md-4 form-group">
              {{ Form::label('lang_id','Language *:') }}
              {{ Form::select('lang_id',$ar9,null,['class' => 'form-control','placeholder' => 'Select','required' => '']) }}
            </div>
            <div class="col-md-4 form-group">
              {{ Form::label('desc_mark','Description of the mark:') }}
              {{ Form::text('desc_mark',null,['class' => 'form-control','placeholder' => 'Enter Description Mark...']) }}
            </div>
            <div class="col-md-4 form-group">
              {{ Form::label('logo','Image File:') }}
              {{ Form::file('logo',['class' => 'form-control']) }}
            </div>
            <div class="col-md-4 form-group">
              {{ Form::label('remarks','remarks:') }}
              {{ Form::text('remarks',null,['class' => 'form-control']) }}
            </div>
            <div class="col-md-12">
              <hr><h4 class="text-center">Class of Goods or Services</h4><hr>
            </div>
            <div class="col-md-12 form-group">
              {{ Form::label('classg_id','Class of Goods or Services *:') }}
              {{ Form::select('classg_id[]',$ar10,null,['class' => 'form-control select2-multi','id' => 'classe','required' => '','multiple' => 'true']) }}
            </div>
            <div class="col-md-12 form-group textf">

            </div>
            <div class="col-md-12">
                <hr><h4 class="text-center">Details of Use</h4><hr>
            </div>
            <div class="col-md-12 form-group">
                {!! Form::select('details_use', ['Proposed to be Used' => 'Proposed to be Used','Used since' => 'Used since'], null, ['class' => 'form-control', 'id' => 'useofde' ,'placeholder' => 'Select']) !!}
            </div>
            <div class="col-md-12">
                <div class="row" id="dispuse" style="display: none">
                    <div class="col-md-6 form-group">
                        {!! Form::label('datea', 'Date') !!}
                        {!! Form::text('datea', null, ['class' => 'form-control','id' => 'dataap']) !!}
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('statement_mark', 'Upload User Affidavit') !!}
                        {!! Form::file('statement_mark[]', ['class' => 'form-control','accept' => 'application/pdf']) !!}
                    </div>
                </div>
            </div>

            <div class="col-md-12">
              <hr><h4 class="text-center">Detail Of The Person Submitting The Application</h4><hr>
            </div>
            <div class="col-md-12 form-group">
              {{ Form::label('name_person','Name:') }}
              {{ Form::text('name_person',null,['class' => 'form-control','id' => 'namein']) }}
            </div>
            <div class="col-md-12 form-group">
              {{ Form::label('authority','Designation:') }}
              {{ Form::text('authority',null,['class' => 'form-control','id' => 'designation']) }}
            </div>
            <div class="col-md-12 form-group">
              {{ Form::label('scan_copy_app','Scan Copy Of Application:') }}
              {{ Form::file('scan_copy_app[]',['class' => 'form-control','multiple' => true]) }}
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <input type="submit" name="draft" value="Save as Draft" class="btn btn-info">
                <input type="submit" name="submit" value="Submit" class="btn btn-success">
            </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
  <script>

  </script>

  @include('include.footer')

  <script type="text/javascript">
    $(document).ready(function(){
        $('#useofde').on('change',function(){
            var useofde = $(this).val();
            if(useofde == 'Used since'){
                $('#dispuse').show();
            }
            if(useofde !='Used since'){
                $('#dispuse').hide();
            }
        });

        $('#agent_id').on('change',function(){
            var agent_id = $(this).val();
            $('#agent_code').attr('value','');
            $('#agent_address').attr('value','');
            $.ajax({
                type:'get',
                url:'{!! URL::to('findagent') !!}',
                data:{'id':agent_id},
                dataType:'json',
                success:function(data){
                  $('#agent_code').attr('value',data.registration_no);
                  $('#agent_address').attr('value',data.address);
              },
            });
        });

      $(document).on('change','#appf_id',function(){
        var appf_id = $(this).val();
        $('#app_type_id').find('option').not(':first').remove();
        $('#fee').attr('value','');
        $.ajax({
          type:'get',
          url:'{!! URL::to('findtype') !!}',
          data:{'id':appf_id},
          success:function(data){
            var leng = data.length;
                    for(var i=0; i<leng; i++){
                        var id = data[i]['id'];
                        var name = data[i]['app_type'];
                        $('#app_type_id').append("<option value='"+id+"'>"+name+"</option>");
                    }
          }
        });
      });

      $(document).on('change','#app_type_id',function(){
        var appt_id = $(this).val();
        $('#fee').attr('value','');
        $.ajax({
          type:'get',
          url:'{!! URL::to('findfee') !!}',
          data:{'id':appt_id},
          dataType:'json',
          success:function(data){
            $('#fee').attr('value',data.fees);
        },
        });
      });

      $(document).on('change','#entid',function(){
          var entids = $(this).val();
          $('#natid').find('option').not(':first').remove();
          $.ajax({
            type:'get',
            url:'{!! URL::to('findnat') !!}',
            data:{'id':entids},
            success:function(data){
                var id = data['id'];
                var name = data['nature_of_applicant'];
                $('#natid').append("<option value='"+id+"' selected>"+name+"</option>");
            }
          });
      });

      $(document).on('change','#userID',function(){
        var userid = $(this).val();
        $('#namein').attr('value','');
        $('#designation').attr('value','');
        $.ajax({
          type:'get',
          url:'{!! URL::to('finduser') !!}',
          data:{'id':userid},
          success:function(data){
            $('#namein').attr('value',data.first_name);
            $('#designation').attr('value',data.designation);
          }
        });
    });

      $(document).on('change','#appf_id',function(){
        var appf = $(this).val();
        if(appf == ''){
            $('#showap').hide();
            $('#showf').hide();
            $('#msme').hide();
        }
        if (appf == '3') {
          $('#showap').show();
          $('#showf').hide();
          $('#msme').hide();
        }
        if(appf =='5'){
            $('#showap').hide();
            $('#showf').show();
            $('#msme').show();
        }
        if(appf =='6'){
            $('#showap').hide();
            $('#showf').show();
            $('#msme').hide();
        }

      });



      $('.select2-multi').select2({
        placeholder: 'Please select class to view details',
        closeOnSelect: true
      });


      $(document).on('change','#classe',function(){
        var class_id = $(this).val();
        var c = $(this).parents();
        var txt=" ";
        if(class_id ==''){
          c.find('.textf').html(" ");

        }
        $.ajax({
          type:'get',
          url:'{!! URL::to('classdes') !!}',
          data:{'id':class_id},
          success:function(data){
            for (var i = 0; i < data.length; i++) {
              txt +='<textarea class="form-control" readonly="">'+data[i].desc+'</textarea>';
            }
            c.find('.textf').html(" ");
            c.find('.textf').append(txt);
          },
          error:function(){

          }
        });
      });

    });
  </script>



  <script>
    $(function() {
        $('#tradedate').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0
       });

       $('#dataap').datepicker({
        changeMonth: true,
        changeYear: true,
        showAnim: 'slideDown',
        maxDate: 0,
        //minDate: +1,
    });
    });
  </script>
