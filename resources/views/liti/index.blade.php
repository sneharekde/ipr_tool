@include('include.header')
    <style>
        .cards{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            background: aliceblue;
        }
        .cards-body{
            padding: 10px;
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Litigation Master | <a href="{{ url('master') }}" > Go Back <i class="fa fa-arrow-circle-left"></i></a></h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/dashbord') }}">Home</a></li>
                            <li class="breadcrumb-item active">Litigation Master</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('liti_type') }}">Litigation Type</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('area_exp') }}">Area Of Expertise</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('law_firm') }}">Law Firm</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('ex_counsel') }}">External Counsel</a></h5>
                                </div>
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('stages') }}">Stages</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('legal_not') }}">Legal Notice Category</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('Court_list') }}">Court/Tribunal</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('advoc') }}">Advocate</a></h5>
                                </div>
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('currenc') }}">Currency </a></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
