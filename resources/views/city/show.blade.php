@include('include.header')

  <div class="page-wrapper">
    <div class="container-fluid">
      <div class="row page-titles">
          <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">City | <a href="{{action('AddTradmarkController@master')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4> 
          </div>
          <div class="col-md-7 align-self-center text-right">
              <div class="d-flex justify-content-end align-items-center">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                      <li class="breadcrumb-item active">City</li>
                  </ol>
                  <a  href="{{action('AddBrandController@cityCreate')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add</a>
              </div>
          </div>
        </div>
    </div>
     @if ($message = Session::get('success'))
        <div class="alert alert-success alert-rounded" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ $message }}
        </div>
        @endif

        <div class="col-md-12">
                     <div class="card">
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped" data-order='[[ 0, "desc" ]]'>
                                         <thead>
                                          <tr>
                                            <th>Sr No</th>
                                            <th>City</th>
                                            <th>Edit</th>
                                          </tr>
                                          </thead>
                                          <tbody>
                                            @foreach($states as $state)
                                            <tr>
                                            <td>{{$state->id}}</td>
                                            <td>{{$state->state}}</td>
                                            <td><a href="{{url('edit/'.$state->id.'/state')}}" class="btn btn-warning">Edit</a></td>
                                          </tr>
                                          @endforeach
                                          </tbody>
                                    </table>
                                </div>
                    




                </div>

               
    

                
            </div>
                   </div>
  </div>

@include('include.footer')