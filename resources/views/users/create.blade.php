@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Add User | <a href="{{action('UserController@ShowUser')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active">Add User</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <form action="{{url('user/save')}}" method="POST"  data-parsley-validate="" accept-charset="utf-8">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box-body">
                                    <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                        <label for="">First Name * :</label>
                                        <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required>
                                        <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                        <label for="">Last Name :</label>
                                        <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required>
                                        <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('mobile_no') ? 'has-error' : '' }}">
                                        <label>Mobile No * :</label>
                                        <input type="number" class="form-control" name="mobile_no" value="{{ old('mobile_no') }}" required>
                                        <span class="text-danger">{{ $errors->first('mobile_no') }}</span>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                        <label>Mail Id * :</label>
                                        <input type="text" class="form-control" name="email" value="{{ old('email') }}" required>
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="form-group {{ $errors->has('user_name') ? 'has-error' : '' }}">
                                        <label>User Name * :</label>
                                        <input type="text" class="form-control" name="user_name" value="{{ old('user_name') }}" required>
                                        <span class="text-danger">{{ $errors->first('user_name') }}</span>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="form-group {{ $errors->has('user_password') ? 'has-error' : '' }}">
                                        <label>User Password * :</label>
                                        <input type="password" class="form-control" name="password" value="{{ old('password') }}" required>
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="form-group {{ $errors->has('password_confirmed') ? 'has-error' : '' }}">
                                        <label>Confirm Password * :</label>
                                        <input type="password" class="form-control" name="password_confirmed" value="{{ old('password_confirmed') }}">
                                        <span class="text-danger">{{ $errors->first('password_confirmed') }}</span>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box-body">
                                    <div class="form-group {{ $errors->has('enttity') ? 'has-error' : '' }}">
                                        <label>Entity * :</label>
                                        <select name="enttity" class="form-control" required>
                                            <option value="">--Select--</option>
                                            @foreach($entity as $ent)
                                            <option value="{{$ent->name}}">{{$ent->name}}</option>
                                            @endforeach
                                          </select>
                                          <span class="text-danger">{{ $errors->first('enttity') }}</span>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="form-group {{ $errors->has('unit') ? 'has-error' : '' }}">
                                        <label>Location * :</label>
                                        <select name="unit" class="form-control" required>
                                            <option value="">--Select--</option>
                                            @foreach($units as $unit)
                                                <option value="{{$unit->unit}}">{{$unit->unit}}</option>
                                            @endforeach
                                          </select>
                                          <span class="text-danger">{{ $errors->first('unit') }}</span>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="form-group {{ $errors->has('function') ? 'has-error' : '' }}">
                                        <label>Function * :</label>
                                        <select name="function" class="form-control" required>
                                            <option value="">--Select--</option>
                                            @foreach($functions as $function)
                                                <option value="{{$function->function}}">{{$function->function}}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('function') }}</span>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="form-group {{ $errors->has('employee_id') ? 'has-error' : '' }}" >
                                        <label for="">Employee ID * :</label>
                                        <input type="text" class="form-control" name="employee_id" value="{{ old('employee_id') }}" required>
                                        <span class="text-danger">{{ $errors->first('employee_id') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('designation') ? 'has-error' : '' }}">
                                        <label>Designation * :</label>
                                        <select name="designation" class="form-control" required>
                                            <option value="">--Select--</option>
                                            @foreach($designations as $designation)
                                                <option value="{{$designation->designation}}" {{ old('designation') == '$designation->designation' ? 'selected' : '' }}>{{$designation->designation}}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('designation') }}</span>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
                                        <label>Role * :</label>
                                        <select name="role" class="form-control" required>
                                            <option value="">--Select--</option>
                                            @foreach($roles as $role)
                                                <option value="{{$role->role_name}}">{{$role->role_name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('role') }}</span>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                                        <label for="">Address * :</label>
                                        <textarea name="address" id="" cols="30" rows="7" class="form-control" required></textarea>
                                        <span class="text-danger">{{ $errors->first('address') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  @include('include.footer')
