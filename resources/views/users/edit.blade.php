@include('include.header')

<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
   <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
              <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Add User | <a href="{{action('UserController@ShowUser')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                                <li class="breadcrumb-item active">Add User</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
                 <form action="{{url('user/update')}}" method="POST" accept-charset="utf-8" data-parsley-validate="">
                   @csrf
                  <input type="hidden" name="id" value="{{$user->id}}">
                <div class="row">
         <div class="col-md-6">
            <div class="box-body">
              <!-- Date -->
              <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                <label for="">First Name * :</label>
                <input type="text" class="form-control" name="first_name" value="{{ $user->first_name }}" required>
                <span class="text-danger">{{ $errors->first('first_name') }}</span>
              </div>
              <!-- /.form group -->

              <!-- Date range -->
              <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                <label for="">Last Name :</label>
               <input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}" required>
              <span class="text-danger">{{ $errors->first('last_name') }}</span>
              </div>
              <!-- /.form group -->

              <!-- Date and time range -->
               <div class="form-group {{ $errors->has('mobile_no') ? 'has-error' : '' }}">
                <label>Mobile No * :</label>

                 <input type="text" class="form-control" name="mobile_no" value="{{ $user->mobile_no }}" required>
                  <span class="text-danger">{{ $errors->first('mobile_no') }}</span>
                <!-- /.input group -->
              </div>

              <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <label>Mail Id * :</label>

                 <input type="text" class="form-control" name="email" value="{{ $user->email }}" required>
                  <span class="text-danger">{{ $errors->first('email') }}</span>
                <!-- /.input group -->
              </div>

               <div class="form-group {{ $errors->has('user_name') ? 'has-error' : '' }}">
                <label>User Name * :</label>

                 <input type="text" class="form-control" name="user_name" value="{{ $user->user_name }}" required>
                  <span class="text-danger">{{ $errors->first('user_name') }}</span>
                <!-- /.input group -->
              </div>




              <!-- /.form group -->
              <!-- /.form group -->

            </div>
            <!-- /.box-body -->
          </div>
          <div class="col-md-6">
            <div class="box-body">
              <!-- Date -->
              <div class="form-group {{ $errors->has('enttity') ? 'has-error' : '' }}">
                <label>Entity * :</label>

                <select name="enttity" class="form-control" required>
                    <option value="">--Select--</option>
                    @foreach($entity as $ides)
                    <option value="{{$ides->name}}" @if($user->enttity == $ides->name) selected="selected" @endif>{{$ides->name}}</option>
                    @endforeach
                  </select>
                  <span class="text-danger">{{ $errors->first('enttity') }}</span>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date range -->
             <div class="form-group {{ $errors->has('unit') ? 'has-error' : '' }}">
                <label>Location * :</label>

                  <select name="unit" class="form-control" required>
                    <option value="">--Select--</option>
                    @foreach($units as $unit)
                    <option value="{{$unit->unit}}" @if($user->unit == $unit->unit) selected="selected" @endif>{{$unit->unit}}</option>
                    @endforeach
                  </select>
                  <span class="text-danger">{{ $errors->first('unit') }}</span>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

               <div class="form-group {{ $errors->has('function') ? 'has-error' : '' }}">
                <label>Function * :</label>

                  <select name="function" class="form-control" required>
                    <option value="">--Select--</option>
                   @foreach($functions as $function)
                    <option value="{{$function->function}}" @if($user->function == $function->function) selected="selected" @endif>{{$function->function}}</option>
                    @endforeach
                  </select>
                  <span class="text-danger">{{ $errors->first('function') }}</span>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

            <div class="form-group {{ $errors->has('employee_id') ? 'has-error' : '' }}">
                <label for="">Employee ID * :</label>
                <input type="text" class="form-control" name="employee_id" value="{{ $user->employee_id }}" required>
                <span class="text-danger">{{ $errors->first('employee_id') }}</span>
              </div>

              <div class="form-group {{ $errors->has('designation') ? 'has-error' : '' }}">
                <label>Designation * :</label>

                  <select name="designation" class="form-control" required>
                    <option value="">--Select--</option>
                    @foreach($designations as $designation)
                    <option value="{{$designation->designation}}" @if($user->designation == $designation->designation) selected="selected" @endif {{ old('designation') == '$designation->designation' ? 'selected' : '' }}>{{$designation->designation}}</option>
                    @endforeach
                  </select>
                  <span class="text-danger">{{ $errors->first('designation') }}</span>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

               <div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
                <label>Role * :</label>

                  <select name="role" class="form-control" required>
                    <option value="">--Select--</option>
                    @foreach($roles as $role)
                    <option value="{{$role->role_name}}" @if($user->role == $role->role_name) selected="selected" @endif>{{$role->role_name}}</option>
                    @endforeach
                  </select>
                  <span class="text-danger">{{ $errors->first('role') }}</span>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

                <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                <label for="">Address * :</label>
                <textarea name="address" id="" cols="30" rows="7" class="form-control" required>{{$user->address}}</textarea>
                <span class="text-danger">{{ $errors->first('address') }}</span>
              </div>



            </div>
            <!-- /.box-body -->
          </div>


            <div class="col-md-12">
              <div class="form-group text-center">
            <button type="submit" class="btn btn-success">Update</button>
          </div>
            </div>


                </div>

              </form>





            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
  @include('include.footer')
