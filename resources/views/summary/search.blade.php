@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Litigation Summary | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Litigation Summary </li>
                        </ol>
                        <a  href="{{route('summary.create')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add Litigation</a>
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-rounded" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ $message }}
                </div>
            @endif
            <div class="card">
                <form action="{{url('/summarys')}}" method="POST" accept-charset="utf-8">
                @csrf
                    <div class="form-body">
                        <div class="card-body">
                            <div class="row pt-3">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Entity</label>
                                        <select class="form-control" name="ent">
                                            <option value="">Select</option>
                                            @foreach($ents as $enta)
                                                <option value="{{ $enta->entity }}" @if($enta->entity == $ent) selected @endif>{{ $enta->entity }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Location  </label>
                                        <select class="form-control" name="loc">
                                            <option value="">Select</option>
                                            @foreach($locs as $loca)
                                                <option value="{{ $loca->Location }}" @if($loca->Location == $loc) selected @endif>{{ $loca->Location }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Department</label>
                                        <select class="form-control" name="dep">
                                            <option value="">Select</option>
                                            @foreach($deps as $depa)
                                                <option value="{{ $depa->department }}" @if($depa->department == $dep) selected @endif>{{ $depa->department }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>By / Against Country</label>
                                        <select class="form-control" name="by_ag">
                                            <option value="">Select</option>
                                            @foreach($bys as $by)
                                                <option value="{{ $by->by_ag }}" @if($by->by_ag == $by_ag) selected @endif>{{ $by->by_ag }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select class="form-control" name="cat">
                                            <option value="">Select</option>
                                            @foreach($cats as $cata)
                                                <option value="{{ $cata->category }}" @if($cata->category == $cat) selected @endif>{{ $cata->category }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Responsible Person</label>
                                        <select class="form-control" name="cnp">
                                            <option value="">Select</option>
                                            @foreach($cnps as $cnpa)
                                                <option value="{{ $cnpa->int_per }}" @if($cnpa->int_per == $cnp) selected @endif>{{ $cnpa->int_per }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Case Filing Date - From :</label>
                                        <div class="input-group">
                                            <input type="date" name="form" class="form-control" value="{{ $from_date }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>To Date :</label>
                                        <div class="input-group">
                                            <input type="date" name="to" class="form-control" value="{{ $to_date }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select class="form-control" name="stat">
                                            <option value="">Select</option>
                                            @foreach($status as $stata)
                                                <option value="{{ $stata->status }}" @if($stata->status == $stat) selected @endif>{{ $stata->status }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Criticality :</label>
                                        <select class="form-control" name="crit">
                                            <option value="">Select</option>
                                            @foreach($critics as $critic)
                                                <option value="{{ $critic->critic }}" @if($critic->critic == $crit) selected @endif>{{ $critic->critic }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-success">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <p id="demo" align="center"></p>
                        <table id="myTable" class="table table-bordered"  data-page-length='50'>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Company (By/Against)</th>
                                    <th>Case Reference No</th>
                                    <th>Next Hearing Date</th>
                                    <th>Case Title</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($results as $result)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $result->by_ag }}</td>
                                        <td>{{ $result->case_ref }}</td>
                                        <td>
                                            @if ($result->next_date != '')
                                            {{ date('d-m-Y',strtotime($result->next_date)) }}
                                            @endif
                                        </td>
                                        <td>{{  $result->by_part.'...'.$result->acting_as.' V/S '.$result->against_part.'...'.$result->opp_act_as }}</td>
                                        <td>{{ $result->status }}</td>
                                        <td>
                                            <a href="{{ route('summary.edit',$result->id) }}" type="edit" class="btn btn-warning">Edit</a>
                                            <a href="{{ url('summary/delete',$result->id) }}" class="btn btn-danger">Delete</a>
                                            <a href="{{ route('summary.show',$result->id) }}" class="btn btn-info">Update</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @include('include.footer')
    <script type="text/javascript">
        var x = document.getElementById("myTable").rows.length - 1;
        document.getElementById("demo").innerHTML = "Total Records are " + x;
    </script>
