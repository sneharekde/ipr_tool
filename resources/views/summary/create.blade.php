@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>

  <div class="page-wrapper">
    <div class="container-fluid">
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Create New | <a href="{{route('summary.index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Create New</li>
            </ol>
          </div>
        </div>
      </div>
      <style type="text/css">
        .rate{
          background-color: #ecf0f1;
          border: 1px solid #000;
          border-radius: 50px;
          font-size: 18px;
          display: flex;
          align-items: center;
          justify-content: center;
          height: 30px;
          padding: 0 10px;
          margin: 30px 15px 30px 25px;
        }
      </style>
      @include('include.message')
      <div class="card">
        <div class="card-body">
          {!! Form::open(['route' => 'summary.store','data-parsley-validate' => '','method' => 'POST','files' => true]) !!}
            <div class="row">
              <div class="col-md-12">
                <hr><h4 class="text-center">Organization Details</h4><hr>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                {{ Form::label('entity','Entity:') }}
                {{ Form::select('entity',$entity,null,['class' => 'form-control','placeholder' => 'Select Entity','required' => '']) }}
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                {{ Form::label('Location','Location:') }}
                {{ Form::select('Location',$uni,null,['class' => 'form-control','placeholder' => 'Select Location','required' => '']) }}
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                {{ Form::label('department','Department:') }}
                {{ Form::select('department',$func,null,['class' => 'form-control','placeholder' => 'Select Department','required' => '']) }}
              </div>
              <div class="col-md-12">
                <hr><h4 class="text-center">Internal Details</h4><hr>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('category','Category:') }}
                {{ Form::select('category',$cat,null,['class' => 'form-control','placeholder' => 'select category','required' => '']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('by_ag','Company(By / Against):') }}
                <select name="by_ag" class="form-control" required="">
                  <option value="">Select</option>
                  <option value="By Company">By Company</option>
                  <option value="Against Company">Against Company</option>
                </select>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('by_part','By Party(s):') }}
                {{ Form::text('by_part',null,['class' => 'form-control','required' => '']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('against_part','Against Party(s):') }}
                {{ Form::text('against_part',null,['class' => 'form-control','required' => '']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('acting_as','Company Acting As:') }}
                <select name="acting_as" class="form-control" required="">
                  <option value="">Select</option>
                  <option value="Plaintiff">Plaintiff</option>
                  <option value="Applicant">Applicant</option>
                  <option value="Complainant">Complainant</option>
                  <option value="Appellant">Appellant</option>
                  <option value="Petitioner">Petitioner</option>
                </select>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('ext_counsel','External Counsel:') }}
                {{ Form::select('ext_counsel',$ext,null,['class' => 'form-control','placeholder' => 'Select']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('law_firm','Advocate Law Firm:') }}
                {{ Form::select('law_firm',$lao,null,['class' => 'form-control','placeholder' => 'Select','required' => '']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('adv','Advocate:') }}
                {{ Form::select('adv',$adv,null,['class' => 'form-control','placeholder' => 'Select','required' => '']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('user_id','Assigned To:') }}
                {{ Form::select('user_id',$use,null,['class' => 'form-control','placeholder' => 'Select']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('case_date','Case Filling Date *:') }}
                {{ Form::text('case_date',null,['class' => 'form-control','required' => '','id' => 'cased']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('int_per','Internal Contact Person:') }}
                {{ Form::text('int_per',null,['class' => 'form-control']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('critic','Criticality *:') }}
                <select name="critic" class="form-control" required="">
                  <option value="">Select</option>
                  <option value="High">High</option>
                  <option value="Medium">Medium</option>
                  <option value="Low">Low</option>
                </select>
              </div>
              <div class="col-md-12">
                <hr><h4 class="text-center">Opposite Party Details</h4><hr>
              </div>
              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Opposite Party Acting As * :</label>
                  <select name="opp_act_as" class="form-control" required="">
                    <option value="">Select</option>
                    <option value="Defendent">Defendent</option>
                    <option value="Opponent">Opponent</option>
                    <option value="Opposite Party">Opposite Party</option>
                    <option value="Respondent">Respondent</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('opp_part_add','Opposite party address:') }}
                {{ Form::text('opp_part_add',null,['class' => 'form-control']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('opp_part_firm','Opposite party advocate law firm:') }}
                {{ Form::text('opp_part_firm',null,['class' => 'form-control']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('opp_part_adv','Advocate name:') }}
                {{ Form::text('opp_part_adv',null,['class' => 'form-control']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('opp_part_adv_c','Advocate contact number:') }}
                {{ Form::text('opp_part_adv_c',null,['class' => 'form-control']) }}
              </div>
              <div class="col-md-12">
                <hr><h4 class="text-center">Litigation Details</h4><hr>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('case_ref','Case Reference No:') }}
                {{ Form::text('case_ref',null,['class' => 'form-control']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('juris','Jurisdiction name:') }}
                {{ Form::text('juris',null,['class' => 'form-control','required' => '']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('court','Court / Tribunal:') }}
                {{ Form::select('court',$cour,null,['class' => 'form-control','placeholder' => 'Select','required' => '']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('rel_law','Relevent Law:') }}
                {{ Form::text('rel_law',null,['class' => 'form-control','required' => '']) }}
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                {{ Form::label('comments','Brief Facts/Comments:') }}
                {{ Form::text('comments',null,['class' => 'form-control']) }}
              </div>
              <div class="col-md-12">
                <hr><h4 class="text-center">Amount Details</h4><hr>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                {{ Form::label('date','Date:') }}
                {{ Form::text('date',null,['class' => 'form-control','required' => '','id' => 'dated']) }}
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                {{ Form::label('amount_involved','Amount Involved:') }}
                {{ Form::text('amount_involved',null,['class' => 'form-control','id' => 'from_ammount','required' => '']) }}
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                {{ Form::label('currency','Currency:') }}
                {{ Form::select('currency',$cur,null,['class' => 'form-control','id' => 'from_currency','placeholder' => 'select','required' => '']) }}
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                {{-- <button id="exchange">
                  <i class="fas fa-exchange-alt"></i>
                </button> --}}
                <div class="rate" id="rate"></div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                {{ Form::label('conv_curr','Converted Currency:') }}
                {{ Form::select('conv_curr',$cur,null,['class' => 'form-control','id' => 'to_currency','placeholder' => 'select','required' => '']) }}
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                {{ Form::label('conv_amt','Converted Amount:') }}
                {{ Form::text('conv_amt',null,['class' => 'form-control','id' => 'to_ammount','readonly' =>'','required' => '']) }}
              </div>
              <div class="col-md-12">
                <div class="form-group text-center">
                  <button type="submit" name="submit" class="btn btn-success">Create</button>
                </div>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
  {{--  <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/bootstrap.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>  --}}
@include('include.footer')

<script>
    $(function() {
        $('#cased').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
            //minDate: +1,
        });
        $('#dated').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
            //minDate: +1,
        });

    });
</script>
