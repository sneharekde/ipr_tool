@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Update | <a href="{{ route('summary.index') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>

            {{-- Date --}}
            @include('include.message')
            <div class="card">
            	<div class="card-body">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                          <a class="nav-link active" href="{{ url('/summary/'.$summ->id) }}">Litigation Details</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link"  href="{{ url('/summary/hearing/'.$summ->id) }}">Next Hearing and Stage</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('/summary/documents/'.$summ->id) }}">Documents</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/summary/fees/'.$summ->id) }}">Fees</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/summary/completion/'.$summ->id) }}">Completion</a>
                        </li>
                    </ul>
                    <table class="table table-striped">
                        {{--  <tr>
                            <th>Litigation Id :</th><td>{{ $summ->id }}</td>
                        </tr>  --}}
                        <tr>
                            <th>By/Against Company :</th><td>{{ $summ->by_ag }}</td>
                        </tr>
                        <tr>
                            <th>Category :</th><td>{{ $summ->category }}</td>
                        </tr>
                        <tr>
                            <th>Case No.  :</th><td>{{ $summ->case_ref }}</td>
                        </tr>
                        <tr>
                            <th>Jurisdiction name  :</th><td>{{ $summ->juris }}</td>
                        </tr>
                        <tr>
                            <th>Case Filling Date :</th><td>{{ date('d-m-Y',strtotime($summ->case_date)) }}</td>
                        </tr>
                        <tr>
                            <th>Company External Counsel : </th><td>{{ $summ->ext_counsel }}</td>
                        </tr>
                        <tr>
                            <th>Matter Handled By : </th><td></td>
                        </tr>
                        <tr>
                            <th>Assigned to   : </th><td>{{ $summ->user->first_name.' '. $summ->user->last_name }}</td>
                        </tr>
                        <tr>
                            <th>Internal Contact Person :</th><td>{{ $summ->int_per }}</td>
                        </tr>
                        <tr>
                            <th>Criticality :</th><td>{{ $summ->critic }}</td>
                        </tr>
                        <tr>
                            <th>By Party :</th><td>{{ $summ->by_part }}</td>
                        </tr>
                        <tr>
                            <th>Against Party :</th><td>{{ $summ->against_part }}</td>
                        </tr>
                        <tr>
                            <th>Opposite Party address :</th><td>{{ $summ->opp_part_add }}</td>
                        </tr>
                        <tr>
                            <th>Opposite Party advocate :</th><td>{{ $summ->opp_part_adv }}</td>
                        </tr>
                        <tr>
                            <th>Opposite Party advocate law firm :</th><td>{{ $summ->opp_part_firm }}</td>
                        </tr>
                        <tr>
                            <th>Opposite Party advocate contact number    :</th><td>{{ $summ->opp_part_adv_c }}</td>
                        </tr>
                        <tr>
                            <th>Court/Tribunal : </th><td>{{ $summ->court }}</td>
                        </tr>
                        <tr>
                            <th>Amount Involved  : </th><td>{{ $summ->amount_involved }} {{ $summ->currency }}</td>
                        </tr>
                        <tr>
                            <th>Relevant law :</th><td>{{ $summ->rel_law }}</td>
                        </tr>
                        <tr>
                            <th>Brief Description:</th><td>{{ $summ->comments }}</td>
                        </tr>
                    </table>





            	</div>
            </div>
		</div>
	</div>
@include('include.footer')
<script type="text/javascript">
    $(document).ready(function () {
        $("#text").hide();
        $("#yes").click(function () {
            $("#text").show();
        });
        $("#no").click(function () {
            $("#text").hide();
        });
    });

    $(function(){
        $('#disp').hide();
        $('#select').change(function(){
                if($('#select').val() == 'Against'){
                    $('#disp').show();
                    $('#other').hide();
                }else{
                    $('#disp').hide();
                    $('#other').show();
                }
            });
    });
</script>
