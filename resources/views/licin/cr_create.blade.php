@include('include.header')
	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">License In | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4> 
                </div>
            </div>
			<div class="card">
				<div class="card-body">
					<table class="table table-bordered">
						@if(isset($cr))
						@foreach($cr as $c )
							<h5>Tradmark Name : {{ $c->trademark }}</h5>
							<tr>
								<th>Application No</th><td>{{ $c->application_no }}</td>
							</tr>
							<tr>
								<th>Application Date</th><td>{{ date('d-m-Y',strtotime($c->app_date)) }}</td>
							</tr>
							<tr>
								<th>Class</th><td>{{ $c->classg->name }}</td>
							</tr>
							<tr>
								<th>Category of Mark</th><td>{{ $c->catmak->category_of_mark }}</td>
							</tr>
							<tr>
								<th>Status</th><td>{{ $c->status }}</td>
							</tr>
							<tr>
								<th>License In</th><td><a href="{{ url('trlicense',$c->id) }}" class="btn btn-success">License In</a></td>
							</tr>
						@endforeach
						@elseif(isset($cr))

						@elseif(isset($des))

						@elseif(isset($pat))

						@else
						<p>No Records Found</p>
						@endif
					</table>	
				</div>
			</div>
		</div>
	</div>
@include('include.footer')