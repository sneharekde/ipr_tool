<style>
.dataTables_filter
{
  display: none;
}
.paginate_button
{
  display: none;
}
.dataTables_info
{
  display: none;
}
</style>
<div class="modal  fade bs-example-modal-lg" id="bootstrap-modal6" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Litigation Status Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow-x: auto;">
        <div id="demo-modal">
          <div class="table-responsive">
            <table id="example33" class="table table-striped table-bordered " >
              <thead>
                <tr>
                  <th>Litigation Id</th>
                  <th>By / Against</th>
                  <th>Case Category</th>
                  <th>Case Title</th>
                  <th>Case Reference No</th>
                </tr>
              </thead>
              <tbody>
                @foreach($results as $result)
                  @if($result->status ==  $name)
                    <tr>
                      <td>{{ $no++ }}</td>
                      <td>{{ $result->by_ag }}</td>
                      <td>{{ $result->category }}</td>
                      <td>{{ $result->by_part.'...'.$result->acting_as.' V/S '.$result->against_part.'...'.$result->opp_act_as}}</td>
                      <td>{{ $result->case_ref }}</td>
                    </tr>
                  @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  $(function () {
    // responsive table
    $('#config-table').DataTable({
      responsive: true
    });
    $('#example33').DataTable({
      dom: 'Bfrtip',
      buttons: [
       {
          extend: 'pdf',
          footer: true,
          title: 'Litigation Report',
          orientation: 'Portrait',
          pageSize: 'LEGAL',
        //   exportOptions: {
        //   columns: [0,2,3,4,5,6,7,8,9,10,11,12]
        // }
      }]
    });
    $('.buttons-pdf').addClass('btn btn-primary mr-1');
  });
</script>
