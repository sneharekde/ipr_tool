@include('include.header')
   <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">License| <a href="{{action('FrontEndController@tradmark_portfolio')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4> 
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                                <li class="breadcrumb-item active">License</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->


                   @if ($message = Session::get('success'))
      <div class="alert alert-success alert-rounded" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{ $message }}
</div>
@endif

                    <!-- Column -->
                   <div class="col-md-12">
                     <div class="card">
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped" data-order='[[ 0, "desc" ]]'>
                                        <thead>
                                        <tr>
                                          <th>Sr No.</th>
                                          <th>Application No</th>
                                          <th>License</th>
                                          <th>Edit</th>
                                        </tr>
                                        </thead>
                                         <tbody>
                                          @foreach($showes as $app)
                                          <tr>
                                          <td>{{$app->id}}</td>
                                          <td>{{$app->application_no}}</td>
                                          <td><a href="{{url('public/license/'.$app->license)}}" download>{{$app->license}}</a></td>
                                          <td><a href="{{url('edit/'.$app->id.'/edit_license')}}" class="btn btn-warning">Edit</a></td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                    




                </div>

               
    

                
            </div>
                   </div>
        </div>
  @include('include.footer')
