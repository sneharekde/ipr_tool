@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Analytics | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Analytics</li>
                        </ol>
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-rounded" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ $message }}
                </div>
            @endif
            <style>
                table thead tr th{
                    border: 1px solid black;
                    padding: 5px;
                }
                table tbody tr td{
                    border: 1px solid black;
                    padding: 10px;
                }
                table{
                    margin-bottom: 20px;
                }
            </style>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <p id="demo" align="center"></p>
                            <table  class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Particulars</th>
                                        <th>Number</th>
                                        <th>Total Expenses</th>
                                        <th>Total Seizure Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                    	                <td>Investigations</td>
                    	                <td>{{ $total_inv }}</td>
                    	                <td>{{ $sum_inv }}</td>
                    	                <td>-</td>
                                    </tr>
                                    <tr>
                  	                    <td>Enforcement Actions</td>
                                        <td>{{ $total_enf }}</td>
                                        <td>{{ $sum_enf }}</td>
                                        <td>{{ $sez_val }}</td>
                                    </tr>
                                    <tr>
                  	                    <td class="text-center"><strong>Total</strong></td>
                  	                    <td>{{ $all_brand }}</td>
                  	                    <td>{{ $all_amount }}</td>
                  	                    <td>{{ $sez_val }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
                            <h3>Enforcement Actions World Wide</h3>
                        </div>
                        <div id="chart_div" style="width: 100%"></div>
                        <div id="modalg"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('include.footer')
    <script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyAeDdGLO4-e-wujNduRoHw5IZOlxyIeyEY" async="" defer="defer"></script>

    <script type='text/javascript'>
        google.charts.load('current', {
            'packages': ['geochart'],
            // Note: you will need to get a mapsApiKey for your project.
            // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
            'mapsApiKey': 'AIzaSyAeDdGLO4-e-wujNduRoHw5IZOlxyIeyEY'
        });
        google.charts.setOnLoadCallback(drawMarkersMap);
        function drawMarkersMap() {
            var rec = {!! json_encode($ar1) !!};

            console.log(rec);
            var data = google.visualization.arrayToDataTable([
            ['Lat','Long','Name', 'Enforcements Actions'],

            @php
                foreach ($enfs as $enf){
                //echo "[".$enf->city->location.','.$enf->count."],";
                echo "[".$enf->city->latitude.", ".$enf->city->longitude.", ".json_encode($enf->city->city).", ".$enf->count."],";
                }
            @endphp


            ]);
            var options = {
                region: 'world',
                displayMode: 'markers',
                backgroundColor:'orange',
                colorAxis: {colors: ['green', 'blue']}
            };
            var chart = new google.visualization.GeoChart(document.getElementById('chart_div'));
            chart.draw(data, options);
            function selectHandler(){
                var selectedItem = chart.getSelection()[0];
                if(selectedItem){
                    var topping = data.getValue(selectedItem.row, 2);
                    if(topping == topping){
                        var trim = topping.replace(/ /g, "%20");
                        $('#modalg').load('modalge/'+trim,function(){
                            $('#modalgeo').modal({
                                show : true
                            });
                        });
                    }
                }
            }
            google.visualization.events.addListener(chart,'select',selectHandler);
        };
    </script>
