@include('include.header')
    <style>
        .cards{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            background: aliceblue;
        }
        .cards-body{
            padding: 10px;
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Trademark Master </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/dashbord') }}">Home</a></li>
                            <li class="breadcrumb-item active">Trademark Master</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('trad_master') }}"><i class="fa fa-tm"></i> Trademark Master</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('brand_master') }}"><i class="fa fa-tm"></i> Brand Protection Master</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('des_mas') }}">Design Master</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('copy_master') }}">Copyright Master</a></h5>
                                </div>
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('patent_master') }}"><i class="fa fa-tm"></i> Patent Master</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ action('LitigationMasterController@index') }}"> Litigation Master</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="cards">
                                <div class="cards-body">
                                    <h5 class="text-center"><a href="{{ url('common_master') }}">Common Master</a></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
