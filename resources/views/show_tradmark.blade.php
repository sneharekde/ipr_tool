@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Show tradmark | <a href="{{action('FrontEndController@tradmark_portfolio')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Show tradmark</li>
                        </ol>
                        <div id="editor"></div>
                        {{--  <button id='cmd' onclick="printHtmldiv()" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa fa-arrow-down"></i> Download PDF</button>  --}}
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="col-md-12" id="content">
                <div class="card">
                    <div class="card-body">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Trademark Detail</a>
                                <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Document</a>
                                <a class="nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Status</a>
                                <a class="nav-link" id="nav-agent-tab" data-toggle="tab" href="#nav-agent" role="tab" aria-controls="nav-agent" aria-selected="false">Agent/ Attorney fees</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <table id="example23" class="table table-bordered">
                                    <tr>
                                        <th>Proprietor Code:</th>
                                        <td>{{$show->app_no}}</td>
                                    </tr>
                                    <tr>
                                        <th>Agent / Attorney Code:</th>
                                        <td>{{ $show->agent_code }}</td>
                                    </tr>
                                    <tr>
                                        <th>Application Date:</th>
                                        <td>{{date('d-m-Y',strtotime($show->app_date))}}</td>
                                    </tr>
                                    <tr>
                                        <th>Type Of Trademark:</th>
                                        <td>{{$show->natapl->nature_of_application}}</td>
                                    </tr>
                                    <tr>
                                        <th>User:</th>
                                        <td>{{$show->user->first_name}} {{ $show->user->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Application No:</th>
                                        <td>{{$show->application_no}}</td>
                                    </tr>
                                    <tr>
                                        <th>Agent / Attorny Fee:</th>
                                        <td>{{ $show->agent_fee }}</td>
                                    </tr>
                                    <tr>
                                        <th>Nature of the Applicant:</th>
                                        <td>{{ $show->appf->application_field }}</td>
                                    </tr>
                                    <tr>
                                        <th>Type of Application:</th>
                                        <td>{{ $show->app_type->app_type }}</td>
                                    </tr>
                                    <tr>
                                        <th>Government Fee:</th>
                                        <td>{{ $show->gov_fee }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left" class="text-center"><h4>Applicant</h4></td>
                                    </tr>
                                    <tr>
                                        <th>Name:</th>
                                        <td>
                                            @if ($show->name_in !='')
                                            {{$show->name_in}}
                                            @else
                                            NA
                                            @endif

                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Entity Name:</th>
                                        <td>
                                            @if ($show->ent_id !='')
                                            {{ $show->ent->name }}
                                            @else
                                                NA
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Nature of Entity:</th>
                                        <td>
                                            @if ($show->nate_id !='')
                                            {{ $show->nate->nature_of_applicant }}
                                            @else
                                            NA
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left" class="text-center"><h4>Applicant Agent</h4></td>
                                    </tr>
                                    <tr>
                                        <th>Name:</th>
                                        <td>{{ $show->agent->name }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left" class="text-center"><h4>Trademark</h4></td>
                                    </tr>
                                    <tr>
                                        <th>Trademark:</th>
                                        <td>{{$show->trademark}}</td>
                                    </tr>
                                    <tr>
                                        <th>Category of mark:</td>
                                        <td>{{$show->catmak->category_of_mark}}</td>
                                    </tr>
                                    <tr>
                                        <th>Language:</th>
                                        <td>{{$show->lang->language}}</td>
                                    </tr>
                                    <tr>
                                        <th>Description of the mark:</th>
                                        <td>{{$show->desc_mark}}</td>
                                    </tr>
                                    <tr>
                                        <th>Remarks:</th>
                                        <td>{{ $show->remarks }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-center"><h4>Class of Goods or Services</h4></td>
                                    </tr>
                                    <tr>
                                        <th>Class:</th>
                                        <td>
                                            @foreach ($show->trade_class as $item)
                                                <button class="btn btn-sm btn-primary">{{ $item->name }}</button>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>User Affidavit:</th>
                                        <td>
                                            <a href="{{ url('public/trademark/'.$show->statement_mark) }}" download="">{{ $show->statement_mark }}</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left" class="text-center"><h4>Detail Of The Person Submitting The Application</h4></td>
                                    </tr>
                                    <tr>
                                        <th align="left">Name:</th>
                                        <td>{{$show->name_person}}</td>
                                    </tr>
                                    <tr>
                                        <th align="left">Designation:</th>
                                        <td>{{$show->authority}}</td>
                                    </tr>

                                    <tr>
                                        <th align="left">Scan Copy Of Application:</th>
                                        <td><a href="{{url('public/trademark/'.$show->scan_copy_app)}}" download="">{{$show->scan_copy_app}}</a></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <table class="table table-bordered" style="margin-top: 10px">
                                    <thead>
                                        <tr>
                                            <th>Document</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><a href="{{ url('trademark/'.$show->logo) }}"><img src="{{ asset('trademark/'.$show->logo) }}" alt="{{ $show->logo }}" width="100" height="100">{{ $show->logo }}</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                <table class="table table-responsive-sm" style="margin-top: 10px">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>tradmark Name</th>
                                            <th>Comment</th>
                                            <th>Document</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($statues as $stat)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$stat->trade->trademark}}</td>
                                                <td>{{$stat->comments}}</td>
                                                <td>
                                                    @foreach(explode(',', $stat->docs) as $info)
                                                        <a href="{{ url('public/trademark/'.$info) }}" download="">{{ $info }}</a>
                                                    @endforeach
                                                </td>
                                                <td>{{$stat->status}} @if($stat->sub_status !='') {{ ' - '.$stat->sub_status }} @endif</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-agent" role="tabpanel" aria-labelledby="nav-agent-tab">
                                <div class="" style="margin-top:10px">
                                    <a href="{{ url('trade-fee',$show->id) }}" class="btn btn-info">Add Fees</a>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Fees</th>
                                                <th>Invoice</th>
                                                <th>stamp paper</th>
                                                <th>notarisation charges</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php  $i = 1; ?>
                                            @foreach ($posts as $post)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ date('d-m-Y',strtotime($post->date)) }}</td>
                                                    <td>{{ $post->fee }}</td>
                                                    <td>
                                                        @if ($post->invoice !='')
                                                            <a href="{{ url('public/copyright/img',$post->invoice) }}">{{ $post->invoice }}</a>
                                                        @else
                                                        NA
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($post->stamp !='')
                                                            <a href="{{ url('public/copyright/img',$post->stamp) }}">{{ $post->stamp }}</a>
                                                        @else
                                                        NA
                                                        @endif
                                                    </td>
                                                    <td>{{ $post->not_fee }}</td>
                                                    <td><a href="{{ url('trade-fee/edit',$post->id) }}" class="btn btn-success btn-xs">Edit</a></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="editor"></div>
        </div>
    </div>
    <script>
        $('#cmd').click(function() {
            var options = {
            'width': 170
            };
            var pdf = new jsPDF('p', 'pt', 'a4');
            pdf.addHTML($("#content"), 15, 15, options.margin, function() {
                pdf.save('{{$show->trademark}}.pdf');
            });
        });
    </script>
@include('include.footer')
