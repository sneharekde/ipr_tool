@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">View Assign To | <a href="{{ url('leads') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">View Assign To</li>
                        </ol>
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="card">
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                <th>Entity Name</th>
                                <th>Investigating Agency</th>
                                <th>Brand Protection Manager</th>
                                <th>Country</th>
                                <th>State</th>
                                <th>City</th>
                                <th>Target Name</th>
                                <th>Product Name</th>
                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posts as $post)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                <td>{{ $post->ent->name }}</td>
                                <td>{{ $post->inag->name }}</td>
                                <td>{{ $post->user->first_name.' '.$post->user->last_name }}</td>
                                <td>{{ $post->country->name }}</td>
                                <td>{{ $post->state->name }}</td>
                                <td>{{ $post->city->city }}</td>
                                <td>{{ $post->target->targets }}</td>
                                <td>{{ $post->nop }}</td>
                                <td>
                                    @if ($post->status == 'assign')
                                    <a href="{{ url('forward-investigation',$post->id) }}" class="btn btn-sm btn-info">Convert To Investiagtion</a>
                                @endif
                                </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('include.footer')
