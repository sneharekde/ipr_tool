<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ url('public/css/parsley.css') }}">
    <title>Lead</title>
  </head>
  <body>

    <div class="container-fluid">
        @include('include.message')
        <form action="{{ action('leadGenController@save') }}" method="POST" data-parsley-validate="" style="margin: 50px;" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="form-group">
                        <label for="">Lead Location</label>
                        <input type="text" name="loca" class="form-control" required="">
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="form-group">
                        <label for="">Lead Category</label>
                        <select name="target_id" id="" class="form-control" required="">
                            <option value="">Select</option>
                            @foreach ($targets as $item)
                                <option value="{{ $item->id }}">{{ $item->targets }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="form-group">
                        <label for="">Product Name</label>
                        <input type="text" name="pname" class="form-control" required="">
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="form-group">
                        <label for="">Approx Quantity or Volume</label>
                        <input type="text" name="qty" class="form-control" required="" data-parsley-type="number">
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="form-group">
                        <label for="">Lead By</label>
                        <input type="text" name="leadby" class="form-control" required="">
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="form-group">
                        <label for="">Phone Number</label>
                        <input type="text" class="form-control" name="phone" required="" minlength="10" maxlength="10" data-parsley-type="digits">
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="form-group">
                        <label for="">Employee ID(Optionals)</label>
                        <input type="text" name="emp_id" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="form-group">
                        <label for="">Photos</label>
                        <input type="file" name="photos[]" class="form-control" multiple>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="form-group">
                        <label for="">Remarks</label>
                        <textarea name="comm" id="" cols="30" rows="5" class="form-control"></textarea>
                    </div>
                </div>







                <div class="col-md-12 form-group">
                    <input type="submit" name="submit" class="btn btn-success">
                </div>
            </div>
        </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="{{ url('public/js/parsley.min.js') }}"></script>
</body>
</html>
