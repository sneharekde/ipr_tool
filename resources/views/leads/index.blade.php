@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
  <div class="page-wrapper">
    <div class="container-fluid">
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Lead Summary | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Lead Summary</li>
                </ol>
            </div>
        </div>
      </div>

      @include('include.message')


      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <p id="demo" align="center"></p>
              <table id="" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Lead Generate By</th>
                    <th>Locaton</th>
                    <th>Approx Quantity</th>
                    <th>Category of Target</th>
                    <th>Phone Number</th>
                    <th>Comment</th>
                    <th>Docs</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($leads as $item)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $item->leadby }}</td>
                            <td>{{ $item->location }}</td>
                            <td>{{ $item->product }}</td>
                            <td>{{ $item->target->targets }}</td>
                            <td>{{ $item->phone }}</td>
                            <td>{{ $item->comments }}</td>
                            <td>
                                @if ($item->docs !='')
                                    @foreach (explode(',', $item->docs) as $file)
                                        <a href="{{ url('public/trademark/'.$file) }}" download="">{{ $file }}</a>
                                    @endforeach
                                @endif
                            </td>
                            <td>{{ $item->status }}</td>
                            <td>
                                @if ($item->status == 'Not Assign')
                                    <a href="{{ url('leads-assign',$item->id) }}" class="btn btn-sm btn-primary">Add Assign</a>
                                @else
                                    {{--  <a href="{{ url('assign-to',$item->id) }}" class="btn btn-sm btn-success">View Assign</a>  --}}
                                @endif

                            </td>
                        </tr>
                    @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>
<script type="text/javascript">
    var x = document.getElementById("myTable").rows.length - 1;
    document.getElementById("demo").innerHTML = "Result : Total Enforcement are " + x;
  </script>

  <script type="text/javascript">
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping

        });

  </script>
@include('include.footer')
