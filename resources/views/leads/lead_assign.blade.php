@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Lead Assign To | <a href="{{ url('leads') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Lead Assign To</li>
                        </ol>
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="card">
                <div class="card-body">
                    {!! Form::open(['url' => 'assign-to/save','data-parsley-validate' => '','method' => 'POST']) !!}
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            {!! Form::hidden('lead_id', $post->id) !!}
                            <div class="form-group">
                                {!! Form::label('ent_id', 'Entity Name:') !!}
                                {!! Form::select('ent_id', $ar2, null, ['class' => 'form-control','required' => '','placeholder' => 'Select']) !!}
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                {!! Form::label('inag_id', 'Investigation Agency:') !!}
                                {!! Form::select('inag_id', $ar1, null, ['class' => 'form-control','required' => '','placeholder' => 'Select']) !!}
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                {!! Form::label('user_id', 'Brand Protection Manager:') !!}
                                {!! Form::select('user_id', $ar3, null, ['class' => 'form-control','required' => '','placeholder' => 'Select']) !!}
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                {!! Form::label('country_id', 'Country:') !!}
                                {!! Form::select('country_id', $ar4, null, ['class' => 'form-control','id' => 'country_id','required' => '','placeholder' => 'Select']) !!}
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                <label for="state_id">State</label>
                                <select name="state_id" id="state_id" class="form-control" required="">
                                    <option value="">Select State</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                <label for="city_id">City</label>
                                <select name="city_id" id="city_id" class="form-control" required="">
                                    <option value="">Select State</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-sm-12">
                            <div class="form-group">
                                {!! Form::label('target_id', 'Category Target') !!}
                                {!! Form::select('target_id', $ar5, $post->target_id, ['class' => 'form-control','required' => '','placeholder' => 'Select']) !!}
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-12">
                            <div class="form-group">
                                {!! Form::label('nop', 'Name of Product') !!}
                                {!! Form::text('nop', $post->product, ['class' => 'form-control','required' => '']) !!}
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                                {!! Form::submit('submit', ['class' => 'btn btn-success']) !!}
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
    <script>
        $(document).ready(function(){
            $('#country_id').change(function(){
                var country_ids = $(this).val();
                console.log(country_ids);
                $('#state_id').find('option').not(':first').remove();
                $.ajax({
                    url:'{!! URL::to('findstate') !!}',
                    type:'GET',
                    data:{'id':country_ids},
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    dataType:'json',
                    success:function(data){
                    //    console.log(data);
                    var leng = data.length;
                        for(var i=0; i<leng; i++){
                            var id = data[i]['id'];
                            var name = data[i]['name'];
                            $('#state_id').append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            });

            $('#state_id').change(function(){
                var state_ids = $(this).val();
                console.log(state_ids);
                $('#city_id').find('option').not(':first').remove();
                $.ajax({
                    url:'{!! URL::to('findcity') !!}',
                    type:'GET',
                    data:{'id':state_ids},
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    dataType:'json',
                    success:function(data){
                    //    console.log(data);
                    var leng = data.length;
                        for(var i=0; i<leng; i++){
                            var id = data[i]['id'];
                            var name = data[i]['city'];
                            $('#city_id').append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            });
        });
    </script>
