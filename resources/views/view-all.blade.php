@include('include.header')
   <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">View All | <a href="{{action('FrontEndController@tradmark_portfolio')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4> 
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">View All</li>
                            </ol>
                            <!-- <a  href="{{action('FrontEndController@add_tradmark')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add tradmark</a> -->
                        </div>
                    </div>
                </div>


<style>
  table thead tr th{
    border: 1px solid black;
    padding: 5px;
  }
   table tbody tr td{
    border: 1px solid black;
    padding: 10px;
   }
   table{
    margin-bottom: 20px;
   }
   select{
    -webkit-box-shadow: 5px 5px 5px #666;
    -moz-box-shadow: 5px 5px 5px #666;
    box-shadow: 5px 5px 5px #666;
    display:inline-block;
   }
      input{
    -webkit-box-shadow: 5px 5px 5px #666;
    -moz-box-shadow: 5px 5px 5px #666;
    box-shadow: 5px 5px 5px #666;
   }
</style>



<div class="col-md-12">
                     <div class="card">
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                  <p id="demo" align="center"></p>
                                    <table id="myTable" class="table-responsive-sm" data-order='[[ 0, "desc" ]]' data-page-length='50'>
                                        <thead>
                                            <tr>
                                                  <th>application No.</th>
                                                    <th>Logo|Device</th>
                                                    <th>Entity</th>
                                                    <th>Application Number</th>
                                                    <th>Applicant Date</th>
                                                    <th>tradmark|Wordmark</th>
                                                    <th>Nature Of <br>The Application</th>
                                                    <th>Application Field</th>
                                                    <th>Classes</th>
                                                    <th>Category <br> Of Mark</th>
                                                    <th>Applicant Agent</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            
                                                 @foreach($showes as $show)
                                                 @if($show->status == 0)
                                                  <tr>
                                                    <td>{{$show->id}}</td>
                                                    <td><img src="{{asset('public/logo/'.$show->logo)}}" alt="" height="50px"></td>
                                                    <td>ABC Limited</td>
                                                    <td>{{$show->app_no}}</td>
                                                    <td>{{$show->app_date}}</td>
                                                    <td>{{$show->tradmark}}</td>
                                                    <td>{{$show->series_marks}}</td>
                                                   <td>{{$show->app_fiel}}</td>
                                                    <td>@foreach(explode(',', $show->class_categorys) as $info)
                                      <span>{{ $info }}</span>
                                  @endforeach</td>

                                                    <td>{{$show->category_of_mark}}</td>
                                                    <td>{{$show->agent_name}}</td>
                                                    <td>{{$show->update_status}}</td>
                                                    <td><a href="{{url('edit_summary/'.$show->id)}}" type="edit" class="btn btn-warning">Edit</a><br><br> <a href="{{url('/add_tradmark/'.$show->id.'/update_status')}}" class="btn btn-info">Update</a><br><br>
                                                      <button type="delete" class="btn btn-danger" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter{{$show->id}}">Disable</button>

  <div class="modal fade" id="exampleModalCenter{{$show->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
       Are you sure you want to disable this {{$show->tradmark}} tradmark.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <form action="{{url('update/status')}}" method="POST" accept-charset="utf-8">
            @csrf
            <input type="hidden" name="id" value="{{$show->id}}">
            <input type="hidden" name="status" value="1">
             <button type="delete" class="btn btn-danger">Confirm</button>
        </form>
      </div>
    </div>
  </div>
</div>

                                                      <br><br>
                                                    <a href="{{url('/tradmark/'.$show->id.'/show')}}" class="btn btn-primary" href="">View</a><br><br>
                                                    <a href="{{url('/action/'.$show->id.'/action_item')}}" class="btn btn-success" href="">Action</a>
                                                  </td>
                                                  </tr>
                                                  @endif
                                                    @endforeach
                                                    


                                                    
                                                 
                                          </tbody>
                                                                      </table>



                                                                      
                                </div>
                    




                </div>

               
    

                
            </div>
                   </div>













<script type="text/javascript">

  var x = document.getElementById("myTable").rows.length - 1;
  document.getElementById("demo").innerHTML = "Result : Show Total Rows Is " + x;

  </script>      

  <script type="text/javascript">
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
          
        });

  </script>         
  @include('include.footer')                