    <style>
        .dataTables_filter {
display: none; 
}
.paginate_button{
    display: none;
}
.dataTables_info{
    display: none;
}
    </style>
    <div class="modal  fade bs-example-modal-lg" id="bootstrap-modal" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
                                          <div class="modal-dialog modal-xl">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">tradmark Details</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                              <div class="modal-body" style="overflow-x: auto;">
                                              <div id="demo-modal">
                                                <div class="table-responsive">
    <table id="example23" class="table table-striped table-bordered " >
        <thead>
            <tr>
                                                  <th>Sr. No</th>
                                                    <th>Logo</th>
                                                    <th>Entity</th>
                                                    <th>Application Number</th>
                                                    <th>Applicant Date</th>
                                                    <th>tradmark Name</th>
                                                    <th>Nature Of <br>The Application</th>
                                                    <th>Application Field</th>
                                                    <th>Classes</th>
                                                    <th>Priority Country</th>
                                                    <th>Category <br> Of Mark</th>
                                                    <th>Applicant Agent</th>
                                                    <th>Status</th>
                                            </tr>
        </thead>
        <tbody>
             @foreach($results as $result)
             @if($result->update_status == $status_name  && $result->entitty == $com_name)
                                                  <tr>
                                                    <td>{{$result->id}}</td>
                                                    <td><img src="{{asset('public/logo/'.$result->logo)}}" alt="" height="50px"></td>
                                                    <td>ABC Limited</td>
                                                    <td>{{$result->app_no}}</td>
                                                    <td>{{$result->app_date}}</td>
                                                    <td>{{$result->tradmark}}</td>
                                                    <td>{{$result->series_marks}}</td>
                                                   <td>{{$result->app_fiel}}</td>
                                                    <td>@foreach(explode(',', $result->class_categorys) as $info)
                                      <span>{{ $info }}</span>
                                  @endforeach</td>
                                                    <td>{{$result->name_organization}}</td>
                                                    <td>{{$result->category_of_mark}}</td>
                                                    <td>{{$result->agent_name}}</td>
                                                    <td>{{$result->update_status}}</td>
                                                    
                                                  </tr>
                                                  
            
                                                  @endif
                                                    @endforeach
                                                    
                                                    
                    
                                                    
                                                    
                                                    
        </tbody>
    </table>
</div>
                                               </div>  
                                            </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>


                                        <script type="text/javascript">
    $(function () {
            
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
       {
           extend: 'pdf',
           footer: true,
           title: 'tradmark Reports export',
           orientation: 'landscape',
           pageSize: 'LEGAL',
           exportOptions: {
                columns: [0,2,3,4,5,6,7,8,9,10,11,12]
            }
       }        
    ]  
    } );
            $('.buttons-pdf').addClass('btn btn-primary mr-1');
        });
  </script>
                                      