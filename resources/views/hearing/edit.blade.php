@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
    <div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Create New | <a href="{{url('summary/hearing',$hear->hear_id)}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Create New</li>
                        </ol>
                    </div>
                </div>
            </div>

            @include('include.message')
            <div class="card">
    			<div class="card-body">
    				{!! Form::model($hear,['url' => ['headering/store',$hear->id],'data-parsley-validate' => '','method' => 'POST','files' => true]) !!}
    				<div class="row">
    					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            			{{ Form::label('date','Date:') }}
            			{{ Form::text('date',null,['class' => 'form-control','id' => 'dated']) }}
          			</div>

          			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            			{{ Form::label('her_date','Next Hearing Dat*:') }}
            			{{ Form::text('her_date',null,['class' => 'form-control','required' => '','id' => 'heard']) }}
          			</div>

          			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            			{{ Form::label('alert1','First Alert:') }}
            			{{ Form::text('alert1',null,['class' => 'form-control','id' => 'remd1']) }}
          			</div>

          			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            			{{ Form::label('alert2','Second Alert:') }}
            			{{ Form::text('alert2',null,['class' => 'form-control','id' => 'remd2']) }}
          			</div>

          			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            			{{ Form::label('alert3','Third Alert:') }}
            			{{ Form::text('alert3',null,['class' => 'form-control','id' => 'remd3']) }}
          			</div>

          			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            			{{ Form::label('email','Additional Email Id:') }}
            			{{ Form::email('email',null,['class' => 'form-control']) }}
          			</div>

          			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            			{{ Form::label('stage_desc','Stage Description:*') }}
            			{{ Form::textarea('stage_desc',null,['class' => 'form-control','required' => '']) }}
          			</div>

          			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            			{{ Form::label('act_item','Action Item:') }}
            			{{ Form::textarea('act_item',null,['class' => 'form-control']) }}
          			</div>

          			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <input type="hidden" name="hear_id" value="{{ $hear->id }}">
            			{{ Form::label('user_id','Responsible Person :') }}
            			{{ Form::select('user_id',$ar1,null,['class' => 'form-control','placeholder' => 'Select','required' => '']) }}
         			 </div>

         			 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            			{{ Form::label('ext_counsel_id','External Counsel:') }}
            			{{ Form::select('ext_counsel_id',$ar2,null,['class' => 'form-control','placeholder' => 'Select']) }}
          			</div>
			          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
			            {{ Form::label('law_firm_id','Advocate Law Firm:') }}
			            {{ Form::select('law_firm_id',$ar3,null,['class' => 'form-control','placeholder' => 'Select']) }}
			          </div>

			          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
            			{{ Form::label('due_date','Action item due Date:*') }}
            			{{ Form::text('due_date',null,['class' => 'form-control','required' => '','id' =>'dued']) }}
          			</div>

          			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        {{ Form::label('docs','Document:') }}
                        @if ($hear->docs !='')
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                @foreach (explode(',',$hear->docs) as $item)
                                    <tr>
                                        <td><a href="{{ url('public/litigation/summary/hearing/'.$item) }}" download="">{{ $item }}</a></td>
                                        <td><a href="{{ url('delete/litiimg/'.$item.'/'.$hear->id) }}" class="btn btn-sm btn-danger">Delete</a></td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        @else
            			{{ Form::file('docs[]',['class' => 'form-control','multiple' => true]) }}
                        @endif

          			</div>

          			<div class="col-md-12">
              			<div class="form-group text-center">
            				<button type="submit" name="submit" class="btn btn-success">Create</button>
          				</div>
            		</div>
    				</div>


    				{!! Form::close() !!}
    			</div>
    		</div>
		</div>
	</div>

@include('include.footer')
<script>
    $(function() {
        $('#heard').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            //maxDate: 0,
            //minDate: +1,
        });

        $('#dated').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
            //minDate: +1,
        });

        $('#remd1').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            //maxDate: 0,
            minDate: +1,
        });

        $('#remd2').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            //maxDate: 0,
            minDate: +1,
        });

        $('#remd3').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            //maxDate: 0,
            minDate: +1,
        });
        $('#dued').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            //maxDate: 0,
            minDate: +1,
        });
    });
</script>
