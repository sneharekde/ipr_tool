@include('include.header')

    <style>
        .card{
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Update | <a href="{{ route('summary.index') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>
            @include('include.message')
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('/summary/'.$summ->id) }}">Litigation Details</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link active"  href="{{ url('/summary/hearing/'.$summ->id) }}">Next Hearing and Stage</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('/summary/documents/'.$summ->id) }}">Documents</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/summary/fees/'.$summ->id) }}">Fees</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/summary/completion/'.$summ->id) }}">Completion</a>
                        </li>
                    </ul>
                    <div class="row" style="margin-top: 10px">
                        @if ($summ->status == 'Pending')
                        <div class="col-md-12">
                            <a href="{{ url('hearing/create/'.$summ->id) }}" class="btn btn-info">Next Hearing And Stage</a>
                        </div>
                        @endif

                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Responsible Person</th>
                                        <th>Counsel Name</th>
                                        <th>Alert Dates</th>
                                        <th>Stage Description</th>
                                        <th>Document</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($hears as $hear)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ date('d-m-Y',strtotime($hear->date)) }}</td>
                                            <td>{{ $hear->user->first_name }}</td>
                                            <td>{{ $hear->ext_counsel->counsel }}</td>
                                            <td>
                                                First:{{ date('d-m-Y',strtotime($hear->alert1)) }}<br>
                                                Sectond:{{ date('d-m-Y',strtotime($hear->alert2)) }}<br>
                                                Third:{{ date('d-m-Y',strtotime($hear->alert3)) }}<br>
                                            </td>
                                            <td>{{ $hear->stage_desc }}</td>
                                            <td>
                                                @foreach(explode(',', $hear->docs) as $file)
                                                    <a href="{{url('public/litigation/summary/hearing',$file)}}" download="">{{ $file }}</a>
                                                @endforeach
                                            </td>
                                            <td>
                                                <a href="{{ url('hearing/edit',$hear->id) }}" class="btn btn-success btn-sm">Edit</a>
                                                <a href="{{ url('delete/hearing',$hear->id) }}" class="btn btn-danger btn-sm">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
