@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">City | <a href="{{ route('cities.index') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active">City</li>
                        </ol>
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-rounded" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ $message }}
                </div>
            @endif
            <div class="card">
                <div class="card-body">
                    {!! Form::open(['route' => 'cities.store','method' => 'POST','data-parsley-validate']) !!}
                        <div class="row">
                            <div class="col-md-3 form-group">
                                {!! Form::label('state_id', 'State') !!}
                                {!! Form::select('state_id', $ar1, null, ['class' => 'form-control','required' => '','placeholder' => 'Select State']) !!}
                            </div>
                            <div class="col-md-3 form-group">
                                {!! Form::label('city', 'City Name') !!}
                                {!! Form::text('city',  null, ['class' => 'form-control','required' => '']) !!}
                            </div>
                            <div class="col-md-3 form-group">
                                {!! Form::label('latitude', 'Latitude') !!}
                                {!! Form::text('latitude',  null, ['class' => 'form-control','required' => '','data-parsley-type' => 'number']) !!}
                            </div>
                            <div class="col-md-3 form-group">
                                {!! Form::label('longitude', 'Longitude') !!}
                                {!! Form::text('longitude',  null, ['class' => 'form-control','required' => '','data-parsley-type' => 'number']) !!}
                            </div>
                            <div class="col-md-12 form-group">
                                <div class="text-center">
                                    {!! Form::submit('submit', ['class' =>'btn  btn-success']) !!}
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
