@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
<!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
              <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">External Counsels | <a href="{{route('ex_counsel.index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                                <li class="breadcrumb-item active">External Counsels</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->

                @include('include.message')
                <div class="row">
                <div class="col-md-12">
                  <div class="card">
                  <div class="card-body">
                    {!! Form::open(['route' => 'ex_counsel.store', 'data-parsley-validate' => '','method' => 'POST']) !!}
                    <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        {{ Form::label('country_id','Country:') }}
                        {!! Form::select('country_id', $ar1, null,['class' => 'form-control','placeholder' => 'Select Country','required']) !!}
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        {{ Form::label('state_id','State:') }}
                        <select id="state_id" name ="state_id" class="form-control" required="">
                            <option value="">Select State</option>
                        </select>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        {{ Form::label('city','City:') }}
                        {{ Form::text('city',null,['class' => 'form-control' ,'required' => '']) }}
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        {{ Form::label('law_id','Law Firm:') }}
                        {{ Form::select('law_id',$law,null,['class' => 'form-control','placeholder' => 'Select Law Firm','required' => '']) }}
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        {{ Form::label('counsel','Counsel Name:') }}
                        {{ Form::text('counsel',null,['class' => 'form-control','required' => '']) }}
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        {{ Form::label('number','Contact No:') }}
                        {{ Form::text('number',null,['class' => 'form-control','required' => '','data-parsley-type'=>'digits','maxlength' => '10']) }}
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        {{ Form::label('email','Email Id:') }}
                        {{ Form::email('email',null,['class' => 'form-control','data-parsley-type' => 'email','required']) }}
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        {{ Form::label('expert_id','Area of Expertise:') }}
                        {{ Form::select('expert_id',$area,null,['class' => 'form-control','placeholder' => 'Select Area of Expertise']) }}
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                        {{ Form::label('address','Address:') }}
                        {{ Form::textarea('address',null,['class' => 'form-control']) }}
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        {{ Form::submit('Submit',['class' => 'btn btn-success']) }}
                      </div>
                    </div>
                    {!! Form::close() !!}
                  </div>
                </div>
                </div>

                </div>





            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
  @include('include.footer')
  <script>
    $(document).ready(function(){
        $('#country_id').change(function(){
            var country_ids = $(this).val();
            console.log(country_ids);
            $('#state_id').find('option').not(':first').remove();
            $.ajax({
                url:'{!! URL::to('findstate') !!}',
                type:'GET',
                data:{'id':country_ids},
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                dataType:'json',
                success:function(data){
                //    console.log(data);
                var leng = data.length;
                    for(var i=0; i<leng; i++){
                        var id = data[i]['id'];
                        var name = data[i]['name'];
                        $('#state_id').append("<option value='"+id+"'>"+name+"</option>");
                    }
                }
            });
        });
    });
</script>
