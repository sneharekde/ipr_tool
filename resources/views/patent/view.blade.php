@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
				<div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Show Patent | <a href="{{action('PatentController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a>
                    </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                	<div class="d-flex justify-content-end align-items-center">
                		<ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Show Patent</li>
                        </ol>
                	</div>
                </div>
			</div>
			<div class="card">
				<div class="card-body">
					<table class="table table-bordered">
						<tr>
                            <th width="400px" align="left">Priority Date:</th>
                            <td>{{ date('d-m-Y',strtotime($pat->priority_date)) }}</td>
                        </tr>
                        <tr>
                            <th width="400px" align="left">Filing Date:</th>
                            <td>{{ date('d-m-Y',strtotime($pat->filing_date)) }}</td>
                        </tr>
                        <tr>
                            <th width="400px" align="left">Application No:</th>
                            <td>{{ $pat->application_no }}</td>
                        </tr>
                        <tr>
                            <th width="400px" align="left">Application Identity No.:</th>
                            <td>{{ $pat->applicant_id }}</td>
                        </tr>
                        <tr>
                            <th width="400px" align="left">Applicant Name:</th>
                            <td>{{ $pat->applicant_name }}</td>
                        </tr>
                        <tr>
                            <th width="400px" align="left">Inventor Name:</th>
                            <td>{{ $pat->invent_name }}</td>
                        </tr>
                        <tr>
                            <th width="400px" align="left">Title of invention:</th>
                            <td>{{ $pat->title_inven }}</td>
                        </tr>
                        <tr>
                            <th width="400px" align="left">Government Fee:</th>
                            <td>{{ $pat->gov }}</td>
                        </tr>
                        <tr>
                            <th width="400px" align="left">Category of Applicant:</th>
                            <td>{{ $pat->cat_app }} @if($pat->cat_app_s !='')  ->  {{ $pat->cat_app_s }} @endif</td>
                        </tr>
                        <tr>
                            <th width="400px" align="left">Status of Application:</th>
                            <td>{{ $pat->status }}</td>
                        </tr>
                        <tr>
                            <th width="400px" align="left">Registered Patent agent name:</th>
                            <td>{{ $pat->reg_pat}}</td>
                        </tr>
                        <tr>
                            <th width="400px" align="left">PCT application no.:</th>
                            <td>
                                @if ($pat->int_app_no !='')
                                {{ $pat->int_app_no }}
                                @else
                                NA
                                @endif
                                </td>
                        </tr>
                        <tr>
                            <th width="400px" align="left">Upload Documents:</th>
                            <td>
                                @if ($pat->file !='')
                                    <a href="{{ url('public/patent/docs/'.$pat->file) }}" download="">{{ $pat->file }}</a>
                                @else
                                    <a href="{{ url('patent/upload/docs',$pat->id) }}" class="btn btn-sm btn-success">Upload Document</a>
                                @endif
                            </td>
                        </tr>
					</table>
				</div>
			</div>
		</div>
	</div>
@include('include.footer')
