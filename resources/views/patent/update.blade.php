@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Update Status of Patent | <a href="{{ url('patent/show') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Update Status of Copyright</li>
                        </ol>
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="card">
                <div class="card-body">
                    <form action="{{ url('/patent/save/status') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    @csrf
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="hidden" name="pat_id" value="{{ $patent->id }}">
                                <label>Status:</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="">Select Status</option>
                                    <option value="Active Application">Active application</option>
                                    <option value="Registered">Registered</option>
                                    <option value="Revoked">Revoked</option>
                                    <option value="Termination">Termination</option>
                                    <option value="Restored">Restored</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="sub_status" style="display: none">
                            <div class="form-group">
                                <label>Sub Status:</label>
                                <select class="form-control" id="sub_status1" name="sub_status">
                                    <option value="">Select Sub Status</option>
                                    <option value="Specification Stage">Specification Stage</option>
                                    <option value="PCT Application">PCT Application</option>
                                    <option value="Publication">Publication</option>
                                    <option value="Pre-Grant">Pre-Grant</option>
                                    <option value="Examination">Examination</option>
                                    <option value="Post-Grant">Post-Grant</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="stage1" style="display: none">
                            <div class="form-group">
                                <label>Stage1:</label>
                                <select class="form-control" id="select1" name="stage">
                                    <option value="">Select Stage1</option>
                                    <option value="Provisional Specification">Provisional Specification</option>
                                    <option value="Complete Specification">Complete Specification</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="stage2" style="display: none">
                            <div class="form-group">
                                <label>Stage2:</label>
                                <select class="form-control" id="select2" name="stage">
                                    <option value="">Select Stage2</option>
                                    <option value="International Phases">International Phases</option>
                                    <option value="National Phases">National Phases</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="stage3" style="display: none">
                            <div class="form-group">
                                <label>Stage3:</label>
                                <select class="form-control" id="select3" name="stage">
                                    <option value="">Select Stage3</option>
                                    <option value="Early Publication">Early Publication</option>
                                    <option value="Request Publication">Request Publication</option>
                                    <option value="Published">Published</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="stage4" style="display: none">
                            <div class="form-group">
                                <label>Stage4:</label>
                                <select class="form-control" id="select4" name="stage">
                                    <option value="">Select Stage4</option>
                                    <option value="Mandatory Waiting Period">Mandatory Waiting Period</option>
                                    <option value="Pre-grant Opposition">Pre-grant Opposition</option>
                                    <option value="Reply to pre-grant opposition">Reply to pre-grant opposition</option>
                                    <option value="Noticed received">Noticed received</option>
                                    <option value="Hearing">Hearing</option>
                                    <option value="Order Passed">Order Passed</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="stage5" style="display: none">
                            <div class="form-group">
                                <label>Stage5:</label>
                                <select class="form-control" id="select5" name="stage">
                                    <option value="">Select Stage5</option>
                                    <option value="Early Examination">Early Examination</option>
                                    <option value="Request for examination">Request for examination</option>
                                    <option value="FER">FER</option>
                                    <option value="Reply to Fer">Reply to Fer</option>
                                    <option value="Hearing (Examination)">Hearing (Examination)</option>
                                    <option value="Order Passed(Examination)">Order Passed(Examination)</option>
                                    <option value="Appeal Order Passed (Examination)">Appeal Order Passed (Examination)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="stage6" style="display: none">
                            <div class="form-group">
                                <label>Stage6:</label>
                                <select class="form-control" id="select6" name="stage">
                                    <option value="">Select Stage6</option>
                                    <option value="Publishing in Journal (Post Grant)">Publishing in Journal (Post Grant)</option>
                                    <option value="Mandatory Waiting Period (Post Grant Opposition)">Mandatory Waiting Period (Post Grant Opposition)</option>
                                    <option value="Post-grant opposition">Post-grant opposition</option>
                                    <option value="Reply to Post Grant Opposition">Reply to Post Grant Opposition</option>
                                    <option value="Noticed received (Post Grant opposition)">Noticed received (Post Grant opposition)</option>
                                    <option value="Hearing (Post Grant Opposition)">Hearing (Post Grant Opposition)</option>
                                    <option value="Post Grant Ordered Passed">Post Grant Ordered Passed</option>
                                    <option value="Appeal(Post Grant Opposition)">Appeal(Post Grant Opposition)</option>
                                    <option value="Appeal Order Passed (Post-Grant Opposition)">Appeal Order Passed (Post-Grant Opposition)</option>
                                </select>
                            </div>
                        </div>
                        <!-- Visible Fields for sub status -->
                        <div id="div1" style="display: none">
                            <h4 class="text-center">Specification Stage</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div id="div2" style="display: none">
                            <h4 class="text-center">Complete Specification</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div id="div3" style="display: none">
                            <h4 class="text-center">International Phase</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div id="div4" style="display: none">
                            <h4 class="text-center">National Phase</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div id="div5" style="display: none">
                            <h4 class="text-center">Early Publication</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div id="div6" style="display: none">
                            <h4 class="text-center">Request Publication</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div id="div7" style="display: none">
                            <h4 class="text-center">Published</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="div8" style="display: none">
                            <h4 class="text-center">Mandatory Waiting Period (Pre-Grant)</h4>
                        </div>

                        <div id="div9" style="display: none">
                            <h4 class="text-center">Pre-Grant opposition</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="div10" style="display: none">
                            <h4 class="text-center">Reply to pre-grant opposition</h4>
                        </div>

                        <div id="div11" style="display: none">
                            <h4 class="text-center">Noticed received (Pre-Grant Opposition)</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div id="div12" style="display: none">
                            <h4 class="text-center">Hearing(Pre-Grant opposition)</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="div13" style="display: none">
                            <h4 class="text-center">Order Passed (Pre-Grant opposition)</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="div14" style="display: none">
                            <h4 class="text-center">Early Examination</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div id="div15" style="display: none">
                            <h4 class="text-center">Request for examination</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="div16" style="display: none">
                            <h4 class="text-center">FER</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="div17" style="display: none">
                            <h4 class="text-center">Reply to FER</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="div18" style="display: none">
                            <h4 class="text-center">Hearing (Examination)</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="div19" style="display: none">
                            <h4 class="text-center">Appeal (Examination)</h4>
                        </div>

                        <div id="div20" style="display: none">
                            <h4 class="text-center">Appeal Order Passed (Examination)</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="div21" style="display: none">
                            <h4 class="text-center">Publishing in Journal (Post-Grant)</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="div22" style="display: none">
                            <h4 class="text-center">Mandatory Waiting period (Post-Grant opposition)</h4>
                        </div>

                        <div id="div23" style="display: none">
                            <h4 class="text-center">Post-Grant opposition</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="div24" style="display: none">
                            <h4 class="text-center">Reply Post-Grant opposition</h4>
                        </div>

                        <div id="div25" style="display: none">
                            <h4 class="text-center">Noticed received (Post-Grant Opposition)</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div id="div26" style="display: none">
                            <h4 class="text-center">Hearing (Post-Grant opposition)</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="div27" style="display: none">
                            <h4 class="text-center">Post-Grant Order Passed</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="div28" style="display: none">
                            <h4 class="text-center">Appeal(Post-Grant opposition)</h4>
                        </div>

                        <div id="div29" style="display: none">
                            <h4 class="text-center">Appeal order Passed(Post-Grant opposition)</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="reg" style="display: none">
                            <h4 class="text-center">Registered</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="rev" style="display: none">
                            <h4 class="text-center">Revoked</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Input Date By User:</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div id="term" style="display: none">
                            <h4 class="text-center">Termination</h4>
                        </div>

                        <div id="rest" style="display: none">
                            <h4 class="text-center">Patent Restoration</h4>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload Docs:</label>
                                    <input type="file" name="docs" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Comments:</label>
                                    <textarea name="comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <!-- New application -->

                        <!-- End of the status and sub status -->
                        <div class="col-sm-12 col-xs-12 col-lg-12 col-md-12">
                            <div class="form-group text-center">
                                <input type="submit" name="submit" value="Submit" class="btn btn-success">
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Document</th>
                        <th>Status</th>
                        <th>Sub Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach($ups as $up)
                        <tr>
                        <td>{{ $i++ }}</td>
                        <td>
                            @if ($up->date !='')
                            {{ date('d-m-Y',strtotime($up->date)) }}
                            @else

                            @endif
                        </td>
                        <td>
                                <a href="{{ url('public/patent/docs'.$up->docs) }}" download="">{{ $up->docs }}</a>
                            </td>
                        <td>{{ $up->status }}</td>
                        <td>{{ $up->sub_status }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
          </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>

        <script>
            $(document).ready(function(){
                $('#status').on('change',function(){
                    var status = $(this).val();
                    if(status == ''){
                        $('#sub_status').hide();
                        $('#reg').hide();
                        $('#rev').hide();
                        $('#term').hide();
                        $('#rest').hide();

                        $('#stage1').hide();
                        $('#stage2').hide();
                        $('#stage3').hide();
                        $('#stage4').hide();
                        $('#stage5').hide();
                        $('#stage6').hide();

                        $('#div1').hide();
                        $('#div2').hide();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(status == 'Active Application'){
                        $('#sub_status').show();
                        $('#reg').hide();
                        $('#rev').hide();
                        $('#term').hide();
                        $('#rest').hide();

                        $('#stage1').hide();
                        $('#stage2').hide();
                        $('#stage3').hide();
                        $('#stage4').hide();
                        $('#stage5').hide();
                        $('#stage6').hide();

                        $('#div1').hide();
                        $('#div2').hide();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }

                    if(status == 'Registered'){
                        $('#reg').show();
                        $('#rev').hide();
                        $('#term').hide();
                        $('#rest').hide();
                        $('#sub_status').hide();

                        $('#stage1').hide();
                        $('#stage2').hide();
                        $('#stage3').hide();
                        $('#stage4').hide();
                        $('#stage5').hide();
                        $('#stage6').hide();

                        $('#div1').hide();
                        $('#div2').hide();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();

                    }
                    if(status == 'Revoked'){
                        $('#rev').show();
                        $('#reg').hide();
                        $('#term').hide();
                        $('#rest').hide();
                        $('#sub_status').hide();

                        $('#stage1').hide();
                        $('#stage2').hide();
                        $('#stage3').hide();
                        $('#stage4').hide();
                        $('#stage5').hide();
                        $('#stage6').hide();

                        $('#div1').hide();
                        $('#div2').hide();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(status == 'Termination'){
                        $('#term').show();
                        $('#reg').hide();
                        $('#rev').hide();
                        $('#rest').hide();
                        $('#sub_status').hide();

                        $('#stage1').hide();
                        $('#stage2').hide();
                        $('#stage3').hide();
                        $('#stage4').hide();
                        $('#stage5').hide();
                        $('#stage6').hide();

                        $('#div1').hide();
                        $('#div2').hide();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(status == 'Restored'){
                        $('#rest').show();
                        $('#reg').hide();
                        $('#rev').hide();
                        $('#term').hide();
                        $('#sub_status').hide();

                        $('#stage1').hide();
                        $('#stage2').hide();
                        $('#stage3').hide();
                        $('#stage4').hide();
                        $('#stage5').hide();
                        $('#stage6').hide();

                        $('#div1').hide();
                        $('#div2').hide();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                });

                $('#sub_status1').on('change',function(){
                    var sub_status = $(this).val();
                    if(sub_status == ''){
                        $('#stage1').hide();
                        $('#stage2').hide();
                        $('#stage3').hide();
                        $('#stage4').hide();
                        $('#stage5').hide();
                        $('#stage6').hide();

                        $('#div1').hide();
                        $('#div2').hide();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();

                    }
                    if(sub_status == 'Specification Stage'){
                        $('#stage1').show();
                        $('#stage2').hide();
                        $('#stage3').hide();
                        $('#stage4').hide();
                        $('#stage5').hide();
                        $('#stage6').hide();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(sub_status == 'PCT Application'){
                        $('#stage2').show();
                        $('#stage3').hide();
                        $('#stage4').hide();
                        $('#stage5').hide();
                        $('#stage6').hide();
                        $('#stage1').hide();

                        $('#div1').hide();
                        $('#div2').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(sub_status == 'Publication'){
                        $('#stage3').show();
                        $('#stage4').hide();
                        $('#stage5').hide();
                        $('#stage6').hide();
                        $('#stage1').hide();
                        $('#stage2').hide();

                        $('#div1').hide();
                        $('#div2').hide();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(sub_status == 'Pre-Grant'){
                        $('#stage4').show();
                        $('#stage5').hide();
                        $('#stage6').hide();
                        $('#stage1').hide();
                        $('#stage2').hide();
                        $('#stage3').hide();

                        $('#div1').hide();
                        $('#div2').hide();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(sub_status == 'Examination'){
                        $('#stage5').show();
                        $('#stage1').hide();
                        $('#stage2').hide();
                        $('#stage3').hide();
                        $('#stage4').hide();
                        $('#stage6').hide();

                        $('#div1').hide();
                        $('#div2').hide();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(sub_status == 'Post-Grant'){
                        $('#stage6').show();
                        $('#stage1').hide();
                        $('#stage2').hide();
                        $('#stage3').hide();
                        $('#stage4').hide();
                        $('#stage5').hide();

                        $('#div1').hide();
                        $('#div2').hide();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                });

                $('#select1').on('change',function(){
                    var select1 = $(this).val();
                    if(select1 == ''){
                        $('#div1').hide();
                        $('#div2').hide();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select1 == 'Provisional Specification'){
                        $('#div1').show();
                        $('#div2').hide();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select1 == 'Complete Specification'){
                        $('#div1').hide();
                        $('#div2').show();

                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();


                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                });

                $('#select2').on('change',function(){
                    var select2 = $(this).val();
                    if(select2 == ''){
                        $('#div3').hide();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select2 == 'International Phases'){
                        $('#div3').show();
                        $('#div4').hide();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select2 == 'National Phases'){
                        $('#div3').hide();
                        $('#div4').show();

                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                });

                $('#select3').on('change',function(){
                    var select3 = $(this).val();
                    if(select3 == ''){
                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select3 == 'Early Publication'){
                        $('#div5').show();
                        $('#div6').hide();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select3 == 'Request Publication'){
                        $('#div5').hide();
                        $('#div6').show();
                        $('#div7').hide();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select3 == 'Published'){
                        $('#div5').hide();
                        $('#div6').hide();
                        $('#div7').show();

                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                });

                $('#select4').on('change',function(){
                    var select4 = $(this).val();
                    if(select4 == ''){
                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select4 == 'Mandatory Waiting Period'){
                        $('#div8').show();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select4 == 'Pre-grant Opposition'){
                        $('#div8').hide();
                        $('#div9').show();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select4 == 'Reply to pre-grant opposition'){
                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').show();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select4 == 'Noticed received'){
                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').show();
                        $('#div12').hide();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select4 == 'Hearing'){
                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').show();
                        $('#div13').hide();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select4 == 'Order Passed'){
                        $('#div8').hide();
                        $('#div9').hide();
                        $('#div10').hide();
                        $('#div11').hide();
                        $('#div12').hide();
                        $('#div13').show();

                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                });

                $('#select5').on('change',function(){
                    var select5 = $(this).val();
                    if(select5 == ''){
                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select5 == 'Early Examination'){
                        $('#div14').show();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select5 == 'Request for examination'){
                        $('#div14').hide();
                        $('#div15').show();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select5 == 'FER'){
                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').show();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select5 == 'Reply to Fer'){
                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').show();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select5 == 'Hearing (Examination)'){
                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').show();
                        $('#div19').hide();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select5 == 'Order Passed(Examination)'){
                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').show();
                        $('#div20').hide();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select5 == 'Appeal Order Passed (Examination)'){
                        $('#div14').hide();
                        $('#div15').hide();
                        $('#div16').hide();
                        $('#div17').hide();
                        $('#div18').hide();
                        $('#div19').hide();
                        $('#div20').show();

                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                });

                $('#select6').on('change',function(){
                    var select6 = $(this).val();
                    if(select6 == ''){
                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select6 == 'Publishing in Journal (Post Grant)'){
                        $('#div21').show();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select6 == 'Mandatory Waiting Period (Post Grant Opposition)'){
                        $('#div21').hide();
                        $('#div22').show();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select6 == 'Post-grant opposition'){
                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').show();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select6 == 'Reply to Post Grant Opposition'){
                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').show();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select6 == 'Noticed received (Post Grant opposition)'){
                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').show();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select6 == 'Hearing (Post Grant Opposition)'){
                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').show();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select6 == 'Post Grant Ordered Passed'){
                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').show();
                        $('#div28').hide();
                        $('#div29').hide();
                    }
                    if(select6 == 'Appeal(Post Grant Opposition)'){
                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').show();
                        $('#div29').hide();
                    }
                    if(select6 == 'Appeal Order Passed (Post-Grant Opposition)'){
                        $('#div21').hide();
                        $('#div22').hide();
                        $('#div23').hide();
                        $('#div24').hide();
                        $('#div25').hide();
                        $('#div26').hide();
                        $('#div27').hide();
                        $('#div28').hide();
                        $('#div29').show();
                    }
                });

            });
        </script>
    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

@include('include.footer')


