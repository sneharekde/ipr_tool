@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
  <div class="page-wrapper">
    <div class="container-fluid">
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Patent Portfolio | <a href="{{action('PatentController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Add Patent</li>
            </ol>
          </div>
        </div>
      </div>
       @include('include.message')
            {{-- Add New --}}
      <div class="card">
        <div class="card-body">
          <form action="{{ url('/patent/submit') }}" data-parsley-validate="" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Priority Date *:</label>
                  <input type="text" id="prd" name="priority_date" class="form-control" required="">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Filing Date :</label>
                  <input type="text" id="fild" name="filing_date" class="form-control" required="">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Application No. :</label>
                  <input type="text" name="application_no" class="form-control" required="">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Applicant Name :</label>
                  <input type="text" name="applicant_name" class="form-control" required="">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Applicant Identity No. :</label>
                  <input type="text" name="applicant_id" class="form-control" required="">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Category of Applicant :</label>
                  <select class="form-control" name="cat_app" id="select2" required="">
                    <option value="">Select</option>
                    <option value="natural person">Natural person</option>
                    <option value="other">Other than natural person</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="choose2">
                <div class="form-grou" >
                  <label>Select Options</label>
                  <select name="cat_app_s" class="form-control">
                    <option value="">Select</option>
                    @foreach($cats as $cat)
                    <option value="{{ $cat->name }}">{{ $cat->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Inventor Name :</label>
                  <input type="text" name="invent_name" class="form-control" required="">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Title of invention :</label>
                  <input type="text" name="title_inven" class="form-control" required="">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Registered Patent agent name :</label>
                  <input type="text" name="reg_pat" class="form-control" required="">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>In case of PCT:</label>
                  <select name="pct" class="form-control" id="select">
                    <option value="">Select</option>
                    <option value="no">No</option>
                    <option value="yes">Yes</option>
                  </select>
                </div>
              </div>
              <div id="choose" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label>International Application No.:</label>
                      <input type="text" name="int_app_no" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label>International Filing Date :</label>
                      <input type="text" id="infdd" name="int_fili_date" class="form-control">
                    </div>
                  </div>
                  </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Renewal Date:</label>
                  <input type="text" id="renewd" name="renewal" class="form-control" required="">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Govt. fees</label>
                  <input type="text" name="gov" class="form-control" required="">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>User:</label>
                  <select class="form-control" name="user_id" required="">
                    <option value="">Select</option>
                    @foreach($users as $user)
                    <option value="{{ $user->id }}">{{ $user->first_name .' '. $user->last_name   }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Upload Documents:</label>
                  <input type="file" name="file" class="form-control">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="text-center">
                  <input type="submit" name="submit" value="Add Patent" class="btn btn-success">
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $(function(){
      $('#choose').hide();
      $('#select').change(function(){
        if ($('#select').val() == 'yes') {
          $('#choose').show();
        }else{
          $('#choose').hide();
        }
      });
    });

    $(function(){
      $('#choose2').hide();
      $('#select2').change(function(){
        if ($('#select2').val() == 'other') {
          $('#choose2').show();
        }else{
          $('#choose2').hide();
        }
      });
    });
  </script>

  <script>
    $(function() {
        $('#prd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
            //minDate: +1,
        });
        $('#fild').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
            //minDate: +1,
        });
        $('#renewd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            //maxDate: 0,
            minDate: +1,
        });

        infdd
        $('#infdd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
            //minDate: +1,
        });
    });
</script>

@include('include.footer')
