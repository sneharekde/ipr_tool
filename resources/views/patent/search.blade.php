@include('include.header')
   <div class="page-wrapper">
    <div class="container-fluid">
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Patent Portfolio | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4> 
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Patent Portfolio</li>
                </ol>
                <a  href="{{action('PatentController@Create')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add Patent</a>
            </div>
        </div>
      </div>

      @if ($message = Session::get('success'))
        <div class="alert alert-success alert-rounded" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          {{ $message }}
        </div>
      @endif
{{-- Search Filter --}}
      <div class="card">
        <form action="{{ url('/patent/show') }}" method="POST">
          @csrf
          <div class="card-body">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Prority Date:</label>
                  <input type="date" name="priority_date" class="form-control">
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Filing Date:</label>
                  <input type="date" name="filing_date" class="form-control">
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Application No. :</label>
                  <input type="text" name="application_no" class="form-control">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Applicant Name:</label>
                  <input type="text" name="applicant_name" class="form-control">
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Applicant Identity No.:</label>
                  <input type="text" name="applicant_id" class="form-control">
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Category Of Applicant :</label>
                  <select class="form-control" name="cat_app" id="select2">
                    <option value="">Select</option>
                    <option value="natural person">Natural Person</option>
                    <option value="other">Other than Natural Person</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="choose2">
                <div class="form-group">
                  <label>Optional :</label>
                  <select class="form-control" name="cat_app_s" >
                    <option value="">Select</option>
                    @foreach($cats as $cat)
                    <option value="{{ $cat->name }}">{{ $cat->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Inventor Name:</label>
                  <input type="text" name="invent_name" class="form-control">
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Title of Invention:</label>
                  <input type="text" name="title_inven" class="form-control">
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Registered Patent Agent Name:</label>
                  <input type="text" name="reg_pat" class="form-control">
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Incase Of PCT:</label>
                  <select class="form-control" name="pct" id="select">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                  </select>
                </div>
              </div>
              <div class="row" id="choose">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>International Application Np.:</label>
                  <input type="text" name="int_app_no" class="form-control">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>International Filing Date :</label>
                  <input type="date" name="int_fili_date" class="form-control">
                </div>
              </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Renewal Date :</label>
                  <input type="date" name="renewal" class="form-control">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <input type="submit" name="submit" value="Search" class="btn btn-success">
              </div>
            </div>
          </div>
        </form>
      </div>

{{-- Table Column --}}
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <p id="demo" align="center"></p>
            <table id="myTable" class="table table-bordered" data-order='[[ 0, "desc" ]]' data-page-length='50'>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Prority Date</th>
                  <th>Filing Date</th>
                  <th>Application No</th>
                  <th>PCT Application No</th>
                  <th>Applicant Name</th>
                  <th>Inventor Name</th>
                  <th>Title of Invention</th>
                  <th>Registered patent agent Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @if(isset($results))
                @foreach($results as $pat)
                <tr>
                  <td>{{ $pat->id }}</td>
                  <td>{{ date('d-m-Y',strtotime($pat->priority_date)) }}</td>
                  <td>{{ date('d-m-Y',strtotime($pat->filing_date)) }}</td>
                  <td>{{ $pat->application_no }}</td>
                  <td>{{ $pat->int_app_no }}</td>
                  <td>{{ $pat->applicant_name }}</td>
                  <td>{{ $pat->invent_name }}</td>
                  <td>{{ $pat->title_inven }}</td>
                  <td>{{ $pat->reg_pat }}</td>
                  <td>
                    <a href="{{ url('/patent/edit',$pat->id) }}" class="btn btn-success">Edit</a>
                    <a href="{{ url('/patent/view',$pat->id) }}" class="btn btn-info">View</a>
                    <a href="" class="btn btn-primary">Update</a>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary">License</button>
                        <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{ url('/patent/license_in',$pat->id) }}">Lincensed In</a>
                          <a class="dropdown-item" href="{{ url('/patent/license_out',$pat->id) }}">Lincensed Out</a>
                        </div>
                      </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary">Assign</button>
                        <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{ url('/patent/assign_in',$pat->id) }}">Assigned In</a>
                          <a class="dropdown-item" href="{{ url('/patent/assign_out',$pat->id) }}">Assigned Out</a>
                        </div>
                      </div>
                  </td>
                </tr>
                @endforeach
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    var x = document.getElementById("myTable").rows.length - 1;
    document.getElementById("demo").innerHTML = "Result : Total Patent Is " + x;
  </script>

  <script type="text/javascript">
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
          
        });

  </script> 
  <script type="text/javascript">
    $(function(){
      $('#choose').hide();
      $('#select').change(function(){
        if ($('#select').val() == 'yes') {
          $('#choose').show();
        }else{
          $('#choose').hide();
        }
      });
    });

    $(function(){
      $('#choose2').hide();
      $('#select2').change(function(){
        if ($('#select2').val() == 'other') {
          $('#choose2').show();
        }else{
          $('#choose2').hide();
        }
      });
    });
  </script>
@include('include.footer')