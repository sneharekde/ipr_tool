@include('include.header')
  <div class="page-wrapper">
    <div class="container-fluid">
      <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Assign In | <a href="{{ url('assignIn') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>
            <div class="card">
              @include('include.message')
              <div class="card-body">

                {!! Form::open(['url' => 'patent/assign_in/save','method' => 'POST','data-parsley-validate' => '','files' => true]) !!}
                    <div class="row">
                        <input type="hidden" name="pat_id" value="{{ $comp->id }}">
                        <div class="col-md-4 form-group">
                            {{ Form::label('licensor','Name of the Assignor:') }}
                            {{ Form::text('licensor',null,['class' => 'form-control','required' => '']) }}
                        </div>
                        <div class="col-md-4 form-group">
                            {{ Form::label('licensee','Name of the Assignee:') }}
                            {{ Form::text('licensee',null,['class' => 'form-control','required' => '']) }}
                        </div>
                        <div class="col-md-4 form-group">
                            {{ Form::label('lic_date','Date of Assignement:') }}
                            {{ Form::date('lic_date',null,['class' => 'form-control','required' => '']) }}
                        </div>
                        <div class="col-md-12 form-group">
                            {{ Form::label('consideration','Consideration:') }}
                            <select name="consideration" class="form-control" id="cons" required>
                                <option value="">Select</option>
                                <option value="Periodic royalty fee">Periodic royalty fee</option>
                                <option value="One time royalty fee">One time royalty fee</option>
                            </select>
                        </div>
                        <div id="ip_head" class="col-md-12" style="display: none;">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                {{ Form::label('rem_ip','Reminder Date:') }}
                                {{ Form::date('rem_ip',null,['class' => 'form-control']) }}
                            </div>
                            <div class="col-md-6 form-group">
                                {{ Form::label('ent_date','Enter Date:') }}
                                {{ Form::date('ent_date',null,['class' => 'form-control']) }}
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 form-group" id="non_ip" style="display: none;">
                                {{ Form::label('ent_date2','Enter Date:') }}
                                {{ Form::date('ent_date2',null,['class' => 'form-control']) }}
                            </div>
                        <div class="col-md-6 form-group">
                            {{ Form::label('docs','Document:') }}
                            {{ Form::file('docs[]',['class' => 'form-control','multiple' => true]) }}
                        </div>
                        <div class="col-md-6 form-group">
                            {{ Form::label('reminder','Reminder:') }}
                            {{ Form::date('reminder',null,['class' => 'form-control']) }}
                        </div>
                        <div class="col-md-12 form-group">
                            {{ Form::label('comments','Comments:') }}
                            {{ Form::textarea('comments',null,['class' => 'form-control','rows' =>'3']) }}
                        </div>
                        <div class="col-md-12 form-group">
                            {{ Form::submit('Add Assign',['class' => 'btn btn-success']) }}
                        </div>
                    </div>
                {!! Form::close() !!}

              </div>
            </div>
            {{-- Table --}}

            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name of The Assignor</th>
                                <th>Name of The Assignee</th>
                                <th>Date of Assignement</th>
                                <th>Consideration </th>
                                <th>Document </th>
                                <th>Comments </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($licin as $lic)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $lic->licensor }}</td>
                                <td>{{ $lic->licensee }}</td>
                                <td>{{ date('d-m-Y',strtotime($lic->lic_date)) }}</td>
                                <td>{{ $lic->consideration }}</td>
                                <td>
                                    @foreach(explode(',', $lic->docs) as $file)
                                        <a href="{{ url('public/trademark',$file) }}" download="">{{ $file }}</a><br>
                                    @endforeach
                                </td>
                                <td>{{ $lic->comments }}</td>
                                <td>
                                    <a href="{{ url('patent/assign_in/edit',$lic->id) }}" class=" btn btn-success">Edit</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
  </div>
@include('include.footer')
<script type="text/javascript">
    $(document).ready(function(){
      $(document).on('change','#cons',function(){
        var model = $(this).val();
        if(model == ''){
          $('#non_ip').hide();
          $('#ip_head').hide();
      }
        if (model == 'Periodic royalty fee') {
          $('#ip_head').show();
          $('#non_ip').hide();
        }else if(model == 'One time royalty fee'){
          $('#non_ip').show();
          $('#ip_head').hide();
        }
      });
    });
  </script>
