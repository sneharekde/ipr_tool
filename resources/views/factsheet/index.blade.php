@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
        table thead tr th{
            border: 1px solid black;
            padding: 5px;
        }
        table tbody tr td{
            border: 1px solid black;
            padding: 10px;
        }
        table
        {
            margin-bottom: 20px;
        }
    </style>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Fact Sheet | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>
            @include('include.message')
            <div class="col-md-12">
                <div class="card">
                <form action="{{url('/factsheet')}}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                    <div class="card-body">
                        <div class="row">
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            <label for="">Name of Initiator * :</label>
	                            	<input type="text" class="form-control" name="name_of_initiator" value="{{ old('name_of_initiator') }}" required>
	                                <span class="text-danger"></span>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            <label for="">Designation * :</label>
	                            <select name="designation" class="form-control" required>
	                            	<option value="">--Select--</option>
	                                @foreach($designations as $designation)
	                                	<option value="{{$designation->id}}" {{ old('designation') == '$designation->designation' ? 'selected' : '' }}>{{$designation->designation}}</option>
	                                @endforeach
	                            </select>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            			<label for="">Name of Channel * :</label>
	                                        <input type="text" class="form-control" name="name_of_channel" value="{{ old('name_of_channel') }}">
	                                		<span class="text-danger"></span>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            			<label for="">Registration Mark * :</label>
	                                        <select name="registration_mark" id="registration-mark-drp" class="form-control" onchange="registrationMarkHandller ()">
	                                            <option value="">--Select--</option>
	                                            <option value="wm">Word Mark</option>
	                                            <option value="im">Image Mark</option>
	                                            <option value="wmim">Both</option>
	                                        </select>
	                                        <input type="text" class="form-control" name="reg_mark_title" id="reg_mark_title" style="display:none;" value="{{ old('name_of_initiator') }}" placeholder="Title">
	                                    	<input type="file" name="reg_mark_img" class="form-control" id="reg_mark_image" style="display: none;">
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            			<label for="">Mark used from* :</label>
	                                        <select name="mark_used_from" id="mark-used-from-drp" class="form-control" onchange="markUsedFromHandller()">
	                                            <option value="">--Select--</option>
	                                            <option value="date">Date</option>
	                                            <option value="ptbu">Proposed to be used</option>
	                                        </select>
	                                        {!! Form::text('mark_used_from_date', null, ['class' => 'form-control', 'id' => 'endd', 'style'=>'display:none;']) !!}
	                                    	<input type="text" class="form-control" name="proposed_to_be_used_txt" id="proposed_to_be_used_txt" style="display:none;" value="Proposed to be used" readonly>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            			<label for="">Class of Registration* :</label>
	                                        <select name="class_of_reg" id="class_of_reg" class="form-control" onchange="classRegistrationHandller()">
	                                            <option value="">--Select--</option>
	                                            <option value="single">Single</option>
	                                            <option value="combo">Combo</option>
	                                        </select>
	                                        <input type="text" class="form-control" name="cor_single" id="cor_single" style="display:none;">
	                                        <input type="text" class="form-control" name="cor_combo" id="cor_combo" style="display:none;">
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            			<label for="">Jurisdiction for Registration (country) * :</label>
	                                        <!-- <input type="text" class="form-control" name="jurisdiction_reg" value="{{ old('jurisdiction_reg') }}">  -->
	                                        <select name="jurisdiction_reg" class="form-control">
	                                            <option value="">--Select--</option>
	                                            @foreach ($country_list as $country)
	                                            	<option value="{{$country['name']}}">{{$country['name']}}</option>
	                                            @endforeach
	                                        </select>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            <label for="">Significance of the work* :</label>
	                                        <textarea name="significance_work" id="" cols="30" rows="7" maxlength = "2000" class="form-control"></textarea>
	                            </div>
	                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group text-center">
                        <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                    </div>
                </form>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                    <p id="demo" align="center"></p>
                    <table id="myTable" class="table-responsive-sm">
                        <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Name of initiator</th>
                            <th>Designation</th>
                            <th>Name of Channel</th>
                            <th>Registration Mark</th>
                            <th>Mark used from</th>
                            <th>Class of Registration</th>
                            <th>Jurisdiction for Registration</th>
                            <th>Significance of Work</th>
                            <th>Created On</th>
                            <th style="width:190px;">Approval Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        	@foreach($factsheets as $show)
                            <tr>
                                    <td>{{ $show ['id'] }}</td>
                                    <td>{{ $show ['name_of_initiator'] }}</td>
                                    <td>{{ $show ['designations'] }}</td>
                                    <td>{{ $show ['name_of_channel'] }}</td>
                                    <td>{!! $show ['registration_mark'] !!}</td>
                                    <td>{!! $show ['mark_used_from'] !!}</td>
                                    <td>{!! $show ['class_of_registration'] !!}</td>
                                    <td>{{ $show ['jurisdiction_for_registration'] }}</td>
                                    <td>{{ $show ['significance_of_work'] }}</td>
                                    <td>{{ $show ['created_on'] }}</td>
                                    <td>{!! $show ['factsheet_approval_status'] !!}</td>
                                    <td>
                                    <a href="{{url('edit_factsheet/'.$show ['id'])}}" type="edit" class="btn btn-warning">Edit</a><br><br>
                                    <button type="delete" class="btn btn-danger" class="btn btn-primary" data-toggle="modal" data-target="#deleteModalCenter1">Delete</button>

                                    <div class="modal fade" id="deleteModalCenter1" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                            Are you sure you want to delete this Fact Sheet.
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <form action="{{url('factsheet/delete')}}" method="POST" accept-charset="utf-8">
                                            @csrf
                                                <input type="hidden" name="id" value="{{$show ['id']}}">
                                                <button type="delete" class="btn btn-danger">Confirm</button>
                                            </form>
                                            </div>
                                        </div>
                                        </div>
                                    </div><br><br>
                                    <a href="{{url('/factsheet/'.$show ['id'].'/show')}}" class="btn btn-primary">View</a><br><br>
                                    @if($show ['initiator_approval_back'] != 0)
                                    	<button class="btn btn-info" class="btn btn-primary" data-toggle="modal" data-target="#approvalModalCenter{{$show ['id']}}">Approval</button>
                                    @endif
                                    
                                    @if($show ['approval_btn'] == 1)
                                    	<button class="btn btn-info" class="btn btn-primary" data-toggle="modal" data-target="#approvalModalCenter{{$show ['id']}}">Approval</button>
                                    @endif

                                    <div class="modal fade" id="approvalModalCenter{{$show ['id']}}" tabindex="-1" role="dialog" aria-labelledby="approvalModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                            	<div class="row">
                                            		<form action="{{url('factsheet/approval_process')}}" method="POST" accept-charset="utf-8">
                                            		@csrf
                                            		<input type="hidden" value="{{$show ['id']}}" name="id">
							                        <div class="col-md-12">
							                            <div class="form-group">
							                            <label for="">Initiator Email:</label>
							                            	<input type="text" class="form-control" name="name_of_initiator" value="{{ $user->first_name }} {{ $user->first_last }}" readonly>
							                            	<input type="text" class="form-control" name="email_of_initiator" value="{{ $user->email }}" readonly>
							                                <span class="text-danger"></span>
							                            </div>
							                        </div>
							                        <div class="col-md-12">
							                            <div class="form-group">
							                            <label for="">External Email :</label>
							                            	<input type="text" class="form-control" name="optional_for_initiator" value="">
							                                <span class="text-danger"></span>
							                            </div>
							                        </div>
							                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	                                                <button class="btn btn-danger">Send to Approval</button>
							                        </form>
							                    </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </td>
                                </tr>
                               @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
  <script type="text/javascript">
    var x = document.getElementById("myTable").rows.length - 1;
    document.getElementById("demo").innerHTML = "Total Records are " + x;

    $(function() {
        $('#endd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
        });
    });

    function markUsedFromHandller () {
    	$('#endd').hide ();
    	$('#proposed_to_be_used_txt').hide ();
    	if ($('#mark-used-from-drp').val() == "date"){
    		$('#endd').show ();
    		$('#proposed_to_be_used_txt').hide ();
    	}
    	if ($('#mark-used-from-drp').val() == "ptbu"){
    		$('#endd').hide ();
        	$('#proposed_to_be_used_txt').show ();
    	}
    }

    function registrationMarkHandller () {
		$('#reg_mark_title').hide ();
    	$('#reg_mark_image').hide ();
    	if ($('#registration-mark-drp').val() == "wm"){
        	$('#reg_mark_title').show ();
        	$('#reg_mark_image').hide ();	
    	}
    	if ($('#registration-mark-drp').val() == "im"){
    		$('#reg_mark_title').hide ();
        	$('#reg_mark_image').show ();	
    	}
    	if ($('#registration-mark-drp').val() == "wmim"){
        	$('#reg_mark_title').show ();
        	$('#reg_mark_image').show ();	
    	}
	}

	function classRegistrationHandller () {
		$('#cor_single').hide ();
		$('#cor_combo').hide ();
		if ($('#class_of_reg').val () == "single"){
			$('#cor_single').show ();
			$('#cor_combo').hide ();
		}
		if ($('#class_of_reg').val () == "combo"){
			$('#cor_single').show ();
			$('#cor_combo').show ();
		}
	}
  </script>
@include('include.footer')
