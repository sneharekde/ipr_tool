@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
  <div class="page-wrapper">
    <div class="container-fluid">
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Edit Fact Sheet | <a href="{{action('FactSheetController@index')}}" >Go Back <i class="fa fa-arrow-circle-left">  </i></a>
          </h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Edit Fact Sheet</li>
            </ol>
          </div>
        </div>
      </div>
      @include('include.message')
      <div class="card">
        <div class="card-body">
          {!! Form::model($fact_sheet,['url' => ['factsheet/update'],'data-parsley-validate' => '','method' => 'POST','files' => true]) !!}
          				<input type="hidden" name="id" value="{{$fact_sheet->id}}">
          				<div class="row">
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            <label for="">Name of Initiator * :</label>
	                            	{{ Form::text('name_of_initiator',null,['class' => 'form-control','required' => '']) }}
	                                <span class="text-danger"></span>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            <label for="">Designation * :</label>
	                            <select name="designation" class="form-control" required>
	                            	<option value="">--Select--</option>
	                                @foreach($designations as $designation)
	                                	<option value="{{$designation->id}}" @if($designation->id == $fact_sheet->designation) selected @endif>{{$designation->designation}}</option>
	                                @endforeach
	                            </select>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            			<label for="">Name of Channel * :</label>
	                                        {{ Form::text('name_of_channel',null,['class' => 'form-control','required' => '']) }}
	                                		<span class="text-danger"></span>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            			<label for="">Registration Mark * :</label>
	                                        <select name="registration_mark" id="registration-mark-drp" class="form-control" onchange="registrationMarkHandller ()">
	                                            <option value="">--Select--</option>
	                                            <option value="wm" @if($reg_mark->reg_m_type == 'wm') selected @endif>Word Mark</option>
	                                            <option value="im" @if($reg_mark->reg_m_type == 'im') selected @endif>Image Mark</option>
	                                            <option value="wmim" @if($reg_mark->reg_m_type == 'wmim') selected @endif>Both</option>
	                                        </select>
	                                    		<input type="file" name="reg_mark_img" class="form-control" id="reg_mark_image">
	                                    		<input type="hidden" name="reg_mark_img_prev" class="form-control" value="@if(isset($reg_mark->reg_m_type_image)){{ $reg_mark->reg_m_type_image }}@endif">
	                                    		@if(isset($reg_mark->reg_m_type_image))<img width='100px' src="{{Config::get ( 'app.url' )}}/Registration_Mark/{{ $reg_mark->reg_m_type_image }}" style="margin-top: 15px;" id="wm-image"/>@endif
	                                        	<input type="text" class="form-control" name="reg_mark_title" id="reg_mark_title" value="@if(isset($reg_mark->reg_m_type_value_title)){{ $reg_mark->reg_m_type_value_title }}@endif" placeholder="Title">
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            			<label for="">Mark used from* :</label>
	                                        <select name="mark_used_from" id="mark-used-from-drp" class="form-control" onchange="markUsedFromHandller()">
	                                            <option value="">--Select--</option>
	                                            <option value="date" @if($mark_used_from->m_u_f_type == 'date') selected @endif>Date</option>
	                                            <option value="ptbu" @if($mark_used_from->m_u_f_type == 'ptbu') selected @endif>Proposed to be used</option>
	                                        </select>
	                                        	@if($mark_used_from->m_u_f_type == 'date')
	                                        		{!! Form::text('mark_used_from_date', $mark_used_from->m_u_f_value, ['class' => 'form-control', 'id' => 'endd']) !!}
	                                        	@else
	                                        		{!! Form::text('mark_used_from_date', null, ['class' => 'form-control', 'id' => 'endd', 'style'=>'display:none;']) !!}
	                                        	@endif
	                                    		<input type="text" class="form-control" name="proposed_to_be_used_txt" id="proposed_to_be_used_txt" value="Proposed to be used" readonly @if($mark_used_from->m_u_f_type == 'date')style="display:none;"@endif>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            			<label for="">Class of Registration* :</label>
	                                        <select name="class_of_reg" id="class_of_reg" class="form-control" onchange="classRegistrationHandller()">
	                                            <option value="">--Select--</option>
	                                            <option value="single" @if($class_reg->cof_type == 'single') selected @endif>Single</option>
	                                            <option value="combo" @if($class_reg->cof_type == 'combo') selected @endif>Combo</option>
	                                        </select>
	                                        <input type="text" class="form-control" name="cor_single" id="cor_single" value="{{$class_reg->cof_single_value}}" >
	                                        <input type="text" class="form-control" name="cor_combo" id="cor_combo" value="@if(isset($class_reg->cof_combo_value)) {{$class_reg->cof_combo_value}} @endif" >
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            			<label for="">Jurisdiction for Registration (country) * :</label>
<!-- 	                                        <input type="text" class="form-control" name="jurisdiction_reg" value="@if(isset($fact_sheet->jurisdiction_for_registration)){{ $fact_sheet->jurisdiction_for_registration }}@endif"> -->
									<select name="jurisdiction_reg" class="form-control">
	                                            <option value="">--Select--</option>
	                                            @foreach ($country_list as $country)
	                                            	<option value="{{$country['name']}}" @if(isset($fact_sheet->jurisdiction_for_registration) && $fact_sheet->jurisdiction_for_registration == $country['name']) selected @endif>{{$country['name']}}</option>
	                                            @endforeach
	                                        </select>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                            <label for="">Significance of the work* :</label>
	                            	<textarea name="significance_work" id="" cols="30" rows="7" maxlength = "2000" class="form-control">{{$fact_sheet->significance_of_work}}</textarea>
	                            </div>
	                        </div>
                        </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <input type="submit" name="submit" value="Submit" class="btn btn-success">
            </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
@include('include.footer')
<script type="text/javascript">
    $(document).ready(function(){
    	$('#endd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
        });
    });
    function registrationMarkHandller () {
		$('#reg_mark_title').hide ();
    	$('#reg_mark_image').hide ();
    	if ($('#registration-mark-drp').val() == "wm"){
    		$('#wm-image').hide ();
        	$('#reg_mark_title').show ();
        	$('#reg_mark_image').hide ();	
    	}
    	if ($('#registration-mark-drp').val() == "im"){
    		$('#wm-image').show ();
    		$('#reg_mark_title').hide ();
        	$('#reg_mark_image').show ();	
    	}
    	if ($('#registration-mark-drp').val() == "wmim"){
        	$('#reg_mark_title').show ();
        	$('#reg_mark_image').show ();	
    	}
	}
    function markUsedFromHandller () {
    	$('#endd').hide ();
    	$('#proposed_to_be_used_txt').hide ();
    	if ($('#mark-used-from-drp').val() == "date"){
    		$('#endd').show ();
    		$('#proposed_to_be_used_txt').hide ();
    	}
    	if ($('#mark-used-from-drp').val() == "ptbu"){
    		$('#endd').hide ();
        	$('#proposed_to_be_used_txt').show ();
    	}
    }
    function classRegistrationHandller () {
		$('#cor_single').hide ();
		$('#cor_combo').hide ();
		if ($('#class_of_reg').val () == "single"){
			$('#cor_single').show ();
			$('#cor_combo').hide ();
		}
		if ($('#class_of_reg').val () == "combo"){
			$('#cor_single').show ();
			$('#cor_combo').show ();
		}
	}
</script>
