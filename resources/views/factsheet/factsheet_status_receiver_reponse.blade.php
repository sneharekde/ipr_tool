@include('include.outerheader')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
        table thead tr th{
            border: 1px solid black;
            padding: 5px;
        }
        table tbody tr td{
            border: 1px solid black;
            padding: 10px;
        }
        table
        {
            margin-bottom: 20px;
        }
    </style>

    <div class="page-wrapper">
        <div class="container-fluid">
        	<div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Fact Sheet Details</h4>
                </div>
            </div>
            @include('include.message')
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <p id="demo" align="center">Comment Shared.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
