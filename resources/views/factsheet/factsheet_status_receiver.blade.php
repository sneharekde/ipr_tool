@include('include.outerheader')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
        table thead tr th{
            border: 1px solid black;
            padding: 5px;
        }
        table tbody tr td{
            border: 1px solid black;
            padding: 10px;
        }
        table
        {
            margin-bottom: 20px;
        }
    </style>

    <div class="page-wrapper">
        <div class="container-fluid">
        	<div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Fact Sheet Details</h4>
                </div>
            </div>
            @include('include.message')
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <p id="demo" align="center"></p>
                            <table  class="table table-bordered">
                                <thead>
                                    <tr>
                                    	<th>#</th>
                                        <th>Name of initiator</th>
                                        <th>Name of channel</th>
                                        <th>Registration mark</th>
                                        <th>Mark used from</th>
                                        <th>Class of registration</th>
                                        <th>Jurisdiction for registration</th>
                                        <th>Significance of work</th>
                                        @if($email_data['initiator_optional_approval_status'])
                                        <th>External Comment</th>
                                        @endif
                                        @if($email_data['legal_approval_status'])
					                    <th>Legal Comment</th>
					                    @endif
					                    @if($email_data['coo_approval_status'])
					                    <th>COO Comment</th>
					                    @endif
					                    @if($email_data['cfo_approval_status'])
					                    <th>CFO Comment</th>
					                    @endif
					                    @if($email_data['ceo_approval_status'])
					                    <th>CEO Comment</th>
					                    @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    	<td>{{ $email_data['id'] }}</td>
                    	                <td>{{ $email_data['nm'] }}</td>
                    	                <td>{{ $email_data['nm_ch'] }}</td>
                    	                @if ($email_data['reg_mark']['reg_m_type'] == 'wm')
					                    	<td>
					                    		<span>Word Mark : {{ $email_data['reg_mark']['reg_m_type_value_title'] }}</span>
					                    	</td>
                   						@endif
                    	                @if ($email_data['reg_mark']['reg_m_type'] == 'im')
					                    	<td>
					                    		<span>Word Image : <img alt="Regisration Mark" style="width: 50px;" src="{{Config::get ( 'app.url' )}}/Registration_Mark/{{ $email_data['reg_mark']['reg_m_type_image'] }}"> </span>
					                    	</td>
                   						@endif
                   						@if ($email_data['reg_mark']['reg_m_type'] == 'wmim')
					                    	<td>
					                    		<span>Word Mark : {{ $email_data['reg_mark']['reg_m_type_value_title'] }}</span>
					                    		<span>Word Image : <img style="width: 50px;" alt="Regisration Mark" src="{{Config::get ( 'app.url' )}}/Registration_Mark/{{ $email_data['reg_mark']['reg_m_type_image'] }}"> </span>
					                    	</td>
                   						@endif
                   						@if ($email_data['mark_used_from']['m_u_f_type'] == 'date')
					                    	<td>
					                    		<span>Date : {{ $email_data['mark_used_from']['m_u_f_value'] }}</span>
					                    	</td>
					                   	@endif
					                   	@if ($email_data['mark_used_from']['m_u_f_type'] == 'ptbu')
					                    	<td>
					                    		<span>Mark used from : {{ $email_data['mark_used_from']['m_u_f_value'] }}</span>
					                    	</td>
					                   	@endif
					                    @if ($email_data['class_reg']['cof_type'] == 'single')
					                    	<td>
					                    		<span>{{ $email_data['class_reg']['cof_single_value'] }}</span>
					                    	</td>
					                   	@endif
					                   	@if ($email_data['class_reg']['cof_type'] == 'combo')
					                    	<td>
					                    		<span>{{ $email_data['class_reg']['cof_single_value'] }}</span>
					                    		<span>{{ $email_data['class_reg']['cof_combo_value'] }}</span>
					                    	</td>
					                   	@endif
					                   	<td>
					                    	<span>{{ $email_data['jur_reg']}}</span>
					                    </td>
					                    <td>
					                    	<span>{{ $email_data['sig_work']}}</span>
					                    </td>
					                    @if($email_data['initiator_optional_approval_status'])
					                    <td>
					                    	<span>{{ $email_data['option_comment']}}</span>
					                    </td>
					                    @endif
					                    @if($email_data['legal_approval_status'])
					                    <td>
					                    	<span>{{ $email_data['legal_comment']}}</span>
					                    </td>
					                    @endif
					                    @if($email_data['coo_approval_status'])
					                    <td>
					                    	<span>{{ $email_data['coo_comment']}}</span>
					                    </td>
					                    @endif
					                    @if($email_data['cfo_approval_status'])
					                    <td>
					                    	<span>{{ $email_data['cfo_comment']}}</span>
					                    </td>
					                    @endif
					                    @if($email_data['ceo_approval_status'])
					                    <td>
					                    	<span>{{ $email_data['ceo_comment']}}</span>
					                    </td>
					                    @endif
                                    </tr>
                                    @if($is_comming_from_email == 1)
                                    <tr>
                                    	<form action="{{url('/factsheetstatusshared')}}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
	                                    	<input type="hidden" name="factsheet_id" value="{{$email_data['factsheet']}}">
	                                    	<input type="hidden" name="step" value="{{$email_data['ap_p_status']}}">
	                    					@csrf
	                                    	<td colspan="5">Share Comment : <textarea name="comment" cols="60" rows="5" maxlength = "2000" class="form-control"></textarea>
	                                    	<input type="submit" name="submit" value="APPROVED" class="btn btn-success">
	                                    	<input type="submit" name="submit" value="HOLD & COMMENT" class="btn btn-danger">
                                    	</td>
                    	            	</form>
                    	            </tr>
                    	            @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
