<style>
    .padd{
    padding: 20px;
    }
    #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }
    #customers tr:nth-child(even){background-color: #f2f2f2;}
    #customers tr:hover {background-color: #ddd;}
    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }
    #resp{
        overflow-x: auto;
    }
</style>


<div class="padd">
    <h3>Dear {{$email_data['dear_details']}}</h3>
    <p>{{$email_data['following_line']}}</p>
    <h3>Factsheet Details :</h3>
    <div id="resp">
        <table style="border-style:solid; border-width:1px; border-color:#000000;font-family: Arial, Helvetica, sans-serif;width: 100%;">
            <thead style="border: 3px solid #000000;background-color:#87CEFA;padding: 8px;">
                <tr>
                	<th>ID</th>
                    <th>Name of initiator</th>
                    <th>Name of channel</th>
                    <th>Registration mark</th>
                    <th>Mark used from</th>
                    <th>Class of registration</th>
                    <th>Jurisdiction for registration</th>
                    <th>Significance of work</th>
                    @if($email_data['initiator_optional_approval_status'] == 2 || $email_data['initiator_optional_approval_status'] == 1)
                    <th>External Comment</th>
                    @endif
                    @if($email_data['legal_approval_status'] == 2 || $email_data['legal_approval_status'] == 1)
                    <th>Legal Comment</th>
                    @endif
                    @if($email_data['coo_approval_status'] == 2 || $email_data['coo_approval_status'] == 1)
                    <th>COO Comment</th>
                    @endif
                    @if($email_data['cfo_approval_status'] == 2 || $email_data['cfo_approval_status'] == 1)
                    <th>CFO Comment</th>
                    @endif
                    @if($email_data['ceo_approval_status'] == 2 || $email_data['ceo_approval_status'] == 1)
                    <th>CEO Comment</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                <tr>
                	<td style="border: 1px solid #00008B;padding: 8px;">{{ $email_data['id'] }}</td>
                    <td style="border: 1px solid #00008B;padding: 8px;">{{ $email_data['nm'] }}</td>
                    <td style="border: 1px solid #00008B;padding: 8px;">{{ $email_data['nm_ch'] }}</td>
                    @if ($email_data['reg_mark']['reg_m_type'] == 'wm')
                    	<td style="border: 1px solid #00008B;padding: 8px;">
                    		<span>Word Mark : {{ $email_data['reg_mark']['reg_m_type_value_title'] }}</span>
                    	</td>
                   	@endif
                    @if ($email_data['reg_mark']['reg_m_type'] == 'im')
                    	<td style="border: 1px solid #00008B;padding: 8px;">
                    		<span>Word Image : <img alt="Regisration Mark" width="100px" src="{{ config('app.url') }}Registration_Mark/{{ $email_data['reg_mark']['reg_m_type_image'] }}"> </span>
                    	</td>
                   	@endif
                   	@if ($email_data['reg_mark']['reg_m_type'] == 'wmim')
                    	<td style="border: 1px solid #00008B;padding: 8px;">
                    		<span>Word Mark : {{ $email_data['reg_mark']['reg_m_type_value_title'] }}</span>
                    		<span>Word Image : <img alt="Regisration Mark" width="100px" src="{{ config('app.url') }}Registration_Mark/{{ $email_data['reg_mark']['reg_m_type_image'] }}"> </span>
                    	</td>
                   	@endif
                    @if ($email_data['mark_used_from']['m_u_f_type'] == 'date')
                    	<td style="border: 1px solid #00008B;padding: 8px;">
                    		<span>Date : {{ $email_data['mark_used_from']['m_u_f_value'] }}</span>
                    	</td>
                   	@endif
                   	@if ($email_data['mark_used_from']['m_u_f_type'] == 'ptbu')
                    	<td style="border: 1px solid #00008B;padding: 8px;">
                    		<span>Mark used from : {{ $email_data['mark_used_from']['m_u_f_value'] }}</span>
                    	</td>
                   	@endif
                    @if ($email_data['class_reg']['cof_type'] == 'single')
                    	<td style="border: 1px solid #00008B;padding: 8px;">
                    		<span>{{ $email_data['class_reg']['cof_single_value'] }}</span>
                    	</td>
                   	@endif
                   	@if ($email_data['class_reg']['cof_type'] == 'combo')
                    	<td style="border: 1px solid #00008B;padding: 8px;">
                    		<span>{{ $email_data['class_reg']['cof_single_value'] }}</span>
                    		<span>{{ $email_data['class_reg']['cof_combo_value'] }}</span>
                    	</td>
                   	@endif
                   	<td style="border: 1px solid #00008B;padding: 8px;">
                    	<span>{{ $email_data['jur_reg']}}</span>
                    </td>
                    <td style="border: 1px solid #00008B;padding: 8px;">
                    	<span>{{ $email_data['sig_work']}}</span>
                    </td>
                    @if($email_data['initiator_optional_approval_status'] == 2 || $email_data['initiator_optional_approval_status'] == 1)
                    <td style="border: 1px solid #00008B;padding: 8px;">
                    	<span>{{ $email_data['option_comment']}}</span>
                    </td>
                    @endif
                    @if($email_data['legal_approval_status'] == 2 || $email_data['legal_approval_status'] == 1)
                    <td style="border: 1px solid #00008B;padding: 8px;">
                    	<span>{{ $email_data['legal_comment']}}</span>
                    </td>
                    @endif
                    @if($email_data['coo_approval_status'] == 2 || $email_data['coo_approval_status'] == 1)
                    <td style="border: 1px solid #00008B;padding: 8px;">
                    	<span>{{ $email_data['coo_comment']}}</span>
                    </td>
                    @endif
                    @if($email_data['cfo_approval_status'] == 2 || $email_data['cfo_approval_status'] == 1)
                    <td style="border: 1px solid #00008B;padding: 8px;">
                    	<span>{{ $email_data['cfo_comment']}}</span>
                    </td>
                    @endif
                    @if($email_data['ceo_approval_status'] == 2 || $email_data['ceo_approval_status'] == 1)
                    <td style="border: 1px solid #00008B;padding: 8px;">
                    	<span>{{ $email_data['ceo_comment']}}</span>
                    </td>
                    @endif
                </tr>
            </tbody>
        </table>
    </div>
    <p>Note:This is a system generated mail. Please do not reply to this mail.</p>
    <p>In case of any doubt or difficulty, please call Helpdesk as per details given on the support page.</p>
    <h3>Yours Sincerely</h3>
    <h3>Team Legal</h3>
</div>
