@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Show Fact Sheet | <a href="{{action('FactSheetController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Show tradmark</li>
                        </ol>
                        <div id="editor"></div>
                        {{--  <button id='cmd' onclick="printHtmldiv()" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa fa-arrow-down"></i> Download PDF</button>  --}}
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="col-md-12" id="content">
                <div class="card">
                    <div class="card-body">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Factsheet Detail</a>
                            	<a class="nav-link" id="nav-approval-tab" data-toggle="tab" href="#nav-approval" role="tab" aria-controls="nav-approval" aria-selected="true">Approval Detail</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <table id="example23" class="table table-bordered">
                                    <tr>
                                        <th>Name of Initiator :</th>
                                        <td>{{$show->name_of_initiator}}</td>
                                    </tr>
                                    <tr>
                                        <th>Designation :</th>
                                        <td>{{ $designation->designation }}</td>
                                    </tr>
                                    <tr>
                                        <th>Name of Channel:</th>
                                        <td>{{ $show->name_of_channel }}</td>
                                    </tr>
                                    <tr>
                                        <th>Registration Mark:</th>
                                        <td>{!! $rm !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Mark Used From:</th>
                                        <td>{!! $mu !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Class of Registration:</th>
                                        <td>{!! $cr !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Jurisdiction For Registration (country):</th>
                                        <td>{{ $show->jurisdiction_for_registration }}</td>
                                    </tr>
                                    <tr>
                                        <th>Significance of Work:</th>
                                        <td>{{ $show->significance_of_work }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-approval" role="tabpanel" aria-labelledby="nav-approval-tab">
                            	<table id="example23" class="table table-bordered">
                                    <tr>
                                        <th>External:</th>
                                        <td><b>Email</b> : @if(isset($approval_status ['initiator_optional_agent_email']) && $approval_status ['initiator_optional_agent_email'] != ""){{$approval_status ['initiator_optional_agent_email']}} @else N/A @endif </br>
                                        	<b>Name</b> : @if(isset($approval_status ['initiator_optional_agent_name']) && $approval_status ['initiator_optional_agent_name'] != ""){{$approval_status ['initiator_optional_agent_name']}} @else N/A @endif
                                        </td>
                                        <td><b>Comment</b> : @if(isset($approval_status ['initiator_optional_agent_comment'])){{$approval_status ['initiator_optional_agent_comment']}}@endif <br> <b>Status</b> : @if(!isset($approval_status)) <span class="text-warning">Not Initiated </span> @else @if($approval_status['initiator_optional_agent_email'] == "") N/A @elseif($approval_status ['initiator_optional_approval_status'] == 1) <span style="color:green;">Approved</span> @elseif($approval_status ['initiator_optional_approval_status'] == 2) <span style="color:red;">Disapproved</span> @else <span style="color:red;">Pending</span>  @endif @endif</td>
                                    </tr>
                                    <tr>
                                        <th>Legal:</th>
                                        <td><b>Email</b> : @if(isset($approval_status ['legal_agent_email'])){{$approval_status ['legal_agent_email']}}@endif <br>
                                        	<b>Name</b> : @if(isset($approval_status ['legal_agent_name'])){{$approval_status ['legal_agent_name']}}@endif
                                        </td>
                                        <td><b>Comment</b> : @if(isset($approval_status ['legal_agent_comment'])){{$approval_status ['legal_agent_comment']}}@endif <br> <b>Status</b> : @if(!isset($approval_status)) <span class="text-warning">Not Initiated </span> @else @if($approval_status ['legal_approval_status'] == 1) <span style="color:green;">Approved</span> @elseif($approval_status ['legal_approval_status'] == 2) <span style="color:red;">Disapproved</span> @else  <span style="color:red;">Pending</span> @endif @endif </td>
                                    </tr>
                                    <tr>
                                        <th>COO:</th>
                                        <td><b>Email</b> : @if(isset($approval_status ['coo_agent_email'])){{$approval_status ['coo_agent_email']}}@endif <br>
                                        	<b>Name</b> : @if(isset($approval_status ['coo_agent_name'])){{$approval_status ['coo_agent_name']}}@endif
                                        </td>
                                        <td><b>Comment</b> : @if(isset($approval_status ['coo_agent_comment'])){{$approval_status ['coo_agent_comment']}}@endif <br> <b>Status</b> : @if(!isset($approval_status)) <span class="text-warning">Not Initiated </span> @else @if($approval_status ['coo_approval_status'] == 1) <span style="color:green;">Approved</span> @elseif($approval_status ['coo_approval_status'] == 2) <span style="color:red;">Disapproved</span> @else  <span style="color:red;">Pending</span> @endif @endif</td>
                                    </tr>
                                    <tr>
                                        <th>CFO:</th>
                                        <td><b>Email</b> : @if(isset($approval_status ['cfo_agent_email'])){{$approval_status ['cfo_agent_email']}}@endif <br>
                                        	<b>Name</b> : @if(isset($approval_status ['cfo_agent_name'])){{$approval_status ['cfo_agent_name']}}@endif
                                        </td>
                                        <td><b>Comment</b> : @if(isset($approval_status ['cfo_agent_comment'])){{$approval_status ['cfo_agent_comment']}}@endif <br> <b>Status</b> : @if(!isset($approval_status)) <span class="text-warning">Not Initiated </span> @else @if($approval_status ['cfo_approval_status'] == 1) <span style="color:green;">Approved</span> @elseif($approval_status ['cfo_approval_status'] == 2) <span style="color:red;">Disapproved</span> @else  <span style="color:red;">Pending</span> @endif @endif</td>
                                    </tr>
                                    <tr>
                                        <th>CEO:</th>
                                        <td><b>Email</b> : @if(isset($approval_status ['ceo_agent_email'])){{$approval_status ['ceo_agent_email']}}@endif <br>
                                        	<b>Name</b> : @if(isset($approval_status ['ceo_agent_name'])){{$approval_status ['ceo_agent_name']}}@endif
                                        </td>
                                        <td><b>Comment</b> : @if(isset($approval_status ['ceo_agent_comment'])){{$approval_status ['ceo_agent_comment']}}@endif <br> <b>Status</b> : @if(!isset($approval_status)) <span class="text-warning">Not Initiated </span> @else @if($approval_status ['ceo_approval_status'] == 1) <span style="color:green;">Approved</span> @elseif($approval_status ['ceo_approval_status'] == 2) <span style="color:red;">Disapproved</span> @else  <span style="color:red;">Pending</span> @endif @endif</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="editor"></div>
        </div>
    </div>
    <script>
    </script>
@include('include.footer')
