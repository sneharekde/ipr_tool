@include('include.header')
    <style>
        .cards{
            box-shadow: 0 4px 8px 0 rgba(0, 0,0,0.2);
        }
        .cards-body{
            padding: 20px
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Mark of Application | <a href="{{action('AddTradmarkController@NatureApplicationShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active">Mark of Application</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="cards">
                <div class="cards-body">
                    <form action="{{url('/nature_of_the_application/save')}}" method="POST" accept-charset="utf-8">
                    @csrf
                        <div class="form-group {{ $errors->has('nature_of_application') ? 'has-error' : '' }}">
                            <label for="">Mark of Application</label>
                            <input type="text" class="form-control" name="nature_of_application" value="{{ old('nature_of_application') }}">
                            <span class="text-danger">{{ $errors->first('nature_of_application') }}</span>
                        </div>
                        <div class="col-md-12 center"><button type="Submit" class="btn btn-success">Submit</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
