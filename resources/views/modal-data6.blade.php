<style>
.dataTables_filter
{
  display: none;
}
.paginate_button
{
  display: none;
}
.dataTables_info
{
  display: none;
}
</style>
<div class="modal  fade bs-example-modal-lg" id="bootstrap-modal5" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Copyright Status Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow-x: auto;">
        <div id="demo-modal">
          <div class="table-responsive">
            <table id="example28" class="table table-striped table-bordered " >
              <thead>
                <tr>
                  <th>Sr. No</th>
                  <th>Dairy No.</th>
                  <th>Date of Filing</th>
                  <th>Title</th>
                  <th>Agent</th>
                  <th>Govt Fee</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                @foreach($results as $result)
                  @if($result->status ==  $name)
                    <tr>
                      <td>{{ $no++ }}</td>
                      <td>{{ $result->diary }}</td>
                      <td>{{ date('d-m-Y',strtotime($result->dof)) }}</td>
                      <td>{{ $result->titlew }}</td>
                      <td>
                          @if($result->agent_id !='')
                            {{ $result->agent->name }}
                          @endif
                      </td>
                      <td>{{ $result->gov }}</td>
                      <td>{{ $result->status }}</td>
                    </tr>
                  @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  $(function () {
    // responsive table
    $('#config-table').DataTable({
      responsive: true
    });
    $('#example28').DataTable({
      dom: 'Bfrtip',
      buttons: [
       {
          extend: 'pdf',
          footer: true,
          title: 'Copyright Status Reports export',
          orientation: 'landscape',
          pageSize: 'LEGAL',
          exportOptions: {
          columns: [0,2,3,4,5,6]
        }
      }]
    });
    $('.buttons-pdf').addClass('btn btn-primary mr-1');
  });
</script>
