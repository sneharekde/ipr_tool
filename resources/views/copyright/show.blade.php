@include('include.header')

    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Show Copyright | <a href="{{action('CopyrightController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Show Copyright</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-12" id="content">
                    <div class="card">
                        <div class="card-body">
                            @include('include.message')
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Details</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Agent/ Attorney fees</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Diary Number</th>
                                            <td>{{ $copyright->diary }}</td>
                                        </tr>
                                        <tr>
                                            <th>Govt. Fee</th>
                                            <td>{{ $copyright->gov }}</td>
                                        </tr>
                                        <tr>
                                            <th>Date of Filing</th>
                                            <td>{{ date('d-m-Y',strtotime($copyright->dof)) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Name of the Applicant</th>
                                            <td>{{ $copyright->nofa }}</td>
                                        </tr>
                                        <tr>
                                            <th>Address of the Applicant</th>
                                            <td>{{ $copyright->addrp }}</td>
                                        </tr>
                                        <tr>
                                            <th>Application filed by</th>
                                            <td>{{ $copyright->app_filed }}</td>
                                        </tr>
                                        <tr>
                                            <th>Name of the Agent/Attorney</th>
                                            <td>
                                                @if ($copyright->agent_id !='')
                                                {{ $copyright->agent->name }}
                                                @else
                                                    NA
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Agent/Attorney code</th>
                                            <td>
                                                @if ($copyright->agent_code !='')
                                                {{ $copyright->agent_code }}
                                                @else
                                                    NA
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Address of the agent/attorney</th>
                                            <td>
                                                @if ($copyright->agent_address !='')
                                                {{ $copyright->agent_address }}
                                                @else
                                                    NA
                                                @endif
                                                </td>
                                        </tr>
                                        <tr>
                                            <th>POA</th>
                                            <td><a href="{{ url('public/copyright/docs',$copyright->poa) }}" download="">{{ $copyright->poa }}</a></td>
                                        </tr>
                                        <tr>
                                            <th>Class of Work</th>
                                            <td>{{ $copyright->titlew }}</td>
                                        </tr>
                                        <tr>
                                            <th>Work title</th>
                                            <td>{{ $copyright->work_title }}</td>
                                        </tr>
                                        <tr>
                                            <th>Work</th>
                                            <td><a href="{{ url('public/copyright/img/'.$copyright->image) }}" download=""></a></td>
                                        </tr>
                                        <tr>
                                            <th>Description</th>
                                            <td>{{ $copyright->description }}</td>
                                        </tr>
                                        <tr>
                                            <th>Language</th>
                                            <td>{{ $copyright->lang->language }}</td>
                                        </tr>
                                        <tr>
                                            <th>Remarks</th>
                                            <td>{{ $copyright->remark }}</td>
                                        </tr>
                                        <tr>
                                            <th>User Affidavit</th>
                                            <td> <a href="{{ url('public/copyright/docs/'.$copyright->user_aff) }}" download="">{{ $copyright->user_aff }}</a></td>
                                        </tr>
                                        <tr>
                                            <th>Payment Details(Pdf)</th>
                                            <td> <a href="{{ url('public/copyright/docs/'.$copyright->copy_app) }}" download="">{{ $copyright->copy_app }}</a></td>
                                        </tr>
                                        <tr>
                                            <th>Date of Registration</th>
                                            <td>
                                                @if ($copyright->dor)
                                                {{ date('d-m-Y',strtotime($copyright->dor)) }}
                                                @else
                                                NA
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Certificate of Registration</th>
                                            <td><a href="{{ url('public/copyright',$copyright->reg_cert) }}" download="">{{ $copyright->reg_cert }}</a></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="" style="margin-top:10px">
                                        <a href="{{ url('copyright-fee',$copyright->id) }}" class="btn btn-info">Add Fees</a>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Date</th>
                                                    <th>Fees</th>
                                                    <th>Invoice</th>
                                                    <th>stamp paper</th>
                                                    <th>notarisation charges</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($posts as $post)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ date('d-m-Y',strtotime($post->date)) }}</td>
                                                        <td>{{ $post->fee }}</td>
                                                        <td>
                                                            @if ($post->invoice !='')
                                                                <a href="{{ url('public/copyright/img',$post->invoice) }}">{{ $post->invoice }}</a>
                                                            @else
                                                            NA
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if ($post->stamp !='')
                                                                <a href="{{ url('public/copyright/img',$post->stamp) }}">{{ $post->stamp }}</a>
                                                            @else
                                                            NA
                                                            @endif
                                                        </td>
                                                        <td>{{ $post->not_fee }}</td>
                                                        <td><a href="{{ url('copyright-fee/edit',$post->id) }}" class="btn btn-success btn-xs">Edit</a></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
