@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Copyright Report | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Copyright </li>
                        </ol>
                        <a  href="{{action('CopyrightController@create')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add New Copyright</a>
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-rounded" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ $message }}
                </div>
            @endif
            <div class="card">
                <form action="{{ route('copyrights.index') }}" method="POST" accept-charset="utf-8">
                @csrf
                <div class="form-body">
                    <div class="card-body">
                        <div class="row pt-3">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Applicant name</label>
                                    <select name="nofas" id="" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($nofa as $item)
                                            <option value="{{ $item->nofa }}" @if($item->nofa == $nofas) selected @endif>{{ $item->nofa }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ROC number  </label>
                                    <select name="rocs" id="" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($roc as $item)
                                            @if($item->roc !="")
                                                <option value="{{ $item->roc }}" @if($item->roc == $rocs) selected @endif>{{ $item->roc }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Diary number</label>
                                    <select name="dairys" id="" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($dairyn as $item)
                                            <option value="{{ $item->diary }}" @if($item->diary == $dairys) selected @endif>{{ $item->diary }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date of Application</label>
                                    <input type="date" name="doas"  class="form-control" value="{{ $doas }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Title of work</label>
                                    <select name="titles" id="" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($titlew as $item)
                                            <option value="{{ $item->titlew }}" @if($item->titlew == $titles) selected @endif>{{ $item->titlew }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="statuss" id="" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($status as $item)
                                            <option value="{{ $item->status }}" @if($item->status == $statuss) selected @endif>{{ $item->status }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>From Date :</label>
                                    <div class="input-group">
                                        <input type="date" name="from" class="form-control" value="{{ $from }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>To Date :</label>
                                    <div class="input-group">
                                        <input type="date" name="to" class="form-control" value="{{ $to }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-success">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <p id="demo" align="center"></p>
                        <table id="myTable" class="table table-bordered" data-order='[[ 0, "desc" ]]' data-page-length='50'>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Diary number</th>
                                    <th>Applicant</th>
                                    <th>Date of filing</th>
                                    <th>Title of Work</th>
                                    <th>Work</th>
                                    <th>Name of Agent/Attorney</th>
                                    <th>Agent/Attorney code</th>
                                    <th>Date of Registration</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        <tbody>

                            @foreach($results as $result)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                        <td>{{ $result->diary }}</td>
                                        <td>{{ $result->nofa }}</td>
                                        <td>{{ date('d-m-Y',strtotime($result->dof)) }}</td>
                                        <td>{{ $result->titlew }}</td>
                                        <td>
                                            @if ($result->image !='')
                                                <a href="{{url('public/copyright/img/'.$result->image )}}" download="">{{ $result->image }}</a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($result->agent_id !='')

                                            @else
                                            NA
                                            @endif
                                        </td>
                                        <td>
                                            @if ($result->agent_code !='')
                                            {{ $result->agent_code }}
                                            @else
                                                NA
                                            @endif
                                        </td>
                                        <td>
                                            @if ($result->dor !='')
                                                {{ date('d-m-Y',strtotime($result->dor)) }}
                                            @else
                                                NA
                                            @endif
                                        </td>
                                        <td>
                                            @if ($result->status == 'Active Application')
                                                {{ $result->status.' -> '.$result->sub_status }}
                                            @else
                                            {{ $result->status }}
                                            @endif
                                            </td>
                                        <td>
                                            <a href="{{ route('copyrights.edit',$result->id) }}" type="edit" class="btn btn-warning">Edit</a>
                                            <a href="{{ route('copyrights.show',$result->id) }}" class="btn btn-success">View</a>
                                            <a href="{{url('/copyright/'.$result->id.'/update_status')}}" class="btn btn-info">Update</a>
                                        </td>
                                </tr>
                            @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var x = document.getElementById("myTable").rows.length - 1;
        document.getElementById("demo").innerHTML = "Total Records are " + x;
    </script>

    <script type="text/javascript">
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping

        });

  </script>
@include('include.footer')
