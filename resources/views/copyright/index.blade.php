@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Copyright Report | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <a href="{{ url('copyright-draft') }}" class="btn btn-primary"><i class="fa fa-upload"></i> Draft</a>
                        <a  href="{{action('CopyrightController@create')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add New Copyright</a>
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-rounded" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ $message }}
                </div>
            @endif
            <div class="card">
                <form action="{{ route('copyrights.index') }}" method="POST" accept-charset="utf-8">
                @csrf
                    <div class="form-body">
                        <div class="card-body">
                            <div class="row pt-3">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Applicant name</label>
                                        <select name="nofas" id="" class="form-control">
                                            <option value="">Select</option>
                                            @foreach ($nofa as $item)
                                                <option value="{{ $item->nofa }}">{{ $item->nofa }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>ROC number  </label>
                                        <select name="rocs" id="" class="form-control">
                                            <option value="">Select</option>
                                            @foreach ($roc as $item)
                                                @if($item->roc !="")
                                                    <option value="{{ $item->roc }}">{{ $item->roc }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Diary number</label>
                                        <select name="dairys" id="" class="form-control">
                                            <option value="">Select</option>
                                            @foreach ($dairyn as $item)
                                                <option value="{{ $item->diary }}">{{ $item->diary }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Date of Application</label>
                                        <input type="date" name="doas"  class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Title of work</label>
                                        <select name="titles" id="" class="form-control">
                                            <option value="">Select</option>
                                            @foreach ($titlew as $item)
                                                <option value="{{ $item->titlew }}">{{ $item->titlew }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select name="statuss" id="" class="form-control">
                                            <option value="">Select</option>
                                            @foreach ($status as $item)
                                                <option value="{{ $item->status }}">{{ $item->status }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>From Date :</label>
                                        <div class="input-group">
                                            <input type="date" name="from" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>To Date :</label>
                                        <div class="input-group">
                                            <input type="date" name="to" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-success">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <p id="demo" align="center"></p>
                        <table id="myTable" class="table table-bordered"  data-page-length='50'>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Diary number</th>
                                    <th>Applicant</th>
                                    <th>Date of filing</th>
                                    <th>Title of Work</th>
                                    <th>Work</th>
                                    <th>Name of Agent/Attorney</th>
                                    <th>Agent/Attorney code</th>
                                    <th>Date of Registration</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($copyrights as $copyright)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $copyright->diary }}</td>
                                        <td>{{ $copyright->nofa }}</td>
                                        <td>{{ date('d-m-Y',strtotime($copyright->dof)) }}</td>
                                        <td>{{ $copyright->titlew }}</td>
                                        <td>
                                            @if ($copyright->image !='')
                                                <a href="{{url('public/copyright/img/'.$copyright->image )}}" download="">{{ $copyright->image }}</a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($copyright->agent_id !='')
                                            {{ $copyright->agent->name }}
                                            @else
                                            NA
                                            @endif
                                        </td>
                                        <td>
                                            @if ($copyright->agent_code !='')
                                            {{ $copyright->agent_code }}
                                            @else
                                                NA
                                            @endif
                                        </td>
                                        <td>
                                            @if ($copyright->dor !='')
                                                {{ date('d-m-Y',strtotime($copyright->dor)) }}
                                            @else
                                                NA
                                            @endif
                                        </td>
                                        <td>
                                            @if ($copyright->status == 'Active Application')
                                                {{ $copyright->status.' -> '.$copyright->sub_status }}
                                            @else
                                            {{ $copyright->status }}
                                            @endif
                                            </td>
                                        <td>
                                            <a href="{{ route('copyrights.edit',$copyright->id) }}" type="edit" class="btn btn-warning">Edit</a>
                                            <a href="{{ route('copyrights.show',$copyright->id) }}" class="btn btn-success">View</a>
                                            <a href="{{url('/copyright/'.$copyright->id.'/update_status')}}" class="btn btn-info">Update</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var x = document.getElementById("myTable").rows.length - 1;
        document.getElementById("demo").innerHTML = "Total Records are " + x;
    </script>

    <script type="text/javascript">
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping

        });
  </script>
@include('include.footer')
