@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Copyright | <a href="{{ route('copyrights.index') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Copyright </li>
                        </ol>
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-rounded" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ $message }}
                </div>
            @endif

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <p id="demo" align="center"></p>
                        <table id="myTable" class="table table-bordered"  data-page-length='50'>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Diary number</th>
                                    <th>Applicant</th>
                                    <th>Date of filing</th>
                                    <th>Title of Work</th>
                                    <th>Image</th>
                                    <th>Agent</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($posts as $copyright)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $copyright->diary }}</td>
                                        <td>{{ $copyright->nofa }}</td>
                                        <td>{{ date('d-m-Y',strtotime($copyright->dof)) }}</td>
                                        <td>{{ $copyright->titlew }}</td>
                                        <td>
                                            @if ($copyright->image !='')
                                                <a href="{{url('public/copyright/img/'.$copyright->image )}}" download="">{{ $copyright->image }}</a>
                                            @endif
                                        <td>
                                            @if($copyright->agent_id !='')
                                            {{ $copyright->agent->name }}
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('copyrights.edit',$copyright->id) }}" type="edit" class="btn btn-warning">Edit</a>
                                            <a href="{{ route('copyrights.show',$copyright->id) }}" class="btn btn-success">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var x = document.getElementById("myTable").rows.length - 1;
        document.getElementById("demo").innerHTML = "Total Records are " + x;
    </script>

    <script type="text/javascript">
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping

        });
  </script>
@include('include.footer')
