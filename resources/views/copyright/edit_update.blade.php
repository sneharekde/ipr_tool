@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Update Status of Copyright | <a href="{{action('CopyrightController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Update Status of Copyright</li>
                        </ol>
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="card">
                <div class="card-body">
                    <form action="{{ url('/copyright/edit_status/submit') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    @csrf
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="hidden" name="copy_id" value="{{ $copyright->id }}">
                                <label>Status:</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="" @if($copyright->status == '') selected @else disabled @endif>Select Status</option>
                                    <option value="Active Application" @if($copyright->status == 'Active Application') selected @else disabled @endif>Active Application</option>
                                    <option value="Abandoned" @if($copyright->status == 'Abandoned') selected @else disabled @endif>Abandoned</option>
                                    <option value="Rejected" @if($copyright->status == 'Rejected') selected @else disabled @endif>Rejected</option>
                                    <option value="Registered" @if($copyright->status == 'Registered') selected @else disabled @endif>Registered</option>
                                    <option value="Expired" @if($copyright->status == 'Expired') selected @else disabled @endif>Expired</option>
                                    <option value="Renewed" @if($copyright->status == 'Renewed') selected @else disabled @endif>Renewed</option>
                                    <option value="Extension expired" @if($copyright->status == 'Extension expired') selected @else disabled @endif>Extension expired</option>
                                </select>
                            </div>
                        </div>
                        @if ($copyright->sub_status != '')
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="sub_status" >
                            <div class="form-group">
                                <label>Sub Status:</label>
                                <select class="form-control" id="select1" name="sub_status">
                                    <option value="" @if($copyright->sub_status == '') selected @else disabled @endif>Select Sub Status</option>
                                    <option value="Applied CR" disabled>Applied CR</option>
                                    <option value="Mandatory waiting period" @if($copyright->sub_status == 'Mandatory waiting period') selected @else disabled @endif>Mandatory waiting period</option>
                                    <option value="Opposition" @if($copyright->sub_status == 'Opposition') selected @else disabled @endif>Opposition</option>
                                    <option value="Reply to opposition submitted" @if($copyright->sub_status == 'Reply to opposition submitted') selected @else disabled @endif>Reply to opposition submitted</option>
                                    <option value="Hearing" @if($copyright->sub_status == 'Hearing') selected @else disabled @endif>Hearing</option>
                                    <option value="Appeal" @if($copyright->sub_status == 'Appeal') selected @else disabled @endif>Appeal</option>
                                    <option value="Scrutinization stage" @if($copyright->sub_status == 'Scrutinization stage') selected @else disabled @endif>Scrutinization stage</option>
                                    <option value="Discrepancy stage" @if($copyright->sub_status == 'Discrepancy stage') selected @else disabled @endif>Discrepancy stage</option>
                                </select>
                            </div>
                        </div>
                        @endif

                        {{-- Status --}}
                        @if ($copyright->status == 'Abandoned')
                        <!-- Abandoned -->
                        <div  id="aban">
                            <h4 class="text-center">Abandoned</h4><hr>
                            <div class="col-md-12 col-lg-12 col-sm-122 col-xs-12">
                                <div class="form-group">
                                    <label>Date:</label>
                                    <input type="text" id="aband" name="date1" class="form-control" value="{{ $copyright->date }}">
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-122 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment1" rows="5">{{ $copyright->comment }}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    @if ($copyright->docs !='')
                                    <div class="col-md-6">
                                        <a href="{{ url('public/copyright/'.$copyright->docs) }}" download="">{{ $copyright->docs }}</a>
                                        <a href="{{ url('copy/delete/'.$copyright->docs.'/'.$copyright->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                    </div>
                                    @else
                                    <input type="file" name="docs1" class="form-control">
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($copyright->status == 'Rejected')
                        <!-- Rejected -->
                        <div  id="rejc">
                            <h4 class="text-center">Rejected</h4><hr>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date:</label>
                                    <input type="text" id="rejectedd" name="date2" class="form-control" value="{{ $copyright->date }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment2" rows="5">{{ $copyright->comment }}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    @if ($copyright->docs !='')
                                    <div class="col-md-6">
                                        <a href="{{ url('public/copyright/'.$copyright->docs) }}" download="">{{ $copyright->docs }}</a>
                                        <a href="{{ url('copy/delete/'.$copyright->docs.'/'.$copyright->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                    </div>
                                    @else
                                    <input type="file" name="docs2" class="form-control">
                                    @endif

                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($copyright->status == 'Registered')
                        {{--  Regestered  --}}
                        <div id="reg">
                            <h4 class="text-center">Registered</h4><hr>
                            <div class="col-lg-12 col-md-12  col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Date of registration:</label>
                                    <input type="text" id="regisd" name="dor" class="form-control" value="{{ $copyright->dor }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12  col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Certificate of registration (doc):</label>
                                    @if ($copyright->docs !='')
                                    <div class="col-md-6">
                                        <a href="{{ url('public/copyright/'.$copyright->docs) }}" download="">{{ $copyright->docs }}</a>
                                        <a href="{{ url('copy/delete/'.$copyright->docs.'/'.$copyright->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                    </div>
                                    @else
                                    <input type="file" name="cor" class="form-control">
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment3" rows="5">{{ $copyright->comment }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label for="">Reminder(Optionals)</label>
                                    <input type="text" id="reg_remider" name="reg_remider" class="form-control" value="{{ $copyright->reg_rem }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>ROC No.</label>
                                    <input type="text" name="roc" class="form-control" value="{{ $trade->roc }}">
                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($copyright->status == 'Expired')
                        <!-- Expired -->
                        <div id="expr">
                            <h4 class="text-center">Expired</h4><hr>
                            <div class="col-md-12 form-group">
                                {!! Form::label('exp_date_exp', 'Date of Expiry of application') !!}
                                {!! Form::text('exp_date_exp', $copyright->exp_date, ['class' => 'form-control','id' => 'exp_date_exp']) !!}
                            </div>
                            <div class="col-md-12 form-group">
                                {!! Form::label('comment_exp', 'Remarks') !!}
                                {!! Form::text('comment_exp', $copyright->comment, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-md-12 form-group">
                                {!! Form::label('docs_exp', 'Documents') !!}
                                @if ($copyright->docs !='')
                                <div class="col-md-6">
                                    <a href="{{ url('public/copyright/'.$copyright->docs) }}" download="">{{ $copyright->docs }}</a>
                                    <a href="{{ url('copy/delete/'.$copyright->docs.'/'.$copyright->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                </div>
                                @else
                                {!! Form::file('docs_exp', ['class' => 'form-control']) !!}
                                @endif

                            </div>
                        </div>
                        @endif
                        @if ($copyright->status == 'Renewed')
                        <!-- Renewed -->
                        <div  id="renw">
                            <h4 class="text-center">Renewed</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of renewal:</label>
                                    <input type="text" id="rend" name="date3" class="form-control" value="{{ $copyright->ren_date }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of expiration of extension:</label>
                                    <input type="text" id="expd" name="doexp" class="form-control" value="{{ $copyright->exp_date }}">
                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($copyright->status == 'Extension expired')
                        {{--  Extension Expire  --}}
                        <div id="ext">
                            <h4 class="text-center">Extension expired</h4><hr>
                        </div>
                        @endif

                        {{-- Sub Status --}}
                        @if ($copyright->sub_status == 'Mandatory waiting period')
                        <!-- Mandatory waiting period -->
                        <div  id="mwp">
                            <h4 class="text-center">Mandatory waiting period</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Reminder:</label>
                                    <input type="text" id="mwpd" name="rem" class="form-control" value="{{ $copyright->rem_mwp_date }}">
                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($copyright->sub_status == 'Opposition')
                        {{--  Opposition  --}}
                        <div id="ops">
                            <h4 class="text-center">Opposition:</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of the receipt of opposition:</label>
                                    <input type="text" id="doopd" name="date4" class="form-control" value="{{ $copyright->date }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" rows="5" name="comment4">{{ $copyright->comment }}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    @if ($copyright->docs !='')
                                    <div class="col-md-6">
                                        <a href="{{ url('public/copyright/'.$copyright->docs) }}" download="">{{ $copyright->docs }}</a>
                                        <a href="{{ url('copy/delete/'.$copyright->docs.'/'.$copyright->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                    </div>
                                    @else
                                    <input type="file" name="docs3" class="form-control">
                                    @endif

                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>1st reminder:</label>
                                    <input type="text" id="remd1" name="rem11" class="form-control" value="{{ $copyright->rem1 }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>2nd reminder:</label>
                                    <input type="text" id="remd2" name="rem21" class="form-control" value="{{ $copyright->rem2 }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>3rd reminder:</label>
                                    <input type="text" id="remd3" name="rem31" class="form-control" value="{{ $copyright->rem3 }}">
                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($copyright->sub_status == 'Reply to opposition submitted')
                        <!-- Reply to opposition submitted -->
                        <div id="rtos">
                            <h4 class="text-center">Reply to opposition submitted</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date:</label>
                                    <input type="text" id="rtopsd" name="date5" class="form-control" value="{{ $copyright->date }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" rows="5" name="comment5">{{ $copyright->comment }}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    @if ($copyright->docs !='')
                                    <div class="col-md-6">
                                        <a href="{{ url('public/copyright/'.$copyright->docs) }}" download="">{{ $copyright->docs }}</a>
                                        <a href="{{ url('copy/delete/'.$copyright->docs.'/'.$copyright->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                    </div>
                                    @else
                                    <input type="file" name="docs4" class="form-control">
                                    @endif

                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($copyright->sub_status == 'Hearing')
                        <!-- Hearing -->
                        <div id="her">
                            <h4 class="text-center">Hearing</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date for hearing:</label>
                                    <input type="text" id="hdd" name="date6" class="form-control" value="{{ $copyright->dfh }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document :</label>
                                    @if ($copyright->docs !='')
                                    <div class="col-md-6">
                                        <a href="{{ url('public/copyright/'.$copyright->docs) }}" download="">{{ $copyright->docs }}</a>
                                        <a href="{{ url('copy/delete/'.$copyright->docs.'/'.$copyright->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                    </div>
                                    @else
                                    <input type="file" name="docs5" class="form-control">
                                    @endif

                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment6" rows="5">{{ $copyright->comment }}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>1st reminder:</label>
                                    <input type="text" id="remd11" name="rem12" class="form-control" value="{{ $copyright->rem1 }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>2nd reminder:</label>
                                    <input type="text" id="remd12" name="rem22" class="form-control" value="{{ $copyright->rem2 }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>3rd reminder:</label>
                                    <input type="text" id="remd13" name="rem32" class="form-control" value="{{ $copyright->rem3 }}">
                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($copyright->sub_status == 'Appeal')
                        <!-- Appeal -->
                        <div id="appe">
                            <h4 class="text-center">Appeal</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of decision / judgement:</label>
                                    <input type="text" id="appdd" class="form-control" value="{{ $copyright->ddj }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>1st reminder:</label>
                                    <input type="text" id="remd21" name="rem13" class="form-control" value="{{ $copyright->rem1 }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>2nd reminder:</label>
                                    <input type="text" id="remd22" name="rem23" class="form-control" value="{{ $copyright->rem2 }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>3rd reminder:</label>
                                    <input type="text" id="remd23" name="rem33" class="form-control" value="{{ $copyright->rem3 }}">
                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($copyright->sub_status == 'Scrutinization stage')
                        <!-- Scrutinization stage: -->
                        <div id="scs">
                            <h4 class="text-center">Scrutinization stage:</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of receipt of the report:</label>
                                    <input type="text" id="scrd" name="date8" class="form-control" value="{{ $copyright->d_rer }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment7" rows="5">{{ $copyright->comment }}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    @if ($copyright->docs !='')
                                    <div class="col-md-6">
                                        <a href="{{ url('public/copyright/'.$copyright->docs) }}" download="">{{ $copyright->docs }}</a>
                                        <a href="{{ url('copy/delete/'.$copyright->docs.'/'.$copyright->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                    </div>
                                    @else
                                    <input type="file" name="docs6" class="form-control">
                                    @endif

                                </div>
                            </div>
                        </div>
                        @endif
                        @if ($copyright->sub_status == 'Discrepancy stage')
                        <!-- Discrepancy stage: -->
                        <div id="dcs">
                            <h4 class="text-center">Discrepancy stage:</h4><hr>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of receipt of the report:</label>
                                    <input type="text" id="dsds" name="date9" class="form-control" value="{{ $copyright->d_rer }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment8" rows="5">{{ $copyright->comment }}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    @if ($copyright->docs !='')
                                    <div class="col-md-6">
                                        <a href="{{ url('public/copyright/'.$copyright->docs) }}" download="">{{ $copyright->docs }}</a>
                                        <a href="{{ url('copy/delete/'.$copyright->docs.'/'.$copyright->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                    </div>
                                    @else
                                    <input type="file" name="docs7" class="form-control">
                                    @endif

                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>1st reminder</label>
                                    <input type="text" id="remd31" name="rem14" class="form-control" value="{{ $copyright->rem1 }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>2nd reminder</label>
                                    <input type="text" id="remd32" name="rem24" class="form-control" value="{{ $copyright->rem2 }}">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>3rd reminder</label>
                                    <input type="text" id="remd33" name="rem34" class="form-control" value="{{ $copyright->rem3 }}">
                                </div>
                            </div>
                        </div>
                        @endif
                        <!-- End of the status and sub status -->
                        <div class="col-sm-12 col-xs-12 col-lg-12 col-md-12">
                            <div class="form-group text-center">
                                <input type="submit" name="submit" value="Submit" class="btn btn-success">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script>
        $(function() {
            $('#exp_date_exp').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#reg_remider').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#appdd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#dsds').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });
            $('#remd31').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd32').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd33').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#scrd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#remd21').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd22').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#remd23').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#rend').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#hdd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#remd11').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd12').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd13').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#rtopsd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#doopd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#remd1').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd2').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd3').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#mwpd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#expd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#regisd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#remd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#aband').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#rejectedd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });
        });
    </script>

@include('include.footer')
