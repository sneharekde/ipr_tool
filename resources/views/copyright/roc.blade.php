@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Registration Certificate(ROC) | <a href="{{ route('copyrights.show',$post->id) }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Registration Certificate(ROC)</li>
                        </ol>
                    </div>
                </div>
            </div>

            <div class="card">
                @include('include.message')
                <div class="card-body">
                    @if ($post->roc && $post->reg_cert =='')
                        {!! Form::open(['url' => ['roc_save',$post->id],'data-parsley-validate' => '','method' => 'POST','files' => true]) !!}
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    {!! Form::label('roc', 'ROC Number:') !!}
                                    {!! Form::text('roc', null, ['class' => 'form-control','required' => '']) !!}
                                </div>
                                <div class="col-md-6 form-group">
                                    {!! Form::label('reg_cert', 'Certificate of Registration:') !!}
                                    {!! Form::file('reg_cert', ['class' => 'form-control','required' => '','accept' => 'application/pdf']) !!}
                                </div>
                                <div class="col-md-12 form-group text-center">
                                    {!! Form::submit('submit', ['class' => 'btn btn-success']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    @else
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ROC NUmber</th>
                                    <th>Certification of Registration</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $post->roc }}</td>
                                    <td><a href="{{ url('public/copyright/img/'.$post->reg_cert) }}" download="">{{ $post->reg_cert }}</a></td>
                                    <td>
                                        <a href="{{ url('reg_cert/edit',$post->id) }}" class="btn btn-sm btn-success">Edit</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    @endif

            	</div>
            </div>
		</div>
	</div>
@include('include.footer')
