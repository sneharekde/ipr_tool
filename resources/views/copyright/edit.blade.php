@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Edit Copyright | <a href="{{ url('copyrights') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Edit Copyright</li>
                        </ol>
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="card">
                <div class="card-body">
                    {!! Form::model($copyright, ['url' => ['copyright/update',$copyright->id], 'method' => 'POST' ,'data-parsley-validate' => '']) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('gov','Govt. Fee * :') }}
                                    {{ Form::text('gov',null,['class' => 'form-control','required' => '', 'maxlength' =>'255']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('diary','Diary Number:') }}
                                    {{ Form::text('diary',null,['class' => 'form-control','required' => '', 'maxlength' =>'255']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('dof','Date of Filing * :') !!}
                                    {!! Form::text('dof', null, ['class' => 'form-control','required' => '','id' => 'datepicker']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('user_id','User* :') }}
                                    {{ Form::select('user_id',$ar2,null,['class' => 'form-control','required' => '','placeholder' => 'Select']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('nofa','Name of the Applicant * :') }}
                                    {{ Form::text('nofa',null,['class' => 'form-control','required' => '', 'maxlength' =>'255']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('addrp','Address of the Applicant * :') }}
                                    {{ Form::text('addrp',null,['class' => 'form-control','required' => '', 'maxlength' =>'255']) }}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('app_filed', 'Application filed by:') !!}
                                    {!! Form::select('app_filed', ['Agent/Attorney' => 'Agent/Attorney','Applicant' => 'Applicant'], null, ['class' => 'form-control','id' => 'appf_filed' ,'required' => '','placeholder' => 'Select']) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                @if ($copyright->app_filed == 'Agent/Attorney')
                                    <div class="row" id="agent_attorney">
                                        <div class="col-md-6 form-group">
                                            {!! Form::label('agent_id', 'Name of the Agent/Attorney') !!}
                                            {!! Form::select('agent_id', $ar1, null, ['class' => 'form-control','placeholder' => 'Select','id' => 'agent_id']) !!}
                                        </div>
                                        <div class="col-md-6 form-group">
                                            {!! Form::label('agent_code', 'Agent/Attorney code') !!}
                                            {!! Form::text('agent_code', null, ['class' => 'form-control','id' => 'agent_code']) !!}
                                        </div>
                                        <div class="col-md-6 form-group">
                                            {!! Form::label('poa', 'upload POA') !!}
                                            @if ($copyright->poa !='')
                                            <div class="col-md-12">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td><a href="{{ url('public/copyright/docs',$copyright->poa) }}" download="">{{ $copyright->poa }}</a></td>
                                                        <td><a href="{{ url('delete/poa/'.$copyright->poa.'/'.$copyright->id) }}" class="btn btn-xs btn-danger">Delete</a></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            @else
                                            {!! Form::file('poa', ['class' => 'form-control']) !!}
                                            @endif
                                        </div>
                                        <div class="col-md-6 form-group">
                                            {!! Form::label('agent_address', 'Address of the agent/attorney') !!}
                                            {!! Form::text('agent_address', null, ['class' => 'form-control','id' => 'agent_address']) !!}
                                        </div>
                                    </div>
                                @else
                                <div class="row" id="agent_attorney" style="display: none">
                                    <div class="col-md-6 form-group">
                                        {!! Form::label('agent_id', 'Name of the Agent/Attorney') !!}
                                        {!! Form::select('agent_id', $ar1, null, ['class' => 'form-control','placeholder' => 'Select','id' => 'agent_id']) !!}
                                    </div>
                                    <div class="col-md-6 form-group">
                                        {!! Form::label('agent_code', 'Agent/Attorney code') !!}
                                        {!! Form::text('agent_code', null, ['class' => 'form-control','id' => 'agent_code']) !!}
                                    </div>
                                    <div class="col-md-6 form-group">
                                        {!! Form::label('poa', 'upload POA') !!}
                                        {!! Form::file('poa', ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="col-md-6 form-group">
                                        {!! Form::label('agent_address', 'Address of the agent/attorney') !!}
                                        {!! Form::text('agent_address', null, ['class' => 'form-control','id' => 'agent_address']) !!}
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('titlew','Class of Work * :') }}
                                    {{ Form::text('titlew',null,['class' => 'form-control','required' => '', 'maxlength' =>'255']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('work_title','Work title * :') }}
                                    {{ Form::text('work_title',null,['class' => 'form-control','required' => '', 'maxlength' =>'255']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('image','Upload work:') }}
                                    @if ($copyright->image !='')
                                    <div class="col-md-6">
                                        <img src="{{ url('public/copyright/img/'.$copyright->image) }}" alt="" class="img-responsive">
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{ url('imagesc/delete/'.$copyright->image.'/'.$copyright->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                    </div>
                                    @else
                                    {{ Form::file('image',['class' => 'form-control']) }}
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('description','Description * :') }}
                                    {{ Form::textarea('description',null,['class' => 'form-control','required' => '', 'maxlength' =>'255']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('lang_id','Language  :') }}
                                    {{ Form::select('lang_id',$ar3,null,['class' => 'form-control','placeholder' => 'Select']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('remarks','Remarks:') }}
                                    {{ Form::text('remarks',null,['class' => 'form-control','maxlength' =>'255']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('user_aff','User Affidavit :') }}
                                    @if ($copyright->user_aff !='')
                                    <div class="col-md-6">
                                        <a href="{{ url('public/copyright/docs/'.$copyright->user_aff) }}" download="">{{ $copyright->user_aff }}</a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{ url('imagesc/deletec/'.$copyright->user_aff.'/'.$copyright->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                    </div>
                                    @else
                                    {{ Form::file('user_aff',['class' => 'form-control']) }}
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('copy_app','Payment Details(Pdf) :') }}
                                    @if ($copyright->copy_app !='')
                                    <div class="col-md-6">
                                        <a href="{{ url('public/copyright/docs/'.$copyright->copy_app) }}" download="">{{ $copyright->copy_app }}</a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{ url('imagesc/deleteca/'.$copyright->copy_app.'/'.$copyright->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                    </div>
                                    @else
                                    {{ Form::file('copy_app',['class' => 'form-control']) }}
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                <input type="submit" name="draft" id="" value="Save Draft" class="btn btn-info">
                                <input type="submit" name="submit" value="Submit" class="btn btn-success">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/bootstrap.min.js"></script>
    <script>
         $(document).ready(function() {
        $('#state').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: 'findCityWithStateID/'+stateID,
                    type: "GET",
                    data : {"_token":"{{ csrf_token() }}"},
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    dataType: "json",
                    success:function(data) {
                        //console.log(data);
                      if(data){
                        $('#city').empty();
                        $('#city').focus;
                        $('#city').append('<option value="">-- Select --</option>');
                        $.each(data, function(key, value){
                        $('select[name="city[]"').append('<option value="'+ value.class +'">' + value.class+ '</option>');
                    });
         //         dlb1 = new DualListbox('#city');
                  }else{
                    $('#city').empty();
                  }
                  }
                });
            }else{
              $('#city').empty();
            }
        });
    });
    </script>



  <script>
  $(function () {

    //Date picker
    $('#datepicker').datepicker({
      format: 'dd/mm/yy',
      autoclose: true,
      todayHighlight: true
    }).datepicker("setDate", new Date());

    $('#Todatepicker').datepicker({
      format: 'dd/mm/yy',
      autoclose: true,
      todayHighlight: true
    }).datepicker("setDate", new Date());

    $('#reservationtime').datepicker({
      format: 'dd/mm/yy',
      autoclose: true,
      todayHighlight: true
    }).datepicker("setDate", new Date());

    $('#priority_since').datepicker({
      format: 'dd/mm/yy',
      autoclose: true,
      todayHighlight: true
    }).datepicker("setDate", new Date());

  });


</script>
    <script>
        $(document).ready(function(){
            $('#appf_filed').on('change',function(){
                var appf_filed = $(this).val();
                if(appf_filed == 'Agent/Attorney'){
                    $('#agent_attorney').show();
                }
                if(appf_filed != 'Agent/Attorney'){
                    $('#agent_attorney').hide();
                }
            });

            $('#agent_id').on('change',function(){
                var agent_id = $(this).val();
                $('#agent_code').attr('value','');
                $('#agent_address').attr('value','');
                $.ajax({
                    type:'get',
                    url:'{!! URL::to('findagent') !!}',
                    data:{'id':agent_id},
                    dataType:'json',
                    success:function(data){
                    $('#agent_code').attr('value',data.registration_no);
                    $('#agent_address').attr('value',data.address);
                },
                });
            });
        });
    </script>

@include('include.footer')
