@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Update Status of Copyright | <a href="{{action('CopyrightController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Update Status of Copyright</li>
                        </ol>
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="card">
                <div class="card-body">
                    <form action="{{ url('/copyright/status/submit') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    @csrf
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="hidden" name="copy_id" value="{{ $copyright->id }}">
                                <label>Status:</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="">Select Status</option>
                                    <option value="Active Application" @if($copyright->status == 'Abandoned' || $copyright->status == 'Rejected' || $copyright->status == 'Registered' || $copyright->status == 'Expired' || $copyright->status == 'Renewed' || $copyright->status == 'Extension expired') disabled @endif>Active Application</option>
                                    <option value="Abandoned" @if($copyright->status == 'Abandoned' || $copyright->status == 'Rejected' || $copyright->status == 'Registered' || $copyright->status == 'Expired' || $copyright->status == 'Renewed' || $copyright->status == 'Extension expired') disabled @endif>Abandoned</option>
                                    <option value="Rejected" @if($copyright->status == 'Rejected' || $copyright->status == 'Registered' || $copyright->status == 'Expired' || $copyright->status == 'Renewed' || $copyright->status == 'Extension expired') disabled @endif>Rejected</option>
                                    <option value="Registered" @if($copyright->status == 'Registered' || $copyright->status == 'Expired' || $copyright->status == 'Renewed' || $copyright->status == 'Extension expired') disabled @endif>Registered</option>
                                    <option value="Expired" @if($copyright->status == 'Expired' || $copyright->status == 'Renewed' || $copyright->status == 'Extension expired') disabled @endif>Expired</option>
                                    <option value="Renewed" @if($copyright->status == 'Renewed' || $copyright->status == 'Extension expired') disabled @endif>Renewed</option>
                                    <option value="Extension expired" @if($copyright->status == 'Extension expired') disabled @endif>Extension expired</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="sub_status" >
                            <div class="form-group">
                                <label>Sub Status:</label>
                                <select class="form-control" id="select1" name="sub_status">
                                    <option value="">Select Sub Status</option>
                                    <option value="Applied CR" disabled>Applied CR</option>
                                    <option value="Mandatory waiting period" @if(($copyright->status == 'Active Application' && $copyright->sub_status == 'Mandatory waiting period') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Opposition') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Reply to opposition submitted') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Hearing') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Appeal') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Scrutinization stage') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Discrepancy stage')) disabled @endif>Mandatory waiting period</option>
                                    <option value="Opposition" @if(($copyright->status == 'Active Application' && $copyright->sub_status == 'Opposition') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Reply to opposition submitted') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Hearing') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Appeal') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Scrutinization stage') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Discrepancy stage')) disabled @endif>Opposition</option>
                                    <option value="Reply to opposition submitted" @if(($copyright->status == 'Active Application' && $copyright->sub_status == 'Reply to opposition submitted') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Hearing') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Appeal') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Scrutinization stage') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Discrepancy stage')) disabled @endif>Reply to opposition submitted</option>
                                    <option value="Hearing" @if(($copyright->status == 'Active Application' && $copyright->sub_status == 'Hearing') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Appeal') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Scrutinization stage') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Discrepancy stage')) disabled @endif>Hearing</option>
                                    <option value="Appeal" @if(($copyright->status == 'Active Application' && $copyright->sub_status == 'Appeal') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Scrutinization stage') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Discrepancy stage')) disabled @endif>Appeal</option>
                                    <option value="Scrutinization stage" @if(($copyright->status == 'Active Application' && $copyright->sub_status == 'Scrutinization stage') || ($copyright->status == 'Active Application' && $copyright->sub_status == 'Discrepancy stage')) disabled @endif>Scrutinization stage</option>
                                    <option value="Discrepancy stage" @if(($copyright->status == 'Active Application' && $copyright->sub_status == 'Discrepancy stage')) disabled @endif>Discrepancy stage</option>
                                </select>
                            </div>
                        </div>
                        {{-- Status --}}
                        <!-- Abandoned -->
                        <div  id="aban">
                            <h4 class="text-center">Abandoned</h4><hr>
                            <div class="col-md-12 col-lg-12 col-sm-122 col-xs-12">
                                <div class="form-group">
                                    <label>Date:</label>
                                    <input type="text" id="aband" name="date1" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-122 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment1" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    <input type="file" name="docs1" class="form-control">
                                </div>
                            </div>
                        </div>
                        <!-- Rejected -->
                        <div  id="rejc">
                            <h4 class="text-center">Rejected</h4><hr>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date:</label>
                                    <input type="text" id="rejectedd" name="date2" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment2" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    <input type="file" name="docs2" class="form-control">
                                </div>
                            </div>
                        </div>
                        {{--  Regestered  --}}
                        <div id="reg">
                            <h4 class="text-center">Registered</h4><hr>
                            <div class="col-lg-12 col-md-12  col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Date of registration:</label>
                                    <input type="text" id="regisd" name="dor" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12  col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Certificate of registration (doc):</label>
                                    <input type="file" name="cor" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment3" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="">Reminders (Yes / No)</label>
                                <select name="rem_option" id="rem_option" class="form-control">
                                    <option value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" id="remds" style="display: none">
                                <div class="form-group">
                                    <label for="">Reminder(Optionals)</label>
                                    <input type="text" id="remd" name="reg_remider" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>ROC No.</label>
                                    <input type="text" name="roc" class="form-control">
                                </div>
                            </div>
                        </div>
                        <!-- Expired -->
                        <div id="expr">
                            <h4 class="text-center">Expired</h4><hr>
                            <div class="col-md-12 form-group">
                                {!! Form::label('exp_date_exp', 'Date of Expiry of application') !!}
                                {!! Form::text('exp_date_exp', null, ['class' => 'form-control','id' => 'exp_date_exp']) !!}
                            </div>
                            <div class="col-md-12 form-group">
                                {!! Form::label('comment_exp', 'Remarks') !!}
                                {!! Form::text('comment_exp', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-md-12 form-group">
                                {!! Form::label('docs_exp', 'Documents') !!}
                                {!! Form::file('docs_exp', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <!-- Renewed -->
                        <div  id="renw">
                            <h4 class="text-center">Renewed</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of renewal:</label>
                                    <input type="text" id="rend" name="date3" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of expiration of extension:</label>
                                    <input type="text" id="expd" name="doexp" class="form-control">
                                </div>
                            </div>
                        </div>
                        {{--  Extension Expire  --}}
                        <div id="ext">
                            <h4 class="text-center">Extension expired</h4><hr>
                        </div>
                        {{-- Sub Status --}}
                        <!-- New application -->
                        <div id="new">
                            <h4 class="text-center">New Application</h4><hr>
                        </div>
                        <!-- Mandatory waiting period -->
                        <div  id="mwp">
                            <h4 class="text-center">Mandatory waiting period</h4><hr>
                            <div class="col-md-12 form-group">
                                <label for="">Reminders (Yes / No)</label>
                                <select name="rem_option2" id="rem_option2" class="form-control">
                                    <option value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="mwpds" style="display: none">
                                <div class="form-group">
                                    <label>Reminder:</label>
                                    <input type="text" id="mwpd" name="rem" class="form-control">
                                </div>
                            </div>
                        </div>
                        {{--  Opposition  --}}
                        <div id="ops">
                            <h4 class="text-center">Opposition:</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of the receipt of opposition:</label>
                                    <input type="text" id="doopd" name="date4" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" rows="5" name="comment4"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    <input type="file" name="docs3" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="">Reminders (Yes / No)</label>
                                <select name="rem_option3" id="rem_option3" class="form-control">
                                    <option value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="remops" style="display: none">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>1st reminder:</label>
                                            <input type="text" id="remd1" name="rem11" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>2nd reminder:</label>
                                            <input type="text" id="remd2" name="rem21" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>3rd reminder:</label>
                                            <input type="text" id="remd3" name="rem31" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Reply to opposition submitted -->
                        <div id="rtos">
                            <h4 class="text-center">Reply to opposition submitted</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date:</label>
                                    <input type="text" id="rtopsd" name="date5" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" rows="5" name="comment5"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    <input type="file" name="docs4" class="form-control">
                                </div>
                            </div>
                        </div>
                        <!-- Hearing -->
                        <div id="her">
                            <h4 class="text-center">Hearing</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date for hearing:</label>
                                    <input type="text" id="hdd" name="date6" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document :</label>
                                    <input type="file" name="docs5" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment6" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="">Reminders (Yes / No)</label>
                                <select name="rem_option4" id="rem_option4" class="form-control">
                                    <option value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="remher" style="display: none">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>1st reminder:</label>
                                            <input type="text" id="remd11" name="rem12" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>2nd reminder:</label>
                                            <input type="text" id="remd12" name="rem22" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>3rd reminder:</label>
                                            <input type="text" id="remd13" name="rem32" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Appeal -->
                        <div id="appe">
                            <h4 class="text-center">Appeal</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of decision / judgement:</label>
                                    <input type="text" id="appdd" name="date7" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="">Reminders (Yes / No)</label>
                                <select name="rem_option5" id="rem_option5" class="form-control">
                                    <option value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="remap" style="display: none">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>1st reminder:</label>
                                            <input type="text" id="remd21" name="rem13" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>2nd reminder:</label>
                                            <input type="text" id="remd22" name="rem23" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>3rd reminder:</label>
                                            <input type="text" id="remd23" name="rem33" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- Scrutinization stage: -->
                        <div id="scs">
                            <h4 class="text-center">Scrutinization stage:</h4><hr>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of receipt of the report:</label>
                                    <input type="text" id="scrd" name="date8" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment7" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    <input type="file" name="docs6" class="form-control">
                                </div>
                            </div>
                        </div>
                        <!-- Discrepancy stage: -->
                        <div id="dcs">
                            <h4 class="text-center">Discrepancy stage:</h4><hr>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Date of receipt of the report:</label>
                                    <input type="text" id="dsds" name="date9" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Comment:</label>
                                    <textarea class="form-control" name="comment8" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Document:</label>
                                    <input type="file" name="docs7" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="">Reminders (Yes / No)</label>
                                <select name="rem_option6" id="rem_option6" class="form-control">
                                    <option value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="remdisc" style="display: none">
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>1st reminder</label>
                                            <input type="text" id="remd31" name="rem14" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>2nd reminder</label>
                                            <input type="text" id="remd32" name="rem24" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>3rd reminder</label>
                                            <input type="text" id="remd33" name="rem34" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- End of the status and sub status -->
                        <div class="col-sm-12 col-xs-12 col-lg-12 col-md-12">
                            <div class="form-group text-center">
                                <input type="submit" name="submit" value="Submit" class="btn btn-success">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Sub Status</th>
                                    <th>Document</th>
                                    <th>Comments</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($statuss as $status)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>
                                            @if($status->date !='')
                                                {{ date('d-m-Y',strtotime($status->date)) }}
                                            @elseif($status->dor != '')
                                                {{ date('d-m-Y',strtotime($status->dor)) }}
                                            @elseif($status->doe != '')
                                                {{ date('d-m-Y',strtotime($status->doe)) }}
                                            @endif
                                        </td>
                                        <td>{{ $status->status }}</td>
                                        <td>{{ $status->sub_status }}</td>
                                        <td>
                                            @if($status->docs != '')
                                                @foreach(explode(',', $status->docs) as $file)
                                                    <a href="{{ url('public/copyright/'.$file) }}" download="">{{ $file }}</a>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>{{ $status->comment }}</td>
                                        <td>
                                            @if ($status->sub_status !='Applied CR')
                                            <a href="{{ url('copyright/edit/update_status/'.$status->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                            @endif

                                            {{--  <a href="" class="btn btn-sm btn-danger">Delete</a>  --}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function(){
            $('#rem_option').on('change',function(){
                var remdop = $(this).val();
                if(remdop == 'Yes'){
                    $('#remds').show();
                }
                if(remdop !='Yes'){
                    $('#remds').hide();
                }
            });

            $('#rem_option2').on('change',function(){
                var remdop = $(this).val();
                if(remdop == 'Yes'){
                    $('#mwpds').show();
                }
                if(remdop !='Yes'){
                    $('#mwpds').hide();
                }
            });

            $('#rem_option3').on('change',function(){
                var remdop = $(this).val();
                if(remdop == 'Yes'){
                    $('#remops').show();
                }
                if(remdop !='Yes'){
                    $('#remops').hide();
                }
            });

            $('#rem_option4').on('change',function(){
                var remdop = $(this).val();
                if(remdop == 'Yes'){
                    $('#remher').show();
                }
                if(remdop !='Yes'){
                    $('#remher').hide();
                }
            });

            $('#rem_option5').on('change',function(){
                var remdop = $(this).val();
                if(remdop == 'Yes'){
                    $('#remap').show();
                }
                if(remdop !='Yes'){
                    $('#remap').hide();
                }
            });

            $('#rem_option6').on('change',function(){
                var remdop = $(this).val();
                if(remdop == 'Yes'){
                    $('#remdisc').show();
                }
                if(remdop !='Yes'){
                    $('#remdisc').hide();
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#sub_status').hide();
            $('#aban').hide();
            $('#rejc').hide();
            $('#reg').hide();
            $('#expr').hide();
            $('#renw').hide();
            $('#ext').hide();

            {{--  Sub_Status  --}}
            $('#new').hide();
            $('#mwp').hide();
            $('#ops').hide();
            $('#rtos').hide();
            $('#her').hide();
            $('#appe').hide();
            $('#scs').hide();
            $('#dcs').hide();

            $('#status').change(function(){
                if($('#status').val() == ''){
                    $('#sub_status').hide();
                    $('#aban').hide();
                    $('#rejc').hide();
                    $('#reg').hide();
                    $('#expr').hide();
                    $('#renw').hide();
                    $('#ext').hide();

                    $('#new').hide();
                    $('#mwp').hide();
                    $('#ops').hide();
                    $('#rtos').hide();
                    $('#her').hide();
                    $('#appe').hide();
                    $('#scs').hide();
                    $('#dcs').hide();
                }
            });

            $('#status').change(function(){
                if ($('#status').val() == 'Active Application')
                {
                    $('#sub_status').show();
                }else{
                    $('#sub_status').hide();
                    $('#mwp').hide();
                    $('#ops').hide();
                    $('#rtos').hide();
                    $('#her').hide();
                    $('#appe').hide();
                    $('#scs').hide();
                    $('#dcs').hide();
                }
            });

            $('#status').change(function(){
                if ($('#status').val() == 'Abandoned')
                {
                    $('#aban').show();
                }else{
                    $('#aban').hide();
                }
            });

            $('#status').change(function(){
                if ($('#status').val() == 'Rejected')
                {
                    $('#rejc ').show();
                }else{
                    $('#rejc ').hide();
                }
            });

            $('#status').change(function(){
                if ($('#status').val() == 'Registered')
                {
                    $('#reg ').show();
                }else{
                    $('#reg ').hide();
                }
            });

            $('#status').change(function(){
                if ($('#status').val() == 'Expired')
                {
                    $('#expr').show();
                }else{
                    $('#expr').hide();
                }
            });

            $('#status').change(function(){
                if ($('#status').val() == 'Renewed')
                {
                    $('#renw').show();
                }else{
                    $('#renw').hide();
                }
            });
            $('#status').change(function(){
                if ($('#status').val() == 'Extension expired')
                {
                    $('#ext').show();
                }else{
                    $('#ext').hide();
                }
            });

            $('#select1').change(function(){
                if($('#select1').val() == ''){
                    $('#new').hide();
                    $('#mwp').hide();
                    $('#ops').hide();
                    $('#rtos').hide();
                    $('#her').hide();
                    $('#appe').hide();
                    $('#scs').hide();
                    $('#dcs').hide();
                }

                if($('#select1').val() == 'Applied CR'){
                    $('#new').show();
                }else{
                    $('#new').hide();
                }

                if($('#select1').val() == 'Mandatory waiting period'){
                    $('#mwp').show();
                }else{
                    $('#mwp').hide();
                }

                if($('#select1').val() == 'Opposition'){
                    $('#ops').show();
                }else{
                    $('#ops').hide();
                }

                if($('#select1').val() == 'Reply to opposition submitted'){
                    $('#rtos').show();
                }else{
                    $('#rtos').hide();
                }

                if($('#select1').val() == 'Hearing'){
                    $('#her').show();
                }else{
                    $('#her').hide();
                }
                if($('#select1').val() == 'Appeal'){
                    $('#appe').show();
                }else{
                    $('#appe').hide();
                }
                if($('#select1').val() == 'Scrutinization stage'){
                    $('#scs').show();
                }else{
                    $('#scs').hide();
                }
                if($('#select1').val() == 'Discrepancy stage'){
                    $('#dcs').show();
                }else{
                    $('#dcs').hide();
                }
            });
        });
    </script>
    <script>
        $(function() {
            $('#exp_date_exp').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#appdd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#dsds').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });
            $('#remd31').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd32').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd33').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#scrd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#remd21').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd22').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });
            $('#remd23').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#rend').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#hdd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#remd11').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd12').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd13').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#rtopsd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#doopd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#remd1').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd2').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#remd3').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#mwpd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#expd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#regisd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#remd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                //maxDate: 0,
                minDate: +1,
            });

            $('#aband').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });

            $('#rejectedd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });
        });
    </script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
@include('include.footer')
