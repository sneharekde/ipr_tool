@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Add Copyright | <a href="{{action('CopyrightController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Add Copyright</li>
                        </ol>
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="card">
                <div class="card-body">



                    <form action="{{ route('copyrights.store') }}" data-parsley-validate="" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('gov') ? 'has-error' : '' }}">
                                    <label for="">Govt. Fee * :</label>
                                    <input type="text" class="form-control" name="gov">
                                    <span class="text-danger">{{ $errors->first('gov') }}</span>
                                </div>
                            </div>

                            <div class="col-md-6 {{ $errors->has('dairy') ? 'has-error' : '' }}">
                                <div class="form-group">
                                    <label>Diary Number :</label>
                                    <input type="text" name="dairy" class="form-control" value="{{ old('dairy') }}" required="">
                                    <span class="text-danger">{{ $errors->first('dairy') }}</span>
                                </div>
                            </div>
                            <div class="col-md-6 {{ $errors->has('dof') ? 'has-error' : '' }}">
                                <div class="form-group">
                                    <label for="">Date of Filing * :</label>
                                    <input type="text" class="form-control" name="dof" id="datepicker" value="{{ old('dof') }}" required="">
                                    <span class="text-danger">{{ $errors->first('dof') }}</span>
                                </div>
                            </div>
                            <div class="col-md-6 {{ $errors->has('user') ? 'has-error' : '' }}">
                                <div class="form-group">
                                    <label for="">User* :</label>
                                    <div class="input-group">
                                        <select class="form-control" name="user" required="">
                                            <option value="">Select</option>
                                            @foreach($users as $user)
                                                <option value="{{ $user->id }}">{{ $user->first_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <span class="text-danger">{{ $errors->first('user') }}</span>
                                </div>
                            </div>
                            <div class="col-md-6 {{ $errors->has('noap') ? 'has-error' : '' }}">
                                <div class="form-group">
                                    <label>Name of the Applicant * :</label>
                                    <input type="text" name="noap" class="form-control" value="{{ old('noap') }}" required="">
                                </div>
                                <span class="text-danger">{{ $errors->first('noap') }}</span>
                            </div>
                            <div class="col-md-6 {{ $errors->has('addrapp') ? 'has-error' : '' }}">
                                <div class="form-group">
                                    <label>Address of the Applicant * :</label>
                                    <input type="text" name="addrapp" class="form-control" value="{{ old('addrapp') }}" required="">
                                </div>
                                <span class="text-danger">{{ $errors->first('addrapp') }}</span>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Application filed by</label>
                                    <select name="app_filed" class="form-control" id="appf_filed">
                                        <option value="">Select</option>
                                        <option value="Agent/Attorney">Agent/Attorney</option>
                                        <option value="Applicant">Applicant</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12" >
                                <div class="row" id="agent_attorney" style="display: none">
                                    <div class="col-md-6 form-group">
                                        <label for="">Name of the Agent/Attorney</label>
                                        <select name="agent_id" id="agent_id" class="form-control">
                                            <option value="">Select</option>
                                            @foreach ($agents as $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="">Agent/Attorney code</label>
                                        <input type="text" name="agent_code" id="agent_code" class="form-control">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="">upload POA</label>
                                        <input type="file" name="poa" class="form-control">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="">Address of the agent/attorney</label>
                                        <input type="text" name="agent_address" id="agent_address" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 {{ $errors->has('title') ? 'has-error' : '' }}">
                                <div class="form-group">
                                    <label>Class of Work * :</label>
                                    <input type="text" name="title" class="form-control" value="{{ old('title') }}" required="">
                                </div>
                                <span class="text-danger">{{ $errors->first('title') }}</span>
                            </div>
                            <div class="col-md-6 {{ $errors->has('work_title') ? 'has-error' : '' }}">
                                <div class="form-group">
                                    <label>Work title * :</label>
                                    <input type="text" name="work_title" class="form-control" value="{{ old('work_title') }}" required="">
                                </div>
                                <span class="text-danger">{{ $errors->first('work_title') }}</span>
                            </div>
                            <div class="col-md-6 {{ $errors->has('image') ? 'has-error' : '' }}">
                                <div class="form-group">
                                    <label>Upload work  :</label>
                                    <input type="file" name="image" class="form-control">
                                </div>
                                <span class="text-danger">{{ $errors->first('image') }}</span>
                            </div>
                            <div class="col-md-6 {{ $errors->has('description') ? 'has-error' : '' }}">
                                <div class="form-group">
                                    <label>Description * :</label>
                                    <textarea class="form-control" rows="10" name="description" required=""></textarea>
                                </div>
                                <span class="text-danger">{{ $errors->first('description') }}</span>
                            </div>
                            <div class="col-md-6 {{ $errors->has('lang') ? 'has-error' : '' }}">
                                <div class="form-group">
                                    <label>Language * :</label>
                                    <select class="form-control" name="lang" required="">
                                        <option value="">Select</option>
                                        @foreach($languages as $language)
                                            <option value="{{ $language->id }}">{{ $language->language }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <span class="text-danger">{{ $errors->first('lang') }}</span>
                            </div>
                            <div class="col-md-6 {{ $errors->has('remark') ? 'has-error' : '' }}">
                                <div class="form-group">
                                    <label>Remarks :</label>
                                    <input type="text" name="remark" class="form-control" value="{{ old('remark') }}">
                                </div>
                                <span class="text-danger">{{ $errors->first('remark') }}</span>
                            </div>
                            <div class="col-md-6 {{ $errors->has('affid') ? 'has-error' : '' }}">
                                <div class="form-group">
                                    <label>User Affidavit :</label>
                                    <input type="file" name="affid" class="form-control">
                                </div>
                                <span class="text-danger">{{ $errors->first('affid') }}</span>
                            </div>

                            <div class="col-md-6 {{ $errors->has('coppdf') ? 'has-error' : '' }}">
                                <div class="form-group">
                                    <label>Payment Details(Pdf) :</label>
                                    <input type="file" name="coppdf" class="form-control">
                                </div>
                                <span class="text-danger">{{ $errors->first('coppdf') }}</span>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <input type="submit" name="draft" id="" value="Save Draft" class="btn btn-info">
                                    <input type="submit" name="submit" value="Submit" class="btn btn-success">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://programmingpot.com/demo/assets/js/bootstrap.min.js"></script>
    <script>
        $(function () {
        //Date picker
            $('#datepicker').datepicker({
                format: 'dd/mm/yy',
                autoclose: true,
                todayHighlight: true
            }).datepicker("setDate", new Date());

            $('#Todatepicker').datepicker({
                format: 'dd/mm/yy',
                autoclose: true,
                todayHighlight: true
            }).datepicker("setDate", new Date());

            $('#reservationtime').datepicker({
                format: 'dd/mm/yy',
                autoclose: true,
                todayHighlight: true
            }).datepicker("setDate", new Date());

            $('#priority_since').datepicker({
                format: 'dd/mm/yy',
                autoclose: true,
                todayHighlight: true
            }).datepicker("setDate", new Date());
        });
    </script>
    <script>
        $(document).ready(function(){
            $('#appf_filed').on('change',function(){
                var appf_filed = $(this).val();
                if(appf_filed == 'Agent/Attorney'){
                    $('#agent_attorney').show();
                }
                if(appf_filed != 'Agent/Attorney'){
                    $('#agent_attorney').hide();
                }
            });

            $('#agent_id').on('change',function(){
                var agent_id = $(this).val();
                $('#agent_code').attr('value','');
                $('#agent_address').attr('value','');
                $.ajax({
                    type:'get',
                    url:'{!! URL::to('findagent') !!}',
                    data:{'id':agent_id},
                    dataType:'json',
                    success:function(data){
                      $('#agent_code').attr('value',data.registration_no);
                      $('#agent_address').attr('value',data.address);
                  },
                });
            });
        });
    </script>
@include('include.footer')
