@include('include.header')
   <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
              <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Class of Good or Services | <a href="{{action('AddTradmarkController@ClassGoodServicesShow')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                                <li class="breadcrumb-item active">Class of Good or Services</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->

                <div class="row">
                <div class="col-md-12">
                  <div class="card">
                  <div class="card-body">
                     <form action="{{url('/class_good_services/update')}}" method="POST" accept-charset="utf-8">
            @csrf
            <input type="hidden" name="id" value="{{$classgoodservices->id}}">
              <div class="form-group {{ $errors->has('class_of_good_services') ? 'has-error' : '' }}">
              <label for="">Class of Good or Services</label>
              <input type="text" class="form-control" name="class_of_good_services" value="{{ $classgoodservices->class_of_good_services }}">
              <span class="text-danger">{{ $errors->first('class_of_good_services') }}</span>
            </div>
                      <div class="col-md-12 center"><button type="Submit" class="btn btn-success">Submit</button></div>
                    </form>
                  </div>
                </div>
                </div>

                </div>

               
    

                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
  @include('include.footer')