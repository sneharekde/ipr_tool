@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
   <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
              <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Add Entity Mapping | <a href="{{action('AddTradmarkController@entityMapping')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Add Entity Mapping</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
            <form action="{{url('entity_mapping/save')}}" method="POST" accept-charset="utf-8">
              @csrf
               <div class="card">
                    <div class="card-body">
                <div class="row">
                       <div class="col-md-4">
            <div class="form-group {{ $errors->has('entity') ? 'has-error' : '' }}">
              <label for="">Entity</label>
              <select name="entity" class="form-control select2">
                    <option value="0">--Select--</option>
                    @foreach($parent_entityes as $entity)
                    <option value="{{$entity->entity_child}}">{{$entity->entity_child}}</option>
                    @endforeach
                  </select>
                  <span class="text-danger">{{ $errors->first('entity') }}</span>
            </div>
            </div>
            <!-- /.box-body -->

          <div class="col-md-4">
            <div class="form-group {{ $errors->has('unit') ? 'has-error' : '' }}">
              <label for="">Unit</label>
              <select name="unit" class="form-control select2">
                    <option value="0">--Select--</option>
                    @foreach($units as $unit)
                    <option value="{{$unit->unit}}">{{$unit->unit}}</option>
                    @endforeach
                  </select>
                  <span class="text-danger">{{ $errors->first('unit') }}</span>
            </div>

            <!-- /.box-body -->
          </div>
          <div class="col-md-4">
            <div class="form-group {{ $errors->has('function') ? 'has-error' : '' }}">
              <label for="">Function</label>
              <select name="function[]" class="form-control select2" multiple>
                    <option value="NA">--Select--</option>
                    @foreach($functions as $function)
                    <option value="{{$function->function}}">{{$function->function}}</option>
                    @endforeach
                  </select>
                  <span class="text-danger">{{ $errors->first('function') }}</span>
            </div>
            <!-- /.box-body -->
          </div>
           <div class="col-md-12">
              <div class="form-group text-center">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
            </div>

                </div>
                    </div>
                  </div>

              </form>





            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
  @include('include.footer')
