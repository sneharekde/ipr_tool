@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
        table thead tr th{
            border: 1px solid black;
            padding: 5px;
        }
        table tbody tr td{
            border: 1px solid black;
            padding: 10px;
        }
        table
        {
            margin-bottom: 20px;
        }
    </style>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">TM Portfolio | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <a href="{{ url('/draft') }}" class="btn btn-primary"><i class="fa fa-upload"></i> Draft</a>
                        <a  href="{{action('FrontEndController@add_tradmark')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Add Trademark</a>
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="col-md-12">
                <div class="card">
                <form action="{{url('/tm_portfolio')}}" method="POST" accept-charset="utf-8">
                    @csrf
                    <div class="form-body">
                    <div class="card-body">
                        <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                            <label>Nature Of The Application :</label>
                            <select name="natapl" class="form-control">
                                <option value="" >--Select--</option>
                                @foreach($natapls as $app)
                                <option value="{{$app->natapl->id}}">{{$app->natapl->nature_of_application}}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label>Nature of Entity :</label>
                            <select name="nate" class="form-control">
                                <option value="" >--Select--</option>
                                @foreach($nates as $natureapplicant)
                                @if($natureapplicant->nate_id !='')
                                <option value="{{$natureapplicant->nate->id}}">{{$natureapplicant->nate->nature_of_applicant}}</option>
                                @endif
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label>Nature of the Applicant :</label>
                            <select name="appf" class="form-control">
                                <option value="" >--Select--</option>
                                @foreach($nateps as $application)
                                <option value="{{$application->appf->id}}">{{$application->appf->application_field}}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label>Category of mark :</label>
                            <select name="catmak" class="form-control">
                                <option value="" >--Select--</option>
                                @foreach($catmaks as $categorymark)
                                <option value="{{$categorymark->catmak->id}}">{{$categorymark->catmak->category_of_mark}}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label>Applicant Name :</label>
                            <select name="applname" class="form-control">
                                <option value="" >--Select--</option>
                                @foreach($applcs as $applicant)
                                @if($applicant->name_in !='')
                                <option value="{{$applicant->name_in}}">{{$applicant->name_in}}</option>
                                @endif
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label>Agent Name :</label>
                            <select name="agant" class="form-control">
                                <option value="">--Select--</option>
                                @foreach($agents as $agent)
                                @if($agent->agent_id !='')
                                <option value="{{$agent->agent->id}}">{{$agent->agent->name}}</option>
                                @endif
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label>Entity :</label>
                            <select name="ent" class="form-control">
                                <option value="" >--Select--</option>

                                @foreach($ents as $entity)
                                @if($entity->ent_id !='')
                                <option value="{{$entity->ent->id}}">{{$entity->ent->name}}</option>
                                @endif
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label>Status :</label>
                            <select name="status" class="form-control" >
                                <option value="" >--Select--</option>
                                @foreach($trademark_statuss as $tradmark_status)
                                <option value="{{$tradmark_status->status}}">{{$tradmark_status->status}}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                        {{--  <div class="col-md-4">
                            <div class="form-group">
                            <label>Class :</label>
                            <select name="class" class="form-control">
                                <option value="" >--Select--</option>
                                @foreach ($classes as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>  --}}
                        <div class="col-md-4">
                            <div class="form-group">
                            <label>Agent / Attorney Code :</label>
                            <select name="agec" class="form-control">
                                <option value="" >--Select--</option>
                                @foreach($ages as $age)
                                @if ($age->agent_code !="")
                                <option value="{{$age->agent_code}}">{{$age->agent_code}}</option>
                                @endif
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label>From Date :</label>
                            <div class="input-group">
                                <input type="date" name="from" class="form-control">
                                <div class="input-group-append">
                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label>To Date :</label>
                            <div class="input-group">
                                <input type="date" name="to" class="form-control">
                                <div class="input-group-append">
                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group text-center">
                        <button type="submit" class="btn btn-success">Search</button>
                        </div>
                    </div>
                    </div>
                </form>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                    <p id="demo" align="center"></p>
                    <table id="myTable" class="table-responsive-sm">
                        <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Trademark</th>
                            <th>Logo|Device</th>
                            <th>Entity</th>
                            <th>Application Number</th>
                            <th>Applicant Date</th>
                            <th>Nature Of <br>The Application</th>
                            <th>Application Field</th>
                            <th>Classes</th>
                            <th>Category <br> Of Mark</th>
                            <th>Applicant Agent</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(isset($results))
                                @foreach ($results as $item)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td><img src="{{asset('public/trademark/'.$result->logo)}}" alt="" height="50px"></td>
                                    <td>
                                    @if($result->ent_id != '')
                                    {{ $result->ent->name }}
                                    @endif
                                    </td>
                                    <td>{{$result->application_no}}</td>
                                    <td>{{date('d-m-Y',strtotime($result->app_date))}}</td>
                                    <td>{{$result->natapl->nature_of_application}}</td>
                                    <td>{{$result->appf->application_field}}</td>
                                    <td>
                                    @foreach ($result->trade_class as $item)
                                        <span>{{ $item->name }}</span>
                                    @endforeach
                                    </td>
                                    <td>{{$result->catmak->category_of_mark}}</td>
                                    <td>
                                    @if($result->agent_id !='')
                                    {{$result->agent->name}}
                                    @endif
                                    </td>
                                    <td>{{$result->status}}</td>
                                    <td>
                                    <a href="{{url('edit_summary/'.$result->id)}}" type="edit" class="btn btn-warning">Edit</a><br><br>
                                    <a href="{{url('/add_tradmark/'.$result->id.'/update_status')}}" class="btn btn-info">Update</a><br><br>
                                    <button type="delete" class="btn btn-danger" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter{{$result->id}}">Disable</button>

                                    <div class="modal fade" id="exampleModalCenter{{$result->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                            Are you sure you want to disable this {{$result->trademark}} Trademark.
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <form action="{{url('update/status')}}" method="POST" accept-charset="utf-8">
                                            @csrf
                                                <input type="hidden" name="id" value="{{$result->id}}">
                                                <input type="hidden" name="status" value="1">
                                                <button type="delete" class="btn btn-danger">Confirm</button>
                                            </form>
                                            </div>
                                        </div>
                                        </div>
                                    </div><br><br>
                                    <a href="{{url('/tradmark/'.$result->id.'/show')}}" class="btn btn-primary">View</a><br><br>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                            @foreach($showes as $show)
                            @if($show->app_status == 0)
                                <tr>
                                <td>{{$no++}}</td>
                                <td>{{ $show->trademark }}</td>
                                <td><img src="{{asset('trademark/'.$show->logo)}}" alt="" style="width: 100px;"></td>
                                <td>
                                    @if($show->ent_id != '')
                                    {{ $show->ent->name }}
                                    @endif
                                </td>
                                <td>{{$show->application_no}}</td>
                                <td>{{date('d-m-Y',strtotime($show->app_date))}}</td>
                                <td>{{$show->natapl->nature_of_application}}</td>
                                <td>{{$show->appf->application_field}}</td>
                                <td>
                                    @foreach ($show->trade_class as $item)
                                    <span class="label label-success" >{{ $item->name }}</span>
                                    @endforeach
                                </td>
                                <td>{{$show->catmak->category_of_mark}}</td>
                                <td>
                                    @if($show->agent_id !='')
                                    {{$show->agent->name}}
                                    @endif
                                </td>
                                <td>
                                    @if ($show->status == 'Active Application')
                                    {{ $show->status.' -> '.$show->sub_status }}
                                    @else
                                    {{$show->status}}
                                    @endif
                                    </td>
                                <td>
                                    <a href="{{url('edit_summary/'.$show->id)}}" type="edit" class="btn btn-warning">Edit</a><br><br>
                                    <a href="{{url('/add_tradmark/'.$show->id.'/update_status')}}" class="btn btn-info">Update</a><br><br>
                                    <button type="delete" class="btn btn-danger" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter{{$show->id}}">Disable</button>

                                    <div class="modal fade" id="exampleModalCenter{{$show->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                        <div class="modal-body">
                                            Are you sure you want to disable this {{$show->trademark}} Trademark.
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <form action="{{url('update/status')}}" method="POST" accept-charset="utf-8">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$show->id}}">
                                            <input type="hidden" name="status" value="1">
                                            <button type="delete" class="btn btn-danger">Confirm</button>
                                            </form>
                                        </div>
                                        </div>
                                    </div>
                                    </div><br><br>
                                    <a href="{{url('/tradmark/'.$show->id.'/show')}}" class="btn btn-primary">View</a><br><br>
                                </td>
                                </tr>
                            @endif
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
  <script type="text/javascript">
    var x = document.getElementById("myTable").rows.length - 1;
    document.getElementById("demo").innerHTML = "Total Records are " + x;
  </script>
@include('include.footer')
