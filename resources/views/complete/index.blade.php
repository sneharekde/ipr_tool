@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }

    form .error {
        color: #ff0000;
        }
</style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Update | <a href="{{ route('summary.index') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('/summary/'.$summ->id) }}">Litigation Details</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link"  href="{{ url('/summary/hearing/'.$summ->id) }}">Next Hearing and Stage</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('/summary/documents/'.$summ->id) }}">Documents</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/summary/fees/'.$summ->id) }}">Fees</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ url('/summary/completion/'.$summ->id) }}">Completion</a>
                        </li>
                    </ul>
                    @if($summ->status == 'Pending')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="">
                                    <div class="">
                                        {!! Form::open(['route' => 'completion.store','data-parsley-validate' => '','method' => 'POST','id' => 'forms','files' => true]) !!}
                                            <div class="col-lg-12 col-md-12 form-group">
                                                <input type="hidden" name="liti_id" value="{{ $summ->id }}">
                                                {{ Form::label('status','Result:') }}
                                                <select name="status" class="form-control" id="select" required>
                                                    <option value="">Select</option>
                                                    <option value="Against">Against</option>
                                                    <option value="In Favour">In Favour</option>
                                                    <option value="Settled">Settled</option>
                                                    <option value="WithDrawn">WithDrawn</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-12 col-md-12 form-group">
                                                {{ Form::label('dis_date','Disposal Date *:') }}
                                                {{ Form::text('dis_date',null,['class' => 'form-control','required' => '','id' => 'disd']) }}
                                            </div>
                                            <div id="other">
                                                <div class="col-lg-12 col-md-12 form-group">
                                                    {{ Form::label('synop','Synopsis of the order :*') }}
                                                    {{ Form::textarea('synop',null,['class' => 'form-control']) }}
                                                </div>
                                                <div class="col-lg-12 col-md-12 form-group">
                                                    {{ Form::label('court','Court/Tribunal : *') }}
                                                    {{ Form::select('court',$cour,null,['class' => 'form-control','placeholder' => 'Select']) }}
                                                </div>
                                            </div>
                                            <div id="disp">
                                                <div class="col-lg-12 col-md-12">
                                                    {{ Form::label('appel','Whether want to filing Appeal:') }}
                                                    <div class="">
                                                        <input type="radio" name="filing" id="yes" value="yes"  onClick="getResults()">Yes
                                                        <input type="radio" name="filing" id="no"  value="no" checked>No
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 form-group" id="text">
                                                    {{ Form::label('last_date','Last Date for Fillig Appeal:') }}
                                                    {{ Form::text('last_date',null,['class' => 'form-control','id' => 'ldd']) }}
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 form-group">
                                                {{ Form::label('comments','Comments *:') }}
                                                {{ Form::textarea('comments',null,['class' => 'form-control','required' => '']) }}
                                            </div>
                                            <div class="col-lg-12 col-md-12 form-group">
                                                {{ Form::label('doc','Upload File:') }}
                                                {{ Form::file('doc',['class' => 'form-control']) }}
                                            </div>
                                            <div class="col-md-12 text-center">
                                                {{ Form::submit('Submit',['class' => 'btn btn-success']) }}
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <table class="table table-striped table-bordered" style="margin-top: 10px">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Litigation Result</th>
                                    <th>Disposal Date</th>
                                    <th>Synopsis of Order</th>
                                    <th>Court/Tribunal</th>
                                    <th>Last Date for Filling Appeal</th>
                                    <th>Document</th>
                                    {{--  <th>Action</th>  --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($comps as $cop)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $cop->status }}</td>
                                    <td>{{ date('d-m-Y',strtotime($cop->dis_date)) }}</td>
                                    <td>
                                        @if ($cop->synop !='')
                                            {{ $cop->synop }}
                                        @else
                                            NA
                                        @endif

                                    </td>
                                    <td>
                                        @if ($cop->court !='')
                                            {{ $cop->court }}
                                        @else
                                            NA
                                        @endif
                                    </td>
                                    <td>
                                        @if($cop->last_date != '')
                                            {{ date('d-m-Y',strtotime($cop->last_date)) }}

                                        @else

                                        NA
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('public/litigation/summary/update',$cop->doc) }}" download="">{{ $cop->doc }}</a>
                                    </td>
                                    <td>
                                        {{--  <a href="{{ url('edit-completion',$cop->id) }}" class="btn btn-sm btn-success">Edit</a>  --}}
                                        {{--  <a href="{{ url('delete-completion',$cop->id) }}" class="btn btn-sm btn-danger">Delete</a>  --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
    <script>
        $(document).ready(function () {
            $("#text").hide();
            $("#yes").click(function () {
                $("#text").show();
            });
            $("#no").click(function () {
                $("#text").hide();
            });
        });
        $(function(){
            $('#disp').hide();
            $('#select').change(function(){
                    if($('#select').val() == 'Against'){
                        $('#disp').show();
                        $('#other').hide();
                    }else{
                        $('#disp').hide();
                        $('#other').show();
                    }
                });
        });
    </script>
    <script>
        $(function() {
            $('#disd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });
            $('#ldd').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });
        });
    </script>
