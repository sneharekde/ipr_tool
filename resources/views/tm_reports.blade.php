@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Trademark Summary Reports | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active">Trademark Summary Reports</li>
                        </ol>

                    </div>
                </div>
            </div>



        <div class="col-lg-12">
            <div class="card">
                <form action="{{url('/tm_reports')}}" method="POST" accept-charset="utf-8">
                @csrf
                    <div class="form-body">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Form :</label>
                                        <div class="input-group">
                                            <input type="date" name="from" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>To :</label>
                                        <div class="input-group">
                                            <input type="date" name="to" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Application Field :</label>
                                        <select name="appfss" class="form-control">
                                            <option value="">--Select--</option>
                                            @foreach($appfs as $application)
                                                <option value="{{$application->appf_id}}">{{$application->appf->application_field}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status :</label>
                                        <select name="status" class="form-control" >
                                            <option value="">--Select--</option>
                                            @foreach($statuss as $tradmark_status)
                                                <option value="{{$tradmark_status->status}}">{{$tradmark_status->status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Agent Name :</label>
                                        <select name="agent_name" class="form-control">
                                            <option value="">--Select--</option>
                                            @foreach($agents as $agent)
                                                @if($agent->agent_id != '')
                                                <option value="{{$agent->agent_id}}">{{ $agent->agent->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Applicant Name :</label>
                                        <select name="applcn" class="form-control">
                                            <option value="">--Select--</option>
                                            @foreach($namein as $applicant)
                                            @if($applicant->name_in !='')
                                                <option value="{{$applicant->name_in}}">{{$applicant->name_in}}</option>
                                            @endif
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Application Date :</label>
                                        <div class="input-group">
                                            <input type="date" name="application_date" class="form-control" placeholder="mm/dd/yyyy">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success">Search</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive ">
                        <table id="example2456" class="table table-bordered"  data-page-length='50'>
                            <thead>
                                <tr>
                                    <th>Sr. No</th>
                                    <th>Logo</th>
                                    <th>Entity</th>
                                    <th>Application Number</th>
                                    <th>Applicant Date</th>
                                    <th>Trademark Name</th>
                                    <th>Nature Of <br>The Application</th>
                                    <th>Application Field</th>
                                    <th>Classes</th>
                                    <th>Category <br> Of Mark</th>
                                    <th>Applicant Agent</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($results))
                                    @foreach($results as $result)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td><img src="{{asset('public/trademark/'.$result->logo)}}" alt="" width="25%"></td>
                                            <td>
                                                @if($result->ent_id !='')
                                                    {{ $result->ent->entity_child }}
                                                @endif
                                            </td>
                                            <td>{{$result->app_no}}</td>
                                            <td>{{ date('d-m-Y',strtotime($result->app_date))}}</td>
                                            <td>{{$result->trademark}}</td>
                                            <td>{{$result->natapl->nature_of_application}}</td>
                                            <td>{{$result->appf->application_field}}</td>
                                            <td>
                                                @foreach(explode(',', $result->classg_id) as $info)
                                                    <span>{{ $info }}</span>
                                                @endforeach
                                            </td>
                                            <td>{{$result->catmak->category_of_mark}}</td>
                                            <td>
                                                @if($result->agent_id !='')
                                                    {{ $result->agent->name }}
                                                @endif
                                            </td>
                                            <td>{{$result->status}}</td>
                                        </tr>
                                    @endforeach
                                @else
                                @foreach($showes as $show)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td><img src="{{asset('public/trademark/'.$show->logo)}}" alt="" width="50%"></td>
                                        <td>
                                            @if($show->ent_id !='')
                                            {{ $show->ent->entity_child }}
                                            @endif
                                        </td>
                                        <td>{{$show->app_no}}</td>
                                        <td>{{ date('d-m-Y',strtotime($show->app_date))}}</td>
                                        <td>{{$show->trademark}}</td>
                                        <td>{{$show->natapl->nature_of_application}}</td>
                                        <td>{{$show->appf->application_field}}</td>
                                        <td>
                                            @foreach($show->trade_class as $info)
                                                <span>{{ $info->name }}</span>
                                            @endforeach
                                        </td>
                                        <td>{{$show->catmak->category_of_mark}}</td>
                                        <td>
                                            @if($show->agent_id !='')
                                            {{ $show->agent->name }}
                                            @endif
                                        </td>
                                        <td>{{$show->status}}</td>
                                    </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

    var x = document.getElementById("example2456").rows.length - 1;
    document.getElementById("demo").innerHTML = "Result : Show Total Rows Is " + x;

    </script>

    <script type="text/javascript">
        $(function () {

            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example2456').DataTable({
                dom: 'Bfrtip',
                buttons: [
       {
           extend: 'pdf',
           footer: true,
           title: 'tradmark Reports export',
           orientation: 'landscape',
           pageSize: 'LEGAL',
           exportOptions: {
                columns: [0,2,3,4,5,6,7,8,9,10,11]
            }
       },
       {
           extend: 'excel',
           title: 'tradmark Reports export',
           footer: false,
           exportOptions: {
                columns: [0,2,3,4,5,6,7,8,9,10,11]
            }
       },
       {
        extend: 'print',
        title: 'tradmark Reports export',
        footer: false,
        exportOptions: {
                columns: [0,2,3,4,5,6,7,8,9,10,11]
            }
       }
    ]
    } );
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });
  </script>

  @include('include.footer')
