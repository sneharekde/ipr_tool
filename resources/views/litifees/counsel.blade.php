@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
        .rate{
            background-color: #ecf0f1;
            border: 1px solid #000;
            border-radius: 50px;
            font-size: 18px;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 30px;
            padding: 0 10px;
            margin: 30px 15px 30px 25px;
          }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Update | <a href="{{ url('/summary/fees',$summ->id) }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    @include('include.message')
                    {!! Form::open(['url' => 'counsel_fees/submit','method' => 'POST','data-parsley-validate' => '','files' => true]) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" value="{{ $summ->id }}" name="liti_id">
                            <div class="form-group">
                                {!! Form::label('law_id', 'Law Firm*:') !!}
                                {!! Form::select('law_id', $ar1, null, ['class' => 'form-control','placeholder' => 'Select Law Firm','required' => '']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('counsel_id', 'Counsel Name*:') !!}
                                {!! Form::select('counsel_id', $ar2, null, ['class' => 'form-control','placeholder' => 'Select Counsel Name','required' => '']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('fees_type', 'Type of Fees*:') !!}

                                {!! Form::select('fees_type', ['lump sum' => 'lump sum','appearance' => 'appearance','Hourly' => 'Hourly'], null, ['class' => 'form-control','id' => 'select','required' => '','placeholder' => 'Select Fees Type']) !!}


                            </div>
                        </div>

                            <div class="col-md-12">
                                <div class="form-group" id="dumpp">
                                    {!! Form::label('effect', 'Effective / Non-Effective Fees*:') !!}
                                    {!! Form::select('effect', ['Effective' => 'Effective','Non-Effective' => 'Non-Effective'], null, ['class' => 'form-control','placeholder' => 'Select']) !!}
                                </div>
                            </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('comment', 'Comments:') !!}
                                {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('doc', 'Document:') !!}
                                {!! Form::file('doc', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr><h4 class="text-center">Fees Details</h4><hr>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            {{ Form::label('date','Date:') }}
                            {{ Form::text('date',null,['class' => 'form-control','required' => '','id' => 'dated']) }}
                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            {{ Form::label('fees','Amount Involved:') }}
                            {{ Form::text('fees',null,['class' => 'form-control','id' => 'from_ammount','required' => '']) }}
                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            {{ Form::label('currency','Currency:') }}
                            {{ Form::select('currency',$cur,null,['class' => 'form-control','id' => 'from_currency','placeholder' => 'select','required' => '']) }}
                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            {{-- <button id="exchange">
                              <i class="fas fa-exchange-alt"></i>
                            </button> --}}
                            <div class="rate" id="rate"></div>
                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            {{ Form::label('conv_curr','Converted Currency:') }}
                            {{ Form::select('conv_curr',$cur,null,['class' => 'form-control','id' => 'to_currency','placeholder' => 'select','required' => '']) }}
                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            {{ Form::label('conv_amt','Converted Amount:') }}
                            {{ Form::text('conv_amt',null,['class' => 'form-control','id' => 'to_ammount','readonly' =>'','required' => '']) }}
                          </div>
                          <div class="col-md-12">
                            <div class="form-group text-center">
                              <button type="submit" name="submit" class="btn btn-success">Create</button>
                            </div>
                          </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@include('include.footer')

    <script>
        $(function(){
            $('#dumpp').hide();
            $('#select').change(function(){
                if($('#select').val() == 'appearance'){
                    $('#dumpp').show();
                }else{
                    $('#dumpp').hide();
                }
            });
        });
    </script>
    <script>
        $(function() {
            $('#dated').datepicker({
                changeMonth: true,
                changeYear: true,
                showAnim: 'slideDown',
                maxDate: 0,
                //minDate: +1,
            });
        });
    </script>

