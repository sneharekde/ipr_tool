@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Paid Fees | <a href="{{ url('/summary/fees',$post->liti_id) }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    @include('include.message')
                    <a href="{{ url('advocate/paid_fees',$post->id) }}" class="btn btn-sm btn-primary">Pay Fees</a>
                    <div class="table-responsive" style="margin-top: 10px">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Invoice No</th>
                                    <th>Invoice Date</th>
                                    <th>Comments</th>
                                    <th>Paid Fees</th>
                                    <th>Document</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($fees as $item)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $item->inv_no }}</td>
                                        <td> {{ date('d-m-Y', strtotime($item->inv_date)) }} </td>
                                        <td>{{ $item->comment }}</td>
                                        <td>{{ $item->paid_fees.' '.$item->currency }}</td>
                                        <td><a href="{{ url('public/litigation/fess/'.$item->docs) }}" download="">{{ $item->docs }}</a></td>
                                        <td>
                                            <a href="{{ url('advocate-paid-fees/edit',$item->id) }}" class="btn btn-sm btn-success">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
