@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
        .rate{
            background-color: #ecf0f1;
            border: 1px solid #000;
            border-radius: 50px;
            font-size: 18px;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 30px;
            padding: 0 10px;
            margin: 30px 15px 30px 25px;
          }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Pay Counsel Fees | <a href="{{ url('counsel-paid-fees',$post->counself_id) }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    @include('include.message')
                    {!! Form::model($post,['url' => ['counsel_paid_fees_edit/submit',$post->id],'method' => 'POST','data-parsley-validate' => '','files' => true]) !!}
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('inv_no', 'Invoice No*:') !!}
                                {!! Form::text('inv_no', null, ['class' => 'form-control','required' => '']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('inv_amt', 'Invoice Amount*:') !!}
                                {!! Form::text('inv_amt', null, ['class' => 'form-control','required' => '','data-parsley-type' => 'number']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('inv_curr', 'Invoice Currency*:') !!}
                                {!! Form::select('inv_curr', $cur, null, ['class' => 'form-control','required' => '','placeholder' => 'Select']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('inv_date', 'Invoice Date*:') !!}
                                {!! Form::text('inv_date', null, ['class' => 'form-control','required' => '','id' => 'invd']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('comment', 'Comments:') !!}
                                {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('doc', 'Document:') !!}
                                @if ($post->docs !='')
                                <div class="col-md-4">
                                    <a href="{{ url('public/litigation/fess/'.$post->docs) }}" download="">{{ $post->docs }}</a>
                                    <a href="{{ url('delete/paid_fess/'.$post->docs.'/'.$post->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                </div>
                                @else
                                {!! Form::file('doc', ['class' => 'form-control']) !!}
                                @endif

                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr><h4 class="text-center">Fees Details</h4><hr>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            {{ Form::label('date','Date:') }}
                            {{ Form::text('date',null,['class' => 'form-control','required' => '','id' => 'dated']) }}
                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            {{ Form::label('paid_fees','Paid Fees:') }}
                            {{ Form::text('paid_fees',null,['class' => 'form-control','id' => 'from_ammount','required' => '']) }}
                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            {{ Form::label('currency','Currency:') }}
                            {{ Form::select('currency',$cur,null,['class' => 'form-control','id' => 'from_currency','placeholder' => 'select','required' => '']) }}
                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            {{-- <button id="exchange">
                              <i class="fas fa-exchange-alt"></i>
                            </button> --}}
                            <div class="rate" id="rate"></div>
                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            {{ Form::label('conv_curr','Converted Currency:') }}
                            {{ Form::select('conv_curr',$cur,null,['class' => 'form-control','id' => 'to_currency','placeholder' => 'select','required' => '']) }}
                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            {{ Form::label('conv_amt','Converted Amount:') }}
                            {{ Form::text('conv_amt',null,['class' => 'form-control','id' => 'to_ammount','readonly' =>'','required' => '']) }}
                          </div>
                          <div class="col-md-12">
                            <div class="form-group text-center">
                              <button type="submit" name="submit" class="btn btn-success">Create</button>
                            </div>
                          </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@include('include.footer')

<script>
    $(function() {
        $('#dated').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
            //minDate: +1,
        });
        $('#invd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0,
            //minDate: +1,
        });
    });
</script>
