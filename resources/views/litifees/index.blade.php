@include('include.header')

    <style>
        .card{
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Update | <a href="{{ route('summary.index') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>
            @include('include.message')
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('/summary/'.$summ->id) }}">Litigation Details</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link"  href="{{ url('/summary/hearing/'.$summ->id) }}">Next Hearing and Stage</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('/summary/documents/'.$summ->id) }}">Documents</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ url('/summary/fees/'.$summ->id) }}">Fees</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/summary/completion/'.$summ->id) }}">Completion</a>
                        </li>
                    </ul>
                    <div style="margin-top:10px ">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Counsel Fees</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Advocate Fees</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                @if ($summ->status == 'Pending')
                                <a href="{{ url('counsel-fees/'.$summ->id) }}" class="btn btn-sm btn-primary" style="margin-top: 10px">Add Agreed Fees</a>
                                @endif
                                <table class="table table-bordered table-striped" style="margin-top: 10px">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Counsel Name</th>
                                            <th>Types of Fees</th>
                                            <th>Effective / Non-Effective Fee</th>
                                            <th>Fees</th>
                                            <th>Comments</th>
                                            <th>Document</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($feescs as $item)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $item->counsel->counsel }}</td>
                                                <td>{{ $item->fees_type }}</td>
                                                <td>
                                                    @if ($item->effect !='')
                                                        {{ $item->effect }}
                                                    @else
                                                        {{ 'NA' }}
                                                    @endif
                                                </td>
                                                <td>{{ $item->fees.' '.$item->currency }}</td>
                                                <td>{{ $item->comment }}</td>
                                                <td><a href="{{ url('public/litigation/fess',$item->docs) }}" download="">{{ $item->docs }}</a></td>
                                                <td>
                                                    <a href="{{ url('summary/counsel-fee/edit',$item->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                                    <a href="{{ url('counsel-paid-fees',$item->id) }}" class="btn btn-sm btn-primary">Paid Fees</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                @if ($summ->status == 'Pending')
                                <a href="{{ url('advocate-fees/'.$summ->id) }}" class="btn btn-sm btn-primary" style="margin-top: 10px">Add Agreed Fees</a>
                                @endif

                                <table class="table table-bordered table-striped" style="margin-top: 10px">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Advocate Name</th>
                                            <th>Types of Fees</th>
                                            <th>Effective / Non-Effective Fee</th>
                                            <th>Fees</th>
                                            <th>Comments</th>
                                            <th>Document</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                        @foreach ($feesca as $items)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $items->advoc->advocate }}</td>
                                                <td>{{ $items->fees_type }}</td>
                                                <td>
                                                    @if ($items->effect !='')
                                                        {{ $items->effect }}
                                                    @else
                                                        {{ 'NA' }}
                                                    @endif
                                                </td>
                                                <td>{{ $items->fees.' '.$items->currency }}</td>
                                                <td>{{ $items->comment }}</td>
                                                <td><a href="{{ url('public/litigation/fess',$items->docs) }}" download="">{{ $items->docs }}</a></td>
                                                <td>
                                                    <a href="{{ url('summary/advocate-fee/edit',$items->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                                    <a href="{{ url('advocate-paid-fees',$items->id) }}" class="btn btn-sm btn-primary">Paid Fees</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
