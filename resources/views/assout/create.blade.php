@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Assign Out | <a href="{{ url('assignOut') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>
            <div class="card">
				<div class="card-body">
                    @if(isset($tm))
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Trademark Name</th>
                                    <th>Application Number</th>
                                    <th>Application Date</th>
                                    <th>Status</th>
                                    <th>License In</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{--  @foreach($tm as $t )  --}}
                                <tr>
                                    <td>{{ $tm->trademark }}</td>
                                    <td>{{ $tm->application_no }}</td>
                                    <td>{{ date('d-m-Y',strtotime($tm->app_date)) }}</td>
                                    <td>{{ $tm->status }}</td>
                                    <td><a href="{{ url('trass/out',$tm->id) }}" class="btn btn-success btn-sm">Assign Out</a></td>
                                </tr>
                                {{--  @endforeach  --}}
                            </tbody>
                        </table>
                    @elseif(isset($cr))
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Diary No</th>
                                    <th>Date of Filing</th>
                                    <th>Status</th>
                                    <th>License In</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{--  @foreach($cr as $c )  --}}
                                    <tr>
                                        <td>{{ $cr->titlew }}</td>
                                        <td>{{ $cr->diary }}</td>
                                        <td>{{ date('d-m-Y',strtotime($cr->dof)) }}</td>
                                        <td>{{ $cr->status }}</td>
                                        <td><a href="{{ url('copyright/assign_out',$cr->id) }}" class="btn btn-success btn-sm">Assign Out</a></td>
                                    </tr>
                                {{--  @endforeach  --}}
                            </tbody>
                        </table>
                    @elseif(isset($des))
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name of Article</th>
                                    <th>Application No</th>
                                    <th>Date of Filing</th>
                                    <th>Status</th>
                                    <th>License In</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{--  @foreach($des as $d )  --}}
                                    <tr>
                                        <td>{{ $des->article_name }}</td>
                                        <td>{{ $des->app_no }}</td>
                                        <td>{{ date('d-m-Y',strtotime($des->filing_date)) }}</td>
                                        <td>{{ $des->status }}</td>
                                        <td><a href="{{ url('design/assign_out',$des->id) }}" class="btn btn-success btn=sm">Assign Out</a></td>
                                    </tr>
                                {{--  @endforeach  --}}
                            </tbody>
                        </table>
                    @elseif(isset($pat))
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Title of Invention</th>
                                    <th>Application No</th>
                                    <th>Date of Filing</th>
                                    <th>Status</th>
                                    <th>License In</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{--  @foreach($pat as $p )  --}}
                                    <tr>
                                        <td>{{ $pat->invent_name }}</td>
                                        <td>{{ $pat->application_no }}</td>
                                        <td>{{ date('d-m-Y',strtotime($pat->filing_date)) }}</td>
                                        <td>{{ $pat->status }}</td>
                                        <td><a href="{{ url('/patent/assign_out',$pat->id) }}" class="btn btn-success btn-sm">Assign Out</a></td>
                                    </tr>
                                {{--  @endforeach  --}}
                            </tbody>
                        </table>
					@else
						<p>No Records Found</p>
					@endif
				</div>
			</div>
		</div>
	</div>
@include('include.footer')
