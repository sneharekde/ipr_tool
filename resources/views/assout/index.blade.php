@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Assign Out | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
            </div>
            <div class="card">
            	@include('include.message')
            	<div class="card-body">
            		<div class="form-group col-md-12">
	            		{{ Form::label('module' ,'Select Module') }}
        				<select name="module" class="form-control" id="module">
        					<option value="">Select</option>
        					<option value="TM">TM</option>
        					<option value="CR">CR</option>
        					<option value="Designs">Designs</option>
        					<option value="Patents">Patents</option>
        				</select>
            		</div>
            		{!! Form::open(['route' => 'assignOut.create','method' => 'GET']) !!}
            			<div class="row">
            				<div class="form-group col-md-12" style="display: none;" id="tm_no">
            					{{ Form::label('tm_no','TM Application No.') }}
            					{{ Form::text('tm_no',null,['class' => 'form-control']) }}

            					{{ Form::submit('Submit',['class' => 'btn btn-success']) }}
            				</div>
            			</div>
            		{!! Form::close() !!}
            		{!! Form::open(['url' => 'cr_assout','method' => 'GET']) !!}
            			<div class="row">
            				<div class="form-group col-md-12" style="display: none;" id="dairy_no">
            					{{ Form::label('dairy_no','Dairy No.') }}
            					{{ Form::text('dairy_no',null,['class' => 'form-control']) }}

            					{{ Form::submit('Submit',['class' => 'btn btn-success']) }}
            				</div>
            			</div>
            		{!! Form::close() !!}
            		{!! Form::open(['url' => 'des_assout','method' => 'GET']) !!}
            			<div class="row">
            				<div class="form-group col-md-12" style="display: none;" id="desi_no">
            					{{ Form::label('desi_no','Design Application No.') }}
            					{{ Form::text('desi_no',null,['class' => 'form-control']) }}

            					{{ Form::submit('Submit',['class' => 'btn btn-success']) }}
            				</div>
            			</div>
            		{!! Form::close() !!}
            		{!! Form::open(['url' => 'pat_assout','method' => 'GET']) !!}
            			<div class="row">
            				<div class="form-group col-md-12" style="display: none;" id="pat_no">
            					{{ Form::label('pat_no','Patent Application No.') }}
            					{{ Form::text('pat_no',null,['class' => 'form-control']) }}

            					{{ Form::submit('Submit',['class' => 'btn btn-success']) }}
            				</div>
            			</div>
            		{!! Form::close() !!}
            	</div>
            </div>
		</div>
	</div>
@include('include.footer')
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('change','#module',function(){
            var model = $(this).val();
            if(model == ''){
                $('#tm_no').hide();
				$('#dairy_no').hide();
				$('#desi_no').hide();
				$('#pat_no').hide();
            }
			if (model == 'TM') {
				$('#tm_no').show();
				$('#dairy_no').hide();
				$('#desi_no').hide();
				$('#pat_no').hide();
			}else if(model == 'CR'){
				$('#dairy_no').show();
				$('#tm_no').hide();
				$('#desi_no').hide();
				$('#pat_no').hide();
			}else if(model == 'Designs'){
				$('#desi_no').show();
				$('#tm_no').hide();
				$('#dairy_no').hide();
				$('#pat_no').hide();
			}else if(model == 'Patents'){
				$('#pat_no').show();
				$('#tm_no').hide();
				$('#dairy_no').hide();
				$('#desi_no').hide();
			}

		});
	});
</script>
