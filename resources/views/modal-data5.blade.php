<style>
.dataTables_filter
{
  display: none;
}
.paginate_button
{
  display: none;
}
.dataTables_info
{
  display: none;
}
</style>
<div class="modal  fade bs-example-modal-lg" id="bootstrap-modal4" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Design Sub Status Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow-x: auto;">
        <div id="demo-modal">
          <div class="table-responsive">
            <table id="example27" class="table table-striped table-bordered " >
              <thead>
                <tr>
                  <th>Sr. No</th>
                  <th>Proprieter Code</th>
                  <th>Filing Date</th>
                  <th>Application No.</th>
                  <th>Name of Applicant</th>
                  <th>Entity</th>
                  <th>Name of Article</th>
                  <th>Sub Status</th>
                </tr>
              </thead>
              <tbody>
                @foreach($results as $result)
                  @if($result->sub_status ==  $name)
                    <tr>
                      <td>{{ $no++ }}</td>
                      <td>{{ $result->prop_code  }}</td>
                      <td>{{ date('d-m-Y',strtotime($result->filing_date)) }}</td>
                      <td>{{ $result->app_no }}</td>
                      <td>{{ $result->name_of_applicant }}</td>
                      <td>
                          @if($result->ent_id !='')
                          {{ $result->ent->name }}
                          @endif
                      </td>
                      <td>{{ $result->article_name }}</td>
                      <td>{{ $result->sub_status }}</td>
                    </tr>
                  @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  $(function () {
    // responsive table
    $('#config-table').DataTable({
      responsive: true
    });
    $('#example27').DataTable({
      dom: 'Bfrtip',
      buttons: [
       {
          extend: 'pdf',
          footer: true,
          title: 'Design Sub Status Reports export',
          orientation: 'landscape',
          pageSize: 'LEGAL',
          exportOptions: {
          columns: [0,2,3,4,5,6,7]
        }
      }]
    });
    $('.buttons-pdf').addClass('btn btn-primary mr-1');
  });
</script>
