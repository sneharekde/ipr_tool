@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">States | <a href="{{url('state')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('state.create')}}">Home</a></li>
                            <li class="breadcrumb-item active">State</li>
                        </ol>
                    </div>
                </div>
            </div>
            @include('include.message')
            <div class="row">
            	<div class="col-md-12">
            		<div class="card">
            			<div class="card-body">
                            {!! Form::open(['route' => 'state.store','method' =>'POST' ,'data-parsley-validate' => '']) !!}
                                <div class="form-group">
                                    {!! Form::label('name', 'State:') !!}
                                    {!! Form::text('name', null, ['class' => 'form-control','required' => '']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('country_id', 'Country:') !!}
                                    {!! Form::select('country_id', $ar1,null,['class' => 'form-control','placeholder' =>'Select Country','required' => '']) !!}
                                </div>
                                <div class="form-group text-center">
                                    {!! Form::submit('submit', ['class' => 'btn btn-success']) !!}
                                </div>
                            {!! Form::close() !!}
            			</div>
            		</div>
            	</div>
            </div>
		</div>
	</div>
@include('include.footer')
