<style>
  .dataTables_filter
  {
    display: none;
  }
  .paginate_button
  {
    display: none;
  }
  .dataTables_info
  {
    display: none;
  }
</style>
<div class="modal fade bs-example-modal-lg" id="bootstrap-modals" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">tradmark Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow-x: auto;">
        <div id="demo-modal">
          <div class="table-responsive">
            <table id="example30" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Sr. No</th>
                  <th>Logo</th>
                  <th>Entity</th>
                  <th>Application Number</th>
                  <th>Applicant Date</th>
                  <th>tradmark Name</th>
                  <th>Nature Of <br>The Application</th>
                  <th>Application Field</th>
                  <th>Classes</th>
                  <th>Category <br> Of Mark</th>
                  <th>Sub Status</th>
                </tr>
              </thead>
              <tbody>
                @foreach($results as $result)
                  @if($result->sub_status ==  $name)
                    <tr>
                      <td>{{$no++}}</td>
                      <td><img src="{{asset('public/trademark/'.$result->logo)}}" alt="" height="50px"></td>
                      <td>
                        @if($result->ent_id !='')
                        {{ $result->ent->name }}
                        @endif
                      </td>
                      <td>{{$result->app_no}}</td>
                        <td>{{$result->app_date}}</td>
                        <td>{{$result->trademark}}</td>
                        <td>{{$result->natapl->nature_of_application}}</td>
                        <td>{{$result->appf->application_field}}</td>
                        <td>
                            @foreach ($result->trade_class as $item)
                                <span>{{ $item->name }}</span>
                            @endforeach
                        </td>
                        <td>{{$result->catmak->category_of_mark}}</td>

                        <td>{{$result->sub_status}}</td>
                    </tr>
                  @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  $(function () {
    // responsive table
    $('#config-table').DataTable({
      responsive: true
    });
    $('#example30').DataTable({
      dom: 'Bfrtip',
      buttons: [
       {
          extend: 'pdf',
          footer: true,
          title: 'tradmark Status Reports export',
          orientation: 'landscape',
          pageSize: 'LEGAL',
          exportOptions: {
          columns: [0,2,3,4,5,6,7,8,9,10]
        }
      }]
    });
    $('.buttons-pdf').addClass('btn btn-primary mr-1');
  });
</script>
