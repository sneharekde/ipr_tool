@include('include.header')
<style>
    .card{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
</style>
  <div class="page-wrapper">
    <div class="container-fluid">
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Edit Trademark Status | <a href="{{ url('add_tradmark/'.$stats->trade_id.'/update_status') }}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ action('FrontEndController@index') }}">Home</a></li>
              <li class="breadcrumb-item active">Trademark Status</li>
            </ol>
          </div>
        </div>
      </div>
      @include('include.message')
      <div class="card">
        <div class="card-body" id="app">
          <form action="{{ url('edit/status/submit') }}" method="POST" data-parsley-valiate="" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-lg-12 col-md-12">
                <input type="hidden" name="status_id" value="{{ $stats->id }}">
              <div class="form-group">
                <label>Status</label>
                <select class="form-control" name="status" id="status" required="">
                  <option value="" @if($stats->status == '') selected="selected" @else disabled @endif>Select</option>
                  <option value="Active Application" @if($stats->status == 'Active Application') selected="selected" @else disabled @endif>Active Application</option>
                  <option value="Registered" @if($stats->status == 'Registered') selected="selected" @else disabled @endif>Registered</option>
                  <option value="Abandoned or rejected" @if($stats->status == 'Abandoned or rejected') selected="selected" @else disabled @endif>Abandoned or rejected</option>
                  <option value="Withdrawn" @if($stats->status == 'Withdrawn') selected="selected" @else disabled @endif>Withdrawn</option>
                </select>
              </div>
            </div>
            @if ($stats->sub_status != '')
            <div class="col-lg-12 col-md-12">
                <div class="form-group" id="sub_status">
                  <label>Sub Status</label>
                  <select name="sub_status" class="form-control" id="select1">
                    <option value="" @if($stats->sub_status == '') selected="selected" @else disabled @endif>Select</option>
                    <option value="Application Submitted" disabled>Application Submitted</option>
                    <option value="Sent to Vienna Codification" @if($stats->sub_status == 'Sent to Vienna Codification') selected="selected" @else disabled @endif>Sent to Vienna Codification</option>
                    <option value="Marked for exam" @if($stats->sub_status == 'Marked for exam') selected="selected" @else disabled @endif>Marked for exam</option>
                    <option value="Examination report received" @if($stats->sub_status == 'Examination report received') selected="selected" @else disabled @endif>Examination report received</option>
                    <option value="Replied to examination report" @if($stats->sub_status == 'Replied to examination report') selected="selected" @else disabled @endif>Replied to examination report</option>
                    <option value="Hearing for objection" @if($stats->sub_status == 'Hearing for objection') selected="selected" @else disabled @endif>Hearing for objection</option>
                    <option value="Accepted" @if($stats->sub_status == 'Accepted') selected="selected" @else disabled @endif>Accepted</option>
                    <option value="Accepted and advertised" @if($stats->sub_status == 'Accepted and advertised') selected="selected" @else disabled @endif>Accepted and advertised</option>
                    <option value="Opposition noticed received" @if($stats->sub_status == 'Opposition noticed received') selected="selected" @else disabled @endif>Opposition noticed received</option>
                    <option value="Counter statment submitted" @if($stats->sub_status == 'Counter statment submitted') selected="selected" @else disabled @endif>Counter statment submitted</option>
                    <option value="Evidence and hearing" @if($stats->sub_status == 'Evidence and hearing') selected="selected" @else disabled @endif>Evidence and hearing</option>
                    <option value="Decision pending" @if($stats->sub_status == 'Decision pending') selected="selected" @else disabled @endif>Decision pending</option>
                  </select>
                </div>
              </div>
            @endif

          </div>
          {{-- Status Wise Field Show --}}
          @if ($stats->status == 'Registered')
          <div id="reg">
            <div class="form-group col-md-12">
              <label>Date of Registration:</label>
              <input type="text" id="regd" name="dor" class="form-control" value="{{ $stats->dor_date }}">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              @if ($stats->docs !='')
                <div class="col-md-3">
                    <img src="{{ url('public/trademark/'.$stats->docs) }}" alt="" class="img-responsive">
                    <a href="{{ url('images/delete/'.$stats->docs.'/'.$stats->id) }}" class="btn btn-sm btn-danger">Delete</a>
                </div>
                @else
                <input type="file" name="docs7" class="form-control">
                @endif

            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm9" class="form-control" rows="5">{{ $stats->comments }}</textarea>
            </div>
          </div>
          @endif
          @if ($stats->status == 'Abandoned or rejected')
          <div id="aban">
            <div class="form-group col-md-12">
              <label>Date:</label>
              <input type="text" id="abad" name="date3" class="form-control" value="{{ $stats->date }}">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              @if ($stats->docs !='')
                <div class="col-md-3">
                    <img src="{{ url('public/trademark/'.$stats->docs) }}" alt="" class="img-responsive">
                    <a href="{{ url('images/delete/'.$stats->docs.'/'.$stats->id) }}" class="btn btn-sm btn-danger">Delete</a>
                </div>
                @else
                <input type="file" name="docs8" class="form-control">
                @endif

            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm10" class="form-control" rows="5">{{ $stats->comments }}</textarea>
            </div>
          </div>
          @endif
          @if ($stats->status == 'Withdrawn')
          <div id="witd">
            <div class="form-group col-md-12">
              <label>Date:</label>
              <input type="text" id="withd" name="date4" class="form-control" value="{{ $stats->date }}">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              @if ($stats->docs !='')
                <div class="col-md-3">
                    <img src="{{ url('public/trademark/'.$stats->docs) }}" alt="" class="img-responsive">
                    <a href="{{ url('images/delete/'.$stats->docs.'/'.$stats->id) }}" class="btn btn-sm btn-danger">Delete</a>
                </div>
                @else
                <input type="file" name="docs9" class="form-control">
                @endif

            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm11" class="form-control" rows="5">{{ $stats->comments }}</textarea>
            </div>
          </div>
          @endif
          {{-- Sub Status Wise Show --}}
          @if ($stats->status == 'Active Application' && $stats->sub_status == 'Marked for exam')
          <div id="mfe">
            <div class="form-group col-md-12">
              <label>Date:</label>
              <input type="text" id="mfed" name="date1" value="{{ $stats->date }}" class="form-control">
            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm1" class="form-control" rows="5">{{ $stats->comments }}</textarea>
            </div>
          </div>
          @endif
          @if ($stats->status == 'Active Application' && $stats->sub_status == 'Examination report received')
          <div id="errs">
            <div class="form-group col-md-12">
              <label>Date of Receipt of ER:</label>
              <input type="text" id="errd" name="rdate" class="form-control" value="{{ $stats->rec_date }}">
            </div>
            <div class="form-group col-md-12">
                <label for="">Deadline for Reply:</label>
                <input type="text" name="edeadline" id="dropdate" class="form-control" value="{{ $stats->rep_dead }}">
            </div>
            <div class="form-group col-md-12">
                <label>Document:</label>
                @if ($stats->docs !='')
                <div class="col-md-3">
                    <img src="{{ url('public/trademark/'.$stats->docs) }}" alt="" class="img-responsive">
                    <a href="{{ url('images/delete/'.$stats->docs.'/'.$stats->id) }}" class="btn btn-sm btn-danger">Delete</a>
                </div>
                @else
                <input type="file" name="docs1" class="form-control">
                @endif
              </div>

            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm2" class="form-control" rows="5">{{ $stats->comments }}</textarea>
            </div>
            <div class="form-group col-md-12">
              <label>Reminder1:</label>
              <input type="text" id="remd1" name="rem11" class="form-control" value="{{ $stats->rem1 }}">
            </div>
            <div class="form-group col-md-12">
              <label>Reminder2:</label>
              <input type="text" id="remd2" name="rem21" class="form-control" value="{{ $stats->rem2 }}">
            </div>
            <div class="form-group col-md-12">
              <label>Reminder3:</label>
              <input type="text" id="remd3" name="rem31" class="form-control" value="{{ $stats->rem3 }}">
            </div>
          </div>
          @endif
          @if ($stats->status == 'Active Application' && $stats->sub_status == 'Replied to examination report')
          <div id="rter">
            <div class="form-group col-md-12">
              <label>Replied Date:</label>
              <input type="text" id="rtod" name="rpdate" class="form-control" value="{{ $stats->rep_date }}">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              @if ($stats->docs !='')
                <div class="col-md-3">
                    <img src="{{ url('public/trademark/'.$stats->docs) }}" alt="" class="img-responsive">
                    <a href="{{ url('images/delete/'.$stats->docs.'/'.$stats->id) }}" class="btn btn-sm btn-danger">Delete</a>
                </div>
                @else
                <input type="file" name="docs2" class="form-control">
                @endif

            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm3" class="form-control" rows="5">{{ $stats->comments }}</textarea>
            </div>
          </div>
          @endif
          @if ($stats->status == 'Active Application' && $stats->sub_status == 'Hearing for objection')
          <div id="hfob">
            <div class="form-group col-md-12">
              <label>Date for Hearing:</label>
              <input type="text" id="herds" name="dfh" class="form-control" value="{{ $stats->her_date }}">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              @if ($stats->docs !='')
                @foreach (explode(",",$stats->docs) as $item)
                <div class="col-md-3">
                    <a href="{{ url('public/trademark/'.$item) }}" download="">{{ $item }}</a>
                    <a href="{{ url('imagesh/delete/'.$item.'/'.$stats->id) }}" class="btn btn-sm btn-danger">Delete</a>
                </div>
                @endforeach
              @else
              <input type="file" name="docss[]" class="form-control" multiple>
              @endif

            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm4" class="form-control" rows="5">{{ $stats->comments }}</textarea>
            </div>
            <div class="form-group col-md-12">
              <label>Reminder1:</label>
              <input type="text" id="herd1" name="rem12" class="form-control" value="{{ $stats->rem1 }}">
            </div>
            <div class="form-group col-md-12">
              <label>Reminder2:</label>
              <input type="text" id="herd2" name="rem22" class="form-control" value="{{ $stats->rem2 }}">
            </div>
            <div class="form-group col-md-12">
              <label>Reminder2:</label>
              <input type="text" id="herd3" name="rem32" class="form-control" value="{{ $stats->rem3 }}">
            </div>
          </div>
          @endif
          @if ($stats->status == 'Active Application' && $stats->sub_status == 'Accepted and advertised')
          <div id="acads">
            <div class="form-group col-md-12">
              <label>Date of Advertisement:</label>
              <input type="text" id="adad" name="doa" class="form-control" value="{{ $stats->adv_date }}">
            </div>
            <div class="form-group col-md-12">
                <label>Journal number:</label>
                <input type="text" name="jno" class="form-control" value="{{ $stats->jno }}">
              </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              @if ($stats->docs !='')
                <div class="col-md-3">
                    <img src="{{ url('public/trademark/'.$stats->docs) }}" alt="" class="img-responsive">
                    <a href="{{ url('images/delete/'.$stats->docs.'/'.$stats->id) }}" class="btn btn-sm btn-danger">Delete</a>
                </div>
                @else
                <input type="file" name="docs3" class="form-control">
                @endif

            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm5" class="form-control" rows="5">{{ $stats->comments }}</textarea>
            </div>
          </div>
          @endif
          @if ($stats->status == 'Active Application' && $stats->sub_status == 'Opposition noticed received')
          <div id="opnor">
            <div class="form-group col-md-12">
              <label>Date of Notice:</label>
              <input type="text" id="opon" name="nod" class="form-control" value="{{ $stats->not_date }}">
            </div>
            <div class="form-group col-md-12">
              <label>Date of Receipt of Notice:</label>
              <input type="text" id="oponr" name="recd" class="form-control" value="{{ $stats->reo_date }}">
            </div>
            <div class="col-md-12 form-group">
                <label for="">Deadline for Counterstatement</label>
                <input type="text" name="dfcstate" id="dfcstate" class="form-control" value="{{ $stats->cou_dead }}">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              @if ($stats->docs !='')
                <div class="col-md-3">
                    <img src="{{ url('public/trademark/'.$stats->docs) }}" alt="" class="img-responsive">
                    <a href="{{ url('images/delete/'.$stats->docs.'/'.$stats->id) }}" class="btn btn-sm btn-danger">Delete</a>
                </div>
                @else
                <input type="file" name="docs4" class="form-control">
                @endif

            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm6" class="form-control" rows="5">{{ $stats->comments }}</textarea>
            </div>
            <div class="form-group col-md-12">
                <label>Reminder1:</label>
                <input type="text" id="evdd1s" name="rem13s" class="form-control" value="{{ $stats->rem1 }}">
              </div>
              <div class="form-group col-md-12">
                <label>Reminder2:</label>
                <input type="text" id="evdd2s" name="rem23s" class="form-control" value="{{ $stats->rem2 }}">
              </div>
              <div class="form-group col-md-12">
                <label>Reminder3:</label>
                <input type="text" id="evdd3s" name="rem33s" class="form-control" value="{{ $stats->rem3 }}">
              </div>
          </div>
          @endif
          @if ($stats->status == 'Active Application' && $stats->sub_status == 'Counter statment submitted')
          <div id="coss">
            <div class="form-group col-md-12">
              <label>Date:</label>
              <input type="text" id="countsd" name="date2" class="form-control" value="{{ $stats->date }}">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              @if ($stats->docs !='')
                <div class="col-md-3">
                    <img src="{{ url('public/trademark/'.$stats->docs) }}" alt="" class="img-responsive">
                    <a href="{{ url('images/delete/'.$stats->docs.'/'.$stats->id) }}" class="btn btn-sm btn-danger">Delete</a>
                </div>
                @else
                <input type="file" name="docs5" class="form-control">
                @endif

            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm7" class="form-control" rows="5">{{ $stats->comments }}</textarea>
            </div>
          </div>
          @endif
          @if ($stats->status == 'Active Application' && $stats->sub_status == 'Evidence and hearing')
          <div id="evhe">
            <div class="form-group col-md-12">
              <label>Date of evidence / hearing:</label>
              <input type="text" id="evdds" name="devd" class="form-control" value="{{ $stats->evi_date }}">
            </div>
            <div class="form-group col-md-12">
              <label>Document:</label>
              @if ($stats->docs !='')
                <div class="col-md-3">
                    <img src="{{ url('public/trademark/'.$stats->docs) }}" alt="" class="img-responsive">
                    <a href="{{ url('images/delete/'.$stats->docs.'/'.$stats->id) }}" class="btn btn-sm btn-danger">Delete</a>
                </div>
                @else
                <input type="file" name="docs6" class="form-control">
                @endif

            </div>
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm8" class="form-control" rows="5">{{ $stats->comments }}</textarea>
            </div>

            <div class="form-group col-md-12">
              <label>Reminder1:</label>
              <input type="text" id="evdd1" name="rem13" class="form-control" value="{{ $stats->rem1 }}">
            </div>
            <div class="form-group col-md-12">
              <label>Reminder2:</label>
              <input type="text" id="evdd2" name="rem23" class="form-control" value="{{ $stats->rem2 }}">
            </div>
            <div class="form-group col-md-12">
              <label>Reminder3:</label>
              <input type="text" id="evdd3" name="rem33" class="form-control" value="{{ $stats->rem3 }}">
            </div>
          </div>
          @endif
          @if ($stats->status == 'Active Application' && $stats->sub_status == 'Decision pending')
          <div id="depe">
            <div class="form-group col-md-12">
              <label>Comments:</label>
              <textarea name="comm12" class="form-control" rows="5">{{ $stats->comments }}</textarea>
            </div>
          </div>
          @endif

          <button type="submit" class="btn btn-success">Submit</button>
        </form>
        </div>
      </div>

    </div>
  </div>


@include('include.footer')
<script>
    $(function() {
        $('#regd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0
       });
       $('#abad').datepicker({
        changeMonth: true,
        changeYear: true,
        showAnim: 'slideDown',
        maxDate: 0
        });
        $('#withd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0
        });
        $('#mfed').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            maxDate: 0
       });
       $('#errd').datepicker({
        changeMonth: true,
        changeYear: true,
        showAnim: 'slideDown',
        maxDate: 0,
        onClose: function(){
            var date2 = $('#errd').datepicker('getDate');
            date2.setDate(date2.getDate()+30)
            $('#dropdate').datepicker('setDate',date2);
        }
        });
        $('#dropdate').datepicker();
        $('#rtod').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',

        });
        $('#herds').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',

        });
        $('#adad').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',

        });
        $('#opon').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',

        });
        $('#oponr').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            onClose: function(){
                var date2 = $('#oponr').datepicker('getDate');
                date2.setDate(date2.getDate()+60)
                $('#dfcstate').datepicker('setDate',date2);
            }

        });
        $('#dfcstate').datepicker();
        $('#countsd').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',

        });
        $('#evdds').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',

        });

        $('#remd1').datepicker({
        changeMonth: true,
        changeYear: true,
        showAnim: 'slideDown',
        minDate: +1
        });
        $('#remd2').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#remd3').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });

        $('#herd1').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#herd2').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#herd3').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });

        $('#evdd1').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#evdd2').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#evdd3').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });

        $('#evdd1s').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#evdd2s').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
        $('#evdd3s').datepicker({
            changeMonth: true,
            changeYear: true,
            showAnim: 'slideDown',
            minDate: +1
        });
    });
</script>
