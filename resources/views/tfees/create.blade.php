@include('include.header')
	<div class="page-wrapper">
		<div class="container-fluid">
			@include('include.message')
			<a href="{{ route('trademark_fees.index') }}" class="btn btn-xs btn-info">Go Back</a>
			{!! Form::open(['route' => 'trademark_fees.store','method' => 'POST']) !!}
				<div class="row">
					<div class="col-md-4 form-group">
						{{ Form::label('appf_id','Application Field:') }}
						{{ Form::select('appf_id',$ar1,null,['class' => 'form-control','placeholder' => 'Select']) }}
					</div>
					<div class="col-md-4 form-group">
						{{ Form::label('app_type','Application Type:') }}
						<select name="app_type" class="form-control">
							<option value="">Select</option>
							<option value="Online">Online</option>
							<option value="Offline">Offline</option>
						</select>
					</div>
					<div class="col-md-4 form-group">
						{{ Form::label('fees','Government Fees:') }}
						{{ Form::text('fees',null,['class' => 'form-control']) }}
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group text-center">
					{{ Form::submit('Submit',['class' => 'btn btn-success']) }}
				</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@include('include.footer')