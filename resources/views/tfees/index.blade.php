@include('include.header')
	<div class="page-wrapper">
		<div class="container-fluid">
			@include('include.message')
			<a href="{{ route('trademark_fees.create') }}" class="btn btn-xs btn-success">Create</a>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>Application Type</th>
						<th>Application Field</th>
						<th>Fees</th>
					</tr>
				</thead>
				<tbody>
					@foreach($comps as $comp)
						<tr>
							<td>{{ $comp->id }}</td>
							<td>{{ $comp->app_type }}</td>
							<td>{{ $comp->appf->application_field }}</td>
							<td>{{ $comp->fees }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@include('include.footer')