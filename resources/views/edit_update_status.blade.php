@include('include.header')
  <div class="page-wrapper">
    <div class="container-fluid">
      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Edit tradmark Status | <a href="{{action('FrontEndController@index')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Edit tradmark Status</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-body" id="app">
          <form action="{{url('status/updated')}}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
          @csrf
            <input type="hidden" name="id" value="{{$status->id}}">
            <input type="hidden" name="tradmark_id" value="{{$status->tradmark_id}}">
            <div class="row justify-content-center align-items-center">
              <div class="col-md-10">
                <div class="form-group">
                  <h5 align="center">tradmark Name : {{$status->tradmark_name}} </h5>
                  <input type="hidden" class="form-control" name="tradmark_name" value="{{$status->tradmark_name}}">
                </div>
                <div class="from-group" >
                  <label for="">Status</label>
                  <select name="status" class="form-control" >
                    <option value="0">--- Select ---</option>
                    @foreach($tradmark_statuss as $stat)
                      <option value="{{$stat->tradmark_status}}" @if($status->status== $stat->tradmark_status) selected='selected' @endif>{{$stat->tradmark_status}}</option>
                    @endforeach
                  </select>
                </div><br>
                <div class="from-group">
                  <label for="">Comment</label>
                  <textarea name="comment" id="" cols="30" rows="10" class="form-control">{{$status->comment}}</textarea>
                  </div>

              <div class="form-group" v-if="selected === 'Appealed/Opposed'">
                <h3>Opponent Details</h3><br>
                <label for="">Name</label>
                <input type="text" name="opponent_name" class="form-control" value="{{$appeal_opposeds[0]->opponent_name}}">
              </div>
              <div class="form-group" v-if="selected === 'Appealed/Opposed'">
                <label for="">Address</label>
                <textarea name="address" id="" cols="30" rows="10" class="form-control">{{$appeal_opposeds[0]->address}}</textarea>
              </div>
              <div class="form-group" v-if="selected === 'Appealed/Opposed'">
                <label for="">Mark</label>
                <input type="text" name="mark" class="form-control" value="{{$appeal_opposeds[0]->mark}}">
              </div>

               <div class="form-group">
                <label>Document :</label>

                @foreach(explode(',', $status->document) as $info)
             <li><img src="{{url('public/document/'.$info )}}" alt="" height="20px"></li>
         @endforeach

                  <input type="file" name="files[]" multiple class="form-control" value="{{$status->document}}">
                <!-- /.input group -->
              </div>

              <div class="from-group {{ $errors->has('date') ? 'has-error' : '' }}">
                <label for="">Date</label>
                <input type="date" name="date" class="form-control" value="{{$status->date}}">
                <span class="text-danger">{{ $errors->first('date') }}</span>
              </div><br>

             

            <!--   <div class="form-group" style="padding-bottom: 140px;">
                <label for="">Next Stage Date</label>
                <div class="input-group">
                  <div class="input-group-addon">
                   <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="stage_date" class="form-control pull-right" value="{{$status->stage_date}}" >
                </div>
              </div> -->

              <div class="form-group" v-if="selected === 'Appealed/Opposed'">
                <label for="">TM Journal Date</label>
                <div class="input-group">
                  <div class="input-group-addon">
                   <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="opponent_tm_journal_date" class="form-control pull-right" value="{{$appeal_opposeds[0]->opponent_tm_journal_date}}" id="reservationtime">
                </div>
              </div>

              <div class="form-group" v-if="selected === 'Appealed/Opposed'">
                <label for="">Class</label>
                <input type="text" name="class" class="form-control" value="{{$appeal_opposeds[0]->class}}">
              </div>
                    </div>

                    </div>

                     

                    <div class="col-md-12 text-center"><button type="submit" class="btn btn-success">Update</button></div>

                  </form>
                   <br>

                   <div class="col-md-12">
            
</section>

 <section class="content"  v-else>
             <table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
        <th>tradmark Name</th>
        <th>Comment</th>
        <th>Document</th>
        <th>Status</th>
        <th>Next Stage Date</th>
        <th>Edit</th>
      </tr>
    </thead>
    <tbody>
            <?php $i = 1; ?>
      @foreach($statues as $stat)
      <tr>
        <td>{{$i++}}</td>
        <td>{{$stat->tradmark_name}}</td>
        <td>{{$stat->comment}}</td>
        <td>@foreach(explode(',', $stat->document) as $info)
             <li><img src="{{url('public/document/'.$info )}}" alt="" height="20px"></li>
         @endforeach</td>
        <td>{{$stat->status}}</td>
        <td>{{$stat->stage_date}}</td>
        <td><a href="{{url('status/'.$stat->id.'/edit')}}" class="btn btn-primary">Edit</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
                   </div>
                  </div>
                    
                </div>

               
    

                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>

<script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script type="text/javascript">
     var demo = new Vue({
      el: '#app',
      data: {
               selected: '0'
             }
       });
    </script>
  @include('include.footer')