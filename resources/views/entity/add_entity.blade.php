@include('include.header')
    <style>
        .card{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Add Entity | <a href="{{action('AddTradmarkController@entity')}}" >Go Back <i class="fa fa-arrow-circle-left"></i></a> </h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{action('FrontEndController@index')}}">Home</a></li>
                            <li class="breadcrumb-item active">Add Entity</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            {!! Form::open(['url' => 'entity/save','data-parsley-validate' => '','method' => 'POST']) !!}
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        {!! Form::label('name', 'Entity Name:') !!}
                                        {!! Form::text('name', null, ['class' => 'form-control','required' => '']) !!}
                                    </div>
                                    <div class="col-md-6 form-group">
                                        {!! Form::label('nat_id', 'Nature of Entity:') !!}
                                        {!! Form::select('nat_id', $ar1, null, ['class' => 'form-control','required' => '','placeholder' => 'Select Nature of Entity']) !!}
                                    </div>
                                    <div class="col-md-12 text-center form-group">
                                        {!! Form::submit('submit', ['class' => 'btn btn-success']) !!}
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')
