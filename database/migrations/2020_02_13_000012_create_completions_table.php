<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompletionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('completions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('liti_id');
            $table->string('status');
            $table->date('dis_date');
            $table->date('last_date')->nullable();
            $table->text('comments');
            $table->string('doc')->nullable();
            $table->string('synop')->nullable();
            $table->string('court');
            $table->timestamps();
        });

        Schema::table('completions', function ($table){
            $table->foreign('liti_id')->references('id')->on('litigation_summaries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('completions');
    }
}
