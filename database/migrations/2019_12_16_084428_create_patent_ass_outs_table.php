<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatentAssOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patent_ass_outs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_of_assignor');
            $table->string('name_of_assignee');
            $table->date('date_of_assignment');
            $table->string('docs');
            $table->date('rem1')->nullable();
            $table->text('comments');
            $table->integer('assin_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patent_ass_outs');
    }
}
