<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestigationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investigations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('investigation_agency');
            $table->string('contact_n');
            $table->string('contact_no');
            $table->string('email');
            $table->string('state');
            $table->string('city');
            $table->string('target');
            $table->string('product');
            $table->date('dsi');
            $table->date('dei');
            $table->string('docs');
            $table->string('tm_app');
            $table->string('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investigations');
    }
}
