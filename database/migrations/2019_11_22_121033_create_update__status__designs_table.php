<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateStatusDesignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('update__status__designs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('des_id')->unsigned();
            $table->string('status')->nullable();
            $table->string('sub_status')->nullable();
            $table->date('date')->nullable();
            $table->text('comment')->nullable();
            $table->string('doc')->nullable();
            $table->date('rep_d')->nullable();
            $table->date('rem1')->nullable();
            $table->date('rem2')->nullable();
            $table->date('rem3')->nullable();
            $table->date('rem4')->nullable();
            $table->string('cor')->nullable();
            $table->date('dor')->nullable();
            $table->string('jno')->nullable();
            $table->date('doe')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update__status__designs');
    }
}
