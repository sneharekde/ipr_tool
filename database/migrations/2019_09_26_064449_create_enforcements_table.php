<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnforcementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enforcements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('target_name');
            $table->string('owner_name');
            $table->string('state');
            $table->string('city');
            $table->string('target');
            $table->string('name_of_product');
            $table->string('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enforcements');
    }
}
