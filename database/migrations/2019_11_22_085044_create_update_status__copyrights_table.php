<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateStatusCopyrightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('update_status__copyrights', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status')->nullable();
            $table->string('sub_status')->nullable();
            $table->date('date')->nullable();
            $table->date('rem')->nullable();
            $table->text('comment')->nullable();
            $table->string('docs')->nullable();
            $table->date('reply_deadline')->nullable();
            $table->date('rem1')->nullable();
            $table->date('rem2')->nullable();
            $table->date('rem3')->nullable();
            $table->date('auto_deadline')->nullable();
            $table->string('cor')->nullable();
            $table->date('dor')->nullable();
            $table->date('doe')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_status__copyrights');
    }
}
