<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradAgentFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trad_agent_fees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('trade_id');
            $table->date('date');
            $table->decimal('fee',10,2);
            $table->string('invoice')->nullable();
            $table->decimal('not_fee',10,2)->nullable();
            $table->string('stamp')->nullable();
            $table->timestamps();
        });

        Schema::table('trad_agent_fees', function ($table){
            $table->foreign('trade_id')->references('id')->on('trademarks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trad_agent_fees');
    }
}
