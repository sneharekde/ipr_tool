<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCopyAssOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('copy_ass_outs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('copy_id')->nullable();
            $table->string('licensor');
            $table->string('licensee');
            $table->integer('term');
            $table->date('lic_date');
            $table->date('exp_date');
            $table->string('consideration');
            $table->date('rem_ip')->nullable();
            $table->date('ent_date')->nullable();
            $table->date('ent_date2')->nullable();
            $table->string('docs')->nullable();
            $table->date('reminder')->nullable();
            $table->text('comments')->nullable();
            $table->timestamps();
        });

        Schema::table('copy_ass_outs', function ($table){
            $table->foreign('copy_id')->references('id')->on('copyrights')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('copy_ass_outs');
    }
}
