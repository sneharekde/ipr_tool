<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHearingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hearings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('hear_id');
            $table->date('date');
            $table->date('her_date');
            $table->date('alert1');
            $table->date('alert2');
            $table->date('alert3');
            $table->string('email');
            $table->text('stage_desc');
            $table->text('act_item');
            $table->unsignedBigInteger('user_id');
            $table->string('ext_counsel');
            $table->string('law_firm');
            $table->date('due_date');
            $table->string('docs');
            $table->timestamps();
        });

        Schema::table('hearings', function ($table){
            $table->foreign('hear_id')->references('id')->on('litigation_summaries')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hearings');
    }
}
