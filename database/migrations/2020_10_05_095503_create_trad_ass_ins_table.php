<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradAssInsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trad_ass_ins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('trad_id')->nullable();
            $table->string('licensor');
            $table->string('licensee');
            $table->integer('term');
            $table->date('lic_date');
            $table->date('exp_date');
            $table->string('consideration');
            $table->date('rem_ip')->nullable();
            $table->date('ent_date')->nullable();
            $table->date('ent_date2')->nullable();
            $table->string('docs')->nullable();
            $table->date('reminder')->nullable();
            $table->text('comments')->nullable();
            $table->timestamps();
        });
        Schema::table('trad_ass_ins', function ($table){
            $table->foreign('trad_id')->references('id')->on('trademarks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trad_ass_ins');
    }
}
