<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnforcementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enforcementss', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ent_id')->nullable();
            $table->string('target_name')->nullable();
            $table->string('owner')->nullable();
            $table->date('date');
            $table->unsignedBigInteger('country_id');
            $table->unsignedBigInteger('state_id');
            $table->string('city');
            $table->string('target');
            $table->string('nop');
            $table->string('qty');
            $table->string('amt');
            $table->text('docs');
            $table->string('tm_app');
            $table->timestamps();
        });
        Schema::table('enforcementss', function ($table){
            $table->foreign('ent_id')->references('id')->on('entity_lists')->onDelete('cascade');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('state_id')->references('id')->on('stetes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enforcements');
    }
}
