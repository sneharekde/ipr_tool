<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCounselPaidFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counsel_paid_fees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('counself_id');
            $table->string('inv_no');
            $table->decimal('inv_amt',10,2);
            $table->string('inv_curr');
            $table->date('inv_date');
            $table->text('comment')->nullable();
            $table->text('docs')->nullable();
            $table->date('date');
            $table->decimal('paid_fees',10,2);
            $table->string('currency');
            $table->string('conv_curr');
            $table->decimal('conv_amt',10,2);
            $table->timestamps();
        });

        Schema::table('counsel_paid_fees',function($table){
            $table->foreign('counself_id')->references('id')->on('counsel_fees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counsel_paid_fees');
    }
}
