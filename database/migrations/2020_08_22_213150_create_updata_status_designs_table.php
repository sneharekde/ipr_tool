<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdataStatusDesignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('updata_status_designs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('design_id');
            $table->string('status');
            $table->string('sub_status')->nullable();
            $table->date('date')->nullable();
            $table->text('comment')->nullable();
            $table->text('docs')->nullable();
            $table->date('derrer')->nullable();
            $table->date('rep_dead')->nullable();
            $table->date('rem1')->nullable();
            $table->date('rem2')->nullable();
            $table->date('rem3')->nullable();
            $table->date('rem4')->nullable();
            $table->date('dfh')->nullable();
            $table->date('dfrem')->nullable();
            $table->date('dor')->nullable();
            $table->date('d_renr')->nullable();
            $table->date('pub_date')->nullable();
            $table->string('jrno')->nullable();
            $table->date('ren_date')->nullable();
            $table->date('exp_date')->nullable();
            $table->boolean('sent_status')->default('0');
            $table->timestamps();
        });

        Schema::table('updata_status_designs', function ($table){
            $table->foreign('design_id')->references('id')->on('designms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('updata_status_designs');
    }
}
