<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLitigationSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('litigation_summaries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('entity');
            $table->string('Location');
            $table->string('department');
            $table->string('category');
            $table->string('by_ag');
            $table->string('by_part')->nullable();
            $table->string('against_part')->nullable();
            $table->string('acting_as')->nullable();
            $table->string('ext_counsel')->nullable();
            $table->string('law_firm')->nullable();
            $table->string('adv')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->date('case_date')->nullable();
            $table->string('int_per')->nullable();
            $table->string('critic')->nullable();
            $table->string('opp_act_as')->nullable();
            $table->string('opp_part_add')->nullable();
            $table->string('opp_part_firm')->nullable();
            $table->string('opp_part_adv')->nullable();
            $table->string('opp_part_adv_c')->nullable();
            $table->string('case_ref')->nullable();
            $table->string('juris')->nullable();
            $table->string('court')->nullable();
            $table->string('rel_law')->nullable();
            $table->string('comments')->nullable();
            $table->date('date')->nullable();
            $table->decimal('amount_involved',8,2)->nullable();
            $table->string('currency')->nullable();
            $table->string('conv_curr')->nullable();
            $table->string('conv_amt')->nullable();
            $table->string('status')->default('pending');
            $table->string('prio')->default('1');
            $table->timestamps();
        });

        Schema::table('litigation_summaries', function ($table){
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('litigation_summaries');
    }
}
