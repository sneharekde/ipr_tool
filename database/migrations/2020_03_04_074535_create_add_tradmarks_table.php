<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddTradmarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_tradmarks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('app_no');
            $table->string('agent_code')->nullable();
            $table->unsignedBigInteger('prod_id')->nullable();
            $table->date('app_date');
            $table->unsignedBigInteger('natapl_id');
            $table->unsignedBigInteger('user_id');
            $table->string('application_no');
            $table->decimal('agent_fee',10,2)->nullable();
            $table->unsignedBigInteger('appf_id');
            $table->unsignedBigInteger('app_type_id');
            $table->decimal('gov_fee',10,2);
            $table->unsignedBigInteger('ent_id')->nullable();
            $table->unsignedBigInteger('nate_id')->nullable();
            $table->string('name_in');
            $table->unsignedBigInteger('agent_id')->nullable();
            $table->string('trademark');
            $table->unsignedBigInteger('catmak_id');
            $table->unsignedBigInteger('lang_id');
            $table->text('desc_mark')->nullable();
            $table->string('remarks')->nullable();
            $table->unsignedBigInteger('classg_id');
            $table->string('name_person')->nullable();
            $table->string('authority')->nullable();
            $table->string('logo')->nullable();
            $table->string('statement_mark')->nullable();
            $table->string('scan_copy_app')->nullable();
            $table->boolean('app_status')->default('0');
            $table->string('status')->default('Active Application');
            $table->string('sub_status')->default('Application Submitted');
            $table->integer('sp')->default('1');
            $table->integer('ssp')->default('1');
            $table->timestamps();
        });

        Schema::table('add_tradmarks', function ($table){
            $table->foreign('prod_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('natapl_id')->references('id')->on('nature_applications')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('appf_id')->references('id')->on('application_fields')->onDelete('cascade');
            $table->foreign('app_type_id')->references('id')->on('trademark_gov_fees')->onDelete('cascade');
            $table->foreign('ent_id')->references('id')->on('entity_lists')->onDelete('cascade');
            $table->foreign('nate_id')->references('id')->on('nature_applicants')->onDelete('cascade');
            $table->foreign('agent_id')->references('id')->on('agents')->onDelete('cascade');
            $table->foreign('catmak_id')->references('id')->on('category_marks')->onDelete('cascade');
            $table->foreign('lang_id')->references('id')->on('languages')->onDelete('cascade');
            $table->foreign('classg_id')->references('id')->on('trade_classes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_tradmarks');
    }
}
