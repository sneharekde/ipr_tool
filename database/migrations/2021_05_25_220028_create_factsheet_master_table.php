<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactsheetMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factsheet_master', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_of_initiator');
            $table->integer('designation');
            $table->string('name_of_channel');
            $table->longText('registration_mark');
            $table->longText('mark_used_from');
            $table->longText('class_of_registration');
            $table->string('jurisdiction_for_registration');
            $table->string('significance_of_work');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factsheet_master');
    }
}
