<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCopyrightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('copyrights', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('gov')->unsigned();
            $table->string('agent');
            $table->integer('dairy');
            $table->date('dof');
            $table->string('user');
            $table->string('nofa');
            $table->string('title');
            $table->string('image');
            $table->text('description');
            $table->string('language');
            $table->text('remarks');
            $table->string('user_aff');
            $table->string('name');
            $table->string('designation');
            $table->string('copy_app');
            $table->string('status');
            $table->string('sub_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('copyrights');
    }
}
