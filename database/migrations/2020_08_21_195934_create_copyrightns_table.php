<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCopyrightnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('copyrightns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('gov',10,2);
            $table->unsignedBigInteger('agent_id')->nullable();
            $table->string('diary');
            $table->date('dof');
            $table->unsignedBigInteger('user_id');
            $table->string('nofa')->nullable();
            $table->string('titlew')->nullable();
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->unsignedBigInteger('lang_id')->nullable();
            $table->text('remarks')->nullable();
            $table->text('user_aff')->nullable();
            $table->string('name')->nullable();
            $table->string('designation')->nullable();
            $table->text('copy_app')->nullable();
            $table->text('roc')->nullable();
            $table->string('status');
            $table->string('sub_status')->nullable();
            $table->string('sp')->default('1');
            $table->string('ssp')->default('1');
            $table->boolean('app_status')->default('1');
            $table->timestamps();
        });

        Schema::table('copyrightns', function ($table){
            $table->foreign('agent_id')->references('id')->on('agents')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('lang_id')->references('id')->on('languages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('copyrightns');
    }
}
