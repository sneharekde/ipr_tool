<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateCopyrightStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('update_copyright_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('copy_id');
            $table->string('status');
            $table->string('sub_status')->nullable();
            $table->date('date')->nullable();
            $table->text('comment')->nullable();
            $table->text('docs')->nullable();
            $table->date('mwp_date')->nullable();
            $table->date('rem_mwp_date')->nullable();
            $table->date('rep_dead')->nullable();
            $table->date('rem1')->nullable();
            $table->date('rem2')->nullable();
            $table->date('rem3')->nullable();
            $table->date('dfh')->nullable();
            $table->date('ddj')->nullable();
            $table->date('deadline')->nullable();
            $table->date('d_rer')->nullable();
            $table->date('dor')->nullable();
            $table->date('dren')->nullable();
            $table->date('ren_date')->nullable();
            $table->date('exp_date')->nullable();
            $table->boolean('sent_status')->default('0');
            $table->timestamps();
        });

        Schema::table('update_copyright_statuses', function ($table){
            $table->foreign('copy_id')->references('id')->on('copyrightns')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_copyright_statuses');
    }
}
