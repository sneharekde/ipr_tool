<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('license')->nullable();
            $table->date('license_date')->nullable();
            $table->string('term_of_license')->nullable();
            $table->date('expiry_date')->nullable();
            $table->date('reminder_two')->nullable();
            $table->string('license_document')->nullable();
            $table->integer('tradmark_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_items');
    }
}
