<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvocatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advocates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('country_id');
            $table->unsignedBigInteger('state_id');
            $table->string('city');
            $table->unsignedBigInteger('law_id');
            $table->string('counsel');
            $table->string('number');
            $table->string('email');
            $table->text('address')->nullable();
            $table->unsignedBigInteger('expert_id');
            $table->timestamps();
        });
        Schema::table('advocates',function($table){
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('state_id')->references('id')->on('stetes')->onDelete('cascade');
            $table->foreign('law_id')->references('id')->on('law_firms')->onDelete('cascade');
            $table->foreign('expert_id')->references('id')->on('area_exps')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advocates');
    }
}
