<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('location')->nullable();
            $table->string('leadby')->nullable();
            $table->string('product')->nullable();
            $table->unsignedBigInteger('target_id')->nullable();
            $table->string('emp_id')->nullable();
            $table->text('docs')->nullable();
            $table->text('comments')->nullable();
            $table->timestamps();
        });

        Schema::table('leads', function ($table){
            $table->foreign('target_id')->references('id')->on('targets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
