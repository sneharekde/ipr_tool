<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradeClassTrademarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_class_trademarks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('trademarks_id');
            $table->unsignedBigInteger('trade_classes_id');
            $table->timestamps();
        });
        Schema::table('trade_class_trademarks',function($table){
            $table->foreign('trademarks_id')->references('id')->on('trademarks')->onDelete('cascade');
            $table->foreign('trade_classes_id')->references('id')->on('trade_classes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_class_trademarks');
    }
}
