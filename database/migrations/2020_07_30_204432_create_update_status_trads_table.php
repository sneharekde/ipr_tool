<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateStatusTradsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('update_status_trads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('trade_id');
            $table->string('status');
            $table->string('sub_status')->nullable();
            $table->text('comments')->nullable();
            $table->string('docs')->nullable();
            $table->date('date')->nullable();
            $table->date('rem1')->nullable();
            $table->date('rem2')->nullable();
            $table->date('rem3')->nullable();
            $table->date('rem4')->nullable();
            $table->date('rec_date')->nullable();
            $table->date('rep_date')->nullable();
            $table->date('her_date')->nullable();
            $table->date('adv_date')->nullable();
            $table->date('not_date')->nullable();
            $table->date('reo_date')->nullable();
            $table->date('evi_date')->nullable();
            $table->date('dor_date')->nullable();
            $table->date('rep_dead')->nullable();
            $table->date('opp_dead')->nullable();
            $table->date('cou_dead')->nullable();
            $table->date('int_dead')->nullable();
            $table->date('ren_date')->nullable();
            $table->boolean('sent_status')->default('0');
            $table->timestamps();
        });

        Schema::table('update_status_trads', function ($table){
            $table->foreign('trade_id')->references('id')->on('trademarks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_status_trads');
    }
}
