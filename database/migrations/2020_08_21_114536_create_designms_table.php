<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('prop_code');
            $table->decimal('govt_fee',10,2);
            $table->unsignedBigInteger('agent_id')->nullable();
            $table->string('app_no');
            $table->date('filing_date');
            $table->string('app_type');
            $table->unsignedBigInteger('user_id');
            $table->string('name_of_applicant')->nullable();
            $table->unsignedBigInteger('natappli_id')->nullable();
            $table->unsignedBigInteger('ent_id')->nullable();
            $table->unsignedBigInteger('natent_id')->nullable();
            $table->string('article_name')->nullable();
            $table->string('image_t')->nullable();
            $table->text('description')->nullable();
            $table->unsignedBigInteger('classg_id');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('lang_id')->nullable();
            $table->string('remarks')->nullable();
            $table->string('user_aff')->nullable();
            $table->string('name')->nullable();
            $table->string('designation')->nullable();
            $table->string('status');
            $table->string('sub_status');
            $table->string('sp')->default('1');
            $table->string('ssp')->default('1');
            $table->boolean('app_status')->default('1');
            $table->timestamps();
        });

        Schema::table('designms', function ($table){
            $table->foreign('agent_id')->references('id')->on('agents')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('natappli_id')->references('id')->on('application_fields')->onDelete('cascade');
            $table->foreign('ent_id')->references('id')->on('entity_lists')->onDelete('cascade');
            $table->foreign('natent_id')->references('id')->on('nature_applicants')->onDelete('cascade');
            $table->foreign('classg_id')->references('id')->on('design_classes')->onDelete('cascade');
            $table->foreign('lang_id')->references('id')->on('languages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designms');
    }
}
