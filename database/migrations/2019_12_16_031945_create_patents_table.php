<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('priority_date');
            $table->date('filing_date');
            $table->string('application_no');
            $table->string('applicant_name');
            $table->string('applicant_id');
            $table->string('cat_app');
            $table->string('cat_app_s')->nullable();
            $table->string('invent_name');
            $table->string('title_inven');
            $table->string('reg_pat');
            $table->string('pct')->nullable();
            $table->string('int_app_no')->nullable();
            $table->date('int_fili_date')->nullable();
            $table->date('renewal');
            $table->integer('gov')->unsigned();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('file')->nullable();
            $table->timestamps();
        });

        Schema::table('patents', function ($table){
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patents');
        Schema::dropForeign(['user_id']);
    }
}
