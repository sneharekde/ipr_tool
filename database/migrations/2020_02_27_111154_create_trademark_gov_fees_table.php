<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrademarkGovFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trademark_gov_fees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('appf_id');
            $table->string('app_type');
            $table->decimal('fees',10,2);
            $table->timestamps();
        });

        Schema::table('trademark_gov_fees', function ($table){
            $table->foreign('appf_id')->references('id')->on('application_fields')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trademark_gov_fees');
    }
}
