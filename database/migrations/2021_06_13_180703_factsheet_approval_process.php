<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FactsheetApprovalProcess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('factsheet_approval_process', function (Blueprint $table) {
    		$table->bigIncrements('id');
    		$table->integer('factsheet_id');
    		$table->integer('initiator_id');
    		$table->string('initiator_agent_name');
    		$table->string('initiator_agent_email');
    		$table->integer('initiator_optional_id');
    		$table->string('initiator_optional_agent_name');
    		$table->string('initiator_optional_agent_email');
    		$table->integer('legal_id');
    		$table->string('legal_agent_name');
    		$table->string('legal_agent_email');
    		$table->integer('coo_id');
    		$table->string('coo_agent_name');
    		$table->string('coo_agent_email');
    		$table->integer('cfo_id');
    		$table->string('cfo_agent_name');
    		$table->string('cfo_agent_email');
    		$table->integer('ceo_id');
    		$table->string('ceo_agent_name');
    		$table->string('ceo_agent_email');
    		$table->integer('factsheet_status_steps');
    		$table->tinyInteger('factsheet_approval_status');
    		$table->timestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
