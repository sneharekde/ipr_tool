<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCopyrightLicOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('copyright_lic_outs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_of_licensor');
            $table->string('name_of_licensee');
            $table->string('term_of_license');
            $table->date('license_date');
            $table->date('expiry_date');
            $table->string('docs');
            $table->date('rem1')->nullable();
            $table->text('comments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('copyright_lic_outs');
    }
}
