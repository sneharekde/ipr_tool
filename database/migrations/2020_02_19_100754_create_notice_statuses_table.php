<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticeStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notice_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('stat_id');
            $table->date('r_date');
            $table->text('act_take');
            $table->text('next_act');
            $table->date('due_date');
            $table->date('reminder');
            $table->unsignedBigInteger('user_id');
            $table->string('docs');
            $table->timestamps();
        });

        Schema::table('notice_statuses', function ($table){
            $table->foreign('stat_id')->references('id')->on('litigation_notices')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notice_statuses');
    }
}
