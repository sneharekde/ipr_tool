<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLitigationNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('litigation_notices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('entity');
            $table->string('Location');
            $table->string('department');
            $table->string('by_ag');
            $table->string('opp_part')->nullable();
            $table->string('category');
            $table->string('not_ref')->nullable();
            $table->string('addr_to')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->string('int_per');
            $table->date('notice_date')->nullable();
            $table->date('sent_rec')->nullable();
            $table->date('not_reply')->nullable();
            $table->date('not_rem')->nullable();
            $table->string('ext_counsel')->nullable();
            $table->string('opp_part_adv')->nullable();
            $table->string('rel_law')->nullable();
            $table->text('comments')->nullable();
            $table->string('docs')->nullable();
            $table->date('date')->nullable();
            $table->decimal('amount_involved',8,2)->nullable();
            $table->string('currency')->nullable();
            $table->string('conv_curr')->nullable();
            $table->string('conv_amt')->nullable();
            $table->string('status')->default('pending');
            $table->timestamps();
        });
        Schema::table('litigation_notices', function ($table){
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('litigation_notices');
    }
}
