<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCounselFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counsel_fees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('liti_id');
            $table->unsignedBigInteger('law_id');
            $table->unsignedBigInteger('counsel_id');
            $table->string('fees_type');
            $table->string('effect')->nullable();
            $table->text('comment')->nullable();
            $table->text('docs')->nullable();
            $table->date('date');
            $table->decimal('fees',10,2);
            $table->string('currency');
            $table->string('conv_curr');
            $table->decimal('conv_amt',10,2);
            $table->timestamps();
        });
        Schema::table('counsel_fees',function($table){
            $table->foreign('liti_id')->references('id')->on('litigation_summaries')->onDelete('cascade');
            $table->foreign('law_id')->references('id')->on('law_firms')->onDelete('cascade');
            $table->foreign('counsel_id')->references('id')->on('external_counsels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counsel_fees');
    }
}
