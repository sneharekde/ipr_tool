<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('entity');
            $table->string('address_for_service');
            $table->string('trading_as');
            $table->string('country');
            $table->string('state');
            $table->string('mobile_number');
            $table->string('applicant_name');
            $table->text('address');
            $table->string('s_state');
            $table->string('s_country');
            $table->string('e_mail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants');
    }
}
