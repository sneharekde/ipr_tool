<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('update_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('tradmark_id');
            $table->string('tradmark_name');
            $table->string('document');
            $table->text('comment');
            $table->string('status');
            $table->string('stage_date');
            $table->string('opponent_name')->nullable();
            $table->string('opponent_tm_journal_date')->nullable();
            $table->text('address')->nullable();
            $table->string('class')->nullable();
            $table->string('mark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_statuses');
    }
}
