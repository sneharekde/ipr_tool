<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ent_id')->nullable();
            $table->unsignedBigInteger('inag_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('country_id');
            $table->unsignedBigInteger('state_id');
            $table->string('city');
            $table->string('target')->nullable();
            $table->string('nop');
            $table->date('dsi');
            $table->date('dei');
            $table->text('docs')->nullable();
            $table->string('tm_app')->nullable();
            $table->text('nota')->nullable();
            $table->text('n_addr')->nullable();
            $table->timestamps();
        });

        Schema::table('invests', function ($table){
            $table->foreign('ent_id')->references('id')->on('entity_lists')->onDelete('cascade');
            $table->foreign('inag_id')->references('id')->on('agencies')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('state_id')->references('id')->on('stetes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invests');
    }
}
