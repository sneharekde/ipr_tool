<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\AddTradmark;
use Faker\Generator as Faker;

$factory->define(AddTradmark::class, function (Faker $faker) {
    return [
        //
            'app_no' => $faker->app_no,
            'agent_code'=>$faker->agent_code,
            'attorney_code'=>$faker->attorney_code,
            'type_app'=>$faker->type_app,
    		'app_date'=>$faker->app_date,
    		'user_info'=>$faker->user_info,
    		'series_marks'=>$faker->series_marks,
    		'app_fiel'=>$faker->app_fiel,
    		'fee'=>$faker->fee,
    		'appicatn_name'=>$faker->appicatn_name,
    		'nature_of_app'=>$faker->nature_of_app,
    		'agent_name'=>$faker->agent_name,
    		'trademark'=>$faker->trademark,
    		'category_of_mark'=>$faker->category_of_mark,
    		'language'=>$faker->language,
    		'd_mark'=>$faker->d_mark,
    		'logo'=>$file_logo,
    		'conditions_limitations'=>$faker->conditions_limitations,
    		'class_of_goods'=>$faker->class_of_goods,
            'class_categorys'=>$class,
    		'statement_mark'=>$faker->statement_mark,
    		'priority_since'=>$faker->priority_since,
    		'name_organization'=>$faker->name_organization,
    		'priority_app'=>$faker->priority_app,
    		'statement'=>$faker->statement,
    		'signature'=>$faker->signature,
    		'name_person'=>$faker->name_person,
    		'authority'=>$faker->authority,
    		'scan_copy_app'=>$file_scan_copy
    ];
});
