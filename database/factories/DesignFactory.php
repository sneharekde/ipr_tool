<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Design;
use Faker\Generator as Faker;

$factory->define(Design::class, function (Faker $faker) {
    return [
        'proprietor_code' 		=> $faker->proprietor_code,
        'govt_fee' 				=> $faker->govt_fee,
        'agent_name' 			=> $faker->agent_name,
        'app_no' 				=> $faker->app_no,
        'filing_date' 			=> $faker->filing_date,
        'type_of_app' 			=> $faker->type_of_app,
        'user' 					=> $faker->user,
        'name_of_applicant' 	=> $faker->name_of_applicant,
        'nature_of_applicant' 	=> $faker->nature_of_applicant,
        'entity' 				=> $faker->entity,
        'nature_of_entity' 		=> $faker->nature_of_entity,
        'name_of_article' 		=> $faker->name_of_article,
        'image_t' 				=> $faker->image_t,
        'description' 			=> $faker->description,
        'class_of_goods' 		=> $faker->class_of_goods,
        'class_categorys' 		=> $faker->class_categorys,
        'image' 				=> $faker->image,
        'language' 				=> $faker->language,
        'remarks' 				=> $faker->remarks,
        'user_aff' 				=> $faker->user_aff,
        'name' 					=> $faker->name,
        'designation' 			=> $faker->designation,
        'copy_application' 		=> $faker->copy_application
    ];
});
