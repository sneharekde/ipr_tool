<?php

use Illuminate\Database\Seeder;
use App\AddTradmark;

class AddTradmarksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $count = 10;
        factory(AddTradmark::class, $count)->create();
    }
}
