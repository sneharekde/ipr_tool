<?php

use Illuminate\Database\Seeder;
use App\Design;
class DesignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	$count = 10;
        factory(Design::class, $count)->create();   
    }
}
