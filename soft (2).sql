-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2019 at 08:22 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `soft`
--

-- --------------------------------------------------------

--
-- Table structure for table `action_items`
--

CREATE TABLE `action_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trademark_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_item` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_deadline` date NOT NULL,
  `reminder_one` date NOT NULL,
  `reminder_two` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tradmark_id` int(11) NOT NULL,
  `update_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `action_items`
--

INSERT INTO `action_items` (`id`, `trademark_name`, `status`, `action_item`, `action_deadline`, `reminder_one`, `reminder_two`, `created_at`, `updated_at`, `tradmark_id`, `update_status`, `comments`) VALUES
(2, 'T2', 'New Tradmark', 'Demo 2', '2019-08-12', '2019-08-09', '2019-08-11', NULL, '2019-08-15 09:39:57', 2, 'Completed', 'this is completed'),
(3, 'T6', 'Marked for Exam', 'What To do....', '2019-08-20', '2019-08-16', '2019-08-17', '2019-08-15 09:08:38', '2019-08-15 09:08:38', 3, NULL, NULL),
(4, 'T4', 'Registered', 'What To do....', '2019-08-24', '2019-08-20', '2019-08-23', '2019-08-15 09:11:28', '2019-08-15 09:11:28', 4, NULL, NULL),
(5, 'T1', 'New Trademark', 'This is testings.....', '2019-08-30', '2019-08-17', '2019-08-21', '2019-08-15 09:25:00', '2019-08-15 09:25:00', 1, NULL, NULL),
(6, 'T2', 'Marked for Exam', 'This is testings.....', '2019-08-22', '2019-08-17', '2019-08-18', '2019-08-15 09:27:27', '2019-08-15 09:27:27', 2, NULL, NULL),
(7, 'T2', 'Marked for Exam', 'What To do....', '2019-08-30', '2019-08-27', '2019-08-26', '2019-08-15 09:29:08', '2019-08-15 09:29:08', 2, NULL, NULL),
(8, 'T8', 'Accepted and Advertised', 'Follow up', '2019-08-30', '2019-08-27', '2019-08-24', '2019-08-17 16:56:36', '2019-08-17 16:57:30', 8, 'Completed', 'Followed up');

-- --------------------------------------------------------

--
-- Table structure for table `add_tradmarks`
--

CREATE TABLE `add_tradmarks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `app_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_date` date NOT NULL,
  `user_info` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `series_marks` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_fiel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appicatn_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nature_of_app` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trademark` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_of_mark` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `d_mark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `conditions_limitations` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_of_goods` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statement_mark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority_since` date DEFAULT NULL,
  `name_organization` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority_app` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statement` text COLLATE utf8mb4_unicode_ci,
  `signature` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_person` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authority` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scan_copy_app` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `class_categorys` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `update_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entitty` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attorney_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_app` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `add_tradmarks`
--

INSERT INTO `add_tradmarks` (`id`, `app_no`, `app_date`, `user_info`, `series_marks`, `app_fiel`, `fee`, `appicatn_name`, `nature_of_app`, `agent_name`, `trademark`, `category_of_mark`, `language`, `d_mark`, `logo`, `conditions_limitations`, `class_of_goods`, `statement_mark`, `priority_since`, `name_organization`, `priority_app`, `statement`, `signature`, `name_person`, `authority`, `scan_copy_app`, `status`, `created_at`, `updated_at`, `class_categorys`, `update_status`, `entitty`, `agent_code`, `attorney_code`, `type_app`) VALUES
(1, '1', '2019-07-17', '0', 'Standard Trademark', 'Individual', '500', 'Demo test', 'Partnership Firm', 'Nilesh', 'T1', '0', '0', 't1 trademark', 'brown-circle-and-chocolate-coffee.png', 'NA', '2', 'NA', '2015-05-19', 'India', '12', 'This is statement test mark', 'Asif Khan', 'A.K', 'ag', '551103-1TOqFD1502285018.jpg', '1', '2019-05-14 00:37:54', '2019-08-29 07:04:47', 'class 1, class 2', 'Active Trademark', 'ABC Limited', NULL, NULL, NULL),
(2, '2', '2019-07-18', '2', 'Standard Trademark', 'Individual', '2000', 'Arif Ansari', 'Partnership Firm', 'arif', 'T2', 'Three Dimensional trademark', '2', 't2  trademark', 'logo-4.jpg', 'NA', '1', 'NA', '2016-05-19', 'India', '12', 'This is test....', 'Asif Khan', 'A.K', 'AG', 'logo-ex-7.png', '0', '2019-05-14 00:44:38', '2019-08-29 07:53:12', 'class4, class6', 'Registered', 'ABC Limited', NULL, NULL, NULL),
(3, '3', '2019-07-19', '1', 'Series marks', 'Enterprise', '3000', 'Sharat Kumar', 'Partnership Firm', 'Kalam ', 'T3', 'Colour', '2', 't3  trademark', 'logo-4.jpg', 'NA', '2', 'NA', '2016-05-19', 'India', '13', 'this is testings...', 'Asif Khan', 'A.K', 'AG', 'logo-ex-7.png', '0', '2019-05-14 00:50:27', '2019-07-27 07:32:21', 'class 8, class 10', 'Marked for Exam', 'ABC Limited', NULL, NULL, NULL),
(4, '4', '2019-07-16', '1', 'Series marks', 'StartUp', '4000', 'Demo Name', 'Body-incorporate including Private Limited/limited Company', 'saif Ali', 'T4', 'Device Mark', '1', 't4  trademark', 'logo-4.jpg', 'NA', '1', 'NA', '2020-05-19', 'India', '12', 'This is test', 'Saif Ali', 'S.A', 'AG', 'logo-ex-7.png', '0', '2019-05-14 00:56:39', '2019-07-15 01:12:24', 'class 12, class 13', 'Registered', 'ABC Limited', NULL, NULL, NULL),
(6, '05', '2003-06-19', 'Asif', 'Series marks', 'Individual', '6000', 'sharad', 'Body-incorporate including Private Limited/limited Company', 'Asif Khan', 'T6', 'Word mark', 'English', 't6 tradmark', 'no-image.png', 'NA', '1', 'NA', '2003-06-19', 'India', '12', 'this is demo...', 'Asif Kahn', 'A.K', 'ag', 'no-image.png', '0', '2019-06-03 05:32:33', '2019-08-15 08:55:11', 'class 10,Class 11', 'Marked for Exam', 'AaraWebSolution', NULL, NULL, NULL),
(7, '2356', '2016-08-19', 'Kalam', 'Standard Trademark', 'Individual', '6000', 'sharad', 'Partnership Firm', 'Asif Khan', 'T7', 'Word mark', 'English', 't7 trademark', 'no-image.png', NULL, '1', 'This is demo mark.....', '2020-08-19', 'India', '12', 'This is statements.....', 'asif Khan', 'asif', 'Laxcare', 'no-image.png', '0', '2019-08-15 12:46:57', '2019-08-16 17:47:56', 'class 10,Class 11', 'Active Trademark', 'AaraWebSolution', NULL, NULL, NULL),
(8, '2567', '2019-08-19', 'Mukesh', 'Standard Trademark', 'StartUp', '2000', 'sharad', 'Limited Liability Partnership', 'Asif Khan', 'T8', 'Colour', 'Hindi', 'T8 Is a Trademark', 'no-image.png', NULL, '2', 'This is demo mark.....', '2020-08-19', 'India', '24', 'This is testing of tradmark....', 'asif Khan', 'asif', 'Laxcare', 'no-image.png', '0', '2019-08-17 11:03:29', '2019-08-17 11:04:19', 'class 36,class 37', 'Accepted and Advertised', 'AaraWebSolution', NULL, NULL, NULL),
(9, '123456', '2020-08-19', 'Asif', 'Standard Trademark', 'Enterprise', '5000', 'sharad', 'Body-incorporate including Private Limited/limited Company', 'Asif Khan', 'lawyers Database', 'Word mark', 'English', 'Name for lawyers database portal.', 'no-image.png', NULL, '2', NULL, '2020-08-19', NULL, NULL, NULL, NULL, 'Kalam', NULL, 'Invoice _ Indeed.com.pdf', '0', '2019-08-19 20:56:40', '2019-08-21 00:17:55', 'class 35,class 36', 'Marked for Exam', 'AaraWebSolution', NULL, NULL, NULL),
(10, NULL, '2027-08-20', 'Mukesh', 'Standard Trademark', 'Enterprise', NULL, 'sharad', 'Limited Liability Partnership', 'Asif Khan', 'T15', 'Word mark', 'Hindi', NULL, 'no-image.png', NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no-image.png', '0', '2019-08-26 16:01:26', '2019-08-26 18:29:39', 'class  29,class 10', 'Due for Hearing', 'ABC Limited', NULL, NULL, 'Online'),
(11, NULL, '2029-08-19', 'Mukesh', 'Standard Trademark', 'StartUp', NULL, 'sharad', 'Partnership Firm', '0', 'This is demo Tradmark', 'Word mark', 'English', NULL, 'no-image.png', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no-image.png', '0', '2019-08-29 06:58:31', '2019-09-07 13:44:00', 'class  29,class 10', 'Active Trademark', 'ABC Limited', NULL, NULL, 'Online'),
(12, NULL, '2004-09-19', 'Mukesh', 'Standard Trademark', 'StartUp', NULL, 'sharad', 'Statutory Organization', 'Asif Khan', 'Newentren', 'Device Mark', 'other', NULL, 'no-image.png', NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no-image.png', '0', '2019-09-04 17:52:55', '2019-09-07 14:40:30', 'class 35,class 36', 'Marked for Exam', 'AaraWebSolution', NULL, NULL, 'Offline');

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE `agents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_mail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registration_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agents`
--

INSERT INTO `agents` (`id`, `name`, `address`, `e_mail`, `mobile_number`, `registration_no`, `created_at`, `updated_at`) VALUES
(1, 'Asif Khan', 'Pune', 'asif85@gmail.com', '702157548', '87', '2019-05-25 05:29:09', '2019-08-17 10:47:55');

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE `applicants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `entity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_for_service` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trading_as` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicant_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_mail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`id`, `entity`, `address_for_service`, `trading_as`, `country`, `state`, `mobile_number`, `applicant_name`, `address`, `s_state`, `s_country`, `e_mail`, `created_at`, `updated_at`) VALUES
(1, 'AaraWebSolution', 'Pune', 'trade', 'india', 'Maharashtra', '9999999999', 'sharad', 'pune', 'Maharashtra', 'india', 'sharadt@gmail.com', '2019-05-25 03:26:41', '2019-08-17 10:54:05');

-- --------------------------------------------------------

--
-- Table structure for table `application_fields`
--

CREATE TABLE `application_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `application_field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `application_fields`
--

INSERT INTO `application_fields` (`id`, `application_field`, `created_at`, `updated_at`) VALUES
(3, 'Individual', '2019-05-23 07:15:55', '2019-05-24 23:50:38'),
(4, 'StartUp', '2019-08-17 10:37:25', '2019-08-17 10:37:25'),
(5, 'Enterprise', '2019-08-17 10:38:19', '2019-08-17 10:52:00');

-- --------------------------------------------------------

--
-- Table structure for table `category_marks`
--

CREATE TABLE `category_marks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_of_mark` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_marks`
--

INSERT INTO `category_marks` (`id`, `category_of_mark`, `created_at`, `updated_at`) VALUES
(1, 'Word mark', '2019-05-25 01:01:26', '2019-05-25 01:02:04'),
(2, 'Device Mark', '2019-08-17 10:44:19', '2019-08-17 10:44:19'),
(3, 'Colour', '2019-08-17 10:45:11', '2019-08-17 10:53:18'),
(4, 'Three Dimensional trademark', '2019-08-17 10:45:25', '2019-08-17 10:45:25'),
(5, 'Sound', '2019-08-17 10:45:38', '2019-08-17 10:45:38');

-- --------------------------------------------------------

--
-- Table structure for table `class_good_services`
--

CREATE TABLE `class_good_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `class_of_good_services` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `class_good_services`
--

INSERT INTO `class_good_services` (`id`, `class_of_good_services`, `created_at`, `updated_at`) VALUES
(1, 'Class Of Goods', '2019-05-25 01:21:43', '2019-05-25 01:21:43'),
(2, 'Class Of Services', '2019-05-25 01:30:57', '2019-05-25 01:32:06');

-- --------------------------------------------------------

--
-- Table structure for table `designation_lists`
--

CREATE TABLE `designation_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designation_lists`
--

INSERT INTO `designation_lists` (`id`, `designation`, `created_at`, `updated_at`) VALUES
(1, 'Developer', '2019-05-20 05:57:16', '2019-08-17 10:59:32');

-- --------------------------------------------------------

--
-- Table structure for table `entity_lists`
--

CREATE TABLE `entity_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_entity_id` int(11) NOT NULL,
  `entity_child` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `entity_lists`
--

INSERT INTO `entity_lists` (`id`, `parent_entity_id`, `entity_child`, `created_at`, `updated_at`) VALUES
(1, 1, 'ABC Limited', '2019-05-21 03:33:08', '2019-05-21 05:16:49'),
(3, 2, 'ABC Pvt Ltd', '2019-05-21 03:51:24', '2019-05-21 05:16:27'),
(4, 1, 'AaraWebSolution', '2019-05-25 02:48:07', '2019-05-25 02:48:07'),
(5, 4, 'AarCanbs', '2019-05-25 02:48:33', '2019-05-25 02:48:33'),
(6, 2, 'sss', '2019-08-13 03:32:03', '2019-08-17 10:55:25');

-- --------------------------------------------------------

--
-- Table structure for table `entity_mappings`
--

CREATE TABLE `entity_mappings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `entity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `function` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `entity_mappings`
--

INSERT INTO `entity_mappings` (`id`, `entity`, `unit`, `function`, `created_at`, `updated_at`) VALUES
(3, 'ABC Limited', 'Pune', 'IT', '2019-05-23 00:56:28', '2019-05-23 00:56:28'),
(4, 'ABC Pvt Ltd', 'Pune', 'Legal', '2019-05-23 00:56:39', '2019-05-23 00:56:39'),
(5, 'ABC Pvt Ltd', 'Pune', 'Finance', '2019-05-23 00:56:53', '2019-05-23 00:56:53'),
(6, 'AaraWebSolution', 'Pune', 'IT,Legal,Finance', '2019-06-17 12:12:26', '2019-06-17 12:12:26');

-- --------------------------------------------------------

--
-- Table structure for table `function_lists`
--

CREATE TABLE `function_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `function` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `function_lists`
--

INSERT INTO `function_lists` (`id`, `function`, `created_at`, `updated_at`) VALUES
(1, 'IT', '2019-05-20 03:09:31', '2019-05-20 05:27:04'),
(2, 'Legal', '2019-05-20 03:23:55', '2019-05-20 03:23:55'),
(3, 'Finance', '2019-05-20 03:24:06', '2019-08-17 10:57:47');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `language`, `created_at`, `updated_at`) VALUES
(1, 'English', '2019-05-25 01:58:25', '2019-05-25 01:58:35'),
(2, 'Hindi', '2019-08-17 10:46:52', '2019-08-17 10:53:43'),
(3, 'other', '2019-08-17 10:47:07', '2019-08-17 10:47:07');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_05_13_104835_create_add_tradmarks_table', 1),
(5, '2019_05_20_070006_create_units_table', 2),
(9, '2019_05_20_083549_create_function_lists_table', 3),
(10, '2019_05_20_111858_create_designation_lists_table', 4),
(12, '2019_05_21_084841_create_parent_entities_table', 6),
(13, '2019_05_20_192242_create_entity_lists_table', 7),
(14, '2019_05_23_055737_create_entity_mappings_table', 8),
(15, '2019_05_23_104007_create_nature_applications_table', 9),
(16, '2019_05_23_122044_create_application_fields_table', 10),
(17, '2019_05_25_053128_create_nature_applicants_table', 11),
(18, '2019_05_25_060131_create_nature_agents_table', 12),
(19, '2019_05_25_062144_create_category_marks_table', 13),
(20, '2019_05_25_064124_create_class_good_services_table', 14),
(21, '2019_05_25_071549_create_languages_table', 15),
(22, '2019_05_25_084109_create_applicants_table', 16),
(23, '2019_05_25_102759_create_agents_table', 17),
(25, '2014_10_12_000000_create_users_table', 19),
(26, '2019_05_25_115828_create_sub_class_goods_services_table', 20),
(27, '2019_06_09_080304_create_trademark_statuses_table', 21),
(28, '2019_06_16_072453_create_update_statuses_table', 22),
(29, '2019_07_22_064415_create_roles_table', 23),
(30, '2019_07_28_165931_create_action_items_table', 24);

-- --------------------------------------------------------

--
-- Table structure for table `nature_agents`
--

CREATE TABLE `nature_agents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nature_of_agent` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nature_agents`
--

INSERT INTO `nature_agents` (`id`, `nature_of_agent`, `created_at`, `updated_at`) VALUES
(1, 'Registered Trade Marks Agent', '2019-05-25 00:43:02', '2019-05-25 01:33:06'),
(2, 'Advocate', '2019-08-17 10:43:04', '2019-08-17 10:52:53'),
(3, 'Constituted Attorney', '2019-08-17 10:43:34', '2019-08-17 10:43:34');

-- --------------------------------------------------------

--
-- Table structure for table `nature_applicants`
--

CREATE TABLE `nature_applicants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nature_of_applicant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nature_applicants`
--

INSERT INTO `nature_applicants` (`id`, `nature_of_applicant`, `created_at`, `updated_at`) VALUES
(1, 'Partnership Firm', '2019-05-25 00:08:43', '2019-05-25 00:21:05'),
(2, 'Body-incorporate including Private Limited/limited Company', '2019-05-25 00:11:57', '2019-05-25 00:11:57'),
(3, 'Limited Liability Partnership', '2019-08-17 10:40:49', '2019-08-17 10:40:49'),
(4, 'Society', '2019-08-17 10:41:00', '2019-08-17 10:41:00'),
(5, 'Trust', '2019-08-17 10:41:10', '2019-08-17 10:41:10'),
(6, 'Government Department', '2019-08-17 10:41:20', '2019-08-17 10:41:20'),
(7, 'Statutory Organization', '2019-08-17 10:41:31', '2019-08-17 10:52:24'),
(8, 'Association Of Persons', '2019-08-17 10:41:46', '2019-08-17 10:41:46');

-- --------------------------------------------------------

--
-- Table structure for table `nature_applications`
--

CREATE TABLE `nature_applications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nature_of_application` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nature_applications`
--

INSERT INTO `nature_applications` (`id`, `nature_of_application`, `created_at`, `updated_at`) VALUES
(1, 'Series marks', '2019-05-23 06:33:17', '2019-05-23 06:47:24'),
(2, 'Standard Trademark', '2019-05-23 06:36:49', '2019-05-23 06:36:49'),
(3, 'Collective Mark', '2019-08-17 10:35:42', '2019-08-17 10:35:42'),
(4, 'Certification Mark', '2019-08-17 10:36:41', '2019-08-17 10:51:39');

-- --------------------------------------------------------

--
-- Table structure for table `parent_entities`
--

CREATE TABLE `parent_entities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_entity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parent_entities`
--

INSERT INTO `parent_entities` (`id`, `parent_entity`, `created_at`, `updated_at`) VALUES
(1, 'NA', NULL, NULL),
(2, 'ABC Limited', '2019-05-21 03:34:51', '2019-05-21 03:34:51'),
(3, 'ABC Pvt Ltd', '2019-05-21 03:51:24', '2019-05-21 03:51:24'),
(4, 'AaraWebSolution', '2019-05-25 02:48:07', '2019-05-25 02:48:07'),
(5, 'AarCanbs', '2019-05-25 02:48:33', '2019-05-25 02:48:33'),
(6, 'sss', '2019-08-13 03:32:03', '2019-08-13 03:32:03');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('asif85.in@gmail.com', '$2y$10$uOnHxGn5NJeirgAuGmhI3ONqiS1aDHm5FwZT7C7aZuYAvgiDgAMZK', '2019-08-17 02:36:19');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'Internal Associate', NULL, NULL),
(2, 'Manager', NULL, NULL),
(3, 'User', NULL, NULL),
(4, 'Administrator', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_class_goods_services`
--

CREATE TABLE `sub_class_goods_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `class_good_services_id` int(11) NOT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_class_goods_services`
--

INSERT INTO `sub_class_goods_services` (`id`, `class_good_services_id`, `class`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'class  29', 'Meat, fish, poultry, and game; meat extracts; preserved, dried and cooked fruits and vegetables; jellies, jams; eggs, milk and milk products; edible oils and fats; salad dressings; preserves.', '2019-06-02 23:44:35', '2019-06-02 23:44:35'),
(2, 1, 'class 10', 'Surgical, medical, dental and veterinary apparatus and instruments, artificial limbs, eyes and teeth; orthopedic articles; suture materials', '2019-06-02 23:47:28', '2019-06-02 23:47:28'),
(3, 2, 'class 35', 'Advertising and Business This class includes mainly services rendered by persons or organizations principally with the object of: help in the working or management of a commercial undertaking or, help in the management of the business affairs or commercial functions of an industrial or commercial enterprise, as well as services rendered by ; advertising establishments primarily undertaking communications to the public, declarations or announcements by all means of diffusion and concerning all kinds of goods or services.', '2019-06-02 23:47:59', '2019-06-02 23:48:26'),
(4, 2, 'class 36', 'Insurance and Financial This class includes mainly services rendered in financial and monetary affairs and services rendered in relations to insurance contracts of all kinds.', '2019-06-02 23:49:17', '2019-06-02 23:49:17'),
(5, 1, 'Class 11', 'Apparatus for lighting, heating, steam generating, cooking, refrigerating, drying, ventilating, water supply and sanitary purposes.', '2019-06-03 04:38:28', '2019-06-03 04:38:28'),
(6, 2, 'class 37', 'Construction and Repair This class includes mainly services rendered by contractors or subcontractors in the construction or making of permanent buildings, as well as services rendered by persons or organizations engaged in the restoration of objects to their original condition or in their preservation without altering their physical or chemical properties.', '2019-06-03 04:38:54', '2019-08-17 10:50:50');

-- --------------------------------------------------------

--
-- Table structure for table `trademark_statuses`
--

CREATE TABLE `trademark_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trademark_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trademark_statuses`
--

INSERT INTO `trademark_statuses` (`id`, `trademark_status`, `created_at`, `updated_at`) VALUES
(1, 'Active Trademark', NULL, NULL),
(2, 'Marked for Exam', NULL, NULL),
(3, 'Reply to Examination Report', NULL, NULL),
(4, 'Due for Hearing', NULL, NULL),
(5, 'Accepted and Advertised', NULL, NULL),
(6, 'Opposition Notice Received', NULL, NULL),
(7, 'Reply to Opposition ', NULL, NULL),
(8, 'Hearing for Opposition', NULL, NULL),
(9, 'Registered ', NULL, NULL),
(10, 'Rejected by TM Office', NULL, NULL),
(11, 'Decision Pending', NULL, NULL),
(12, 'Abandoned', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `unit`, `created_at`, `updated_at`) VALUES
(1, 'Pune', '2019-05-20 01:55:49', '2019-05-20 02:16:07'),
(2, 'Navi Mumbai', '2019-06-17 12:33:16', '2019-08-17 10:56:39');

-- --------------------------------------------------------

--
-- Table structure for table `update_statuses`
--

CREATE TABLE `update_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tradmark_id` int(11) NOT NULL,
  `tradmark_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stage_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opponent_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opponent_tm_journal_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `update_statuses`
--

INSERT INTO `update_statuses` (`id`, `tradmark_id`, `tradmark_name`, `document`, `comment`, `status`, `stage_date`, `opponent_name`, `opponent_tm_journal_date`, `address`, `class`, `mark`, `created_at`, `updated_at`, `date`) VALUES
(8, 6, 'T6', 'Chrysanthemum.jpg,Desert.jpg,Hydrangeas.jpg,Jellyfish.jpg', 'This is testing comments Updated....', 'New Trademark', '16/06/19', NULL, NULL, NULL, NULL, NULL, '2019-06-16 10:10:12', '2019-06-17 07:31:48', '0000-00-00'),
(9, 6, 'T6', 'Koala.jpg,Lighthouse.jpg', 'This another Comments....', 'Under Examination', '18/06/19', NULL, NULL, NULL, NULL, NULL, '2019-06-16 13:14:34', '2019-06-16 13:14:34', '0000-00-00'),
(10, 6, 'T6', 'Koala.jpg,Lighthouse.jpg', 'This is testing Comments....', 'Appealed/Opposed', '17/06/19', 'Saif Ali', '18/10/19', 'Sanpada', 'class 10', 'Word Mark', '2019-06-16 15:14:57', '2019-06-16 15:14:57', '0000-00-00'),
(11, 1, 'T1', 'vlcsnap-2019-03-21-15h17m12s617.png,vlcsnap-2019-04-21-14h47m44s923.png', 'This is testing Comments....', '0', '26/06/19', NULL, NULL, NULL, NULL, NULL, '2019-06-25 05:29:26', '2019-07-10 03:48:19', '0000-00-00'),
(12, 6, 'T6', 'vlcsnap-2019-03-21-15h17m12s617.png', 'this testing', 'Rejected', '25/06/19', NULL, NULL, NULL, NULL, NULL, '2019-06-25 06:44:30', '2019-06-25 06:44:30', '0000-00-00'),
(13, 3, 'T3', 'vlcsnap-2019-03-21-15h17m12s617.png,vlcsnap-2019-04-21-14h47m44s923.png', 'This is demo testing....', 'New Trademark', '25/06/19', NULL, NULL, NULL, NULL, NULL, '2019-06-25 06:47:50', '2019-06-25 06:47:50', '0000-00-00'),
(14, 3, 'T3', 'vlcsnap-2019-03-21-15h17m12s617.png,vlcsnap-2019-04-21-14h47m44s923.png', 'This is Comments another....', 'Under Examination', '28/06/19', NULL, NULL, NULL, NULL, NULL, '2019-06-28 05:55:21', '2019-06-28 05:55:21', '0000-00-00'),
(15, 2, 'T2', 'wall png toilet.png', 'This is testing comments....', 'Under Examination', '04/07/19', NULL, NULL, NULL, NULL, NULL, '2019-07-04 00:46:33', '2019-07-04 00:46:33', '0000-00-00'),
(16, 4, 'T4', 'vlcsnap-2019-03-21-15h17m12s617.png', 'This is another comments....', 'Awarded', '04/07/19', NULL, NULL, NULL, NULL, NULL, '2019-07-04 00:47:16', '2019-07-04 00:47:16', '0000-00-00'),
(17, 4, 'T4', 'vlcsnap-2019-03-21-15h17m12s617.png', 'This is another comments areas.....', 'Rejected', '15/07/19', NULL, NULL, NULL, NULL, NULL, '2019-07-15 00:50:58', '2019-07-15 00:50:58', '0000-00-00'),
(18, 4, 'T4', 'vlcsnap-2019-03-21-15h17m12s617.png', 'This is another comments.....', 'New Trademark', '10/07/19', NULL, NULL, NULL, NULL, NULL, '2019-07-15 00:54:36', '2019-07-15 00:54:36', '0000-00-00'),
(19, 4, 'T4', 'vlcsnap-2019-03-21-15h17m12s617.png,vlcsnap-2019-04-21-14h47m44s923.png', 'This is another comments....', 'New Trademark', '05/07/19', NULL, NULL, NULL, NULL, NULL, '2019-07-15 00:56:25', '2019-07-15 00:56:25', '0000-00-00'),
(20, 4, 'T4', 'vlcsnap-2019-03-21-15h17m12s617.png,vlcsnap-2019-04-21-14h47m44s923.png', 'This is another comments....', 'New Trademark', '05/07/19', NULL, NULL, NULL, NULL, NULL, '2019-07-15 00:57:11', '2019-07-15 00:57:11', '0000-00-00'),
(21, 4, 'T4', 'vlcsnap-2019-03-21-15h17m12s617.png,vlcsnap-2019-04-21-14h47m44s923.png', 'This is dgf', 'Rejected', '4/5/19', NULL, NULL, NULL, NULL, NULL, '2019-07-15 00:59:00', '2019-07-15 01:01:37', '0000-00-00'),
(22, 1, 'T1', 'vlcsnap-2019-03-21-15h17m12s617.png,vlcsnap-2019-04-21-14h47m44s923.png', 'This another testings....', 'Under Examination', '18/06/19', NULL, NULL, NULL, NULL, NULL, '2019-07-15 01:02:41', '2019-07-15 01:04:54', '0000-00-00'),
(23, 2, 'T2', 'vlcsnap-2019-03-21-15h17m12s617.png', 'This comments.....', '0', NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-15 01:10:17', '2019-08-29 07:53:12', '2019-08-30'),
(24, 3, 'T3', 'vlcsnap-2019-03-21-15h17m12s617.png', 'This is comments....', 'Rejected', '07/07/19', NULL, NULL, NULL, NULL, NULL, '2019-07-15 01:11:32', '2019-07-15 01:11:32', '0000-00-00'),
(25, 4, 'T4', 'vlcsnap-2019-03-21-15h17m12s617.png,vlcsnap-2019-04-21-14h47m44s923.png', 'This is comments.....', 'Awarded', '07/07/19', NULL, NULL, NULL, NULL, NULL, '2019-07-15 01:12:24', '2019-07-15 01:12:24', '0000-00-00'),
(26, 6, 'T6', 'vlcsnap-2019-03-21-15h17m12s617.png,vlcsnap-2019-04-21-14h47m44s923.png', 'this is comments Updated one More Time....', '0', NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-15 01:13:22', '2019-07-27 09:29:26', '0000-00-00'),
(27, 3, 'T3', '---', 'This Under Hearing Process....', 'Under Hearing', '2019-07-25', NULL, NULL, NULL, NULL, NULL, '2019-07-27 07:32:21', '2019-07-27 07:32:21', '0000-00-00'),
(28, 6, 'T6', '---', 'This is another comments...', 'Marked for Exam', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-15 08:55:11', '2019-08-15 09:01:31', '2019-08-17'),
(29, 7, 'T7', '---', 'This is updated...', 'New Trademark', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-16 17:47:56', '2019-08-16 17:47:56', '2019-08-19'),
(30, 8, 'T8', '---', 'This is testing....', 'Accepted and Advertised', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-17 11:04:19', '2019-08-17 11:04:19', '2019-08-19'),
(31, 9, 'lawyers Database', '---', 'Marked for examed', 'Marked for Exam', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-19 21:02:50', '2019-08-19 21:02:50', '2019-08-19'),
(32, 10, 'T15', '---', 'This updated comments....', 'Due for Hearing', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-26 16:09:52', '2019-08-26 16:09:52', '2019-08-27'),
(33, 12, 'Newentren', 'vlcsnap-2019-03-21-15h17m12s617.png,vlcsnap-2019-04-21-14h47m44s923.png', 'This is demo', 'Marked for Exam', NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-07 14:40:30', '2019-09-07 14:40:30', '2019-09-08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enttity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `function` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `mobile_no`, `email`, `user_name`, `password`, `enttity`, `unit`, `function`, `employee_id`, `designation`, `role`, `address`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(8, 'Asif', 'Khan', '7021575748', 'asif87.in@gmail.com', 'asif87', '$2y$10$c.ujJUrqnGbSow.zIi9eeemi1ATLAeHIXSKfx5ImjH7I9Ib4bm9Ee', 'AarCanbs', 'Pune', 'IT', '0015', 'Developer', 'User', 'Vashi Navi Mumbai', '0', NULL, '2019-05-28 01:20:00', '2019-05-28 03:52:10'),
(9, 'saif', 'Khan', '7021575748', 'asif87.in@gmail.com', 'asif87', '$2y$10$M1MZqbYbjdloHUb9o73rCOr/AucqR0LcqnW3XsQhL1rU1uZd2Dcby', 'AaraWebSolution', 'Pune', 'IT', '0015', 'Developer', 'Admin', 'vashi Navi Mumbai....', '0', NULL, '2019-05-28 01:23:19', '2019-05-28 03:00:58'),
(11, 'Kalam', 'sheikh', '7021575745', 'kashaikh611@gmail.com', 'kalam85', '$2y$10$QZrxehjVAO4MNDpHWjdeiOKyMXVaEIOyHau38PtkikZ6Nph/h2/5m', 'AaraWebSolution', 'Navi Mumbai', 'IT', '014', 'Developer', 'Internal Associate', 'Cheeta Camp Trombay....', '0', NULL, '2019-07-22 01:33:11', '2019-08-17 11:23:53'),
(12, 'Mukesh', 'Sir', '2223334445', 'mukeshgadge@gmail.com', 'mukesh04', '$2y$10$7iepCht5FookrIvb.Nq19uL6DPE1RkPsfdgKa0ukCsOP2McCKM9Pm', 'ABC Pvt Ltd', 'Pune', 'Legal', '016', 'Developer', 'Administrator', 'Navi Mumbai....', '0', NULL, '2019-07-22 01:43:42', '2019-08-17 10:32:54'),
(13, 'Asif', 'Khan', '7021575746', 'asif85.in@gmail.com', 'asif85', '$2y$10$X2L.3SDnVI2XptZjAX5w6O7NssjfnxDzWzqs8ZyBq4BNdzT9ObEOC', 'AaraWebSolution', 'Navi Mumbai', 'IT', '03', 'Developer', 'Administrator', 'Vashi, Navi Mumbai....', '0', 'uxMgezeXBgfX4SIJmR0xCrmy1JR9DLpRZ9ikSFKANJws3Ufrox0Y0GTT7rYc', '2019-08-03 03:55:38', '2019-08-15 16:59:04'),
(14, 'Asif', 'Khan', '7021575749', 'asif.in67@gmail.com', 'asif95', '$2y$10$YMeeYX8CfMd7YmFh0Mh6vO0sEg84gSzoWFgOMa.C/RZf1IScv1W92', 'AaraWebSolution', 'Navi Mumbai', 'IT', '003', 'Developer', 'Administrator', 'Vashi Navi Mumbai.....', '0', 'uTnSxZRLh32PGCHZIBDwSWuLNpyomw6rqargNgurUyCOiimjQthAiHz9nJNO', '2019-08-17 10:31:23', '2019-08-17 11:15:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action_items`
--
ALTER TABLE `action_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `add_tradmarks`
--
ALTER TABLE `add_tradmarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applicants`
--
ALTER TABLE `applicants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `application_fields`
--
ALTER TABLE `application_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_marks`
--
ALTER TABLE `category_marks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_good_services`
--
ALTER TABLE `class_good_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designation_lists`
--
ALTER TABLE `designation_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entity_lists`
--
ALTER TABLE `entity_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entity_mappings`
--
ALTER TABLE `entity_mappings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `function_lists`
--
ALTER TABLE `function_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nature_agents`
--
ALTER TABLE `nature_agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nature_applicants`
--
ALTER TABLE `nature_applicants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nature_applications`
--
ALTER TABLE `nature_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parent_entities`
--
ALTER TABLE `parent_entities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_class_goods_services`
--
ALTER TABLE `sub_class_goods_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trademark_statuses`
--
ALTER TABLE `trademark_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `update_statuses`
--
ALTER TABLE `update_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `action_items`
--
ALTER TABLE `action_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `add_tradmarks`
--
ALTER TABLE `add_tradmarks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `agents`
--
ALTER TABLE `agents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `applicants`
--
ALTER TABLE `applicants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `application_fields`
--
ALTER TABLE `application_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `category_marks`
--
ALTER TABLE `category_marks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `class_good_services`
--
ALTER TABLE `class_good_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `designation_lists`
--
ALTER TABLE `designation_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `entity_lists`
--
ALTER TABLE `entity_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `entity_mappings`
--
ALTER TABLE `entity_mappings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `function_lists`
--
ALTER TABLE `function_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `nature_agents`
--
ALTER TABLE `nature_agents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nature_applicants`
--
ALTER TABLE `nature_applicants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `nature_applications`
--
ALTER TABLE `nature_applications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `parent_entities`
--
ALTER TABLE `parent_entities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sub_class_goods_services`
--
ALTER TABLE `sub_class_goods_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `trademark_statuses`
--
ALTER TABLE `trademark_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `update_statuses`
--
ALTER TABLE `update_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
