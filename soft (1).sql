-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 27, 2020 at 01:19 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `soft`
--

-- --------------------------------------------------------

--
-- Table structure for table `action_items`
--

CREATE TABLE `action_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `license` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license_of_sub` date DEFAULT NULL,
  `term_of_license` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `reminder_two` date DEFAULT NULL,
  `license_document` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tradmark_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `action_items`
--

INSERT INTO `action_items` (`id`, `license`, `license_of_sub`, `term_of_license`, `expiry_date`, `reminder_two`, `license_document`, `tradmark_id`, `created_at`, `updated_at`) VALUES
(24, 'This is demo Licence updated', '2019-09-16', 'Demo', '2019-09-21', '2019-09-21', 'OoPdfFormExample.pdf', 13, '2019-09-15 11:12:55', '2019-09-15 11:27:01'),
(25, 'Namo', '2020-01-12', '10', '2020-01-12', '2020-01-29', 'Tradmark Reports export (1).pdf', 18, '2020-01-12 17:50:07', '2020-01-12 17:50:07');

-- --------------------------------------------------------

--
-- Table structure for table `advocates`
--

CREATE TABLE `advocates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `law_firm` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `advocates`
--

INSERT INTO `advocates` (`id`, `country`, `state`, `city`, `law_firm`, `name`, `cn`, `email`, `address`, `area`, `created_at`, `updated_at`) VALUES
(1, 'India', 'Maharashtra', 'Mumbai', 'XYZ pvt ltd', 'Kalam shaikh', '9594785319', 'kashaikh611@gmail.com', 'F-SECTOR, W2-LINE,ROOM NO.11,CHEETA CAMP,TROMBAY', 'Legal Advise', '2020-02-11 07:11:55', '2020-02-11 07:11:55');

-- --------------------------------------------------------

--
-- Table structure for table `agencies`
--

CREATE TABLE `agencies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `person` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agencies`
--

INSERT INTO `agencies` (`id`, `name`, `email`, `contact`, `person`, `created_at`, `updated_at`) VALUES
(1, 'Brand Defence Consulting Pvt Ltd', 'mukesh.gadge@lexcareglobal.com', '09922113707', 'Mukesh Gadge', '2020-01-11 05:36:02', '2020-01-11 16:06:14'),
(2, 'Aara Protection Pvt Ltd', 'kashaikh611@gmail.com', '9594785319', 'Abdul Kalam Shaikh', '2020-01-11 09:44:43', '2020-01-11 16:05:37');

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE `agents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_mail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registration_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agents`
--

INSERT INTO `agents` (`id`, `name`, `address`, `e_mail`, `mobile_number`, `registration_no`, `created_at`, `updated_at`) VALUES
(1, 'Abdul Kalam Shaikh', 'F', 'kashaikh611@gmail.com', '09594785319', '215977515', '2020-08-24 09:13:03', '2020-08-24 09:13:03');

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE `applicants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `entity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_for_service` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trading_as` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicant_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_mail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`id`, `entity`, `address_for_service`, `trading_as`, `country`, `state`, `mobile_number`, `applicant_name`, `address`, `s_state`, `s_country`, `e_mail`, `created_at`, `updated_at`) VALUES
(1, 'ABC Limited', 'Mumbai', 'trade', 'India', 'Maharashtra', '9999999999', 'Shreedhar', 'pune', 'Maharashtra', 'india', 'shredhar@gmail.com', '2019-05-25 03:26:41', '2020-01-12 17:28:50');

-- --------------------------------------------------------

--
-- Table structure for table `application_fields`
--

CREATE TABLE `application_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `application_field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `application_fields`
--

INSERT INTO `application_fields` (`id`, `application_field`, `created_at`, `updated_at`) VALUES
(3, 'Individual', '2019-05-23 07:15:55', '2019-05-24 23:50:38'),
(4, 'StartUp', '2019-08-17 10:37:25', '2019-08-17 10:37:25'),
(5, 'Enterprise / SME', '2019-08-17 10:38:19', '2020-02-27 15:33:24'),
(6, 'Others', '2020-02-27 15:33:35', '2020-02-27 15:33:35');

-- --------------------------------------------------------

--
-- Table structure for table `area_exps`
--

CREATE TABLE `area_exps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `area_exps`
--

INSERT INTO `area_exps` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Legal Advise', '2020-02-11 05:09:02', '2020-02-11 05:09:02'),
(2, 'XYZ pvt ltds', '2020-02-11 05:19:44', '2020-02-19 05:42:52');

-- --------------------------------------------------------

--
-- Table structure for table `category_applicants`
--

CREATE TABLE `category_applicants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_applicants`
--

INSERT INTO `category_applicants` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Small entity', '2019-12-15 21:45:11', '2019-12-15 21:45:11');

-- --------------------------------------------------------

--
-- Table structure for table `category_marks`
--

CREATE TABLE `category_marks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_of_mark` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_marks`
--

INSERT INTO `category_marks` (`id`, `category_of_mark`, `created_at`, `updated_at`) VALUES
(1, 'Word mark', '2019-05-25 01:01:26', '2019-05-25 01:02:04'),
(2, 'Device Mark', '2019-08-17 10:44:19', '2019-08-17 10:44:19'),
(3, 'Colour', '2019-08-17 10:45:11', '2019-08-17 10:53:18'),
(4, 'Three Dimensional trademark', '2019-08-17 10:45:25', '2019-08-17 10:45:25'),
(5, 'Sound', '2019-08-17 10:45:38', '2019-08-17 10:45:38');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `class_good_services`
--

CREATE TABLE `class_good_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `class_of_good_services` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `class_good_services`
--

INSERT INTO `class_good_services` (`id`, `class_of_good_services`, `created_at`, `updated_at`) VALUES
(1, 'Class Of Goods', '2019-05-25 01:21:43', '2019-05-25 01:21:43'),
(2, 'Class Of Services', '2019-05-25 01:30:57', '2019-05-25 01:32:06');

-- --------------------------------------------------------

--
-- Table structure for table `completions`
--

CREATE TABLE `completions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `liti_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dis_date` date NOT NULL,
  `last_date` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `synop` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `court` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `completions`
--

INSERT INTO `completions` (`id`, `liti_id`, `status`, `dis_date`, `last_date`, `comments`, `doc`, `synop`, `court`, `created_at`, `updated_at`) VALUES
(1, 1, 'In Favour', '2020-02-12', NULL, 'Nothing', 'BALA AND UJDA CHAMAN- Bald Cap, Bollywood Creativity Taking a Nap..pdf', 'law', 'HIGH COURT', '2020-02-13 05:48:23', '2020-02-13 05:48:23'),
(2, 3, 'Settled', '2020-08-18', '1970-01-01', 'No comments', 'Litigation Report (2).pdf', 'All matter are closed', 'DISTRICT COURT', '2020-08-23 19:29:55', '2020-08-23 19:29:55');

-- --------------------------------------------------------

--
-- Table structure for table `copyrightns`
--

CREATE TABLE `copyrightns` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gov` decimal(10,2) NOT NULL,
  `agent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `diary` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dof` date NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `nofa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titlew` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `lang_id` bigint(20) UNSIGNED DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `user_aff` text COLLATE utf8mb4_unicode_ci,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copy_app` text COLLATE utf8mb4_unicode_ci,
  `roc` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `ssp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `app_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `copyrightns`
--

INSERT INTO `copyrightns` (`id`, `gov`, `agent_id`, `diary`, `dof`, `user_id`, `nofa`, `titlew`, `image`, `description`, `lang_id`, `remarks`, `user_aff`, `name`, `designation`, `copy_app`, `roc`, `status`, `sub_status`, `sp`, `ssp`, `app_status`, `created_at`, `updated_at`) VALUES
(1, '500.00', NULL, '3560', '2020-08-21', 23, 'Rajesh Malhotra', 'The book of Golden Era', 'IMG-20200814-WA0003.jpg', 'This is an epic book', 1, 'Tagline', 'IMG-20200814-WA0003.jpg', 'Kalam Shaikh', 'Data Entry Operator', 'Career in Food Industry.pdf', NULL, 'Active Application', 'Discrepancy stage', '1', '8', 1, '2020-08-21 16:30:00', '2020-08-23 13:22:52'),
(2, '500.00', NULL, '13508', '2020-08-11', 23, 'Manvinder Singh', 'The Ebook of Golden Era', 'IMG-20200814-WA0003.jpg', 'We are find some books in here', 1, 'Nothing', 'IMG-20200814-WA0003.jpg', 'Kalam Shaikh', 'Data Entry Operator', 'IMG-20200814-WA0003.jpg', NULL, 'Active Application', 'Mandatory waiting period', '1', '2', 1, '2020-08-21 16:41:25', '2020-08-23 10:08:49'),
(3, '500.00', NULL, '9690', '2020-08-03', 23, 'Kalam shaikh', 'Learning Video', 'IMG-20200814-WA0003.jpg', 'Educational Video about', 2, 'Youtube videos', 'Litigation Report (3).pdf', 'Kalam Shaikh', 'Data Entry Operator', 'Litigation Report (3).pdf', NULL, 'Active Application', 'Applied CR', '1', '1', 1, '2020-08-23 11:41:37', '2020-08-23 11:41:37'),
(4, '500.00', NULL, '6939', '2020-07-31', 23, 'Nelam Mathur', 'The autor life', 'IMG20200819173939.jpg', 'book regarding of author life', 1, 'tets', 'Litigation Report (2).pdf', 'Mukesh Gadge', 'MD', 'IMG-20200814-WA0003.jpg', NULL, 'Active Application', 'Scrutinization stage', '1', '7', 1, '2020-08-23 11:43:16', '2020-08-23 13:21:41'),
(5, '500.00', NULL, '9876', '2020-06-16', 23, 'Sujjal Basu', 'Himalaya Painting', 'IMG-20200814-WA0003.jpg', 'This is an Himalaya Painting', 1, 'None', 'Litigation Report (2).pdf', 'Rajesh Kumar', 'Software Developer', 'Litigation Report (3).pdf', NULL, 'Active Application', 'Appeal', '1', '6', 1, '2020-08-23 11:50:27', '2020-08-23 13:20:51'),
(6, '500.00', NULL, '34569', '2020-07-17', 23, 'Mukesh Gadge', 'Compliance Management Book', 'Litigation Report (3).pdf', 'Author life', 1, 'des', 'IMG20200819173939.jpg', 'Kalam Shaikh', 'Data Entry Operator', 'Litigation Report (3).pdf', NULL, 'Active Application', 'Hearing', '1', '5', 1, '2020-08-23 11:51:49', '2020-08-23 13:20:02'),
(7, '500.00', NULL, '98989', '2020-08-17', 23, 'Kalam shaikh', 'Kalam Shaikh', 'IMG-20200814-WA0003.jpg', 'nn', 1, 'Server And Domain', 'compressjpeg.zip', 'Kalam Shaikh', 'UI / UX', 'Litigation Report (3).pdf', NULL, 'Active Application', 'Reply to opposition submitted', '1', '4', 1, '2020-08-23 11:53:50', '2020-08-23 13:19:06'),
(8, '500.00', NULL, '2987', '2020-08-01', 23, 'Sajjad khaib', 'logo design', 'IMG20200819173939.jpg', 'array', 1, 'none', 'Litigation Report (3).pdf', 'Kalam Shaikh', 'management', 'IMG20200819173939.jpg', NULL, 'Active Application', 'Opposition', '1', '3', 1, '2020-08-23 12:05:07', '2020-08-23 13:18:14'),
(9, '500.00', NULL, '98975', '2020-08-02', 23, 'Manoj Vajpai', 'Sample Tag', 'IMG20200819173939.jpg', 'des', 1, 'None', 'Litigation Report (2).pdf', 'Manoj Kumar', 'Data Entry Operator', 'Litigation Report (3).pdf', NULL, 'Extension expired', '', '7', '', 1, '2020-08-23 12:06:50', '2020-08-23 13:15:23'),
(10, '500.00', NULL, '9691', '2020-08-19', 23, 'Arman Malik', 'Tere Bina Song', 'IMG-20200814-WA0003.jpg', 'song of the day', 2, 'none', 'Litigation Report (3).pdf', 'Kalam Shaikh', 'Data Entry Operator', 'IMG20200819173939.jpg', NULL, 'Renewed', '', '6', '', 1, '2020-08-23 12:16:21', '2020-08-23 13:14:49'),
(11, '500.00', NULL, '1234', '2020-08-07', 23, 'Sonu Nigam', 'Abhi Mujhme Kahi', 'IMG-20200814-WA0003.jpg', 'Noice', 2, 'none', 'Litigation Report (3).pdf', 'Kalam shaikh', 'UI / UX', 'Litigation Report (3).pdf', NULL, 'Expired', '', '5', '', 1, '2020-08-23 12:18:19', '2020-08-23 13:14:16'),
(12, '500.00', NULL, '6938', '2020-08-03', 23, 'Freed Shaikh', 'Education B', 'IMG20200819173634.jpg', 'Later', 1, 'noine', 'Litigation Report (2).pdf', 'Brijesh Gupta', 'Data Entry Operator', 'Litigation Report (2).pdf', '987128631258', 'Registered', '', '4', '', 1, '2020-08-23 12:37:16', '2020-08-23 13:13:43'),
(13, '500.00', NULL, '7890', '2020-07-29', 23, 'Abhinav Shelge', 'The ran movie script', 'IMG-20200814-WA0003.jpg', 'movie script', 2, 'none', 'Litigation Report (3).pdf', 'Raju Khan', 'Data Entry Operator', 'Litigation Report (2).pdf', NULL, 'Rejected', '', '3', '', 1, '2020-08-23 12:39:21', '2020-08-23 13:12:58'),
(14, '500.00', NULL, '5148', '2020-08-12', 23, 'Saurav Jadav', 'Packing Design', 'IMG-20200814-WA0003.jpg', 'this', 2, 'none', 'Litigation Report (3).pdf', 'Rajiv', 'Data Entry Operator', 'Litigation Report (2).pdf', NULL, 'Abandoned', '', '2', '', 1, '2020-08-23 12:41:05', '2020-08-23 13:04:11');

-- --------------------------------------------------------

--
-- Table structure for table `copyrights`
--

CREATE TABLE `copyrights` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gov` int(10) UNSIGNED NOT NULL,
  `agent` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dairy` int(11) NOT NULL,
  `dof` date NOT NULL,
  `user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nofa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `user_aff` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `copy_app` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sp` int(11) DEFAULT NULL,
  `ssp` int(11) DEFAULT NULL,
  `roc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `copyrights`
--

INSERT INTO `copyrights` (`id`, `gov`, `agent`, `dairy`, `dof`, `user`, `nofa`, `title`, `image`, `description`, `language`, `remarks`, `user_aff`, `name`, `designation`, `copy_app`, `status`, `sub_status`, `sp`, `ssp`, `roc`, `created_at`, `updated_at`) VALUES
(1, 500, 'kalam Shaikh', 56611, '2020-03-09', '22', 'Kalam shaikh', 'Title Place', 'ip18.jpg', 'none', 'English', 'none', 'ip14.jpg', 'Abdul Kalam Shaikh', 'MD', 'ip20.jpg', 'Active Application', 'Applied CR', 1, 1, NULL, '2020-03-09 09:54:26', '2020-03-09 09:54:26'),
(2, 500, 'Mukesh', 8910, '2020-03-09', '22', 'Kalam shaikh', 'kakla', 'ip17.jpg', 'none', 'Hindi', 'nine', 'ip11.jpg', 'Kalam shaikh', 'MD', 'ip15.jpg', 'Active Application', 'Applied CR', 1, 1, NULL, '2020-03-09 09:57:18', '2020-03-09 09:57:18');

-- --------------------------------------------------------

--
-- Table structure for table `copyright_lic_ins`
--

CREATE TABLE `copyright_lic_ins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `licin_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term` int(11) NOT NULL,
  `lic_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `copyright_lic_ins`
--

INSERT INTO `copyright_lic_ins` (`id`, `licin_id`, `licensor`, `licensee`, `term`, `lic_date`, `exp_date`, `consideration`, `rem_ip`, `ent_date`, `ent_date2`, `docs`, `reminder`, `comments`, `created_at`, `updated_at`) VALUES
(1, 2, 'kalam', 'Mukesh Gadge', 10, '2017-11-22', '2027-11-22', 'Periodic royalty fee', '2020-03-28', '2020-03-10', NULL, 'ip14.jpg,ip15.jpg', NULL, 'this is', '2020-03-11 07:05:27', '2020-03-11 07:05:27');

-- --------------------------------------------------------

--
-- Table structure for table `copyright_lic_outs`
--

CREATE TABLE `copyright_lic_outs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `licin_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term` int(11) NOT NULL,
  `lic_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `copyright_lic_outs`
--

INSERT INTO `copyright_lic_outs` (`id`, `licin_id`, `licensor`, `licensee`, `term`, `lic_date`, `exp_date`, `consideration`, `rem_ip`, `ent_date`, `ent_date2`, `docs`, `reminder`, `comments`, `created_at`, `updated_at`) VALUES
(1, 2, 'kalam', 'Mukesh Gadge', 10, '2016-09-28', '2026-09-28', 'One time royalty fee', NULL, NULL, '2020-03-31', 'ip17.jpg', NULL, 'this is', '2020-03-12 06:16:51', '2020-03-12 06:16:51');

-- --------------------------------------------------------

--
-- Table structure for table `copy_ass_ins`
--

CREATE TABLE `copy_ass_ins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `copy_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term` int(11) NOT NULL,
  `lic_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `copy_ass_ins`
--

INSERT INTO `copy_ass_ins` (`id`, `copy_id`, `licensor`, `licensee`, `term`, `lic_date`, `exp_date`, `consideration`, `rem_ip`, `ent_date`, `ent_date2`, `docs`, `reminder`, `comments`, `created_at`, `updated_at`) VALUES
(1, 2, 'kalam', 'Mukesh Gadge', 0, '2017-11-30', '0000-00-00', 'One time royalty fee', NULL, NULL, '2020-03-31', 'ip18.jpg', NULL, 'testing', '2020-03-18 09:10:44', '2020-03-18 09:10:44');

-- --------------------------------------------------------

--
-- Table structure for table `copy_ass_outs`
--

CREATE TABLE `copy_ass_outs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `copy_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term` int(11) NOT NULL,
  `lic_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `copy_ass_outs`
--

INSERT INTO `copy_ass_outs` (`id`, `copy_id`, `licensor`, `licensee`, `term`, `lic_date`, `exp_date`, `consideration`, `rem_ip`, `ent_date`, `ent_date2`, `docs`, `reminder`, `comments`, `created_at`, `updated_at`) VALUES
(1, 2, 'kalam', 'Kalam shaikh', 0, '2017-10-30', '0000-00-00', 'One time royalty fee', NULL, NULL, '2018-10-29', 'ip17.jpg', NULL, 'none', '2020-03-18 13:48:06', '2020-03-18 13:48:06');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'India', '2020-08-25 05:46:40', '2020-08-25 05:46:40'),
(2, 'Pakistan', '2020-08-25 05:51:44', '2020-08-25 05:51:44'),
(3, 'Nepal', '2020-08-25 05:51:52', '2020-08-25 05:51:52'),
(4, 'China', '2020-08-25 05:52:00', '2020-08-25 05:52:00'),
(5, 'Sri Lanka', '2020-08-25 05:52:10', '2020-08-25 05:52:10'),
(6, 'Bhutan', '2020-08-25 05:52:20', '2020-08-25 05:52:20'),
(7, 'Afghanistan', '2020-08-25 05:52:45', '2020-08-25 05:52:45'),
(8, 'Bangladesh', '2020-08-25 05:53:09', '2020-08-25 05:53:09');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `country` text NOT NULL,
  `state` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `country`, `state`) VALUES
(1, 'India', 'Maharashtra'),
(2, 'China', 'Anhui Province'),
(3, 'India', 'Delhi'),
(4, 'India', 'Goa');

-- --------------------------------------------------------

--
-- Table structure for table `court_lists`
--

CREATE TABLE `court_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `court_lists`
--

INSERT INTO `court_lists` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'HIGH COURT', '2020-02-11 05:49:37', '2020-02-11 05:49:37'),
(2, 'SUPREME COURT', '2020-02-11 05:49:55', '2020-02-11 05:49:55'),
(3, 'DISTRICT COURT', '2020-02-11 05:50:06', '2020-02-11 05:50:06'),
(4, 'DISTRICT COURTs', '2020-02-19 05:57:53', '2020-02-19 05:57:53');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `code`, `name`, `created_at`, `updated_at`) VALUES
(1, 'INR', 'Indian Rupees', '2020-02-11 05:57:36', '2020-02-11 05:57:36'),
(2, 'USD', 'US Dollars', '2020-02-11 05:57:52', '2020-02-19 06:05:27');

-- --------------------------------------------------------

--
-- Table structure for table `designation_lists`
--

CREATE TABLE `designation_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designation_lists`
--

INSERT INTO `designation_lists` (`id`, `designation`, `created_at`, `updated_at`) VALUES
(1, 'Associate', '2019-05-20 05:57:16', '2020-01-12 18:11:04'),
(2, 'IP Head', '2020-01-12 18:11:16', '2020-01-12 18:11:16'),
(3, 'BP Manager', '2020-01-12 18:11:26', '2020-01-12 18:11:26'),
(4, 'Software Development', '2020-08-17 07:46:53', '2020-08-17 07:46:53');

-- --------------------------------------------------------

--
-- Table structure for table `designms`
--

CREATE TABLE `designms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `prop_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `govt_fee` decimal(10,2) NOT NULL,
  `agent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `app_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filing_date` date NOT NULL,
  `app_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name_of_applicant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `natappli_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `natent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `article_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_t` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `classg_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang_id` bigint(20) UNSIGNED DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_aff` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copy_application` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `ssp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `app_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designms`
--

INSERT INTO `designms` (`id`, `prop_code`, `govt_fee`, `agent_id`, `app_no`, `filing_date`, `app_type`, `user_id`, `name_of_applicant`, `natappli_id`, `ent_id`, `natent_id`, `article_name`, `image_t`, `description`, `classg_id`, `image`, `lang_id`, `remarks`, `user_aff`, `name`, `designation`, `copy_application`, `status`, `sub_status`, `sp`, `ssp`, `app_status`, `created_at`, `updated_at`) VALUES
(2, '32453', '5000.00', NULL, '1235890', '2020-08-24', 'Online', 23, 'ABC Limited', 3, 2, 2, 'Logo Design', 'IMG-20200814-WA0003.jpg', 'This is image is related to trademark', 15, 'IMG-20200814-WA0003.jpg', 1, 'none', 'IMG-20200814-WA0003.jpg', 'Kalam Shaikh', 'Data Entry Operator', 'Career in Food Industry.pdf', 'Active Application', 'Hearing for Objection', '1', '4', 1, '2020-08-21 06:57:38', '2020-08-23 07:17:06'),
(3, '45000', '5000.00', NULL, '12344', '2020-04-25', 'Online', 23, 'ABC Limited', 4, 1, 2, 'Corporate Steel Material', 'IMG-20200814-WA0003.jpg', 'Dummy', 9, 'IMG-20200814-WA0003.jpg', 1, 'Article', 'IMG-20200814-WA0003.jpg', 'Kalam Shaikh', 'Data Entry Operator', 'Career in Food Industry.pdf', 'Active Application', 'Reply to objection submitted', '1', '3', 1, '2020-08-21 08:35:32', '2020-08-23 07:10:57'),
(4, '6666', '50000.00', NULL, '459326', '2020-06-21', 'Online', 23, 'ABC Limited', 3, 1, 3, 'Manvider Jindal Design', 'IMG-20200814-WA0003.jpg', 'this is an logo description', 16, 'IMG-20200814-WA0003.jpg', 1, 'none of them', 'Career in Food Industry.pdf', 'Mukesh Gadge', 'Developer', 'IMG-20200814-WA0003.jpg', 'Active Application', 'Examination Report Received', '1', '2', 1, '2020-08-22 07:27:45', '2020-08-23 06:50:20'),
(5, '78952', '5000.00', NULL, '759820', '2020-08-11', 'Online', 23, 'ABC Limited', 3, 1, 1, 'Graduate Person', 'IMG-20200814-WA0003.jpg', 'This', 16, 'IMG-20200814-WA0003.jpg', 1, 'cong', 'Litigation Report (2).pdf', 'Mukesh Gadge', 'MD', 'Litigation Report (2).pdf', 'Active Application', 'Removal of Objection Period Extended', '1', '5', 1, '2020-08-23 10:58:31', '2020-08-23 10:59:54'),
(6, '6666', '50000.00', NULL, '147850', '2020-07-29', 'Online', 23, 'ABC Limited', 3, NULL, NULL, 'FacadeCleaning', 'IMG-20200814-WA0003.jpg', 'this', 15, 'Litigation Report (2).pdf', 1, 'none', 'Litigation Report (3).pdf', 'Vallab Bhai Patel', 'Senior Manager', 'Litigation Report (2).pdf', 'Abandoned / Rejected', '', '2', '', 1, '2020-08-23 11:02:47', '2020-08-23 11:09:04'),
(7, '199875', '5000.00', NULL, '120500', '2020-07-28', 'Offline', 23, NULL, 4, 3, 2, 'web', 'IMG20200819173939.jpg', 'web', 15, 'Litigation Report (2).pdf', 1, 'Logo of an company', 'Litigation Report (3).pdf', 'Rajive', 'Sanor', 'IMG20200819173939.jpg', 'Registered and Awaiting Publication', '', '3', '', 1, '2020-08-23 11:11:42', '2020-08-23 11:15:29'),
(8, '36889', '5000.00', NULL, '236854', '2020-07-26', 'Offline', 23, 'ABC Limited', 3, NULL, NULL, 'Steel Deploye', 'IMG20200819173939.jpg', 'sometines', 15, 'Litigation Report (2).pdf', 1, 'Des', 'Litigation Report (3).pdf', 'Kallan Pasha', 'data', 'Litigation Report (2).pdf', 'Expired - For Renewal', '', '5', '', 1, '2020-08-23 11:17:39', '2020-08-23 11:20:52'),
(9, '98979', '5000.00', NULL, '369239', '2020-06-04', 'Online', 23, NULL, 4, 4, 2, 'Video Lessar', 'IMG-20200814-WA0003.jpg', 'Dummy', 17, 'Litigation Report (3).pdf', 1, 'Videi', 'Litigation Report (2).pdf', 'Kalam Shaikh', 'Manager', 'Litigation Report (2).pdf', 'Registered and Published', '', '4', '', 1, '2020-08-23 11:19:53', '2020-08-23 11:20:31'),
(10, '12000', '6000.00', NULL, '123687', '2020-07-21', 'Offline', 23, NULL, 5, 2, 3, 'Situation Handle', 'IMG-20200814-WA0003.jpg', 'This is an advanced', 22, 'Litigation Report (3).pdf', NULL, 'none', 'Litigation Report (2).pdf', 'Abdul Balaksha', 'CEO of company', 'Litigation Report (3).pdf', 'Renewed', '', '6', '', 1, '2020-08-23 11:29:47', '2020-08-23 11:30:20'),
(11, '34695149', '45000.00', NULL, '479', '2020-06-24', 'Offline', 23, 'ABC Limited', 3, NULL, NULL, 'article', 'IMG20200819173925.jpg', 'This', 17, 'Litigation Report (2).pdf', 1, 'articl', 'Litigation Report (2).pdf', 'Kalam', 'Developer', 'IMG-20200814-WA0003.jpg', 'Active Application', 'New Application', '1', '1', 1, '2020-08-23 11:33:14', '2020-08-23 11:33:14'),
(12, '34596', '4500.00', NULL, '125693', '2020-08-15', 'Offline', 23, NULL, 6, 1, 3, 'Design', 'IMG-20200814-WA0003.jpg', 'yes i want to learn', 17, 'Litigation Report (2).pdf', 1, 'Ln', 'Litigation Report (2).pdf', 'Baladhar Ganga', 'manager', 'Litigation Report (2).pdf', 'Extension Expired', '', '7', '', 1, '2020-08-23 11:37:43', '2020-08-23 11:38:33');

-- --------------------------------------------------------

--
-- Table structure for table `designs`
--

CREATE TABLE `designs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `proprietor_code` int(10) UNSIGNED NOT NULL,
  `govt_fee` int(10) UNSIGNED NOT NULL,
  `agent_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filing_date` date NOT NULL,
  `type_of_app` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_of_applicant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nature_of_applicant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nature_of_entity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_of_article` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_t` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_of_goods` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_aff` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `copy_application` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp` int(11) DEFAULT NULL,
  `ssp` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designs`
--

INSERT INTO `designs` (`id`, `proprietor_code`, `govt_fee`, `agent_name`, `app_no`, `filing_date`, `type_of_app`, `user`, `name_of_applicant`, `nature_of_applicant`, `entity`, `nature_of_entity`, `name_of_article`, `image_t`, `description`, `class_of_goods`, `image`, `language`, `remarks`, `user_aff`, `name`, `designation`, `copy_application`, `status`, `sub_status`, `sp`, `ssp`, `created_at`, `updated_at`) VALUES
(1, 23555, 12000, 'NA', '123456', '2020-03-09', 'Online', 'Shilpa', 'ABC Limited', 'Individual', 'Mahindra', 'Partnership Firm', 'Logo Design', 'ip17.jpg', 'Company Logo', 'Class 17', 'no-image.png', 'English', 'None', 'ip15.jpg', 'Mukesh Gadge', 'manager', '130597198_1582358882244.pdf', NULL, NULL, NULL, NULL, '2020-03-11 01:35:16', '2020-03-11 01:35:16');

-- --------------------------------------------------------

--
-- Table structure for table `designss`
--

CREATE TABLE `designss` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `prop_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `govt_fee` decimal(10,2) NOT NULL,
  `agent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `app_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filing_date` date NOT NULL,
  `app_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name_of_applicant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `natappli_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `natent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `article_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_t` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `classg_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang_id` bigint(20) UNSIGNED DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_aff` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `ssp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `app_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `design_classes`
--

CREATE TABLE `design_classes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `design_classes`
--

INSERT INTO `design_classes` (`id`, `name`, `desc`, `created_at`, `updated_at`) VALUES
(1, 'Class 1', 'FOODSTUFFS', '2020-02-19 06:53:07', '2020-03-04 18:00:27'),
(2, 'Class 2', 'ARTICLES OF CLOTHING AMD HABERDASHERY', '2020-03-04 18:02:02', '2020-03-04 18:02:02'),
(3, 'Class 3', 'TRAVEL GOODS. CASES. PARASOLS AND PERSONAL BELONGINGS, NOT ELSEWHERE SPECIFIED', '2020-03-04 18:02:27', '2020-03-04 18:02:27'),
(4, 'Class 4', 'BRUSH WARE', '2020-03-04 18:02:58', '2020-03-04 18:02:58'),
(5, 'Class 5', 'TEXTILE PIECE GOODS, ARTIFICIAL AND NATURAL SHEET MATERIAL', '2020-03-04 18:03:18', '2020-03-04 18:03:18'),
(6, 'Class 6', 'FURNISHING', '2020-03-04 18:03:42', '2020-03-04 18:03:42'),
(7, 'Class 7', 'HOUSEHOLD GOODS, NOT ELSEWHERE SPECIFIED', '2020-03-04 18:04:01', '2020-03-04 18:04:01'),
(8, 'Class 8', 'TOOLS & HARDWARE', '2020-03-04 18:04:19', '2020-03-04 18:04:19'),
(9, 'Class 9', 'PACKAGES AND CONTAINERS FOR THE TRANSPORT OR HANDLING OF GOODS', '2020-03-04 18:04:38', '2020-03-04 18:04:38'),
(10, 'Class 10', 'CLOCKS AND WATCHES AND OTHER MEASURING INSTRUMENTS, CHECKING AND SIGNALLING INSTRUMENTS', '2020-03-04 18:05:04', '2020-03-04 18:05:04'),
(11, 'Class 11', 'ARTICLES OF ADORNMENT', '2020-03-04 18:05:23', '2020-03-04 18:05:23'),
(12, 'Class 12', 'MEANS OF TRANSPORT OR HOISTING', '2020-03-04 18:05:42', '2020-03-04 18:05:42'),
(13, 'Class 13', 'EQUIPMENT FOR PRODUCTION. DISTRIBUTION OR TRANSFORMATION OF ELECTRICITY', '2020-03-04 18:06:21', '2020-03-04 18:06:21'),
(14, 'Class 14', 'RECORDING, COMMUNICATION OR INFORMATION RETRIEVAL EQUIPMENT', '2020-03-04 18:06:39', '2020-03-04 18:06:39'),
(15, 'Class 15', 'MACHINES, NOT ELSEWHERE SPECIFIED', '2020-03-04 18:06:56', '2020-03-04 18:06:56'),
(16, 'Class 16', 'PHOTOGRAPHIC CINEMATOGRAPHIC AND OPTICAL APPARATUS', '2020-03-04 18:07:15', '2020-03-04 18:07:15'),
(17, 'Class 17', 'MUSICAL INSTRUMENTS', '2020-03-04 18:07:34', '2020-03-04 18:07:34'),
(18, 'Class 18', 'PRINTING AND OFFICE MACHINERY', '2020-03-04 18:07:53', '2020-03-04 18:07:53'),
(19, 'Class 19', 'STATIONERY AND OFFICE EQUIPMENT, ARTISTS\' AND TEACHING MATERIALS', '2020-03-04 18:08:22', '2020-03-04 18:08:22'),
(20, 'Class 20', 'SALES AND ADVERTISING EQUIPMENT. SIGNS', '2020-03-04 18:08:40', '2020-03-04 18:08:40'),
(21, 'Class 21', 'GAMES, TOYS. TENTS AND SPORTS GOODS', '2020-03-04 18:09:04', '2020-03-04 18:09:04'),
(22, 'Class 22', 'ARMS, PYROTECHNIC ARTICLES. ARTICLES FOR HUNTING. FISHING AND PEST KILLING', '2020-03-04 18:09:22', '2020-03-04 18:09:22'),
(23, 'Class 23', 'FLUID DISTRIBUTION EQUIPMENTS, SANITARY HEATING VENTILATION AND AIR-CONDITIONING EQUIPMENT, SOLID FUEL', '2020-03-04 18:09:41', '2020-03-04 18:09:41'),
(24, 'Class 24', 'MEDICAL AND LABORATORY EQUIPMENTS', '2020-03-04 18:10:01', '2020-03-04 18:10:01'),
(25, 'Class 25', 'BUILDING UNITS AND CONSTRUCTION ELEMENTS', '2020-03-04 18:10:22', '2020-03-04 18:10:22'),
(26, 'Class 26', 'LIGHTING APPARATUS', '2020-03-04 18:10:44', '2020-03-04 18:10:44'),
(27, 'Class 27', 'TOBACCO AND SMOKERS\' SUPPLIERS', '2020-03-04 18:11:08', '2020-03-04 18:11:08'),
(28, 'Class 28', 'PHARMACEUTICAL AND COSMETIC PRODUCTS, TOILET AND APPARATUS', '2020-03-04 18:11:28', '2020-03-04 18:11:28'),
(29, 'Class 29', 'DEVICES AND EQUIPMENT AGAINST FIRE HAZARDS. FOR ACCIDENT PREVENTION AND FOR RESCUE', '2020-03-04 18:11:49', '2020-03-04 18:11:49'),
(30, 'Class 30', 'ARTICLES FOR THE CARE AND HANDLING OF ANIMALS', '2020-03-04 18:12:10', '2020-03-04 18:12:10'),
(31, 'Class 31', 'MACHINES AND APPLIANCES FOR PREPARING FOOD OR DRINK NOT  ELSEWHERE SPECIFIED', '2020-03-04 18:12:37', '2020-03-04 18:12:37'),
(32, 'Class 99', 'MISCELLANEOUS', '2020-03-04 18:12:56', '2020-03-04 18:12:56');

-- --------------------------------------------------------

--
-- Table structure for table `design_lic_ins`
--

CREATE TABLE `design_lic_ins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `des_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term` int(11) NOT NULL,
  `lic_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `design_lic_ins`
--

INSERT INTO `design_lic_ins` (`id`, `des_id`, `licensor`, `licensee`, `term`, `lic_date`, `exp_date`, `consideration`, `rem_ip`, `ent_date`, `ent_date2`, `docs`, `reminder`, `comments`, `created_at`, `updated_at`) VALUES
(3, 1, 'kalam', 'Mukesh Gadge', 10, '2015-10-26', '2025-10-26', 'One time royalty fee', NULL, NULL, '2020-11-30', 'ip18.jpg', NULL, 'this isn', '2020-03-11 07:41:40', '2020-03-11 07:41:40');

-- --------------------------------------------------------

--
-- Table structure for table `design_lic_outs`
--

CREATE TABLE `design_lic_outs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `des_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term` int(11) NOT NULL,
  `lic_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `design_lic_outs`
--

INSERT INTO `design_lic_outs` (`id`, `des_id`, `licensor`, `licensee`, `term`, `lic_date`, `exp_date`, `consideration`, `rem_ip`, `ent_date`, `ent_date2`, `docs`, `reminder`, `comments`, `created_at`, `updated_at`) VALUES
(1, 1, 'kalam', 'Mukesh Gadge', 10, '2018-10-29', '2028-10-29', 'One time royalty fee', NULL, NULL, '2018-10-28', 'ip13.jpg', NULL, 'none', '2020-03-12 08:14:24', '2020-03-12 08:14:24');

-- --------------------------------------------------------

--
-- Table structure for table `des_ass_ins`
--

CREATE TABLE `des_ass_ins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `des_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term` int(11) NOT NULL,
  `lic_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `des_ass_ins`
--

INSERT INTO `des_ass_ins` (`id`, `des_id`, `licensor`, `licensee`, `term`, `lic_date`, `exp_date`, `consideration`, `rem_ip`, `ent_date`, `ent_date2`, `docs`, `reminder`, `comments`, `created_at`, `updated_at`) VALUES
(1, 1, 'kalam', 'Kalam shaikh', 0, '2018-12-29', '0000-00-00', 'One time royalty fee', NULL, NULL, '2020-03-31', 'ip18.jpg', NULL, 'testi', '2020-03-18 09:18:15', '2020-03-18 09:18:15');

-- --------------------------------------------------------

--
-- Table structure for table `des_ass_outs`
--

CREATE TABLE `des_ass_outs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `des_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term` int(11) NOT NULL,
  `lic_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `des_ass_outs`
--

INSERT INTO `des_ass_outs` (`id`, `des_id`, `licensor`, `licensee`, `term`, `lic_date`, `exp_date`, `consideration`, `rem_ip`, `ent_date`, `ent_date2`, `docs`, `reminder`, `comments`, `created_at`, `updated_at`) VALUES
(1, 1, 'kalam', 'Kalam shaikh', 0, '2017-11-29', '0000-00-00', 'One time royalty fee', NULL, NULL, '2019-10-30', 'ip18.jpg', NULL, 'none', '2020-03-18 13:42:04', '2020-03-18 13:42:04');

-- --------------------------------------------------------

--
-- Table structure for table `dynamic_fields`
--

CREATE TABLE `dynamic_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dynamic_fields`
--

INSERT INTO `dynamic_fields` (`id`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES
(1, 'Kalam', 'Shaikh', NULL, NULL),
(2, 'Nasreen', 'Shaikh', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `enforcements`
--

CREATE TABLE `enforcements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `target_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_of_product` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `qty` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `tm_app` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entity` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docs` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `enforcements`
--

INSERT INTO `enforcements` (`id`, `target_name`, `owner_name`, `state`, `city`, `target`, `name_of_product`, `user`, `created_at`, `updated_at`, `qty`, `amount`, `date`, `tm_app`, `entity`, `country`, `docs`) VALUES
(1, 'City Mall', 'Shailash Jain', 'Maharashtra', 'Pune', 'Manufacturer', 'Lizol', NULL, '2020-03-09 10:58:04', '2020-03-09 10:58:04', '14000', '120000', '2020-03-08', NULL, 'Mahindra', 'India', 'ip18.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `enfs`
--

CREATE TABLE `enfs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `target_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `country_id` bigint(20) UNSIGNED NOT NULL,
  `state_id` bigint(20) UNSIGNED NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nop` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amt` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `docs` text COLLATE utf8mb4_unicode_ci,
  `tm_app` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `enfs`
--

INSERT INTO `enfs` (`id`, `ent_id`, `target_name`, `owner`, `date`, `country_id`, `state_id`, `city`, `target`, `nop`, `qty`, `amt`, `docs`, `tm_app`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Showroom', 'Kalam Shaikh', '2020-07-28', 1, 1, 'Mumbai', 'Retailer', 'TVS Jupiter', '12', '2487933', 'IMG-20200814-WA0003.jpg', '45786689', '2020-08-25 07:59:55', '2020-08-25 07:59:55'),
(2, NULL, 'Raja Ram Paan Patti', 'Bihari Laal', '2020-02-06', 1, 3, 'Ahamedabad', 'Retailer', 'Parag Paan Masala', '5000', '5000', 'IMG-20200814-WA0003.jpg', NULL, '2020-08-25 08:19:36', '2020-08-25 10:08:13'),
(3, 5, 'Rehan Cosmetic Shop', 'Rehan Malik', '2019-07-15', 1, 1, 'Nagpur', 'Retailer', 'Loreal Eyeshade', '15000', '125600', NULL, NULL, '2020-08-25 08:23:14', '2020-08-25 10:07:12');

-- --------------------------------------------------------

--
-- Table structure for table `entity_lists`
--

CREATE TABLE `entity_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_entity_id` int(11) NOT NULL,
  `entity_child` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `entity_lists`
--

INSERT INTO `entity_lists` (`id`, `parent_entity_id`, `entity_child`, `created_at`, `updated_at`) VALUES
(1, 1, 'Mahindra', '2019-05-21 03:33:08', '2020-02-03 10:57:30'),
(2, 1, 'Tata', '2020-08-17 07:40:45', '2020-08-17 07:40:45'),
(3, 1, 'Aara Web Solutions', '2020-08-17 07:41:16', '2020-08-17 07:41:16'),
(4, 1, 'Lexschool Pvt Ltd', '2020-08-17 07:41:46', '2020-08-17 07:42:14'),
(5, 1, 'Naziya Beuty Parlour', '2020-08-24 06:09:03', '2020-08-24 06:09:41');

-- --------------------------------------------------------

--
-- Table structure for table `entity_mappings`
--

CREATE TABLE `entity_mappings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `entity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `function` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `entity_mappings`
--

INSERT INTO `entity_mappings` (`id`, `entity`, `unit`, `function`, `created_at`, `updated_at`) VALUES
(3, 'ABC Limited', 'Pune', 'IT', '2019-05-23 00:56:28', '2019-05-23 00:56:28'),
(4, 'ABC Pvt Ltd', 'Pune', 'Legal', '2019-05-23 00:56:39', '2019-05-23 00:56:39'),
(5, 'ABC Pvt Ltd', 'Pune', 'Finance', '2019-05-23 00:56:53', '2019-05-23 00:56:53'),
(6, 'AaraWebSolution', 'Pune', 'IT,Legal,Finance', '2019-06-17 12:12:26', '2019-06-17 12:12:26'),
(7, 'ABC Pvt Ltd', 'Navi Mumbai', 'IT,Legal,Finance', '2020-01-07 09:22:44', '2020-01-07 09:22:44');

-- --------------------------------------------------------

--
-- Table structure for table `expenseins`
--

CREATE TABLE `expenseins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upload` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_stat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invest_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expenseins`
--

INSERT INTO `expenseins` (`id`, `invoice_no`, `amount`, `upload`, `pay_stat`, `invest_id`, `date`, `remarks`, `created_at`, `updated_at`) VALUES
(1, '321', '12000', 'ip15.jpg', 'Paid', 1, '2020-03-08', 'Through Online', '2020-03-09 10:50:03', '2020-03-09 10:50:03'),
(2, '2719', '2950', 'tradmark Reports export (1).pdf', 'Unpaid', 4, NULL, NULL, '2020-08-25 13:24:30', '2020-08-25 13:24:30');

-- --------------------------------------------------------

--
-- Table structure for table `expensens`
--

CREATE TABLE `expensens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upload` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_stat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enforce_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expensens`
--

INSERT INTO `expensens` (`id`, `invoice_no`, `amount`, `upload`, `pay_stat`, `date`, `remarks`, `enforce_id`, `created_at`, `updated_at`) VALUES
(1, '2010', '150000', 'ip18.jpg', 'Paid', '2020-03-08', 'Payment Through Online', 1, '2020-03-09 10:59:37', '2020-03-09 11:02:13');

-- --------------------------------------------------------

--
-- Table structure for table `external_counsels`
--

CREATE TABLE `external_counsels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `law_firm` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `external_counsels`
--

INSERT INTO `external_counsels` (`id`, `country`, `state`, `city`, `law_firm`, `name`, `cn`, `email`, `address`, `area`, `created_at`, `updated_at`) VALUES
(1, 'India', 'Maharashtra', 'Mumbai', 'XYZ pvt ltd', 'Kalam shaikh', '9594785319', 'kashaikh611@gmail.com', 'F-SECTOR, W2-LINE,ROOM NO.11,CHEETA CAMP,TROMBAY', 'Legal Advise', '2020-02-11 06:59:50', '2020-02-11 06:59:50'),
(2, 'India', 'Maharashtra', 'MUMBAI', 'XYZ pvt ltd', 'Kalam shaikh', '9594785319', 'kashaikh611@gmail.com', 'F-SECTOR, W2-LINE,ROOM NO.11,CHEETA CAMP,TROMBAY', 'Legal Advise', '2020-02-11 07:10:17', '2020-02-11 07:10:17');

-- --------------------------------------------------------

--
-- Table structure for table `firs`
--

CREATE TABLE `firs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fir_date` date NOT NULL,
  `fir_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `police` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nof` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dof` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fir_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `function_lists`
--

CREATE TABLE `function_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `function` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `function_lists`
--

INSERT INTO `function_lists` (`id`, `function`, `created_at`, `updated_at`) VALUES
(1, 'IT', '2019-05-20 03:09:31', '2019-05-20 05:27:04'),
(2, 'Legal', '2019-05-20 03:23:55', '2019-05-20 03:23:55'),
(3, 'Finance', '2019-05-20 03:24:06', '2019-08-17 10:57:47');

-- --------------------------------------------------------

--
-- Table structure for table `hearings`
--

CREATE TABLE `hearings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hear_id` bigint(20) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `her_date` date NOT NULL,
  `alert1` date DEFAULT NULL,
  `alert2` date DEFAULT NULL,
  `alert3` date DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stage_desc` text COLLATE utf8mb4_unicode_ci,
  `act_item` text COLLATE utf8mb4_unicode_ci,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `ext_counsel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `law_firm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hearings`
--

INSERT INTO `hearings` (`id`, `hear_id`, `date`, `her_date`, `alert1`, `alert2`, `alert3`, `email`, `stage_desc`, `act_item`, `user_id`, `ext_counsel`, `law_firm`, `due_date`, `docs`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-02-19', '2020-02-29', '2020-02-14', '2020-02-20', '2020-02-21', NULL, 'Hearing', NULL, 22, 'Kalam shaikh', 'XYZ pvt ltd', '2020-02-21', 'ip16.jpg', '2020-02-19 02:36:53', '2020-02-19 02:36:53'),
(2, 2, '2020-08-01', '2020-08-29', '2020-08-23', '2020-08-24', '2020-08-25', 'kashaikh611@gmail.com', 'Evidence Submission', 'Evidence Submission', 22, 'Kalam shaikh', 'XYZ pvt ltd', '2020-08-28', 'Litigation Report (3).pdf', '2020-08-23 13:38:32', '2020-08-23 13:38:32');

-- --------------------------------------------------------

--
-- Table structure for table `investigations`
--

CREATE TABLE `investigations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dsi` date NOT NULL,
  `dei` date NOT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tm_app` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(170) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nota` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tar_add` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `investigations`
--

INSERT INTO `investigations` (`id`, `inag`, `state`, `city`, `target`, `product`, `dsi`, `dei`, `docs`, `tm_app`, `user`, `entity`, `country`, `nota`, `tar_add`, `created_at`, `updated_at`) VALUES
(1, 'Brand Defence Consulting Pvt Ltd', 'Maharashtra', 'Pune', 'Manufacturer', 'Lizol', '2020-03-01', '2020-03-08', 'ip20.jpg', NULL, 'shilpa99', 'Mahindra', 'India', 'Shailash Jain', 'F-SECTOR, W2-LINE,ROOM NO.11,CHEETA CAMP,TROMBAY', '2020-03-09 10:45:00', '2020-03-09 10:49:13'),
(2, 'Aara Protection Pvt Ltd', 'Dhaka', 'Dhaka', 'Retailer', 'Yashwa Gandha', '2020-07-31', '2020-08-10', 'IMG-20200814-WA0003.jpg', NULL, 'shilpa99', 'Mahindra', 'Bangladesh', 'Bijwendra Yadav', 'Yamuna Nagar Delhi', '2020-08-17 07:01:47', '2020-08-17 07:01:47');

-- --------------------------------------------------------

--
-- Table structure for table `invests`
--

CREATE TABLE `invests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `inag_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `country_id` bigint(20) UNSIGNED NOT NULL,
  `state_id` bigint(20) UNSIGNED NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nop` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dsi` date NOT NULL,
  `dei` date NOT NULL,
  `docs` text COLLATE utf8mb4_unicode_ci,
  `tm_app` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nota` text COLLATE utf8mb4_unicode_ci,
  `n_addr` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invests`
--

INSERT INTO `invests` (`id`, `ent_id`, `inag_id`, `user_id`, `country_id`, `state_id`, `city`, `target`, `nop`, `dsi`, `dei`, `docs`, `tm_app`, `nota`, `n_addr`, `created_at`, `updated_at`) VALUES
(3, 1, 1, 23, 1, 1, 'Mumbai', 'Manufacturer', 'Auto Spare Parts', '2020-07-20', '2020-08-15', 'IMG-20200814-WA0003.jpg', '45786689', 'Ridhesh Waghmare', 'F-SECTOR, W2-LINE,ROOM NO.11,CHEETA CAMP,TROMBAY', '2020-08-25 10:38:58', '2020-08-25 13:08:38'),
(4, 4, 1, 22, 1, 2, 'Panji', 'Wholeseller', 'Compliance Management', '2020-06-15', '2020-07-20', 'IMG-20200814-WA0003.jpg', '12364', 'Kalam Shaikh', 'F-SECTOR, W2-LINE,ROOM NO.11,CHEETA CAMP,TROMBAY', '2020-08-25 11:49:45', '2020-08-25 11:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `language`, `created_at`, `updated_at`) VALUES
(1, 'English', '2019-05-25 01:58:25', '2020-02-13 10:28:59'),
(2, 'Hindi', '2019-08-17 10:46:52', '2019-08-17 10:53:43'),
(3, 'other', '2019-08-17 10:47:07', '2019-08-17 10:47:07');

-- --------------------------------------------------------

--
-- Table structure for table `law_firms`
--

CREATE TABLE `law_firms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `law_firms`
--

INSERT INTO `law_firms` (`id`, `name`, `cp`, `cn`, `email`, `address`, `created_at`, `updated_at`) VALUES
(1, 'XYZ pvt ltd', 'Mangesh Gadge', '8787878787', 'abc@gmail.com', 'pune', '2020-02-11 05:23:11', '2020-02-11 05:23:11');

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leadby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_id` bigint(20) UNSIGNED DEFAULT NULL,
  `emp_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docs` text COLLATE utf8mb4_unicode_ci,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legal_notices`
--

CREATE TABLE `legal_notices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `legal_notices`
--

INSERT INTO `legal_notices` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'POLICE COMPLAINT', '2020-02-11 05:40:14', '2020-02-11 05:40:14'),
(2, 'Legal Notice', '2020-02-11 05:40:30', '2020-02-11 05:40:30'),
(3, 'TERMINATION LETTER', '2020-02-11 05:40:42', '2020-02-11 05:40:42'),
(4, 'TERMINATION NOTICE', '2020-02-11 05:40:55', '2020-02-11 05:40:55'),
(5, 'COURT NOTICE', '2020-02-11 05:41:11', '2020-02-11 05:41:11'),
(6, 'REPLY TO ADVOCATE', '2020-02-11 05:41:25', '2020-02-11 05:41:25'),
(7, 'REPLY TO ADVOCAT', '2020-02-19 05:55:06', '2020-02-19 05:55:06');

-- --------------------------------------------------------

--
-- Table structure for table `litigation_notices`
--

CREATE TABLE `litigation_notices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `entity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `by_ag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opp_part` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `not_ref` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addr_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `int_per` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_date` date DEFAULT NULL,
  `sent_rec` date DEFAULT NULL,
  `not_reply` date DEFAULT NULL,
  `not_rem` date DEFAULT NULL,
  `ext_counsel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opp_part_adv` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rel_law` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `amount_involved` decimal(8,2) DEFAULT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `conv_curr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `conv_amt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `litigation_notices`
--

INSERT INTO `litigation_notices` (`id`, `entity`, `Location`, `department`, `by_ag`, `opp_part`, `category`, `not_ref`, `addr_to`, `user_id`, `int_per`, `notice_date`, `sent_rec`, `not_reply`, `not_rem`, `ext_counsel`, `opp_part_adv`, `rel_law`, `comments`, `docs`, `date`, `amount_involved`, `currency`, `conv_curr`, `conv_amt`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Mahindra', 'Pune', 'Legal', 'By Company', 'uyz', 'Arbitration', '8888', 'uiuiu', 22, 'nkkkq', '2020-02-11', '2020-02-11', '2020-02-12', '2020-02-14', 'Kalam shaikh', 'xyz', 'xw', 'hjhjw', 'Advcance for modules.pdf', '2020-02-10', '1.00', 'INR', 'INR', '1.00', 'pending', '2020-02-11 18:45:16', '2020-02-11 18:45:16'),
(2, 'Mahindra', 'Navi Mumbai', 'IT', 'By Company', 'xyz', 'Arbitration', '1111', 'Delhi', 22, 'Kalam', '2020-02-02', '2020-02-03', '2020-02-07', '2020-02-05', 'Kalam shaikh', 'Mukesh', 'Nones', 'Pendig', 'aws.pdf', '2020-02-05', '1000.00', 'INR', 'INR', '1000.00', 'pending', '2020-02-12 03:16:09', '2020-02-19 05:13:27');

-- --------------------------------------------------------

--
-- Table structure for table `litigation_summaries`
--

CREATE TABLE `litigation_summaries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `entity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `by_ag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `by_part` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `against_part` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acting_as` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ext_counsel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `law_firm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adv` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `case_date` date DEFAULT NULL,
  `int_per` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `critic` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opp_act_as` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opp_part_add` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opp_part_firm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opp_part_adv` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opp_part_adv_c` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_ref` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `juris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `court` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rel_law` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `amount_involved` decimal(8,2) DEFAULT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `conv_curr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `conv_amt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `prio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `litigation_summaries`
--

INSERT INTO `litigation_summaries` (`id`, `entity`, `Location`, `department`, `category`, `by_ag`, `by_part`, `against_part`, `acting_as`, `ext_counsel`, `law_firm`, `adv`, `user_id`, `case_date`, `int_per`, `critic`, `opp_act_as`, `opp_part_add`, `opp_part_firm`, `opp_part_adv`, `opp_part_adv_c`, `case_ref`, `juris`, `court`, `rel_law`, `comments`, `date`, `amount_involved`, `currency`, `conv_curr`, `conv_amt`, `status`, `prio`, `created_at`, `updated_at`) VALUES
(1, 'Mahindra', 'Pune', 'IT', 'Arbitration', 'By Company', 'xyz', 'zyd', 'Plaintiff', 'Kalam shaikh', 'XYZ pvt ltd', 'Kalam shaikh', 22, '2020-02-12', 'nkkkq', 'Medium', 'Defendent', 'F-SECTOR, W2-LINE,ROOM NO.11,CHEETA CAMP,TROMBAY', 'XYZ', 'Mukesh', '9594785319', '155151', 'kkaa', 'DISTRICT COURT', 'lawjjk', 'amka', '2020-02-11', '1000.00', 'INR', 'INR', '1000.00', 'In Favour', '2', '2020-02-12 07:57:59', '2020-02-13 05:48:23'),
(2, 'Mahindra', 'Pune', 'IT', 'Arbitration', 'By Company', 'xyz', 'zyd', 'Plaintiff', 'Kalam shaikh', 'XYZ pvt ltd', 'Kalam shaikh', 22, '2020-02-12', 'nkkkq', 'Medium', 'Opponent', 'F-SECTOR, W2-LINE,ROOM NO.11,CHEETA CAMP,TROMBAY', 'XYZ', 'Mukesh', '9594785319', '155151', 'kkaa', 'HIGH COURT', 'lawjjk', 'amka', '2020-02-11', '1000.00', 'INR', 'INR', '1000.00', 'pending', '1', '2020-02-19 05:22:08', '2020-02-19 05:22:08'),
(3, 'Aara Web Solutions', 'Navi Mumbai', 'IT', 'Arbitration', 'Against Company', 'xyz', 'zyd', 'Plaintiff', 'Kalam shaikh', 'XYZ pvt ltd', 'Kalam shaikh', 22, '2020-08-17', 'Gajendra Verma', 'Low', 'Respondent', 'F-SECTOR, W2-LINE,ROOM NO.11,CHEETA CAMP,TROMBAY', 'XYZ', 'Mukesh', '09594785319', '2589', 'Kurla Distric', 'DISTRICT COURT', 'Article 36', NULL, '2020-08-11', '56000.00', 'INR', 'INR', '56000.00', 'Against', '2', '2020-08-23 19:27:25', '2020-08-23 19:29:56');

-- --------------------------------------------------------

--
-- Table structure for table `liti_docs`
--

CREATE TABLE `liti_docs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `doc_id` bigint(20) UNSIGNED NOT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `liti_docs`
--

INSERT INTO `liti_docs` (`id`, `doc_id`, `docs`, `created_at`, `updated_at`) VALUES
(1, 1, 'ip15.jpg', '2020-02-19 03:21:06', '2020-02-19 03:21:06'),
(2, 2, 'Litigation Report (3).pdf', '2020-08-23 13:34:32', '2020-08-23 13:34:32'),
(3, 2, 'IMG20200819173634.jpg', '2020-08-23 13:36:27', '2020-08-23 13:36:27');

-- --------------------------------------------------------

--
-- Table structure for table `liti_types`
--

CREATE TABLE `liti_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `liti_types`
--

INSERT INTO `liti_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Arbitration', '2020-02-11 04:57:48', '2020-02-11 04:57:48'),
(3, 'Legal Advis', '2020-02-11 05:07:59', '2020-02-19 05:38:15');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_05_13_104835_create_add_tradmarks_table', 1),
(5, '2019_05_20_070006_create_units_table', 2),
(9, '2019_05_20_083549_create_function_lists_table', 3),
(10, '2019_05_20_111858_create_designation_lists_table', 4),
(12, '2019_05_21_084841_create_parent_entities_table', 6),
(13, '2019_05_20_192242_create_entity_lists_table', 7),
(14, '2019_05_23_055737_create_entity_mappings_table', 8),
(15, '2019_05_23_104007_create_nature_applications_table', 9),
(16, '2019_05_23_122044_create_application_fields_table', 10),
(17, '2019_05_25_053128_create_nature_applicants_table', 11),
(18, '2019_05_25_060131_create_nature_agents_table', 12),
(19, '2019_05_25_062144_create_category_marks_table', 13),
(20, '2019_05_25_064124_create_class_good_services_table', 14),
(21, '2019_05_25_071549_create_languages_table', 15),
(22, '2019_05_25_084109_create_applicants_table', 16),
(23, '2019_05_25_102759_create_agents_table', 17),
(25, '2014_10_12_000000_create_users_table', 19),
(26, '2019_05_25_115828_create_sub_class_goods_services_table', 20),
(27, '2019_06_09_080304_create_trademark_statuses_table', 21),
(28, '2019_06_16_072453_create_update_statuses_table', 22),
(29, '2019_07_22_064415_create_roles_table', 23),
(33, '2019_07_28_165931_create_action_items_table', 24),
(34, '2019_09_25_151903_create_targets_table', 25),
(35, '2019_09_26_024113_create_investigations_table', 26),
(36, '2019_09_26_064449_create_enforcements_table', 27),
(37, '2019_09_26_080025_create_expenseins_table', 28),
(38, '2019_09_29_202337_create_dynamic_field', 29),
(39, '2019_09_29_231125_create_expensens_table', 30),
(40, '2019_09_29_233906_create_firs_table', 31),
(41, '2019_10_14_092810_create_states_table', 32),
(42, '2019_10_14_093027_create_cities_table', 32),
(43, '2019_11_13_144200_create_copyrights_table', 33),
(44, '2019_11_21_175732_create_copyright_lic_ins_table', 34),
(45, '2019_11_21_193915_create_designs_table', 35),
(46, '2019_11_22_020158_create_copyright_lic_outs_table', 35),
(47, '2019_11_22_023101_create_copyright_ass_ins_table', 36),
(48, '2019_11_22_024600_create_copyright_ass_outs_table', 37),
(49, '2019_11_22_063729_create_design_lic_ins_table', 38),
(50, '2019_11_22_063806_create_design_lic_outs_table', 38),
(51, '2019_11_22_063826_create_design_ass_ins_table', 38),
(52, '2019_11_22_063847_create_design_ass_outs_table', 38),
(53, '2019_11_22_085044_create_update_status__copyrights_table', 39),
(54, '2019_11_22_121033_create_update__status__designs_table', 40),
(55, '2019_12_16_024702_create_category_applicants_table', 41),
(56, '2019_12_16_031945_create_patents_table', 42),
(57, '2019_12_16_050103_add__to__patent', 43),
(58, '2019_12_16_052114_create_patent_lic_ins_table', 44),
(59, '2019_12_16_082123_create_patent_lic_outs_table', 45),
(60, '2019_12_16_083005_create_patent_ass_ins_table', 46),
(61, '2019_12_16_084428_create_patent_ass_outs_table', 47),
(62, '2019_12_19_225642_create_testings_table', 48),
(63, '2019_12_20_101623_create_update_status_patents_table', 49),
(64, '2020_01_08_155344_create_update_status_trads_table', 50),
(65, '2020_01_11_104104_create_agencies_table', 51),
(66, '2020_02_11_101220_create_liti_types_table', 52),
(67, '2020_02_11_103326_create_area_exps_table', 53),
(68, '2020_02_11_104423_create_law_firms_table', 54),
(69, '2020_02_11_105758_create_stages_table', 55),
(70, '2020_02_11_110449_create_legal_notices_table', 56),
(71, '2020_02_11_111220_create_court_lists_table', 57),
(72, '2020_02_11_112054_create_currencies_table', 58),
(73, '2020_02_11_121330_create_external_counsels_table', 59),
(74, '2020_02_11_123145_create_advocates_table', 60),
(75, '2020_02_11_232858_create_litigation_notices_table', 61),
(76, '2020_02_12_130133_create_litigation_summaries_table', 62),
(77, '2020_02_13_000012_create_completions_table', 63),
(78, '2020_02_13_134825_create_products_table', 64),
(79, '2020_02_19_073801_create_hearings_table', 65),
(80, '2020_02_19_083540_create_liti_docs_table', 66),
(81, '2020_02_19_091140_create_notice_statuses_table', 67),
(82, '2020_02_19_100754_create_notice_statuses_table', 68),
(83, '2020_02_19_120045_create_design_classes_table', 69),
(84, '2020_02_27_111154_create_trademark_gov_fees_table', 70),
(85, '2020_03_01_222318_create_trade_classes_table', 71),
(86, '2020_03_02_150139_create_add_tradmarks_table', 72),
(87, '2020_03_04_074535_create_add_tradmarks_table', 73),
(88, '2020_03_11_094146_create_trad_lic_ins_table', 74),
(89, '2020_03_11_122044_create_copyright_lic_ins_table', 75),
(90, '2020_03_11_125655_create_patent_lic_ins_table', 76),
(91, '2020_03_11_130013_create_design_lic_ins_table', 77),
(92, '2020_03_12_112725_create_trad_lic_outs_table', 78),
(93, '2020_03_12_114352_create_copyright_lic_outs_table', 79),
(94, '2020_03_12_133922_create_design_lic_outs_table', 80),
(95, '2020_03_12_140243_create_patent_lic_outs_table', 81),
(96, '2020_03_12_142932_create_trad_ass_ins_table', 82),
(97, '2020_03_12_145505_create_trad_ass_outs_table', 83),
(98, '2020_03_18_143644_create_copy_ass_ins_table', 84),
(99, '2020_03_18_144134_create_des_ass_ins_table', 85),
(100, '2020_03_18_144948_create_pat_ass_ins_table', 86),
(101, '2020_03_18_184856_create_pat_ass_outs_table', 87),
(102, '2020_03_18_190745_create_des_ass_outs_table', 88),
(103, '2020_03_18_191444_create_copy_ass_outs_table', 89),
(104, '2020_07_24_121350_create_trademarks_table', 90),
(105, '2020_07_30_204432_create_update_status_trads_table', 91),
(106, '2020_08_21_111536_create_designs_table', 92),
(107, '2020_08_21_114536_create_designms_table', 93),
(108, '2020_08_21_195934_create_copyrightns_table', 94),
(109, '2020_08_22_213150_create_updata_status_designs_table', 95),
(110, '2020_08_23_131125_create_update_copyright_statuses_table', 96),
(111, '2020_08_24_043154_create_leads_table', 97),
(112, '2020_08_25_104413_create_countries_table', 98),
(113, '2020_08_25_110022_create_stetes_table', 98),
(114, '2020_08_25_120002_create_enforcements_table', 99),
(115, '2020_08_25_132629_create_enfs_table', 100),
(116, '2020_08_25_154023_create_invests_table', 101);

-- --------------------------------------------------------

--
-- Table structure for table `nature_agents`
--

CREATE TABLE `nature_agents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nature_of_agent` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nature_agents`
--

INSERT INTO `nature_agents` (`id`, `nature_of_agent`, `created_at`, `updated_at`) VALUES
(1, 'Registered Trade Marks Agent', '2019-05-25 00:43:02', '2019-05-25 01:33:06'),
(2, 'Advocate', '2019-08-17 10:43:04', '2019-08-17 10:52:53'),
(3, 'Constituted Attorney', '2019-08-17 10:43:34', '2019-08-17 10:43:34');

-- --------------------------------------------------------

--
-- Table structure for table `nature_applicants`
--

CREATE TABLE `nature_applicants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nature_of_applicant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nature_applicants`
--

INSERT INTO `nature_applicants` (`id`, `nature_of_applicant`, `created_at`, `updated_at`) VALUES
(1, 'Partnership Firm', '2019-05-25 00:08:43', '2019-05-25 00:21:05'),
(2, 'Body-incorporate including Private Limited/limited Company', '2019-05-25 00:11:57', '2019-05-25 00:11:57'),
(3, 'Limited Liability Partnership', '2019-08-17 10:40:49', '2019-08-17 10:40:49'),
(4, 'Society', '2019-08-17 10:41:00', '2019-08-17 10:41:00'),
(5, 'Trust', '2019-08-17 10:41:10', '2019-08-17 10:41:10'),
(6, 'Government Department', '2019-08-17 10:41:20', '2019-08-17 10:41:20'),
(7, 'Statutory Organization', '2019-08-17 10:41:31', '2019-08-17 10:52:24'),
(8, 'Association Of Persons', '2019-08-17 10:41:46', '2019-08-17 10:41:46');

-- --------------------------------------------------------

--
-- Table structure for table `nature_applications`
--

CREATE TABLE `nature_applications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nature_of_application` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nature_applications`
--

INSERT INTO `nature_applications` (`id`, `nature_of_application`, `created_at`, `updated_at`) VALUES
(1, 'General Trademark', '2019-05-23 06:33:17', '2019-05-23 06:47:24'),
(2, 'Series Mark', '2019-05-23 06:36:49', '2019-05-23 06:36:49'),
(3, 'Collective Mark', '2019-08-17 10:35:42', '2019-08-17 10:35:42'),
(4, 'Certification Mark', '2019-08-17 10:36:41', '2019-08-17 10:51:39');

-- --------------------------------------------------------

--
-- Table structure for table `notice_statuses`
--

CREATE TABLE `notice_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `stat_id` bigint(20) UNSIGNED NOT NULL,
  `r_date` date NOT NULL,
  `act_take` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `next_act` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `due_date` date NOT NULL,
  `reminder` date NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notice_statuses`
--

INSERT INTO `notice_statuses` (`id`, `stat_id`, `r_date`, `act_take`, `next_act`, `due_date`, `reminder`, `user_id`, `docs`, `created_at`, `updated_at`) VALUES
(1, 2, '2020-02-21', 'No Action taken', 'Hearing', '2020-02-29', '2020-02-21', 22, 'ip14.jpg', '2020-02-19 04:43:13', '2020-02-19 04:43:13'),
(2, 1, '2020-08-17', 'Not there', 'Stage is complete', '2020-08-31', '2020-08-22', 22, '', '2020-08-16 19:19:56', '2020-08-16 19:19:56');

-- --------------------------------------------------------

--
-- Table structure for table `parent_entities`
--

CREATE TABLE `parent_entities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_entity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parent_entities`
--

INSERT INTO `parent_entities` (`id`, `parent_entity`, `created_at`, `updated_at`) VALUES
(1, 'NA', NULL, NULL),
(2, 'ABC Limited', '2019-05-21 03:34:51', '2019-05-21 03:34:51'),
(3, 'ABC Pvt Ltd', '2019-05-21 03:51:24', '2019-05-21 03:51:24'),
(4, 'AaraWebSolution', '2019-05-25 02:48:07', '2019-05-25 02:48:07'),
(5, 'AarCanbs', '2019-05-25 02:48:33', '2019-05-25 02:48:33'),
(6, 'sss', '2019-08-13 03:32:03', '2019-08-13 03:32:03'),
(7, 'Tata', '2019-11-21 22:38:22', '2019-11-21 22:38:22'),
(8, 'Godrej', '2020-01-07 06:04:35', '2020-01-07 06:04:35'),
(9, 'Godrej Property', '2020-01-07 06:05:23', '2020-01-07 06:05:23'),
(10, 'XYZ Limited', '2020-01-12 17:25:43', '2020-01-12 17:25:43'),
(11, 'Tata', '2020-08-17 07:40:45', '2020-08-17 07:40:45'),
(12, 'Aara Web Solutions', '2020-08-17 07:41:16', '2020-08-17 07:41:16'),
(13, 'Lexschool', '2020-08-17 07:41:46', '2020-08-17 07:41:46'),
(14, 'ABC Limited Entity', '2020-08-24 06:09:03', '2020-08-24 06:09:03');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('asif85.in@gmail.com', '$2y$10$uOnHxGn5NJeirgAuGmhI3ONqiS1aDHm5FwZT7C7aZuYAvgiDgAMZK', '2019-08-17 02:36:19'),
('kashaikh611@gmail.com', '$2y$10$JIguTLkZ0ksCZeS.L29c0uJ/o79L8b0lmk0LyHZx8JMCIBXFrn5Ei', '2019-11-15 13:59:19'),
('Nilesh.Puntambekar@emerson.com', '$2y$10$XjNHSvJvpkipmA7QVwjBs.uSXn7aGKSWAP7t3lbATd0ZuIY18cT1O', '2020-02-03 09:49:40'),
('JOSHI.SHILPA@mahindra.com', '$2y$10$40ggGiKrD1uk7ENyUNrpmOKQILHOxykWn3XAudX.bCt8RbH4MLKqa', '2020-08-24 00:26:47');

-- --------------------------------------------------------

--
-- Table structure for table `patents`
--

CREATE TABLE `patents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `priority_date` date NOT NULL,
  `filing_date` date NOT NULL,
  `application_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicant_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicant_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_app` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_app_s` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invent_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_inven` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg_pat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pct` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `int_app_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `int_fili_date` date DEFAULT NULL,
  `renewal` date NOT NULL,
  `gov` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active Application',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sub_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patents`
--

INSERT INTO `patents` (`id`, `priority_date`, `filing_date`, `application_no`, `applicant_name`, `applicant_id`, `cat_app`, `cat_app_s`, `invent_name`, `title_inven`, `reg_pat`, `pct`, `int_app_no`, `int_fili_date`, `renewal`, `gov`, `user_id`, `file`, `status`, `created_at`, `updated_at`, `sub_status`) VALUES
(1, '2020-02-19', '2020-02-21', '1234', 'Kalam shaikh', 'kashaikh611@gmail.com', 'natural person', NULL, 'Ka', 'Sciense', 'ka', 'no', NULL, '1970-01-01', '2020-02-20', 50000, 22, 'ip15.jpg', 'Active Application', '2020-02-19 07:33:45', '2020-02-19 07:33:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `patent_ass_outs`
--

CREATE TABLE `patent_ass_outs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_of_assignor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_of_assignee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_assignment` date NOT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem1` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `assin_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patent_lic_ins`
--

CREATE TABLE `patent_lic_ins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `patent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term` int(11) NOT NULL,
  `lic_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patent_lic_ins`
--

INSERT INTO `patent_lic_ins` (`id`, `patent_id`, `licensor`, `licensee`, `term`, `lic_date`, `exp_date`, `consideration`, `rem_ip`, `ent_date`, `ent_date2`, `docs`, `reminder`, `comments`, `created_at`, `updated_at`) VALUES
(1, 1, 'kalam', 'Mukesh Gadge', 10, '2018-09-26', '2028-09-26', 'Periodic royalty fee', '2020-03-31', '2020-03-10', NULL, 'ip14.jpg', NULL, 'Nne', '2020-03-11 07:29:43', '2020-03-11 07:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `pat_ass_ins`
--

CREATE TABLE `pat_ass_ins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pat_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term` int(11) NOT NULL,
  `lic_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pat_ass_ins`
--

INSERT INTO `pat_ass_ins` (`id`, `pat_id`, `licensor`, `licensee`, `term`, `lic_date`, `exp_date`, `consideration`, `rem_ip`, `ent_date`, `ent_date2`, `docs`, `reminder`, `comments`, `created_at`, `updated_at`) VALUES
(1, 1, 'kalam', 'Kalam shaikh', 0, '2017-10-29', '0000-00-00', 'One time royalty fee', NULL, NULL, '2016-10-29', 'ip17.jpg', NULL, 'none', '2020-03-18 12:44:32', '2020-03-18 12:44:32'),
(2, 1, 'kalam', 'Kalam shaikh', 0, '2017-11-29', '0000-00-00', 'One time royalty fee', NULL, NULL, '2018-10-29', 'ip18.jpg', NULL, 'none', '2020-03-18 13:22:16', '2020-03-18 13:22:16'),
(3, 1, 'kalam', 'Kalam shaikh', 0, '2018-09-28', '0000-00-00', 'Periodic royalty fee', '2017-08-27', '2021-09-29', NULL, 'ip17.jpg', NULL, 'none', '2020-03-18 13:23:41', '2020-03-18 13:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `pat_ass_outs`
--

CREATE TABLE `pat_ass_outs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pat_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term` int(11) NOT NULL,
  `lic_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pat_ass_outs`
--

INSERT INTO `pat_ass_outs` (`id`, `pat_id`, `licensor`, `licensee`, `term`, `lic_date`, `exp_date`, `consideration`, `rem_ip`, `ent_date`, `ent_date2`, `docs`, `reminder`, `comments`, `created_at`, `updated_at`) VALUES
(1, 1, 'kalam', 'Mukesh Gadge', 0, '2018-07-28', '0000-00-00', 'One time royalty fee', NULL, NULL, '2017-09-28', 'ip17.jpg', NULL, 'ne', '2020-03-18 13:25:03', '2020-03-18 13:25:03');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Dettol', '2020-02-13 08:27:52', '2020-02-13 08:27:52');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'Internal Associate', NULL, NULL),
(2, 'IP Head', NULL, NULL),
(3, 'Agent', NULL, NULL),
(4, 'Administrator', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stages`
--

CREATE TABLE `stages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stages`
--

INSERT INTO `stages` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Initial hearing', '2020-02-11 05:30:31', '2020-02-11 05:30:31'),
(2, 'Final Hearing', '2020-02-11 05:30:47', '2020-02-11 05:30:47'),
(3, 'Hearing Continues', '2020-02-11 05:30:59', '2020-02-11 05:30:59');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Maharashtra', '2019-10-14 04:18:14', '2019-10-14 04:18:14'),
(2, 'Gujarat', '2019-10-14 04:18:31', '2019-10-14 04:18:31'),
(3, 'Madhya Pradesh', '2019-10-14 04:18:44', '2019-10-14 04:18:44'),
(4, 'Goa', '2019-10-14 04:18:56', '2019-10-14 04:18:56'),
(5, 'Andhra Pradesh', '2019-10-14 04:19:11', '2019-10-14 04:19:11');

-- --------------------------------------------------------

--
-- Table structure for table `stetes`
--

CREATE TABLE `stetes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stetes`
--

INSERT INTO `stetes` (`id`, `name`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'Maharashtra', 1, '2020-08-25 06:14:13', '2020-08-25 06:14:13'),
(2, 'Goa', 1, '2020-08-25 06:17:05', '2020-08-25 06:17:05'),
(3, 'Gujrat', 1, '2020-08-25 06:17:46', '2020-08-25 06:17:46'),
(4, 'Lahore', 2, '2020-08-25 06:18:47', '2020-08-25 06:18:47'),
(5, 'Hainan', 4, '2020-08-25 06:19:21', '2020-08-25 06:19:21');

-- --------------------------------------------------------

--
-- Table structure for table `sub_class_goods_services`
--

CREATE TABLE `sub_class_goods_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `class_good_services_id` int(11) NOT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_class_goods_services`
--

INSERT INTO `sub_class_goods_services` (`id`, `class_good_services_id`, `class`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'class  29', 'Meat, fish, poultry, and game; meat extracts; preserved, dried and cooked fruits and vegetables; jellies, jams; eggs, milk and milk products; edible oils and fats; salad dressings; preserves.', '2019-06-02 23:44:35', '2019-06-02 23:44:35'),
(2, 1, 'class 10', 'Surgical, medical, dental and veterinary apparatus and instruments, artificial limbs, eyes and teeth; orthopedic articles; suture materials', '2019-06-02 23:47:28', '2019-06-02 23:47:28'),
(3, 2, 'class 35', 'Advertising and Business This class includes mainly services rendered by persons or organizations principally with the object of: help in the working or management of a commercial undertaking or, help in the management of the business affairs or commercial functions of an industrial or commercial enterprise, as well as services rendered by ; advertising establishments primarily undertaking communications to the public, declarations or announcements by all means of diffusion and concerning all kinds of goods or services.', '2019-06-02 23:47:59', '2019-06-02 23:48:26'),
(4, 2, 'class 36', 'Insurance and Financial This class includes mainly services rendered in financial and monetary affairs and services rendered in relations to insurance contracts of all kinds.', '2019-06-02 23:49:17', '2019-06-02 23:49:17'),
(5, 1, 'Class 11', 'Apparatus for lighting, heating, steam generating, cooking, refrigerating, drying, ventilating, water supply and sanitary purposes.', '2019-06-03 04:38:28', '2019-06-03 04:38:28'),
(6, 2, 'class 37', 'Construction and Repair This class includes mainly services rendered by contractors or subcontractors in the construction or making of permanent buildings, as well as services rendered by persons or organizations engaged in the restoration of objects to their original condition or in their preservation without altering their physical or chemical properties.', '2019-06-03 04:38:54', '2019-08-17 10:50:50');

-- --------------------------------------------------------

--
-- Table structure for table `targets`
--

CREATE TABLE `targets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `targets` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `targets`
--

INSERT INTO `targets` (`id`, `targets`, `created_at`, `updated_at`) VALUES
(1, 'Wholeseller', '2019-09-25 20:16:30', '2019-09-25 20:16:30'),
(2, 'Manufacturer', '2019-09-25 20:36:22', '2019-09-25 20:49:53'),
(3, 'Retailer', '2019-09-25 20:50:28', '2019-09-25 20:50:28'),
(4, 'Stockist / Warehouse', '2019-09-25 20:50:46', '2019-09-25 20:50:46'),
(5, 'Importer', '2019-09-25 20:50:56', '2019-09-25 20:50:56'),
(6, 'Packaging Facility', '2019-09-25 20:51:07', '2019-09-25 20:51:28');

-- --------------------------------------------------------

--
-- Table structure for table `testings`
--

CREATE TABLE `testings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pdf` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testings`
--

INSERT INTO `testings` (`id`, `image`, `pdf`, `file`, `created_at`, `updated_at`) VALUES
(1, '001.jpg', 'ALLOTMENT OF DIRECTOR IDENTIFICATION NUMBER (DIN).PDF', 'bill.jpg,Birth_certificate.jpg', '2019-12-19 17:45:04', '2019-12-19 17:45:04'),
(3, 'kalam.jpg', 'pan_kalam.pdf', '[\"002.jpg\",\"kalam.jpg\"]', '2019-12-19 18:25:39', '2019-12-19 18:25:39'),
(4, 'self_declaration.jpg', 'pan_kalam.pdf', 'kalam.jpg', '2019-12-19 18:43:11', '2019-12-19 18:43:11'),
(5, 'ip04.jpg', '130597198_1574865272337.pdf', 'ip07.jpg', '2019-12-19 19:20:10', '2019-12-19 19:20:10');

-- --------------------------------------------------------

--
-- Table structure for table `trademarks`
--

CREATE TABLE `trademarks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `app_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prod_id` bigint(20) UNSIGNED DEFAULT NULL,
  `app_date` date NOT NULL,
  `natapl_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `application_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_fee` decimal(10,2) DEFAULT NULL,
  `appf_id` bigint(20) UNSIGNED NOT NULL,
  `app_type_id` bigint(20) UNSIGNED NOT NULL,
  `gov_fee` decimal(10,2) NOT NULL,
  `ent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `nate_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name_in` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `trademark` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catmak_id` bigint(20) UNSIGNED NOT NULL,
  `lang_id` bigint(20) UNSIGNED DEFAULT NULL,
  `desc_mark` text COLLATE utf8mb4_unicode_ci,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `classg_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_person` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authority` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statement_mark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scan_copy_app` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_status` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active Application',
  `sub_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Application Submitted',
  `sp` int(11) NOT NULL DEFAULT '1',
  `ssp` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trademarks`
--

INSERT INTO `trademarks` (`id`, `app_no`, `agent_code`, `prod_id`, `app_date`, `natapl_id`, `user_id`, `application_no`, `agent_fee`, `appf_id`, `app_type_id`, `gov_fee`, `ent_id`, `nate_id`, `name_in`, `agent_id`, `trademark`, `catmak_id`, `lang_id`, `desc_mark`, `remarks`, `classg_id`, `name_person`, `authority`, `logo`, `statement_mark`, `scan_copy_app`, `app_status`, `status`, `sub_status`, `sp`, `ssp`, `created_at`, `updated_at`) VALUES
(1, '50001', '567', 1, '2020-07-15', 1, 22, '1233455', '40000.00', 3, 3, '4500.00', NULL, NULL, 'Rav Jadeh', NULL, 'Shastra', 1, 2, 'Education portal logo', 'Logo of an company', '3', 'Kalam Shaikh', 'Associare', 'logof.png', 'districts.pdf', 'aws.pdf', 0, 'Abandoned or rejected', '', 3, 0, '2020-07-24 07:52:14', '2020-08-03 20:19:18'),
(2, '3500', '601', 1, '2020-12-07', 1, 22, '120003586', '10000.00', 5, 5, '4500.00', 1, 2, NULL, NULL, 'Kan Web Solutions', 1, 1, 'Offiical Logo', 'This is my logo file', '4,5', 'Mohan Lal Pyaare', 'Data Entry Operator', 'dollar-1362244_1920.jpg', 'IPOctopus Invoice_04.jpg', 'pdf (2).pdf', 0, 'Active Application', 'Decision pending', 1, 10, '2020-08-03 06:35:31', '2020-08-03 17:12:45'),
(3, '4690', '12500', 1, '2020-07-28', 1, 22, '1350000', '75000.00', 4, 9, '5000.00', 1, 1, NULL, NULL, 'Kan Real Estate Logo', 1, 1, 'Its is an Real Estate Logo', 'Flyer not Comming', '15', 'Mahesh Kondhwal', 'Accountant Cum Data Entry', 'Vers02_02_F_B-01.png', 'IPOctopus Invoice_04.jpg', 'pdf (2).pdf', 0, 'Active Application', 'Counter statment submitted', 1, 8, '2020-08-03 06:40:39', '2020-08-03 19:53:29'),
(4, '6710', '800052', 1, '2020-06-12', 1, 22, '12368001245', '7500.00', 6, 7, '9000.00', 1, 6, NULL, NULL, 'Kan News', 2, 1, 'Kan News Logo', 'Logo of Kan NEws', '37', 'Abdul Kalam Shaikh', 'Senior Data Analyst', 'KanNewsLogoForYoutube.png', 'IPOctopus Invoice_04.jpg', 'pdf (2).pdf', 0, 'Active Application', 'Opposition noticed received', 1, 7, '2020-08-03 06:45:47', '2020-08-03 19:52:47'),
(5, '125014', '123357', 1, '2020-07-15', 2, 22, '12680000', '7500.00', 6, 11, '10000.00', 1, 7, NULL, NULL, 'Cook With Sadiya', 5, 1, 'Video Sound', 'This is an Sound', '9', 'Kalam Shaikh', 'MD', 'IPOctopus Invoice_04.jpg', 'IPOctopus Invoice_04.jpg', 'Homepage Nido Robotics.pdf', 0, 'Active Application', 'Examination report received', 1, 3, '2020-08-03 06:58:44', '2020-08-03 20:27:14'),
(6, '45088', '123500', 1, '2020-07-02', 3, 22, '2504400', '6500.00', 4, 4, '4500.00', 1, 3, NULL, NULL, 'Kan Academy', 3, 1, 'Kan Academy Logo Colour', 'Color is Green', '19', 'Mukesh Gadge', 'MD', 'IPOctopus Invoice_04.jpg', 'IPOctopus Invoice_04.jpg', 'pdf (2).pdf', 0, 'Active Application', 'Evidence and hearing', 1, 9, '2020-08-03 07:01:17', '2020-08-03 20:10:42'),
(7, '1235659', '123574', 1, '2020-07-02', 4, 22, '145052028', '6500.00', 4, 9, '5000.00', 1, 8, NULL, NULL, 'Sky Faced', 2, 1, 'Logo of the associations', 'Check The Availabilty', '42', 'Kalam Shaikh', 'Senior Data Analyst', 'IPOctopus Invoice_04.jpg', 'IPOctopus Invoice_04.jpg', 'pdf (1).pdf', 0, 'Active Application', 'Accepted and advertised', 1, 6, '2020-08-03 07:03:55', '2020-08-26 14:48:38'),
(8, '4506', '52350', 1, '2020-07-05', 3, 22, '34500012', '5000.00', 3, 8, '5000.00', NULL, NULL, 'Naziya Food Center', NULL, 'Naziya Food Center Logo', 1, 2, 'Logo of the indiviudak Logo', 'Remars', '16', 'Shabnam', 'Mahatre', 'Vers02_02_F_B-01.png', 'IPOctopus Invoice_04.jpg', 'pdf (2).pdf', 0, 'Active Application', 'Hearing for objection', 1, 5, '2020-08-03 07:21:06', '2020-08-03 19:50:54'),
(9, '75620', '6905', 1, '2020-07-14', 4, 22, '135022520', '8500.00', 5, 10, '5000.00', 1, 4, NULL, NULL, 'Society Logp', 3, 1, 'Logo Color', 'Remarks', '45', 'Ajay Devgan', 'Lawyers', 'IPOctopus Invoice_04.jpg', 'IPOctopus Invoice_04.jpg', 'pdf (1).pdf', 0, 'Active Application', 'Replied to examination report', 1, 4, '2020-08-03 07:25:42', '2020-08-03 19:49:46'),
(10, '456025', '12350', 1, '2020-07-22', 2, 22, '450525526', '4500.00', 4, 4, '4500.00', 1, 1, NULL, NULL, 'Xyz Logo', 4, 2, 'Hindi logo', 'This is an Hindi Logo', '16', 'Ram Das Gore', 'Senior Data Analyst', 'IPOctopus Invoice_04.jpg', 'IPOctopus Invoice_04.jpg', 'pdf.pdf', 0, 'Active Application', 'Examination report received', 1, 3, '2020-08-03 07:30:23', '2020-08-03 19:48:19'),
(11, '6900', '1235820', 1, '2020-07-10', 3, 22, '54826980', '7500.00', 3, 8, '5000.00', NULL, NULL, 'Nasreen Cake Shop', NULL, 'Cake Shop Logo', 4, 1, 'Only Cake should be register as trademark', 'This is illogical xyz', '22,34', 'Ali Mirza Guru', 'Data Miner', 'IPOctopus Invoice_04.jpg', 'IPOctopus Invoice_04.jpg', 'pdf.pdf', 0, 'Registered', '', 2, 0, '2020-08-03 07:35:54', '2020-08-03 19:46:25'),
(12, '75469', '6935', 1, '2020-07-06', 2, 22, '693658946', '9500.00', 6, 11, '10000.00', 1, 7, NULL, NULL, 'Jindal Co Associate', 2, 1, 'Trade mark for agency', 'None', '8', 'Kalam Shaikh', 'Managers', 'IPOctopus Invoice_04.jpg', 'IPOctopus Invoice_04.jpg', 'pdf.pdf', 0, 'Withdrawn', '', 4, 0, '2020-08-03 07:39:43', '2020-08-26 14:46:43'),
(13, '456920', '65820', 1, '2020-07-17', 2, 22, '5406826920', '9820.00', 3, 3, '4500.00', NULL, NULL, 'Shifa Clinics', NULL, 'Shifa Clinic', 2, 1, 'Shifa Clinic Isolotions', 'None of them', '2', 'Galwan Ghaati', 'Senior Inspector', 'IPOctopus Invoice_04.jpg', 'IPOctopus Invoice_04.jpg', 'pdf.pdf', 0, 'Active Application', 'Application Submitted', 1, 1, '2020-08-03 07:44:45', '2020-08-26 14:45:16');

-- --------------------------------------------------------

--
-- Table structure for table `trademark_gov_fees`
--

CREATE TABLE `trademark_gov_fees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `appf_id` bigint(20) UNSIGNED NOT NULL,
  `app_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fees` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trademark_gov_fees`
--

INSERT INTO `trademark_gov_fees` (`id`, `appf_id`, `app_type`, `fees`, `created_at`, `updated_at`) VALUES
(3, 3, 'Online', '4500.00', '2020-02-27 15:53:27', '2020-02-27 15:53:27'),
(4, 4, 'Online', '4500.00', '2020-02-27 15:53:57', '2020-02-27 15:53:57'),
(5, 5, 'Online', '4500.00', '2020-02-27 15:54:10', '2020-02-27 15:54:10'),
(7, 6, 'Online', '9000.00', '2020-02-27 15:54:55', '2020-02-27 15:54:55'),
(8, 3, 'Offline', '5000.00', '2020-02-27 15:55:14', '2020-02-27 15:55:14'),
(9, 4, 'Offline', '5000.00', '2020-02-27 15:55:26', '2020-02-27 15:55:26'),
(10, 5, 'Offline', '5000.00', '2020-02-27 15:55:38', '2020-02-27 15:55:38'),
(11, 6, 'Offline', '10000.00', '2020-02-27 15:56:01', '2020-02-27 15:56:01');

-- --------------------------------------------------------

--
-- Table structure for table `trademark_statuses`
--

CREATE TABLE `trademark_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trademark_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trademark_statuses`
--

INSERT INTO `trademark_statuses` (`id`, `trademark_status`, `created_at`, `updated_at`) VALUES
(1, 'Active Trademark', NULL, NULL),
(2, 'Marked for Exam', NULL, NULL),
(3, 'Examination report received', NULL, NULL),
(4, 'Replied to examination report', NULL, NULL),
(5, 'Hearing for objection', NULL, NULL),
(6, 'Accepted and advertised', NULL, NULL),
(7, 'Opposition notice received', NULL, NULL),
(8, 'Counter statement submitted', NULL, NULL),
(9, 'Evidence and  hearing', NULL, NULL),
(10, 'Decision pending', NULL, NULL),
(11, 'Registered', NULL, NULL),
(12, 'Abandoned or rejected', NULL, NULL),
(13, 'Withdrawn', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trade_classes`
--

CREATE TABLE `trade_classes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trade_classes`
--

INSERT INTO `trade_classes` (`id`, `name`, `type`, `desc`, `created_at`, `updated_at`) VALUES
(1, 'Class 1', 'Class of Goods', 'Chemical used in industry, science, photography, agriculture, horticulture and forestry; unprocessed artificial resins, unprocessed plastics; manures; fire extinguishing compositions; tempering and soldering preparations; chemical substances for preserving foodstuffs; tanning substances; adhesive used in industry', '2020-03-01 17:03:33', '2020-03-01 17:03:33'),
(2, 'Class 2', 'Class of Goods', 'Paints, varnishes, lacquers; preservatives against rust and against deterioration of wood; colorants; mordents; raw natural resins; metals in foil and powder form for painters; decorators; printers and artists', '2020-03-01 17:09:09', '2020-03-01 17:09:09'),
(3, 'Class 3', 'Class of Goods', 'Bleaching preparations and other substances for laundry use; cleaning; polishing; scouring and abrasive preparations; soaps; perfumery, essential oils, cosmetics, hair lotions, dentifrices', '2020-03-01 17:12:04', '2020-03-01 17:12:04'),
(4, 'Class 4', 'Class of Goods', 'Industrial oils and greases; lubricants; dust absorbing, wetting and binding compositions; fuels(including motor spirit) and illuminants; candles, wicks', '2020-03-01 17:12:37', '2020-03-01 17:12:37'),
(5, 'Class 5', 'Class of Goods', 'Pharmaceutical, veterinary and sanitary preparations; dietetic substances adapted for medical use, food for babies; plasters, materials for dressings; materials for stopping teeth, dental wax; disinfectants; preparation for destroying vermin; fungicides, herbicides', '2020-03-01 17:13:13', '2020-03-01 17:13:13'),
(6, 'Class 6', 'Class of Goods', 'Common metals and their alloys; metal building materials;transportable buildings of metal; materials of metal for railway tracks; non-electric cables and wires of common metal; ironmongery, small items of metal hardware; pipes and tubes of metal; safes; goods of common metal not included in other classes; ores', '2020-03-01 17:13:33', '2020-03-01 17:18:01'),
(7, 'Class 7', 'Class of Goods', 'Machines and machine tools; motors and engines (except for land vehicles); machine coupling and transmission components (except for land vehicles); agricultural implements other than hand-operated; incubators for eggs', '2020-03-01 17:19:10', '2020-03-01 17:19:10'),
(8, 'Class 8', 'Class of Goods', 'Hand tools and implements (hand-operated); cutlery; side arms; razors', '2020-03-01 17:19:32', '2020-03-01 17:19:32'),
(9, 'Class 9', 'Class of Goods', 'Scientific, nautical, surveying, electric, photographic, cinematographic, optical, weighing, measuring, signalling, checking (supervision), life saving and teaching apparatus and instruments; apparatus for recording, transmission or reproduction of sound or images; magnetic data carriers, recording discs; automatic vending machines and mechanisms for coin-operated apparatus; cash registers, calculating machines, data processing equipment and computers; fire extinguishing apparatus', '2020-03-01 17:19:51', '2020-03-01 17:19:51'),
(10, 'Class 10', 'Class of Goods', 'Surgical, medical, dental and veterinary apparatus and instruments, artificial limbs, eyes and teeth; orthopaedic articles; suture materials', '2020-03-01 17:20:19', '2020-03-01 17:20:19'),
(11, 'Class 11', 'Class of Goods', 'Apparatus for lighting, heating, steam generating, cooking, refrigerating, drying ventilating, water supply and sanitary purposes', '2020-03-01 17:20:41', '2020-03-01 17:20:41'),
(12, 'Class 12', 'Class of Goods', 'Vehicles; apparatus for locomotion by land, air or water', '2020-03-01 17:21:10', '2020-03-01 17:21:10'),
(13, 'Class 13', 'Class of Goods', 'Firearms; ammunition and projectiles; explosives; fire works', '2020-03-01 17:21:30', '2020-03-01 17:21:30'),
(14, 'Class 14', 'Class of Goods', 'Precious metals and their alloys and goods in precious metals or coated therewith, not included in other classes; jewellery, precious stones; horological and other chronometric instruments', '2020-03-01 17:21:54', '2020-03-01 17:21:54'),
(15, 'Class 15', 'Class of Goods', 'Musical instruments', '2020-03-01 17:22:16', '2020-03-01 17:22:16'),
(16, 'Class 16', 'Class of Goods', 'Paper, cardboard and goods made from these materials, not included in other classes; printed matter; bookbinding material; photographs; stationery; adhesives for stationery or household purposes; artists\' materials; paint brushes; typewriters and office requisites (except furniture); instructional and teaching material (except apparatus); plastic materials for packaging (not included in other classes); playing cards; printers\' type; printing blocks', '2020-03-01 17:22:49', '2020-03-01 17:22:49'),
(17, 'Class 17', 'Class of Goods', 'Rubber, gutta percha, gum, asbestos, mica and goods made from these materials and not included in other classes; plastics in extruded form for use in manufacture; packing, stopping and insulating materials; flexible pipes, not of metal', '2020-03-01 17:23:43', '2020-03-01 17:23:43'),
(18, 'Class 18', 'Class of Goods', 'Leather and imitations of leather, and goods made of these materials and not included in other classes; animal skins, hides, trunks and travelling bags; umbrellas, parasols and walking sticks; whips, harness and saddlery', '2020-03-01 17:24:12', '2020-03-01 17:24:12'),
(19, 'Class 19', 'Class of Goods', 'Building materials, (non-metallic), non-metallic rigid pipes for building; asphalt, pitch and bitumen; non-metallic transportable buildings; monuments, not of metal', '2020-03-01 17:24:39', '2020-03-01 17:24:39'),
(20, 'Class 20', 'Class of Goods', 'Furniture, mirrors, picture frames; goods(not included in other classes) of wood, cork, reed, cane, wicker, horn, bone, ivory, whalebone, shell, amber, mother- of-pearl, meerschaum and substitutes for all these materials, or of plastics', '2020-03-01 17:24:58', '2020-03-01 17:24:58'),
(21, 'Class 21', 'Class of Goods', 'Household or kitchen utensils and containers(not of precious metal or coated therewith); combs and sponges; brushes(except paints brushes); brush making materials; articles for cleaning purposes; steelwool; unworked or semi-worked glass (except glass used in building); glassware, porcelain and earthenware not included in other classes', '2020-03-01 17:25:23', '2020-03-01 17:25:23'),
(22, 'Class 22', 'Class of Goods', 'Ropes, string, nets, tents, awnings, tarpaulins, sails, sacks and bags (not included in other classes) padding and stuffing materials(except of rubber or plastics); raw fibrous textile materials', '2020-03-01 17:25:50', '2020-03-01 17:25:50'),
(23, 'Class 23', 'Class of Goods', 'Yarns and threads, for textile use', '2020-03-01 17:26:08', '2020-03-01 17:26:08'),
(24, 'Class 24', 'Class of Goods', 'Textiles and textile goods, not included in other classes; bed and table covers.', '2020-03-01 17:26:30', '2020-03-01 17:26:30'),
(25, 'Class 25', 'Class of Goods', 'Clothing, footwear, headgear', '2020-03-01 17:26:49', '2020-03-01 17:26:49'),
(26, 'Class 26', 'Class of Goods', 'Lace and embroidery, ribbons and braid; buttons, hooks and eyes, pins and needles; artificial flowers', '2020-03-01 17:27:13', '2020-03-01 17:27:13'),
(27, 'Class 27', 'Class of Goods', 'Carpets, rugs, mats and matting, linoleum and other materials for covering existing floors; wall hangings(non-textile)', '2020-03-01 17:27:35', '2020-03-01 17:27:35'),
(28, 'Class 28', 'Class of Goods', 'Games and playthings, gymnastic and sporting articles not included in other classes; decorations for Christmas trees', '2020-03-01 17:27:58', '2020-03-01 17:27:58'),
(29, 'Class 29', 'Class of Goods', 'Meat, fish, poultry and game; meat extracts; preserved, dried and cooked fruits and vegetables; jellies, jams, fruit sauces; eggs, milk and milk products; edible oils and fats', '2020-03-01 17:28:24', '2020-03-01 17:28:24'),
(30, 'Class 30', 'Class of Goods', 'Coffee, tea, cocoa, sugar, rice, tapioca, sago, artificial coffee; flour and preparations made from cereals, bread, pastry and confectionery, ices; honey, treacle; yeast, baking powder; salt, mustard; vinegar, sauces, (condiments); spices; ice', '2020-03-01 17:28:45', '2020-03-01 17:28:45'),
(31, 'Class 31', 'Class of Goods', 'Agricultural, horticultural and forestry products and grains not included in other classes; live animals; fresh fruits and vegetables; seeds, natural plants and flowers; foodstuffs for animals, malt', '2020-03-01 17:29:15', '2020-03-01 17:29:15'),
(32, 'Class 32', 'Class of Goods', 'Beers, mineral and aerated waters, and other non-alcoholic drinks; fruit drinks and fruit juices; syrups and other preparations for making beverages', '2020-03-01 17:29:37', '2020-03-01 17:29:37'),
(33, 'Class 33', 'Class of Goods', 'Alcoholic beverages(except beers)', '2020-03-01 17:29:58', '2020-03-01 17:29:58'),
(34, 'Class 34', 'Class of Goods', 'Tobacco, smokers\' articles, matches', '2020-03-01 17:30:16', '2020-03-01 17:30:16'),
(35, 'Class 35', 'Class of Services', 'Advertising, business management, business administration, office functions.', '2020-03-01 17:30:46', '2020-03-01 17:30:46'),
(36, 'Class 36', 'Class of Services', '.Insurance, financial affairs; monetary affairs; real estate affairs.', '2020-03-01 17:31:19', '2020-03-01 17:31:19'),
(37, 'Class 37', 'Class of Services', 'Building construction; repair; installation services.', '2020-03-01 17:32:17', '2020-03-01 17:32:17'),
(38, 'Class 38', 'Class of Services', 'Telecommunications', '2020-03-01 17:32:35', '2020-03-01 17:32:35'),
(39, 'Class 39', 'Class of Services', 'Transport; packaging and storage of goods; travel arrangement.', '2020-03-01 17:32:52', '2020-03-01 17:32:52'),
(40, 'Class 40', 'Class of Services', 'Treatment of materials', '2020-03-01 17:33:12', '2020-03-01 17:33:12'),
(41, 'Class 41', 'Class of Services', 'Education; providing of training; entertainment; sporting and cultural activities', '2020-03-01 17:34:00', '2020-03-01 17:34:00'),
(42, 'Class 42', 'Class of Services', 'Scientific and technological services and research and design relating thereto; industrial analysis and research services; design and development of computer hardware and software', '2020-03-01 17:34:50', '2020-03-01 17:34:50'),
(43, 'Class 43', 'Class of Services', 'Services for providing food and drink; temporary accommodation', '2020-03-01 17:35:10', '2020-03-01 17:35:10'),
(44, 'Class 44', 'Class of Services', 'Medical services, veterinary services, hygienic and beauty care for human beings or animals; agriculture, horticulture and forestry services.', '2020-03-01 17:36:22', '2020-03-01 17:36:22'),
(45, 'Class 45', 'Class of Services', 'Legal services; security services for the protection of property and individuals; personal and social services rendered by others to meet the needs of individuals.', '2020-03-01 17:36:48', '2020-03-01 17:36:48');

-- --------------------------------------------------------

--
-- Table structure for table `trad_ass_ins`
--

CREATE TABLE `trad_ass_ins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trad_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lic_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trad_ass_outs`
--

CREATE TABLE `trad_ass_outs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trad_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lic_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trad_lic_ins`
--

CREATE TABLE `trad_lic_ins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trad_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term` int(11) NOT NULL,
  `lic_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trad_lic_outs`
--

CREATE TABLE `trad_lic_outs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trad_id` bigint(20) UNSIGNED DEFAULT NULL,
  `licensor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licensee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term` int(11) NOT NULL,
  `lic_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `consideration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rem_ip` date DEFAULT NULL,
  `ent_date` date DEFAULT NULL,
  `ent_date2` date DEFAULT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reminder` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `unit`, `created_at`, `updated_at`) VALUES
(1, 'Pune', '2019-05-20 01:55:49', '2019-05-20 02:16:07'),
(2, 'Navi Mumbai', '2019-06-17 12:33:16', '2019-08-17 10:56:39');

-- --------------------------------------------------------

--
-- Table structure for table `updata_status_designs`
--

CREATE TABLE `updata_status_designs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `design_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `docs` text COLLATE utf8mb4_unicode_ci,
  `derrer` date DEFAULT NULL,
  `rep_dead` date DEFAULT NULL,
  `rem1` date DEFAULT NULL,
  `rem2` date DEFAULT NULL,
  `rem3` date DEFAULT NULL,
  `rem4` date DEFAULT NULL,
  `dfh` date DEFAULT NULL,
  `dfrem` date DEFAULT NULL,
  `dor` date DEFAULT NULL,
  `d_renr` date DEFAULT NULL,
  `pub_date` date DEFAULT NULL,
  `jrno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ren_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `sent_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `updata_status_designs`
--

INSERT INTO `updata_status_designs` (`id`, `design_id`, `status`, `sub_status`, `date`, `comment`, `docs`, `derrer`, `rep_dead`, `rem1`, `rem2`, `rem3`, `rem4`, `dfh`, `dfrem`, `dor`, `d_renr`, `pub_date`, `jrno`, `ren_date`, `exp_date`, `sent_status`, `created_at`, `updated_at`) VALUES
(1, 4, 'Active Application', 'Examination Report Received', NULL, 'i have received', 'IMG-20200814-WA0003.jpg', '2020-08-12', '2020-11-21', '2020-08-24', '2020-08-23', '2020-08-23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-08-23 06:50:20', '2020-08-23 06:50:20'),
(2, 3, 'Active Application', 'Examination Report Received', NULL, 'rec', NULL, '2020-08-10', '2020-11-21', '2020-08-28', '2020-08-26', '2020-08-24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-23 07:09:20', '2020-08-23 07:10:57'),
(3, 3, 'Active Application', 'Reply to objection submitted', '2020-08-17', 'rec', 'IMG-20200814-WA0003.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-23 07:10:57', '2020-08-23 07:10:57'),
(4, 2, 'Active Application', 'Examination Report Received', NULL, 'rec', 'IMG-20200814-WA0003.jpg', '2020-08-14', '2020-11-21', '2020-08-26', '2020-08-28', '2020-09-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-23 07:14:03', '2020-08-23 07:14:40'),
(5, 2, 'Active Application', 'Reply to objection submitted', '2020-08-17', 'rep', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-23 07:14:40', '2020-08-23 07:14:40'),
(6, 2, 'Active Application', 'Hearing for Objection', NULL, 'hearing', 'IMG-20200814-WA0003.jpg', NULL, NULL, '2020-08-26', '2020-08-27', '2020-08-28', NULL, '2020-08-29', '2021-02-20', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-08-23 07:17:06', '2020-08-23 07:17:06'),
(7, 5, 'Active Application', 'Removal of Objection Period Extended', NULL, 'Liti management sysyte', 'Litigation Report (2).pdf', NULL, NULL, '2020-08-23', '2020-08-24', '2020-08-25', NULL, NULL, '2021-05-08', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-08-23 10:59:53', '2020-08-23 10:59:53'),
(8, 6, 'Abandoned / Rejected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-23 11:09:04', '2020-08-23 11:09:04'),
(9, 7, 'Registered and Awaiting Publication', NULL, NULL, 'Rec', 'Litigation Report (2).pdf', NULL, NULL, '2030-02-20', '2030-05-21', '2030-07-20', '2030-08-04', NULL, NULL, '2020-08-21', '2030-08-19', NULL, NULL, NULL, NULL, 1, '2020-08-23 11:15:29', '2020-08-23 11:15:29'),
(10, 9, 'Registered and Published', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-22', '1259658', NULL, NULL, 0, '2020-08-23 11:20:31', '2020-08-23 11:20:31'),
(11, 8, 'Expired - For Renewal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-23 11:20:52', '2020-08-23 11:20:52'),
(12, 10, 'Renewed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-23', '2020-09-05', 0, '2020-08-23 11:30:20', '2020-08-23 11:30:20'),
(13, 12, 'Extension Expired', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-23 11:38:33', '2020-08-23 11:38:33');

-- --------------------------------------------------------

--
-- Table structure for table `update_copyright_statuses`
--

CREATE TABLE `update_copyright_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `copy_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `docs` text COLLATE utf8mb4_unicode_ci,
  `mwp_date` date DEFAULT NULL,
  `rem_mwp_date` date DEFAULT NULL,
  `rep_dead` date DEFAULT NULL,
  `rem1` date DEFAULT NULL,
  `rem2` date DEFAULT NULL,
  `rem3` date DEFAULT NULL,
  `dfh` date DEFAULT NULL,
  `ddj` date DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `d_rer` date DEFAULT NULL,
  `dor` date DEFAULT NULL,
  `reg_rem` date DEFAULT NULL,
  `dren` date DEFAULT NULL,
  `ren_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `sent_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `update_copyright_statuses`
--

INSERT INTO `update_copyright_statuses` (`id`, `copy_id`, `status`, `sub_status`, `date`, `comment`, `docs`, `mwp_date`, `rem_mwp_date`, `rep_dead`, `rem1`, `rem2`, `rem3`, `dfh`, `ddj`, `deadline`, `d_rer`, `dor`, `reg_rem`, `dren`, `ren_date`, `exp_date`, `sent_status`, `created_at`, `updated_at`) VALUES
(2, 2, 'Active Application', 'Mandatory waiting period', NULL, NULL, NULL, '2020-08-11', '2020-09-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-08-23 10:11:21', '2020-08-23 10:11:21'),
(3, 14, 'Abandoned', NULL, '2020-08-17', 'this was abandoned', 'Litigation Report (2).pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-23 13:04:11', '2020-08-23 13:04:11'),
(4, 13, 'Rejected', NULL, '2020-08-10', 'rejected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-23 13:12:58', '2020-08-23 13:12:58'),
(5, 12, 'Registered', NULL, NULL, 'Registed', 'Litigation Report (2).pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-08', '1970-01-01', '2080-07-24', NULL, NULL, 0, '2020-08-23 13:13:43', '2020-08-23 13:13:43'),
(6, 11, 'Expired', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-23 13:14:16', '2020-08-23 13:14:16'),
(7, 10, 'Renewed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-23', '2020-09-05', 0, '2020-08-23 13:14:49', '2020-08-23 13:14:49'),
(8, 9, 'Extension expired', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-23 13:15:23', '2020-08-23 13:15:23'),
(9, 8, 'Active Application', 'Opposition', '2020-08-06', 'opp', 'Litigation Report (3).pdf', NULL, NULL, '2020-09-22', '2020-08-24', '2020-08-24', '2020-08-25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-08-23 13:18:14', '2020-08-23 13:18:14'),
(10, 7, 'Active Application', 'Reply to opposition submitted', '2020-08-31', 'this is an', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-23 13:19:06', '2020-08-23 13:19:06'),
(11, 6, 'Active Application', 'Hearing', NULL, 'sub', 'Litigation Report (3).pdf', NULL, NULL, NULL, '2020-08-23', '2020-08-24', '2020-08-25', '2020-08-29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-08-23 13:20:02', '2020-08-23 13:20:02'),
(12, 5, 'Active Application', 'Appeal', NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-23', '2020-08-24', '2020-08-25', NULL, '2020-09-05', '2020-12-04', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-08-23 13:20:51', '2020-08-23 13:20:51'),
(13, 4, 'Active Application', 'Scrutinization stage', NULL, 'receivd', 'Litigation Report (3).pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-22', NULL, NULL, NULL, NULL, NULL, 0, '2020-08-23 13:21:41', '2020-08-23 13:21:41'),
(14, 1, 'Active Application', 'Discrepancy stage', NULL, NULL, 'Litigation Report (2).pdf', NULL, NULL, NULL, '2020-08-23', '2020-08-24', '2020-08-25', NULL, NULL, '2020-09-16', '2020-08-17', NULL, NULL, NULL, NULL, NULL, 0, '2020-08-23 13:22:52', '2020-08-23 13:22:52');

-- --------------------------------------------------------

--
-- Table structure for table `update_statuses`
--

CREATE TABLE `update_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tradmark_id` int(11) NOT NULL,
  `tradmark_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document` text COLLATE utf8mb4_unicode_ci,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `reply_deadline` date DEFAULT NULL,
  `rem1` date DEFAULT NULL,
  `rem12` date DEFAULT NULL,
  `rem3` date DEFAULT NULL,
  `rem4` date DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `recvon` date DEFAULT NULL,
  `deadcounter` date DEFAULT NULL,
  `renew` date DEFAULT NULL,
  `noticd` date DEFAULT NULL,
  `intd` date DEFAULT NULL,
  `sub_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `update_status_patents`
--

CREATE TABLE `update_status_patents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `patent_id` bigint(20) UNSIGNED NOT NULL,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `update_status_patents`
--

INSERT INTO `update_status_patents` (`id`, `patent_id`, `docs`, `comment`, `date`, `status`, `sub_status`, `stage`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, NULL, 'Active Application', 'Specification Stage', NULL, '2019-12-20 05:13:44', '2019-12-20 05:13:44');

-- --------------------------------------------------------

--
-- Table structure for table `update_status_trads`
--

CREATE TABLE `update_status_trads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trade_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `docs` text COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `rem1` date DEFAULT NULL,
  `rem2` date DEFAULT NULL,
  `rem3` date DEFAULT NULL,
  `rem4` date DEFAULT NULL,
  `rec_date` date DEFAULT NULL,
  `rep_date` date DEFAULT NULL,
  `her_date` date DEFAULT NULL,
  `adv_date` date DEFAULT NULL,
  `not_date` date DEFAULT NULL,
  `reo_date` date DEFAULT NULL,
  `evi_date` date DEFAULT NULL,
  `dor_date` date DEFAULT NULL,
  `rep_dead` date DEFAULT NULL,
  `opp_dead` date DEFAULT NULL,
  `cou_dead` date DEFAULT NULL,
  `int_dead` date DEFAULT NULL,
  `ren_date` date DEFAULT NULL,
  `sent_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `update_status_trads`
--

INSERT INTO `update_status_trads` (`id`, `trade_id`, `status`, `sub_status`, `comments`, `docs`, `date`, `rem1`, `rem2`, `rem3`, `rem4`, `rec_date`, `rep_date`, `her_date`, `adv_date`, `not_date`, `reo_date`, `evi_date`, `dor_date`, `rep_dead`, `opp_dead`, `cou_dead`, `int_dead`, `ren_date`, `sent_status`, `created_at`, `updated_at`) VALUES
(30, 12, 'Active Application', 'Examination report received', 'Examination Report Received', 'IPOctopus Invoice_04.jpg', NULL, '2020-08-03', '2020-08-04', '2020-08-05', NULL, '2020-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-31', NULL, NULL, NULL, NULL, 0, '2020-08-03 08:30:22', '2020-08-03 13:03:56'),
(31, 11, 'Active Application', 'Marked for exam', 'Marked For Exam', NULL, '2020-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 08:35:03', '2020-08-03 08:35:03'),
(32, 10, 'Active Application', 'Marked for exam', 'Marked for Exam', NULL, '2020-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 08:38:11', '2020-08-03 08:38:11'),
(33, 9, 'Active Application', 'Marked for exam', 'marked for exam', NULL, '2020-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 08:38:51', '2020-08-03 08:38:51'),
(34, 1, 'Active Application', 'Marked for exam', 'Marked For Exam', NULL, '2020-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 08:39:46', '2020-08-03 08:39:46'),
(35, 2, 'Active Application', 'Marked for exam', 'Marked for Exam', NULL, '2020-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 08:42:18', '2020-08-03 08:42:18'),
(36, 3, 'Active Application', 'Marked for exam', 'Marked For Exam', NULL, '2020-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 08:43:08', '2020-08-03 08:43:08'),
(37, 7, 'Active Application', 'Marked for exam', 'Marked for exam', NULL, '2020-07-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 08:45:30', '2020-08-03 08:45:30'),
(38, 6, 'Active Application', 'Marked for exam', 'Marked For Exam', NULL, '2020-07-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 08:46:03', '2020-08-03 08:46:03'),
(39, 5, 'Active Application', 'Marked for exam', 'Marked For Exam', NULL, '2020-07-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 08:46:36', '2020-08-03 08:46:36'),
(40, 4, 'Active Application', 'Marked for exam', 'Marked For Exam', NULL, '2020-07-30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 08:47:44', '2020-08-03 08:47:44'),
(41, 12, 'Active Application', 'Replied to examination report', 'Replied Examination Report', 'IPOctopus Invoice_04.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 12:34:51', '2020-08-03 12:34:51'),
(42, 12, 'Active Application', 'Hearing for objection', 'Hearing For Objection', 'IPOctopus Invoice_04.jpg,Vers02_02_F_B-01.png', NULL, '2020-08-04', '2020-08-05', '2020-08-06', NULL, NULL, '2020-08-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 12:37:42', '2020-08-03 13:03:56'),
(43, 12, 'Active Application', 'Accepted and advertised', 'Accepted and Advertised', 'IPOctopus Invoice_04.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-03', NULL, NULL, NULL, NULL, NULL, '2020-12-01', NULL, NULL, NULL, 0, '2020-08-03 12:43:26', '2020-08-03 12:43:26'),
(44, 12, 'Active Application', 'Opposition noticed received', 'Opposition Noticed Rceivd', 'IPOctopus Invoice_04.jpg', NULL, '2020-08-26', '2020-09-10', '2020-09-23', NULL, NULL, NULL, NULL, NULL, '2020-07-31', '2020-08-03', NULL, NULL, NULL, NULL, '2020-10-02', '2020-09-25', NULL, 0, '2020-08-03 12:47:00', '2020-08-03 13:03:56'),
(45, 12, 'Active Application', 'Counter statment submitted', 'Counter Statement', 'IPOctopus Invoice_04.jpg', '2020-08-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 12:50:15', '2020-08-03 12:50:15'),
(46, 12, 'Active Application', 'Evidence and hearing', 'Evidence and Hearing', 'IPOctopus Invoice_04.jpg', NULL, '2020-08-04', '2020-08-05', '2020-08-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-03', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 12:55:00', '2020-08-03 13:07:56'),
(47, 12, 'Active Application', 'Decision pending', 'Decision Pending', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 12:57:35', '2020-08-03 12:57:35'),
(48, 12, 'Registered', NULL, 'Registered', 'IPOctopus Invoice_04.jpg', NULL, '2030-02-02', '2030-05-03', '2030-07-02', '2030-07-25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-03', NULL, NULL, NULL, NULL, '2030-08-01', 0, '2020-08-03 13:03:56', '2020-08-03 13:07:56'),
(49, 12, 'Abandoned or rejected', NULL, 'Abandoned', 'IPOctopus Invoice_04.jpg', '2020-08-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 13:07:56', '2020-08-03 13:07:56'),
(50, 12, 'Withdrawn', NULL, 'Withdrawn', 'IPOctopus Invoice_04.jpg', '2020-08-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 13:09:32', '2020-08-03 13:09:32'),
(51, 13, 'Active Application', 'Examination report received', 'xya', 'IPOctopus Invoice_04.jpg', NULL, '2020-08-04', '2020-08-05', '2020-08-06', NULL, '2020-08-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-02', NULL, NULL, NULL, NULL, 0, '2020-08-03 13:11:48', '2020-08-03 17:26:18'),
(52, 11, 'Abandoned or rejected', NULL, 'Rejected', NULL, '2020-08-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 15:36:57', '2020-08-03 15:36:57'),
(54, 2, 'Active Application', 'Decision pending', 'Decision Pending', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 17:12:44', '2020-08-03 17:12:44'),
(55, 7, 'Abandoned or rejected', NULL, 'Rejected', 'IPOctopus Invoice_04.jpg', '2020-08-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 17:24:44', '2020-08-03 17:24:44'),
(56, 13, 'Abandoned or rejected', NULL, 'comdkm', NULL, '2020-08-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 17:26:19', '2020-08-03 17:26:19'),
(57, 11, 'Registered', NULL, 'Registered', 'E_Gazette_User_Manual_PDF_21.pdf', NULL, '2020-08-04', '2020-08-04', '2020-08-04', '2020-08-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-03', NULL, NULL, NULL, NULL, '2030-08-01', 1, '2020-08-03 19:46:25', '2020-08-03 19:46:25'),
(58, 10, 'Active Application', 'Examination report received', 'Examination Report receive', 'E_Gazette_User_Manual_PDF_21.pdf', NULL, '2020-08-04', '2020-08-04', '2020-08-04', NULL, '2020-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-31', NULL, NULL, NULL, NULL, 1, '2020-08-03 19:48:19', '2020-08-03 19:48:19'),
(59, 9, 'Active Application', 'Replied to examination report', 'Replied', 'E_Gazette_User_Manual_PDF_21.pdf', NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 19:49:46', '2020-08-03 19:49:46'),
(60, 8, 'Active Application', 'Hearing for objection', 'Hearing for Objection', 'E_Gazette_User_Manual_PDF_21.pdf', NULL, '2020-08-04', '2020-08-04', '2020-08-04', NULL, NULL, NULL, '2020-08-25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-08-03 19:50:54', '2020-08-03 19:50:54'),
(61, 7, 'Active Application', 'Accepted and advertised', 'Accepted', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-01', NULL, NULL, NULL, NULL, NULL, '2020-11-29', NULL, NULL, NULL, 0, '2020-08-03 19:51:44', '2020-08-03 19:51:44'),
(62, 4, 'Active Application', 'Opposition noticed received', 'Oppopition Noticed Received', NULL, NULL, '2020-08-04', '2020-08-04', '2020-08-04', NULL, NULL, NULL, NULL, NULL, '2020-08-01', '1970-01-01', NULL, NULL, NULL, NULL, '2020-10-03', '2020-09-26', NULL, 1, '2020-08-03 19:52:46', '2020-08-03 19:52:46'),
(63, 3, 'Active Application', 'Counter statment submitted', 'Counter Statement', NULL, '2020-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 19:53:28', '2020-08-03 19:53:28'),
(64, 6, 'Active Application', 'Evidence and hearing', 'Evidence and Hearng', NULL, NULL, '2020-08-04', '2020-08-04', '2020-08-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-20', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-08-03 20:10:42', '2020-08-03 20:10:42'),
(65, 1, 'Abandoned or rejected', NULL, 'Rejected', NULL, '2020-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-08-03 20:19:18', '2020-08-03 20:19:18'),
(66, 5, 'Active Application', 'Examination report received', 'Re', NULL, NULL, '2020-08-04', '2020-08-04', '2020-08-04', NULL, '2020-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-31', NULL, NULL, NULL, NULL, 1, '2020-08-03 20:27:14', '2020-08-03 20:27:14');

-- --------------------------------------------------------

--
-- Table structure for table `update_status__copyrights`
--

CREATE TABLE `update_status__copyrights` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `copy_id` int(11) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `rem` date DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_deadline` date DEFAULT NULL,
  `rem1` date DEFAULT NULL,
  `rem2` date DEFAULT NULL,
  `rem3` date DEFAULT NULL,
  `auto_deadline` date DEFAULT NULL,
  `cor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dor` date DEFAULT NULL,
  `doe` date DEFAULT NULL,
  `roc` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `update_status__copyrights`
--

INSERT INTO `update_status__copyrights` (`id`, `copy_id`, `status`, `sub_status`, `date`, `rem`, `comment`, `docs`, `reply_deadline`, `rem1`, `rem2`, `rem3`, `auto_deadline`, `cor`, `dor`, `doe`, `roc`, `created_at`, `updated_at`) VALUES
(9, 5, 'Abandoned', NULL, '2020-01-20', NULL, 'This is fine', 'sample.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-13 07:55:06', '2020-01-13 07:55:06'),
(10, 5, 'Active Application', 'Mandatory waiting period', NULL, '2020-01-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-13 08:00:03', '2020-01-13 08:00:03'),
(11, 2, 'Active Application', 'Mandatory waiting period', NULL, '2020-07-30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-08 18:32:07', '2020-07-08 18:32:07');

-- --------------------------------------------------------

--
-- Table structure for table `update__status__designs`
--

CREATE TABLE `update__status__designs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `des_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `docs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rep_d` date DEFAULT NULL,
  `rem1` date DEFAULT NULL,
  `rem2` date DEFAULT NULL,
  `rem3` date DEFAULT NULL,
  `rem4` date DEFAULT NULL,
  `cor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dor` date DEFAULT NULL,
  `jno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doe` date DEFAULT NULL,
  `her_date` date DEFAULT NULL,
  `dead_rem` date DEFAULT NULL,
  `dead_rem_ob` date DEFAULT NULL,
  `doren` date DEFAULT NULL,
  `dorenew` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `update__status__designs`
--

INSERT INTO `update__status__designs` (`id`, `des_id`, `status`, `sub_status`, `date`, `comment`, `docs`, `rep_d`, `rem1`, `rem2`, `rem3`, `rem4`, `cor`, `dor`, `jno`, `doe`, `her_date`, `dead_rem`, `dead_rem_ob`, `doren`, `dorenew`, `created_at`, `updated_at`) VALUES
(7, 2, 'Abandoned', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-13 07:42:11', '2020-01-13 07:42:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enttity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `function` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `mobile_no`, `email`, `user_name`, `password`, `enttity`, `unit`, `function`, `employee_id`, `designation`, `role`, `address`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(22, 'Mukesh', 'Gadge', '09922113707', 'mukesh.gadge@lexcareglobal.com', 'Muk123', '$2y$10$BaRUnuwQ9xLc8TqnrbqAZ.HCwqKmgNLYUakkmIGucFrvNfIpOXwQO', 'Lexschool', 'Navi Mumbai', 'Finance', '0002', 'IP Head', 'Administrator', 'xyz', '0', NULL, '2020-02-03 11:17:10', '2020-08-26 23:15:36'),
(23, 'Abdu Kalam', 'Shaikh', '9591785319', 'kashaikh611@gmail.com', 'Kalam611', '$2y$10$MN6P4mYyjD1gU01H/zrg1eBAn9Q5skxgpYeempyUzTSutON.qtgFC', 'Aara Web Solutions', 'Navi Mumbai', 'IT', '101', 'BP Manager', 'Administrator', '1202,Patel Residency,Plot No.84', '0', NULL, '2020-08-17 07:44:30', '2020-08-17 07:44:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action_items`
--
ALTER TABLE `action_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advocates`
--
ALTER TABLE `advocates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agencies`
--
ALTER TABLE `agencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applicants`
--
ALTER TABLE `applicants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `application_fields`
--
ALTER TABLE `application_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `area_exps`
--
ALTER TABLE `area_exps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_applicants`
--
ALTER TABLE `category_applicants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_marks`
--
ALTER TABLE `category_marks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_good_services`
--
ALTER TABLE `class_good_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `completions`
--
ALTER TABLE `completions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `completions_liti_id_foreign` (`liti_id`);

--
-- Indexes for table `copyrightns`
--
ALTER TABLE `copyrightns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `copyrightns_agent_id_foreign` (`agent_id`),
  ADD KEY `copyrightns_user_id_foreign` (`user_id`),
  ADD KEY `copyrightns_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `copyrights`
--
ALTER TABLE `copyrights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `copyright_lic_ins`
--
ALTER TABLE `copyright_lic_ins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `copyright_lic_ins_licin_id_foreign` (`licin_id`);

--
-- Indexes for table `copyright_lic_outs`
--
ALTER TABLE `copyright_lic_outs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `copyright_lic_outs_licin_id_foreign` (`licin_id`);

--
-- Indexes for table `copy_ass_ins`
--
ALTER TABLE `copy_ass_ins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `copy_ass_ins_copy_id_foreign` (`copy_id`);

--
-- Indexes for table `copy_ass_outs`
--
ALTER TABLE `copy_ass_outs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `copy_ass_outs_copy_id_foreign` (`copy_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `court_lists`
--
ALTER TABLE `court_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designation_lists`
--
ALTER TABLE `designation_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designms`
--
ALTER TABLE `designms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `designms_agent_id_foreign` (`agent_id`),
  ADD KEY `designms_user_id_foreign` (`user_id`),
  ADD KEY `designms_natappli_id_foreign` (`natappli_id`),
  ADD KEY `designms_ent_id_foreign` (`ent_id`),
  ADD KEY `designms_natent_id_foreign` (`natent_id`),
  ADD KEY `designms_classg_id_foreign` (`classg_id`),
  ADD KEY `designms_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `designs`
--
ALTER TABLE `designs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designss`
--
ALTER TABLE `designss`
  ADD PRIMARY KEY (`id`),
  ADD KEY `designss_agent_id_foreign` (`agent_id`),
  ADD KEY `designss_user_id_foreign` (`user_id`),
  ADD KEY `designss_natappli_id_foreign` (`natappli_id`),
  ADD KEY `designss_ent_id_foreign` (`ent_id`),
  ADD KEY `designss_natent_id_foreign` (`natent_id`),
  ADD KEY `designss_classg_id_foreign` (`classg_id`),
  ADD KEY `designss_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `design_classes`
--
ALTER TABLE `design_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `design_lic_ins`
--
ALTER TABLE `design_lic_ins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `design_lic_ins_des_id_foreign` (`des_id`);

--
-- Indexes for table `design_lic_outs`
--
ALTER TABLE `design_lic_outs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `design_lic_outs_des_id_foreign` (`des_id`);

--
-- Indexes for table `des_ass_ins`
--
ALTER TABLE `des_ass_ins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `des_ass_ins_des_id_foreign` (`des_id`);

--
-- Indexes for table `des_ass_outs`
--
ALTER TABLE `des_ass_outs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `des_ass_outs_des_id_foreign` (`des_id`);

--
-- Indexes for table `dynamic_fields`
--
ALTER TABLE `dynamic_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enforcements`
--
ALTER TABLE `enforcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enfs`
--
ALTER TABLE `enfs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `enfs_ent_id_foreign` (`ent_id`),
  ADD KEY `enfs_country_id_foreign` (`country_id`),
  ADD KEY `enfs_state_id_foreign` (`state_id`);

--
-- Indexes for table `entity_lists`
--
ALTER TABLE `entity_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entity_mappings`
--
ALTER TABLE `entity_mappings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenseins`
--
ALTER TABLE `expenseins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expensens`
--
ALTER TABLE `expensens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `external_counsels`
--
ALTER TABLE `external_counsels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `firs`
--
ALTER TABLE `firs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `function_lists`
--
ALTER TABLE `function_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hearings`
--
ALTER TABLE `hearings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hearings_hear_id_foreign` (`hear_id`),
  ADD KEY `hearings_user_id_foreign` (`user_id`);

--
-- Indexes for table `investigations`
--
ALTER TABLE `investigations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invests`
--
ALTER TABLE `invests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invests_ent_id_foreign` (`ent_id`),
  ADD KEY `invests_inag_id_foreign` (`inag_id`),
  ADD KEY `invests_user_id_foreign` (`user_id`),
  ADD KEY `invests_country_id_foreign` (`country_id`),
  ADD KEY `invests_state_id_foreign` (`state_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `law_firms`
--
ALTER TABLE `law_firms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leads_target_id_foreign` (`target_id`);

--
-- Indexes for table `legal_notices`
--
ALTER TABLE `legal_notices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `litigation_notices`
--
ALTER TABLE `litigation_notices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `litigation_notices_user_id_foreign` (`user_id`);

--
-- Indexes for table `litigation_summaries`
--
ALTER TABLE `litigation_summaries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `litigation_summaries_user_id_foreign` (`user_id`);

--
-- Indexes for table `liti_docs`
--
ALTER TABLE `liti_docs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `liti_docs_doc_id_foreign` (`doc_id`);

--
-- Indexes for table `liti_types`
--
ALTER TABLE `liti_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nature_agents`
--
ALTER TABLE `nature_agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nature_applicants`
--
ALTER TABLE `nature_applicants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nature_applications`
--
ALTER TABLE `nature_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice_statuses`
--
ALTER TABLE `notice_statuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notice_statuses_stat_id_foreign` (`stat_id`),
  ADD KEY `notice_statuses_user_id_foreign` (`user_id`);

--
-- Indexes for table `parent_entities`
--
ALTER TABLE `parent_entities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `patents`
--
ALTER TABLE `patents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patents_user_id_foreign` (`user_id`);

--
-- Indexes for table `patent_ass_outs`
--
ALTER TABLE `patent_ass_outs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patent_lic_ins`
--
ALTER TABLE `patent_lic_ins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patent_lic_ins_patent_id_foreign` (`patent_id`);

--
-- Indexes for table `pat_ass_ins`
--
ALTER TABLE `pat_ass_ins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pat_ass_ins_pat_id_foreign` (`pat_id`);

--
-- Indexes for table `pat_ass_outs`
--
ALTER TABLE `pat_ass_outs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pat_ass_outs_pat_id_foreign` (`pat_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stages`
--
ALTER TABLE `stages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stetes`
--
ALTER TABLE `stetes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stetes_country_id_foreign` (`country_id`);

--
-- Indexes for table `sub_class_goods_services`
--
ALTER TABLE `sub_class_goods_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `targets`
--
ALTER TABLE `targets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testings`
--
ALTER TABLE `testings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trademarks`
--
ALTER TABLE `trademarks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trademarks_prod_id_foreign` (`prod_id`),
  ADD KEY `trademarks_natapl_id_foreign` (`natapl_id`),
  ADD KEY `trademarks_user_id_foreign` (`user_id`),
  ADD KEY `trademarks_appf_id_foreign` (`appf_id`),
  ADD KEY `trademarks_app_type_id_foreign` (`app_type_id`),
  ADD KEY `trademarks_ent_id_foreign` (`ent_id`),
  ADD KEY `trademarks_nate_id_foreign` (`nate_id`),
  ADD KEY `trademarks_agent_id_foreign` (`agent_id`),
  ADD KEY `trademarks_catmak_id_foreign` (`catmak_id`),
  ADD KEY `trademarks_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `trademark_gov_fees`
--
ALTER TABLE `trademark_gov_fees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trademark_gov_fees_appf_id_foreign` (`appf_id`);

--
-- Indexes for table `trademark_statuses`
--
ALTER TABLE `trademark_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trade_classes`
--
ALTER TABLE `trade_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trad_ass_ins`
--
ALTER TABLE `trad_ass_ins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trad_ass_ins_trad_id_foreign` (`trad_id`);

--
-- Indexes for table `trad_ass_outs`
--
ALTER TABLE `trad_ass_outs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trad_ass_outs_trad_id_foreign` (`trad_id`);

--
-- Indexes for table `trad_lic_ins`
--
ALTER TABLE `trad_lic_ins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trad_lic_ins_trad_id_foreign` (`trad_id`);

--
-- Indexes for table `trad_lic_outs`
--
ALTER TABLE `trad_lic_outs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trad_lic_outs_trad_id_foreign` (`trad_id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `updata_status_designs`
--
ALTER TABLE `updata_status_designs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `updata_status_designs_design_id_foreign` (`design_id`);

--
-- Indexes for table `update_copyright_statuses`
--
ALTER TABLE `update_copyright_statuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `update_copyright_statuses_copy_id_foreign` (`copy_id`);

--
-- Indexes for table `update_statuses`
--
ALTER TABLE `update_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `update_status_patents`
--
ALTER TABLE `update_status_patents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `update_status_trads`
--
ALTER TABLE `update_status_trads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `update_status_trads_trade_id_foreign` (`trade_id`);

--
-- Indexes for table `update_status__copyrights`
--
ALTER TABLE `update_status__copyrights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `update__status__designs`
--
ALTER TABLE `update__status__designs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `action_items`
--
ALTER TABLE `action_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `advocates`
--
ALTER TABLE `advocates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `agencies`
--
ALTER TABLE `agencies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `agents`
--
ALTER TABLE `agents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `applicants`
--
ALTER TABLE `applicants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `application_fields`
--
ALTER TABLE `application_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `area_exps`
--
ALTER TABLE `area_exps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category_applicants`
--
ALTER TABLE `category_applicants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `category_marks`
--
ALTER TABLE `category_marks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `class_good_services`
--
ALTER TABLE `class_good_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `completions`
--
ALTER TABLE `completions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `copyrightns`
--
ALTER TABLE `copyrightns`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `copyrights`
--
ALTER TABLE `copyrights`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `copyright_lic_ins`
--
ALTER TABLE `copyright_lic_ins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `copyright_lic_outs`
--
ALTER TABLE `copyright_lic_outs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `copy_ass_ins`
--
ALTER TABLE `copy_ass_ins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `copy_ass_outs`
--
ALTER TABLE `copy_ass_outs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `court_lists`
--
ALTER TABLE `court_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `designation_lists`
--
ALTER TABLE `designation_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `designms`
--
ALTER TABLE `designms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `designs`
--
ALTER TABLE `designs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `designss`
--
ALTER TABLE `designss`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `design_classes`
--
ALTER TABLE `design_classes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `design_lic_ins`
--
ALTER TABLE `design_lic_ins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `design_lic_outs`
--
ALTER TABLE `design_lic_outs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `des_ass_ins`
--
ALTER TABLE `des_ass_ins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `des_ass_outs`
--
ALTER TABLE `des_ass_outs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dynamic_fields`
--
ALTER TABLE `dynamic_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `enforcements`
--
ALTER TABLE `enforcements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `enfs`
--
ALTER TABLE `enfs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `entity_lists`
--
ALTER TABLE `entity_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `entity_mappings`
--
ALTER TABLE `entity_mappings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `expenseins`
--
ALTER TABLE `expenseins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `expensens`
--
ALTER TABLE `expensens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `external_counsels`
--
ALTER TABLE `external_counsels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `firs`
--
ALTER TABLE `firs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `function_lists`
--
ALTER TABLE `function_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `hearings`
--
ALTER TABLE `hearings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `investigations`
--
ALTER TABLE `investigations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `invests`
--
ALTER TABLE `invests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `law_firms`
--
ALTER TABLE `law_firms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legal_notices`
--
ALTER TABLE `legal_notices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `litigation_notices`
--
ALTER TABLE `litigation_notices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `litigation_summaries`
--
ALTER TABLE `litigation_summaries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `liti_docs`
--
ALTER TABLE `liti_docs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `liti_types`
--
ALTER TABLE `liti_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `nature_agents`
--
ALTER TABLE `nature_agents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nature_applicants`
--
ALTER TABLE `nature_applicants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `nature_applications`
--
ALTER TABLE `nature_applications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `notice_statuses`
--
ALTER TABLE `notice_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `parent_entities`
--
ALTER TABLE `parent_entities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `patents`
--
ALTER TABLE `patents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `patent_ass_outs`
--
ALTER TABLE `patent_ass_outs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patent_lic_ins`
--
ALTER TABLE `patent_lic_ins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pat_ass_ins`
--
ALTER TABLE `pat_ass_ins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pat_ass_outs`
--
ALTER TABLE `pat_ass_outs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `stages`
--
ALTER TABLE `stages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stetes`
--
ALTER TABLE `stetes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sub_class_goods_services`
--
ALTER TABLE `sub_class_goods_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `targets`
--
ALTER TABLE `targets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `testings`
--
ALTER TABLE `testings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `trademarks`
--
ALTER TABLE `trademarks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `trademark_gov_fees`
--
ALTER TABLE `trademark_gov_fees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `trademark_statuses`
--
ALTER TABLE `trademark_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `trade_classes`
--
ALTER TABLE `trade_classes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `trad_ass_ins`
--
ALTER TABLE `trad_ass_ins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trad_ass_outs`
--
ALTER TABLE `trad_ass_outs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trad_lic_ins`
--
ALTER TABLE `trad_lic_ins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trad_lic_outs`
--
ALTER TABLE `trad_lic_outs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `updata_status_designs`
--
ALTER TABLE `updata_status_designs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `update_copyright_statuses`
--
ALTER TABLE `update_copyright_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `update_statuses`
--
ALTER TABLE `update_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `update_status_patents`
--
ALTER TABLE `update_status_patents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `update_status_trads`
--
ALTER TABLE `update_status_trads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `update_status__copyrights`
--
ALTER TABLE `update_status__copyrights`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `update__status__designs`
--
ALTER TABLE `update__status__designs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `completions`
--
ALTER TABLE `completions`
  ADD CONSTRAINT `completions_liti_id_foreign` FOREIGN KEY (`liti_id`) REFERENCES `litigation_summaries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `copyrightns`
--
ALTER TABLE `copyrightns`
  ADD CONSTRAINT `copyrightns_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `copyrightns_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `copyrightns_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `copyright_lic_ins`
--
ALTER TABLE `copyright_lic_ins`
  ADD CONSTRAINT `copyright_lic_ins_licin_id_foreign` FOREIGN KEY (`licin_id`) REFERENCES `copyrights` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `copyright_lic_outs`
--
ALTER TABLE `copyright_lic_outs`
  ADD CONSTRAINT `copyright_lic_outs_licin_id_foreign` FOREIGN KEY (`licin_id`) REFERENCES `copyrights` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `copy_ass_ins`
--
ALTER TABLE `copy_ass_ins`
  ADD CONSTRAINT `copy_ass_ins_copy_id_foreign` FOREIGN KEY (`copy_id`) REFERENCES `copyrights` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `copy_ass_outs`
--
ALTER TABLE `copy_ass_outs`
  ADD CONSTRAINT `copy_ass_outs_copy_id_foreign` FOREIGN KEY (`copy_id`) REFERENCES `copyrights` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `designms`
--
ALTER TABLE `designms`
  ADD CONSTRAINT `designms_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designms_classg_id_foreign` FOREIGN KEY (`classg_id`) REFERENCES `design_classes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designms_ent_id_foreign` FOREIGN KEY (`ent_id`) REFERENCES `entity_lists` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designms_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designms_natappli_id_foreign` FOREIGN KEY (`natappli_id`) REFERENCES `application_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designms_natent_id_foreign` FOREIGN KEY (`natent_id`) REFERENCES `nature_applicants` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designms_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `designss`
--
ALTER TABLE `designss`
  ADD CONSTRAINT `designss_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designss_classg_id_foreign` FOREIGN KEY (`classg_id`) REFERENCES `design_classes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designss_ent_id_foreign` FOREIGN KEY (`ent_id`) REFERENCES `entity_lists` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designss_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designss_natappli_id_foreign` FOREIGN KEY (`natappli_id`) REFERENCES `application_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designss_natent_id_foreign` FOREIGN KEY (`natent_id`) REFERENCES `nature_applicants` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designss_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `design_lic_ins`
--
ALTER TABLE `design_lic_ins`
  ADD CONSTRAINT `design_lic_ins_des_id_foreign` FOREIGN KEY (`des_id`) REFERENCES `designs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `design_lic_outs`
--
ALTER TABLE `design_lic_outs`
  ADD CONSTRAINT `design_lic_outs_des_id_foreign` FOREIGN KEY (`des_id`) REFERENCES `designs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `des_ass_ins`
--
ALTER TABLE `des_ass_ins`
  ADD CONSTRAINT `des_ass_ins_des_id_foreign` FOREIGN KEY (`des_id`) REFERENCES `designs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `des_ass_outs`
--
ALTER TABLE `des_ass_outs`
  ADD CONSTRAINT `des_ass_outs_des_id_foreign` FOREIGN KEY (`des_id`) REFERENCES `designs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `enfs`
--
ALTER TABLE `enfs`
  ADD CONSTRAINT `enfs_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `enfs_ent_id_foreign` FOREIGN KEY (`ent_id`) REFERENCES `entity_lists` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `enfs_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `stetes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `hearings`
--
ALTER TABLE `hearings`
  ADD CONSTRAINT `hearings_hear_id_foreign` FOREIGN KEY (`hear_id`) REFERENCES `litigation_summaries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `hearings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invests`
--
ALTER TABLE `invests`
  ADD CONSTRAINT `invests_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invests_ent_id_foreign` FOREIGN KEY (`ent_id`) REFERENCES `entity_lists` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invests_inag_id_foreign` FOREIGN KEY (`inag_id`) REFERENCES `agencies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invests_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `stetes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invests_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `leads`
--
ALTER TABLE `leads`
  ADD CONSTRAINT `leads_target_id_foreign` FOREIGN KEY (`target_id`) REFERENCES `targets` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `litigation_notices`
--
ALTER TABLE `litigation_notices`
  ADD CONSTRAINT `litigation_notices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `litigation_summaries`
--
ALTER TABLE `litigation_summaries`
  ADD CONSTRAINT `litigation_summaries_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `liti_docs`
--
ALTER TABLE `liti_docs`
  ADD CONSTRAINT `liti_docs_doc_id_foreign` FOREIGN KEY (`doc_id`) REFERENCES `litigation_summaries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notice_statuses`
--
ALTER TABLE `notice_statuses`
  ADD CONSTRAINT `notice_statuses_stat_id_foreign` FOREIGN KEY (`stat_id`) REFERENCES `litigation_notices` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notice_statuses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `patents`
--
ALTER TABLE `patents`
  ADD CONSTRAINT `patents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `patent_lic_ins`
--
ALTER TABLE `patent_lic_ins`
  ADD CONSTRAINT `patent_lic_ins_patent_id_foreign` FOREIGN KEY (`patent_id`) REFERENCES `patents` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pat_ass_ins`
--
ALTER TABLE `pat_ass_ins`
  ADD CONSTRAINT `pat_ass_ins_pat_id_foreign` FOREIGN KEY (`pat_id`) REFERENCES `patents` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pat_ass_outs`
--
ALTER TABLE `pat_ass_outs`
  ADD CONSTRAINT `pat_ass_outs_pat_id_foreign` FOREIGN KEY (`pat_id`) REFERENCES `patents` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `stetes`
--
ALTER TABLE `stetes`
  ADD CONSTRAINT `stetes_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `trademarks`
--
ALTER TABLE `trademarks`
  ADD CONSTRAINT `trademarks_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `trademarks_app_type_id_foreign` FOREIGN KEY (`app_type_id`) REFERENCES `trademark_gov_fees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `trademarks_appf_id_foreign` FOREIGN KEY (`appf_id`) REFERENCES `application_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `trademarks_catmak_id_foreign` FOREIGN KEY (`catmak_id`) REFERENCES `category_marks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `trademarks_ent_id_foreign` FOREIGN KEY (`ent_id`) REFERENCES `entity_lists` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `trademarks_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `trademarks_natapl_id_foreign` FOREIGN KEY (`natapl_id`) REFERENCES `nature_applications` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `trademarks_nate_id_foreign` FOREIGN KEY (`nate_id`) REFERENCES `nature_applicants` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `trademarks_prod_id_foreign` FOREIGN KEY (`prod_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `trademarks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `trademark_gov_fees`
--
ALTER TABLE `trademark_gov_fees`
  ADD CONSTRAINT `trademark_gov_fees_appf_id_foreign` FOREIGN KEY (`appf_id`) REFERENCES `application_fields` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `trad_ass_ins`
--
ALTER TABLE `trad_ass_ins`
  ADD CONSTRAINT `trad_ass_ins_trad_id_foreign` FOREIGN KEY (`trad_id`) REFERENCES `add_tradmarks` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `trad_ass_outs`
--
ALTER TABLE `trad_ass_outs`
  ADD CONSTRAINT `trad_ass_outs_trad_id_foreign` FOREIGN KEY (`trad_id`) REFERENCES `add_tradmarks` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `trad_lic_ins`
--
ALTER TABLE `trad_lic_ins`
  ADD CONSTRAINT `trad_lic_ins_trad_id_foreign` FOREIGN KEY (`trad_id`) REFERENCES `add_tradmarks` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `trad_lic_outs`
--
ALTER TABLE `trad_lic_outs`
  ADD CONSTRAINT `trad_lic_outs_trad_id_foreign` FOREIGN KEY (`trad_id`) REFERENCES `add_tradmarks` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `updata_status_designs`
--
ALTER TABLE `updata_status_designs`
  ADD CONSTRAINT `updata_status_designs_design_id_foreign` FOREIGN KEY (`design_id`) REFERENCES `designms` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `update_copyright_statuses`
--
ALTER TABLE `update_copyright_statuses`
  ADD CONSTRAINT `update_copyright_statuses_copy_id_foreign` FOREIGN KEY (`copy_id`) REFERENCES `copyrightns` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `update_status_trads`
--
ALTER TABLE `update_status_trads`
  ADD CONSTRAINT `update_status_trads_trade_id_foreign` FOREIGN KEY (`trade_id`) REFERENCES `trademarks` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
